<div class="pagination"><!-- pagination underneath the box's content -->
<ul>
	<li><a href="?f=<?php echo $statsName; ?><?php echo getOrderQueryParam( 1 );?>">首页</a></li>
	<?php 
		$lastPage = ceil( $totalNum/$pageSize );
	
		for( $i = max( 1 , $currPage - 5 ) ; $i<= min( $lastPage , $currPage + 5 ) ; $i++ ){
	?>
	<li><a href="?f=<?php echo $statsName; ?><?php echo getOrderQueryParam( $i );?>">
	<?php
	if( $i == $currPage )
	{
		echo "<strong>$i</strong>";
	}
	else
	{
		echo $i;
	}
	?></a>
	
	</li>
	<?php }
		
	?>
	<li><a href="?f=<?php echo $statsName; ?><?php echo getOrderQueryParam( $lastPage );?>">末页</a></li>
</ul>
</div>