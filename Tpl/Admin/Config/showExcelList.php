<?php
include( TPL_DIR .'/header.php' );
include( TPL_DIR .'/menu.php' );
//include( TPL_DIR .'/Config/subMenu.php' );
?>
<div id="container">
			<div class="inner-container">
				<?php 
					if( $messages )
					{
						foreach ( $messages as $msg )
						{
							echo "<div class='msg msg-ok'><p>$msg</p></div>";
						}
						
					}
				
				?>
				<div id="box1" class="box box-100"><!-- box full-width -->
					<form id="form1" class="plain" action="?mod=Config&act=uploadFile" method="post" enctype="multipart/form-data">
					<div class="boxin">
						<?php 
							include( TPL_DIR .'/Config/subMenu.php' );
						?>
						
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td class="tc"><input type="checkbox" id="data-1-check-all" name="data-1-check-all" value="true" /></td>
												<th>文件名</th>
												<td class="tc">文件类型</td>
												<td>修改日期</td>
												<td class="tc">Actions</td>
											</tr>
										</thead>
										
										<tbody>
										
										<?php
											if( $excelList )
											{
									        	foreach( $excelList as $exchanger )
									        	{
									        		?>
										       
										       		<tr class="first"><!-- .first for first row of the table (only if there is thead) -->
														<td class="tc"><input type="checkbox" id="data-1-check-1" name="data-1-check-1" value="true" /></td>
														
														<th><a href="?mod=Config&act=showExcelFile&fileName=<?php echo urlencode( $exchanger->getFileName( ) ) ?>"><?php echo $exchanger->getFileName() ?></a></th>
														
														<td class="tc"><span class="tag tag-gray">EXCEL</span></td>
														<td><?php echo date( "Y-m-d H:i:s" , $exchanger->getLastModifyTime() ) ?></td>
														
														<td class="tc"><!-- action icons - feel free to add/modify your own - icons are located in "css/img/led-ico/*" -->
															<ul class="actions">
																<li><a class="ico" href="?mod=Config&act=showExcelFile&fileName=<?php echo urlencode( $exchanger->getFileName( ) ) ?>" title="edit"><img src="css/img/led-ico/pencil.png" alt="edit" /></a></li>
																<li><a class="ico" href="#" title="delete"><img src="css/img/led-ico/delete.png" alt="delete" /></a></li>
															</ul>
														</td>
													</tr>
										       
										       
									
									        		<?php
									        	}
											}
								        ?>
											
											
										</tbody>
									</table>
								</fieldset>
							
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<li><a href="#">previous</a></li>
									<li><a href="#">next</a></li>
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						</form>
						<!-- code bellow is only example for switching between tabs, not regular content -->
						<div id="box1-grid" class="content"><!-- content box 2 for tabs switching (hidden by default) -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td class="tc"><input type="checkbox" id="data-1-check-all" name="data-1-check-all" value="true" /></td>
												<th>文件名</th>
												<td class="tc">文件类型</td>
												<td>修改日期</td>
												<td class="tc">Actions</td>
											</tr>
										</thead>
										
										<tbody>
										
										<?php
							
											if( $phpConfigList )
											{
									        	foreach( $phpConfigList as $exchanger )
									        	{
									        		?>
										       
										       		<tr class="first"><!-- .first for first row of the table (only if there is thead) -->
														<td class="tc"><input type="checkbox" id="data-1-check-1" name="data-1-check-1" value="true" /></td>
														
														<th><a href="?mod=Config&act=showPHPFile&fileName=<?php echo urlencode( $exchanger->getFileName( ) ) ?>"><?php echo $exchanger->getFileName() ?></a></th>
														<td class="tc"><span class="tag tag-gray">PHP</span></td>
														<td><?php echo date( "Y-m-d H:i:s" , $exchanger->getLastModifyTime() ) ?></td>
														
														<td class="tc"><!-- action icons - feel free to add/modify your own - icons are located in "css/img/led-ico/*" -->
															<ul class="actions">
																<li><a class="ico" href="?mod=Config&act=showPHPFile&fileName=<?php echo urlencode( $exchanger->getFileName( ) ) ?>" title="edit"><img src="css/img/led-ico/pencil.png" alt="edit" /></a></li>
																<li><a class="ico" href="#" title="delete"><img src="css/img/led-ico/delete.png" alt="delete" /></a></li>
															</ul>
														</td>
													</tr>
										       
										       
									
									        		<?php
									        	}
											}
								        ?>
											
											
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination">
								<ul>
									<li><a href="#">previous</a></li>
									<li><a href="#">next</a></li>
								</ul>
							</div>
						</div><!-- .content#box-1-grid -->
					</div>
				</div>
				
				
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include( TPL_DIR .'/footer.php' );
?>