<?php
include( TPL_DIR .'/header.php' );
include( TPL_DIR .'/menu.php' );
include( TPL_DIR .'/Config/subMenu.php' );
?>

<div class="pannel">
	<div class="float_left_div">
        <table class="list_table">
        	<tr class="list_table_title">
        		<td>文件名</td>
        		<td>最后修改日期</td>
        		<td>查看内容</td>
        	</tr>
        	<?php
        	foreach( $phpConfigList as $exchanger )
        	{
        		?>
	        	<tr>
	        		<td><?php echo $exchanger->getFileName() ?></td>
	        		<td><?php echo date( "Y-m-d H:i:s" , $exchanger->getLastModifyTime() ) ?></td>
	        		<td><a href="?mod=Config&act=showPHPFile&fileName=<?php echo urlencode( $exchanger->getFileName( ) ) ?>" >查看</a></td>
	        	</tr>
        		<?php
        	}
        	?>
        </table>
	</div>
</div>
<?php
include( TPL_DIR .'/footer.php' );
?>