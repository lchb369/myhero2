<?php
include( TPL_DIR .'/header.php' );
include( TPL_DIR .'/menu.php' );

?>



<div id="container">
			<div class="inner-container">
				
				<div id="box1" class="box box-100"><!-- box full-width -->
					<div class="boxin">
						<?php 
						include( TPL_DIR .'/Config/subMenu.php' );
						?>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									
										<?php
										if( $table )
										{
										?>
								
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<?php
									        	foreach( $table['columns'] as $column )
									        	{
									        	?>
									        		<td><?php echo $column; ?></td>
									        	<?php
									        	}
									        	?>
											</tr>
										</thead>
										
										<tbody>
										
										<?php
        	
								        	foreach( $table['datas'] as $row )
								        	{
								        		?>
									        	<tr>
									        	<?php
								        		foreach( $table['columns'] as $column )
								        		{
									        	?>
									        		<td><?php echo $row[$column] ?></td>
								        		<?php
								        		}
								        		?>
									        	</tr>
								        		<?php
								        	}
								        	?>
											
											
										</tbody>
									</table>
									<?php }?>
								</fieldset>
							</form>
							
						</div><!-- .content#box-1-holder -->
						
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include( TPL_DIR .'/footer.php' );
?>

