<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

function getOrderQueryParam( $page )
{
	global $userId;
	$url = "&input_uid=".$userId."&currPage=".$page."&startTime=".$_GET['startTime']."&endTime=".$_GET['endTime']."&byUid=".$_GET['byUid'];
	return $url;
}
?>
<script>
</script>

		<div id="container">
			<div class="inner-container"  style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>用户道具流水</h3>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form id="formMain" class="basic" action="" method="get" ><!-- Default basic forms -->
								<div class="inner-form">
									<dl>
										<?php 
											$startTime = $_GET['startTime'];
											$startTime = $startTime > 0 ? $startTime : date( "Y-m-d" , $_SERVER['REQUEST_TIME'] - 4 * 86400 );
										
											$endTime = $_GET['endTime'];
											$endTime = $endTime > 0 ? $endTime : date( "Y-m-d" , $_SERVER['REQUEST_TIME'] + 86400 );
										?>
										<dd>
											开始时间<input type="text" size="10" style="width:200px;margin-left:20px" name="startTime" class="txt" value="<?php echo $startTime;?>"/>
											结束时间<input type="text" size="10" style="width:200px;margin-left:20px" name="endTime" class="txt" value="<?php echo $endTime;?>"/>
										</dd>
									
										
										<dd>
											用户ID<input type="text" size="20" name="byUid"  style="width:200px;margin-left:20px" class="txt" value="<?php echo $_GET['byUid']; ?>"/>
										</dd>
										
										<dd>
											<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
											<input type="hidden" name="do" value="query" />
											<input type="hidden" name="f" value="<?php echo $statsName;?>" />
											<input class="button" type="submit" value="查询" />
										</dd>
									</dl>
								</div>
							</form>
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td class="tc">操作</td>
												<td class="tc">道具名称</td>
												<td class="tc">数量</td>
												<td class="tc">类型</td>
												<td class="tc">操作时间</td>
											</tr>
										</thead>
										<tbody>
											<?php
												foreach ( $statsData as $itemData )
												{
											?>
											<tr>
												<td class="tc"><?php echo $itemData['action']; ?></td>
												<?php
													$cardConfig = Common::getConfig( "card" );
													if( is_array( $itemData['id'] ) )
													{
														$itemName = array();
														foreach( $itemData['id'] as $cardCode )
														{
															$itemName[] = $cardConfig[$cardCode]['star'] . "星" . $cardConfig[$cardCode]['name'];
														}
														$itemName = implode( " , " , $itemName );
													}
													else 
													{
														$itemName = $cardConfig[$itemData['id']]['star'] . "星" . $cardConfig[$itemData['id']]['name'];
													}
												?>
												<td class="tc"><?php echo $itemName; ?></td>
												<td class="tc"><?php echo $itemData['num'];?></td>
												<td class="tc"><?php echo MakeCommand_Config::getItemLogType( $itemData['logType'] );?></td>
												<td class="tc"><?php echo date( 'Y-m-d H:i:s' , $itemData['addTime']);?></td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</fieldset>
							</form>
							<?php include('pagination.php');?>
						</div><!-- .content#box-1-holder -->
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>