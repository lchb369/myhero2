<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>
<script>
function submitUserInfo()
{
	$("#userInfo").submit();
}


function deleteCard( cardId )
{
	if( confirm( "是否删除") == true )
	{
		
		var url = "?input_uid=<?php echo $userId;?>&do=delete&card="+cardId;
		window.location.href = url;
	}
}
function addFriendReq()
{
	$("#do").val( "req" );
	$("#formMain").submit();
}
</script>
		<div id="container">
			<div class="inner-container">
				<div id="box1" class="box box-100"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3><?php echo MakeCommand_LangPack::get("friend_list_title"); ?></h3>
							
							<ul>
								<li><a rel="box1-tabular" href="#" class="active"><?php echo MakeCommand_LangPack::get("friend_list_view"); ?></a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
									<?php if( $canwrite == 1 ){ ?>
								<li><a rel="box1-grid" href="#"><?php echo MakeCommand_LangPack::get("friend_add_view"); ?></a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
									<?php }?>
							</ul>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td class="tc"><input type="checkbox" id="data-1-check-all" name="data-1-check-all" value="true" /></td>
												<td class="tc">ID</td>
												<th><?php echo MakeCommand_LangPack::get("friend_nickname"); ?></th>
												<td><?php echo MakeCommand_LangPack::get("friend_level"); ?></td>
												<td class="tc"><?php echo MakeCommand_LangPack::get("friend_camp"); ?></td>
												<td class="tc"><?php echo MakeCommand_LangPack::get("friend_add_time"); ?></td>
												<td class="tc"><?php echo MakeCommand_LangPack::get("friend_operation"); ?></td>
											</tr>
										</thead>
										<tfoot><!-- table foot - what to do with selected items -->
											<tr>
												<td colspan="8"><!-- do not forget to set appropriate colspan if you will edit this table -->
													<label>
														with selected do:
														<select name="data-1-groupaction">
															<option value="delete">delete</option>
															<option value="edit">edit</option>
														</select>
													</label>
													<input class="button altbutton" type="submit" value="OK" />
												</td>
											</tr>
										</tfoot>
										<tbody>
											<?php
												$count = count( $friendList );
												$pageNum = ceil( $count/5 );
												$currpage = $_GET['cardPage'] ?  $_GET['cardPage']  : 1 ;
												$index = 0;
												$start = 5*( $currpage - 1 ) + 1;
												$end = 5*$currpage;
												foreach ( $friendList as $key => $friendInfo )
												{
													$index += 1;
													if( $index < $start || $index > $end )
													{
														continue;
													}
													$fInfo = Data_User_Info::getInstance( $friendInfo['id'] )->getData();
													$fProfile = Data_User_Profile::getInstance( $friendInfo['id'] )->getData();
													
											?>
											<tr <?php if($index == 1 ){?> class="first" <?php } ?> ><!-- .first for first row of the table (only if there is thead) -->
												<td class="tc"><input type="checkbox" id="data-1-check-1" name="data-1-check-1" value="true" /></td>
												<td class="tc"><?php echo $friendInfo['id'];?></td>
												<th><?php echo $fProfile['nickName'];?></th>
												<td><?php echo $fInfo['level'];?></td>
												<td class="tc"><?php echo $fInfo['country'];?></td>
												
												<td class="tc"><?php echo date( "Y-m-d H:i:s" , $friendInfo['addTime'] );?></td>
												<td class="tc"><!-- action icons - feel free to add/modify your own - icons are located in "css/img/led-ico/*" -->
													<ul class="actions">
														<li><a class="ico" href="#" title="edit"><img src="css/img/led-ico/pencil.png" alt="edit" /></a></li>
														<li><a class="ico" onclick="deleteCard(<?php echo $friendInfo['id'];?>)" href="#" title="delete"><img src="css/img/led-ico/delete.png" alt="delete" /></a></li>
													</ul>
												</td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<li><a href="?f=friend&input_uid=<?php echo $userId;?>&cardPage=1">previous</a></li>
									<?php for( $i=1 ; $i<= $pageNum ; $i++ ){?>
									<li><a href="?f=friend&input_uid=<?php echo $userId;?>&cardPage=<?php echo $i;?>">
									<?php
									if( $i == $_GET['cardPage'] )
									{
										echo "<strong>$i</strong>";
									}
									else
									{
										echo $i;
									}
									?></a>
									
									</li>
									<?php }?>
									<li><a href="#">next</a></li>
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
						<div id="box1-grid" class="content card"><!-- content box 2 for tabs switching (hidden by default) -->
						<form id="formMain" class="basic" action="" method="get" ><!-- Default basic forms -->
							<div class="inner-form">
								<!-- error and information messages -->
								<div class="msg msg-ok"><p>I'm a <strong>success</strong> message and I'm proud of it!</p></div>
									<dl>
										<dd>
											<select id="some10" name="randFriendId">
												<option value="0"><?php echo MakeCommand_LangPack::get("friend_choose"); ?></option>
												<?php 
											
													foreach ( $helpers as $helperId )
													{
													
												?>
													<option value="<?php echo $helperId; ?>"><?php echo $helperId; ?></option>
												<?php 
													}
												?>
											</select>
										</dd>
									
										
										<dd>
											<input type="text" size="20" name="friendId" class="txt" value=""/>
										</dd>
										<dt><?php echo MakeCommand_LangPack::get("friend_add_by_id"); ?></dt>
										
										<dd>
											<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
											<input type="hidden" id="do" name="do" value="add" />
											<input type="hidden" name="f" value="friend" />
											<input class="button" type="submit" value="<?php echo MakeCommand_LangPack::get("friend_do_add"); ?>" />
											<input class="button" type="button" onclick="addFriendReq();" value="<?php echo MakeCommand_LangPack::get("friend_do_req"); ?>" />
										</dd>
									</dl>
								</div>
							</form>
						<div>	
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>