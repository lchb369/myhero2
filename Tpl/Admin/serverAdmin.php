<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');


?>
<script>
function submitUserInfo()
{
	$("#userInfo").submit();
}


function deleteCard( cardId )
{
	if( confirm( "是否删除") == true )
	{
		
		var url = "?input_uid=<?php echo $userId;?>&do=delete&card="+cardId;
		window.location.href = url;
	}
}


function editCard( cardId )
{
	if( confirm( "确认修改") == true )
	{
		var cardExp = $("#card_exp_"+cardId ).val();
		$.ajax({
			   type:"GET",
			   url:"?f=user&do=updateCard",
			   data:{
				   uid:<?php echo $userId;?>,
				   cid:cardId,
				   cexp:cardExp,
				},
			   success:function(echo){if(echo!='')alert(echo);window.location.reload();},
			   error:function(){alert('操作失败 请重试');}
		});
	}
}


</script>
		<div id="container">
			<div class="inner-container">

			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>