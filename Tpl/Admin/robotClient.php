<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
?>
<script>
(function(){
	if(typeof MYHERO !== 'undefined') return;
	this.MYHERO = {};
	MYHERO.log = function(str){
		if(typeof console !== 'undefined') console.info(str);
	};
	
	MYHERO.robots = {};
	MYHERO.url = '<?php echo Common::getServerAddr(); ?>Web/Api/api.php';
	MYHERO.urlSuf = '<?php echo '&pf='.$_SESSION['appPlatform'].'&sid='.$_SESSION['appSid']; ?>';

	MYHERO.getSession = function(id){
		MYHERO.log("[" + id + "] MYHERO.getSession: request");
		var queryStr = $.query.empty()
			.set('loginName', MYHERO.robots[id]['loginName'])
			.set('method', 'User.getSession').toString() + MYHERO.urlSuf;
		$.get(
			MYHERO.url + queryStr + '&rand=' + new Date().valueOf(),
			function(data,status){
				MYHERO.log("[" + id + "] MYHERO.getSession: " + data + "\nStatus: " + status);
				var ret = $.evalJSON(data);
				
				MYHERO.robots[id]['session'] = ret['session'];
				if(ret['newbie']){
					MYHERO.setNewbie(id);
				}else{
					MYHERO.randomAction(id);
				}
				
			}
		);
	};

	MYHERO.setNewbie = function(id){
		MYHERO.log("[" + id + "] MYHERO.setNewbie: request");
		var queryStr = $.query.empty()
			.set('nickName', MYHERO.robots[id]['loginName'])
			.set('country', 1)
			.set('step', 0)
			.set('loginName', MYHERO.robots[id]['loginName'])
			.set('session', MYHERO.robots[id]['session'])
			.set('method', 'User.setNewbie').toString() + MYHERO.urlSuf;
		$.get(
			MYHERO.url + queryStr + '&rand=' + new Date().valueOf(),
			function(data,status){
				MYHERO.log("[" + id + "] MYHERO.setNewbie: " + data + "\nStatus: " + status);
				var ret = $.evalJSON(data);
				
				MYHERO.robots[id]['uid'] = ret['user_info']['uid'];
				MYHERO.randomAction(id);
			}
		);
	};

	MYHERO.randomAction = function(id){
		MYHERO.log("[" + id + "] MYHERO.randomAction: request");

		var methodId = Math.floor(Math.random()) + 1;
		if(methodId == 1){
			MYHERO.sceneInit(id);
		}
	};

	MYHERO.sceneInit = function(id){
		MYHERO.log("[" + id + "] MYHERO.sceneInit: request");
		var queryStr = $.query.empty()
			.set('loginName', MYHERO.robots[id]['loginName'])
			.set('session', MYHERO.robots[id]['session'])
			.set('method', 'User.setNewbie').toString() + MYHERO.urlSuf;
		$.get(
			MYHERO.url + queryStr + '&rand=' + new Date().valueOf(),
			function(data,status){
				MYHERO.log("[" + id + "] MYHERO.sceneInit: Status: " + status);
				var ret = $.evalJSON(data);
				
				MYHERO.robots[id]['uid'] = ret['user_info']['uid'];
				MYHERO.randomAction(id);
			}
		);
	}
	
})();

$(document).ready(function(){
	var id = parseInt($.query.get("id"));
	if(id <= 0) return;
	 
	MYHERO.robots[id] = {};
	MYHERO.robots[id]['loginName'] = 'myheroRobots' + id;
	MYHERO.getSession(id);
});
</script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="content">
			
		</div>
		
    </body>
<?php
include('footer.php');
?>