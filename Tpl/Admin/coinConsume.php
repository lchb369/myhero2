<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

$tbSumItems = array(
	'sum' => array( 'name' => '消费元宝总数'),
);

$tbNumItems = array(
	'num' => array( 'name' => '消费人数' ),
);

foreach( Stats_Consume::$consumeConf as $consumeName => $conf )
{
	$tbSumItems[$consumeName.'Sum'] = array( 'name' => $conf['name']."元宝数" );
	$tbNumItems[$consumeName.'Num'] = array( 'name' => $conf['name']."次数" );
}

$dates = array();
$showSumDatas = array();
$showNumDatas = array();

/**
	消费金额数
 */
$index = 0;
foreach ( $tbSumItems as $item => $info )
{
	$showSumDatas['series'][$index]['name'] = $info['name'];
	foreach (  $consumeData as $date => $dateData )
	{
		
		$showSumDatas['series'][$index]['data'][] = $dateData[$item];
		$date = date( 'd' , strtotime( $date ) );
		if( !in_array( $date , $dates ))
		{
			$dates[] = $date;
		}
	}
	$index++;
}

/**
 	消费次数
 */
$index = 0;
foreach ( $tbNumItems as $item => $info )
{

	$showNumDatas['series'][$index]['name'] = $info['name'];
	foreach (  $consumeData as $date => $dateData )
	{
		$showNumDatas['series'][$index]['data'][] = $dateData[$item];
	}
	$index++;
}

?>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js"></script>
<script type="text/javascript">


$(function () {
    $('#statContainer').highcharts({
        chart: {
            type: 'line',
            marginRight: 130,
            marginBottom: 80,
            //width:800,
            //height:300,
        },
        title: {
            text:"消费元宝统计",
            x: -20 //center
        },
        /*
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
       */
        yAxis: {
            title: {
                text:"元宝数",
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100,
            borderWidth: 0
        },
        
        xAxis:  {"categories":<?php echo json_encode( $dates );?>},
        series: <?php echo json_encode( $showSumDatas['series'] );?>
    });
        // series: [{"name":"\u7559\u5b58\u7387(%)","data":[0,0,0,0,0,0,0,0]}]        });
   
});


$(function () {
    $('#statContainer1').highcharts({
        chart: {
            type: 'line',
            marginRight: 130,
            marginBottom: 80,
            //width:800,
            //height:300,
        },
        title: {
            text:"消费元宝次数",
            x: -20 //center
        },
        /*
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
       */
        yAxis: {
            title: {
                text:"次数",
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100,
            borderWidth: 0
        },
        
        xAxis:  {"categories":<?php echo json_encode( $dates );?>},
        series:  <?php echo json_encode( $showNumDatas['series'] );?>
       
    });
        // series: [{"name":"\u7559\u5b58\u7387(%)","data":[0,0,0,0,0,0,0,0]}]        });
   
});

function exportCsv()
{
	 var act =  'coinConsume';
	 var csvData = $('#statTable').table2CSV();
     window.location.href ='?f=exportCsv&act='+act+'&data='+encodeURI( csvData );
}
</script>

<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container" style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
				
					<div class="boxin">
						<div class="header">
							<h3>元宝消耗统计&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<?php echo date( 'Y-m' , $serverTime );?></h3>
							<a class="button" href="#" onclick="exportCsv()"  >导出</a>
						</div>
						<div id="statContainer" style="min-width: 800px; height: 400px; margin: 0 auto"></div><!-- 统计图表显示区域 -->
						
						<div id="statContainer1" style="min-width: 800px; height: 400px; margin: 0 auto"></div><!-- 统计图表显示区域 -->
						
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table id="statTable" cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td>统计日期</td>
												<td><a href="#" onclick="javascript:show">消费人数</a></td>
												<td>消费元宝</td>
												<?php 
												foreach( Stats_Consume::$consumeConf as $consumeName => $conf )
												{
												?>
													<td><?php echo $conf['name'];?><br/>(元宝/次数)</td>
												<?php 
												}
												
												?>
											</tr>
										</thead>
										
										<tbody>
								
										<?php
											//krsort( $consumeData );
											foreach ( $consumeData as  $eachDay => $eachInfo )
											{
										?>
											<tr <?php if($index == 1 ){?> class="first" <?php } ?> ><!-- .first for first row of the table (only if there is thead) -->
												<td class="tc"><?php  echo $eachDay ;?></td>
												<td class="tc"><?php echo $eachInfo['num'] ;?></td>
												<td><?php echo $eachInfo['sum'] ;?></td>
												
												<?php 
												foreach( Stats_Consume::$consumeConf as $consumeName => $conf )
												{
												?>
													<td><?php echo "(".$eachInfo[$consumeName.'Sum']."/".$eachInfo[$consumeName.'Num'].")" ;?></td>
												<?php 
												}
												
												?>
											</tr>
										
										<?php 
											
											}
										?>
										
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<?php 
										
										$time = $_GET['time'] ?  $_GET['time'] : $_SERVER['REQUEST_TIME'];
										
									?>
									
									<li><a href="?f=<?php echo $statsName;?>&time=<?php  echo  $time-30*86400;?>">前个月</a></li>
									<li><a href="?f=<?php echo $statsName;?>&time=<?php  echo  $time+30*86400;?>">后个月</a></li>
									
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
					
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		<script src="js/highcharts.js"></script>
<script src="js/modules/exporting.js"></script>
    </body>

<?php
include('footer.php');
?>
