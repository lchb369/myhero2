<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');
?>
<script>
function robotStart(){
	$("#robotStart").hide();
	$("#robotStop").show();
	var startId = parseInt($('#startId').val());
	var endId = parseInt($('#endId').val());
	for(var i = startId; i <= endId; i++){
		var frameStr = '<iframe style="display:none;" src="http://<?php echo $_SERVER['SERVER_ADDR'].substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')); ?>?f=robotClient&id='+i+'&rand=' + new Date().valueOf() + '"></iframe>';
		$("#frames").append(frameStr);
	}
};
function robotStop(){
	$("#frames").html('');
	$("#robotStart").show();
	$("#robotStop").hide();
}
</script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container">
				<div id="box1" class="box box-100"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>机器人测试</h3>	
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<div id="content">
								<div style="clear:both;">
									开始id：&nbsp;&nbsp;&nbsp;&nbsp; <input type="input" value="" id="startId">
								</div>
								<div style="clear:both;">
									结束id：&nbsp;&nbsp;&nbsp;&nbsp; <input type="input" value="" id="endId">
								</div>
								<div style="clear:both;">
									<input class="button" type="button" value="运行" id="robotStart" onclick="robotStart();">
									<input class="button" type="button" value="停止" id="robotStop" onclick="robotStop();" style="display: none;">
								</div>
							</div>
							<div id="frames">
							</div>
						</div><!-- .content#box-1-holder -->
				</div>
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>