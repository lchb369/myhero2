<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>
<script>

<?php 
$maxTitleLen = 10;
$maxContentRow = 7;
$maxContentRowLen = 11;
?>

function sendMail()
{
	var maxTitleLen = <?php echo $maxTitleLen; ?>;
	var maxContentRow = <?php echo $maxContentRow; ?>;
	var maxContentRowLen = <?php echo $maxContentRowLen; ?>;
	
	do{
		var errorMsg = "参数错误";
		var toId = $("#toId").val();
		if($.trim(toId) == "")
		{
			errorMsg = "用户id不能为空";
			break;
		}
		var title = $("#title").val();
		if($.trim(title) == "" || $.trim(title).length > maxTitleLen)
		{
			errorMsg = "标题为空或超长";
			break;
		}
		var content = $("#content").val();
		if($.trim(content) == "")
		{
			errorMsg = "邮件内容为空";
			break;
		}
		var contentArr = $.trim(content).split("\n");
		if(contentArr.length > maxContentRow)
		{
			errorMsg = "邮件内容行数超长";
			break;
		}
		var overLen = false;
		for(row in contentArr)
		{
			if(contentArr[row].length > maxContentRowLen)
			{
				overLen = true;
				errorMsg = "内容第" + (parseInt(row) + 1) + "行超长";
				break;
			}
		}
		if(overLen) break;
		
		var bonus = $("#bonus").val();
		if($.trim(bonus) == "") bonus = {};
		var data = {
			"toId":toId,
			"title":title,
			"content":content,
			"bonus":$.toJSON(bonus)	
		}
		$("#sendBtn").attr("disabled","disabled");
		
		$.ajax({
			   type:"POST",
			   url:"?f=mail&do=send&rand="+new Date().valueOf(),
			   data:data,
			   success:function(ret){
				   console != undefined && console.info(ret);
				   
					if(ret){
						ret = $.evalJSON(ret);
						if(ret && ret.rc == 0)
						{
							alert("发送成功");	
						}
						else
						{
							alert("发送失败");
						}
					}
					$("#sendBtn").attr("disabled","");
			   	},
			   error:function(){
				   alert('操作失败 请重试');
				   $("#sendBtn").attr("disabled","");
				}
		});
		errorMsg = "";
	}while(0);

	if(errorMsg != "") alert(errorMsg);
	
}

</script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container">
				<div id="box1" class="box box-100"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>邮件管理</h3>	
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<div style="clear:both;">
								用户id：&nbsp;&nbsp;&nbsp;&nbsp; <input type="input" value="" id="toId">
<!-- 								是否群发： <input type="checkbox" value="" id="sendAll"> -->
							</div>
							<div style="clear:both;">
								邮件标题： <input type="input" value="" id="title" size="30">
								<font style="color:red;">*不能超过<?php echo $maxTitleLen; ?>字符*</font>
							</div>
							<div style="clear:both;">
								邮件内容： <textarea rows="4" cols="30" id="content"></textarea>
								<font style="color:red;">*每行不能超过<?php echo $maxContentRowLen; ?>字符, 总行数不能超过<?php echo $maxContentRow; ?>行*</font>
							</div>
							<div style="clear:both;display:none;">
								奖励内容： <input type="input" value="" id="bonus">
							</div>
							<div style="clear:both;">
								<input class="button" type="button" value="发送" id="sendBtn" onclick="sendMail();">
							</div>
						</div><!-- .content#box-1-holder -->
				</div>
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>