<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>
<script>
function submitUserInfo()
{
	$("#userInfo").submit();
}


function deleteCard( cardId )
{
	if( confirm( "是否删除") == true )
	{
		
		var url = "?input_uid=<?php echo $userId;?>&do=delete&card="+cardId;
		window.location.href = url;
	}
}
</script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container">
				<div id="box1" class="box box-100"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>收入总览 统计</h3>
							<a class="button" href="#" onclick="path.click()"  >Android</a>
						    <a class="button" href="#" onclick="submitUpload()">IPhone</a>
							<a class="button" href="?mod=Config&act=buildConfig" >IPad</a>
							
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
											
												<td>统计日期</td>
												<td>付费用户</td>
												<td>付费次数</td>
												<td>收入</td>
												<td>单均价</td>
                                                <td>ARPU</td>
											</tr>
										</thead>
										<tfoot><!-- table foot - what to do with selected items -->
											<tr>
												<td colspan="8"><!-- do not forget to set appropriate colspan if you will edit this table -->
													<label>
													查询时间:
													</label>
													
												</td>
											</tr>
										</tfoot>
										<tbody>
											
											<tr ><!-- .first for first row of the table (only if there is thead) -->
												<td class="tc">$</td>
												<td class="tc">$</td>
												<td class="tc">$</td>
												<td class="tc">$</td>
												<td class="tc">$$$</td>
												<td class="tc">$$$</td>
											</tr>
											
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<li><a href="?f=order&input_uid=<?php echo $userId;?>&currPage=1&startTime=<?php echo $_GET['startTime'];?>&endTime=<?php echo $_GET['endTime']?>&byUid=<?php echo $_GET['byUid']?>">首页</a></li>
									<?php 
										if( $totalNum%20 == 0 )
										{
											$lastPage = intval( $totalNum/20 );
										}
										else
										{
											$lastPage = intval( $totalNum/20 ) + 1;
										}
									
										for( $i=1 ; $i<= $lastPage ; $i++ ){
										if( $i > 10 )
										{
											continue;
										}
									?>
									<li><a href="?f=order&input_uid=<?php echo $userId;?>&currPage=<?php echo $i;?>&startTime=<?php echo $_GET['startTime'];?>&endTime=<?php echo $_GET['endTime']?>&byUid=<?php echo $_GET['byUid']?>">
									<?php
									if( $i == $_GET['cardPage'] )
									{
										echo "<strong>$i</strong>";
									}
									else
									{
										echo $i;
									}
									?></a>
									
									</li>
									<?php }
										
									?>
									<li><a href="?f=order&input_uid=<?php echo $userId;?>&currPage=<?php echo $lastPage; ?>&startTime=<?php echo $_GET['startTime'];?>&endTime=<?php echo $_GET['endTime']?>&byUid=<?php echo $_GET['byUid']?>">末页</a></li>
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>