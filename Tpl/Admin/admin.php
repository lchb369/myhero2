<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');


?>
<script>
function submitUserInfo()
{
	$("#userInfo").submit();
}

function editAdmin( loginName )
{

	$('#box1 .header ul a').removeClass('active');
	$('#box1-grid-rel').addClass('active'); // make clicked tab active
	$('#box1 .content').hide(); // hide all content

	$('#box1-grid' ).show();

	//	$('#box1').find('#' + $('#box1-grid').attr('rel')).show(); // and show content related to clicked tab
}


function deleteAdmin( loginName )
{
	if( confirm( "是否删除") == true )
	{
		var url = "?f=admin&input_uid=<?php echo $userId;?>&do=delete&loginName="+loginName;
		window.location.href = url;
	}
}

</script>
		<div id="container">
			<div class="inner-container">
				<div id="box1" class="box box-100"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>管理员列表</h3>
							
							<ul>
								<li><a rel="box1-tabular" href="#" class="active">列表视图</a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
								<li><a rel="box1-grid" id="box1-grid-rel" href="#">添加管理员</a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
							</ul>
						</div>
						
						
						
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td class="tc"><input type="checkbox" id="data-1-check-all" name="data-1-check-all" value="true" /></td>
												<th>用户名</th>
												<td>权限</td>
												<td class="tc">操作</td>
											</tr>
										</thead>
										<tfoot><!-- table foot - what to do with selected items -->
											<tr>
												<td colspan="8"><!-- do not forget to set appropriate colspan if you will edit this table -->
													<label>
														with selected do:
														<select name="data-1-groupaction">
															<option value="delete">delete</option>
															<option value="edit">edit</option>
														</select>
													</label>
													<input class="button altbutton" type="submit" value="OK" />
												</td>
											</tr>
										</tfoot>
										<tbody>
											<?php
												$count = count( $adminList );
												$pageNum = ceil( $count/5 );
												$currpage = $_GET['cardPage'] ?  $_GET['cardPage']  : 1 ;
												$index = 0;
												$start = 5*( $currpage - 1 ) + 1;
												$end = 5*$currpage;
												foreach ( $adminList as $key => $adminInfo )
												{
													$index += 1;
													if( $index < $start || $index > $end )
													{
														continue;
													}
											?>
											<tr <?php if($index == 1 ){?> class="first" <?php } ?> ><!-- .first for first row of the table (only if there is thead) -->
												<td class="tc"><input type="checkbox" id="data-1-check-1" name="data-1-check-1" value="true" /></td>
												<td class="tc"><?php echo $adminInfo['loginName'];?></td>
												<th><?php echo $adminInfo['checkedName'];?></th>
												
												<td class="tc"><!-- action icons - feel free to add/modify your own - icons are located in "css/img/led-ico/*" -->
													<ul class="actions">
														<li><a class="ico" onclick="editAdmin()" href="#" title="edit"><img src="css/img/led-ico/pencil.png" alt="edit" /></a></li>
														<li><a class="ico" onclick="deleteAdmin('<?php echo $adminInfo['loginName'];?>')" href="#" title="delete"><img src="css/img/led-ico/delete.png" alt="delete" /></a></li>
													</ul>
												</td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<?php for( $i=1 ; $i<= $pageNum ; $i++ ){?>
									<li><a href="?f=admin&input_uid=<?php echo $userId;?>&cardPage=<?php echo $i;?>">
									<?php
									if( $i == $_GET['cardPage'] )
									{
										echo "<strong>$i</strong>";
									}
									else
									{
										echo $i;
									}
									?></a>
									
									</li>
									<?php }?>
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
						<div id="box1-grid" class="content card" ><!-- content box 2 for tabs switching (hidden by default) -->
						<form class="basic" action="" method="get" ><!-- Default basic forms -->
							<div class="inner-form">
								<!-- error and information messages -->
								<div class="msg msg-ok"><p>I'm a <strong>success</strong> message and I'm proud of it!</p></div>
									<dl>
										<dd>
											帐号: <input type="text" class="txt" name="loginName"  value="" />
										</dd>
										<dd>
											密码: <input type="password" class="txt" name="password" value="" />
										</dd>
										<dd>
											权限: <input type="checkbox" name="permissions[]" value="1" >游戏数据只读
											<input type="checkbox" name="permissions[]" value="2" >游戏数据读写
											<input type="checkbox" name="permissions[]" value="4" >配置管理
											<input type="checkbox" name="permissions[]" value="8" >订单管理
											<input type="checkbox" name="permissions[]" value="16" >数据分析
											<input type="checkbox" name="permissions[]" value="32" >管理员管理
										</dd>

										<dt></dt>
										<dd>
											<input type="hidden" name="f" value="admin" />	
											<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
											<input type="hidden" name="do" value="add" />
											<input class="button" type="submit" value="提交" />
										</dd>
									</dl>
								</div>
							</form>
						<div>	
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>