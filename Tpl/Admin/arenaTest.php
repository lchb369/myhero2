<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>
<script>
<?php 
$cardLen = 5;
?>
var battleCnt = 0;
function arenaTest()
{
	var data = {'times':$("#times").val(),'isFirst':$("#isFirst").val()};
	var cardLen = <?php echo($cardLen); ?>;
	var cCard = '' , bCard = '';
	for(var i=0;i<cardLen;i++)
	{
		if(cCard != '') cCard += ',';
		if(bCard != '') bCard += ','; 
		cCard += '{"val":"'+$("#cCard"+i).val()+'","lv":"'+$("#cCardLv"+i).val()+'"}';
		bCard += '{"val":"'+$("#bCard"+i).val()+'","lv":"'+$("#bCardLv"+i).val()+'"}';
	}
	data['cCard'] = '[' + cCard + ']';
	data['bCard'] = '[' + bCard + ']';
	$.ajax({
		   type:"GET",
		   url:"?f=arenaTest&do=pk&rand="+new Date().valueOf(),
		   data:data,
		   success:function(ret){
				if(ret){
					$("#console").prepend("#"+(++battleCnt)+" : "+ret+"<br/>");
				}
		   	},
		   error:function(){alert('操作失败 请重试');}
	});
}

</script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container">
				<div id="box1" class="box box-100"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>竞技场测试</h3>	
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<div style="clear:both;">
								<?php 
									$cardConfig = Common::getConfig( "card" );
									$selCard = '<option value="">选择武将 …</option>';
									foreach ( $cardConfig as  $cardId => $cardCnf )
									{
										$selCard .= '<option value="'.$cardId.'">'.$cardCnf['star'].'星-'.$cardCnf['name'].'-'.$cardId.'</option>';
									}
								?>
								<div style="float:left;width:50%;">
									挑战方：<br/>
									<?php
										for( $i = 0 ; $i < $cardLen ; $i++ )
										{
									?>
										<select id="cCard<?php echo($i); ?>" name="cCard" >
											<?php echo($selCard); ?>
										</select>
										&nbsp;&nbsp;&nbsp;
										等级：
										<input type="input" id="cCardLv<?php echo($i); ?>" name="cCardLv">
										<br/>
									<?php 
										}
									?>
								</div>
								<div style="float:left;width:50%;">
									被挑战方：<br/>
									<?php
										for( $i = 0 ; $i < $cardLen ; $i++ )
										{
									?>
										<select id="bCard<?php echo($i); ?>" name="bCard" >
											<?php echo($selCard); ?>
										</select>
										&nbsp;&nbsp;&nbsp;
										等级：
										<input type="input" id="bCardLv<?php echo($i); ?>" name="bCardLv">
										<br/>
									<?php 
										}
									?>
								</div>
							</div>
							<div style="clear:both;">
								次数： <input type="input" value="100" id="times">
								出手先后(1为挑战方先出手)： <input type="input" value="1" id="isFirst">
								&nbsp;&nbsp;&nbsp;
								<input class="button" type="button" value="PK" onclick="arenaTest();">
							</div>
							<div id="console" style="height:500px;overflow:auto;">
							</div>
						</div><!-- .content#box-1-holder -->
				</div>
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>