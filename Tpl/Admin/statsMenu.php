<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>
<script>
</script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container">
				<div id="box1" class="box box-100"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>数据统计</h3>	
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table>
<?php 
if(Helper_Common::inPlatform(array('wk'))){
?> 
                                         <tr>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=newUser"><span>日注册用户</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=loginUser"><span>日登录用户</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=userRefer"><span>新注册来源</span></a></td>
                                        </tr>
                                        <tr>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=keepLogin2Days"><span>次日留存率</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=keepLogin3Days"><span>3日留存率</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=keepLogin4Days"><span>4日留存率</span></a></td>
                                        </tr>
                                         <tr>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=keepLogin5Days"><span>5日留存率</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=keepLogin6Days"><span>6日留存率</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=keepLogin7Days"><span>7日留存率</span></a></td>
                                        </tr>
                                        
                                         <tr>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=keepLogin8Days"><span>8日留存率</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=keepLogin14Days"><span>14日留存率</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=keepLogin30Days"><span>30日留存率</span></a></td>
                                        </tr>
                                        
                                         <tr>
                                        	<td><a class="h-ico ico-login" href="?f=statDisplay&act=channelPay"><span>渠道付费统计</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=coinConsume<?php echo $queryStr ?>"><span>元宝消耗统计</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=coinKeep<?php echo $queryStr ?>"><span>元宝留存统计</span></a></td>
                                        </tr>
                                        <tr>
                                        	<td><a class="h-ico ico-login" href="?f=statsUserOnline<?php echo $queryStr ?>"><span>在线人数统计</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=statsUserOnlineByMonth<?php echo $queryStr ?>"><span>在线人数统计(按月)</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=statsUserGuide<?php echo $queryStr ?>"><span>新手引导统计</span></a></td>
                                        </tr>
                                         <tr>
                                        	<td><a class="h-ico ico-login" href="?f=userCoinLog<?php echo $queryStr ?>"><span>用户元宝流水</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=userItemLog<?php echo $queryStr ?>"><span>用户道具流水</span></a></td>
                                         	<td><a class="h-ico ico-login" href="?f=userLoginLog<?php echo $queryStr ?>"><span>用户登录流水</span></a></td>
                                        </tr>
<?php 
}
?> 
                                        <tr>
	                                        <!-- <td><a class="h-ico ico-login" href="?f=userArenaLog<?php echo $queryStr ?>"><span>用户竞技场流水</span></a></td> -->
                                        	<td><a class="h-ico ico-cash" href="?f=banList<?php echo $queryStr ?>"><span>封锁用户列表</span></a></td>
	                                        <td><a class="h-ico ico-cash" href="?f=activeCode<?php echo $queryStr ?>"><span>激活码使用记录</span></a></td>
                                        	<td><a class="h-ico ico-cash" href="?f=inviteThirdHistory<?php echo $queryStr ?>"><span>平台邀请记录</span></a></td>
                                        </tr>
                                        
                                        
                                       <tr>
	                                        <!-- <td><a class="h-ico ico-login" href="?f=userArenaLog<?php echo $queryStr ?>"><span>用户竞技场流水</span></a></td> -->
	                                        <td><a class="h-ico ico-cash" href="?f=userList<?php echo $queryStr ?>"><span>用户列表查询</span></a></td>
                                        	<td><a class="h-ico ico-cash" href="?f=fileLog<?php echo $queryStr ?>"><span>查看文件日志</span></a></td>
                                        </tr>
                                        
                                        <tr>
                                        	<!-- TODO:: --><!-- <td><a class="h-ico ico-cash" href="?f=newstatsPay<?php echo $queryStr ?>"><span>新增付费用户统计</span></a></td> -->
                                        </tr>
                                    </table>
								</fieldset>
							</form>
						</div><!-- .content#box-1-holder -->
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>