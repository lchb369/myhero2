<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

$startDate = $_GET['startDate'] ? trim( $_GET['startDate'] ) : date( "Ymd" , strtotime( "-7 day" ) );
$endDate = $_GET['endDate'] ? trim( $_GET['endDate'] ) :  date( "Ymd" );

/*
$data = array(
	'titleText' => '日注册用户统计',
	'yAxisText' =>  '用户数量',
	'categories' => "[ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]",
	'series' => "[{
                name: '数量',
                data: [7.0, 0, 0, 14.5, 18.2, 78, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
            }]",
);
*/



?>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/Calendar6.js"></script>
<script type="text/javascript" src="js/table2CSV.js"></script>
<script type="text/javascript">
$(function () {
        $('#statContainer').highcharts({
            chart: {
                type: 'line',
                marginRight: 130,
                marginBottom: 25,
                //width:800,
                //height:300,
            },
            title: {
                text:"<?php echo $data['titleText']; ?>",
                x: -20 //center
            },
            /*
            subtitle: {
                text: 'Source: WorldClimate.com',
                x: -20
            },
           */
            yAxis: {
                title: {
                    text:"<?php echo $data['yAxisText']; ?>",
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            
            xAxis:  <?php echo json_encode( $data['xAxis']); ?>,
            series: <?php echo json_encode( $data['series'] ); ?>
        });
    });


function exportCsv()
{
	 var act =  $('#act').val();
	 var csvData = $('#statTable').table2CSV();
     window.location.href ='?f=exportCsv&act='+act+'&data='+encodeURI( csvData );
}

</script>

<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container" style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<form name="form1" method="get" action="?f=statDisplay&act=newUser" >
					<div class="boxin">
						<div class="header">
							<h3>用户统计</h3>
							开始日期<input type="text" name="startDate" size="15" value="<?php echo $startDate;?>"  onclick="javascript:show_calendar( 'form1.startDate' );" />
							结束日期<input type="text" name="endDate" size="15"   value="<?php echo $endDate;?>"  onclick="javascript:show_calendar( 'form1.endDate' );" />
							<a class="button" href="#" onclick="form1.submit()"  >查询</a>
							<a class="button" href="#" onclick="exportCsv()"  >导出</a>
						</div>
					
						<input type="hidden" name="f" value="<?php echo $_GET['f'];?>" />
						<input type="hidden" id="act" name="act" value="<?php echo $_GET['act'];?>" />
						
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<div id="statContainer" style="min-width: 800px; height: 400px; margin: 0 auto"></div><!-- 统计图表显示区域 -->
							
						</div><!-- .content#box-1-holder -->
						</form>
						<!-- code bellow is only example for switching between tabs, not regular content -->
						<!-- .inner-container -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
							<div id="box1-tabular" class="content" style="margin-top: 30px"><!-- content box 1 for tab switching -->
								<fieldset>
									<table cellspacing="0" id="statTable" >
										<thead><!-- universal table heading -->
											<tr>
												<td>时间点</td>
												<?php 
													foreach ( $data['xAxis']['categories'] as $cate )
													{
														echo "<td>{$cate}</td>";
													}
												?>
											</tr>
										</thead>
										<tbody>
										<?php 
											foreach ( $data['series'] as $series )
											{
												echo "<tr><td>{$series['name']}</td>";
												
												foreach ( $series['data'] as $d )
												{
													
													echo "<td>{$d}</td>";
													
												}
												echo "</tr>";
											}
										?>
										</tbody>
									</table>
								</fieldset>
							
						</div><!-- .content#box-1-holder -->
				</div>	
		</div><!-- #container -->
    </body>
    
<script src="js/highcharts.js"></script>
<script src="js/modules/exporting.js"></script>
<?php
include('footer.php');
?>
