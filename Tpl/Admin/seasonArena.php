<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');
?>
<div class="pannel">
    <form method="GET" action="?">
    <b>用户ID:</b><input name="input_uid" type="text" value="<?php echo $_GET[ 'input_uid' ];?>"/>
    <input type="submit" value="查询"/>
    <input type="hidden" name="f" value="<?php echo $_GET[ 'f' ];?>"/>
    <input type="hidden" name="do" value="get"/>
    <textarea class="json"><?php echo json_encode( $seasonArenaInfo );?></textarea>
    </form>
</div>

<div class="pannel">
	<div class="float_left_div">
	    <b>当前赛季:</b><span>第<?php echo $seasonId-1;?>赛季</span>
		
	</div>
</div>

<div class="pannel">
	<div class="float_left_div">
	    <form method="GET" action="?">
    <b>头衔:</b><input name="title" type="text" value="<?php echo $title;?>"/>
	<input type="hidden" name="input_uid" value="<?php echo $seasonArenaInfo[ 'uid' ];?>"/>
    <input type="hidden" name="f" value="<?php echo $_GET[ 'f' ];?>"/>
    <input type="hidden" name="do" value="updateTitle"/>
	<input type="submit" value="修改" class="btn"/>
    </form>		
	</div>
</div>

<div class="pannel">
	<div class="float_left_div">
		<b>当前赛季信息:&nbsp;</b><span style="color:red;"><?php 
		if( !isset( $seasonArenaInfo['arena'][$seasonId] ) ) echo "没有当前赛季信息"
		?></span>
	    <form method="GET" action="?">
	    <?php 
	    if( !empty( $seasonArenaInfo ) )
	    {
	    ?>
	    <table class="list_table">
	    	<tr class="list_table_title">
	    		<td>用户ID</td>
	    		<td>竞技场积分</td>
	    		<td>竞技场代币</td>
	    		<td>竞技场战斗次数</td>
	    		<td>胜利次数</td>
	    		<td>今天剩余的战斗次数</td>
	    		<td>操作</td>
	    	</tr>
	    	<tr>
	    		<td><?php echo $seasonArenaInfo[ 'uid' ];?></td>
	    		<td><input type="text" name="score" value="<?php 
	    		if( is_null( $seasonArenaInfo['arena'][$seasonId][ 'score' ] ) )
	    			echo 1000;
	    		else
	    			echo $seasonArenaInfo['arena'][$seasonId][ 'score' ];		
	    		?>"/></td>
	    		<td><input type="text" name="point" value="<?php echo $point;?>"/></td>
	    		<td><input type="text" name="totalNum" value="<?php echo $seasonArenaInfo['arena'][$seasonId][ 'totalNum' ];?>"/></td>
	    		<td><input type="text" name="winNum" value="<?php echo $seasonArenaInfo['arena'][$seasonId][ 'winNum' ];?>"/></td>
	    		<td><input type="text" name="remainNum" value="<?php 
	    		if( is_null( $seasonArenaInfo['arena'][$seasonId][ 'remainNum' ] ) )
	    			echo 10;
	    		else
	    			echo $seasonArenaInfo['arena'][$seasonId][ 'remainNum' ];
	    		
	    		?>"/></td>
	    		<td>
					<input type="hidden" name="input_uid" value="<?php echo $seasonArenaInfo[ 'uid' ];?>"/>
				    <input type="hidden" name="f" value="<?php echo $_GET[ 'f' ];?>"/>
				    <input type="hidden" name="do" value="update"/>
					<input type="submit" value="修改" class="btn"/>
				</td>
	    	</tr>
	    </table>
	    <?php
	    }
	    ?>
	    </form>
	</div>
</div>

<div class="pannel">
	<div class="float_left_div">
	    <b>排名信息</b>
	    <form method="GET" action="?">
		<input type="hidden" name="input_uid" value="<?php echo $seasonArenaInfo[ 'uid' ];?>"/>
		<input type="hidden" name="f" value="<?php echo $_GET[ 'f' ];?>"/>
		<input type="hidden" name="do" value="clearRankInfo"/>
		<input type="submit" value="清空排名信息" class="btn"/>
	    </form>
		<div><pre><?php print_r( $rankInfo);?></pre></div>
	</div>
</div>


<div class="pannel">
	<div class="float_left_div">
	    <b>排名</b>
		<form method="GET" action="?">
		<input type="hidden" name="input_uid" value="<?php echo $seasonArenaInfo[ 'uid' ];?>"/>
		<input type="hidden" name="f" value="<?php echo $_GET[ 'f' ];?>"/>
		<input type="hidden" name="do" value="rank"/>
		<input type="submit" value="重新排名" class="btn"/>
	    </form>
	</div>
	
		<div class="float_left_div">
			<input type="button" onclick="_p.searchRank()" class="btn" value="排名查询"/>
			<div><pre id="rankTop"></pre></div>
	</div>
</div>

<div class="pannel">
	<div class="float_left_div">
		<table class="list_table">
	    	<tr class="list_table_title">
	    		<td>等级</td>
	    		<td>查询</td>
	    	</tr>
	    	<tr>
	    		<td><textarea id="beLootNumber" readonly cols="40" , rows="12"></textarea></td>
	    		<td>
	    			<input type="button" onclick="_p.searchSeasonArenaNum()" class="btn" value="查询"/>
				</td>
	    	</tr>
	    </table>
	</div>
</div>
<?php
include('footer.php');
?>