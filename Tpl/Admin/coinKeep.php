<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

$tbSumItems = array(
	'costSum' => array( 'name' => "消耗元宝数" ),
	'registerSum' => array( 'name' => "注册获得元宝数" ),
	'incomeSum' => array( 'name' => "游戏获得元宝数" ),
	'paySum' => array( 'name' => "充值元宝数" ),
);

$tbNumItems = array(
	'costNum' => array( 'name' => "消耗次数" ),
	'registerNum' => array( 'name' => "注册获得次数" ),
	'incomeNum' => array( 'name' => "游戏获得次数" ),
	'payNum' => array( 'name' => "充值次数" ),
);

$dates = array();
$showSumDatas = array();
$showNumDatas = array();
$tbSumLeft = array();

/**
	消费金额数
 */
$index = 0;
foreach ( $tbSumItems as $item => $info )
{
	$showSumDatas['series'][$index]['name'] = $info['name'];
	foreach (  $statsData as $date => $dateData )
	{
		
		$showSumDatas['series'][$index]['data'][] = $dateData[$item];
		$date = date( 'd' , strtotime( $date ) );
		if( !in_array( $date , $dates ))
		{
			$dates[] = $date;
		}
	}
	$index++;
}

/**
 	消费次数
 */
$index = 0;
foreach ( $tbNumItems as $item => $info )
{

	$showNumDatas['series'][$index]['name'] = $info['name'];
	foreach (  $statsData as $date => $dateData )
	{
		$showNumDatas['series'][$index]['data'][] = $dateData[$item];
	}
	$index++;
}


/**
 * 剩余元宝数
 */

	$tbSumLeft['series'][0]['name'] = '剩余元宝数';
	foreach (  $statsData as $date => $dateData )
	{
		$tbSumLeft['series'][0]['data'][] = $dateData['keepSum'] ? (int)$dateData['keepSum'] : 0 ;
	}


?>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js"></script>
<script type="text/javascript">


$(function () {
    $('#statContainer').highcharts({
        chart: {
            type: 'line',
            marginRight: 130,
            marginBottom: 80,
            //width:800,
            //height:300,
        },
        title: {
            text:"元宝统计",
            x: -20 //center
        },
        /*
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
       */
        yAxis: {
            title: {
                text:"元宝数",
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100,
            borderWidth: 0
        },
        
        xAxis:  {"categories":<?php echo json_encode( $dates );?>},
        series: <?php echo json_encode( $showSumDatas['series'] );?>
    });
        // series: [{"name":"\u7559\u5b58\u7387(%)","data":[0,0,0,0,0,0,0,0]}]        });
   
});


$(function () {
    $('#statContainer1').highcharts({
        chart: {
            type: 'line',
            marginRight: 130,
            marginBottom: 80,
            //width:800,
            //height:300,
        },
        title: {
            text:"次数统计",
            x: -20 //center
        },
        /*
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
       */
        yAxis: {
            title: {
                text:"次数",
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100,
            borderWidth: 0
        },
        
        xAxis:  {"categories":<?php echo json_encode( $dates );?>},
        series:  <?php echo json_encode( $showNumDatas['series'] );?>
       
    });
        // series: [{"name":"\u7559\u5b58\u7387(%)","data":[0,0,0,0,0,0,0,0]}]        });
   
});

$(function () {
    $('#statContainer2').highcharts({
        chart: {
            type: 'line',
            marginRight: 130,
            marginBottom: 80,
            //width:800,
            //height:300,
        },
        title: {
            text:"剩余元宝数",
            x: -20 //center
        },
        /*
        subtitle: {
            text: 'Source: WorldClimate.com',
            x: -20
        },
       */
        yAxis: {
            title: {
                text:"元宝数量",
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100,
            borderWidth: 0
        },
        
        xAxis:  {"categories":<?php echo json_encode( $dates );?>},
        series:  <?php echo json_encode( $tbSumLeft['series'] );?>
    });
});

function exportCsv()
{
	 var act =  'coinKeep';
	 var csvData = $('#statTable').table2CSV();
     window.location.href ='?f=exportCsv&act='+act+'&data='+encodeURI( csvData );
}
</script>

<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container" style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
				
					<div class="boxin">
						<div class="header">
							<h3>元宝留存统计&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<?php echo date( 'Y-m' , $serverTime );?></h3>
							<a class="button" href="#" onclick="exportCsv()"  >导出</a>
						</div>
						<div id="statContainer" style="min-width: 800px; height: 400px; margin: 0 auto"></div><!-- 统计图表显示区域 -->
						
						<div id="statContainer1" style="min-width: 800px; height: 400px; margin: 0 auto"></div><!-- 统计图表显示区域 -->
						<div id="statContainer2" style="min-width: 800px; height: 400px; margin: 0 auto"></div><!-- 统计图表显示区域 -->
						
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table id="statTable" cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td>统计日期</td>
												<td>消费<br/>(元宝/次数)</td>
												<td>注册获得<br/>(元宝/次数)</td>
												<td>游戏获得<br/>(元宝/次数)</td>
												<td>充值<br/>(元宝/次数)</td>
												
												<td>元宝增量</td>
												<td>元宝留存</td>
											</tr>
										</thead>
										
										<tbody>
								
										<?php
											foreach ( $statsData as  $eachDay => $eachInfo )
											{
										?>
											<tr <?php if($index == 1 ){?> class="first" <?php } ?> ><!-- .first for first row of the table (only if there is thead) -->
												<td class="tc"><?php  echo $eachDay ;?></td>
												
												<td><?php echo "(".$eachInfo['costSum']."/".$eachInfo['costNum'].")" ;?></td>
												<td><?php echo "(".$eachInfo['registerSum']."/".$eachInfo['registerNum'].")" ;?></td>
												<td><?php echo "(".$eachInfo['incomeSum']."/".$eachInfo['incomeNum'].")" ;?></td>
												<td><?php echo "(".$eachInfo['paySum']."/".$eachInfo['payNum'].")" ;?></td>
												
												<td><?php echo $eachInfo['registerSum'] + $eachInfo['incomeSum'] + $eachInfo['paySum'] - $eachInfo['costSum'];?></td>
												<td><?php echo $eachInfo['keepSum']?></td>
											</tr>
										
										<?php 
											
											}
										?>
										
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<?php 
										
										$time = $_GET['time'] ?  $_GET['time'] : $_SERVER['REQUEST_TIME'];
										
									?>
									
									<li><a href="?f=<?php echo $statsName;?>&time=<?php  echo  $time-30*86400;?>">前个月</a></li>
									<li><a href="?f=<?php echo $statsName;?>&time=<?php  echo  $time+30*86400;?>">后个月</a></li>
									
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
					
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		<script src="js/highcharts.js"></script>
<script src="js/modules/exporting.js"></script>
    </body>

<?php
include('footer.php');
?>
