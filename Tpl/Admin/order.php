<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

function getOrderQueryParam( $page )
{
	global $userId;
	$url = "&input_uid=".$userId."&currPage=".$page."&startTime=".$_GET['startTime']."&endTime=".$_GET['endTime']."&byUid=".$_GET['byUid']."&payPf=".$_GET['payPf']."&date=".$_GET['date'];
	return $url;
}
?>
<script>
function onChgPayType()
{
	$("#payPf").val($("#selPf").val());
	$("#formMain").submit();
}
$(function(){
	$("#selPf").val("<?php echo $_GET['payPf']; ?>");
});
</script>

		<div id="container">
			<div class="inner-container"  style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>订单列表</h3>
							<a class="button" href="?f=order&type=export<?php echo getOrderQueryParam( $currPage );?>">导出Excel>></a>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<select id="selPf" onchange="onChgPayType();">
							<option value="">所有</option>
							<?php
							foreach( $payTypes as $payType )
							{
							?>
							<option value="<?php echo $payType['platform'];?>"><?php echo $payType['platform'];?></option>
							<?php
							} 
							?>
							</select>
							<ul>
								<li><a rel="box1-tabular" href="#" class="active">列表视图</a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
								<li><a rel="box1-grid" href="#">订单查询</a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
							</ul>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td class="tc"><input type="checkbox" id="data-1-check-all" name="data-1-check-all" value="true" /></td>
												<td class="tc">用户ID</td>
												<th>订单序号</th>
												<th>第三方流水号</th>
												<td class="tc">商品名称</td>
												<td class="tc">商品数量</td>
												<td class="tc">元宝数量</td>
												<td class="tc">总金额</td>
												<td class="tc">支付平台</td>
												<td class="tc">下单时间</td>
												<td class="tc">交易时间</td>
												<td class="tc">支付状态</td>
												
											</tr>
										</thead>
										<tfoot><!-- table foot - what to do with selected items -->
											<tr>
												<td colspan="8"><!-- do not forget to set appropriate colspan if you will edit this table -->
													<label>
													<strong>订单总数:<?php echo (int)$totalNum;?>
													成功交易订单数:<?php echo (int)$succNum;?>
													总金额:<?php echo (int)$totalPrice;?>
													实付总额:<?php echo (int)$price;?>
													</strong>
													</label>
													
												</td>
											</tr>
										</tfoot>
										<tbody>
                                        
											<?php
												foreach ( $orders as $key => $order )
												{
													
											?>
											<tr <?php if($index == 1 ){?> class="first" <?php } ?> ><!-- .first for first row of the table (only if there is thead) -->
												<td class="tc"><input type="checkbox" id="data-1-check-1" name="data-1-check-1" value="true" /></td>
												<td><?php  echo $order['uid'];?></td>
												<td class="tc"><?php echo $order['id'];?></td>
												<td class="tc"><?php echo $order['thirdId'];?></td>
												<td><?php  echo $order['goodsName'];?></td>
												<td><?php  echo $order['goodsNum'];?></td>
												
												<td><?php  echo $order['coin'];?></td>
												<td><?php  echo $order['price'];?></td>
												<td><?php  echo $order['platform'];?></td>
													
												<td><?php  echo date( "Y-m-d H:i:s" , $order['addTime'] );?></td>
												<td><?php  echo date( "Y-m-d H:i:s" , $order['tradeTime'] );?></td>
												
												<td><?php  echo $order['status'];?></td>
											
											
											</tr>
											<?php }?>
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<li><a href="?f=order<?php echo getOrderQueryParam( 1 );?>">首页</a></li>
									<?php 
										if( $totalNum%20 == 0 )
										{
											$lastPage = intval( $totalNum/20 );
										}
										else
										{
											$lastPage = intval( $totalNum/20 ) + 1;
										}
									
										for( $i=1 ; $i<= $lastPage ; $i++ ){
										if( $i > 10 )
										{
											continue;
										}
									?>
									<li><a href="?f=order<?php echo getOrderQueryParam( $i );?>">
									<?php
									if( $i == $_GET['cardPage'] )
									{
										echo "<strong>$i</strong>";
									}
									else
									{
										echo $i;
									}
									?></a>
									
									</li>
									<?php }
										
									?>
									<li><a href="?f=order<?php echo getOrderQueryParam( $lastPage );?>">末页</a></li>
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
						<div id="box1-grid" class="content card"><!-- content box 2 for tabs switching (hidden by default) -->
						<form id="formMain" class="basic" action="" method="get" ><!-- Default basic forms -->
							<div class="inner-form">
								<!-- error and information messages -->
								<div class="msg msg-ok"><p>I'm a <strong>success</strong> message and I'm proud of it!</p></div>
									<dl>
										<dd>
											开始时间<input type="text" size="10" style="width:200px;margin-left:20px" name="startTime" class="txt" value="<?php echo date('Y-m-d H:i:s', $startTime);?>"/>
											结束时间<input type="text" size="10" style="width:200px;margin-left:20px" name="endTime" class="txt" value="<?php echo date('Y-m-d H:i:s', $endTime);?>"/>
										</dd>
									
										
										<dd>
											按用户ID<input type="text" size="20" name="byUid"  style="width:200px;margin-left:20px" class="txt" value=""/>
										</dd>
										
									
											<input type="hidden" id="payPf" name="payPf" value="<?php echo $_GET['payPf']; ?>" />
										
											<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
											<input type="hidden" name="do" value="query" />
											<input type="hidden" name="f" value="order" />
											<input class="button" type="submit" value="查询" />
										</dd>
									</dl>
								</div>
							</form>
						<div>	
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>