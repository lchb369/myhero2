<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

//[{"x":1373420700000,"y":4},{"x":1373421000000,"y":10},{"x":1373421300000,"y":0}],
?>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
$(function () {
    $(document).ready(function() {
        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });
        var chart;
        var updateSec = 500*5*60;//500为1秒,500*60*5为五分钟
        $('#containerStat').highcharts({
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                marginBottom: 60,
                //每次心跳更新的数据
                events: {
                    /*
                    load: function() {
                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function() {
                            var x = (new Date()).getTime(), // current time
                                y = 1.2;//Math.random();  ////每次心跳更新的数据
                            series.addPoint([x, y], true, true);
                        }, updateSec );
                    }
                    */
                }
            },
            title: {
                text: '在线人数统计'
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 100
            },
            yAxis: {
                title: {
                    text: '在线人数'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                	return Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) +'<br/>'+'在线'+this.y+'人';
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: 'Random data',
                data: <?php echo $statsData['display']; ?>,
            }]
        });
    }); 
});
	</script>
	</head>
	<body>
<script src="js/highcharts.js"></script>
<script src="js/modules/exporting.js"></script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container" style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>在线人数统计</h3>
						</div>
						<div id="containerStat" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
						
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td>时间点</td>
											<?php 
											
												foreach ( $statsData['data'] as $time => $num )
												{
													echo "<td>{$time}</td>";
												}
											
											?>
											
											</tr>
										</thead>
										
										<tbody>
								
						
									<tr>
										<td>登录人数</td>
										<?php 
										foreach ( $statsData['data'] as $time => $num )
										{
											echo "<td>{$num}</td>";
											
										}
										?>
									</tr>
									
										
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<?php 
										
										$preTime = $startTime - 3600;
										$nextTime = $startTime + 3600;
										
									?>
									<li><a href="?f=statsUserOnline&startTime=<?php echo $startTime-86400;?>" >前一天</a></li>
									<li><a href="?f=statsUserOnline&startTime=<?php echo $preTime;?>" >前一小时</a></li>
									<li><a href="?f=statsUserOnline&startTime=<?php echo $endTime-3600;?>" >当前时间</a></li>
									<li><a href="?f=statsUserOnline&startTime=<?php echo $nextTime;?>"  >后一小时</a></li>
									
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
					
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>
