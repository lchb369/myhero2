<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

function getOrderQueryParam( $page )
{
	global $userId;
	$url = "&input_uid=".$userId."&currPage=".$page."&startTime=".$_GET['startTime']."&endTime=".$_GET['endTime']."&byUid=".$_GET['byUid'];
	return $url;
}
?>
<script>
$(function(){
	$(":radio[name='coinType'][value='<?php echo $_GET['coinType']; ?>']").attr("checked", true);
});
</script>

		<div id="container">
			<div class="inner-container"  style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>用户元宝流水</h3>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form id="formMain" class="basic" action="" method="get" ><!-- Default basic forms -->
								<div class="inner-form">
									<dl>
										<?php 
											$startTime = $_GET['startTime'];
											$startTime = $startTime > 0 ? $startTime : date( "Y-m-d" , $_SERVER['REQUEST_TIME'] - 4 * 86400 );
										
											$endTime = $_GET['endTime'];
											$endTime = $endTime > 0 ? $endTime : date( "Y-m-d" , $_SERVER['REQUEST_TIME'] + 86400 );
										?>
										<dd>
											开始时间<input type="text" size="10" style="width:200px;margin-left:20px" name="startTime" class="txt" value="<?php echo $startTime;?>"/>
											结束时间<input type="text" size="10" style="width:200px;margin-left:20px" name="endTime" class="txt" value="<?php echo $endTime;?>"/>
										</dd>
									
										
										<dd>
											用户ID<input type="text" size="20" name="byUid"  style="width:200px;margin-left:20px" class="txt" value="<?php echo $_GET['byUid']; ?>"/>
											&nbsp;&nbsp;&nbsp;&nbsp;
											获得<input type="radio" name="coinType" value="income"/>
											充值<input type="radio" name="coinType" value="pay"/>
											消耗<input type="radio" name="coinType" value="consume"/>
										</dd>
										
										<dd>
											<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
											<input type="hidden" name="do" value="query" />
											<input type="hidden" name="f" value="<?php echo $statsName;?>" />
											<input class="button" type="submit" value="查询" />
										</dd>
									</dl>
								</div>
							</form>
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td class="tc">类型</td>
												<td class="tc">子类型</td>
												<td class="tc">数量</td>
												<td class="tc">操作时间</td>
											</tr>
										</thead>
										<tbody>
											<?php
												foreach ( $statsData as $loginData )
												{
											?>
											<tr>
												<?php
												$subType = ""; 
												//子类型
												if( !empty( $loginData['subType'] ) )
												{
													//首次通关 firstPass.type->floorId->roomId
													if( $loginData['gid'] == "firstPass" )
													{
														$roomInfo = explode( "->", $loginData['subType'] );
														$battleType = $roomInfo[0];
														$floorId = $roomInfo[1];
														$roomId = $roomInfo[2];
														$battleConfig = Common::getConfig( $roomInfo[0] );
														$battleConfig = $battleConfig[$floorId];
														$roomConfig = Battle_Model::getBattleConfig( $floorId , $roomId , $battleType );
														$subType = $battleType."->".$battleConfig['name']."->".$roomConfig['name'];
													}
													elseif( $loginData['gid'] == "paymentOrder" )
													{
														$subType = $loginData['subType'];
													}
												}
												?>
												<td class="tc"><?php echo MakeCommand_Config::getCoinLogType( $loginData['gid'] );?></td>
												<td class="tc"><?php echo $subType;?></td>
												<td class="tc"><?php echo $loginData['coin'];?></td>
												<td class="tc"><?php echo date( 'Y-m-d H:i:s' , $loginData['addTime']);?></td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</fieldset>
							</form>
							<?php include('pagination.php');?>
						</div><!-- .content#box-1-holder -->
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>