<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

function getOrderQueryParam( $page )
{
	global $userId;
	$url = "&input_uid=".$userId."&currPage=".$page."&startTime=".$_GET['startTime']."&endTime=".$_GET['endTime'];
	return $url;
}
?>
<script>
</script>

		<div id="container">
			<div class="inner-container"  style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>用户登录统计</h3>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form id="formMain" class="basic" action="" method="get" ><!-- Default basic forms -->
								<div class="inner-form">
									<dl>
										<?php 
											$startTime = $_GET['startTime'];
											$startTime = $startTime > 0 ? $startTime : date( "Y-m-01 00:00:00" , $_SERVER['REQUEST_TIME'] );
										
											$endTime = $_GET['endTime'];
											$endTime = $endTime > 0 ? $endTime : date( "Y-m-d H:i:s" , $_SERVER['REQUEST_TIME'] );
										?>
										<dd>
											开始时间<input type="text" size="10" style="width:200px;margin-left:20px" name="startTime" class="txt" value="<?php echo $startTime;?>"/>
											结束时间<input type="text" size="10" style="width:200px;margin-left:20px" name="endTime" class="txt" value="<?php echo $endTime;?>"/>
										</dd>
									
										
										<dd>
											按用户ID<input type="text" size="20" name="byUid"  style="width:200px;margin-left:20px" class="txt" value="<?php echo $_GET['byUid']; ?>"/>
										</dd>
										
										<dd>
											<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
											<input type="hidden" name="do" value="query" />
											<input type="hidden" name="f" value="<?php echo $statsName;?>" />
											<input class="button" type="submit" value="查询" />
										</dd>
									</dl>
								</div>
							</form>
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td class="tc">IP</td>
												<td class="tc">登录时间</td>
											</tr>
										</thead>
										<tbody>
											<?php
												foreach ( $statsData as $loginData )
												{
													
											?>
											<tr>
												<td class="tc"><?php echo $loginData['ip'];?></td>
												<td class="tc"><?php echo $loginData['loginTime'];?></td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<li><a href="?f=<?php echo $statsName; ?><?php echo getOrderQueryParam( 1 );?>">首页</a></li>
									<?php 
										if( $totalNum%$pageSize == 0 )
										{
											$lastPage = intval( $totalNum/$pageSize );
										}
										else
										{
											$lastPage = intval( $totalNum/$pageSize ) + 1;
										}
									
										for( $i=1 ; $i<= $lastPage ; $i++ ){
										if( $i > 10 )
										{
											continue;
										}
									?>
									<li><a href="?f=order<?php echo getOrderQueryParam( $i );?>">
									<?php
									if( $i == $_GET['cardPage'] )
									{
										echo "<strong>$i</strong>";
									}
									else
									{
										echo $i;
									}
									?></a>
									
									</li>
									<?php }
										
									?>
									<li><a href="?f=<?php echo $statsName; ?><?php echo getOrderQueryParam( $lastPage );?>">末页</a></li>
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>