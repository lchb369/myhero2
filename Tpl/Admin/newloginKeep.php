<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>
<script>
</script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container" style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>新注册留存统计</h3>
							<span style="color:red;">(*創建角色名稱後注册用户+1)</span>
							<ul>
								<li><a rel="box1-tabular" href="#" class="active">用户分析</a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
								<li><a rel="box1-grid" href="#">自助查询</a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
							</ul>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<th>统计日期</th>
												<td class="tc">新注册用户数</td>
												<td>1日留存率</td>
												<td>2日留存率</td>
												<td class="tc">3日留存率</td>
                                                <td class="tc">4日留存率</td>
                                                <td class="tc">5日留存率</td>
                                                <td class="tc">6日留存率</td>
                                                <td class="tc">7日留存率</td>
                                                <td class="tc">14日留存率</td>
                                                <td class="tc">30日留存率</td>
											</tr>
										</thead>
										<tfoot><!-- table foot - what to do with selected items -->
											<tr>
												<td colspan="11"><!-- do not forget to set appropriate colspan if you will edit this table -->
													<label>
													查询时间:$
													</label>
													
												</td>
											</tr>
										</tfoot>
										<tbody>
								
									<?php 
									
									if(  $monthStats )
									{
									
									$minTime = strtotime( "2013-04-25");
									krsort(  $monthStats );
									foreach ( $monthStats as $eachDay => $eachInfo )
									{
									
										  if( strtotime( $eachDay ) < $minTime )
										  {
											continue;
										}		
								?>
											<tr <?php if($index == 1 ){?> class="first" <?php } ?> ><!-- .first for first row of the table (only if there is thead) -->
												<td class="tc"><?php echo  $eachDay;?></td>
												<td class="tc"><?php echo  $eachInfo['registerNum'];?></td>
												<td class="tc"><?php echo  $eachInfo[1];?></td>									
												<td><?php echo  $eachInfo[2];?></td>
												<td><?php echo  $eachInfo[3];?></td>
												<td><?php echo  $eachInfo['4'];?></td>
												<td><?php echo  $eachInfo['5'];?></td>
												<td><?php echo  $eachInfo['6'];?></td>
												<td><?php echo  $eachInfo['7'];?></td>
                                                <td><?php echo  $eachInfo['14'];?></td>
                                                <td><?php echo  $eachInfo['30'];?></td>					
											</tr>
										<?php 
										}
									}
										?>
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
							<?php 
										$time = $_GET['time'] ?  $_GET['time'] : $_SERVER['REQUEST_TIME'];
									?>
								<ul>
									<li><a href="?f=newloginKeep&time=<?php  echo  $time-30*86400;?>">前个月</a></li>
									<li><a href="?f=newloginKeep&time=<?php  echo  $time+30*86400;?>">后个月</a></li>
								</ul>
							
							</div>
						</div><!-- .content#box-1-holder -->
					
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>
