<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

$platform = Common::getConfig( "platform" );
$filename = "/data/stats/".$platform.".txt" ;
$data = file_get_contents( "/data/stats/".$platform.".txt"  );
$dataArr = explode( "\n", $data );

$statsData = array();
foreach ( $dataArr as $lineData )
{
	$lineData = str_replace( "\t",  "@", $lineData );
	$lineData = str_replace( " ",  "@", $lineData );
    $lineData =  	preg_replace( "/(@)+/",  "@" , $lineData)."\n";
	
	$col = explode(  "@",  $lineData );
	$statsData[] = $col; 
}

unset(  $statsData[0] );
$startTime = strtotime( "2013-04-25" );


?>

		<div id="container">
			<div class="inner-container"  style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>用户统计</h3>	 
							
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<th>日期</th>
												<td>注册人数</td>
												<td class="tc">活跃人数</td>
												<td class="tc">1日留存</td>
												<td class="tc">2日留存</td>
												<td class="tc">3日留存</td>
												<td class="tc">4日留存</td>
												<td class="tc">5日留存</td>
											
												<td class="tc">7日留存</td>
												<td class="tc">14日留存</td>
											</tr>
										</thead>
										
										<tbody>
											<tr  class="first"  ><!-- .first for first row of the table (only if there is thead) -->
							
									<?php 
									
										$maxTimes = intval( ( $_SERVER['REQUEST_TIME'] - $startTime  ) / 86400 );
										
										for ( $i=0; ;$i++ )
										{
											$eachDay = date( "Ymd" ,  $startTime + $i*86400 );
											if(  $eachDay >= date( "Ymd") )
											{
												break;
											}
											$showDay = date( "Y-m-d" ,  $startTime + $i*86400 );
											$reloginTimes = $maxTimes - $i -1;
										
									?>
							
												<td class="tc"><?php echo $showDay;?></td>
												<td><?php  echo $statsData[1][$i+1];?></td>
												<td><?php   echo $statsData[2][$i+1];?></td>
												<!-- 留存率-->
											
						
												<td><?php   if( $reloginTimes >= 1 ){  echo $statsData[3][$i+1]; }else{ echo "-";}?></td>
												<td><?php   if( $reloginTimes >= 2 ){  echo $statsData[4][$i+1]; }else{ echo "-";}?></td>
												<td><?php   if( $reloginTimes >= 3 ){  echo $statsData[5][$i+1]; }else{ echo "-";}?></td>
												<td><?php   if( $reloginTimes >= 4 ){  echo $statsData[6][$i+1]; }else{ echo "-";}?></td>
												<td><?php   if( $reloginTimes >= 5 ){  echo $statsData[7][$i+1]; }else{ echo "-";}?></td>
												
												<td><?php   if( $reloginTimes >= 7 ){  echo $statsData[8][$i+1]; }else{ echo "-";}?></td>
												<td><?php   if( $reloginTimes >= 14 ){  echo $statsData[9][$i+1]; }else{ echo "-";}?></td>
											
											
											
											</tr>
										<?php
										}
										?>
										
										</tbody>
									</table>
								</fieldset>
							</form>
							
						</div><!-- .content#box-1-holder -->
					
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>