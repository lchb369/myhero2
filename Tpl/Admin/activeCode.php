<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

function getOrderQueryParam( $page )
{
	global $userId;
	$url = "&input_uid=".$userId."&currPage=".$page."&startTime=".$_GET['startTime']."&endTime=".$_GET['endTime']."&byUid=".$_GET['byUid']."&byCode=".$_GET['byCode'];
	return $url;
}
?>
<script>
function activeCode()
{
	$("#do").val( "query" );
	$("#formMain").submit();
}


</script>

		<div id="container">
			<div class="inner-container"  style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>激活码使用记录</h3>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form id="formMain" class="basic" action="" method="get" ><!-- Default basic forms -->
								<div class="inner-form">
									<dl>
										<?php 
											$startTime = $_GET['startTime'];
											$startTime = $startTime > 0 ? $startTime : date( "Y-m-d" , $_SERVER['REQUEST_TIME'] - 4 * 86400 );
										
											$endTime = $_GET['endTime'];
											$endTime = $endTime > 0 ? $endTime : date( "Y-m-d" , $_SERVER['REQUEST_TIME'] + 86400 );
										?>
										<!-- 
										<dd>
											开始时间<input type="text" size="10" style="width:200px;margin-left:20px" name="startTime" class="txt" value="<?php echo $startTime;?>"/>
											结束时间<input type="text" size="10" style="width:200px;margin-left:20px" name="endTime" class="txt" value="<?php echo $endTime;?>"/>
										</dd>
										-->
										
										<dd>
											按用户ID<input type="text" size="20" name="byUid" id="byUid"
											 style="width:200px;margin-left:20px" class="txt" value="<?php echo $_GET['byUid'];?>"/>
											 按激活码<input type="text" size="20" name="byCode" id="byCode"
											 style="width:200px;margin-left:20px" class="txt" value="<?php echo $_GET['byCode'];?>"/>
										</dd>
										 
										
										<dd>
											<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
											<input type="hidden" name="do" id="do" value="query" />
											<input type="hidden" name="f" value="<?php echo $statsName;?>" />
											<!-- <input class="button" type="submit" value="查询" /> -->
											<input class="button" type="button" value="查询" onclick="activeCode();"/>
										</dd>
									</dl>
								</div>
							</form>
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td class="tc">uid</td>
												<td class="tc">nickname</td>
												<td class="tc">激活码</td>
											</tr>
										</thead>
										<tbody>
											<?php
												foreach ( $statsData as $activeCodeData )
												{
													$_uid = $activeCodeData['2'];
													$userProfile = User_Model::exist($_uid)
														? User_Profile::getInstance($_uid)->getData()
														: array('nickName' => 'not exist');
											?>
											<tr>
												<td class="tc"><?php echo $activeCodeData['2'];?></td>
												<td class="tc"><?php echo $userProfile['nickName'];?></td>
												<td class="tc"><?php echo $activeCodeData['0'];?></td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</fieldset>
							</form>
							<?php include('pagination.php'); ?>										
						</div><!-- .content#box-1-holder -->
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>