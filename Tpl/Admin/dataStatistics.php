<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>
<script>
</script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container">
				<div id="box1" class="box box-100"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>数据统计</h3>	
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table>
                                        <tr>
                                        	<?php $queryStr = $_GET['input_uid'] ? "&input_uid={$_GET['input_uid']}&do=get" : ''; ?>
                                        	<td><a class="h-ico ico-login" href="?f=statsLogin<?php echo $queryStr ?>"><span>每日注册登陆统计</span></a></td>
                                            <td><a class="h-ico ico-login" href="?f=newstatsLogin<?php echo $queryStr ?>"><span>新注册来源统计</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=newloginKeep<?php echo $queryStr ?>"><span>新注册留存统计</span></a></td>
                                        </tr>
                                        <tr>
                                            <td><a class="h-ico ico-cash" href="?f=channelstatsPay<?php echo $queryStr ?>"><span>渠道付费统计</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=coinConsume<?php echo $queryStr ?>"><span>元宝消耗统计</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=statsUserOnline<?php echo $queryStr ?>"><span>在线人数统计</span></a></td>
                                        </tr>
                                        <tr>
                                        	<td><a class="h-ico ico-login" href="?f=userCoinLog<?php echo $queryStr ?>"><span>用户元宝流水</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=userItemLog<?php echo $queryStr ?>"><span>用户道具流水</span></a></td>
                                        	<td><a class="h-ico ico-login" href="?f=userArenaLog<?php echo $queryStr ?>"><span>用户竞技场流水</span></a></td>
                                        </tr>
                                         <tr>
                                         	<td><a class="h-ico ico-login" href="?f=userLoginLog<?php echo $queryStr ?>"><span>用户登录流水</span></a></td>
                                         	<td><a class="h-ico ico-cash" href="?f=banList<?php echo $queryStr ?>"><span>封锁用户列表</span></a></td>
                                        	<td><a class="h-ico ico-cash" href="?f=fileLog<?php echo $queryStr ?>"><span>查看文件日志</span></a></td>
                                            <!-- TODO:: --><!-- <td><a class="h-ico ico-login" href="?f=newloginPay<?php echo $queryStr ?>"><span>新注册付费统计</span></a></td> -->
                                            <!-- TODO:: --><!-- <td><a class="h-ico ico-login" href="?f=loginSource<?php echo $queryStr ?>"><span>登陆来源统计</span></a></td> -->
                                            <!-- TODO:: --><!-- <td><a class="h-ico ico-login" href="?f=newbieStats<?php echo $queryStr ?>"><span>新手引导统计</span></a></td> -->
                                        	<!-- TODO:: --><!-- <td><a class="h-ico ico-active" href="?f=statsActive<?php echo $queryStr ?>"><span>用户活跃天统计</span></a></td> -->
                                            <!-- TODO:: --><!-- <td><a class="h-ico ico-active" href="?f=continuestatsActive<?php echo $queryStr ?>"><span>用户连续活跃天统计</span></a></td> -->
                                            <!-- TODO:: --><!-- <td><a class="h-ico ico-cash" href="?f=statsPay<?php echo $queryStr ?>"><span>收入总览统计</span></a></td> -->
                                        </tr>
                                        
                                      
                                        
                                        <tr>
                                        	<!-- TODO:: --><!-- <td><a class="h-ico ico-cash" href="?f=newstatsPay<?php echo $queryStr ?>"><span>新增付费用户统计</span></a></td> -->
                                        </tr>
                                    </table>
								</fieldset>
							</form>
						</div><!-- .content#box-1-holder -->
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>