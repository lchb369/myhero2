<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>
<script>
</script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container" style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>新手引导统计&nbsp;&nbsp;<?php echo date( "Y-m" , $serverTime ); ?></h3>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td style="width:100px;">统计日期</td>
												<?php 
													$newbieLog['type'] = array();
													foreach( $statsData as $time => &$dayStats )
													{
														foreach( $dayStats as &$stats)
														{
															$newbieLog['type'][$stats['subType']] = $stats['subType'];
														} 
													}
													foreach( $newbieLog['type'] as $typeKey => $typeValue )
													{
												?>
												<td style="width:50px;"><?php echo $typeValue; ?></td>
												<?php
													}
												?>
											</tr>
										</thead>
										<tfoot><!-- table foot - what to do with selected items -->
											<tr>
												<td colspan="8"><!-- do not forget to set appropriate colspan if you will edit this table -->
													<label>
													查询时间:<?php  echo $useTime; ?>
													</label>
												</td>
											</tr>
										</tfoot>
										<tbody>
											<?php
												foreach( $statsData as $time => &$dayStats )
												{
											?>
												<tr>
													<td><?php echo date( 'Y-m-d' , $time ); ?></td>
											<?php
													foreach( $newbieLog['type'] as $typeKey => $typeValue )
													{
														unset( $referPay );
														
														foreach( $dayStats as &$stats)
														{
															if( $typeKey == $stats['subType'] )
															{
																$referPay = floatval( $stats['count'] );
																break;
															}
														}
											?>
														<td><?php echo isset( $referPay ) ? $referPay : 0; ?></td>
											<?php
													} 
											?>
												</tr>
											<?php
												}
											?>
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
									<?php 
										$time = $_GET['time'] ?  $_GET['time'] : $_SERVER['REQUEST_TIME'];
									?>
								<ul>
									<li><a href="?f=<?php echo $statsName; ?>&time=<?php  echo  $time-30*86400;?>">前个月</a></li>
									<li><a href="?f=<?php echo $statsName; ?>&time=<?php  echo  $time+30*86400;?>">后个月</a></li>
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
						<!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>
