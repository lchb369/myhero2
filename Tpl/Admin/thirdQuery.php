<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>
<script>
function submitUserInfo()
{
	$("#userInfo").submit();
}


function deleteCard( cardId )
{
	if( confirm( "是否删除") == true )
	{
		var url = "?input_uid=<?php echo $userId;?>&do=delete&card="+cardId;
		window.location.href = url;
	}
}
</script>

		<div id="container">
			<div class="inner-container"  style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>注册用户</h3>
							 
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
											
												<td>时间</td>
												<td>注册用户数</td>
												<td>订单数</td>
												<td>订单总金额</td>
											
											</tr>
										</thead>
										<tfoot><!-- table foot - what to do with selected items -->
											<tr>
												<td colspan="8"><!-- do not forget to set appropriate colspan if you will edit this table -->
													<label>
													<strong>
													总用户数:<?php echo $total;?> 订单数:<?php echo $orderCount;?>  总金额:<?php echo $sumTotal;?> 
													</strong>
													</label>
													
												</td>
											</tr>
										</tfoot>
										<tbody>
                                        
											<?php
												if(  !empty( $recordData ) )
												{
											
												foreach ( $recordData as $date => $record )
												{
													
													if( $record['register'] == 0 )
													{
														continue;
													}
													
											?>
											<tr <?php if($index == 1 ){?> class="first" <?php } ?> ><!-- .first for first row of the table (only if there is thead) -->
											
												<td class="tc">
												<a href="?f=thirdQuery&date=<?php echo$date; ?>&input_uid=<?php echo $_GET['refer']; ?>" ><?php echo $date;?></a>
												</td>
												<td><?php  echo $record['register'];?></td>
												<td><?php  echo $record['orderCount'];?></td>
												<td><?php  echo $record['orderSum'];?></td>
											</tr>
											<?php 
												}
												}
											?>
										</tbody>
									</table>
								</fieldset>
							</form>
							
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
						
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>
