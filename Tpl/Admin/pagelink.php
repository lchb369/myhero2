<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
	if( !empty($result['pageInfo']) )
	{
?>
<div id="page_links">
    
<?php
	for( $i=1;$i<=$result['pageInfo']['totalPage'];$i++ )
	{
		if( $i != $result['pageInfo']['pageNow'] )
		{
?>
            <a  href="?f=task&page=<?php echo $i;?>&pagesize=<?php echo $result['pageInfo']['pageSize'];?>"  class="page_others">
            <?php echo $i;?>
            </a>	
		<?php
		}
		else
		{
		?>
            <a class="page_now"><?php echo $i;?></a>
<?php
		}
	}
?> 
    <span class="page_more">
    &nbsp;
    共(<?php echo $result['pageInfo']['total'];?>)条，每页:
    </span>
    <a class="<?php if( $result['pageInfo']['pageSize'] == 20 )echo 'page_now'; else echo 'page_others';?>"  href="?f=task&page=<?php echo $result['pageInfo']['pageNow'];?>&pagesize=20">20</a>
	<a class="<?php if( $result['pageInfo']['pageSize'] == 50 )echo 'page_now'; else echo 'page_others';?>"  href="?f=task&page=<?php echo $result['pageInfo']['pageNow'];?>&pagesize=50">50</a>
    <a class="<?php if( $result['pageInfo']['pageSize'] == 100 )echo 'page_now'; else echo 'page_others';?>"  href="?f=task&page=<?php echo $result['pageInfo']['pageNow'];?>&pagesize=100">100</a>
</div>
<?php
	}
?>