<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>

<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container" style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>在线人数统计</h3>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td>统计日期</td>
											<?php for( $i = 0 ; $i < 24 ; $i++ )
											{
											?>
												<td><?php echo $i;?></td>
											<?php 
											}
											?>
											</tr>
										</thead>
										<tfoot><!-- table foot - what to do with selected items -->
											<tr>
												<td colspan="8"><!-- do not forget to set appropriate colspan if you will edit this table -->
													<label>
													查询时间:<?php  echo $useTime; ?>
													</label>
													
												</td>
											</tr>
										</tfoot>
										<tbody>
								
										<?php
											foreach ( $statsData as  $eachDay => $eachInfo )
											{
										?>
											<tr>
												<td class="tc"><?php  echo date( 'Y-m-d' , $eachDay );?></td>
												<?php for( $i = 0 ; $i < 24 ; $i++ )
												{
												?>
													<td class="tc"><?php echo isset( $eachInfo[$i] ) ? $eachInfo[$i] : 0;?></td>
												<?php 
												}
												?>
											</tr>
										
										<?php 
											
											}
										?>
										
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<?php 
										
										$time = $_GET['time'] ?  $_GET['time'] : $_SERVER['REQUEST_TIME'];
										
									?>
									
									<li><a href="?f=<?php echo $statsName; ?>&time=<?php  echo  $time-30*86400;?>">前个月</a></li>
									<li><a href="?f=<?php echo $statsName; ?>&time=<?php  echo  $time+30*86400;?>">后个月</a></li>
									
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
					
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>
