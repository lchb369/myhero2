<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>
<script>
function submitUserInfo()
{
	$("#userInfo").submit();
}


function deleteCard( cardId )
{
	if( confirm( "是否删除") == true )
	{
		
		var url = "?input_uid=<?php echo $userId;?>&do=delete&card="+cardId;
		window.location.href = url;
	}
}
</script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container" style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>活跃用户数统计</h3>
							
							<a class="button" href="#" onclick="path.click()"  >Android</a>
						    <a class="button" href="#" onclick="submitUpload()">IPhone</a>
							<a class="button" href="?mod=Config&act=buildConfig" >IPad</a>

							<ul>
								<li><a rel="box1-tabular" href="#" class="active">用户分析</a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
								<li><a rel="box1-grid" href="#">自助查询</a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
							</ul>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
											
												
												<td>统计日期</td>
												<td class="tc">日活跃用户数</td>
												<td>周活跃用户数</td>
												<td>月活跃用户数</td>
												<td class="tc">自然周活跃用户数</td>
                                                <td class="tc">自然月活跃用户数</td>
											</tr>
										</thead>
										<tfoot><!-- table foot - what to do with selected items -->
											<tr>
												<td colspan="8"><!-- do not forget to set appropriate colspan if you will edit this table -->
													<label>
													查询时间:<?php echo  $useTime;?>
													</label>
													
												</td>
											</tr>
										</tfoot>
										<tbody>
							
										<?php
									//	print_r( $monthStats  );
											foreach (  $monthStats as $day => $eachInfo )
											{
										?>
											<tr><!-- .first for first row of the table (only if there is thead) -->
												<td class="tc"><?php echo date("Y-m-" , $_SERVER['REQUEST_TIME'] ).$day?></td>
												<td class="tc"><?php  echo $eachInfo['activeDay']; ?></td>
												<td class="tc"><?php  echo $eachInfo['activeWeek']; ?></td>
												<td><?php  echo $eachInfo['activeMonth']; ?></td>
												<td>$</td>
												<td>$</td>
											</tr>
											<?php 
											}
											?>
											
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<li><a href="?f=order&input_uid=<?php echo $userId;?>&currPage=1&startTime=<?php echo $_GET['startTime'];?>&endTime=<?php echo $_GET['endTime']?>&byUid=<?php echo $_GET['byUid']?>">首页</a></li>
									<?php 
										if( $totalNum%20 == 0 )
										{
											$lastPage = intval( $totalNum/20 );
										}
										else
										{
											$lastPage = intval( $totalNum/20 ) + 1;
										}
									
										for( $i=1 ; $i<= $lastPage ; $i++ ){
										if( $i > 10 )
										{
											continue;
										}
									?>
									<li><a href="?f=order&input_uid=<?php echo $userId;?>&currPage=<?php echo $i;?>&startTime=<?php echo $_GET['startTime'];?>&endTime=<?php echo $_GET['endTime']?>&byUid=<?php echo $_GET['byUid']?>">
									<?php
									if( $i == $_GET['cardPage'] )
									{
										echo "<strong>$i</strong>";
									}
									else
									{
										echo $i;
									}
									?></a>
									
									</li>
									<?php }
										
									?>
									<li><a href="?f=order&input_uid=<?php echo $userId;?>&currPage=<?php echo $lastPage; ?>&startTime=<?php echo $_GET['startTime'];?>&endTime=<?php echo $_GET['endTime']?>&byUid=<?php echo $_GET['byUid']?>">末页</a></li>
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
						<div id="box1-grid" class="content card"><!-- content box 2 for tabs switching (hidden by default) -->
						<form class="basic" action="" method="post" ><!-- Default basic forms -->
							<div class="inner-form">
								<!-- error and information messages -->
								<div class="msg msg-ok"><p>I'm a <strong>success</strong> message and I'm proud of it!</p></div>
									<dl>
										<?php 
										
											$startTime = $_GET['startTime'];
											$startTime = $startTime > 0 ? $startTime : date( "Y-m-d H:i:s" , 0 );
										
											$endTime = $_GET['endTime'];
											$endTime = $endTime > 0 ? $endTime : date( "Y-m-d H:i:s" , $_SERVER['REQUEST_TIME'] );
										
										?>
										<dd>
											开始时间<input type="text" size="10" style="width:200px;margin-left:20px" name="startTime" class="txt" value="<?php echo $startTime;?>"/>
											结束时间<input type="text" size="10" style="width:200px;margin-left:20px" name="endTime" class="txt" value="<?php echo $endTime;?>"/>
										</dd>
									
										
										<dd>
											客户端类型<select name="os" >
											<option value="">all</option>
											<option  value="android">android</option>
											<option  value="iphone">iphone</option>
											<option value="ipad">ipad</option>
											<option  value="windows">windows</option>
											<option  value="other">other</option>
											</select>
										</dd>
										
										
											<dd>
											下载点<select name="refer">
											<option value="0">0</option>
											<option value="1">1</option>
											<option  value="2">2</option>
											<option  value="3">3</option>
											<option value="4">4</option>
											<option  value="5">5</option>
											<option  value="6">6</option>
											</select>
										</dd>
										
										
										<dd>
											<input type="hidden" name="input_uid"  value="<?php echo $userId;?>" />
											<input type="hidden" name="do"  value="query" />
											<input type="hidden" name="f"  value="statsLogin" />
											<input class="button" type="submit" value="查询" />
										</dd>
									</dl>
								</div>
							</form>
						<div>	
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>