<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');


?>
<script>
function delUser()
{
	do{
		if( confirm( "真的要删除?") == false ) break;

		if( confirm( "又不想删除啦?") == true ) break;

		if( confirm( "确认删除") == false ) break;

		$.ajax({
			   type:"GET",
			   url:"?f=user&do=delUser",
			   data:{
				   uid:<?php echo $userId;?>,
					rand:new Date().getTime()
				},
			   success:function(echo){if(echo!='')alert(echo);window.location.reload();},
			   error:function(){alert('操作失败 请重试');}
		});
						
	}while(0);
}

function submitUserInfo()
{
	$("#userInfo").submit();
}

function deleteCard( cardId )
{
	if( confirm( "是否删除") == true )
	{
		
		var url = "?input_uid=<?php echo $userId;?>&do=delete&card="+cardId;
		window.location.href = url;
	}
}


function editCard( cardId )
{
	if( confirm( "确认修改") == true )
	{
		var cardExp = $("#card_exp_"+cardId ).val();
		var sklv = $("#sklv_"+cardId ).val();
		$.ajax({
			   type:"GET",
			   url:"?f=user&do=updateCard",
			   data:{
				   uid:<?php echo $userId;?>,
				   cid:cardId,
				   cexp:cardExp,
				   sklv:sklv,
				},
			   success:function(echo){if(echo!='')alert(echo);window.location.reload();},
			   error:function(){alert('操作失败 请重试');}
		});
	}
}


</script>
		<div id="container">
			<div class="inner-container">
				
				<!-- Main content – all content is within boxes. Feel free to add your boxes (see the examples on the bottom of this document) and your content within -->
				<div id="altbox" class="box box-50 altbox"><!-- .altbox for alternative box's color -->
					<div class="boxin">
						<div class="header">
							<h3><?php echo MakeCommand_LangPack::get("role_info_title"); ?></h3>
							<?php if( $canwrite == 1 ){ ?>
							<a class="button" onclick="submitUserInfo()" href="#">write new&nbsp;»</a><!-- Action button in the header of the box -->
							<?php }?>
							<ul>
								<li><a rel="altbox-tabular" href="#" class="active"><?php echo MakeCommand_LangPack::get("role_basic_info"); ?></a></li>
								<li><a rel="altbox-grid" href="#"  ><?php echo MakeCommand_LangPack::get("role_more_info"); ?></a></li>
							</ul>
						</div>
						<div class="content" id="altbox-tabular">
							<table cellspacing="0">
								<form id="userInfo" method="get" >
								<thead>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_col_name"); ?></th>
										<td class="tc"><?php echo MakeCommand_LangPack::get("role_col_value"); ?></td>
										
									</tr>
								</thead>
								<tbody>
									<tr class="first"><!-- .first for first row of the table (only if there is thead) -->
										<th><?php echo MakeCommand_LangPack::get("role_exp"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text"  name="exp" value="<?php echo $userData['exp'];?>"></input></td><!-- a.ico-comms for comment-like backgrounds -->
										
									</tr>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_level"); ?></th>
										<td class="tc"><input readonly class="noboderTxt" type="text" name="level"  value="<?php echo $userData['level'];?>"></input></td>
										
									</tr>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_country"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text" name="country"  value="<?php echo $userData['country'];?>"></input></td>
										
									</tr>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_coin"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text"  name="coin" value="<?php echo $userData['coin'];?>"></input></td>
										
									</tr>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_gold"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text" name="gold" value="<?php echo $userData['gold'];?>"></input></td>
										
									</tr>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_leadership"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text" name="leaderShip" value="<?php echo $userData['leaderShip'];?>"></input></td>
									
									</tr>
									
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_welfare"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text"  name="benefit" value="<?php echo $arenaData['benefit'];?>"></input></td>
										
									</tr>
									
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_floor"); ?></th>
										<td class="tc">
											<input style="width:20px;" class="noboderTxt" type="text"  name="normal_floor"
											 value="<?php echo $battleNormalData['floor'];?>"></input>
										<?php echo MakeCommand_LangPack::get("role_room"); ?>
											<input style="width:20px;" class="noboderTxt" type="text"  name="normal_room"
											 value="<?php echo $battleNormalData['room'];?>"></input>
										<?php echo MakeCommand_LangPack::get("role_is_pass"); ?>
											<input style="width:20px;" class="noboderTxt" type="text"  name="normal_pass"
											 value="<?php echo $battleNormalData['pass'];?>"></input>
										</td>
									</tr>
									
									<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
									
								</tbody>

							
							</table>
						</div>
						
						<div class="content" id="altbox-grid">
							<table cellspacing="0">
								<form id="userInfo2" method="get" >
								<thead>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_col_name"); ?></th>
										<td class="tc"><?php echo MakeCommand_LangPack::get("role_col_value"); ?></td>
									
									</tr>
								</thead>
								<tbody>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_stamina"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text" name="stamina" value="<?php echo $userData['stamina'];?>"></input></td>
										
									</tr>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_gacha_point"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text" name="gachaPoint" value="<?php echo $userData['gachaPoint'];?>"></input></td>
										
									</tr>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_max_card"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text" name="maxCardNum" value="<?php echo $userData['maxCardNum'];?>"></input></td>
										
									</tr>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_max_friend"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text" name="maxFriendNum" value="<?php echo $userData['maxFriendNum'];?>"></input></td>
										
									</tr>
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_max_stamina"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text"  name="maxStamina" value="<?php echo $userData['maxStamina'];?>"></input></td>
										
									</tr>

									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_invite_code"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text" name="inviteCode" value="<?php echo $userData['inviteCode'];?>"></input></td>
										
									</tr>
									
									<tr>
										<th><?php echo MakeCommand_LangPack::get("role_card_team"); ?></th>
										<td class="tc"><input class="noboderTxt" type="text" name="cardTeam" value="<?php echo $cardTeam['cards'];?>"></input></td>
										
									</tr>
									
									<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
									
								</tbody>

							
							</table>
						</div>
					</div>
				</div>
				</form>
				
				<div class="box box-50"><!-- box 50% width -->
					<div class="boxin">
						<div class="header">
							<h3><?php echo MakeCommand_LangPack::get("user_data_title"); ?></h3>
							<?php if( $canwrite == 1 ){ ?> 
							<a style = "float:right;" class="button" onclick="delUser()" href="#"><?php echo MakeCommand_LangPack::get("user_del_btn"); ?></a>
							<?php }?>
						</div>
						<div class="content">
							<ul class="simple"><!-- ul.simple for simple listings of pages, categories, etc. -->
								<li>
									<strong><a href="#"><?php echo MakeCommand_LangPack::get("user_id"); ?></a></strong>
									<span><a href="#"><?php echo $userProfile['uid'];?></a></span>
								</li>
								<li>
									<strong><a href="#"><?php echo MakeCommand_LangPack::get("user_account"); ?></a></strong>
									<span><a href="#"><?php echo $userProfile['loginName'];?></a></span>
								</li>
								<li>
									<strong><a href="#"><?php echo MakeCommand_LangPack::get("user_nickname"); ?></a></strong>
									<span><a href="#"><?php echo $userProfile['nickName'];?></a></span>
								</li>
								<li>
									<strong><a href="#"><?php echo MakeCommand_LangPack::get("user_reg_date"); ?></a></strong>
									<span><a href="#"><?php echo date( "Y-m-d H:i:s" , $userProfile['registerTime'] );?></a></span>
								</li>
								<li>
									<strong><a href="#"><?php echo MakeCommand_LangPack::get("user_login_date"); ?></a></strong>
									<span><a href="#"><?php echo date( "Y-m-d H:i:s" , $userProfile['loginTime'] );?></a></span>
								</li>
								<li>
									<strong><a href="#"><?php echo MakeCommand_LangPack::get("user_login_times"); ?></a></strong>
									<span><a href="#"><?php echo $userProfile['loginTimes'];?></a></span>
								</li>
								<li>
									<strong><a href="#"><?php echo MakeCommand_LangPack::get("user_consecutive_days"); ?></a></strong>
									<span><a href="#"><?php echo $userProfile['loginDays'];?></a></span>
								</li>
								<li>
									<strong><a href="#"><?php echo MakeCommand_LangPack::get("user_platform"); ?></a></strong>
									<span><a href="#"><?php echo $userProfile['platform'];?></a></span>
								</li>
								
								<!-- start 第三方平台信息 -->
								<?php 
									if(!empty($userData['thirdId']) && Helper_Common::inPlatform(array('vn', 'test')))
									{
										foreach($userData['thirdId'] as $thirdCol => $thirdInfo)
										{
								?>
								<li>
									<strong><a href="#"><?php echo $thirdCol; ?></a></strong>
									<span><a href="#"><?php echo $thirdInfo;?></a></span>
								</li>
								<?php 
										}	
									} 
								?>
								<!-- end   第三方平台信息 -->
								
								<li>
									<strong><a href="#"><?php echo MakeCommand_LangPack::get("user_total_pay"); ?></a></strong>
									<span><a href="#"><?php echo $userProfile['totalRecharge'];?></a></span>
								</li>
								<li>
									<strong><a href="#"><?php echo MakeCommand_LangPack::get("user_total_buy_coin"); ?></a></strong>
									<span><a href="#"><?php echo $userProfile['totalRechargeCoin'];?></a></span>
								</li>
							</ul>
						</div>
					</div>
				</div>
				
				
				
				<div id="box1" class="box box-100"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3><?php echo MakeCommand_LangPack::get("hero_list"); ?></h3>
							<a class="button" href="?f=fileLog"><?php echo MakeCommand_LangPack::get("check_log"); ?>&nbsp;»</a>
							<?php if( $canwrite == 1 ){ ?>
							<ul>
								<li><a rel="box1-tabular" href="#" class="active"><?php echo MakeCommand_LangPack::get("list_view"); ?></a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
								<li><a rel="box1-grid" href="#"><?php echo MakeCommand_LangPack::get("add_hero"); ?></a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
							</ul>
							<?php }?>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td class="tc"><input type="checkbox" id="data-1-check-all" name="data-1-check-all" value="true" /></td>
												<td class="tc">ID</td>
												<td class="tc"><?php echo MakeCommand_LangPack::get("card_id"); ?></td>
												<td class="tc"><?php echo MakeCommand_LangPack::get("card_exp"); ?></td>
												<td class="tc"><?php echo MakeCommand_LangPack::get("card_level"); ?></td>
												<td class="tc"><?php echo MakeCommand_LangPack::get("card_skill_level"); ?></td>
												<td class="tc"><?php echo MakeCommand_LangPack::get("card_add_time"); ?></td>
												<?php if( $canwrite == 1 ){ ?>
												<td class="tc"><?php echo MakeCommand_LangPack::get("card_operation"); ?></td>
												<?php }?>
											</tr>
										</thead>
										<tfoot><!-- table foot - what to do with selected items -->
											<tr>
												<td colspan="8"><!-- do not forget to set appropriate colspan if you will edit this table -->
													<label>
														with selected do:
														<select name="data-1-groupaction">
															<option value="delete">delete</option>
															<option value="edit">edit</option>
														</select>
													</label>
													<input class="button altbutton" type="submit" value="OK" />
												</td>
											</tr>
										</tfoot>
										<tbody>
											<?php
												$count = count( $cardList );
												$pageNum = ceil( $count/5 );
												$currpage = $_GET['cardPage'] ?  $_GET['cardPage']  : 1 ;
												$index = 0;
												$start = 5*( $currpage - 1 ) + 1;
												$end = 5*$currpage;
												foreach ( $cardList as $key => $cardInfo )
												{
													$index += 1;
													if( $index < $start || $index > $end )
													{
														continue;
													}
											?>
											<tr <?php if($index == 1 ){?> class="first" <?php } ?> ><!-- .first for first row of the table (only if there is thead) -->
												<td class="tc"><input type="checkbox" id="data-1-check-1" name="data-1-check-1" value="true" /></td>
												<td class="tc"><?php echo $cardInfo['id'];?></td>
												<th><?php echo $cardInfo['cardId'];?></th>
												<td>
												<input type="text"  id="<?php echo "card_exp_".$cardInfo['id'];?>" class="noboderTxt"  name="cardExp" value="<?php echo  $cardInfo['exp'];?>" />
												</td>
												<td class="tc"><?php echo $cardInfo['level'];?></td>
												
												<td>
												<input type="text"  id="<?php echo "sklv_".$cardInfo['id'];?>" class="noboderTxt"  name="skillLevel" value="<?php echo  $cardInfo['skillLevel'];?>" />
										
												</td>
												
												<td class="tc"><?php echo date( "Y-m-d H:i:s" , $cardInfo['addTime'] );?></td>
													<?php if( $canwrite == 1 ){ ?>
												<td class="tc"><!-- action icons - feel free to add/modify your own - icons are located in "css/img/led-ico/*" -->
													<ul class="actions">
														<li><a class="ico" onclick="editCard(<?php echo $cardInfo['id'];?>)" href="#" title="edit"><img src="css/img/led-ico/pencil.png" alt="edit" /></a></li>
														<li><a class="ico" onclick="deleteCard(<?php echo $cardInfo['id'];?>)" href="#" title="delete"><img src="css/img/led-ico/delete.png" alt="delete" /></a></li>
													</ul>
												</td>
													<?php }?>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
								<ul>
									<li><a href="?input_uid=<?php echo $userId;?>&cardPage=1">previous</a></li>
									<?php for( $i=1 ; $i<= $pageNum ; $i++ ){?>
									<li><a href="?input_uid=<?php echo $userId;?>&cardPage=<?php echo $i;?>">
									<?php
									if( $i == $_GET['cardPage'] )
									{
										echo "<strong>$i</strong>";
									}
									else
									{
										echo $i;
									}
									?></a>
									
									</li>
									<?php }?>
									<li><a href="#">next</a></li>
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
						<div id="box1-grid" class="content card"><!-- content box 2 for tabs switching (hidden by default) -->
						<form class="basic" action="" method="get" ><!-- Default basic forms -->
							<div class="inner-form">
								<!-- error and information messages -->
								<div class="msg msg-ok"><p>I'm a <strong>success</strong> message and I'm proud of it!</p></div>
									<dl>
									
										<dd>
											<?php echo MakeCommand_LangPack::get("card_no"); ?><input type="text"  name="inputCard" />
											<?php echo MakeCommand_LangPack::get("quantity"); ?><input type="text"  name="inputNum" />
										</dd>
									
									
										<dd>
											<?php echo MakeCommand_LangPack::get("multi_select"); ?> <select id="some10" name="card[]"  multiple="multiple" size="20"  >
												<option value="val1">选择武将 …</option>
												<?php 
													$cardConfig = Common::getConfig( "card" );
													foreach ( $cardConfig as  $cardId => $cardCnf )
													{
													
												?>
													<option value="<?php echo $cardId; ?>"><?php echo $cardCnf['star']."星-".$cardCnf['name']."-".$cardId; ?></option>
												<?php 
													}
												?>
											</select>
										</dd>
								
										<dt></dt>
										<dd>
											<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
											<input type="hidden" name="do" value="add" />
											<input class="button" type="submit" value="<?php echo MakeCommand_LangPack::get("do_add_hero"); ?>" />
										</dd>
									</dl>
								</div>
							</form>
						<div>	
				</div>
				</div>
			
			
			<div class="box box-100" style="margin-top:20px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>帐号绑定</h3>
			
						</div>
						<div  class="content"><!-- content box 1 for tab switching -->
							<form class="basic" action="" method="get" ><!-- Default basic forms -->
							<div class="inner-form">
							<?php if( $bindMsg ){?>
								<div class="msg msg-ok"><p><?php echo $bindMsg; ?></p></div>
							<?php }?>
									<dl>
									
										<dd>
											新登录帐号字符串<input type="text"  name="lname" />
											老帐号数字ID<input type="text"  name="oldUid" />
											
											
											<input class="button" type="submit" value="帐号绑定" />
										</dd>
										<dd>
											<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
											<input type="hidden" name="do" value="bind" />
										</dd>
									</dl>
								</div>
							</form>
						
						</div><!-- .content#box-1-holder -->
						
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
		<div class="box box-100" style="margin-top:20px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>测试订单</h3>
			
						</div>
						<div  class="content"><!-- content box 1 for tab switching -->
							<form class="basic" action="" method="get" ><!-- Default basic forms -->
							<div class="inner-form">
									<dl>
									
										<dd>
											gid<input type="text"  name="gid" />
											
											<input class="button" type="submit" value="添加" />
										</dd>
										<dd>
											<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
											<input type="hidden" name="do" value="testOrder" />
										</dd>
									</dl>
								</div>
							</form>
						
						</div><!-- .content#box-1-holder -->
						
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>