<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

?>
<script>
</script>
<style>
td
{
	text-align:center;	
}
</style>
		<div id="container">
			<div class="inner-container" style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>每日注册登录统计</h3>
							&nbsp;&nbsp;&nbsp;
							<span style="color:red;">*新註冊用户數→創角數 , 活躍用戶數->当天登录过的用户数</span>
							<!-- 
							<a class="button" href="#" onclick="path.click()"  >Android</a>
						    <a class="button" href="#" onclick="submitUpload()">IPhone</a>
							<a class="button" href="?mod=Config&act=buildConfig" >IPad</a>
 							-->
							<ul>
								<li><a rel="box1-tabular" href="#" class="active">用户分析</a></li><!-- insert ID of content related to this tab into the rel attribute of this tab -->
							</ul>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
											
												
												<td>统计日期</td>
												
												<td>新注册用户数（UID）</td>
											
                                                <td class="tc">日活跃用户数（UID）</td>
											</tr>
										</thead>
										<tfoot><!-- table foot - what to do with selected items -->
											<tr>
												<td colspan="8"><!-- do not forget to set appropriate colspan if you will edit this table -->
													<label>
													查询时间:<?php  echo $useTime; ?>
													</label>
													
												</td>
											</tr>
										</tfoot>
										<tbody>
								
										<?php
											krsort(  $monthStats );
											$minTime = strtotime( "2013-04-25");
		
											$i = ( $_SERVER['REQUEST_TIME'] - 86400 - $minTime ) / 86400;
											foreach ( $monthStats as  $eachDay => $eachInfo )
											{
												if( strtotime( $eachDay ) < $minTime || $eachDay > date( "Y-m-d") )
												{
													continue;
												}
												$i--;	
										?>
											<tr <?php if($index == 1 ){?> class="first" <?php } ?> ><!-- .first for first row of the table (only if there is thead) -->
												<td class="tc"><?php  echo $eachDay ;?></td>
												<td class="tc"><?php echo $eachInfo['registerNum'] ;?></td>
												<td><?php
													if( $eachDay == "2013-04-25" )
													{
														 echo intval(  $eachInfo['loginTime'] );
													}
													else
													{	
														$preDayTime = strtotime( $eachDay ) - 86400;
														$preDay = date( "Y-m-d" , $preDayTime  );
														$eachInfo['loginTime'] =  $eachInfo['registerTime'] + ( 100 - $eachInfo['relogin1'] )/100 * $monthStats[$preDay]['registerTime']; 
														echo $eachInfo['loginNum'];
													}
												;?></td>											
											</tr>
										
										<?php 
											
											}
										?>
										
										</tbody>
									</table>
								</fieldset>
							</form>
							<div class="pagination"><!-- pagination underneath the box's content -->
									<?php 
										$time = $_GET['time'] ?  $_GET['time'] : $_SERVER['REQUEST_TIME'];
									?>
								<ul>
									<li><a href="?f=statsLogin&time=<?php  echo  $time-30*86400;?>">前个月</a></li>
									<li><a href="?f=statsLogin&time=<?php  echo  $time+30*86400;?>">后个月</a></li>
								</ul>
							</div>
						</div><!-- .content#box-1-holder -->
						<!-- code bellow is only example for switching between tabs, not regular content -->
						<!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>
