<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');
?>
<div class="pannel">
    <form method="GET" action="?">
    <b>用户ID:</b><input name="input_uid" type="text" value="<?php echo $_GET[ 'input_uid' ];?>"/>
    <input type="submit" value="查询"/>
    <input type="hidden" name="f" value="<?php echo $_GET[ 'f' ];?>"/>
    <input type="hidden" name="do" value="get"/>
    <textarea class="json"><?php echo json_encode( $fightInfo );?></textarea>
    </form>
</div>

<div class="pannel">
	<div class="float_left_div">
	<b>用户回合战斗信息</b>
	    <form method="GET" action="?">
	    <?php
	    if( !empty( $fightInfo ) )
	    {
	    ?>
	    <table class="list_table">
	    	<tr class="list_table_title">
	    		<td>用户ID</td>
	    		<td>目标Id</td>
	    		<td>回合次数</td>
	    		<td>血量</td>
	    		<td>攻击</td>
	    		<td>敏捷</td>
	    		<td>命中</td>
	    		<td>暴击</td>
	    		<td>闪避</td>
	    		<td>类型</td>
	    		<td>AI</td>
	    		<td>自动</td>
	    		<td>操作</td>
	    	</tr>
	    	<tr>
	    		<td><?php echo $userId;?></td>
	    		<td><input style="width:80px" type="text" name="fightObject" value="<?php echo $fightInfo[ 'fightObject' ];?>"/></td>
	    		<td><input style="width:35px" type="text" name="roundTimes" value="<?php echo $fightInfo[ 'roundTimes' ];?>"/></td>
	    		<td><input style="width:65px" type="text" name="hp" value="<?php echo $fightInfo[ 'hp' ];?>"/></td>
	    		<td><input style="width:65px" type="text" name="damage" value="<?php echo $fightInfo[ 'damage' ];?>"/></td>
	    		<td><input style="width:65px" type="text" name="agility" value="<?php echo $fightInfo['agility'];?>" /></td>
	    		<td><input style="width:65px" type="text" name="hitRate" value="<?php echo $fightInfo['hitRate'];?>" /></td>
	    		<td><input style="width:65px" type="text" name="criticalRate" value="<?php echo $fightInfo['criticalRate']; ?>" /></td>
	    		<td><input style="width:65px" type="text" name="dodgeRate" value="<?php echo $fightInfo['dodgeRate']; ?>" /></td>
	    		<td><input style="width:65px" type="text" name="objectType" value="<?php echo $fightInfo['objectType']; ?>" /></td>
	    		<td><input style="width:65px" type="text" name="fighterType" value="<?php echo $fightInfo['fighterType']; ?>" /></td>
	    		<td><input style="width:65px" type="text" name="autoFight" value="<?php echo $fightInfo['autoFight']; ?>" /></td>
	    		<td>
					<input style="width:75px" type="hidden" name="input_uid" value="<?php echo $userId;?>"/>
					<input style="width:75px" type="hidden" name="objectId" value="<?php echo $userId;?>"/>
				    <input style="width:75px" type="hidden" name="f" value="<?php echo $_GET[ 'f' ];?>"/>
				    <input style="width:75px" type="hidden" name="action" value="delete"/>
					<input type="submit" value="删除缓存" class="btn"/>
				</td>
	    	</tr>
	    </table>
	    <?php
	    }
	    ?>
	    </form>
	</div>
</div>


<div class="pannel">
	<div class="float_left_div">
	<b>对手回合战斗信息</b>
	    <form method="GET" action="?">
	    <?php
	    if( !empty( $underAttackerInfo ) )
	    {
	    ?>
	    <table class="list_table">
	    	<tr class="list_table_title">
	    		<td>用户ID</td>
	    		<td>目标Id</td>
	    		<td>回合次数</td>
	    		<td>血量</td>
	    		<td>攻击</td>
	    		<td>敏捷</td>
	    		<td>命中</td>
	    		<td>暴击</td>
	    		<td>闪避</td>
	    		<td>类型</td>
	    		<td>AI</td>
	    		<td>自动</td>
	    		<td>操作</td>
	    	</tr>
	    	<tr>
	    		<td><?php echo $objectId;?></td>
	    		<td><input style="width:80px" type="text" name="fightObject" value="<?php echo $underAttackerInfo[ 'fightObject' ];?>"/></td>
	    		<td><input style="width:35px" type="text" name="roundTimes" value="<?php echo $underAttackerInfo[ 'roundTimes' ];?>"/></td>
	    		<td><input style="width:65px" type="text" name="hp" value="<?php echo $underAttackerInfo[ 'hp' ];?>"/></td>
	    		<td><input style="width:65px" type="text" name="damage" value="<?php echo $underAttackerInfo[ 'damage' ];?>"/></td>
	    		<td><input style="width:65px" type="text" name="agility" value="<?php echo $underAttackerInfo['agility'];?>" /></td>
	    		<td><input style="width:65px" type="text" name="hitRate" value="<?php echo $underAttackerInfo['hitRate'];?>" /></td>
	    		<td><input style="width:65px" type="text" name="criticalRate" value="<?php echo $underAttackerInfo['criticalRate']; ?>" /></td>
	    		<td><input style="width:65px" type="text" name="dodgeRate" value="<?php echo $underAttackerInfo['dodgeRate']; ?>" /></td>
	    		<td><input style="width:65px" type="text" name="objectType" value="<?php echo $underAttackerInfo['objectType']; ?>" /></td>
	    		<td><input style="width:65px" type="text" name="fighterType" value="<?php echo $underAttackerInfo['fighterType']; ?>" /></td>
	    		<td><input style="width:65px" type="text" name="autoFight" value="<?php echo $underAttackerInfo['autoFight']; ?>" /></td>
	    		<td>
					<input style="width:75px" type="hidden" name="input_uid" value="<?php echo $userId;?>"/>
					<input style="width:75px" type="hidden" name="objectId" value="<?php echo $objectId;?>"/>
				    <input style="width:75px" type="hidden" name="f" value="<?php echo $_GET[ 'f' ];?>"/>
				    <input style="width:75px" type="hidden" name="action" value="delete"/>
					<input type="submit" value="删除缓存" class="btn"/>
				</td>
	    	</tr>
	    </table>
	    <?php
	    }
	    ?>
	    </form>
	</div>
</div>

<div class="pannel">
	<div class="float_left_div">
	<b>其他信息</b>
	    <form method="GET" action="?">
	    <table class="list_table">
	    	<tr class="list_table_title">
	    		<td>重发2.0版本礼包</td>
	    		<td>重发2.2版本礼包</td>
	    	</tr>
	    	<tr>
	    		<td>
					<input style="width:75px" type="hidden" name="input_uid" value="<?php echo $userId;?>"/>
				    <input style="width:75px" type="hidden" name="f" value="<?php echo $_GET[ 'f' ];?>"/>
				    <input style="width:75px" type="hidden" name="action" value="resendGift"/>
					<input type="submit" value="重发竞技场礼包" class="btn"/>
				</td>
				<td>
					<input style="width:75px" type="hidden" name="input_uid" value="<?php echo $userId;?>"/>
				    <input style="width:75px" type="hidden" name="f" value="<?php echo $_GET[ 'f' ];?>"/>
				    <input style="width:75px" type="hidden" name="action" value="resendJewel"/>
					<input type="submit" value="重发宝石大礼包" class="btn"/>
				</td>
	    	</tr>
	    </table>
	    </form>
	</div>
</div>

<?php
include('footer.php');
?>