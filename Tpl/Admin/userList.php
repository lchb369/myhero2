<?php
if( !defined( 'IN_INU' ) )
{
    return;
}
include('header.php');
include('menu.php');

function getOrderQueryParam( $page )
{
	global $userId;
	$url = "&input_uid=".$userId."&currPage=".$page."&startTime=".$_GET['startTime']."&endTime=".$_GET['endTime']."&searchType=".$_GET['searchType'];
	return $url;
}
?>
<script>
function formSubmit()
{
	$("#do").val( "query" );
	$("#formMain").submit();
}
$(function(){
	$("#searchType").val("<?php echo $_GET['searchType']; ?>");
});
</script>

		<div id="container">
			<div class="inner-container"  style="width: 1100px">
				<div id="box1" class="box box-100" style="width: 1100px"><!-- box full-width -->
					<div class="boxin">
						<div class="header">
							<h3>用户列表查询</h3>
						</div>
						<div id="box1-tabular" class="content"><!-- content box 1 for tab switching -->
							<form id="formMain" class="basic" action="" method="get" ><!-- Default basic forms -->
								<div class="inner-form">
									<dl>
										<dd>
											查找类型
											<select id="searchType" name="searchType" >
												<option value="level">等级</option>
												<option value="registerTime">注册时间</option>
												<option value="loginTime">最后登陆时间</option>
												<option value="coin">元宝</option>
												<option value="gold">金币</option>
												<option value="cardId">拥有武将</option>
												<option value="benefit">福利点</option>
											</select>
											<br/>
											<font color="red">若类型为时间，条件格式应为 2013-10-01；
											<br/>查询拥有武将的用户时在开始条件中填入武将卡片id，如 23_card </font>
										</dd>
										
										<dd>
											开始条件<input type="text" size="10" style="width:200px;margin-left:20px" id="startTime" name="startTime" class="txt" value="<?php echo $_GET['startTime'];?>"/>
											结束条件<input type="text" size="10" style="width:200px;margin-left:20px" id="endTime" name="endTime" class="txt" value="<?php echo $_GET['endTime'];?>"/>
										</dd>
										
										<dd>
											<input type="hidden" name="input_uid" value="<?php echo $userId;?>" />
											<input type="hidden" name="do" id="do" value="query" />
											<input type="hidden" name="f" value="<?php echo $statsName;?>" />
											<!-- <input class="button" type="submit" value="查询" /> -->
											<input class="button" type="button" value="查询" onclick="formSubmit();"/>
											<font color="red">数据有一小时缓存</font>
										</dd>
									</dl>
								</div>
							</form>
							<form class="plain" action="" method="post" enctype="multipart/form-data">
								<fieldset>
									<table cellspacing="0">
										<thead><!-- universal table heading -->
											<tr>
												<td class="tc">序号</td>
												<td class="tc">uid</td>
												<td class="tc">昵称</td>
												<td class="tc">等级</td>
												<td class="tc">注册时间</td>
												<td class="tc">最后登陆时间</td>
												<td class="tc">元宝</td>
												<td class="tc">金币</td>
												<td class="tc">福利点</td>
											</tr>
										</thead>
										<tbody>
											<?php
												$startNo = ($currPage-1)*$pageSize;
												foreach ( $statsData as $data )
												{
													$_uid = $data[0];
													if(User_Model::exist($_uid))
													{
														$userProfile = User_Profile::getInstance($_uid)->getData();
														$userInfo = User_Info::getInstance($_uid)->getData();
														$arenaInfo = Data_Arena_Normal::getNormalInfo($_uid);
													}
											?>
											<tr>
												<td class="tc"><?php echo $startNo + (++$i);?></td>
												<td class="tc"><?php echo $_uid;?></td>
												<td class="tc"><?php echo $userProfile['nickName'];?></td>
												<td class="tc"><?php echo $userInfo['level'];?></td>
												<td class="tc"><?php echo date('Y-m-d H:i:s', $userProfile['registerTime']);?></td>
												<td class="tc"><?php echo date('Y-m-d H:i:s', $userProfile['loginTime']);?></td>
												<td class="tc"><?php echo $userInfo['coin'];?></td>
												<td class="tc"><?php echo $userInfo['gold'];?></td>
												<td class="tc"><?php echo $arenaInfo['benefit'];?></td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</fieldset>
							</form>
							<?php include('pagination.php'); ?>										
						</div><!-- .content#box-1-holder -->
				</div>
			
			<div id="footer"><!-- footer, maybe you don't need it -->
				<p>© You! 2009, <a href="#">some link</a></p>
			</div>
			
			</div><!-- .inner-container -->
		</div><!-- #container -->
		
    </body>
<?php
include('footer.php');
?>