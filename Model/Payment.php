<?php
/**
 * QQ平台交易查询
 * @author askldfhjg
 *
 */
class Payment
{
	private function _getParameters($baseOptions)
	{
		$baseOptions['ts'] = time();

		//校验码
		$appkey = $baseOptions['appkey'];
		ksort( $baseOptions );
		foreach ( $baseOptions as $k=>$v )
		{
			$str[] = "{$k}{$v}";
		}
		$str = implode( "" , $str ) . $appkey;
		$sign =  md5( $str );

		$baseOptions['userip'] = $_SERVER['REMOTE_ADDR'];
		$baseOptions['sig'] = $sign;
		$baseOptions['device'] = 0;
		return $baseOptions;
	}
	
	/**
	 * 请求腾讯接口
	 **/
	public function getBalance()
	{
		//初始化接口数据
		$paymentConfig = & Common::getConfig( "TencentQB" );
		$baseUrl = $paymentConfig['baseUrl'];
		$getBalanceDir = $paymentConfig['getBalanceDir'];
		//$_GET['openid'] = '0000000000000000000000000EB6DB05';
		// $_GET['openkey'] = '723AC1FD8009FD9EF8EF54E79A30E443C875EF17DB958BC918ABE22A6C86525619722B7878E0B6D335CDC317308D3AE213283199FFC3138130256C677E29E9CB';
		$baseOptions = array (
			"appid" => $paymentConfig['appid'],
			"appkey" => $paymentConfig['appkey'],
			"appname"=> $paymentConfig['appname'],
			"openid" => $_GET['openid'],
			"openkey"=> $_GET['openkey'],
		);
		$queryString = http_build_query( $this->_getParameters ( $baseOptions ) );
		if( function_exists( 'curl_init' ) )
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $baseUrl . $getBalanceDir . "?" . $queryString );
			print_r( $baseUrl . $getBalanceDir . "?" . $queryString );
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
			$ret = json_decode( $ch , true );
			if( is_array( $ret ) )
			{
				$ret["ret"] = true;
				return $ret;
			}
			else return array( "ret" => false , "balance" => 0 );
		}
	}
}
?>