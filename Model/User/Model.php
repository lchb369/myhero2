<?php

/**
 * 重构User Model
 * 实现用户注册和删除功能
 * 
 * @author	Liuchangbing
 * @since 2013-1-15
 */
if( !defined( 'IN_INU' ) )
{
    return;
}
class User_Model
{
	public static $maxNewbieStep = 11;
	public static $CACHE_KEY_TID2UID = 'user_profile_tid2uid_';
	
	public static $ECN_LOGIN_SERVER = 'http://user.ecngame.com/api.php';
	public static $ECN_LOGIN_APPID = '1002';
	
	public static function deleteUser( $userId )
	{
		$userId = intval($userId);
		if(empty($userId)) return false;
		if(!User_Model::exist($userId)) return false;
		
		//删除第三方平台注册的缓存
		$userProfile = User_Profile::getInstance($userId)->getData();
		User_Model::delCacheThirdId($userProfile['thirdId']);
		
		//删除缓存（请开发者自觉按照表名的字母排序修改这段代码）
		//从数据库中删除（请开发者自觉按照表名的字母排序修改这段代码）
		$tableConfig = array(
			array("table"=>"activity",				"cache"=>"activity_model"),
			array("table"=>"activity_consume",		"cache"=>"activity_consume"),
			array("table"=>"arena_normal",			"cache"=>"arena_normal"),
			array("table"=>"battle_normal",			"cache"=>"battle_normal"),
			array("table"=>"battle_special",		"cache"=>"battle_special"),
			array("table"=>"battle_weekly",			"cache"=>"battle_weekly"),
			array("table"=>"card",					"cache"=>"card_model"),
			array("table"=>"card_team",				"cache"=>"card_team"),
			array("table"=>"card_unlock",			"cache"=>"card_unlock"),
			array("table"=>"code_invite_outline",	"cache"=>"code_invite_outline"),
			array("table"=>"friend",				"cache"=>"friend_model"),
			array("table"=>"friend_request",		"cache"=>"friend_request"),
			array("table"=>"invite_code",			"cache"=>"invite_code"),
			array("table"=>"mail",					"cache"=>"mail"),
			//需要判断是否有重复验证id购买, 不能删除order表
			//array("table"=>"order",					"cache"=>"order_model"),
			array("table"=>"user_help",				"cache"=>"user_help"),
			array("table"=>"user_info",				"cache"=>"user_info"),
			array("table"=>"user_profile",			"cache"=>"user_profile"),
		);
		
		$cache = Common::getCache();
		$db = Common::getDB( $userId );
		
		foreach( $tableConfig as $config )
		{
			$cache->delete( $userId."_".$config['cache'] );
			$db->query( array("delete from `".$config['table']."` where uid=".$userId."") );
		}
		
		return true;
			
	}

	/**
	 * 校验session
	 * @param unknown $userId
	 * @param unknown $session
	 * @return boolean
	 */
	public static function checkSession( $userId  , $session )
	{
		$cache = & Common::getCache();
		$cacheSession = $cache->get( "session_".$userId  );
		if( $cacheSession !=  $session )
		{
			return false;
		}
		return true;
	}	

	/**
	 * 获取session
	 * @param unknown $userId
	 * @return string
	 */
	public static function getSession( $userId  )
        {
	        $sessoinValue = md5( $userId.$_SERVER['REQUEST_TIME'] );
                $cache = & Common::getCache();
                $cache->set( "session_".$userId ,  $sessoinValue );
      		return $sessoinValue;	
        }       	
	/**
	 * 是否存在这个用户
	 * @param int $userId
	 */
	public static function exist( $userId )
	{
		$cache = Common::getCache();
		if( $cache == false )
		{
			return false;
		}
		$userInfoKey = $userId."_user_info";
		$data = $cache->get( $userInfoKey );
		if( !$data )
		{
			$db = Common::getDB( $userId );
			$data = $db->find( 'user_info' );
		}
		return $data ? true : false;
	}
	
	/**
	 * 根据第三方平台id获得游戏id 
	 * @param unknown $tId
	 */
	public static function getIdByThird($tId)
	{
		$ret = false;
		
		do
		{
			$cache = Common::getCache();
			$key = User_Model::$CACHE_KEY_TID2UID.$tId;
			if(!!$data = $cache->get($key))
			{
				$ret = $data;
				break;
			}
			
			$dbEngine = Common::getDB( 1 );
			$sql = "SELECT uid FROM user_profile WHERE thirdId='".$tId."' limit 1";
			$users = $dbEngine->findQuery($sql);
			if(!empty($users[0][0]))
			{
				$ret = $users[0][0];
				$cache->set($key, $ret);
				break;
			}
			
		}while(0);
		
		return $ret;
	}
	
	/**
	 * 删除第三方平台注册的缓存
	 * @param unknown $tId
	 * @return boolean
	 */
	public static function delCacheThirdId($tId)
	{
		$cache = Common::getCache();
		$key = User_Model::$CACHE_KEY_TID2UID.$tId;
		return $cache->delete($key);
	}
	
	public static function setSig(  $userId , $sig )
	{
		$cache = & Common::getCache();
		$userSigKey = $userId."_user_checksig";
		$cache->set( $userSigKey ,  $sig );
	}
	/**
	 * 校检SIG
	 * @param int $userId
	 * @param string $sig
	 */
	public static function checkSig( $userId , $sig )
	{
		$cache = & Common::getCache();
		if( $cache == false )
		{
			return false;
		}
		$userSigKey = $userId."_user_checksig";
		$data = $cache->get( $userSigKey );
		
		if( $data != $sig )
		{
			//$cache->set( $userSigKey ,  $sig );
			return false;
		}
		return true;
	}
	
	/**
	 * 心跳包
	 * Enter description here ...
	 * @param unknown_type $userId
	 */
	public static function heartbeat( $userId )
	{
		if( !!$lock = Helper_Common::addLock( "online" ) )
		{
			$cache = Common::getCache();
			$online = $cache->get( "online" );
			
			$online[$userId] = array(
				't' => $_SERVER['REQUEST_TIME'],
				'pf' => Common::getConfig('platform'),
			);
			
			$cache->set( "online" , $online , 300 );
			
			Helper_Common::delLock( "online" );
		}
		
	}
	
}
