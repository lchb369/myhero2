<?php
/**
 * 重构User Model
 * 实现用户基本属性，经验相关的所有功能
 * 
 * @name Ability.php
 * @author Lucky
 * @modifier Roy
 * @since 2011-3-9
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class User_Ability extends Logical_Abstract
{
	/**
	 * 基本属性点类型（HP）
	 * @var	int
	 */
	const POINT_TYPE_STRONG = 1;
	
	/**
	 * 基本属性点类型（攻击力）
	 * @var	int
	 */
	const POINT_TYPE_BRAWN = 2;
	
	/**
	 * 基本属性点类型（敏捷度）
	 * @var	int
	 */
	const POINT_TYPE_DEXTERITY = 3;
	
	/**
	 * @var	int
	 */
	const POINT_TYPE_MP = 4;
	
	/**
	 * 单例对象
	 * @var	User_Model[]
	 */
	protected static $singletonObjects;
	
	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	protected function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	User_Ability
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		
		return self::$singletonObjects[$userId];
	}
 
	/**
	 * 更改可使用技能点GM工具专用
	 * @return	int
	 */
	public function setCanUseAttributePoint( $point )
    {
    	return $this->getUserAbilityWithWriteLock()->setCanUseAttributePoint( $point );
    }
	/**
	 * 获取可加点数
	 * @return	int
	 */
    public function getCanUseAttributePoint()
    {
    	return $this->getUserAbility()->getCanUseAttributePoint();
    }
	/**
	 * 加点
	 * @param	int $pointType	能力值类型
	 * @param	int $point		点数
	 * @throws 	User_Exception::STATUS_NOT_ENOUGH_ATTRIBUTE_POINT
	 * @return	boolean
	 */
	public function addPoint( $pointType , $point )
	{
		if( $point > $this->getUserAbility()->getCanUseAttributePoint() )
		{
			throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_ATTRIBUTE_POINT );
		}
		
		switch ( $pointType )
		{
			case self::POINT_TYPE_STRONG :
				$this->getUserAbilityWithWriteLock()->changeStrong( $point );
				break;
			case self::POINT_TYPE_BRAWN :
				$this->getUserAbilityWithWriteLock()->changeBrawn( $point );
				break;
			case self::POINT_TYPE_DEXTERITY :
				$this->getUserAbilityWithWriteLock()->changeDexterity( $point );
				break;
		}
		$this->getUserAbilityWithWriteLock()->changeCanUseAttributePoint( -$point );
		
		$ship = Ship_Model::getInstance( $this->userId );
		$ship->activeToCompute3Size();
		return true;
	}
	
	/**
	 * 洗点（把所有已分配的基本属性点归零，并让用户可以重新分配）
	 * @return	boolean
	 */
	public function clearPoint()
	{
		$this->getUserAbilityWithWriteLock()->setCanUseAttributePoint(
			$this->getUserAbility()->getStrong()
			+ $this->getUserAbility()->getBrawn()
			+ $this->getUserAbility()->getDexterity()
			+ $this->getUserAbility()->getCanUseAttributePoint()
		);
		$this->getUserAbilityWithWriteLock()->setStrong( 0 );
		$this->getUserAbilityWithWriteLock()->setBrawn( 0 );
		$this->getUserAbilityWithWriteLock()->setDexterity( 0 );
		$ship = Ship_Model::getInstance( $this->userId );
		$ship->activeToCompute3Size();
		
		return true;
	}
	
	/**
	 * 加经验
	 * @param	int $exp	需要增加的经验
	 * @param	int $isAddtionExp	是否附加经验（附加经验不会是用户马上升级）
	 * @return	boolean
	 */
	public function addExp( $exp , $isAddtionExp = false )
	{
		$exp = ceil( $exp );
		if( $exp <= 0 )
		{
			return false;
		}
		if( $isAddtionExp )
		{
			$this->getUserAbilityWithWriteLock()->changeAdditionExp( $exp );
			return true;
		}
		$this->getUserAbilityWithWriteLock()->changeExp( $exp );
		
		if( $this->getUserAbility()->getAdditionExp() > 0 )
		{
			$this->getUserAbilityWithWriteLock()->changeExp( $this->getUserAbility()->getAdditionExp() );
			$this->getUserAbilityWithWriteLock()->changeAdditionExp( -$this->getUserAbility()->getAdditionExp() );
		}
		$configLevel = self::getConfig( 'LevelConfig' );
		$level = $this->getUserAbility()->getLevel();
		//计算升级
		while( isset( $configLevel[$this->getUserAbility()->getLevel()]['exp'] ) && $configLevel[$this->getUserAbility()->getLevel()]['exp'] <= $this->getUserAbility()->getExp() )
		{
			//奖励道具
			if( !empty( $configLevel[$this->getUserAbility()->getLevel()]['awardList']['items'] ) )
			{
				foreach( $configLevel[$this->getUserAbility()->getLevel()]['awardList']['items'] as $item )
				{
					if( Bag_Model::getItemType( $item['itemId'] ) != Bag_Model::ITEM_TYPE_EQUIPMENT )
					{
						Bag_Model::getInstance( $this->userId )->changeItem( $item['itemId'] , $item['number'] );
					}
					else 
					{
						Bag_Model::getInstance( $this->userId )->addEquipment( $item['itemId'] , 0 , 0 );
					}
				}
			}
			//奖励基本属性点
			$this->getUserAbilityWithWriteLock()->changeCanUseAttributePoint( $configLevel[$this->getUserAbility()->getLevel()]['awardList']['canUseAttributePoint'] );
			
			//增加金币
			$this->changeGold( $configLevel[$this->getUserAbility()->getLevel()]['awardList']['gold'] );
			
			//vip每次升级增加额外金币
			$platform = Common::getConfig( 'platform' );
			if( $platform == 'qzone' )
			{
				$vipInfo = FM_FriendServer::getInstance( $this->userId )->getVipInfo( $this->userId );
				if( isset( $vipInfo ) && $vipInfo['is_vip'] == 1 )
				{
					$this->changeGold( $configLevel[$this->getUserAbility()->getLevel()]['vipAward']['gold'] );
				}
			}
			//增加原始能力点
			$shipModel = Ship_Model::getInstance( $this->userId );
			//HP
			if( $configLevel[$this->getUserAbility()->getLevel()]['awardList']['awardBasePoint']['hp'] > 0 )
			{
				$shipModel->changeBasePoint( self::POINT_TYPE_STRONG , $configLevel[$this->getUserAbility()->getLevel()]['awardList']['awardBasePoint']['hp'] );
			}
			//攻击力
			if( $configLevel[$this->getUserAbility()->getLevel()]['awardList']['awardBasePoint']['damage'] > 0 )
			{
				$shipModel->changeBasePoint( self::POINT_TYPE_BRAWN , $configLevel[$this->getUserAbility()->getLevel()]['awardList']['awardBasePoint']['damage'] );
			}
			//敏捷度
			if( $configLevel[$this->getUserAbility()->getLevel()]['awardList']['awardBasePoint']['hitRate'] > 0 )
			{
				$shipModel->changeBasePoint( self::POINT_TYPE_DEXTERITY , $configLevel[$this->getUserAbility()->getLevel()]['awardList']['awardBasePoint']['hitRate'] );
			}
			//回复MP
			$mpLimit = Ship_Model::getInstance( $this->userId )->getMPLimit();
			$attributes = Buff_Model::getInstance( $this->userId )->getBuffedAbility( array( 'mpLimit' => (integer)$mpLimit ) );
			if( isset( $attributes['mpLimit'] ) )
			{
				//调整上限
				$mpLimit = $attributes['mpLimit'];
			}
			
			$currentMP = Ship_Model::getInstance( $this->userId )->getMP();
			if( $currentMP < $mpLimit )
			{
				Ship_Model::getInstance( $this->userId )->changeMP( $mpLimit - $currentMP );
			}
			
			//增加等级数值
			$this->getUserAbilityWithWriteLock()->changeLevel( 1 );
			//更新任务
			Task_User::getInstance( $this->userId )->actionIncrement( Task_Abstract::ACTION_TYPE_COLLECT_ITEMS , Task_Abstract::HAVING_OBJECT_TYPE_LEVEL , 1 );
			Task_User::getInstance( $this->userId )->actionIncrement( Task_Abstract::ACTION_TYPE_LEVEL_UP ,  0 , 1 );
			//更新成就
			Task_User::getInstance( $this->userId )->actionIncrement( 1000 ,  0 , 1 );
			
			//通知升级奖励
			Award::add( 'levelUp' , $this->getUserAbility()->getLevel() );
			
			//如果用户已经到达可以打劫的等级，则打劫用户更新
			$etcConfig = self::getConfig( 'etc' );
			if( $this->getUserAbility()->getLevel() >= $etcConfig['inArenaLevel'] )
			{
				FM_LootLogic::getInstance()->updateUser( $this->userId  ,  $this->getUserAbility()->getLevel() , Arena_Model::getInstance( $this->userId )->getNewArenaScore() );
			}
			
			//通知统计系统
			$acc = FM_Accumulator::getInstance( $this->userId );
			$acc->user( 1012 , 1 , 1 );//升级
			$acc->setUserData( FM_Accumulator::USER_DATA_TYPE_LEVEL , $this->getUserAbility()->getLevel() );
			$acc->action( 1012 ,$this->getUserAbility()->getLevel() , 0 , 0 );//升级
		}
		
		
		$tackleConfig = Common::getConfig( 'TackleConfig' );
		//如果升到15级，初始化大作战
		if(  $this->getUserAbility()->getLevel() >=  $tackleConfig['tackleUnlockLevel']  )
		{
			//采集铂金玩法
			$tackle = Tackle_Model::getInstance(  $this->userId );
			//设置昨天排行
			$tackle->setHistoryInfo();
			Award::add( 'tackleInfo' , MakeCommand_Tackle::tackleInfo( $this->userId )  );
		}
		
		//如果已经是最高级了
		//顶级经验封顶
		if( !isset( $configLevel[$this->getUserAbility()->getLevel()]['exp'] ) && $configLevel[$this->getUserAbility()->getLevel() - 1]['exp'] <= $this->getUserAbility()->getExp() )
		{
			$this->getUserAbilityWithWriteLock()->changeExp( $configLevel[$this->getUserAbility()->getLevel() - 1]['exp'] - $this->getUserAbility()->getExp() );
		}
		
		$this->_updateArenaRank( $level );		
	
		//统计用户数据：经验
		FM_Accumulator::getInstance( $this->userId )->setUserData( FM_Accumulator::USER_DATA_TYPE_EXP , $this->getUserAbility()->getExp() );
		return true;
	}

	
	private function _updateArenaRank( $level )
	{
		$newLevel = $this->getUserAbility()->getLevel();
		$rankLevelConfig = Common::getConfig( 'RankLevelConfig' );
		$oldListId = $newListId = 0;	
		foreach (  $rankLevelConfig['rankList'] as $listId => $listInfo  )
		{
			if( $level >= $listInfo['lowerLimit'] &&  $level <=  $listInfo['upperLimit'] )
			{
				 $oldListId = $listId;
			}
		
			if( $newLevel >= $listInfo['lowerLimit'] &&  $newLevel <=  $listInfo['upperLimit'] )
                        {
                                 $newListId = $listId;
                        }       	
		}
		if( $newListId != $oldListId  )
		{
			Arena_RankFight::changeListId( $this->userId , $oldListId , $newListId );
		}
	}
	
	/**
	 * 获取金币
	 * @return	int
	 */
	public function getGold()
	{
		return $this->getUserAbility()->getGold();
	}
	
	/**
	 * 计算打好友所得的金币
	 * @param	mediumint $fpopularity	好友的悬赏等级
	 * @param	int $fgold	好友的金币
	 * @return	int $gold   应得的金币
	 */
	public function caculateGold( $friendPopularityLevel , $friendGold , $attackFriendTimes )
	{
		$awardConfig = self::getConfig( 'AttackFriendAwardConfig' );
		$gold = $awardConfig['goldAward'][$friendPopularityLevel];
		if( !isset( $awardConfig['attackFriendAwardRate'][$attackFriendTimes] ) )
		{
			return 0;
		}
		$gold = ceil( $gold * $awardConfig['attackFriendAwardRate'][$attackFriendTimes] );
		if( $gold >= $friendGold )
		{
			$gold = $friendGold;
		}
		return $gold;
	}
	
	/**
	 * 改变金币
	 * @param	int $gold	金币
	 * @throws	User_Exception::STATUS_NOT_ENOUGH_MONEY
	 * @return	boolean
	 */
	public function changeGold( $gold )
	{
		$gold = (int)$gold;
		if( $this->getUserAbility()->getGold() + $gold < 0 )
		{
			throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_MONEY );
		}
		
		$this->getUserAbilityWithWriteLock()->changeGold( $gold );
		
		//统计游戏里玩家金币的总收入和支出
		FM_Accumulator::getInstance( $this->userId )->action( ( ( $gold > 0 ) ? 1040 : 1041 ) , 0 , 0 , 0 , abs( $gold ) );
		
		return true;
	}
	
	/**
	 * 获取等级
	 * @return	int
	 */
	public function getLevel()
	{
		return $this->getUserAbility()->getLevel();
	}

	/**
	 * 获取基本属性点
	 * @return	int
	 */
	public function getAttributePoint( $pointType )
	{
		switch( $pointType )
		{
			case self::POINT_TYPE_STRONG:
				return $this->getUserAbility()->getStrong();
				break;
			
			case self::POINT_TYPE_BRAWN:
				return $this->getUserAbility()->getBrawn();
				break;
				
			case self::POINT_TYPE_DEXTERITY:
				return $this->getUserAbility()->getDexterity();
				break;
				
		}
		return 0;
	}

	/**
	 * 获取用户当前经验值
	 * @return	int
	 */
	public function getExp()
	{
		return $this->getUserAbility()->getExp();
	}
	/**
	 * 获取数据
	 */
	public function getData()
	{
		return $this->getUserAbility()->getData();
	}
}
