<?php
/**
 * 用户帐户信息
 * 
 * @name Profile.php
 * @author Liuchangbing
 * @since 2013-1-14
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class User_Profile extends Logical_Abstract
{
	/**
	 * 标识是新手
	 */	
	const STATUS_IS_NEW_PLAYER = 1;
	/**
	 * 单例对象
	 * @var	User_Model[]
	 */
	protected static $singletonObjects;

	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	User_Profile
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}

	/**
	 * 是否新用户
	 */
	public function isNewUser()
	{
		return Data_User_Profile::getInstance( $this->userId )->isNewUser();
	}
	
	
	/**
	 * 设置昵称,并检查有效性
	 * @param unknown $nickName
	 */
	public function setNickName( $nickName )
	{
		if( !$nickName  )
		{
			return false;
		}
		
		//$platform = Common::getConfig( "platform" );
		//台湾平台不判断
		//if( strpos( $platform , "wk" ) !== 0 )
		//{
			$dbEngine = Common::getDB( $this->userId );
			$sql = "select * from user_profile where nickName='{$nickName}'";
			$record = $dbEngine->findQuery( $sql );
			if( count( $record ) > 0 )
			{
				return false;
			}
		//}
		
		Data_User_Profile::getInstance( $this->userId , true )->setUserInfo( array( 'nickName' => $nickName ) );
		return true;
	}
	
	public function setThirdId($thirdId)
	{
		Data_User_Profile::getInstance($this->userId, true)->setThirdId($thirdId);
	}
	
	
	/**
	 * 设置用户信息
	 * @param unknown_type $userInfo
	 */
	public function setUserInfo( $userInfo )
	{
		Data_User_Profile::getInstance( $this->userId , true )->setUserInfo( $userInfo );
	}
	

	public function getData()
	{
		return Data_User_Profile::getInstance( $this->userId )->getData();
	}
}