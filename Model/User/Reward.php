<?php
/**
 * 重构User Model
 * 实现上线奖励功能
 * 
 * @name Reward.php
 * @author Lucky
 * @modifier Roy
 * @since 2011-3-9
 */
if( !defined( 'IN_INU' ) )
{
    return;
}
class User_Reward extends Logical_Abstract
{
	/**
	 * 奖励物品：金币
	 */
	 const AWARD_TYPE_GOLD = 1;
	 
	 /**
	  * 奖励物品：水手
	  */
	 const AWARD_TYPE_SAILOR = 2;
	 
	 /**
	  * 奖励物品：悬赏值
	  */
	 const AWARD_TYPE_POPULARITY = 3;
	 
	 /**
	  * 奖励物品：精力
	  */
	 const AWARD_TYPE_MP = 4;
	 
	 /**
	 * 奖励类型：VIP每日奖励
	 */
	const STATUS_VIP_DAY_AWARD = 1;
	 
	 /**
	 * 奖励类型：VIP年费每日奖励
	 */
	const STATUS_VIP_YEAR_DAY_AWARD = 2;
	 
	/**
	 * 单例对象
	 * @var	User_Model[]
	 */
	protected static $singletonObjects;
	
	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	protected function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	User_Reward
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}


	/**
     * 上线奖励可领取数量
     * @param int $serialMaxDay 最大能连续几天
     * @return boolean,返回是连续上线了几天
     */
    public function rewardGetReceiveNum( $serialMaxDay )
    {
        //初始化数据
        $reward = $this->getUseReward()->getReward();
        if( $reward == null )
        {
        	$this->getUseRewardWithWriteLock()->setReward();
            $this->getUseRewardWithWriteLock()->setLoginTime();
        }
        
        //取得今天零点的时间戳
        $todayZeroTimestamp = strtotime( date( 'Y-m-d', $_SERVER['REQUEST_TIME'] ) );
        
	
        //记录今天己登陆并初始化相关数据
        $time = $this->getUseReward()->getDayLoginTime( $todayZeroTimestamp );
       if( $time == null )
        {
            $loginDays = $this->getUseRewardWithWriteLock()->getLoginDays();
            $yesterdayZeroTimestamp = $todayZeroTimestamp - 86400;
           $loginDay = $this->getUseReward()->getDayLoginTime( $yesterdayZeroTimestamp );
            
            if( $loginDays == null || $loginDay == null )
            {
            	$this->getUseRewardWithWriteLock()->setReward();
            	$this->getUseRewardWithWriteLock()->setLoginDays( 1 );
            }
            else 
            {
            	$this->getUseRewardWithWriteLock()->setLoginDays( 1 + $this->getUseReward()->getLoginDays() );
            }
            
            $this->getUseRewardWithWriteLock()->initReward( $todayZeroTimestamp );
            $this->getUseRewardWithWriteLock()->setCanReceive( $todayZeroTimestamp , true );
			$this->getUseRewardWithWriteLock()->setDayLoginTimes( 0 );
        }

        //按指定长度裁删登陆记录, 队列方式(天为单位)
        if( count( $this->getUseReward()->getLoginTime() ) > $serialMaxDay )
        {
            $loginTimeArrayOffset = 0;
            $loginTimeArrayOffset -= $serialMaxDay;
            $loginTime = array_slice( $this->getUseReward()->getLoginTime() , $loginTimeArrayOffset, null , true );
            $this->getUseRewardWithWriteLock()->setLoginTime( $loginTime );
        }
        
        //计算今天可领取次数
        $loginTimeArr = $this->getUseReward()->getLoginTime();
        $mayReceiveNum = 1;
        $loginTimeArrKeys = array_keys( $loginTimeArr );      
        for( $i = count( $loginTimeArr ) - 1; $i > 0; $i-- )
        {
            if( ($loginTimeArr[ $loginTimeArrKeys[ $i ] ]['todayZeroTimestamp'] - $loginTimeArr[ $loginTimeArrKeys[ $i - 1 ] ]['todayZeroTimestamp']) == 86400 )
            {
                $mayReceiveNum++;
                if( $mayReceiveNum == $serialMaxDay )
                {
                    break;
                }
                continue;
            }
            break;
        }
        return $mayReceiveNum;
    }
    /**
     * 返回玩家当前可领取的奖品
     * @return array
     */
    public function rewardGetItems()
    {
     	$todayZeroTimestamp = strtotime( date( 'Y-m-d', $_SERVER['REQUEST_TIME'] ) );
        $items = $this->getUseReward()->getDayAward( $todayZeroTimestamp );
        return $items;
    }
    /**
     * 今天是否还可以领取
     */
    public function rewardCanReceive()
    {
        //取得今天零点的时间戳
        $todayZeroTimestamp = strtotime( date( 'Y-m-d', $_SERVER['REQUEST_TIME'] ) );
        return  $this->getUseReward()->getStatus( $todayZeroTimestamp );
    }
    /**
     * 领取今天的奖励
     */
    public function rewardReceive()
    {
        //取得今天零点的时间戳
        $todayZeroTimestamp = strtotime( date( 'Y-m-d', $_SERVER['REQUEST_TIME'] ) );
        $this->getUseRewardWithWriteLock()->setCanReceive( $todayZeroTimestamp , false );
    }
     /**
     * 设置玩家当前可领取的奖品
     * @param $items
     */
    public function rewardSetItems( $items )
    {
        $todayZeroTimestamp = strtotime( date( 'Y-m-d', $_SERVER['REQUEST_TIME'] ) );
        $this->getUseRewardWithWriteLock()->setDayAward( $todayZeroTimestamp , $items );
    }
    
    /**
     * 获取连续登录天数
     * @return	int
     */
    public function getRewardLoginDays()
    {
    	return (integer)$this->getUseReward()->getLoginDays();
    }
    
    /**
	 * 返回全部奖励商品
	 * @param int $num 
	 */
	public function getReward( $num )
	{
	    //当前级别奖励的物品列表
	    $levelItems = $this->_getLevelItems();
		
	    //验证数据完整性
	    if( !is_array( $levelItems ) || count( $levelItems ) <= 0 )
	    {
	        return false;
	    }
	    if( !is_int( $num ) || $num <= 0 )
	    {
	        return false;
	    }
	    
	    //对指定的列表随机选择部分物品
	    $rewardItems = $this->_getRandomItems( $levelItems, $num );	    
	    return $rewardItems;
	}
	
	/**
	 * 是否隔天登陆
	 * @return boolean
	 */
	public function isDiscontinuousLogin()
	{
		$yesterdayTimetstamp = Helper_Date::computeYestodayTime( $_SERVER['REQUEST_TIME'] );
		$theDayBeforeYesterdayTimestamp = Helper_Date::computeYestodayTime( $yesterdayTimetstamp['startTime'] );
		$theDayBeforeYesterdayStartTime = $this->getUseReward()->getLoginStartTime( $theDayBeforeYesterdayTimestamp['startTime'] );
		$yesterdayStartTime = $this->getUseReward()->getLoginStartTime( $yesterdayTimetstamp['startTime'] );
		return $theDayBeforeYesterdayStartTime != null && $yesterdayStartTime == null;
	}
	
	/**
	 * 统计每日登陆次数
	 * @return void
	 */
	public function increaseLoginTimes()
	{
        $this->getUseRewardWithWriteLock()->changeDayLoginTimes( 1 );
		FM_Accumulator::getInstance( $this->userId )->action( 1005 , $this->getUseReward()->getDayLoginTimes() , 0 , 0 );
	}

	/**
	 * 随机选择物品
	 * @param array $items 
	 * @param int $num
	 * @return 	array(
	 *			   	array( 'mp' 	=> int ),
     *			   	array( 'buffId'	=> int ),
     *			   	array( {itemId}	=> int ),
     *		    )
	 */
	protected function _getRandomItems( $items , $num )
	{
	    shuffle( $items );
	    $descItems = array_slice( $items , 0 , $num , true );
	    return $descItems;
	}
	
	/**
	 * 返回指定等级的奖励物品列表
	 * @param int $num
	 * @return array
	 */
	protected function _getLevelItems()
	{
	    $itemAll = self::getConfig( 'RewardConfig' );
	    $levelItems = $itemAll[ User_Ability::getInstance( $this->userId )->getLevel() ];
	    if( !is_array( $levelItems ) )
	    {
	        return false;
	    }
	    return $levelItems;
	}
	
	/**
	 * 获取最后一次获取黄金钻日奖励的时间
	 */
	public function getLastVipDayAwardTime()
	{
		return $this->getUseReward()->getLastVipDayAwardTime();
	}
	
	/**
	 * 设置最后一次获得黄金钻日奖励的时间
	 */
	public function setLastVipDayAwardTime()
	{
		$this->getUseRewardWithWriteLock()->setLastVipDayAwardTime();
	}
	
	/**
	 * 获取奖励
	 */
	public function addAward( $type ,  $isYear , $level = 0 )
	{
		$vipAwardConfig = Common::getConfig( 'VipAwardConfig' );
		if( $type == User_Reward::STATUS_VIP_DAY_AWARD )
		{
			if( isset( $vipAwardConfig['dayAward'][$level] ) )
			{
				$awardList = $vipAwardConfig['dayAward'][$level];
			}
			else 
			{
				return;
			}
		}
		
		if( $type == User_Reward::STATUS_VIP_YEAR_DAY_AWARD )
		{
			$awardList = $vipAwardConfig['yearAward'];
		}
		
		if( isset( $awardList[User_Reward::AWARD_TYPE_GOLD] ) )
		{
			User_Ability::getInstance( $this->userId )->changeGold( $awardList[User_Reward::AWARD_TYPE_GOLD] );
		}
		
		if( isset( $awardList[User_Reward::AWARD_TYPE_SAILOR] ) )
		{
			Ship_Model::getInstance( $this->userId )->changeSailor( $awardList[User_Reward::AWARD_TYPE_SAILOR] ); 
		}
		
		if( isset( $awardList[User_Reward::AWARD_TYPE_POPULARITY] ) )
		{
			User_Profile::getInstance( $this->userId )->changePopularity( $awardList[User_Reward::AWARD_TYPE_POPULARITY] );
		}
		
		if( isset( $awardList[User_Reward::AWARD_TYPE_MP] ) )
		{
			Ship_Model::getInstance( $this->userId )->changeMP( $awardList[User_Reward::AWARD_TYPE_MP] );
		}
		
		if( isset( $awardList['itemList'] ) )
		{
			$bagModel = Bag_Model::getInstance( $this->userId );
			foreach( $awardList['itemList'] as $itemId => $num )
			{
				$bagModel->changeItem( $itemId , $num );
			}
		}
	}
	
	/**
	 * 设置是否领取黄钻每日奖励
	 */
	public function setIsReceiveDayVipAward( $type )
	{
		$this->getUseRewardWithWriteLock()->setIsReceiveVipDayAward( $type );
	}
	
	/**
	 * 运营活动：参与抽奖
	 */
	public function participateInActivity()
	{
		$activityConfig = self::getConfig( 'MarsActivityConfig' );
		$nowTime = $_SERVER['REQUEST_TIME'];
		if( $nowTime < $activityConfig['startTime'] || $nowTime > $activityConfig['endTime'])
		{
			throw new Arena_Exception( Arena_Exception::STATUS_NOT_IN_ACTIVE_TIME );
		}
		$list = $this->getUseReward()->getChooseAward();
		if( isset( $list ) && !empty( $list ) )
		{
			return $list;
		}
		
		Bag_Model::getInstance( $this->userId )->changeItem( $activityConfig['permitId'] , -$activityConfig['consumePermitNum'] );
		$dropItem = Ship_AttackFactory::computeEachDropItems( $activityConfig['awardList'] );
		foreach ( $dropItem as $cardId => $info )
		{
			$this->getUseRewardWithWriteLock()->setChooseAward( $cardId + 1 , $info['itemId'] , $info['itemNum'] );
		}
		$this->getUseRewardWithWriteLock()->setDeleteCardsNum( 0 );
		return $this->getUseReward()->getChooseAward();
	}
	
	/**
	 * 运营活动：踢牌
	 */
	public function deleteCard( $cardId )
	{
		$awardList = $this->getUseReward()->getChooseAward();
		if( empty( $awardList ) )
		{
			throw new User_Exception( User_Exception::STATUS_NOT_PARTICIPATE_IN );
		}
		
		if( !isset( $awardList[$cardId] ) )
		{
			throw new User_Exception( User_Exception::STATUS_NOT_HAVE_THE_CARD );
		} 
		
		if( isset( $awardList[$cardId]['isClear'] ) )
		{
			if( !empty( $awardList[$cardId]['isClear'] ) )
			{
				throw new User_Exception( User_Exception::STATUS_HAVE_REJECT_THE_CARD );
			}
		}
		
		$activityConfig = self::getConfig( 'MarsActivityConfig' );
		if( $this->getUseReward()->getDeleteCardsNum() >= $activityConfig['maxRejectNum'] )
		{
			throw new User_Exception( User_Exception::STATUS_OVER_THE_MAX_REJECT_NUMBER );
		}
		//扣除F币
		$toDeductFb = $activityConfig['rejectNeedFb'];
		$accFb = $toDeductFb;
		$toDeductFb = User_Profile::getInstance( $this->userId )->tryPayByVirtualFB( $toDeductFb );
		
		if( $toDeductFb > 0 )
		{
			//判断F币是否足够
			$payClient = FM_Pay::init();
			try
			{
				$payInfo = $payClient->getUser( $this->userId );
			}
			catch( Exception $ex )
			{
				$payInfo = array(
					'creditsBalance' => 0 ,
				);
			}
			if( $payInfo['creditsBalance'] < $toDeductFb )
			{
				throw new Shop_Exception( Shop_Exception::STATUS_NOT_ENOUGH_FB );
			}
			
			try
			{
				$result = $payClient->buy( $this->userId , $toDeductFb , 300013 );
			}
			catch( Exception $ex )
			{
				throw new Shop_Exception( Shop_Exception::STATUS_CONSUME_FB_FAIL );
			}
		}
		
		$acc = FM_Accumulator::getInstance( $this->userId );
		
		if( $accFb > 0 )
		{
			if( $toDeductFb == 0 )
			{
				//按等级统计消费FB次数
				$acc->action( 205 , User_Ability::getInstance( $this->userId )->getLevel() , 0 , 1 );
				//按等级统计消费FB数量
				$acc->action( 206 , User_Ability::getInstance( $this->userId )->getLevel() , 0 , 1 , $accFb );
			}
			elseif( $accFb > $toDeductFb && $toDeductFb != 0 )
			{
				//按等级统计消费FB次数
				$acc->action( 205 , User_Ability::getInstance( $this->userId )->getLevel() , 0 , 0 );
				$acc->action( 205 , User_Ability::getInstance( $this->userId )->getLevel() , 0 , 1 );
				//按等级统计消费FB数量
				$acc->action( 206 , User_Ability::getInstance( $this->userId )->getLevel() , 0 , 0 , $toDeductFb );
				$acc->action( 206 , User_Ability::getInstance( $this->userId )->getLevel() , 0 , 1 , $accFb - $toDeductFb );
			}
			elseif( $accFb == $toDeductFb )
			{
				//按等级统计消费FB次数
				$acc->action( 205 , User_Ability::getInstance( $this->userId )->getLevel() , 0 , 0 );
				//按等级统计消费FB数量
				$acc->action( 206 , User_Ability::getInstance( $this->userId )->getLevel() , 0 , 0 , $toDeductFb );
			}
			$acc->user( 205 , $toDeductFb , 1 );
			$acc->user( 220 , $accFb - $toDeductFb , 1 );
		}
		//统计剔牌次数
		$acc->action( 3114 , 1 , 0 , 0 );	
		$this->getUseRewardWithWriteLock()->changeDeleteCardsNum( 1 );
		return $this->getUseRewardWithWriteLock()->rejectCards( $cardId );
	}
	
	/**
	 * 运营活动：抽奖
	 */
	public function getCardAward()
	{
		$awardList = $this->getUseReward()->getChooseAward();
		if( empty( $awardList ) )
		{
			throw new User_Exception( User_Exception::STATUS_NOT_PARTICIPATE_IN );
		}
		
		$chooseList = array();
		foreach( $awardList as $cardId => $info )
		{
			if( !$info['isClear'] )
			{
				$chooseList[] = $cardId;
			}
		}
		shuffle( $chooseList );
		$items = array();
		$items[] = $chooseList[0];
		$itemId = $awardList[$chooseList[0]]['itemId'];
		if( Bag_Model::getItemType( $itemId ) == Bag_Model::ITEM_TYPE_EQUIPMENT )
		{
			Bag_Model::getInstance( $this->userId )->addEquipment( $itemId );
		}
		else
			Bag_Model::getInstance( $this->userId )->changeItem( $itemId , $awardList[$chooseList[0]]['itemNum'] );
		$this->getUseRewardWithWriteLock()->deleteAwards();
		
		//统计抽奖的次数和人数
		$acc = FM_Accumulator::getInstance( $this->userId );
		$acc->action( 3112 , 1 , 0 , 0 );
		$acc->user( 3113 , 1 , 1 );
		return $items;
	}
	
	/**
	 * 运营活动：获取抽奖信息
	 */
	public function getChooseAward()
	{
		return $this->getUseReward()->getChooseAward();
	}
	
	/**
	 * 获取计时宝箱奖励
	 */
	public function getTimingGiftBoxAward()
	{
		$giftBoxConfig = self::getConfig( 'TimingGiftBoxConfig' );
		$giftBoxInfo = $this->getUseReward()->getTimingAward();
		if( !empty( $giftBoxInfo ) )
		{
			if( $giftBoxInfo['level'] < $giftBoxConfig['maxLevel'] )
			{
				return $giftBoxInfo;
			}
			elseif( $giftBoxInfo['finishTime'] > 0 )
			{
				return $giftBoxInfo;
			}
			else 
			{
				//领取了最高级别的奖励
				return array();
			}
		}
		return $this->getUseRewardWithWriteLock()->addTimingGift( 1 , $giftBoxConfig[1]['finishTime'] );
	}
	
	/**
	 * 收获计时奖励
	 */
	public function gainTimingGift()
	{
		$giftBoxConfig = self::getConfig( 'TimingGiftBoxConfig' );
		$giftBoxInfo = $this->getUseReward()->getTimingAward();
		if( $giftBoxInfo['level'] == $giftBoxConfig['maxLevel'] && $giftBoxInfo['finishTime'] == 0 )
		{
			throw new User_Exception( User_Exception::STATUS_ACCEPT_ALL_TIMING_GIFT );
		}
		if( $giftBoxInfo['finishTime'] > $_SERVER['REQUEST_TIME'] )
		{
			throw new User_Exception( User_Exception::STATUS_CAN_NOT_RECEIVE_TIMING_GIFT );
		}
		$this->_awardTimingGift( $giftBoxInfo['level'] );
		
		//统计领取的宝箱
		FM_Accumulator::getInstance( $this->userId )->action( 3130 , $giftBoxInfo['level'] , 0 , 0 );
		$this->_accumulateNewUser( $giftBoxConfig['maxLevel'] );
		if( $giftBoxInfo['level'] < $giftBoxConfig['maxLevel'] )
		{
			return $this->getUseRewardWithWriteLock()->addTimingGift( $giftBoxInfo['level'] + 1 , $giftBoxConfig[$giftBoxInfo['level']+1]['finishTime'] );
		}
		else
		{
			$this->getUseRewardWithWriteLock()->remarkReceivedAllGift();
			return array();
		}
	}
	
	/**
	 * 计时宝箱：新用户相关统计
	 */
	private function _accumulateNewUser( $maxLevel )
	{
		if( !User_Profile::getInstance( $this->userId )->getIsNewPlayer() )
		{
			return;
		}
		$timingGiftInfo = $this->getUseReward()->getTimingAward();
		$nowDate = date( "Ymd" , $_SERVER['REQUEST_TIME'] );
		$finishDate = date( "Ymd" , $timingGiftInfo['finishTime'] );
		$lastDate = date( "Ymd" , $timingGiftInfo['lastAcceptTime'] );
		$accumulate = FM_Accumulator::getInstance( $this->userId );
		//领取的是最低一级的奖励
		if( $timingGiftInfo['lastAcceptTime'] == 0 )
		{
			if( $nowDate == $finishDate )
			{
				$accumulate->user( 3131 , 1 , 1 );
			}
		}
		else 
		{
			if( $nowDate == $finishDate )
			{
				if( $lastDate == $nowDate )
				{
					$key = 3130 + $timingGiftInfo['level'];
					$accumulate->user( $key , 1 , 1 );
				}
				elseif( $timingGiftInfo['level'] == $maxLevel ) 
				{
					$accumulate->user( 3138 , 1 , 1 );
				}
			}
			elseif( $lastDate == $finishDate ) 
			{
				if( $nowDate == ( $finishDate + 1 ) && $timingGiftInfo['level'] == $maxLevel )
				{
					$accumulate->user( 3138 , 1 , 1 );
				}
			}
		}
	}
	
	/**
	 * 领取计时宝箱奖励
	 */
	private function _awardTimingGift( $giftLevel )
	{
		$giftBoxConfig = self::getConfig( 'TimingGiftBoxConfig' );
		$awardList = $giftBoxConfig[$giftLevel]['awardList'];
		if( isset( $awardList['gold'] ) )
		{
			User_Ability::getInstance( $this->userId )->changeGold( $awardList['gold'] );
		}
		if( isset( $awardList['sailor'] ) )
		{
			Ship_Model::getInstance( $this->userId )->changeSailor( $awardList['sailor'] );
		}
		if( isset( $awardList['popularity'] ) )
		{
			User_Profile::getInstance( $this->userId )->changePopularity( $awardList['popularity'] );
		}
		if( isset( $awardList['itemList'] ) )
		{
			$bagModel = Bag_Model::getInstance( $this->userId );
			foreach( $awardList['itemList'] as $itemId => $num )
			{
				if( Bag_Model::getItemType( $itemId ) != Bag_Model::ITEM_TYPE_EQUIPMENT )
				{
					$bagModel->changeItem( $itemId , $num );
				}
				else 
				{
					for( $i = 0 ; $i < $num ; $i++ )
					{
						$bagModel->addEquipment( $itemId );
					}
				}
			}
		}
	}
	
	/**
	 * 判断是否可以发stream
	 */
	private function _canSendStream( $streamId )
	{
		$config = self::getConfig( 'PublishAwardConfig' );
		$publishConfig = $config['streamLIst'];
		if( !isset( $publishConfig[$streamId] ) )
		{
			return false;
		}
		
		if( empty( $publishConfig[$streamId]['awardList'] ) )
		{
			return false;
		}
		$lastPublishTime = $this->getUseReward()->getLastPublishTime( $streamId );
		$this->getUseRewardWithWriteLock()->setLastPublishTime( $streamId );
		$now = $_SERVER['REQUEST_TIME'];
		if( isset( $publishConfig[$streamId]['startTime'] ) && isset( $publishConfig[$streamId]['endTime'] ) )
		{
			if( $now < $publishConfig[$streamId]['startTime'] || $now > $publishConfig[$streamId]['endTime'] )
			{
				return false;
			}
		}
		
		if( empty( $publishConfig[$streamId]['conditionList'] ) )
		{
			return true;
		}
		if( isset( $publishConfig[$streamId]['conditionList']['level'] ) )
		{
			$level = User_Ability::getInstance( $this->userId )->getLevel();
			if( $level < $publishConfig[$streamId]['conditionList']['level']['lowerLimit'] || $level > $publishConfig[$streamId]['conditionList']['level']['upperLimit'] )
			{
				return false;
			}
		}
		
		if( isset( $publishConfig[$streamId]['conditionList']['dayLogin'] ) )
		{
			//取得今天零点的时间戳
        	$todayZeroTimestamp = Helper_Date::computeTodayTime( $now );
        	if( $lastPublishTime >= $todayZeroTimestamp['startTime'] )
        	{
        		throw new User_Exception( User_Exception::STATUS_GET_AWARD_TODAY );
        	}
		}
		return true;
	}
	
	/**
	 * 获取发送stream奖励
	 */
	public function getStreamAward( $streamId )
	{
		if( !$this->_canSendStream( $streamId ) )
		{
			return;
		}
		$config = self::getConfig( 'PublishAwardConfig' );
		$publishConfig = $config['streamLIst'];
		//buff
		if( isset( $publishConfig[$streamId]['awardList']['buffId'] ) )
		{
			Buff_Model::getInstance( $this->userId )->setBuff( $publishConfig[$streamId]['awardList']['buffId'] );
		}
			
		//背包的物品
		if( isset( $publishConfig[$streamId]['awardList']['itemList'] ) )
		{
			$bagModel = Bag_Model::getInstance( $this->userId );
			foreach( $publishConfig[$streamId]['awardList']['itemList'] as $itemId => $number )
			{
				if( Bag_Model::getItemType( $itemId ) == Bag_Model::ITEM_TYPE_EQUIPMENT )
				{
					for( $i = 0 ; $i < $number ; $i++ )
					{
						$bagModel->addEquipment( $itemId , 0 , 0 );
					}
				}
				else
				{
					$bagModel->changeItem( $itemId , $number );
				}
			}
		}

		//精力
		if( isset( $publishConfig[$streamId]['awardList']['mp'] ) )
		{
			Ship_Model::getInstance( $this->userId )->changeMP( $publishConfig[$streamId]['awardList']['mp'] );
		}

		//金币
		if( isset( $publishConfig[$streamId]['awardList']['gold'] ) )
		{
			User_Ability::getInstance( $this->userId )->changeGold( $publishConfig[$streamId]['awardList']['gold'] );
		}

		//经验
		if( isset( $publishConfig[$streamId]['awardList']['exp'] ) )
		{
			User_Ability::getInstance( $this->userId )->addExp( $publishConfig[$streamId]['awardList']['exp'] );
		}
			
		//水手
		if( isset( $publishConfig[$streamId]['awardList']['sailor'] ) )
		{
			Ship_Model::getInstance( $this->userId )->changeSailor( $publishConfig[$streamId]['awardList']['sailor'] );
		}
	}
	
	/**
	 * 获取数据
	 */
	public function getData()
	{
		return $this->getUseReward()->getData();
	}
	
	public function getLoginDays()
	{
		return $this->getUseReward()->getLoginDays();
	}
}