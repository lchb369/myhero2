<?php
/**
 * 用户游戏信息
 * 
 * @name Info.php
 * @author Liuchangbing
 * @since 2013-1-14
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class User_Info extends Logical_Abstract
{
	const MAX_FRIEND_NUM = 100;
	const MAX_CARD_NUM = 300;
	const MAX_LEVEL = 1000;
	
	/**
	 * 单例对象
	 * @var	User_Info[]
	 */
	protected static $singletonObjects;

	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	User_Info
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	

	/**
	 * 初始化新用户
	 * @param unknown $useInviteCode
	 */
	public function initNewUser( $useInviteCode )
	{
		$rewardGold = $this->useInviteCode( $useInviteCode );
		
		return array(
			'inviteGold' => $rewardGold,
		);
	}
	
	/**
	 * 初始化每次登录
	 */
	public function initPerLogin()
	{
		//检查我邀请的好友都升到多少级了,偿试问他们索取奖励
		$rewardTimes = 0;
		$reward = $this->_getInviteAward( $rewardTimes );	
		
		//检查用户信息完整性
		$this->checkData();
		
		//每次登录日志
		Stats_Model::addUserLoginLog( $this->userId , $_SERVER['REQUEST_TIME'] );
		
		return array( 'inviteReward' => $reward , 'rewardTimes' => $rewardTimes );
		
	}
	
	public function checkData()
	{
		$db = Common::getDB( $this->userId );
		$sql = "select * from user_profile where uid = {$this->userId}";
		$record = $db->findQuery(  $sql  );
		if( !$record[0][0] )
		{
			Data_User_Profile::getInstance( $this->userId , true )->recoveryData();
		}
		
		$sql = "select * from user_info where uid = {$this->userId}";
		$record = $db->findQuery(  $sql  );
		if( !$record[0][0] )
		{
			Data_User_Info::getInstance( $this->userId , true )->recoveryData();
		}
	}
	
	
	/**
	 * 初始化每天首次登录
	 */
	public function initFirstLogin()
	{
		$loginRecord = array();
		//今天0点
		$todaySec = strtotime('today');
		//昨天0点
		$yesTodaySec = $todaySec-86400;
		
		$userProfile = Data_User_Profile::getInstance( $this->userId )->getData();
		
		if( date( "Ymd" , $userProfile['loginTime'] ) != date( "Ymd" , $_SERVER['REQUEST_TIME'] ) )
		{
			
			//发放之前,帮助好友的援军点数
			$loginRecord['helpeReward'] = $this->_sendHelpReward();
			
			
			//清除历史友军助阵记录
			Data_Friend_Model::getInstance( $this->userId  ,true )->clearHelped();
			
			if( $userProfile['loginTime'] >= $yesTodaySec && $userProfile['loginTime'] < $todaySec )
			{
				$userProfile['loginDays'] += 1;
			}
			else
			{
				$userProfile['loginDays'] = 1;
			}
			Data_User_Profile::getInstance( $this->userId , true )->setLoginDays( $userProfile['loginDays'] );
			Data_User_Profile::getInstance( $this->userId , true )->addTotalLogins(); 

			//重置每日挑战次数
			Data_Arena_Normal::getInstance( $this->userId , true )->resetDailyTimes();
			//每日登陆活动
			Activity_Model::getInstance( $this->userId )->firstLogin();
			
			/**
			 * 运营数据统计:注册登录
			 */
			Stats_Model::addUserLogin( 
				$this->userId , 
				$userProfile['registerTime'], 
				$_SERVER['REQUEST_TIME'], 
				$userProfile['loginDays'],
				$_REQUEST['os'],	//客户端系统
				0
			);
		
		}
		
		Data_User_Profile::getInstance( $this->userId , true )->updateLoginTime();
		return $loginRecord;
	}
	
	/**
	 * 发送邀请奖励
	 */
	private function _sendHelpReward()
	{
		$helpInfo = Data_User_Help::getInstance( $this->userId , true )->getData();
		$addPoint = $helpInfo['friendPoint'] + $helpInfo['otherPoint'];
		Data_User_Info::getInstance( $this->userId , true )->changeGachaPoint( $addPoint );
		Data_User_Help::getInstance( $this->userId , true )->clear();
		return $helpInfo;
	}
	
	/**
	 * 获取邀请奖励
	 */
	private function _getInviteAward( & $awardTimes )
	{
	
		$outlineInvited = Data_InviteCode_User::getInstance( $this->userId )->getData();
		$inviteReward = array();
		
		$awardTimes = 0;
		if( $outlineInvited )
		{
			foreach ( $outlineInvited as $uid => $info )
			{
				if( $info['award1'] > 0 )
				{
					$awardTimes += 1;
					continue;
				}
				
				$level = User_Info::getInstance( $uid )->getLevel();
				
				//如果我的好友等级到达15级，我能得到5元宝哇，不过最多只能得6次
				if( $level >= 15 && $awardTimes < 6 )
				{
					User_Info::getInstance( $this->userId )->changeCoin( 5 , "邀请好友" );
					$inviteReward[$uid] = array( 'fid' => $uid , 'coin' => 5 );
					Data_InviteCode_User::getInstance( $this->userId , true )->award( $uid , 1 );
					$awardTimes += 1;
				}
			}
		}
		return $inviteReward;
	}
	
	

	public function setNewbie( $step )
	{
		 Data_User_Info::getInstance( $this->userId , true )->setNewbie( $step );
	}


	public function setCountry( $country )
	{
		Data_User_Info::getInstance( $this->userId , true )->setCountry( $country );
	}
	/**
	 * 增加/使用金币
	 * @param int $gold
	 */
	public function changeGold( $gold , $evt = 'other' )
	{
		return Data_User_Info::getInstance( $this->userId , true )->changeGold( $gold , $evt );
	}
	
	/**
	 * 增加/使用元宝
	 * @param unknown_type $coin
	 * @param unknown_type $type
	 * @param unknown_type $times
	 * @param string $subType
	 */
	public function changeCoin( $coin , $type = "" , $times = 1 , $subType = "" )
	{
		$ret = Data_User_Info::getInstance( $this->userId , true )->changeCoin( $coin );
		if( $ret && !empty( $type ) )
		{
			if( $coin >= 0 )
			{
				//Stats_Model::incomeCoin( $this->userId , $type , $coin , $subType );
				
				/**
				 * EDAC数据中心统计
				 */
				if( $coin > 0 )
				{
					Stats_Edac::gainCoin( $this->userId, $coin , $type , $subType , 1 );
				}
			}
			else
			{
				Stats_Model::buyHistory( $this->userId , $type , $times , -1 * $coin , $subType );
				/**
				 * EDAC数据中心统计
				 */
		
				$type = MakeCommand_Config::getCoinLogType( $type );
				Stats_Edac::consumeCoin( $this->userId, $coin , $type , $subType , $times );
				
			}
		}
		return $ret;
	}
	
	
	public function changeGachaPoint( $point )
	{
		return Data_User_Info::getInstance( $this->userId , true )->changeGachaPoint( $point );
	}
	
	/**
	 * 加经验
	 * @param unknown_type $exp
	 */
	public function addExp($exp, $onlyAdd = true, $useLevelUpBonus = false)
	{
		$levelUpBonus = null;
		
		$userInfo = $this->getData();
		$levelConfig = Common::getConfig( "level" );
	
		$commonConfig = Common::getConfig( "common" );
		$maxLevel = $commonConfig['maxLevel'] ? $commonConfig['maxLevel'] : self::MAX_LEVEL;
		$maxExp = intval( $levelConfig[$maxLevel]['exp'] );
		$totalExp = $userInfo['exp'] + $exp;

		if( $totalExp > $maxExp && $maxExp > 0  )
		{
			$totalExp = $maxExp;
		}

		$newLevel = $oldLevel = $userInfo['level'];
		$addMaxStamina = 0;
		$addLeaderShip = 0;
		
		$initLevel = ( $onlyAdd == true ) ?  $userInfo['level'] : 1;
		for ( $level = $initLevel ; $level <= $maxLevel ; $level++  )
		{
			if( $level > $maxLevel )
			{
				$userInfo['level'] = $maxLevel;
				$userInfo['exp'] = $levelConfig[$maxLevel]['exp'];
				break;
			}

			if( $totalExp >= intval( $levelConfig[$level]['exp'] )  )
			{
				$addMaxStamina = intval( $levelConfig[$level]['maxStamina'] );
				$setMaxFriend = intval( $levelConfig[$level]['maxfriends'] );
				$addLeaderShip = intval( $levelConfig[$level]['leaderShip'] );
				$newLevel = $level;
			}
		}
		
		
		if( $newLevel != $oldLevel )
		{
			$payFriends = ( $userInfo['maxFriendNum'] - $levelConfig[$oldLevel]['maxfriends'] );
			
			Data_User_Info::getInstance( $this->userId , true )->setLevel( $newLevel );	
			Data_User_Info::getInstance( $this->userId , true )->addMaxStamina( $addMaxStamina );
			Data_User_Info::getInstance( $this->userId , true )->addLeaderShip( $addLeaderShip );
			
			Data_User_Info::getInstance( $this->userId , true )->setFriendNum( $levelConfig[$newLevel]['maxfriends'] + $payFriends );
			//体力回满
            Data_User_Info::getInstance( $this->userId , true )->recoverStaminaFull();
            
            //升级奖励
            if($useLevelUpBonus && $newLevel > $oldLevel)
            {
            	$levelConfig = Common::getConfig('level');
            	for($level = $oldLevel; $level < $newLevel; $level++)
            	{
            		$bonus = $levelConfig[$level + 1]['bonus'];
            		if(empty($bonus)) continue;
            		
            		foreach($bonus as $key => $val)
            		{
            			//TODO:: 是否升级送卡片
            			if($key == 'card') continue;
            			$levelUpBonus[$key] = floatval($levelUpBonus[$key]) + floatval($val);
            		}
            	}
            	
            	if(!empty($levelUpBonus))
            	{
	            	Activity_Model::getInstance($this->userId)->sendPackage($levelUpBonus, 'levelUpBonus', '升级奖励');
            	}
            }
            
            /**
             * 升级统计
             */
            Stats_Edac::upLevel( $this->userId , $newLevel );
		}
		Data_User_Info::getInstance( $this->userId , true )->setExp( $totalExp );
		return array(
			'level' => $newLevel,
			'levelUpBonus' => $levelUpBonus,
		);
	}
	
	
	
	/**
	 * 扩允军营数量
	 * 需求：使用6个元宝，使军营扩允五个位置，上限20
	 * 
	 */
	public function expandCamp( $needCoin = -1 , $eachNum = 0 )
	{
		$commonConfig = Common::getConfig( "common" );
		$needCoin = $needCoin >= 0 ? $needCoin : $commonConfig['card_extend_coin'];
		
		$userInfo = $this->getData();
		
		if( $needCoin > 0 )
		{
			if( $userInfo['coin'] < $needCoin )
			{
				throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_COIN );
			}
			
			//扣钱
			$status = User_Info::getInstance( $this->userId )->changeCoin( -$needCoin , 'expandCamp' );
		}
		
		$cardNumLimit = !empty($commonConfig['maxCardNum']) ? $commonConfig['maxCardNum'] : self::MAX_CARD_NUM;
		
		//已经达到上限
		if( $cardNumLimit - $userInfo['maxCardNum'] <= 0 )
		{
			throw new Card_Exception( Card_Exception::STATUS_CARD_NUM_MAX );
		}
		
		//一次加五个位置
		$eachNum = !empty( $eachNum ) ? $eachNum : $commonConfig['card_extend_num'];
		if( $cardNumLimit - $userInfo['maxCardNum'] > $eachNum )
		{
			$number = $eachNum;
		}
		else
		{
			$number = $cardNumLimit - $userInfo['maxCardNum'];
		}
		
		if( $number > 0 )
		{
			Data_User_Info::getInstance( $this->userId , true )->addCardNum( $number );
		}
	}
	
	/**
	 * 扩允好友数量
	 * 需求：使用6个元宝，使好友上限扩允五个位置，上限50
	 */
	public function expandFriend( $needCoin = -1 , $eachNum = 0 )
	{
		$commonConfig = Common::getConfig( "common" );
		$needCoin = $needCoin >= 0 ? $needCoin : 6;
	
		$userInfo = $this->getData();
		
		if( $needCoin > 0 )
		{
			if( $userInfo['coin'] < $needCoin )
			{
				throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_COIN );
			}
		
			//扣钱
			$status = User_Info::getInstance( $this->userId )->changeCoin( -$needCoin , 'expandFriend' );
		}
	
		//加五个位置
		//扩允上限50个
		$friendNumLimit = self::MAX_FRIEND_NUM;
		//已经达到上限
		if( $friendNumLimit - $userInfo['maxFriendNum'] <= 0 )
		{
			throw new User_Exception( User_Exception::STATUS_FRIEND_MAX );
		}
		//每次五个
		$eachNum = !empty( $eachNum ) ? $eachNum : 5;
		if( $friendNumLimit - $userInfo['maxFriendNum'] > $eachNum )
		{
			$number = $eachNum;
		}
		else
		{
			$number = $friendNumLimit - $userInfo['maxFriendNum'];
		}
	
		if( $number > 0 )
		{
			$friendNum = Data_User_Info::getInstance( $this->userId , true )->addFriendNum( $number );
		}
		return $friendNum;
	}
	/**
	 * 花钱回复体力
	 */
	public function recoverStamina()
	{
		$commonConfig = Common::getConfig( "common" );
		$needCoin = $commonConfig['recover_stamina_coin'];
		
		$userInfo = $this->getData();
		if( $userInfo['coin'] < $needCoin )
		{
			throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_COIN );
		}
		
		//扣钱
		$status = User_Info::getInstance( $this->userId )->changeCoin( -$needCoin , 'recoverStamina' );
		//体力回满
		Data_User_Info::getInstance( $this->userId , true )->recoverStaminaFull();
	}
	
	/**
	 * 复活
	 * @throws User_Exception
	 */
	public function revive()
	{
		$commonConfig = Common::getConfig( "common" );
		$needCoin = $commonConfig['revive_coin'];
		
		$userInfo = $this->getData();
		if( $userInfo['coin'] < $needCoin )
		{
			throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_COIN );
		}
		
		//扣钱
		$status = User_Info::getInstance( $this->userId )->changeCoin( -$needCoin , 'revive' );
	}
	
	
	/**
	 * 生成一个邀请码
	 */
	public static function checkInviteCode( $code )
	{
		 $belongUid =  Data_InviteCode_Model::getCodeBelong( $code );
		return $belongUid > 0 ? true : false;
	}
	
	/**
	 * 使用一个邀请码
	 * @param unknown $code
	 */
	public function useInviteCode( $code )
	{
		$belongUid =  Data_InviteCode_Model::getCodeBelong( $code );
		if( !$belongUid )
		{
			throw new User_Exception( User_Exception::STATUS_INVITE_CODE_INVALID );
		}
		
		if( $belongUid )
		{
			//我的好友通过这个邀请码，把我邀请进来了
			Data_InviteCode_User::getInstance( $belongUid , true )->inviteUser( $this->userId );
	
			//我和他这么好，我要把他加为好友，他肯定也想成为我好友
			Data_Friend_Model::getInstance( $this->userId , true )->addFriend( $belongUid , $code );
			Data_Friend_Model::getInstance( $belongUid , true )->addFriend(  $this->userId  );
		}
		$gold = 20000;
		Data_User_Info::getInstance( $this->userId , true )->changeGold( $gold , '邀请码奖励');
		return $gold;
	}
	
	/**
	 * 获取用户信息
	 */
	public function getData()
	{
		return Data_User_Info::getInstance( $this->userId )->getData();
	}
	
	/**
	 * 获取等级
	 * @return Ambigous <>
	 */
	public function getLevel()
	{
		$userInfo = $this->getData();
		return $userInfo['level'];
	}
	
	/**
	 * 自动回复耐力
	 * @param unknown $userId
	 */
	public function autoRefreshStamina()
	{
		$userInfo = $this->getData();
		Data_User_Info::getInstance( $this->userId , true )->clearBuff();
		//	'stamina' => $data[5],	//耐力
		//	'staminaUpdateTime' => $data[6],//耐力回复时间
		//	'maxStamina' => $data[13],	//最大耐力上限
		$maxStamina = Data_User_Info::getInstance($this->userId)->getMaxStamina();
		$stamina = Data_User_Info::getInstance($this->userId)->getStamina();
		if( $stamina >= $maxStamina )
		{
			return;
		}
		
		if( $_SERVER['REQUEST_TIME'] - $userInfo['staminaUpdateTime'] < 600 )
		{
			return;
		}
		
		$addMaxStamina =  $maxStamina - $stamina;
		
		$canAddStamina = (int)( ($_SERVER['REQUEST_TIME'] - $userInfo['staminaUpdateTime'])/600 );
		$canUseTime =  ($_SERVER['REQUEST_TIME'] - $userInfo['staminaUpdateTime'])%600; 
		$updateTime = $_SERVER['REQUEST_TIME'] - $canUseTime;
		
		$addStamina = $canAddStamina > $addMaxStamina ? $addMaxStamina : $canAddStamina;
		Data_User_Info::getInstance( $this->userId , true )->changeStamina( $addStamina );
		Data_User_Info::getInstance( $this->userId , true )->setStaminaUpdateTime( $updateTime );
		
		$staminaLog = new ErrorLog( "autoRefreshStamina" );
		$staminaLog->addLog( "uid:".$this->userId."curStamina:".$stamina."maxStamina:".$maxStamina."addStamina:".$addStamina );
		
	}
	
	
	/**
	 * 购买道具
	 * @param unknown $itemId
	 */
	public function buy( $itemId )
	{
		$shopConfig = Common::getConfig( 'shopItem');
		$itemConfig = $shopConfig[$itemId];
		if( empty( $itemConfig ))
		{
			throw new User_Exception( User_Exception::STATUS_NO_ITEM_CONFIG );
		}
		
		switch ( $itemConfig['type'] ) {
			//扩充军营
			case 'maxCardNum':
				$logName = '扩充军营';
				User_Info::getInstance( $this->userId )->expandCamp( 0 , $itemConfig['effectValue'] );
			break;
			
			//扩充好友
			case 'maxFriendNum':
				$logName = '扩充好友';
				User_Info::getInstance( $this->userId )->expandFriend( 0 , $itemConfig['effectValue'] );
			break;
			
			//增加附加统御力
			case 'leaderShip':
				$logName = '购买统御令';
				Data_User_Info::getInstance( $this->userId , true )->addBuff( $itemId );
			break;
			//集钱袋
			case 'battleGold':
				$logName = '购买集钱袋';
				Data_User_Info::getInstance( $this->userId , true )->addBuff( $itemId );
			break;
			//招募令
			case 'cardDrop':
				$logName = '购买招募令';
				Data_User_Info::getInstance( $this->userId , true )->addBuff( $itemId );
			break;
			//聚宝盆
			case 'itemDrop':
				$logName = '购买聚宝盆';
				Data_User_Info::getInstance( $this->userId , true )->addBuff( $itemId );
			break;
			//经验书
			case 'battleExp':
				$logName = '购买经验书';
				Data_User_Info::getInstance( $this->userId , true )->addBuff( $itemId );
			break;
			//大力丸
			case 'maxStamina':
				$logName = '购买大力丸';
				Data_User_Info::getInstance( $this->userId , true )->addBuff( $itemId );
				Data_User_Info::getInstance( $this->userId , true )->setStaminaUpdateTime( $_SERVER['REQUEST_TIME'] );
			break;
			//道具
			case 'item':
				$logName = '购买道具';
				for( $i = 0 ; $i < $itemConfig['num'] ; $i++ )
				{
					Card_Model::getInstance( $this->userId )->addCard( $itemConfig['effectValue'] , 1 , true , "shop" );
				}
			break;
			//道具礼包
			case 'itemPackage':
				$logName = '购买道具礼包';
				$userInfo = Data_User_Info::getInstance( $this->userId )->getData();
				
				$pack = json_decode( $itemConfig['effectValue'] , true );
				foreach( $pack as $col => $value )
				{
					if( isset( $userInfo[$col] ) )
					{	
						if( $col == 'maxCardNum' )
						{
							User_Info::getInstance( $this->userId )->expandCamp( 0 , $value );
						}
						elseif( $col == 'maxFriendNum' )
						{
							User_Info::getInstance( $this->userId )->expandFriend( 0 , $value );
						}
						elseif( $col == 'gold' )
						{
							User_Info::getInstance( $this->userId )->changeGold( $value , '购买礼包' );
						}
					}
				}
			break;
			//buff礼包
			case 'buffPack':
				$logName = '购买buff礼包';
			
				$pack = json_decode( $itemConfig['effectValue'] , true );
				foreach( $pack as $_itemId )
				{
					if(!empty($itemConfig[$_itemId]))
					{
						Data_User_Info::getInstance( $this->userId , true )->addBuff( $_itemId );
					}
				}
			break;
		}

		//扣除元宝
		if( !User_Info::getInstance( $this->userId )->changeCoin( -$itemConfig['coin'] , $logName , 1 , $itemId ) )
			throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_COIN );
		
		return $itemConfig;
	}
	
	/**
	 * 道具列表
	 * Enter description here ...
	 */
	public function itemList()
	{
		$ret = Common::getConfig( 'shopItem');
		return $ret;
	}
	
}
