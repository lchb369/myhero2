<?php
if( !defined( 'IN_INU' ) )
{
    return;
}

/**
 * 
 * 召回邮件发送
 * @author 000275
 *
 */
class User_Email extends Logical_Abstract
{

	/**
	 * 单例对象
	 * @var	User_Email
	 */
	protected static $singletonObjects;
	
	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	protected function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	User_Reward
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 发送召回邮件
	 */
	public static function sendEmail( $level )
	{
		ini_set( 'memory_limit' , '1024M' );
		ini_set( 'max_execution_time' , 1200 );
		$now = $_SERVER['REQUEST_TIME'];
		$etcConfig = self::getConfig( 'etc' );
		$limitTime = $now - $etcConfig['recalledInterval'];
		$maxTime = $now - 259200 * 3;
		$config = Common::getConfig( 'emailServer' );
		$emailRestClient = new RestClient( $config["emailDataServerUrl"] , $config["emailDataServerType"] , $config["emailDataServerTimeout"] );
		$appConfig = Common::getConfig( 'api' );
		$grade = 10 * $level;
		$min = $grade - 10;
		$sql = "SELECT `up`.`uid`
				FROM `user_profile` AS `up`
				INNER JOIN `user_ability` AS `ua` ON `ua`.`uid` = `up`.`uid` 
				WHERE `up`.`lastLoginTime` <= {$limitTime} AND `up`.`lastLoginTime` > {$maxTime} AND `ua`.`level` > {$min} AND `ua`.`level` <= {$grade}";
				
		$dbInfo = MysqlDber::fetchAllDatabase( $sql );
		if( empty( $dbInfo ) || !is_array( $dbInfo ) )
		{
			return;
		}

		$i = 0;
		foreach ( $dbInfo as $dbPop  )
		{
			foreach (  $dbPop as $pop  )
			{
				if( !isset( $pop['uid'] ) )
				{
					break;
				}
				$userEmail = User_Email::getInstance( $pop['uid'] );
				if( $userEmail->getSendEmailTime() <= $limitTime )
				{
					$array = array(
						'receiverId' => $pop['uid'] ,
						'templateId' => 2 ,
						'appId' => $appConfig["appId"] ,
					);
					try
					{
						$result = $emailRestClient->callMethod( 'service.sendPublicEmail' , $array );
					}
					catch( Exception $e )
					{
						continue;
					}
					$userEmail->setEmail( $now );
					$i ++;
				}
			}
		}
		ObjectStorage::save();
		FM_Accumulator::getInstance( 100000746410981 )->action( 3701 , $i , $min , $grade );
	}

	/**
	 * 领取召回邮件奖励
	 */
	public function receiveAward( $isNew = false )
	{
		$emailInfo = $this->getUserEmail()->getEmailInfo();
		if( empty( $emailInfo ) || $emailInfo['lastSendTime'] <= 0 )
		{
			if( $isNew )
			{
				return;
			}
			throw new User_Exception( User_Exception::STATUS_NO_RECALL_INFO );
		}
		
		if( isset( $emailInfo['isReceived'] ) && $emailInfo['isReceived'] )
		{
			if( $isNew )
			{
				return;
			}
			throw new Event_Exception( Event_Exception::AWARD_HAS_RECEIVED );
		}
		$awardList = $this->_getAwardList();
		if( empty( $awardList ) )
		{
			return;
		}
		
		$bagModel = Bag_Model::getInstance( $this->userId );
		foreach( $awardList as $itemId => $itemNum )
		{
			if( Bag_Model::getItemType( $itemId ) == Bag_Model::ITEM_TYPE_MATERIAL )
			{
				$bagModel->changeItem( $itemId , $itemNum );
			}
			else 
			{
				for( $i = 0 ; $i < $itemNum ; $i ++ )
				{
					$bagModel->addEquipment( $itemId );
				}
			}
		}
		$this->getUserEmailWithWriteLock()->setGetGiftStatus();
	}
	
	/**
	 * 获取用户的奖励信息
	 */
	public function getUserEmailAward()
	{
		$emailInfo = $this->getUserEmail()->getEmailInfo();
		if( empty( $emailInfo ) || $emailInfo['lastSendTime'] <= 0 )
		{
			return array();
		}
		
		if( isset( $emailInfo['isReceived'] ) && $emailInfo['isReceived'] )
		{
			return array();
		}
		return $this->_getAwardList();
	}
	
	/**
	 * 获取领取的邮件奖励信息
	 */
	private function _getAwardList()
	{
		$emailConfig = Common::getConfig( 'RecalledAwardConfig' );
		$level = User_Ability::getInstance( $this->userId )->getLevel();
		$awardList = array();
		foreach( $emailConfig as $key => $info )
		{
			if( $level >= $info['levelLowerLimit'] && $level <= $info['levelUpperLimit'] )
			{
				$awardList = $info['awardList'];
				break;
			}
		}
		return $awardList;
	}
	
	/**
	 * 设置发送email成功的用户信息
	 */
	public function setEmail( $time )
	{
		$this->getUserEmailWithWriteLock()->setEmail( $time );
	}
	
	/**
	 * 设置领取奖励状态为已领取
	 */
	public function setGetGiftStatus()
	{
		$this->getUserEmailWithWriteLock()->setGetGiftStatus();
	}
	
	/**
	 * 获取领取状态
	 */
	public function getEmailInfo()
	{
		return $this->getUserEmail()->getEmailInfo();
	}
	
	/**
	 * 获取发送email的用户时间信息
	 */
	public function getSendEmailTime()
	{
		return $this->getUserEmail()->getSendEmailTime();
	}
	
	/**
	 * 获取数据
	 */
	public function getData()
	{
		return $this->getUserEmail()->getData();
	}
}
