<?php

class User_ConvertUserId
{
	
	const THIRTY_TWO_BIT_LIMIT = 1147483648;
	/**
	 * 转换第三方平台的用户ID到游戏用户ID
	 *
	 * @param	string $originalUserId	第三方平台的用户ID
	 * @return	integer
	 */
	public static function convertToGameUserId( $originalUserId )
	{	
		//校验用户ID是否为空
		if( empty( $originalUserId ) )
		{
			//为空：返回无效的用户ID
			return 0;
		}

		//判断是直接计算还是需要搜索
		if( ( preg_match( '|[^0-9a-fA-F]|' , $originalUserId , $arr ) ) == 0 )
		{
			//没有非16进制的字符：
			//去除左边的零
			$userId = ltrim( $originalUserId , '0' );

			//判断长度
			if( strlen( $userId ) <= 8 )
			{
				//少于等于8位：
				//计算10进制数
				$userId = intval( $userId , 16 );
				if( $userId < self::THIRTY_TWO_BIT_LIMIT )
				{
					return $userId;
				}
			}
			//大于8位：计算用户ID
		}
		//包含非16进制字符：计算用户ID

		//根据第三方平台的用户ID，在缓存中获取信息
		Helper_RunLog::getInstance()->addLog( "convertToGameUserId" , "_getFromCacheByOriginalUserId start" );
		$fmUserId = self::_getFromCacheByOriginalUserId( $originalUserId );
		//校验返回数据
		if( $fmUserId > 0 )
		{
			//不为空：返回游戏用户ID
			return $fmUserId;
		}
		//为空：
		//根据第三方平台的用户ID，在数据库中获取信息

		Helper_RunLog::getInstance()->addLog( "convertToGameUserId" , "_getFromDBByOriginalUserId start" );
		$fmUserId = self::_getFromDBByOriginalUserId( $originalUserId );

		
		//校验返回数据
		if( $fmUserId > 0 )
		{
			//不为空：
			//保存第三方用户ID和游戏用户ID的对应关系到缓存
			self::_setToCache( $originalUserId , $fmUserId );

			//返回游戏用户ID
			return $fmUserId;
		}
		//为空：

		//初始化尝试次数
		Helper_RunLog::getInstance()->addLog( "convertToGameUserId" , "_getNewFMUserId start" );
		$tryTimes = 0;
		do
		{
			//申请一个新的游戏用户ID
			$fmUserId = self::_getNewFMUserId( $originalUserId );
		
			//尝试次数加一
			$tryTimes++;
		}
		//保存第三方用户ID和游戏用户ID的对应关系到数据库
		while( ( $result = self::_updateToDB( $originalUserId , $fmUserId ) ) == false && $tryTimes < 3 );

		Helper_RunLog::getInstance()->addLog( "convertToGameUserId" , "_getNewFMUserId end" );
		if( $result )
		{
			//保存第三方用户ID和游戏用户ID的对应关系到缓存
			self::_setToCache( $originalUserId , $fmUserId );
		}

		//返回游戏用户ID
		return $fmUserId;
	}

	/**
	 * 绑定游戏帐号
	 * 如果之前已经绑过了，则直接返回，如果没有绑过，则用户帐号ID
	 * @param unknown $loginName
	 * @param unknown $bindUid
	 */
	public static function bindGameUserId( $originalUserId  , $gameUid  )
	{
		//检查是否绑定过，如果绑过，就直接返回
		$fmUserId = self::_getFromCacheByOriginalUserId( $originalUserId );
		if( $fmUserId > 0   )
		{
			return $fmUserId;
		}
		//为空：
		//根据第三方平台的用户ID，在数据库中获取信息
		$fmUserId = self::_getFromDBByOriginalUserId( $originalUserId );
		
		//校验返回数据
		if( $fmUserId > 0   )
		{
			//不为空：
			//保存第三方用户ID和游戏用户ID的对应关系到缓存
			self::_setToCache( $originalUserId , $fmUserId );
		  	return $fmUserId; 
		}
		
		//如果没有绑定过，则绑定
		$result = self::_updateToDB( $originalUserId , $gameUid ) ;
		if( $result )
		{
			self::_setToCache( $originalUserId , $gameUid );
		}
		else
		{
			return 0;
		}
		//初始化尝试次数
		return $gameUid;
	}
	
	
	/**
	 * 强制绑定游戏帐号
	 * 如果之前已经绑过了，则直接返回，如果没有绑过，则用户帐号ID
	 * @param unknown $loginName
	 * @param unknown $bindUid
	 */
	public static function forceBindGameUserId( $originalUserId  , $gameUid  )
	{
		//如果没有绑定过，则绑定
		$result = self::_updateToDB( $originalUserId , $gameUid ) ;
		if( $result )
		{
			self::_setToCache( $originalUserId , $gameUid );
		}
		else
		{
			return 0;
		}
		//初始化尝试次数
		return $gameUid;
	}
	
	
	/**
	 * 转换多个第三方平台的用户ID到游戏用户ID
	 *
	 * @param	string $originalUserIds	第三方平台的用户ID
	 * @param 	boolean $needAdd 不存在的用户是否要添加游戏用户ID
	 * @return	integer
	 */
	public static function convertMultiToFMUserId( $originalUserIds , $needAdd = false )
	{
		$returnData = array();
		
		//直接计算游戏用户Id
		foreach ( $originalUserIds as $key => $originalUserId )
		{
			//判断第三方平台用户的ID是否合法
			if( empty( $originalUserId ) )
			{
				//为空：返回无效的用户ID
				unset( $originalUserIds[$key] );
				continue;
			}
			//判断是直接计算还是需要搜索
			if( ( preg_match( '|[^0-9a-fA-F]|' , $originalUserId , $arr ) ) == 0 )
			{
				//没有非16进制的字符：
				//去除左边的零
				$userId = ltrim( $originalUserId , '0' );
				
				//判断长度
				if( strlen( $userId ) <= 8 )
				{
					//少于等于8位：
					//计算10进制数
					$userId = intval( $userId , 16 );
					if( $userId < self::THIRTY_TWO_BIT_LIMIT )
					{
						//将能过计算得到游戏用户Id的第三方平台的ID从$originalUserIds数组中除去
						unset( $originalUserIds[$key] );
						//将计算得到的游戏用户ID存入返回数组
						$returnData[$originalUserId] = $userId;						
					}
				}
				//大于8位：计算用户ID
			}
		}
		
		//从memcache取游戏用户UserId
		if( empty( $originalUserIds ) )
		{
			return $returnData;
		}
		$memcacheFMUserId = self::_getFromCacheByMultiOriginalUserId( $originalUserIds );
		$gotOriginalUserIds = array();
		foreach ( $memcacheFMUserId as $key => $fmUserId )
		{
			//将能过计算得到游戏用户Id的第三方平台的ID从$originalUserIds数组中除去
			$originalUserId = substr( $key , 0 , strlen( $key ) - 11 );
			$gotOriginalUserIds[] = $originalUserId;
			
			//将从memcache取到的游戏用户ID存入返回数组
			$returnData[$originalUserId] = $fmUserId;
		}
		$originalUserIds = array_diff( $originalUserIds , $gotOriginalUserIds );
		
		if( empty( $originalUserIds ) )
		{
			return $returnData;
		}
		
		//从数据库中取游戏用户UserId		
		$tableData = self::_getTableNameByMultiOriginalUserId( $originalUserIds );
		
		//从不同的表中取游戏用户的ID
		foreach ( $tableData as $tableName => $userIds )
		{
			$resultFormTable[] = self::_getFromDBByMultiOriginalUserId( $userIds , $tableName );
		}
		//将取到的数据存入返回数组
		//将能过数据库取到的游戏用户Id的第三方平台的ID从$originalUserIds数组中除去
		$gotOriginalUserIds = array();
		foreach ( $resultFormTable as $row )
		{
			foreach ( $row as $data )
			{
				$returnData[$data["originalUserId"]] = $data["fmUserId"];
				$gotOriginalUserIds[] = $data["originalUserId"];
				self::_setToCache( $data["originalUserId"] , $data["fmUserId"] );
			}
		}
		
		if( $needAdd == false )
		{
			return $returnData;
		}
		
		$originalUserIds = array_diff( $originalUserIds , $gotOriginalUserIds );
		
		if( empty( $originalUserIds ) )
		{
			return $returnData;
		}
		//添加新的$originalUserIds;
		foreach ( $originalUserIds as $originalUserId )
		{			
			//判断第三方平台用户的ID是否合法
			if( empty( $originalUserId ) )
			{
				//为空：返回无效的用户ID
				continue;
			}			
			//初始化尝试次数
			$tryTimes = 0;
			do
			{
				//申请一个新的游戏用户ID
				$fmUserId = self::_getNewFMUserId( $originalUserId );
				
				//尝试次数加一
				$tryTimes++;
			}
			//保存第三方用户ID和游戏用户ID的对应关系到数据库
			while( ( $result = self::_updateToDB( $originalUserId , $fmUserId ) ) == false && $tryTimes < 3 );
			
			if( $result )
			{
				//保存第三方用户ID和游戏用户ID的对应关系到缓存
				self::_setToCache( $originalUserId , $fmUserId );
				$returnData[$originalUserId] = $fmUserId;
			}
			else 
			{
				$returnData[$originalUserId] = 0;
			}
		}
		return $returnData;		
	}
	
	/**
	 * 转换游戏用户ID到第三方平台的用户ID
	 *
	 * @param	integer $fmUserId	游戏用户ID
	 * @return	string
	 */
	public static function convertToOriginalUserId( $fmUserId )
	{
		//校验用户ID是否为空
		if( (integer)$fmUserId <= 0 )
		{
			//为空：返回无效的用户ID
			return "";
		}

		//判断用户ID是否小于8位16进制数字
		if( $fmUserId < self::THIRTY_TWO_BIT_LIMIT )
		{
			//小于8位16进制数：直接计算用户ID
			return sprintf( '%032x' , $fmUserId );
		}

		//根据游戏用户ID，在缓存中获取信息
		$originalUserId = self::_getFromCacheByFMUserId( $fmUserId );

		//校验返回数据
		if( strlen( $originalUserId ) > 0 )
		{
			//不为空：返回第三方平台的用户ID
			return $originalUserId;
		}
		//为空：
		//根据游戏用户ID，在数据库中获取信息
		$originalUserId = self::_getFromDBByFMUserId( $fmUserId );

		//校验返回数据
		if( strlen( $originalUserId ) > 0 )
		{
			//不为空：
			//保存第三方用户ID和游戏用户ID的对应关系到缓存
			self::_setToCache( $originalUserId , $fmUserId );

			//返回第三方平台的用户ID
			return $originalUserId;
		}
		//为空：返回无效的用户ID
		return "";
	}


	/**
	 * 转换游戏多个用户ID到第三方平台的用户ID
	 *
	 * @param	array $fmUserId	游戏用户ID
	 * @return	string
	 */
	public static function convertMultiToOriginalUserId( $fmUserIds )
	{
		//校验用户ID是否为空
		if( !is_array( $fmUserIds )  || empty( $fmUserIds ) )
		{
			//为空：返回无效的用户ID
			return array();
		}
		$originalUserIdArr = array();
		$fmUserIdOverage = array();
		foreach( $fmUserIds as $fmUserId )
		{
			//判断用户ID是否小于8位16进制数字
			if( $fmUserId < self::THIRTY_TWO_BIT_LIMIT )
			{
				//小于8位16进制数：直接计算用户ID
				$originalUserIdArr[$fmUserId] = sprintf( '%032x' , $fmUserId );
			}
			else 
			{
				$fmUserIdOverage[] = $fmUserId;
			}
		}
		
		if( empty( $fmUserIdOverage ) )
		{
			return $originalUserIdArr;
		}
		//根据游戏用户ID，在缓存中获取信息
		$getOriginalFromCache = self::_getFromCacheByFMUserId( $fmUserIdOverage );
		//校验返回数据
		$gotFMUserIds = array();
		if( is_array( $getOriginalFromCache ) )
		{
			foreach ( $getOriginalFromCache as $cacheKey => $originalId )
			{
				$temp = explode( '_' , $cacheKey );
				$originalUserIdArr[$temp[0]] = $originalId;
				$gotFMUserIds[] = $temp[0];
			}
		}
		$fmUserIdOverage = array_diff( $fmUserIdOverage , $gotFMUserIds );
		if( empty( $fmUserIdOverage ) )
		{
			return $originalUserIdArr;
		}
		//为空：
		//根据游戏用户ID，在数据库中获取信息
		$setToCache = array();
		$getOriginalFromDB = self::_getFromDBByFMUserId( $fmUserIdOverage );
		$fmUserIdOverage = array_diff( $fmUserIdOverage , array_keys( $getOriginalFromDB ) );
		foreach( $getOriginalFromDB as $fmUserId => $originalUserId )
		{
			$setToCache[$fmUserId] = $originalUserId;
			$originalUserIdArr[$fmUserId] = $originalUserId;
		}
	
		if( !empty( $setToCache ) )
		{
			foreach ( $setToCache as $fmUserId => $originalId )
			{
				self::_setToCache( $originalId , $fmUserId );
			}
		}
	
		foreach( $fmUserIdOverage as $fmUserId )
		{
			$originalUserIdArr[$fmUserId] = "";
		}
		return $originalUserIdArr;
	}

	/**
	 * 根据第三方平台的用户ID获取缓存信息
	 *
	 * @param	string $originalUserId	第三方平台的用户ID
	 * @return	integer
	 */
	private static function _getFromCacheByOriginalUserId( $originalUserId )
	{
		//获取Cache Key
		$cacheKey = self::_getCacheKeyByOriginalUserId( $originalUserId );

		//获取Cache实例
		$cache = Common::getCache();

		//获取缓存信息
		$fmUserId = $cache->get( $cacheKey );

		//返回游戏用户ID
		return (integer)( $fmUserId );
	}

	/**
	 * 根据第三方平台的用户ID获取缓存信息
	 *
	 * @param	string $originalUserId	第三方平台的用户ID
	 * @return	integer
	 */
	private static function _getFromCacheByMultiOriginalUserId( $originalUserIds )
	{
		//获取Cache Key
		$cacheKey = self::_getCacheKeyByMultiOriginalUserId( $originalUserIds );
		
		//获取Cache实例
		$cache = Common::getCache( "userid" );
		
		//获取缓存信息
		$fmUserId = $cache->get( $cacheKey );
		
		//返回游戏用户ID
		return $fmUserId;
	}
	
	/**
	 * 根据游戏用户ID获取缓存信息
	 *
	 * @param	integer $fmUserId	游戏用户ID
	 * @return	string
	 */
	private static function _getFromCacheByFMUserId( $fmUserId )
	{
		//获取Cache Key
		$cacheKey = self::_getCacheKeyByFMUserId( $fmUserId );

		//获取Cache实例
		$cache = Common::getCache( "userid" );

		//获取缓存信息
		$originalUserId = $cache->get( $cacheKey );

		//返回第三方平台的用户ID
		return empty( $originalUserId ) ? "" : $originalUserId;
	}

	/**
	 * 根据第三方平台的用户ID获取数据库信息
	 *
	 * @param	string $originalUserId	第三方平台的用户ID
	 * @return	integer
	 */
	private static function _getFromDBByOriginalUserId( $originalUserId )
	{
		
		
		//获取数据库实例
		$db = self::_getDB( $originalUserId );

		//计算表定位
		$tableName = self::_getTableNameByOriginalUserId( $originalUserId );

	
		//生成SQL
		$sql = "SELECT `fmUserId` FROM `{$tableName}` WHERE `originalUserId` = '". addslashes( $originalUserId ) ."'";

		
		//执行SQL
		//$result = $db->fetchOneAssoc( $sql );

		
		$result = MysqlPool::getInstance( 1 )->findQuery(  $sql , $db );
		//返回游戏用户ID
		return (integer)( $result[0][0] );
	}

	/**
	 * 根据多个第三方平台的用户ID获取数据库信息
	 *
	 * @param	string $originalUserIds	第三方平台的用户ID
	 * @return	$array(
	 * 				'originalUserId' => ,
	 * 				'fmUserId' => 
	 * 			)
	 */
	private static function _getFromDBByMultiOriginalUserId( $originalUserIds , $tableName )
	{
		//获取数据库实例
		$db = self::_getDB();
		$or = '';
		foreach ( $originalUserIds as $row  )
		{
			if ( empty( $or ) )
			{
				$or = " WHERE `originalUserId` = '". addslashes( $row ) ."'";
			}
			else 
			{
				$or .= " or `originalUserId` = '". addslashes( $row ) ."'";
			}
		}
		
		//生成SQL
		$sql = "SELECT `originalUserId` , `fmUserId` FROM `{$tableName}` ". $or;
		//执行SQL
		$result = $db->fetchArray( $sql );
		//返回
		return $result;
	}
	
	/**
	 * 根据游戏用户ID获取数据库信息
	 *
	 * @param	integer $fmUserId	游戏用户ID
	 * @return	string
	 */
	private static function _getFromDBByFMUserId( $fmUserId )
	{
		//获取数据库实例
		$db = self::_getDB();

		//生成SQL
		if( is_array( $fmUserId ) )
		{
			$returnValue = array();
	        $sqls = array();
			foreach( $fmUserId as $userId )
			{
		        $tableName = self::_getTableNameByFMUserId( $userId );
		        if( !isset( $sqls[$tableName] ) )
		        {
		        	$sqls[$tableName] = "SELECT `originalUserId` FROM `{$tableName}` WHERE `fmUserId` = {$userId}";
		        }
		        else 
		        {
		        	$sqls[$tableName] .= " AND `fmUserId` = {$userId}";
		        }
			}
			foreach( $sqls as $sql )
			{
				$result = $db->fetchArray( $sql );
				if( !empty( $result ) )
				{
					foreach( $result as $record )
					{
					$returnValue[$record['fmUserId']] = $record['originalUserId'];
					}
				}
			
			}
			return $returnValue;
		}
		else 
		{
			//计算表定位
			$tableName = self::_getTableNameByFMUserId( $fmUserId );
			$sql = "SELECT `originalUserId` FROM `{$tableName}` WHERE `fmUserId` = {$fmUserId}";

			//执行SQL
			$result = $db->fetchOneAssoc( $sql );

			//返回第三方平台的用户ID
			return empty( $result["originalUserId"] ) ? "" : $result["originalUserId"];
		}
	}

	/**
	 * 保存第三方用户ID和游戏用户ID的对应关系到缓存
	 *
	 * @param	string $originalUserId	第三方平台的用户ID
	 * @param	integer $fmUserId	游戏用户ID
	 * @return	boolean
	 */
	private static function _setToCache( $originalUserId , $fmUserId )
	{
		//计算第三方平台的用户ID的Cache Key
		$originalUserCacheKey = self::_getCacheKeyByOriginalUserId( $originalUserId );

		//计算游戏用户ID的Cache Key
		$fmUserCacheKey = self::_getCacheKeyByFMUserId( $fmUserId );

		//获取Cache实例
		$cache = Common::getCache();

		//载入通用配置
		$config = Common::getConfig();

		//保存第三方平台的用户ID对应的游戏用户ID到缓存中
		$cache->set( $originalUserCacheKey , $fmUserId );

		//保存游戏用户ID对应的第三方平台的用户ID到缓存中
		$cache->set( $fmUserCacheKey , $originalUserId );

		//返回true
		return true;
	}

	/**
	 * 保存第三方用户ID和游戏用户ID的对应关系到数据库
	 *
	 * @param	string $originalUserId	第三方平台的用户ID
	 * @param	integer $fmUserId	游戏用户ID
	 * @return	boolean
	 */
	private static function _updateToDB( $originalUserId , $fmUserId )
	{
		//获取数据库实例
		$db = self::_getDB();

		//计算表定位
		$tableName = self::_getTableNameByOriginalUserId( $originalUserId );

		//生成SQL
		$sql[] = "INSERT INTO `{$tableName}` ( `originalUserId` , `fmUserId` ) VALUES ( '". addslashes( $originalUserId ) ."' , {$fmUserId} )";

		//执行SQL
		$result = MysqlPool::getInstance( 1 )->query( $sql , $db );

		//返回执行结果
		return $result;
	}

	/**
	 * 根据第三方平台的用户ID计算数据表定位
	 *
	 * @param	string $originalUserId	第三方平台的用户ID
	 * @return	string
	 */
	private static function _getTableNameByOriginalUserId( $originalUserId )
	{
		//对第三方平台的用户ID进行MD5
		//计算数据表定位
		$tableId = substr( md5( $originalUserId ) , 0 , 1);

		//返回数据表定位
		return "index_{$tableId}";
	}

	/**
	 * 根据多个第三方平台的用户ID计算数据表定位
	 *
	 * @param	string $originalUserIds	第三方平台的用户ID
	 * @return	string
	 */
	private static function _getTableNameByMultiOriginalUserId( $originalUserIds )
	{
		//对第三方平台的用户ID进行MD5
		//计算数据表定位
		foreach ( $originalUserIds as $originalUserId )
		{
			//判断第三方平台用户的ID是否合法
			if( empty( $originalUserId ) )
			{
				//为空：返回无效的用户ID
				continue;
			}
			$tableId = substr( md5( $originalUserId ) , 0 , 1);
			$array["index_{$tableId}"][] = $originalUserId;
		}
		
		//返回数据表定位
		return $array;
	}
	
	/**
	 * 根据游戏用户ID计算数据表定位
	 *
	 * @param	integer $fmUserId	游戏用户ID
	 * @return	string
	 */
	private static function _getTableNameByFMUserId( $fmUserId )
	{
		//计算数据表定位
		//返回数据表定位
		return "index_". sprintf( '%x' , ( $fmUserId % 16 ) );
	}

	/**
	 * 获取数据库实例
	 * @return	Db
	 */
	private static function & _getDB()
	{
		//返回数据库实例
		/*
		$dbConfig = Common::getConfig( "userIndex" );
		$db = Common::getNormalDatabaseEngine(  "userIndex"  );
		*/
		$dbConfig = Common::getConfig( "mysqlPool" );
		return $dbConfig['userIndex']; 
	}

	/**
	 * 获取一个新的游戏用户ID
	 *
	 * @param	string $originalUserId	第三方平台的用户ID
	 * @return	integer
	 */
	private static function _getNewFMUserId( $originalUserId )
	{
		Helper_RunLog::getInstance()->addLog( "convertToGameUserId" , "rand _getNewFMUserId start" );
		//生成游戏用户ID
		$md5str = intval( substr( md5( $originalUserId ) , 0 , 1 ) , 16 );
		$str = mt_rand( 1 , self::THIRTY_TWO_BIT_LIMIT );
		$fmUserId = ( $str - ( $str % 16 ) ) + $md5str;

		Helper_RunLog::getInstance()->addLog( "convertToGameUserId" , "rand_getNewFMUserId end" );
		//返回用户ID
		return $fmUserId;
	}

	/**
	 * 根据第三方平台的用户ID获取Cache Key
	 *
	 * @param	string $originalUserId	第三方平台的用户ID
	 * @return	integer
	 */
	private static function _getCacheKeyByOriginalUserId( $originalUserId )
	{
		//计算Cache Key
		//返回Cache Key
		return "{$originalUserId}_toGameUserId";
	}

	/**
	 * 根据多个第三方平台的用户ID获取Cache Key
	 *
	 * @param	string $originalUserId	第三方平台的用户ID
	 * @return	integer
	 */
	private static function _getCacheKeyByMultiOriginalUserId( $originalUserIds )
	{
		//计算Cache Key
		foreach ( $originalUserIds as $originalUserId )
		{
			$array[] = "{$originalUserId}_toFMUserId";
		}
		//返回Cache Key
		return $array;
	}
	
	/**
	 * 根据游戏用户ID获取Cache Key
	 *
	 * @param	string $originalUserId	游戏用户ID
	 * @return	integer
	 */
	private static function _getCacheKeyByFMUserId( $fmUserId )
	{
		//计算Cache Key
		//返回Cache Key
		$cacheKeys = array();
		if( is_array( $fmUserId ))
		{
			foreach( $fmUserId as $item )
			{
				$cacheKeys[] = $item ."_toOriginalUserId";
			}
			return $cacheKeys;
		}
		return "{$fmUserId}_toOriginalUserId";
	}
}

?>