<?php

/**
 * 重构User Model
 * 实现用户注册和删除功能
 * 
 * @author	Lucky
 * @modifier Roy
 * @since 2011-3-9
 */
if( !defined( 'IN_INU' ) )
{
    return;
}
class User_Platform
{
	
	public static  function lootLogin( $userIds = '' )
	{
		$userList = explode( "," , $userIds );
		$message = '';
		foreach (  $userList  as $userId  )
		{
			if( $userId > 0 )
			{
				try {
					$level = User_Ability::getInstance( $userId )->getLevel();
				} 
				catch ( User_Exception $ue )
				{
					if( $ue->getCode() )
					{
						$message .= $userId."|";
						continue;
					}
				}
				FM_LootLogic::getInstance()->login( $userId ,  $level , Arena_Model::getInstance( $userId )->getNewArenaScore() ) ;
			}
		}
		if( $message == '' )
		{
			$message = '已全部login';
		}
		else 
		{
			$message .= '用户不存在';
		}
		return $message;
	}
	
	
	protected static  function deleleRankList( $userId )
	{
		$level = User_Ability::getInstance($userId)->getLevel();
		$rankLevelConfig = Common::getConfig( 'RankLevelConfig' );
		$oldListId = $newListId = 0;	
		foreach (  $rankLevelConfig['rankList'] as $listId => $listInfo  )
		{
			if( $level >= $listInfo['lowerLimit'] &&  $level <=  $listInfo['upperLimit'] )
			{
				 $oldListId = $listId;
			}
		}
		Arena_RankFight::changeListId( $userId , $oldListId , $oldListId );
	}
	
	/**
	 * 删除用户
	 * @param	int $userId	用户ID
	 * @return boolean
	 */
	public static function deleteUser( $userId )
	{
	
		//self::deleleRankList( $userId );
		
		
		//删除缓存（请开发者自觉按照表名的字母排序修改这段代码）
		Data_Arena_Model::getInstance( $userId , true )->deleteCache( true );
		Data_Bag_Model::getInstance( $userId , true )->deleteCache( true );
		Data_Buff_Model::getInstance( $userId , true )->deleteCache( true );
		Data_Building_Model::getInstance( $userId , true )->deleteCache( true );
		Data_Etc_Model::getInstance( $userId , true )->deleteCache( true );
		Data_Map_Model::getInstance( $userId , true )->deleteCache( true );
		Data_Round_Model::getInstance( $userId , true )->deleteCache( true );
		Data_Ship_Avatar::getInstance( $userId , true )->deleteCache( true );
		Data_Ship_Model::getInstance( $userId , true )->deleteCache( true );
		Data_Ship_Suit::getInstance( $userId , true )->deleteCache( true );
		Data_Skill_Model::getInstance( $userId , true )->deleteCache( true );
		Data_Social_Model::getInstance( $userId , true )->deleteCache( true );
		Data_Task_Model::getInstance( $userId , true )->deleteCache( true );
		Data_User_Ability::getInstance( $userId , true )->deleteCache( true );
		Data_User_Profile::getInstance( $userId , true )->deleteCache( true );
		Data_User_Reward::getInstance( $userId , true )->deleteCache( true );
		Data_Tackle_Model::getInstance( $userId , true )->deleteCache( true );
		Data_Event_Model::getInstance(  $userId , true )->deleteCache( true );
		//Data_Arena_RankFight::getInstance(  $userId , true )->deleteCache( true );
		Data_SeasonArena_Model::getInstance(  $userId , true )->deleteCache( true );
		Data_SeasonArena_SystemInfo::getInstance(  $userId , true )->deleteCache( true );
		Data_Arena_Betting::getInstance( $userId , true )->deleteCache( true );
		
		$platform = Common::getConfig( 'platform' );
		if( $platform == 'fb_tw' )
		{
			Data_User_Email::getInstance( $userId , true )->deleteCache( true );
		}
		Log_Model::remove( $userId );
		//从任务服务器删除数据
		Task_User::getInstance( $userId )->deleteUser();
		//从排行榜移除
		
		ObjectStorage::save();
		
		//从数据库中删除（请开发者自觉按照表名的字母排序修改这段代码）
		$db = Common::getDB( $userId );
		$db->delete( 'arena' );
		$db->delete( 'bag' );
		$db->delete( 'bag_equipment' );
		$db->delete( 'bag_item' );
		$db->delete( 'buff' );
		$db->delete( 'building' );	
		$db->delete( 'map' );
		$db->delete( 'map_unlock' );
		$db->delete( 'order' );
		$db->delete( 'season_arena' );
		$db->delete( 'ship' );
		$db->delete( 'ship_avatar' );
		$db->delete( 'ship_npc' );
		$db->delete( 'skill' );
		$db->delete( 'skill_info' );
		$db->delete( 'skill_order' );
		$db->delete( 'social' );
		$db->delete( 'social_trade' );
		$db->delete( 'user_ability' );
		$db->delete( 'user_profile' );
		$db->delete( 'tackle' );
		$db->delete( 'tackle_drug' );
		$db->delete( 'invite_activity' );
		$db->delete( 'rank_fight' );
		$db->delete( 'user_gift' );
		$db->delete( 'user_reward' );
		$db->delete( 'new_rank_fight' );
		$db->delete( 'arena_betting' );

		if( $platform == 'fb_tw' )
		{
			$db->delete( 'email_gift' );
		}
		return true;
	}
}