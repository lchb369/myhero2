<?php

class Neighbor
{
	/**
	 * 单例对象
	 * @var	Neighbor
	 */
	protected static $singletonObject = null;
	
	/**
	 * REST Client
	 * @var	RestClient
	 */
	protected $client;
	
	/**
	 * 邻居关系（是邻居）
	 * @var	int
	 */
	const NEIGHBOR_STATUS_BE_NEIGHBOR = 0;
	
	/**
	 * 邻居关系（邀请中）
	 * @var	int
	 */
	const NEIGHBOR_STATUS_INVITE_PEDDING = 1;
	
	/**
	 * 邻居关系（被邀请中）
	 * @var	int
	 */
	const NEIGHBOR_STATUS_BE_INVITED = 2;
	
	/**
	 * 邻居关系（不是邻居）
	 * @var	int
	 */
	const NEIGHBOR_STATUS_NOT_NEIGHBOR = 3;
	
	/**
	 * 初始化
	 */
	protected function __construct()
	{
		$this->client = & Common::getClient( 'frameServer' );
	}
	
	/**
	 * 获取实例化
	 * @return	Neighbor
	 */
	public static function getInstance()
	{
		if( self::$singletonObject == null )
		{
			self::$singletonObject = new Neighbor();
		}
		
		return self::$singletonObject;
	}
	
	/**
	 * 确认邻居关系
	 * @param	int $userId	用户ID
	 * @param	int $friendId	好友ID
	 * @return	boolean
	 */
	public function confirmNeighbor( $userId , $friendId )
	{
		try
		{
			$result = $this->client->callMethod( 'neighbor.confirmByAPI' , array( 'userId' => $userId , 'friendId' => $friendId ) );
		}
		catch ( Exception $e )
		{
			return false;
		}

		return $result['result'];
	}

	/**
	 * 删除邻居
	 * @param	int $userId	用户ID
	 * @param	int $friendId	好友ID
	 * @return	boolean
	 */
	public function deleteNeighbor( $userId , $friendId )
	{
		try
		{
			$result = $this->client->callMethod( 'neighbor.deleteNeighbor' , array( 'userId' => $userId , 'friendId' => $friendId ) );
		}
		catch ( Exception $e )
		{
			return false;
		}

		return $result['result'];
	}

	/**
	 * 忽略邻居邀请
	 * @param	int $userId	用户ID
	 * @param	int $friendId	好友ID
	 * @return	boolean
	 */
	public function skipInviteNeighbor( $userId , $friendId )
	{
		try
		{
			$result = $this->client->callMethod( 'neighbor.skipInviteByAPI' , array( 'userId' => $userId , 'friendId' => $friendId ) );
		}
		catch ( Exception $e )
		{
			return false;
		}

		return $result['result'];
	}


	/**
	 * 从框架服务器获取邻居列表
	 * @param	int $userId	用户ID
	 * @return	array(
	 * 				{friendId}:array(
	 * 					status:int	//邻居状态
	 * 					isNew:boolean	//是否新邻居
	 * 				)
	 * 			)
	 */
	public function getNeighbors( $userId , $version = 0 )
	{
		$data = $this->getNeighborsFromCache( $userId , $version );
		return empty( $data ) ? array() : $data['list'];
	}

	/**
	 * 从框架服务器获取邻居用户ID
	 * @param	int $userId	用户ID
	 * @return	int[]
	 */
	public function getNeighborIds( $userId , $version = 0 )
	{
		$result = $this->getNeighborsFromCache( $userId , $version );
		$neighborList = array();
		if( isset( $result['list'] ) && is_array( $result['list'] ) )
		{
			foreach ( $result['list'] as $friendId => $friend )
			{
				if( $friend['status'] == Neighbor::NEIGHBOR_STATUS_BE_NEIGHBOR )
				{
					$neighborList[] = $friendId;
				}
			}
		}
		return $neighborList;
	}

	/**
	 * 第一次访问邻居奖励
	 * @param	int $userId	用户ID
	 * @param	int $friendId	好友ID
	 * @return	boolean
	 */
	public function setNotNew( $userId , $friendId )
	{
		//设置邻居状态
		try
		{
			$result = $this->client->callMethod( 'neighbor.setNotNewNeighbor' , array( 'userId' => $userId , 'friendId' => $friendId ) );
		}
		catch ( Exception $e )
		{
			return false;
		}
		return $result['result'];
	}
	
	/**
	 * 获取最后一次更新时间
	 * @param	int $userId	用户ID
	 * @return	int
	 */
	public function getLastUpdateTime( $userId )
	{
		return Etc_Model::getInstance( $userId )->getNeighborsVersion();
	}
	
	/**
	 * 缓存好友数据
	 * @param int $userId 用户ID
	 * @param int $version 前端传入的版本号
	 */
	protected function  getNeighborsFromCache( $userId , $version = 0 )
	{
		$etc = Etc_Model::getInstance( $userId );
		//如果没有传版本号，直接从cache获取,否则对比版本号,如果版本号不相同，则从框架获取
		if(  $version != 0 &&  $version != $etc->getNeighborsVersion()  )
		{
			$result = $this->callGetNeighbors( $userId );
			if(  !empty( $result  )  && isset( $result['version'] ) )
			{
				$etc->setNeighborsVersion(  $result['version']  );
				$etc->setNeighbors(  $result['list']  );
			}
		}
		else 
		{
			$result['list'] = $etc->getNeighbors();
			$result['version'] = $etc->getNeighborsVersion();
		}
		return empty( $result ) ? array() : $result;
	}
	/**
	 * 从框架获取好友列表
	 * @param int $userId 用户ID
	 */
	protected function callGetNeighbors( $userId  )
	{
		static $result = null;
		if( $result === null )
		{
			try
			{
				$result = $this->client->callMethod( 'neighbor.getNeighbors' , array( 'userId' => $userId , 'needVersion' => 1 ) );
			}
			catch ( Exception $e )
			{
				return array();
			}
		}
		return empty( $result ) ? array() : $result;
	}	
	/**
	 * 从框架调用新浪微博排名系统
	 */
	public function achievementSet( $userId , $achvId , $level )
	{
		$achvId = (int)( $achvId- 141000 );
		static $result = null;
		if( $result === null )
		{
			try
			{
				$result = $this->client->callMethod( 'achievement.achievementSet' , array(  'userId' => $userId , 'achvId' => $achvId ) );
			}
			catch ( Exception $e )
			{
				return array();
			}
		}
		return empty( $result ) ? array() : $result;
		
	}
	
	/**
	 * 从框架调用新浪微博排名系统
	 */
	/*public function achievementSet( $userId , $achvId , $level )
	{
		$config = & Common::getConfig( 'platform');
		if( $config != 'weiyouxi' )
		{
			return array();
		}
		$achvId = (int)( $achvId- 141000 );
		static $result = null;
		if( $result === null )
		{
			try
			{
				$result = $this->client->callMethod( 'achievement.achievementSet' , array(  'userId' => $userId , 'achvId' => $achvId ) );
			}
			catch ( Exception $e )
			{
				return array();
			}
		}
		return empty( $result ) ? array() : $result;
		
	}*/
	/**
	 * 设置排名
	 * @param int $rankId
	 * @param int $rankValue
	 */
	public function setLeaderboards( $rankId , $rankValue )
	{
		$config = & Common::getConfig( 'platform');
		if( $config != 'weiyouxi' )
		{
			return array();
		}
		static $result = null;
		if( $result === null )
		{
			try
			{
				$result = $this->client->callMethod( 'achievement.setLeaderboards' , array(  'rankId' => $rankId , 'rankValue' => $rankValue ) );
			}
			catch ( Exception $e )
			{
				return array();
			}
		}
		return empty( $result ) ? array() : $result;
	}
	/*
	 * 排名自增
	 */
	public function incrementLeaderboards( $rankId , $rankValue )
	{
		$config = & Common::getConfig( 'platform');
		if( $config != 'weiyouxi' )
		{
			return array();
		}
		static $result = null;
		if( $result === null )
		{
			try
			{
				$result = $this->client->callMethod( 'achievement.incrementLeaderboards' , array(  'rankId' => $rankId , 'rankValue' => $rankValue ) );
			}
			catch ( Exception $e )
			{
				return array();
			}
		}
		return empty( $result ) ? array() : $result;
		
	}	
	
}

?>