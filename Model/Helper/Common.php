<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class Helper_Common
{
	const CURRENCY_CN2TW = 5;
	
	public static function inPlatform($pfs, $toSearch = "")
	{
		$toSearch = !empty($toSearch) ? $toSearch : Common::getConfig( "platform" );
		$ret = false;
		if(is_array($pfs) && !empty($pfs))
		{
			foreach($pfs as $pf)
			{
				if(strpos($toSearch, $pf) === 0)
				{
					$ret = true;
					break;
				}
			}
		}
		return $ret;
	}
	
	/**
	 *  缓存锁
	 * @param unknown $name 名称
	 * @param number $tryTimes 尝试次数
	 * @param number $time 上锁时间 秒数
	 * @return boolean
	 */
	public static function addLock( $name , $tryTimes = 5 , $expireTime = 10 )
	{
		$cache = Common::getCache();
		$tryTime = 0;
		while( $tryTime++ < $tryTimes )
		{
			if( !!$lock = $cache->add( "lock_".$name , 1 , $expireTime ) ) break; 
			usleep( 10000 );
		}
		
		return $lock;
	}

	public static function delLock( $name )
	{
		$cache = Common::getCache();
		$cache->delete( "lock_".$name );
	}
	
}