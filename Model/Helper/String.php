<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class Helper_String
{

	/**
	 * 返回指定长度的随机字符串
	 * @param unknown_type $length
	 */
	public static function randStr( $length = 8 , $ignoreUpperCase = false ) {  
		// 密码字符集，可任意添加你需要的字符  
		$chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
		if( !$ignoreUpperCase ) $chars .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';  
		$password = '';
		for ( $i = 0; $i < $length; $i++ )  
		{  
			// 这里提供两种字符获取方式  
			// 第一种是使用 substr 截取$chars中的任意一位字符；  
			// 第二种是取字符数组 $chars 的任意元素  
			// $password .= substr($chars, mt_rand(0, strlen($chars) – 1), 1);  
			$password .= $chars[ mt_rand(0, strlen($chars) - 1) ];  
		}  
		return $password;  
	}

	/**
	 * 汉字转Unicode编码
	 * @param string $str 原始汉字的字符串
	 * @param string $encoding 原始汉字的编码
	 * @param boot $ishex 是否为十六进制表示（支持十六进制和十进制）
	 * @param string $prefix 编码后的前缀
	 * @param string $postfix 编码后的后缀
	 */
	//function unicode_encode($str, $encoding = 'UTF-8', $ishex = false, $prefix = '&#', $postfix = ';') {
	public static function unicode_encode($str, $encoding = 'UTF-8', $ishex = true, $prefix = '\\\\u', $postfix = '') {
		$str = iconv($encoding, 'UCS-2', $str);
		$arrstr = str_split($str, 2);
		$unistr = '';
		for($i = 0, $len = count($arrstr); $i < $len; $i++) {
			$dec = $ishex ? bin2hex($arrstr[$i]) : hexdec(bin2hex($arrstr[$i]));
			$unistr .= $prefix . $dec . $postfix;
		}
		return $unistr;
	}
	
	/**
	 * Unicode编码转汉字
	 * @param string $str Unicode编码的字符串
	 * @param string $decoding 原始汉字的编码
	 * @param boot $ishex 是否为十六进制表示（支持十六进制和十进制）
	 * @param string $prefix 编码后的前缀
	 * @param string $postfix 编码后的后缀
	 */
	//function unicode_decode($unistr, $encoding = 'UTF-8', $ishex = false, $prefix = '&#', $postfix = ';') {
	public static function unicode_decode($unistr, $encoding = 'UTF-8', $ishex = true, $prefix = '\\u', $postfix = '') {
		$arruni = explode($prefix, $unistr);
		$unistr = '';
		for($i = 1, $len = count($arruni); $i < $len; $i++) {
			if (strlen($postfix) > 0) {
				$arruni[$i] = substr($arruni[$i], 0, strlen($arruni[$i]) - strlen($postfix));
			}
			$temp = $ishex ? hexdec($arruni[$i]) : intval($arruni[$i]);
			$unistr .= ($temp < 256) ? chr(0) . chr($temp) : chr($temp / 256) . chr($temp % 256);
		}
		return iconv('UCS-2', $encoding, $unistr);
	}
	
}
?>