<?php
/**
 * 点将台
 * 
 * @name PointCard.php
 * @since 2014-1-14
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Activity_PointCard extends Logical_Abstract
{

	/**
	 * 单例对象
	 * @var	Activity_PointCard[]
	 */
	protected static $singletonObjects;

	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Activity_PointCard
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 添加一个点将台武将信息
	 * Enter description here ...
	 * @param unknown_type $cardId
	 */
	public function addPointCardData($cardId)
	{
		$pointCardConf = Common::getConfig("pointCard");
		
		foreach($pointCardConf as $_id => &$_conf)
		{
			//判断时间
			if(!($_SERVER['REQUEST_TIME'] >= strtotime( $_conf['start_time'] )
					&& $_SERVER['REQUEST_TIME'] < strtotime( $_conf['end_time'] )))
				continue;
			$_id += Activity_Model::ACTIVE_POINT_CARD;
			//获得点将信息
			$active = Data_Activity_Model::getInstance( $this->userId )->getActivity($_id);
			//新增武将是否为活动所需武将
			if(empty($_conf['source'][$cardId])) continue;
			
			$active['data']['card'][$cardId]++;
			//添加点将信息
			Data_Activity_Model::getInstance( $this->userId, true )->updateRecord($_id, $active['data']);
		}
	}
	
	/**
	 * 获得点将台武将信息
	 * {"card":{"146_card":1,"145_card":1},"time":1}
	 */
	public function getPointCardData()
	{
		$ret = array();
		$pointCardConf = Common::getConfig("pointCard");
		
		foreach($pointCardConf as $_id => &$_conf)
		{
			//判断时间
			if(!($_SERVER['REQUEST_TIME'] >= strtotime( $_conf['start_time'] )
					&& $_SERVER['REQUEST_TIME'] < strtotime( $_conf['end_time'] )))
				continue;

			if(!!$active = Data_Activity_Model::getInstance( $this->userId )->getActivity($_id + Activity_Model::ACTIVE_POINT_CARD))
			{
				$active['pId'] = $_conf['id'];
				$active['pType'] = $_conf['classesID'];
				
				foreach($active['data']['card'] as $dataId => $dataNum)
				{
					//是否含有当前活动中的卡片
					if(empty($_conf['source'][$dataId])) unset($active['data']['card'][$dataId]);
				}
				
				if(!empty($active['data']['card'])) $ret[] = $active;
			}
		}
		return $ret;
	}
	
	/**
	 * 点将兑换
	 * Enter description here ...
	 * @param unknown_type $actId
	 * @param unknown_type $actType
	 */
	public function pointCard($actId, $actType)
	{
		$ret = array(  'errorCode' => Card_Exception::STATUS_POINT_CARD_FAIL );
		$pointCardConf = Common::getConfig("pointCard");
		
		do
		{
			$id = Activity_PointCard::makePointCardId($actId, $actType);
			if(!$_conf = $pointCardConf[$id]) break;
			$pId = $id + Activity_Model::ACTIVE_POINT_CARD;
	
			//判断时间
			if(!Helper_Date::isInTime($_conf['start_time'], $_conf['end_time']))
				break;
				
			$lockKey = "Activity.pointCard"."_".$this->userId."_".$actId."_".$actType;
			
			//加锁
			if( !$lock = Helper_Common::addLock( $lockKey ) ) break;
				
			if(!$active = Data_Activity_Model::getInstance( $this->userId )->getActivity($pId)) break;
			if(empty($active['data'])) break;
			$activeData = &$active['data'];
			//点将信息
			if(empty($activeData['card'])) break;
			//超过次数
			if($_conf['limit'] > 0 && intval($activeData['time']) > $_conf['limit']) break;
			$bonus = $_conf['target'];
			//是否拥有领取所需武将
			foreach($_conf['source'] as $_id => $_num)
			{
				if(intval($activeData['card'][$_id]) < $_num)
				{
					$bonus = null;
					break;
				}
				$activeData['card'][$_id] -= $_num;
			}
			
			if(!$bonus) break;
			
			//减去点将信息
			$activeData['time']++;
			Data_Activity_Model::getInstance( $this->userId, true )->updateRecord($pId, $activeData);
			
			//减去其他活动的兑换信息
			foreach($pointCardConf as $otherId => &$otherConf)
			{
				if(!Helper_Date::isInTime($otherConf['start_time'], $otherConf['end_time'])) continue;
				if($otherId == $id) continue;
				$otherIdInDB = $otherId + Activity_Model::ACTIVE_POINT_CARD;
				if(!$active = Data_Activity_Model::getInstance( $this->userId )->getActivity($otherIdInDB)) continue;
				if(empty($active['data'])) continue;
				$activeData = &$active['data'];
				
				$needUpdate = false;
				//是否含有当前活动中的卡片
				foreach($_conf['source'] as $_id => $_num)
				{
					if(!empty($activeData['card'][$_id]))
					{
						$needUpdate = true;
						
						$activeData['card'][$_id] =
							$activeData['card'][$_id] > $_num
							? $activeData['card'][$_id] - $_num
							: 0;
					}
				}
				
				if(!$needUpdate) continue;
				
				Data_Activity_Model::getInstance( $this->userId, true )->updateRecord($otherIdInDB, $activeData);
			}
			
			//兑换卡片
			Activity_Model::getInstance($this->userId)->sendPackage(array('card' => $bonus), 'pointCard', '点将台');
			$ret = array('card' => $bonus);
		}
		while(0);

		if($lock) Helper_Common::delLock( $lockKey );
		
		return $ret;
	}
	
	public static function makePointCardId($id, $type)
	{
		return intval($id) * 1000 + intval($type);
	}
	
}
