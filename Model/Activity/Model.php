<?php
/**
 * 活动
 * 
 * @name Model.php
 * @author Liuchangbing
 * @since 2013-1-14
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Activity_Model extends Logical_Abstract
{

	/**
	 * 连续登录
	 * @var unknown
	 */
	const ACTIVE_TYPE_LOGIN_DAYS = 1;
	
	/**
	 * 累计登录
	 * @var unknown
	 */
	const ACTIVE_TYPE_TOTAL_LOGINS = 2;
	
	/**
	 * 充值礼包
	 * @var unknown
	 */
	const ACTIVE_FIRST_RECHARGE  = 3;
	
	/**
	 * 第三方平台信息
	 */
	const ACTIVE_THIRD_INFO = 99;
	
	/**
	 * 神将1+1
	 * @var unknown
	 */
	const ACTIVE_DRAW_CARD_AND_ONE = 100;
	
	/**
	 * 神器1+1
	 * @var unknown
	 */
	const ACTIVE_DRAW_ITEM_AND_ONE = 110;
	
	/**
	 * 活動期間內，強化超成功機率提升2倍
	 * @var unknown
	 */
	const ACTIVE_ENHANCE_WEAPON_RATE_DOUBLE = 101;
	
	/**
	 * 限时首冲翻番
	 * @var unknown
	 */
	const ACTIVE_IN_TIME_FIRST_RECHARGE = 102;
	
	/**
	 * 限时多倍经验
	 * @var unknown
	 */
	const ACTIVE_IN_TIME_BONUS_EXP = 103;
	
	/**
	 * 限时豪饮延时
	 * @var unknown
	 */
	const ACTIVE_IN_TIME_RECOVERY_STAMINA = 104;

	/**
	 * 活動期間內，每日登录
	 * 连续登陆10天 210
	 */
	const ACTIVE_IN_TIME_DAILY_LOGIN = 200;
	
	/**
	 * 手动恢复体力
	 */
	const ACTIVE_MANUAL_RECOVERY_STAMINA = 300;
	
	/**
	 * 第三方平台邀请
	 */
	const ACTIVE_INVITE_THIRD = 301;
	
	/**
	 * 激活码
	 */
	const ACTIVE_CODE = 302;
	
	/**
	 * 点将台,起始id
	 */
	const ACTIVE_POINT_CARD = 100000;
	
	/**
	 * 手动恢复体力时间
	 */
	public static $confRecoveryStamina = array(
		array('start' => 12, 'end' => 13),
		array('start' => 18, 'end' => 19),
	);
	
	public static $confRecoveryStaminaNum = 10;
	
	public static $confActiveCode = array(
		//新浪微博礼包
		array('name' => 'SN', 'start' => '2013-08-23', 'end' => '2014-08-30', 
			'package' => array('gold' => 88888, 'card' => array('201_card' => 2, '186_card' => 2)),
			'platform' => array('GW', '91', 'DL', 'JF', 'UC', 'UUC', 'XM', '91_IOS', 'BR', 'PP', 'vn', 'kr')),
		//論壇禮包
		array('name' => 'SG', 'start' => '2013-06-27', 'end' => '2014-01-01', 
			'package' => array('card' => array('109_card' => 1)),
			'platform' => array('wk')),
		//新手卡（10000个）	
		array('name' => 'RK', 'start' => '2013-12-01', 'end' => '2014-12-01',
			'package' => array('coin' => 28, 'gold' => 50000, 'card' => array('181_card' => 1, '226_card' => 1, '134_card' => 1)),
			'platform' => array('gw_ios')),
		//07073媒体卡（1000个）
		array('name' => '73', 'start' => '2013-12-01', 'end' => '2014-12-01',
			'package' => array('coin' => 38, 'gold' => 80000, 'card' => array('182_card' => 1, '229_card' => 1, '138_card' => 1)),
			'platform' => array('gw_ios')),
		//加Q群活动(500)
		array('name' => 'QQ', 'start' => '2013-12-01', 'end' => '2014-12-01',
			'package' => array('card' => array('346_card' => 2, '171_card' => 1, '230_card' => 1)),
			'platform' => array('gw_ios')),
		//加微信活动(500)
		array('name' => 'WX', 'start' => '2013-12-01', 'end' => '2014-12-01',
			'package' => array('card' => array('347_card' => 2, '172_card' => 1, '231_card' => 1)),
			'platform' => array('gw_ios')),
		//加微博活动(500)
		array('name' => 'WB', 'start' => '2013-12-01', 'end' => '2014-12-01',
			'package' => array('card' => array('348_card' => 2, '173_card' => 1, '232_card' => 1)),
			'platform' => array('gw_ios')),
		//台服android
		array('name' => 'W2', 'start' => '2013-12-01', 'end' => '2014-12-01',
			'package' => array('card' => array('223_card' => 1, '244_card' => 1, '300_card' => 1)),
			'platform' => array('wk')),
		//台服android
		array('name' => 'W3', 'start' => '2013-12-10', 'end' => '2014-05-31',
			'package' => array('gold' => 66666, 'card' => array('291_card' => 1)),
			'platform' => array('wk')),
		//越南内测 20000
		array('name' => 'CB', 'start' => '2013-12-10', 'end' => '2012-12-10',
			'package' => array('gold' => 100000, 'coin' => 5000, 'card' => array('187_card' => 1,
				'181_card' => 1, '184_card' => 1, '295_card' => 1, '301_card' => 1, '298_card' => 1,
				'160_card' => 1, '161_card' => 1, '162_card' => 1, '163_card' => 1, '164_card' => 1)),
			'desc' => 'Giftcode Closed Beta',
			'platform' => array('vn')),
		//越南微博礼包 20000
		array('name' => 'V1', 'start' => '2013-12-10', 'end' => '2014-12-10',
			'package' => array('coin' => 10, 'card' => array('14_card' => 1, '295_card' => 1, '298_card' => 1, '301_card' => 1)),
			'desc' => 'Giftcode Thục',
			'platform' => array('vn')),
		//越南论坛礼包 20000
		array('name' => 'V2', 'start' => '2013-12-10', 'end' => '2014-12-10',
			'package' => array('gold' => 10000, 'card' => array('92_card' => 1,
				'160_card' => 1, '161_card' => 1, '162_card' => 1, '163_card' => 1, '164_card' => 1)),
			'desc' => 'Giftcode Ngô',
			'platform' => array('vn')),
		//越南媒体礼包 20000
		array('name' => 'V3', 'start' => '2013-12-10', 'end' => '2014-12-10',
			'package' => array('coin' => 10, 'card' => array('113_card' => 1, '114_card' => 1, '115_card' => 1)),
			'desc' => 'Giftcode Ngụy',
			'platform' => array('vn')),
		//越南5k礼包 2000
		array('name' => 'K1', 'start' => '2013-12-10', 'end' => '2014-12-10',
			'package' => array('gold' => 5000, 'coin' => 10, 'card' => array('116_card' => 1,
				'181_card' => 1, '184_card' => 1, '157_card' => 1)),
			'desc' => 'Giftcode01',
			'platform' => array('vn')),
		//越南10k礼包 2000
		array('name' => 'K2', 'start' => '2013-12-10', 'end' => '2014-12-10',
			'package' => array('gold' => 10000, 'coin' => 10, 'card' => array('18_card' => 1,
				'295_card' => 1, '298_card' => 1, '301_card' => 1, '158_card' => 1)),
			'desc' => 'Giftcode02',
			'platform' => array('vn')),
		//越南20k礼包 2000
		array('name' => 'K3', 'start' => '2013-12-10', 'end' => '2014-12-10',
			'package' => array('gold' => 20000, 'coin' => 20, 'card' => array('39_card' => 1, '182_card' => 1, '185_card' => 1)),
			'desc' => 'Giftcode03',
			'platform' => array('vn')),
		//越南推广礼包(可以被多人使用, 每人只能用一次)
		array('name' => 'KI', 'start' => '2013-12-10', 'end' => '2014-12-10',
			'package' => array('gold' => 10000, 'coin' => 15),
			'desc' => 'Giftcode04',
			'platform' => array('vn'),
			'multi' => true,),
		//韩国 3000个激活码 （kakao android的），统一配备｛5星吕布（195_card) + 30元宝｝
		array('name' => 'K1', 'start' => '2013-12-19', 'end' => '2014-12-19',
			'package' => array('coin' => 30, 'card' => array('195_card' => 1)),
			'platform' => array('krkk')),
	);
	
	/**
	 * 累计消费
	 */
	const PAID_TYPE_SUM_CONSUME = 1;
	
	/**
	 * 首充
	 */
	const PAID_TYPE_FIRST_PAY = 2;
	
	/**
	 * 累计充值
	 */
	const PAID_TYPE_SUM_PAY = 3;
	 
	/**
	 * 单笔充值
	 */
	const PAID_TYPE_SINGLE_PAY = 4;
	
	/**
	 * 单例对象
	 * @var	Activity_Model[]
	 */
	protected static $singletonObjects;

	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Activity_Model
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	
	/**
	 * 消费记录
	 * @param unknown $coin
	 */
	public function consume( $coin , $type = 0 )
	{
		if( empty( $type ) ) $type = Activity_Model::PAID_TYPE_SUM_CONSUME;
		$paidConfig = Common::getConfig( "paid" );
		$paidConfig = $paidConfig[$type];
		if( empty( $paidConfig ) ) return false;
		
		//$maxRecordCoin = 0;
		
		foreach ( $paidConfig as $actId => $actCnf )
		{
			$startTime = strtotime( $actCnf['startTime']);
			$endTime = strtotime( $actCnf['endTime'] );
			if( $_SERVER['REQUEST_TIME'] >= $startTime && $_SERVER['REQUEST_TIME'] <= $endTime )
			{
				switch ( $type ) {
					case Activity_Model::PAID_TYPE_SUM_CONSUME:
					case Activity_Model::PAID_TYPE_SUM_PAY:
						Data_Activity_Consume::getInstance( $this->userId , true )->addRecord( $actId , $coin );
					break;

					case Activity_Model::PAID_TYPE_SINGLE_PAY:
						//判断符合活动条件
//						if( $coin >= $actCnf['paidCoin'] )
//						{
//							//判断能领取的最大面额
//							if( $coin > $maxRecordCoin )
//							{
//								$maxRecordCoin = $coin;
//								$maxRecordConfig = $actCnf;
//							}
//						}
						if( $coin == $actCnf['paidCoin'] ) $maxRecordConfig = $actCnf;
					break;
					
					case Activity_Model::PAID_TYPE_FIRST_PAY:
					break;
				}
			}
		}
		
		//单笔充值只算符合条件面额最大的一笔
		if( isset( $maxRecordConfig ) )
		{
			Data_Activity_Consume::getInstance( $this->userId , true )->addRecord( $maxRecordConfig['id'] , $maxRecordConfig['paidCoin'] , true );			
		}
	}
	
	/**
	 * 消费领奖活动
	 * @param unknown $actId
	 */
	public function consumeReward( $actId )
	{
		$paidConfig = Common::getConfig( "paid" );
//		$paidConfig = $paidConfig[Activity_Model::PAID_TYPE_SUM_CONSUME];
//		$paidConf = $paidConfig[$actId];
		$paidConf = self::getPaidActivityConfById( $actId );
		$paidConfig = $paidConfig[$paidConf['type']];
		
		//找不到活动配置
		if( empty( $paidConf ) ) throw new User_Exception( User_Exception::STATUS_CANNOT_GET_REWARD );
		
		$startTime = strtotime( $paidConf['startTime']);
		$endTime = strtotime( $paidConf['endTime'] );
		if( $_SERVER['REQUEST_TIME'] > $endTime && $_SERVER['REQUEST_TIME'] < $startTime )
			throw new User_Exception( User_Exception::STATUS_NOT_IN_ACTIVITY_TIME );
		
		$actInfo = Data_Activity_Consume::getInstance( $this->userId )->getActivityInfo( $actId );
		
		switch ( $paidConf['type'] )
		{
			case Activity_Model::PAID_TYPE_SUM_CONSUME:
			case Activity_Model::PAID_TYPE_SUM_PAY:
				//是否积分足够
				if( $actInfo['coin'] < $paidConf['paidCoin'] )
					throw new User_Exception( User_Exception::STATUS_CANNOT_GET_REWARD );
				
				//扣除对应活动期间内累计的消费点数 , 且可以重复领取
				if(1)
				{
					foreach( $paidConfig as $conf )
					{
						if( $_SERVER['REQUEST_TIME'] < strtotime( $conf['endTime'] ) && $_SERVER['REQUEST_TIME'] >= strtotime( $conf['startTime'] ) )
						{
							Data_Activity_Consume::getInstance( $this->userId , true )->addRecord( $conf['id'] , -$paidConf['paidCoin'] );
						}
					}
				}
				else
				{
					//是否领取过
					if( !Data_Activity_Consume::getInstance( $this->userId , true )->reward( $actId ) )
						throw new User_Exception( User_Exception::STATUS_CANNOT_GET_REWARD );
				}
			break;

			case Activity_Model::PAID_TYPE_SINGLE_PAY:
				//积分是否足够				
				if( !Data_Activity_Consume::getInstance( $this->userId , true )->reward( $actId , true ) )
					throw new User_Exception( User_Exception::STATUS_CANNOT_GET_REWARD );
			break;
			
			case Activity_Model::PAID_TYPE_FIRST_PAY:
			break;
			
			default:
				throw new User_Exception( User_Exception::STATUS_CANNOT_GET_REWARD );
			break;
		}
		
		
		//发送奖励
		for ( $i = 0 ; $i <  $paidConf['prizeCardNum'] ; $i++ )
		{
			Card_Model::getInstance( $this->userId )->addCard( $paidConf['prizeCard'] , 1, true , "activity" );
		}
		
	}
	
	/**
	 * 领取奖励
	 * @param unknown $id
	 */
	public function rewardLoginActivity( $id )
	{
		$actInfo = Data_Activity_Model::getInstance( $this->userId )->getActivity( $id );
		
		if( !empty( $actInfo ) && date( "Y-m-d" , $actInfo['rewardTime'] ) ==  date( "Y-m-d" , $_SERVER['REQUEST_TIME'] ) )
		{
			return false;
		}
		
		Data_Activity_Model::getInstance( $this->userId , true )->updateRecord( $id );
		return true;
	}
	
	/**
	 * 批量生成激活码
	 */
	public static  function makeActiveCode( $platform , $num )
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
		
		$cache = Common::getCache();
		$codeSqlList = array();
	
		$index = 0;
		while( $index < $num )
		{
			$newCode = $platform.Helper_String::randStr(8, true);
			if( $cache->get( 'activeCode_'.$newCode  )  )
			{
				continue;
			}
			$index++;
			
			$newCodeInfo = array(
					'id' => $newCode,
					'status' => 0,
					'uid' => 0,
			);
			$cache->set(  'activeCode_'.$newCode , $newCodeInfo , 600 );
			$codeSqlList[$newCode] = "insert into active_code(  id , status , uid  )values( '{$newCode}' , 0 , 0  )";
		}
		
		$dbEngine = Common::getDB( 1 );
		
		foreach( $codeSqlList as $sql )
		{
			$dbEngine->query( array( $sql ) );
		}
		return $index;
	}
	
	
	public function useActiveCode( $code )
	{
		$ret = false;
		do
		{
			$lockKeyCode = "Activity.useActiveCode"."_code_".$code;
			$lockKeyUser = "Activity.useActiveCode"."_user_".$this->userId;
			//加锁
			if( !$lockCode = Helper_Common::addLock( $lockKeyCode ) ) break;
			if( !$lockUser = Helper_Common::addLock( $lockKeyUser ) ) break;
			
			foreach(self::$confActiveCode as $_conf)
			{
				if(!Helper_Common::inPlatform($_conf['platform'])) continue;
					
				if($_conf['start'] && !(
						$_SERVER['REQUEST_TIME'] >= strtotime($_conf['start'])
						&& $_SERVER['REQUEST_TIME'] < strtotime($_conf['end'])
				)) continue;
					
				if($_conf['name'])
				{
					if(strpos( $code , $_conf['name'] ) !== 0) continue;
					
					$dbEngine = Common::getDB( 1 );
					$data = $dbEngine->findQuery("select * from active_code where id='{$code}'");
					if(!$data[0][0]) break;
					
					if(empty($_conf['multi']))
					{
						$status = $data[0][1];
						$userId = $data[0][2];
						
						//1.1 每个序列号只能使用一次。
						if($status == 1 || $userId > 0)
						{
							$ret = array('errorCode' => User_Exception::STATUS_ACTIVE_CODE_USED);
							break;
						}
						//1.2 同类型序列号一个人只能使用一次
						$data = $dbEngine->findQuery("select count(1) from active_code where uid={$this->userId} and instr(`id`,'{$_conf['name']}')=1");
						if($data[0][0] > 0)
						{
							$ret = array('errorCode' => User_Exception::STATUS_ACTIVE_CODE_ONE_TIME_FOR_ONE_ACCOUNT);
							break;
						}
					}
					else
					{
						//2.1 同一序列号一个人只能使用一次
						$active = Data_Activity_Model::getInstance( $this->userId )->getActivity(self::ACTIVE_CODE);
						if(!empty($active['data'][$code]))
						{
							$ret = array('errorCode' => User_Exception::STATUS_ACTIVE_CODE_ONE_TIME_FOR_ONE_ACCOUNT);
							break;
						}
						$active['data'][$code] = $_SERVER['REQUEST_TIME'];
					}
				}
			
				$package = $_conf['package'];
				$desc = $_conf['desc'];
				$isMulti = !empty($_conf['multi']);
				if(!empty($package['card']))
				{
					foreach($package['card'] as $_id => $_num)
					{
						$cardCode = $_id;
						break;
					}
				}
				if(!empty($package)) break;
			}
			
			if(empty($package)) break;
			
			if(!$isMulti)
			{
				$sql = "update active_code set status=1,uid=".$this->userId." where id ='{$code}'";
				if(!$dbEngine->query(array($sql))) break;
			}
			else
			{
				Data_Activity_Model::getInstance( $this->userId, true )->updateRecord(self::ACTIVE_CODE, $active['data']);
			}
			
			$this->sendPackage($package, "activeCode", "激活码");
			
			if(empty($_REQUEST['multi']))
			{
				$cardConfig = Common::getConfig( "card" );
				$cardName = $cardConfig[$cardCode]['name'];
					
				$reward = array(
						'cardId' =>  $cardCode,
						'cardName' => $cardName,
						'level' => 1,
				);
					
				$reward['coin'] = isset( $package['coin'] ) ? $package['coin'] : 0;
				$reward['gold'] = isset( $package['gold'] ) ? $package['gold'] : 0;
				$ret = $reward;
			}
			else
			{
				$ret = $package;
			}
			
			$ret['packDesc'] = strval($desc);
			
		}while(0);
		
		if($lockUser) Helper_Common::delLock( $lockKeyUser );
		if($lockCode) Helper_Common::delLock( $lockKeyCode );
		
		if($ret['errorCode']) throw new User_Exception($ret['errorCode']);
		
		return $ret;
	}

	
	public function getActivityConsumeInfo( $actId )
	{
		return Data_Activity_Consume::getInstance( $this->userId )->getActivityInfo( $actId );
	}
	
	
	/**
	 * 获取首冲奖励
	 */
	public function getFirstChargeReward()
	{
		$ret = array();
		//是否领过
		$actInfo = Data_Activity_Model::getInstance( $this->userId )->getActivity( self::ACTIVE_FIRST_RECHARGE );

		if(  $actInfo['rewardTime'] > 0  )
		{
			throw new User_Exception( User_Exception::STATUS_CANNOT_GET_REWARD );
		}
		
		//是否有领取资格
		$totalRecharge = Data_Order_Model::getInstance( $this->userId )->getTotalRecharge();
		if( $totalRecharge <= 0 ) throw new User_Exception( User_Exception::STATUS_CANNOT_GET_REWARD );
		
		$serverTime = $_SERVER['REQUEST_TIME'];
		
		$paidConfig = Common::getConfig( "paid" );
		$paidConfig = $paidConfig[Activity_Model::PAID_TYPE_FIRST_PAY];
		foreach( $paidConfig as $conf )
		{
			if(   $serverTime >= strtotime( $conf['startTime'] ) &&   $serverTime < strtotime( $conf['endTime'] ) )
			{
				$package['card'][trim($conf['prizeCard'])] = $conf['prizeCardNum'];
			}	
		}
		
		if(isset($package)) $this->sendPackage($package, "firstRecharge", "首冲礼包");
		
		Data_Activity_Model::getInstance( $this->userId , true  )->updateRecord( self::ACTIVE_FIRST_RECHARGE );
		
		$ret = array();
			
		return $ret;
	}
	
	/**
	 * 各种登陆奖励：每日首登，累计登陆，连续登陆
	 */
	public function firstLogin()
	{
		//if( !Helper_Common::inPlatform( array( "PP" , "wk" , "UUC" ) ) ) return array();
		$rewardTotal = array();
		$cache = Common::getCache();
		
		$userProfile = Data_User_Profile::getInstance( $this->userId )->getData();
		$userInfo = Data_User_Info::getInstance( $this->userId )->getData();
		
		if( !!$reward = self::isInTime( self::ACTIVE_IN_TIME_DAILY_LOGIN ) )
		{
			$finalReward = array();
			//限时连续登陆
			if( isset( $reward['login'] ) )
			{
				$startTime = $reward['login']['start'];
				unset($reward['login']['start']);
				foreach( $reward['login'] as $days => $lastsLoginReward )
				{
					$endTime = strtotime( $startTime ) + $days * 86400;
					//连续登陆超过指定天数，只有结束那天才能领
					if( $userProfile['loginDays'] >= $days && date( "Y-m-d" , $endTime - 1 ) == date( "Y-m-d" , $_SERVER['REQUEST_TIME'] ) )
					{
						if( !!$status = Activity_Model::getInstance( $this->userId )->rewardLoginActivity( self::ACTIVE_IN_TIME_DAILY_LOGIN + $days ) )
						{
							self::addData( $reward['login'][$days] , $finalReward , $userInfo );
						}
					}
					if( empty( $status ) )
					{
						unset( $reward['login'][$days] );
						unset( $status );
					}
				}
			}
			//每日首次登陆
			if( !!$firstLogin = $reward['first'] )
			{
				self::addData( $firstLogin , $finalReward , $userInfo );
			}
			
			if( !empty( $finalReward ) ) {
				$this->sendReward( $finalReward , self::ACTIVE_IN_TIME_DAILY_LOGIN );
			}
			
			$rewardTotal[self::ACTIVE_IN_TIME_DAILY_LOGIN] = $reward;
		}
		//连续登陆
		if( !!$reward = $this->getLoginReward( self::ACTIVE_TYPE_LOGIN_DAYS ) )
		{
			$rewardTotal[self::ACTIVE_TYPE_LOGIN_DAYS] = $reward;
		}
		
		//累计登录
		if( !!$reward = $this->getLoginReward( self::ACTIVE_TYPE_TOTAL_LOGINS ) ) 
		{
			$rewardTotal[self::ACTIVE_TYPE_TOTAL_LOGINS] = $reward;
		}
		
		//存入缓存，公告读取用
		if( !empty( $rewardTotal ) )
		{
			$cache->set( "active_reward_".$this->userId , $rewardTotal , Helper_Date::getTodayLeftSeconds() );
		}
		return $rewardTotal;
	}
	
	public function getData()
	{
		return Data_Activity_Model::getInstance( $this->userId )->getData();
	}
	
	/**
	 * 是否在活动期间内,
	 * @param unknown $activeId
	 * @return boolean|array 活动奖励
	 */
	public static function isInTime( $activeId )
	{
		$ret = false;
		$activityConfig = Common::getConfig( "activity" );
		
		if( !empty( $activityConfig) )
		{
			switch ( $activeId ) {
				case self::ACTIVE_IN_TIME_DAILY_LOGIN;
					$configs = self::getInTimeActivityConf( $activeId );
					if( !empty( $configs ) )
					{
						foreach( $configs as $conf )
						{
							//天天登入送元寶 登入送援軍點、銅錢
							if( empty( $conf['subType'] ) )
							{
								$ret['first']['coin'] += $conf['coin'];
								$ret['first']['gold'] += $conf['gold'];
								$ret['first']['gachaPoint'] += $conf['gachaPoint'];
							}
							//连续登入 同时间段只能有一个
							else 
							{
								//$ret["login"] = array( 10 => array( "coin" => 500 ) , "start" => $startTime );
								$ret["login"][$conf['subType']]['coin'] = $conf['coin'];
								$ret["login"][$conf['subType']]['gold'] = $conf['gold'];
								$ret["login"][$conf['subType']]['gachaPoint'] = $conf['gachaPoint'];
								$ret["login"]['start'] = $conf['start_time'];
							}
						}
					}
					break;
				default:
					$configs = self::getInTimeActivityConf( $activeId );
					if( !empty( $configs ) ) $ret = $configs;
					break;
			}
		}
		return $ret;
	}
	
	public function manualRecoveryStamina()
	{
		$ret = false;
		$dayTime = strtotime(date('Y-m-d',$_SERVER['REQUEST_TIME']));
		$startTime = 0;
		
		$confRecoveryStamina = self::getConfRecoveryStamina();
		
		foreach($confRecoveryStamina as $conf)
		{
			if($_SERVER['REQUEST_TIME'] >= $dayTime + $conf['start'] * 3600
				&& $_SERVER['REQUEST_TIME'] < $dayTime + $conf['end'] * 3600)
			{
				$startTime = $dayTime + $conf['start'] * 3600;
				break;
			}
		}
		
		do
		{
			if($startTime == 0) break;
			$actInfo = Data_Activity_Model::getInstance($this->userId)->getActivity(Activity_Model::ACTIVE_MANUAL_RECOVERY_STAMINA);
			//已经领取过
			if(!empty($actInfo) && $actInfo['rewardTime'] > $startTime) break;
			//增加体力
			Data_User_Info::getInstance( $this->userId , true )->changeStamina( self::$confRecoveryStaminaNum );
			//添加领取记录
			Data_Activity_Model::getInstance($this->userId, true)->updateRecord(Activity_Model::ACTIVE_MANUAL_RECOVERY_STAMINA);
			$ret = true;
		}
		while(0);
		
		return $ret;
	}
	
	public static function & getConfRecoveryStamina()
	{
		if(!!$conf = self::isInTime( self::ACTIVE_IN_TIME_RECOVERY_STAMINA ))
		{
			foreach($conf as $_conf)
			{
				self::$confRecoveryStamina = json_decode($_conf['custom'], true);
				break;
			}
		
		}
		return self::$confRecoveryStamina;
	}
	
	public function sendPackage($package, $logType, $evt)
	{
		if(isset($package['card']))
		{
			foreach($package['card'] as $_cardId => $_cardNum)
			{
				for($_i = 0; $_i < $_cardNum; $_i++)
				{
					Card_Model::getInstance( $this->userId )->addCard( $_cardId , 1 , true , $logType );
				}
			}
		}
		
		if(isset($package['coin']))
		{
			User_Info::getInstance( $this->userId )->changeCoin( $package['coin'] , $evt );
		}
		
		if(isset($package['gold']))
		{
			User_Info::getInstance( $this->userId )->changeGold( $package['gold'] , $evt );
		}
		
		if(isset($package['gachaPoint']))
		{
			User_Info::getInstance( $this->userId )->changeGachaPoint($package['gachaPoint']);
		}
	}

	//private
	private function sendReward( $reward , $subType )
	{
		if( $reward['coin'] > 0 )
		{
			$evt = ( $subType == 1 ) ? '连续登录' : '累计登录';
			User_Info::getInstance( $this->userId )->changeCoin( $reward['coin'] , $evt , 1 , $subType );
		}

		if( $reward['gold'] > 0 )
		{
			$evt = ( $subType == 1 ) ? '连续登录' : '累计登录';
			User_Info::getInstance( $this->userId )->changeGold( $reward['gold'] , $evt );
		}

		if( $reward['gachaPoint'] > 0 )
		{
			User_Info::getInstance( $this->userId )->changeGachaPoint( $reward['gachaPoint']  );
		}
	}
	
	private function getLoginReward( $type )
	{
		//$type = (int)$this->get['type'];
		
		//是否已经领过
		$status = Activity_Model::getInstance( $this->userId )->rewardLoginActivity( $type );
		if( $status == false ) return false;
		
		$reward = array(
				'prizeCoin' => 0,
				'prizeGold' => 0,
				'prizePoint' => 0,
		);
		
		$userProfile = User_Profile::getInstance( $this->userId )->getData();
		//连续登录奖励
		if( $type == self::ACTIVE_TYPE_LOGIN_DAYS )
		{
			//连续7天登录
			$days = $userProfile['loginDays'];
			$config = Common::getConfig( "loginDays" );
				
			$days = $days == 0 ? 1 : $days;
			$days = ( $days > 7 ) ? 7 : $days;
			$reward =  $config[$days];
		}
		//累计登录
		else if( $type == self::ACTIVE_TYPE_TOTAL_LOGINS )
		{
			//累计登录
			$days = $userProfile['totalLogins'];
			$config = Common::getConfig( "consecutiveLogin" );
	
			foreach ( $config as $eachDays => $eachConf )
			{
				//累计到那一天才给奖励
				if( $days == $eachDays )
				{
					$reward = $eachConf;
					break;
				}
			}
		}

		$finalReward = array(
			'coin' => floatval( $reward['prizeCoin'] ),
			'gold' => floatval( $reward['prizeGold'] ),
			'gachaPoint' => floatval( $reward['prizePoint'] ),
		);
		$this->sendReward( $finalReward , $type );
		
		$returnData = $finalReward;
		return $returnData;
	}
	
	/**
	 * 根据奖励设置需要增加的属性
	 */
	private static function addData( $source , &$target , $in_cols = null )
	{
		foreach( $source as $col => $val )
		{
			$setFlag = true;
			if( $in_cols && !isset( $in_cols[$col] ) )
			{
				$setFlag = false;
			}
			if( $setFlag ) $target[$col] = floatval( $target[$col] ) + floatval( $val );
		}
	}
	
	private static function getInTimeActivityConf( $type )
	{
		$ret = array();
		
		$configs = Common::getConfig( "activity" );
		$configs = $configs[$type];
		
		if( !empty( $configs ) )
		{
			foreach( $configs as $key => $config )
			{
				if(Helper_Date::isInTime($config['start_time'], $config['end_time'], null, $config['pattern']))
				{
					$ret[$key] = $config;
				}
			}
		}
		
		return $ret;
	}
	
	private static function getPaidActivityConfById( $id )
	{
		$ret = array();
		
		$allConfigs = Common::getConfig( "paid" );
		foreach( $allConfigs as $type => &$configs )
		{
			foreach( $configs as $confId => &$conf )
			{
				if( $confId == $id ){
					$ret = $conf;
					break;
				}
			}
			if( !empty( $ret ) ) break;
		}
		return $ret;
	}
	
}
