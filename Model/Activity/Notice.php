<?php
/**
 * 公告
 * 
 * @name Notice.php
 * @author yanghan
 * @since 2013-12-30
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Activity_Notice extends Logical_Abstract
{

	/**
	 * 单例对象
	 * @var	Activity_Notice[]
	 */
	protected static $singletonObjects;

	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Activity_Notice
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	public function getNotice()
	{
		$ret = array();
		$cache = Common::getCache();
		$cacheKey = 'lastNoticeTime_'.$this->userId;
		$lastNoticeTime = $cache->get($cacheKey);
		
		$noticeConf = Common::getConfig('notice');
		if(!empty($noticeConf))
		{
			foreach($noticeConf as $id => $conf)
			{
				if(empty($conf['enable'])) continue;
				if(!Helper_Date::isInTime($conf['start_time'], $conf['end_time'])) continue;
				if($conf['interval'] < 5) continue;
				
				$startTime = strtotime($conf['start_time']);
				$interval = 60 * $conf['interval'];
				$cnt = intval(($_SERVER['REQUEST_TIME'] - $startTime) / $interval);
				if(!empty($lastNoticeTime)
					&& $lastNoticeTime >= $startTime + $cnt * $interval) continue;
				
				$ret[] = Card_Model::getInstance($this->userId)->pushWordMsg($conf['msg'], true);
			}
			
			if(!empty($ret)) $cache->set($cacheKey, $_SERVER['REQUEST_TIME']);
		}
		
		return $ret;
	}
	
}
