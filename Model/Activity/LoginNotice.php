<?php
/**
 * 公告
 * 
 * @name LoginNotice.php
 * @author yanghan
 * @since 2014-01-21
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Activity_LoginNotice extends Logical_Abstract
{

	/**
	 * 单例对象
	 * @var	Activity_LoginNotice[]
	 */
	protected static $singletonObjects;

	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Activity_LoginNotice
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	public function getLoginNotice($showType)
	{
		$notice = array();
		$cache = Common::getCache();
		$dailyLoginKey = "active_reward_".$this->userId;
	
		//$dailyLoginReward = Activity_Model::getInstance($this->userId)->firstLogin();
		//$dailyLoginReward = json_decode('{"1":{"coin":0,"gold":0,"gachaPoint":200},"2":{"coin":0,"gold":0,"gachaPoint":0},"200":{"first":{"coin":1234,"gold":56789,"gachaPoint":7000},"login":{"2":{"coin":321,"gold":6532,"gachaPoint":200}}}}', true);
		$dailyLoginReward = $cache->get( $dailyLoginKey );
		if( !empty( $dailyLoginReward ) )
		{
			foreach( $dailyLoginReward as $type => $reward )
			{
				$notice[] = $this->getNotice($showType, $type, $reward);
			}
	
			$cache->delete( $dailyLoginKey );
		}
		return $notice;
	}
	
	private function getImgUrl()
	{
		return Common::getServerAddr().'Web/Api/image/';
	}
	
	private function getNotice( $showType, $type , $data )
	{
		$userProfile = User_Profile::getInstance( $this->userId )->getData();
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		
		if( $type == Activity_Model::ACTIVE_TYPE_LOGIN_DAYS )
		{
			$title = MakeCommand_LangPack::get( "activeLoginDaysTitle" );
			$ret['type'] = 4;
			$content = self::getNoticeRewardLogins($showType, $userProfile['loginDays'], 'loginDays', 'activeLoginDaysHead');
		}
		elseif( $type == Activity_Model::ACTIVE_TYPE_TOTAL_LOGINS )
		{
			$title = MakeCommand_LangPack::get( "activeTotalLoginsTitle" );
			$ret['type'] = 4;
			$content = self::getNoticeRewardLogins($showType, $userProfile['totalLogins'], 'consecutiveLogin', 'activeTotalLoginsHead');
		}
		elseif( $type == Activity_Model::ACTIVE_IN_TIME_DAILY_LOGIN )
		{
			$title = MakeCommand_LangPack::get( "activeInTimeDailyLoginTitle" );
			$ret['type'] = 5;
			$content = self::getNoticeRewardActiveDailyLogin($showType, $data, $userProfile['loginDays']);
		}
		
		$ret['title'] = $title;
		if($showType == "json")
		{
			$ret['data'] = $content;
		}
		else
		{
			$colorContent = "#550101";
			$ret['type'] = 3;
			$ret['data'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
.body
{
	overflow:hidden;
	height:430px;
	width:470px;
}
		
.container
{
	height:auto;
	width:auto;
	font-size:16px;
	font-family:Arial, Helvetica, sans-serif;
	color:white;
}
</style>
</head>
<body class="body">
<div class="container" style="color:'.$colorContent.';">
'.$content.'
</div>
</body>
</html>';
		}
		
		return $ret;
	}
	
	/**
	 * 获得登陆奖励提示
	 * @param string $showType
	 * @param unknown $loginDays 登陆天数
	 * @param unknown $configName 配置表 consecutiveLogin
	 * @param unknown $headName 标题文字 activeTotalLoginsHead
	 * @return string
	 */
	private static function getNoticeRewardLogins($showType, $loginDays, $configName, $headName)
	{
		$totalLoginConfig = Common::getConfig($configName);
	
		$confKeys = array_keys($totalLoginConfig);
		$idx = -1;
		foreach($confKeys as $i => $confKey)
		{
			if(intval($confKey) <= intval($loginDays))
			{
				$idx = $i;
			}
		}
		$idxArr = $idx < 1 ? array(0,1,2) : array($idx - 1, $idx, empty($confKeys[$idx+1]) ? $idx : $idx + 1);
		
		$content['head'] = MakeCommand_LangPack::get($headName, "", array('', $loginDays));
		$j = 0;
		foreach($idxArr as $i)
		{
			if(empty($confKeys[$i])) break;
				
			$conf = $totalLoginConfig[$confKeys[$i]];
			$content['content'][] = array(
				'title' => MakeCommand_LangPack::get($headName, "", array('\n', $confKeys[$i])),
				'days' => intval($confKeys[$i]),
				'coin' => intval($conf['prizeCoin']),
				'gold' => intval($conf['prizeGold']),
				'gachaPoint' => intval($conf['prizePoint']),
				'selected' => ($idx >= $i && $j != 3),
			);
		}
		
		if($showType == "json")
		{
			$ret = $content;
		}
		else
		{
			$imgUrl = self::getImgUrl();
			
			$ret = ''.
			'<div style="clear:both;font-weight:bold;">
'.$content['head'].
			'</div>
<div style="clear:both;color:#08f57b;font-weight:bold;">';
			$j = 0;
			foreach($content['content'] as $bonus)
			{
				$bonus['title'] = str_replace('\n', '<br/>', $bonus['title']);
				
				$ret .= ''.
						'<div  style="position:absolute;top:'.(30 + 130 * $j++).'px;">
	<div style="position:absolute;"><img src="'.$imgUrl.'bg.png"/></div>
	<div style="position:absolute;"><img src="'.$imgUrl.'bg_mask.png"/></div>
	<div style="position:absolute;padding-top:40px;padding-left:2px;width:120px;height:80px;color:#f38906;text-align:center;">
		'.$bonus['title'].'&nbsp;
	</div>
	<div style="position:absolute;left:120px;padding-top: 10px;">
		<div><img src="'.$imgUrl.'coin.png"/></div>
		<div style="text-align:center;margin-top:-6px;">x&nbsp;'.$bonus['coin'].'</div>
	</div>
	<div style="position:absolute;left:230px;padding-top: 10px;">
		<div><img src="'.$imgUrl.'gold.png"/></div>
		<div style="text-align:center;margin-top:-6px;">x&nbsp;'.$bonus['gold'].'</div>
	</div>
	<div style="position:absolute;left:340px;padding-top: 10px;">
		<div><img src="'.$imgUrl.'gachaPoint.png"/></div>
		<div style="text-align:center;margin-top:-6px;">x&nbsp;'.$bonus['gachaPoint'].'</div>
	</div>'.
				(
						$bonus['selected'] ?
						'<div style="position:absolute;left:400px;padding-top: 80px;">
	<img src="'.$imgUrl.'sign.png"/>
	</div>' : ''
				).
				'</div>';
			}
			
			$ret .= '</div>';
		}
		
		return $ret;
	}
	
	private static function getNoticeRewardActiveDailyLogin($showType, $data, $loginDays)
	{
		$content['first'] = self::getNoticeRewardActive($showType, $data['first'],
					MakeCommand_LangPack::get( "activeInTimeDailyLoginHead1" ));
		$content['login'] = self::getNoticeRewardActive($showType, $data['login'][$loginDays],
					MakeCommand_LangPack::get( "activeInTimeDailyLoginHead2" , "" , array( $loginDays ) ));
		
		if($showType == "json")
		{
			$ret = $content;
		}
		else
		{
			$ret = '';
			$ret .= $content['first'];
			$ret .= $content['login'];
		}
		
		return $ret;
	}
	
	/**
	 * 获得活动奖励提示
	 * @param string $showType
	 * @param unknown $conf 奖励配置
	 * @param unknown $headName 标题
	 * @return string
	 */
	private static function getNoticeRewardActive($showType, $conf, $headName)
	{
		$content['head'] = $headName;
	
		$isConfEmpty = true;
		foreach($conf as $_conf)
		{
			if(!empty($_conf))
			{
				$isConfEmpty = false;
				break;
			}
		}
		
		$content['coin'] = intval($conf['coin']);
		$content['gold'] = intval($conf['gold']);
		$content['gachaPoint'] = intval($conf['gachaPoint']);
		
	
		if($showType == "json")
		{
			$ret = $content;
		}
		else
		{
			$imgUrl = self::getImgUrl();
			
			$ret = '<div style="clear:both;font-weight:bold;">'.$content['head'].'</div>';
			
			if(!$isConfEmpty)
			{
				$ret .= ''
.'<div style="clear:both;color:#08f57b;font-weight:bold;">
	<div style="float:left;padding-right:2px;">
		<div><img src="'.$imgUrl.'coin.png" /></div>
		<div style="text-align:center;">x&nbsp;'.$content['coin'].'</div>
	</div>
	<div style="float:left;padding-right:2px;">
		<div><img src="'.$imgUrl.'gold.png" /></div>
		<div style="text-align:center;">x&nbsp;'.$content['gold'].'</div>
	</div>
	<div style="float:left;padding-right:2px;">
		<div><img src="'.$imgUrl.'gachaPoint.png" /></div>
		<div style="text-align:center;">x&nbsp;'.$content['gachaPoint'].'</div>
	</div>
</div>';
			}
			
		}
		
		
		return $ret;
	}
	
}
