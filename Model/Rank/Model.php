<?php
/**
 *  排行榜
 * @name Model.php
 * @author yanghan
 * @since 2013-05-30
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Rank_Model extends Logical_Abstract
{
	/**
	 * 单例对象
	 * @var	Rank_Model[]
	 */
	protected static $singletonObjects;

	const CACHE_KEY_RANK = "rank_";
	
	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Rank_Model
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 获得排行榜
	 * @param string $type 类型 gold:财富; level:等级
	 * @param int $num 返回数量
	 * @return array
	 * */
	public function getRank( $type , $num = 10 )
	{
		$cache = Common::getCache();
		if( !$rank = $cache->get( self::CACHE_KEY_RANK.$type ) )
		{
			if( $type == "gold" )
			{
				$sql = "select `uid` from `user_info` order by `gold` desc,`exp` desc limit 1000";
			}
			else if( $type == "level" )
			{
				$sql = "select `uid` from `user_info` order by `exp` desc,`levelUpTime` asc limit 1000";
			}
			
			$dbEngine = Common::getDB( 1 );
			$rank = $dbEngine->findQuery( $sql  );

			$temp = array();
			foreach($rank as $k => &$v)
			{
				$vv['rank'] = $k + 1;
				$vv['uid'] = intval( $v[0] );
				
				$user_info = MakeCommand_User::userInfo( $vv['uid'] );
				$vv['nickname'] = $user_info['username'];
				$vv['level'] = $user_info['lv'];
				$vv['gold'] = $user_info['gold'];
				$vv['type'] = $this->userId == $vv['uid'] ? "me" : "user";
				$vv['cardList'] = MakeCommand_Card::getCardsTeamDetailInfo( $vv['uid'] );
				$temp[] = $vv;
			}
			$rank = $temp;
			if( !empty( $rank ) )
			{
				$cache->set( self::CACHE_KEY_RANK.$type , $rank , Helper_Date::getTodayLeftSeconds() );
			}
		}
		
		$temp = array();
		$cnt = 0;
		foreach( $rank as &$v )
		{
			if( ++$cnt > $num ) break;
			$temp[] = $v;
		}
		$rank = $temp;
		
		return $rank;
	}
	
}