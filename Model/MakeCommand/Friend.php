<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class MakeCommand_Friend
{
	
	/**
	 * 获取好友列表
	 * @param unknown_type $userId
	 */
    public static function friendList( $userId )
	{
	  	$friendList = Friend_Model::getInstance( $userId )->getFriends();
	  	$friendListInfo = array();
	  	foreach ( $friendList as $friendId => &$info )
	  	{
	  		$friendInfo = self::friendInfo( $userId , $friendId );
	  		$userProfile = Data_User_Profile::getInstance($friendId)->getData();
	  		$friendInfo['tId'] = strval($userProfile['thirdId']);
	  		
	  		$friendListInfo[] = $friendInfo;
	  	}
	  	
	 	return $friendListInfo;
	}
	
	/**
	 * 获取申请列表
	 * @param unknown_type $userId
	 */
	public static function friendReqList( $userId )
	{
		$friendList = Friend_Model::getInstance( $userId )->getFriendReqs();
		$reqListInfo = array();
		foreach ( $friendList as $friendId => $info )
		{
			$reqListInfo[] = self::friendInfo( $userId , $friendId );
		  
		}
		return $reqListInfo;
	}
	
	/**
	 * 获取单个好友关系信息
	 * @param int $userId
	 * @param int $fId
	 */
	public static function friendInfo( $userId , $fId )
	{
		//$friendList = Friend_Model::getInstance( $userId )->getFriends();
	
		$fUserInfo = User_Info::getInstance( $fId )->getData();
		$fUserProfile = User_Profile::getInstance( $fId )->getData();
		$leaderInfo = Card_Model::getInstance( $fId )->getLeaderCard();	
	
		$friendInfo = array(
			'country' => (int)$fUserInfo['country'],
			'fid' => strval( $fId ),
			'gacha_pt' => (int)$fUserInfo['gachaPoint'],
			'lv' => (int)$fUserInfo['level'],
			'name' => strval( $fUserProfile['nickName'] ),
			'leader_card' => array(
					'cid' => $leaderInfo['cardId'],
					'lv' => intval( $leaderInfo['level'] ),
					'sk_lv' => intval( $leaderInfo['skillLevel'] ),
			),
			'max_friend_num' => 100,
			'login_time' => (int)$fUserProfile['loginTime'],
			//'now_friend_num' => (int)count( $friendList ),
			'now_friend_num' => 0,
		);
		
		return $friendInfo;
	}
	
	
	/**
	 * 获取好友用户信息
	 * @param int $userId
	 * @param int $fId
	 */
	public static function userInfo( $userId , $fId )
	{
		$friendList = Friend_Model::getInstance( $userId )->getFriends();
		$friendData = $friendList[$fId];
		$fUserInfo = User_Info::getInstance( $fId )->getData();
		$fUserProfile = User_Profile::getInstance( $fId )->getData();
		$leaderInfo = Card_Model::getInstance( $fId )->getLeaderCard();
		
		$friendInfo = array(
				'country' => (int)$fUserInfo['country'],
				'fid' => strval( $fId ),
				'gacha_pt' => (int)$fUserInfo['gachaPoint'],
				'lv' => (int)$fUserInfo['level'],
				'name' => strval( $fUserProfile['nickName'] ),
				'leader_card' => array(
						'cid' => $leaderInfo['cardId'],
						'lv' => intval( $leaderInfo['level'] ),
						'sk_lv' => intval( $leaderInfo['skillLevel'] ),
				),
				'login_time' => $fUserProfile['loginTime'],
				'max_friend_num' => (int)$fUserInfo['maxFriendNum'],
				'now_friend_num' => count( $friendList ),
		);
		return $friendInfo;
	}
	
	
	/**
	 * 获取友军信息
	 * @param unknown_type $fids
	 * 
	 */
	public static function HelpersInfo( $userIds , $isFriend )
	{
		if( !$userIds )
		{
			return array();
		}	
		
		$helperInfos = array(); 
		foreach ( $userIds as $userId )
		{
			$userInfo = User_Info::getInstance( $userId )->getData();
			$userProfile = User_Profile::getInstance( $userId )->getData();
			
			if( $isFriend )
			{
				$gachaPoint = 10;
			}
			else
			{
				$gachaPoint = 5;
			}
			
			
			$leaderInfo = Card_Model::getInstance( $userId )->getLeaderCard();
			if(  !$leaderInfo  )
			{
				continue;
			}
			$helperInfo = array(
				'country' => (int)$userInfo['country'],
				'fid' => strval( $userId ),
				'gacha_pt' => $gachaPoint,//(int)$userInfo['gachaPoint'],
				'lv' => (int)$userInfo['level'],
				'sk_lv' => (int)$userInfo['skillLevel'],
				'name' => strval( $userProfile['nickName'] ),
				'leader_card' => array(
						'cid' => $leaderInfo['cardId'],
						'lv' => intval( $leaderInfo['level'] ),
						'sk_lv' => intval( $leaderInfo['skillLevel'] ),
				),
			);
			
			$helperInfos[] = $helperInfo; 
		}
		
		return $helperInfos;
		
	}
	
	
}
?>
