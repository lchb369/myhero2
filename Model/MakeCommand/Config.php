<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class MakeCommand_Config
{
	public static function getReferName( $id )
	{
		return self::getName( $id , "refer" );
	}
	
	public static function getPayTypeName( $id )
	{
		return self::getName( $id , "payType" );
	}
	
	public static function getCoinLogType( $id )
	{
		return self::getName( $id , "coinLogType" );
	}
	
	public static function getItemLogType( $id )
	{
		return self::getName( $id , "itemLogType" );
	}
	
	public static function getArenaLogType( $id )
	{
		return self::getName( $id , "arenaLogType" );
	}
	
	private static function getName( $id , $type )
	{
		static $conf = array();
		if( empty( $conf[$type] ) )
		{
			$method = $type."Conf";
			if( method_exists( __CLASS__ , $method ) )
			{
				$conf[$type] = self::$method();
			}
		}
		
		if( !empty( $conf[$type][$id]['name'] ) )
		{
			$name = $conf[$type][$id]['name'];
		}
		elseif( !empty( $conf[$type][$id]['id'] ) )
		{
			$name = $conf[$type][$id]['id'];
		}
		else
		{
			$name = $id;	
		}
		return $name;
	}

	/**
 	 * 渠道配置
	 */
	private static function & referConf()
	{
		return array
		(
			"1"   => array( "id" => "DL" ,     "name" => "当乐" ),
			"4"   => array( "id" => "crossmo" ,"name" => "十字猫" ),
			"24"  => array( "id" => "115" , 	"name" => "115渠道" ),
			"25"  => array( "id" => "ud" , 		"name" => "" ),
			"26"  => array( "id" => "qc" , 		"name" => "" ), //千尺
			"100" => array( "id" => "uge" , 	"name" => "官网" ),
			"101" => array( "id" => "UC" , 		"name" => "" ),
			"102" => array( "id" => "XM" , 		"name" => "小米" ),
			"103" => array( "id" => "WL" , 		"name" => "瓦力" ),
			"120" => array( "id" => "91DJ" , 	"name" => "91点金" ),
			"121" => array( "id" => "91DJ-1" , 	"name" => "91点金1" ),
			"130" => array( "id" => "BaoRuan" ,"name" => "宝软" ),
			"131" => array( "id" => "51" ,	 	"name" => "" ),
			"136" => array( "id" => "tw" ,		"name" => "" ),
			"360" => array( "id" => "360" , 	"name" => "" ),
			"191" => array( "id" => "91_ios" , 	"name" => "" ),
			"200" => array( "id" => "uge_ios" ,"name" => "官网ios" ),
			"192" => array( "id" => "zq" , 		"name" => "掌趣" ),
			"94"  => array( "id" => "JF" , 		"name" => "机锋" ),
			"93"  => array( "id" => "DL" , 		"name" => "当乐" ),
			"189" => array( "id" => "TY" , 		"name" => "天翼" ),
			"117" => array( "id" => "UUC" , 	"name" => "悠悠村" ),
		);
	}
	
	/**
 	 * 支付平台配置
	 */
	private static function & payTypeConf()
	{
		return array
		(
		);
	}
	
	/**
	 * 元宝流水类型
	 */
	private static function & coinLogTypeConf()
	{
		return array
		(
			//支出
			"arenaRefreshEnemy" => array( "name" => "刷新竞技场对手" ),
			"arenaRefreshTimes" => array( "name" => "刷新竞技场次数" ),
			"drawCard" => array( "name" => "抽将" ),
			"expandCamp" => array( "name" => "扩充军营" ),
			"expandFriend" => array( "name" => "扩充好友" ),
			"recoverStamina" => array( "name" => "恢复体力" ),
			"revive" => array( "name" => "复活" ),
			//收入
			"tapjoy" => array( "name" => "tapjoy" ),
			"activity" => array( "name" => "运营活动" ),
			"activeCode" => array( "name" => "激活码" ),
			"firstPass" => array( "name" => "首次通关" ),
			"paymentOrder" => array( "name" => "充值" ),
		);
	}
	
	/**
	 * 道具流水类型
	 */
	private static function & itemLogTypeConf()
	{
		return array
		(
			//失去
			"rebirth" => array( "name" => "转生" ),
			"reinforce" => array( "name" => "强化" ),
			"sell" => array( "name" => "出售" ),
			"upgradeSkill" => array( "name" => "升级技能" ),
			//获得
			"activity" => array( "name" => "活动" ),
			"firstRecharge" => array( "name" => "首冲" ),
			"activeCode" => array( "name" => "激活码" ),
			"arenaBonus" => array( "name" => "竞技场奖励" ),
			"battleReward" => array( "name" => "战场掉落" ),
			"drawCard" => array( "name" => "抽将" ),
		);
	}
	
/**
	 * 竞技场流水奖励类型
	 */
	private static function & arenaLogTypeConf()
	{
		return array
		(
			"normalScore" => array( "name" => "积分" ),
			"benifit" => array( "name" => "福利值" ),
		);
	}
	
	
}
