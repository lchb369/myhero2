<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class MakeCommand_Refer
{
	/**
 	 * 渠道配置
	 */
	public static function conf()
	{
		return array
		(
			"4"   => array( "id" => "crossmo" ,"name" => "十字猫" ),
			"24"  => array( "id" => "115" , 	"name" => "" ),
			"25"  => array( "id" => "ud" , 		"name" => "" ),
			"100" => array( "id" => "uge" , 	"name" => "官网" ),
			"101" => array( "id" => "UC" , 		"name" => "" ),
			"102" => array( "id" => "XM" , 		"name" => "小米" ),
			"103" => array( "id" => "WL" , 		"name" => "瓦力" ),
			"120" => array( "id" => "91DJ" , 	"name" => "91点金" ),
			"121" => array( "id" => "91DJ-1" , 	"name" => "91点金1" ),
			"130" => array( "id" => "BaoRuan" ,"name" => "宝软" ),
			"131" => array( "id" => "51" ,	 	"name" => "" ),
			"136" => array( "id" => "tw" ,		"name" => "" ),
			"360" => array( "id" => "360" , 	"name" => "" ),
			"191" => array( "id" => "91_ios" , 	"name" => "" ),
			"200" => array( "id" => "uge_ios" ,"name" => "官网ios" ),
			"192" => array( "id" => "zq" , 		"name" => "掌趣" ),
			"94"  => array( "id" => "JF" , 		"name" => "机锋" ),
			"93"  => array( "id" => "DL" , 		"name" => "当乐" ),
			"189" => array( "id" => "TY" , 		"name" => "天翼" ),
		);
	}
	
	public static function getReferName( $id )
	{
		static $conf = array();
		if( empty( $conf ) )
		{
			$conf = self::conf();
		}
		
		if( !empty( $conf[$id]['name'] ) )
		{
			$name = $conf[$id]['name'];
		}
		elseif( !empty( $conf[$id]['id'] ) )
		{
			$name = $conf[$id]['id'];
		}
		else
		{
			$name = $id;	
		}
		
		return $name;
	}

}
