<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class MakeCommand_Card
{
	/**
	 * 
	 * 生成客户端用户信息数据
	 * @param unknown_type $userId
	 */
	public static function cards( $userId )
	{
		$cardInfos = Card_Model::getInstance( $userId )->getData();
		$cardData = array();
		
		foreach ( $cardInfos as $info )
		{
			$tmpData = array(
					
				'cid' => $info['cardId'],
				'exp' => (int)$info['exp'],
				'lv' => (int)$info['level'],
				'sk_lv'	=> (int)$info['skillLevel'],
				'upd_time' => (int)$info['addTime'],	
		  		
			);
			$cardData[$info['id']] = $tmpData;
		}
		return $cardData;
	}
	
	/**
	 * 获取一个武将信息
	 * @param int $userId
	 * @param int $cardId
	 */
	public static function newCard( $userId , $cardIds , $isFirst )
	{
		$cardInfos = Card_Model::getInstance( $userId )->getData();
		$cardData = array();
		
		$cardInfo = array();
		foreach ( $cardIds as $cardId )
		{
			$info = $cardInfos[$cardId];
			$cardInfo[] = array(
					'ucid' => strval( $cardId ),
					'cid' => $info['cardId'],
					'exp' => (int)$info['exp'],
					'lv' => (int)$info['level'],
					'sk_lv'	=> (int)$info['skillLevel'],
					'upd_time' => (int)$info['addTime'],
					'is_first' => $isFirst,
			);
		}
		
		if( count( $cardInfo ) == 1 )
		{
			return $cardInfo[0];
		}
				
		return $cardInfo;
	}

	/**
	 * 武将编队
	 */
	public static function cardsTeam( $userId )
	{
		$teamInfo = Data_Card_Team::getInstance( $userId )->getAll();
		return $teamInfo['teams'][$teamInfo['cur']];
	}
	
	/**
	 * 多武将编队
	 */
	public static function cardsTeamMulti( $userId )
	{
		return Data_Card_Team::getInstance( $userId )->getAll();
	}
	
	/**
	 * 获得玩家编队中的前三名武将头像和等级
	 * @param unknown $userid 玩家id
	 * @param number $cardLen 获取武将数量
	 * @return multitype:multitype:number unknown
	 */
	public static function getCardsTeamDetailInfo( $userid , $cardsLen = 3 )
	{
		$user_deck =  self::cardsTeam( $userid );
		$cards = array();
		if(!empty($user_deck))
		{
			$user_deck = array_slice( $user_deck , 0 , $cardsLen );
			foreach($user_deck as $v)
			{
				$cardInfo = Data_Card_Model::getInstance( $userid )->getCardInfo( $v );
				$cards[] = array(
						'cardId' => $cardInfo['cardId'],
						'level' => intval($cardInfo['level']),
						'skillLevel' => intval($cardInfo['skillLevel']),
				);
			}
		}
		return $cards;
	}
	
}
