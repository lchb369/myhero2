<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class MakeCommand_LangPack
{
	/**
	 * 获得语言包
	 * Enter description here ...
	 * @param unknown_type $key 标识
	 * @param unknown_type $default 默认值
	 * @param unknown_type $replArr 语言包中的{%1}{%2}会被替换成该数组中的值
	 */
	public static function get( $key , $default = "" , $replArr = array() )
	{
		$lang = !empty($_SESSION['lang']) ? $_SESSION['lang'] : 
				(!empty($_REQUEST['lang']) ? $_REQUEST['lang'] : "cn");
		if(Helper_Common::inPlatform(array("wk"))){$lang = "tw";}
		elseif(Helper_Common::inPlatform(array("kr"))){if(empty($lang)) $lang = "kr";}
		
		if(defined('IS_ADMIN'))
		{
			$key = 'admin_'.$key;
			if( Helper_Common::inPlatform( array( "wk" ) ) ) $lang = "cn";
		}
		
		static $conf = array();
		if( !isset( $conf[$lang] ) )
		{
			$method = "get".strtoupper($lang);
			if(method_exists(__CLASS__, $method))
			{
				$conf[$lang] = self::$method();
			}
		}
		
		if( isset( $conf[$lang][$key] ) )
		{
			$ret = $conf[$lang][$key];
		}
		else
		{
			$ret = $default;
		}
		
		if( !empty( $replArr ) )
		{
			$idx = 1;
			foreach( $replArr as $repl )
			{
				$ret = str_replace( "{%".($idx++)."}", $repl , $ret );
			}
		}
		
		return $ret;
	}

	/**
	 * 繁体中文
	 */
	private static function & getTW()
	{
		return array
		(
			'activeLoginDaysTitle' => '每日獎勵',
			'activeLoginDaysHead' => '連續登錄{%1}{%2}日',
			'activeTotalLoginsTitle' => '每日獎勵',
			'activeTotalLoginsHead' => '累計登錄{%1}{%2}日',
			'activeInTimeDailyLoginTitle' => '活動獎勵',
			'activeInTimeDailyLoginHead1' => '每日首登',
			'activeInTimeDailyLoginHead2' => '活動累計登入{%1}天獲得',
			'noticeRewardGold' => '<li>獲得銅錢<font color="#00b4ff">{%1}</font>枚</li>',
			'noticeRewardCoin' => '<li>獲得元寶<font color="#00b4ff">{%1}</font>個</li>',
			'noticeRewardGacha' => '<li>獲得援軍點數<font color="#00b4ff">{%1}</font>點</li>',
				
			'paySuccess' => '購買成功',
			'payFail' => '購買失敗',
		);
	}
	
	/**
	 * 简体中文
	 */
	private static function & getCN()
	{
		return array
		(
			'activeLoginDaysTitle' => '每日奖励',
			'activeLoginDaysHead' => '连续登录{%1}{%2}日',
			'activeTotalLoginsTitle' => '每日奖励',
			'activeTotalLoginsHead' => '累计登录{%1}{%2}日',
			'activeInTimeDailyLoginTitle' => '活动奖励',
			'activeInTimeDailyLoginHead1' => '每日首登',
			'activeInTimeDailyLoginHead2' => '活动累计登入{%1}天获得',
			'noticeRewardGold' => '<li>获得铜钱<font color="#00b4ff">{%1}</font>枚</li>',
			'noticeRewardCoin' => '<li>获得元宝<font color="#00b4ff">{%1}</font>个</li>',
			'noticeRewardGacha' => '<li>获得援军点数<font color="#00b4ff">{%1}</font>点</li>',
				
			'paySuccess' => '购买成功',
			'payFail' => '购买失败',
				
			'admin_control_panel' => '控制面板',
			'admin_user_info' => '用户信息',
			'admin_friends_manage' => '好友管理',
			'admin_order_manage' => '订单管理',
			'admin_data_statistics' => '数据统计',
			'admin_config_manage' => '配置管理',
			'admin_test_tools' => '测试工具',
			'admin_operation_tools' => '运维工具',
			'admin_administrator' => '管理员',
			'admin_logout' => '退出',
				

			'admin_hero_list' => '武将列表',
			'admin_check_log' => '查看日志',
			'admin_list_view' => '列表视图',
			'admin_add_hero' => '添加武将',
			'admin_card_no' => '卡号',
			'admin_quantity' => '数量',
			'admin_multi_select' => '多选',
			'admin_do_add_hero' => '新增武将',
			
			'admin_bind_title' => '账号绑定',
			'admin_bind_id_new' => '新登录账号字符串',
			'admin_bind_id_old' => '老账号数字ID',
			'admin_bind_btn' => '账号绑定',
				
			'admin_card_id' => '武将卡ID',
			'admin_card_exp' => '武将经验',
			'admin_card_level' => '武将等级',
			'admin_card_skill_level' => '技能等级',
			'admin_card_add_time' => '添加时间',
			'admin_card_operation' => '操作',
				
			'admin_user_data_title' => '用户资料',
			'admin_user_del_btn' => '删除用户',
			'admin_user_id' => '用户ID',
			'admin_user_account' => '帐号',
			'admin_user_nickname' => '昵称',
			'admin_user_reg_date' => '注册时间',
			'admin_user_login_date' => '登录时间',
			'admin_user_login_times' => '登录次数',
			'admin_user_consecutive_days' => '连续上线天数',
			'admin_user_platform' => '用户平台',
			'admin_user_total_pay' => '累计充值金额',
			'admin_user_total_buy_coin' => '累计充值获得元宝',
			
			'admin_role_info_title' => '角色信息',
			'admin_role_basic_info' => '基本信息',
			'admin_role_more_info' => '更多信息',
			'admin_role_col_name' => '属性名',
			'admin_role_col_value' => '属性值',
			'admin_role_exp' => '经验',
			'admin_role_level' => '等级',
			'admin_role_country' => '国家',
			'admin_role_coin' => '充值币',
			'admin_role_gold' => '金币',
			'admin_role_leadership' => '统御力',
			'admin_role_welfare' => '福利',
			'admin_role_floor' => '普通战场进度',
			'admin_role_room' => '小关编号',
			'admin_role_is_pass' => '是否通关',
			
			'admin_role_stamina' => '耐力值',
			'admin_role_gacha_point' => '援军点数',
			'admin_role_max_card' => '最大武将数',
			'admin_role_max_friend' => '最大好友数',
			'admin_role_max_stamina' => '体力上限',
			'admin_role_invite_code' => '邀请码',
			'admin_role_card_team' => '武将序列',
				
			'admin_friend_list_title' => '好友列表',
			'admin_friend_list_view' => '列表视图',
			'admin_friend_add_view' => '添加好友',
			'admin_friend_nickname' => '好友昵称',
			'admin_friend_level' => '好友等级',
			'admin_friend_camp' => '好友阵营',
			'admin_friend_add_time' => '添加时间',
			'admin_friend_operation' => '操作',
				
			'admin_friend_choose' => '选择推荐好友 …',
			'admin_friend_add_by_id' => '按好友ID添加',
			'admin_friend_do_add' => '新增好友',
			'admin_friend_do_req' => '新增好友邀请',
		);
	}
	
	/**
	 * 韩语
	 */
	private static function & getKR()
	{
		return array
		(
			'activeLoginDaysTitle' => '로그인 보너스',
			'activeLoginDaysHead' => '연속 로그인 {%1}{%2}일',
			'activeTotalLoginsTitle' => '로그인 보너스',
			'activeTotalLoginsHead' => '누적 로그인 {%1}{%2}일',
			'activeInTimeDailyLoginTitle' => '이벤트 보너스',
			'activeInTimeDailyLoginHead1' => '일일 로그인',
			'activeInTimeDailyLoginHead2' => '이벤트 누적 참여 {%1}일 기록',
			'noticeRewardGold' => '<li>코인 <font color="#333399">{%1}</font> 개 획득</li>',
			'noticeRewardCoin' => '<li>원보 <font color="#333399">{%1}</font> 개 획득</li>',
			'noticeRewardGacha' => '<li>원군포인트 <font color="#333399">{%1}</font> 획득</li>',
				
			'paySuccess' => '구입 성공!',
			'payFail' => '구입 실패!',
			'noNameUser' => '무명 장수',
		);
	}
	
	/**
	 * 越南
	 */
	private static function & getVN()
	{
		return array
		(
			'activeLoginDaysTitle' => 'Quà mỗi ngày',
			'activeLoginDaysHead' => 'Đăng nhập liên tục {%1}{%2} ngày',
			'activeTotalLoginsTitle' => 'Quà mỗi ngày',
			'activeTotalLoginsHead' => 'Tích lũy đăng nhập {%1}{%2} ngày',
			'activeInTimeDailyLoginTitle' => 'Phần thưởng sự kiện',
			'activeInTimeDailyLoginHead1' => 'Đăng nhập lần đầu',
			'activeInTimeDailyLoginHead2' => 'Tích lũy đăng nhập {%1} ngày nhận được',
			'noticeRewardGold' => '<li>Nhận tiền xu<font color="#333399">{%1}</font>Xu</li>',
			'noticeRewardCoin' => '<li>Nhận KNB<font color="#333399">{%1}</font>KNB</li>',
			'noticeRewardGacha' => '<li>Nhận điểm chi viện<font color="#333399">{%1}</font>Điểm</li>',

			'paySuccess' => 'paySuccess',
			'payFail' => 'payFail',
		);
	}
	
	private static function & getUS()
	{
		return array
		(
			'admin_control_panel' => 'Control Panel',
			'admin_user_info' => 'User Inforamtion',
			'admin_friends_manage' => 'Friends Management',
			'admin_order_manage' => 'Order Management',
			'admin_data_statistics' => 'Data Statistics',
			'admin_config_manage' => 'Configuration Management',
			'admin_test_tools' => 'Test Tools',
			'admin_operation_tools' => 'Operation Tools',
			'admin_administrator' => 'Administrator',
			'admin_logout' => 'Logout',

			'admin_hero_list' => 'Hero List',
			'admin_check_log' => 'Check Log',
			'admin_list_view' => 'List View',
			'admin_add_hero' => 'Add Hero',
			'admin_card_no' => 'Card No.',
			'admin_quantity' => 'Quantity',
			'admin_multi_select' => 'Multi-select',
			'admin_do_add_hero' => 'Do Add Hero',
				
			'admin_bind_title' => 'Bind Account',
			'admin_bind_id_new' => 'New-lgoin Account String',
			'admin_bind_id_old' => 'Old Account Digital ID',
			'admin_bind_btn' => 'Bind Account',
				
			'admin_card_id' => 'Hero Card ID',
			'admin_card_exp' => 'Hero EXP',
			'admin_card_level' => 'Hero Grade',
			'admin_card_skill_level' => 'Skill Level',
			'admin_card_add_time' => 'Add Time',
			'admin_card_operation' => 'Operation',
				
			'admin_user_data_title' => 'User\'s data',
			'admin_user_del_btn' => 'Delete User',
			'admin_user_id' => 'User ID',
			'admin_user_account' => 'Account',
			'admin_user_nickname' => 'Nickname',
			'admin_user_reg_date' => 'Join Date',
			'admin_user_login_date' => 'Logon Date',
			'admin_user_login_times' => 'Login Times',
			'admin_user_consecutive_days' => 'Consecutive Days',
			'admin_user_platform' => 'User Platform',
			'admin_user_total_pay' => 'Cumulative Recharge',
			'admin_user_total_buy_coin' => 'Cumulative Coin of Recharging',
				
			'admin_role_info_title' => 'Role Info',
			'admin_role_basic_info' => 'Basic Info',
			'admin_role_more_info' => 'More Info',
			'admin_role_col_name' => 'Attribute Name',
			'admin_role_col_value' => 'Property Value',
			'admin_role_exp' => 'EXP',
			'admin_role_level' => 'Level',
			'admin_role_country' => 'Country',
			'admin_role_coin' => 'Coin',
			'admin_role_gold' => 'Gold',
			'admin_role_leadership' => 'Leadership',
			'admin_role_welfare' => 'Welfare',
			'admin_role_floor' => 'Common battlefield<br/>progress',
			'admin_role_room' => 'Act No.',
			'admin_role_is_pass' => 'Accomplished or Not',
				
			'admin_role_stamina' => 'Stamina',
			'admin_role_gacha_point' => 'Reinforcement Points ',
			'admin_role_max_card' => 'Maximum No.of Heros',
			'admin_role_max_friend' => 'Maximum No.of Friends',
			'admin_role_max_stamina' => 'Physical Limits',
			'admin_role_invite_code' => 'Invite Code',
			'admin_role_card_team' => 'Card Team',
				
			'admin_friend_list_title' => 'Friend List',
			'admin_friend_list_view' => 'List View',
			'admin_friend_add_view' => 'Add Friends',
			'admin_friend_nickname' => 'Friend Nickname',
			'admin_friend_level' => 'Friend Level',
			'admin_friend_camp' => 'Friend Camp',
			'admin_friend_add_time' => 'Add Time',
			'admin_friend_operation' => 'Operation',
			
			'admin_friend_choose' => 'Choose Recommended Friend',
			'admin_friend_add_by_id' => 'Add by Friend ID',
			'admin_friend_do_add' => 'Add Friend',
			'admin_friend_do_req' => 'Add Friend Request',
				
		);
	}
	
}
