<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class MakeCommand_User
{
	/**
	 * 
	 * 生成客户端用户信息数据
	 * @param unknown_type $userId
	 */
	public static function userInfo( $userId )
	{
		$userProfile = User_Profile::getInstance( $userId )->getData();
		$userInfo = User_Info::getInstance( $userId )->getData();
		
		//是否新用户
		$isNewUser = User_Profile::getInstance( $userId )->isNewUser();
		
		//经验相差计算
		$levelConfig = Common::getConfig( "level" );
		$level = $userInfo['level'] ? $userInfo['level'] : 1;
		$nextLevelNeedExp = $levelConfig[$level+1]['exp'] -  $userInfo['exp'];
		$nextLevelNeedExp = $nextLevelNeedExp < 0 ? 0 : $nextLevelNeedExp;
		$thisLevelNowExp = $userInfo['exp'] - $levelConfig[$level]['exp'];
		
		//武将卡信息
		$leaderInfo = Card_Model::getInstance( $userId )->getLeaderCard();	
	
		//$userInfo['leaderShip']= 10;
		//$userInfo['staminaUpdateTime'] = 1348891895;
		//首次冲值
		$userInfo['firstCharge'] = intval(Data_Order_Model::getInstance( $userId )->isFirstRecharge(false));

		$userData = array(
			'uid' => strval( $userProfile['uid'] ),
			//'username' => strval( $userProfile['uid'] ),
			'username' => $userProfile['nickName'],
			'coin' => (int)$userInfo['coin'],
			'cost' => Data_User_Info::getInstance( $userId )->getLeaderShip(),//统于力
			'country' =>(int)$userInfo['country'],
			'exp' => (int)$userInfo['exp'],
			'first_charge' => (int)$userInfo['firstCharge'],
			'gacha_pt' => (int)$userInfo['gachaPoint'],
			'gold' => (int)$userInfo['gold'],
			'login_time' => $userProfile['loginTime'],
			'login_days' => (int)$userProfile['loginDays'],
			'total_logins' => (int)$userProfile['totalLogins'],
			'lv' => (int)$userInfo['level'],
			'stamina' => Data_User_Info::getInstance( $userId )->getStamina(),
			'stamina_upd_time' => (int)$userInfo['staminaUpdateTime'],
			'max_card_num' => (int)$userInfo['maxCardNum'],
			'max_stamina' => Data_User_Info::getInstance( $userId )->getMaxStamina(),
			'max_friend_num' => (int)$userInfo['maxFriendNum'],
			'newbie' => $userInfo['newbieStep'] < User_Model::$maxNewbieStep ? 1 : 0,
			'newbie_step' => (int)$userInfo['newbieStep'],
			'next_lv_need_exp' => (int)$nextLevelNeedExp,
			'this_lv_now_exp' => (int)$thisLevelNowExp,
			'invite_code' => strval( $userInfo['inviteCode'] ),
			'free_draw_time' => (int)$userInfo['freeDrawTime'],
			'leader_card' => array(
					'cid' => $leaderInfo['cardId'],
					'lv' => intval( $leaderInfo['level'] ),
					'sk_lv' => intval( $leaderInfo['skillLevel'] ),
			),
			'buff' => Data_User_Info::getInstance( $userId )->getBuffs(),
		);
		return $userData;
	}

}
