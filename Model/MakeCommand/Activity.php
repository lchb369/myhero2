<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class MakeCommand_Activity
{
	/**
	 * 
	 * 生成客户端用户信息数据
	 * @param unknown_type $userId
	 */
	public static function consume( $userId , $type = 1)
	{
		
		$paidConfig = Common::getConfig( "paid" );
		$paidConfig = $paidConfig[$type];
		$returnData = array();
		foreach ( $paidConfig as $id => $info )
		{
			$serverTime = $_SERVER['REQUEST_TIME'];
			if( $serverTime >= strtotime( $paidConfig[$id]['startTime'] ) && $serverTime < strtotime( $paidConfig[$id]['endTime'] ) )
			{
				switch ( $type ) {
					case Activity_Model::PAID_TYPE_SUM_CONSUME:
					case Activity_Model::PAID_TYPE_SUM_PAY:
					case Activity_Model::PAID_TYPE_SINGLE_PAY:
						$info = Activity_Model::getInstance( $userId )->getActivityConsumeInfo( $id );
						$paidConfig[$id]['currCoin'] =  $info['coin'] ? intval($info['coin']) : 0;
						$paidConfig[$id]['reward'] =  $info['reward'] ? intval($info['reward']) : 0;
						$data = $paidConfig[$id];
					break;
					
					case Activity_Model::PAID_TYPE_FIRST_PAY:
						$paidConfig[$id]['currCoin'] = 0;
						//是否有领取资格
						$totalRecharge = Data_Order_Model::getInstance( $userId )->getTotalRecharge();
						if( $totalRecharge > 0 )
						{
							//是否领取过
							$actInfo = Data_Activity_Model::getInstance( $userId )->getActivity( Activity_Model::ACTIVE_FIRST_RECHARGE );
							$paidConfig[$id]['reward'] = $actInfo['rewardTime'] > 0 ? 1 : 0;
						}
						else
						{
							$paidConfig[$id]['reward'] = 2;
						}
						$data = $paidConfig[$id];
					break;
				}
			}
			if( !empty( $data ))
			{
				unset( $data['type'] );
				$returnData[] = $data;
				unset( $data);
			}
		}
		return $returnData;
	}
	
	
}
