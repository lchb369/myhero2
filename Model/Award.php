<?php
/**
 * 奖励反馈通知类
 * @author LiuChangbing
 */
if( !defined( 'IN_INU' ) )
{
    return;
}
class Award
{
	//返回的数据

	protected static $data = array();

	/**
	 * 标识升级和奖励
	 * @param	string $cmdType	命令类型，如:升级levelUp
	 * @param	array  $data	要奖励的信息
	 */
	public static function add( $cmdType , $data )
	{
		switch( $cmdType )
		{
			case 'levelUp':
				
				if( !isset( self::$data['levelUp'] ) )
				{
					self::$data['levelUp'] = array();
				}
				
				self::$data['levelUp'][] = $data;
				
				ObjectStorage::saveTask();
				if( isset( $GLOBALS['uId'] ) && $GLOBALS['uId'] > 0 )
				{
					self::$data['taskList'] = MakeCommand_Task::taskList( $GLOBALS['uId'] );
				}
				break;
				
			case 'overWeight':
				
				if( !isset( self::$data['overWeight'] ) )
				{
					self::$data['overWeight'] = array();
				}
				
				self::$data['overWeight'][] = $data;
				break;
				
			case 'updateTaskStatus':
				
				self::$data['updateTaskStatus'][$data['taskId']] = $data;
				break;
				
			case 'finishedTask':
				
				ObjectStorage::saveTask();
				if( isset( $GLOBALS['uId'] ) && $GLOBALS['uId'] > 0 )
				{
					self::$data['taskList'] = MakeCommand_Task::taskList( $GLOBALS['uId'] );
				}
				if( !isset( self::$data[$cmdType] ) )
				{
					self::$data[$cmdType] = array();
				}
				self::$data[$cmdType][] = $data;
				break;
				
			case 'popularityLevelUp':
				
				if( !isset( self::$data[$cmdType] ) )
				{
					self::$data[$cmdType] = array();
				}
				self::$data[$cmdType][] = $data;
				break;
				
			case 'addBag':
				
				if( !isset( self::$data[$cmdType][$data['itemId']] ) )
				{
					self::$data[$cmdType][$data['itemId']] = $data;
				}
				else 
				{
					self::$data[$cmdType][$data['itemId']]['number'] += $data['number'];
					if( self::$data[$cmdType][$data['itemId']]['number'] == 0 )
					{
						unset( self::$data[$cmdType][$data['itemId']] );
					}
					
					if( count( self::$data[$cmdType] ) == 0 )
					{
						unset( self::$data[$cmdType] );
					}
				}
				break;
				
			case 'dailyTaskInfo':
				self::$data['receivedDailyTaskAwardInfo'] = $data;
				break;
				
			case 'achievement':
				if( !isset( self::$data[$cmdType] ) )
				{
					self::$data[$cmdType] = array();
				}
				self::$data[$cmdType][] = $data;
				break;
				
			default:
				
				self::$data[$cmdType] = $data;
				break;
		}
	}
	
	/**
	 * 获取是否有升级或奖励事件
	 * @return array
	 */

	public static function get()
	{
		return self::$data;
	}
}



?>