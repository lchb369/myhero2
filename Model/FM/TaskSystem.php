<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * a) 任务状态值说明： 0， 未接受； 1，接受； 2， 完成； 3，已奖励； 4，已拒绝； 5，已超时；
 * b) 普通任务与奖杯任务的区分： 在json中，如果存在 taskId 与 did_head两项，将会可能是奖杯或者普通任务，只有taskId肯定是普通任务
 *					 taskId != 0 且 did_head == 0 , 普通任务
 *					 taskId == 0 且 did_head != 0 , 奖杯任务
 *
 */

/**
 * FM任务类
 * @author	Tom , Luckyboys
 */
class FM_TaskSystem
{
	/**
	 * 单例对象数组
	 * @var	array(
	 * 			{userId}:FM_TaskSystem
	 * 		)
	 */
	protected static $singletonObjects = array();
	
	/**
	 * 任务客户端
	 * @var	fm_task
	 */
	protected static $client;
	
	/**
	 * 用户ID
	 * @var	integer
	 */
	protected $userId;
	
	/**
	 * 等级
	 * @var	integer
	 */
	protected $level;
	
	/**
	 * 完成的任务或者奖杯
	 * @var	array
	 */
	protected $result = array();
	
	/**
	 * 配置
	 * @var	array
	 */
	protected static $config = array();
	
	/**
	 * 获取任务类实例
	 * @param	int $userId	用户ID
	 * @param	int $level	等级
	 * @return	FM_TaskSystem
	 */
	public static function & getInstance( $userId , $level = 0 )
	{
		if( self::$singletonObjects[$userId] == null )
		{
			self::$singletonObjects[$userId] = new FM_TaskSystem( $userId , $level );
		}
		
		if( self::$config == null )
		{
			self::$config = & Common::getConfig( 'Task' );
		}
		
		if( self::$client == null && class_exists( 'fm_task' ) )
		{
			self::$client = new fm_task( self::$config['host'] , self::$config['port'] );
		}
		
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 任务用户登录
	 * @param int $userId 	用户ID
	 * @param int $level 	等级
	 */
	protected function __construct( $userId , $level )
	{
		$this->userId = $userId;
		$this->level = $level;
		
		if( self::$client )
		{
			self::$client->login( $this->userId , $this->level );
		}
	}
	
	/**
	 * 任务列表获取
	 * @return	array
	 */
	public function getTasks()
	{
		if( self::$client )
		{
			$result = self::$client->get_tasks( $this->userId , $this->level );
			$result = json_decode( $result , true );
		}
		return is_array( $result ) ? $result : array();
	}
	
	/**
	 * 奖杯列表获取
	 * @return	array
	 */
	public function getCups()
	{
		if( self::$client )
		{
			$result = self::$client->get_cups( $this->userId , $this->level );
			$result = json_decode( $result , true );
		}
		return is_array( $result ) ? $result : array();
	}
	
	/**
	 * 任务接受
	 * @param	int $taskId	任务Id
	 * @return	array
	 */
	public function acceptTask( $taskId )
	{
		if( self::$client )
		{
			$result = self::$client->accept_task( $this->userId , $this->level , $taskId );
			$result = json_decode( $result , true );
		}
		return is_array( $result ) ? $result : array();
	}
	
	/**
	 * 任务拒绝
	 * @param	int $taskId	任务Id
	 * @return	array
	 */
	public function rejectTask( $taskId )
	{
		if( self::$client )
		{
			$result = self::$client->reject_task( $this->userId , $this->level , $taskId );
			$result = json_decode( $result , true );
		}
		return is_array( $result ) ? $result : array();
	}
	
	/**
	 * 用户操作自增
	 * @param	int $action	行动状态
	 * @param	int $object	对象名
	 * @param	int $value	自增的值
	 * @return	array
	 */
	public function actionIncrement( $action , $object , $value = 1 )
	{
		if( self::$client )
		{
			$result = self::$client->action_increment( $this->userId , $action , $object , $value );
			$result = json_decode( $result , true );
		}
		$result = is_array( $result ) ? $result : array();
		if( $result )
		{
			$this->result = array_merge( $result , $this->result );
		}
	}
	
	/**
	 * 用户操作设置
	 * @param	int $object	对象名
	 * @param	int $value	自增的值
	 * @return	array
	 */
	public function actionSet( $action , $object , $value )
	{
		if( self::$client )
		{
			$result = self::$client->action_set( $this->userId , $action , $object , $value );
			$result = json_decode( $result , true );
		}
		return is_array( $result ) ? $result : array();
	}
	
	/**
	 * 任务奖励
	 * @param	int $taskId		任务Id
	 * @param	int $did_Head	奖杯任务参数
	 * @return	array
	 */
	public function awardsTask( $taskId = 0 , $did_Head = 0 )
	{
		if( self::$client )
		{
			$result = self::$client->awards_task( $this->userId , $this->level , $taskId , $did_Head );
			if( $result )
			{
				$result = json_decode( $result , true );
				return is_array( $result ) ? $result : array();
			}
		}
		return false;
	}
	
	/**
	 * 获取已经完成列表
	 *
	 * @return array
	 */
	public function getResult()
	{
		return $this->result;
	}
}

?>