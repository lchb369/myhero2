<?php


if( !defined( 'IN_INU' ) )
{
	return;
}

class FM_Fight
{

	/**
	 * 单例模式对象
	 * @var	FM_Rank
	 */
	protected static $singletonObject = null;
	
	/**
	 * C++通知服务客户端
	 * @var	fm_rank
	 */
	protected static $client = null;
	
	/**
	 * C++通知服务客户端（从属）
	 * @var	fm_rank
	 */
	protected static $slaveClient = null;
	
	/**
	 * 系统配置
	 * @var	array
	 */
	protected $config;
	
	/**
	 * 实例化
	 */
	protected function __construct()
	{
		$this->config = & Common::getConfig( 'rankFightServer' );
		try
		{
			if( class_exists( 'fm_rank' ) && $this->config['host'] && $this->config['port'] )
			{
				self::$client = new fm_rank( $this->config['host'] , $this->config['port'] );
			}
		}
		catch( Exception $ex )
		{
			;
		}
	}
	
	/**
	 * 获取一个Http代理类实例
	 * @return	FM_Rank
	 */
	public static function & getInstance()
	{
		if( self::$singletonObject == null )
		{
			self::$singletonObject = new FM_Fight();
		}
		
		return self::$singletonObject;
	}
	
	/**
	 * 设置排行榜
	 * @param	int $listId	排行榜ID
	 * @param	int $maxNum	榜单最大数量
	 * @return	string 0表示建立了新榜单，1表示榜单已经建立
	 */
	public function setRankingList( $listId , $maxNum )
	{
		if( self::$client )
		{
			
			return  self::$client->set_ranking_list( $listId , $maxNum );
		}
	}
	
	/**
	 * 设置指定玩家排行榜数值
	 * @param	int $listId	 		排行榜ID
	 * @param	int $userId			用户ID
	 * @param	int $rankValue		排行榜数值
	 * @param	string $userName	用户名
	 * @param	string $avatar		头像url
	 * @return	ArrayObject 		此榜单最后一名信息
	 */
	public function setUserRankValue( $listId , $userId , $rankValue , $userName = "" , $avatar = "" )
	{
		if( self::$client )
		{
			$data = self::$client->set_user_rank_value( $listId , $userId , $rankValue , $userName , $avatar );
			if( $data )
			{
				$lastUserData = json_decode( $data , true );
				return $lastUserData;
			}
		}
		return false;
	}
	
	/**
	 * 获取某个玩家排行的值
	 * @param	int $listId	 		排行榜ID
	 * @param	int $userId			用户ID
	 */
	public function getUserRankInfo( $userId , $listId )
	{
		if( self::$client )
		{
			$data = self::$client->get_user_rank_info( $listId , $userId );
			if( $data )
			{
				$userInfo = json_decode( $data , true );
				return $userInfo;
			}
		}
		return false;
	}
	/**
	 * 在排行榜中获取用户列表
	 * @param	int $listId	 		排行榜ID
	 * @param	int $rank			名次
	 * @param	int $formerNum		前者数量
	 * @param	int $latterNum		后者数量
	 */
	public function getRankList( $listId , $rank = 0 , $formerNum = 0 , $latterNum = 0 )
	{
		if( self::$client )
		{
			$data = self::$client->get_users_by_rank( $listId , $rank , $formerNum , $latterNum );
			if( $data )
			{
				$rankInfo = json_decode( $data , true );
				return $rankInfo;
			}
		}
		return false;
	}
	/**
	 * 获取某用户在排行榜中其前后数值信息
	 * @param	int $listId	 		排行榜ID
	 * @param	int $userId			用户ID
	 * @param	int $formerNum		前者数量
	 * @param	int $latterNum		后者数量
	 */
	public function getUserRangeInfo( $listId , $userId , $formerNum , $latterNum )
	{
		if( self::$client )
		{
			$data = self::$client->get_users_by_uid( $listId , $userId , $formerNum , $latterNum );
			if( $data )
			{
				$rankInfo = json_decode( $data , true );
				return $rankInfo;
			}
		}
		return false;
	}
	/**
	 * 获取排行榜的指定名次间的数值
	 * @param	int $listId	 		排行榜ID
	 * @param	int $formerNum		前者数量
	 * @param	int $latterNum		后者数量
	 */
	public function getRangeInfo( $listId , $formerNum , $latterNum )
	{
		if( self::$client )
		{
			$data = self::$client->get_interval_users( $listId , $formerNum , $latterNum );
			if( $data )
			{
				$rankInfo = json_decode( $data , true );
				return $rankInfo;
			}
		}
		return false;
	}
	
	/**
	 * 清空指定排行榜的所有数据
	 * @param	int $listId	 		排行榜ID
	 */
	public function destroyRankingList( $listId )
	{
		if( self::$client )
		{
			return self::$client->destroy_ranking_list( $listId );
		}
		return true;
	}
	
	/**
	 * 获取指定排行榜最后一名用户信息
	 * @param	int $listId	 		排行榜ID
	 */
	public function getLastUserInfo( $listId )
	{
		if( self::$client )
		{
			$data = self::$client-> get_last_user( $listId );
			if( $data )
			{
				$lastUserInfo = json_decode( $data , true );
				return $lastUserInfo;
			}
		}
		return false;
	}
	/**
	 * 替换榜内某一个排名
	 * @param int $listId	//榜ID
	 * @param int $userId	
	 * @param int $rank
	 */
	public function forceReplaceByRank( $listId , $userId , $userName , $url , $rank )
	{
		if( self::$client )
		{
			return  self::$client-> force_replace_by_rank( $listId  , $userId , $userName , $url ,  $rank );
		}
		return false;
	}
	
	/**
	 * 删除榜内某一个排名
	 * @param int $listId	//榜ID
	 * @param int $userId	
	 * @param int $rank
	 */
	public function forceDeleteRank( $listId , $rankId )
	{
		if( self::$client )
		{
			return self::$client-> force_delete_by_rank( $listId , $rankId );
		}
		return false;
	}
	
	
	
	
}

?>
	

