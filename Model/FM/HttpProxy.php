<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * FM Http代理类
 * @author	Luckyboys
 */
class FM_HttpProxy
{
	/**
	 * 单例模式对象
	 * @var	FM_Accumulator
	 */
	protected static $singletonObject = null;
	
	/**
	 * C++代理服务客户端
	 * @var	fm_http_proxy
	 */
	protected $client = null;
	
	/**
	 * 代理配置
	 * @var	array
	 */
	protected $config;
	
	/**
	 * 实例化
	 */
	protected function __construct()
	{
		$this->config = & Common::getConfig( 'FMHttpProxy' );
		try
		{
			if( class_exists( 'fm_http_proxy' ) && $this->config['host'] && $this->config['port'] )
			{
				$this->client = new fm_http_proxy( $this->config['host'] , $this->config['port'] );
			}
		}
		catch( Exception $ex )
		{
			;
		}
	}
	
	/**
	 * 获取一个Http代理类实例
	 * @return	FM_HttpProxy
	 */
	public static function & getInstance()
	{
		if( self::$singletonObject == null )
		{
			self::$singletonObject = new FM_HttpProxy();
		}
		
		return self::$singletonObject;
	}
	
	/**
	 * 发送一个数据到一个链接上
	 * @param	string $url			目标URL
	 * @param	array $postData		Post给目标URL的数据
	 */
	public function send( $url , $postData = array() )
	{
		if( $this->client )
		{
			//检查地址上面是否包含http协议头
			if( strpos( $url , "http://" ) !== 0 && strpos( $url , "https://" ) !== 0 )
			{
				$url = "http://{$url}";
			}
			
			//格式化Post数据
			$postString = empty( $postData ) ? "" : http_build_query( $postData );
			
			try
			{
				$this->client->send( $url , $postString );
			}
			catch( Exception $ex )
			{
				;
			}
		}
	}
}

?>