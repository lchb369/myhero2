<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * FM好友列表
 * @author	wzhzhang , Luckyboys
 */
class FM_FriendServer
{
	
	protected $useApp = false;
	
	protected  $platform;
	/**
	 * 好友客户端
	 * @var	fm_friend
	 */
	protected static $client = null;
	
	/**
	 * 用户ID
	 * @var	integer
	 */
	protected $userId;
	
	/**
	 * 单例对象
	 * @var	FM_FriendServer
	 */
	protected static $singletonObjects = array();
	
	/**
	 * 请求类型（同步）
	 * @var	int
	 */
	const REQUEST_TYPE_SYNCHRONOUS = 1;
	
	/**
	 * 请求类型（异步）
	 * @var	int
	 */
	const REQUEST_TYPE_ASYNCHRONOUS = 0;
	
	/**
	 * 获取好友类实例
	 * @param	int $userId	用户ID
	 * @return	FM_FriendServer
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new FM_FriendServer( $userId );
		}
		
		if( self::$client == null )
		{
			if( class_exists( 'fm_friend' ) )
			{
				$config = & Common::getConfig( 'friendServer' );	
				self::$client = new fm_friend( $config['host'] , $config['port'] );
			}
		}
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 实例化
	 * @param	int $userId	用户ID
	 */
	protected function __construct( $userId )
	{
		$this->platform = Common::getConfig( "platform" );
		if( $this->platform == "minik" ||  $this->platform == "fminutes" )
		{
			$this->useApp = true ;
			
		}
		$this->userId = $userId;
	}
	
	/**
	 * 用户登录
	 * 此接口返回该uid的好友数据在服务器中缓存建立的时间戳， 服务器会使用其中参数session_key异步的去平台请求数据，并缓存起来。
	 * @param string $sessionKey
	 * @return	int | false	//版本号或者服务器出错
	 */
	public function login( $sessionKey )
	{
		if( self::$client )
		{
			return self::$client->login( $this->userId , $sessionKey );	
		}
	}
	
	/**
	 * 获取好友列表版本号
	 * @return	int
	 */
	public function getVersion()
	{
		if( self::$client )
		{
			return self::$client->get_version( $this->userId );
		}
	}
	
	/**
	 * 获得当前用户属性信息
	 * @param	int $syncFlag		//1:表示为同步请求，session_key参数必须设置，服务器若在缓存中没有此uid数据，
	 * 								则立即去平台获取数据，并将数据返回。
	 * 								0:则为异步接口，session_key参数则设置为空即可，服务器立即返回数据。
	 * @param	string $sessionKey
	 * @return	array | false
	 */
	public function getUserInfo( $syncFlag = self::REQUEST_TYPE_ASYNCHRONOUS , $sessionKey = '' )
	{
		
		if(  $this->platform == "minik"  )
		{
			$info = Minik_Api::getUserInfo();
			return $info;
		}
		elseif (   $this->platform == "fminutes"  )
		{
			$info = Fminutes_Friend::getUserInfo();
			return $info;
		}
		
		if( self::$client )
		{
			$info = self::$client->get_self_info( $this->userId , $syncFlag , $sessionKey );
			if( $info )
			{
				$info = json_decode( $info , true );
				return $info;
			}
		}
		return false;
	}
	
	/**
	 * 获得安装了app的好友的id列表
	 * @param	int $syncFlag		//1:表示为同步请求，session_key参数必须设置，服务器若在缓存中没有此uid数据，
	 * 								则立即去平台获取数据，并将数据返回。
	 * 								0:则为异步接口，session_key参数则设置为空即可，服务器立即返回数据。
	 * @param	string $sessionKey
	 * @return	array | false
	 */
	public function getAppIds( $syncFlag = self::REQUEST_TYPE_ASYNCHRONOUS , $sessionKey = '' )
	{
		if(  $this->platform == "minik"  )
		{
			$info = Minik_Api::getAppIds( $this->userId );
			return $info;
		}
		elseif (   $this->platform == "fminutes"  )
		{
			$info = Fminutes_Friend::getAppIds();
			return $info;
		}
		

		if( self::$client )
		{
			$info = self::$client->get_app_ids( $this->userId , $syncFlag , $sessionKey );
			if( $info )
			{
				return json_decode( $info , true );
			}
		}
		return false;
	}
	
	/**
	 * 获得所有的好友的id列表
	 *
	 * @param	int $syncFlag		//1:表示为同步请求，session_key参数必须设置，服务器若在缓存中没有此uid数据，
	 * 								则立即去平台获取数据，并将数据返回。
	 * 								0:则为异步接口，session_key参数则设置为空即可，服务器立即返回数据。
	 * @param	string $sessionKey
	 * @return	array | false
	 */
	public function getIds( $syncFlag = self::REQUEST_TYPE_ASYNCHRONOUS , $sessionKey = '' )
	{
	
		if(  $this->platform == "minik"  )
		{
			$info = Minik_Api::getIds( $this->userId );
			return $info;
		}
		elseif (   $this->platform == "fminutes"  )
		{
			$info = Fminutes_Friend::getIds();
			return $info;
		}
		
		
		
		if( self::$client )
		{
			$info = self::$client->get_all_ids( $this->userId , $syncFlag , $sessionKey );
			if( $info )
			{
				return json_decode( $info , true );
			}
		}
		
		return false;
	}
	
	/**
	 * 获得安装了app的好友的属性信息列表
	 * @param	int $syncFlag		//1:表示为同步请求，session_key参数必须设置，服务器若在缓存中没有此uid数据，
	 * 								则立即去平台获取数据，并将数据返回。
	 * 								0:则为异步接口，session_key参数则设置为空即可，服务器立即返回数据。
	 * @param	string $sessionKey
	 * @return	array | false
	 */
	public function getAppFriendList( $syncFlag = self::REQUEST_TYPE_ASYNCHRONOUS , $sessionKey = '' )
	{
		if( $this->useApp == true )
		{
			$info = Minik_Api::getAppFriendList( $this->userId );
			return $info;
		}
		
		
		if( self::$client )
		{
			$info = self::$client->get_app_fds( $this->userId , $syncFlag , $sessionKey );
			if( $info )
			{
				return json_decode( $info , true );
			}
		}
		
		return false;
	}
	
	/**
	 * 获得所有的好友的属性信息列表
	 * @param	int $syncFlag		//1:表示为同步请求，session_key参数必须设置，服务器若在缓存中没有此uid数据，
	 * 								则立即去平台获取数据，并将数据返回。
	 * 								0:则为异步接口，session_key参数则设置为空即可，服务器立即返回数据。
	 * @param	string $sessionKey
	 * @return	array | false
	 */
	public function getFriendList( $syncFlag = self::REQUEST_TYPE_ASYNCHRONOUS , $sessionKey = '' )
	{
		if( $this->useApp == true )
		{
			$info = Minik_Api::getFriendList( $this->userId );
			return $info;
		}
		
		if( self::$client )
		{
			$info = self::$client->get_all_fds( $this->userId , $syncFlag , $sessionKey );
			if( $info )
			{
				return json_decode( $info , true );
			}
		}
		
		return false;
	}
	
	/**
	 * 判断两个安装了app的uid是否是好友
	 *
	 * @param	int $friendId	好友ID
	 * @return	boolean
	 */
	public function areAppFriends( $friendId )
	{
		if( $this->useApp == true )
		{
			$info = Minik_Api::areAppFriends( $friendId );
			return $info;
		}
		
		
		if( self::$client )
		{
			return self::$client->check_app_fd( $this->userId , $friendId );
		}
	}
	
	/**
	 * 判断两个uid是否是好友
	 * @param	int $friendId	好友ID
	 * @return	boolean
	 */
	public function areFriends( $friendId )
	{
		if( self::$client )
		{
			return self::$client->check_all_fd( $this->userId , $friendId );
		}
	}
	
	/**
	 * 判断user是否为app的关注者
	 * @return	boolean
	 */
	public function isFan()
	{
		if( self::$client )
		{
			return self::$client->check_app_fans( $this->userId );
		}
	}
	
	/**
	 * Qzone VIP用户
	 * @param int $syncFlag
	 * @param string $sessionKey
	 */

	public function getVipInfo(  $userId , $syncFlag = self::REQUEST_TYPE_ASYNCHRONOUS ,  $sessionKey = '' )
	{
		$platform = Common::getConfig( 'platform' );
		$returnData = array();
		if( self::$client  && $platform == 'qzone' )
		{
			$info =  self::$client->check_vip_info( $userId ,  $syncFlag , $sessionKey );
			$decodeInfo = json_decode( $info , true  );
			$returnData = array(
				'is_vip' => ( isset( $decodeInfo['is_vip'] )  &&   $decodeInfo['is_vip']  == true ) ? 1: 0 ,
				'is_year_vip' =>  ( isset( $decodeInfo['is_year_vip'] )  &&   $decodeInfo['is_year_vip']  == true ) ? 1: 0 ,
				'vip_level' =>  ( isset( $decodeInfo['vip_level'] )  &&   $decodeInfo['vip_level'] >  0 ) ?  $decodeInfo['vip_level'] : 0,
			);
		}
		return $returnData;
	}
	
}
