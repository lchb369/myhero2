<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 统计器
 * @author	wzhzhang , Luckyboys , simon
 * @since	2010-10-11	Luckyboys	从游戏框架代码中拷贝并修改
 */
class FM_Accumulator
{
	/**
	 * 统计器配置
	 * @var	array
	 */
	protected $config;
	
	/**
	 * 行为编号对应的值
	 * @var	int[]
	 */
	protected $keys;
	
	/**
	 * 单例模式对象
	 * @var	FM_Accumulator
	 */
	protected static $singletonObject = null;
	
	/**
	 * C++统计服务客户端
	 * @var	fm_accumulate
	 */
	protected static $client = null;
	
	/**
	 * C++统计服务客户端（UDP版）
	 * @var	fm_accumulate_udp
	 */
	protected static $udpClient = null;
	
	/**
	 * 用户ID
	 * @var	int
	 */
	protected $userId = 0;
	
	/**
	 * API速度统计行为
	 * @var int
	 */
	const ACTION_API_TRACE_TIME = 10001;
	
	/**
	 * Tag Data 类型（第一次安装游戏时游戏的好友数）
	 * @var	int
	 */
	const TAG_DATA_TYPE_FIRST_INSTALL_GAME_FRIEND_COUNT = 1;
	
	/**
	 * Tag Data 类型（第一次安装游戏时平台的好友数）
	 * @var	int
	 */
	const TAG_DATA_TYPE_FIRST_INSTALL_GAME_PLATFORM_FRIEND_COUNT = 2;
	
	/**
	 * Tag Data 类型（第一次购买的物品ID）
	 * @var	int
	 */
	const TAG_DATA_TYPE_FIRST_BOUGHT_GOODS_ID = 3;
	
	/**
	 * User Data 类型（等级）
	 * @var	int
	 */
	const USER_DATA_TYPE_LEVEL = 1;
	
	/**
	 * User Data 类型（邻居数）
	 * @var	int
	 */
	const USER_DATA_TYPE_NEIGHBOR_COUNT = 2;
	
	/**
	 * User Data 类型（最后登录时间）
	 * @var	int
	 */
	const USER_DATA_TYPE_LAST_LOGIN_TIME = 3;
	
	/**
	 * User Data 类型（最后充值时间）
	 * @var	int
	 */
	const USER_DATA_TYPE_LAST_CHARGE_TIME = 4;
	
	/**
	 * User Data 类型（平台好友数）
	 * @var	int
	 */
	const USER_DATA_TYPE_PLATFORM_FRIEND_COUNT = 5;
	
	/**
	 * User Data 类型（经验值）
	 * @var	int
	 */
	const USER_DATA_TYPE_EXP = 6;
	
	/**
	 * user统计的type类型（增加）
	 * @var	int
	 */
	const USER_ACCUMULATE_TYPE_ADD = 1;
	
	/**
	 * user统计的type类型（替换）
	 * @var	int
	 */
	const USER_ACCUMULATE_TYPE_REPLACE = 2;
	
	/**
	 * user统计的type类型（按位或）
	 * @var	int
	 */
	const USER_ACCUMULATE_TYPE_OR_OPERATION = 3;
	
	/**
	 * 获取统计模块的单例
	 * @return	FM_Accumulator
	 */
	public static function & getInstance( $userId )
	{
		if( self::$singletonObject == null )
		{
			self::$singletonObject = new FM_Accumulator();
		}
		
		self::$singletonObject->userId = $userId;
		return self::$singletonObject;
	}
	
	/**
	 * 实例化统计模块
	 */
	protected function __construct()
	{
		$this->config = & Common::getConfig( 'statisticsServer' );
		
		try
		{
			if( class_exists( 'fm_accumulate' ) && $this->config['host'] && $this->config['port'] )
			{
				self::$client = new fm_accumulate( $this->config['host'] , $this->config['port'] );
			}
		}
		catch( Exception $ex )
		{
			;
		}
		
		try
		{
			if( class_exists( 'fm_accumulate_udp' ) && $this->config['udpHost'] && $this->config['udpPort'] )
			{
				self::$udpClient = new fm_accumulate_udp( $this->config['udpHost'] , $this->config['udpPort'] );
			}
		}
		catch( Exception $ex )
		{
			;
		}
	}
	
	/**
	 * 发送登录信息
	 */
	public function login()
	{
		if( self::$client )
		{
			self::$client->login( $this->userId );
		}
		
		if( self::$udpClient )
		{
			self::$udpClient->login( $this->userId );
		}
	}
	
	/**
	 * 行为统计
	 * @param	int $action	//行为编号
	 * @param	int $object	//目标编号(范围 1 - 999999)
	 * @param	int $attribute //属性编号(范围 1 - 99)
	 * @param	int $attributeValue	//属性值编号(范围 1 - 99)
	 * @param	int $incrementCount	//统计的值
	 */
	public function action( $action , $object , $attribute , $attributeValue , $incrementCount = 1 , $userId = null )
	{
		if( self::$client )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			try
			{
				self::$client->action( $action , $object , $attribute , $attributeValue , $incrementCount , $userId );
			}
			catch( Exception $ex )
			{
				;
			}
		}
		
		if( self::$udpClient )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			try
			{
				self::$udpClient->action( $action , $object , $attribute , $attributeValue , $incrementCount , $userId );
			}
			catch( Exception $ex )
			{
				;
			}
		}
	}

	/**
	 * 用户统计：用于统计具体属于某种类型（值）的用户的统计
	 * @param	int $userId	//用户ID
	 * @param	int $key	//类型(预先定义exp:1  gold:2)
	 * @param	int $value	//值
	 */
	public function user( $key , $value , $type = self::USER_ACCUMULATE_TYPE_ADD , $userId = null )
	{
		if( self::$client )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			try
			{
				self::$client->user( $userId , $key , $value , $type );
			}
			catch( Exception $ex )
			{
				;
			}
		}
		
		if( self::$udpClient )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			try
			{
				self::$udpClient->user( $userId , $key , $value , $type );
			}
			catch( Exception $ex )
			{
				;
			}
		}
	}
	
	/**
	 * 标签统计
	 * @param	int $userId //用户编号
	 * @param	int $tagPos //要设置的标签(tag)所在的位，首次添加用户标签时该值应设置为0
	 * @param	int $regTime //用户注册时间(UNIX时间戳)
	 */
	public function tagSet( $tagPos = 0 , $regTime = 0 , $userId = null )
	{
		if( self::$client )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$client->tag_set( $userId , $tagPos , $regTime );
		}
		
		if( self::$udpClient )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$udpClient->tag_set( $userId , $tagPos , $regTime );
		}
	}
	
	/**
	 * 单个用户的批量行为统计
	 * @param	array $actionAttr		Array(
	 * 										Array(
	 * 											"action" => int,
	 * 											"object" => int,
	 * 											"attribute" => int,
	 * 											"attribute_value" => int,
	 * 											"increment_count" => int
	 * 										),
	 * 										...
	 * 									)
	 */
	public function actionArray( $actionAttr , $userId = null )
	{
		if( self::$client )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$client->action_array( $userId , $actionAttr );
		}
		
		if( self::$udpClient )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$udpClient->action_array( $userId , $actionAttr );
		}
	}
	
	/**
	 * 设置用户的可变更数据
	 * @param	int $field 	需要修改的字段ID
	 * @param	int $value	设置的值
	 */
	public function setUserData( $field = 0 , $value = 0 , $userId = null )
	{
		if( self::$client )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$client->set_user_data( $userId , $field , $value );
		}
		
		if( self::$udpClient )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$udpClient->set_user_data( $userId , $field , $value );
		}
	}
	
	
	/**
	 * 主要统计游戏中的单一行为多种属性，可以按照时间间隔写入数据库；当前版本，时间间隔按照小时作为单位
	 * @param	int $action
	 * @param	int $object
	 * @param	int $attr TODO:应该合并为一个， 但接口文档如此写， 有待确定。
	 * @param	int $attr_value TODO:应该合并为一个， 但接口文档如此写， 有待确定。
	 * @param	int $increment_count
	 */
	public function actionMultiAttr( $action , $object , $incrementCount , $attr , $attrValue , $userId = null )
	{
		if( self::$client )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$client->action_multi_attr( $userId , $action , $object , $attr , $attrValue , $incrementCount );
		}
		
		if( self::$udpClient )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$udpClient->action_multi_attr( $userId , $action , $object , $attr , $attrValue , $incrementCount );
		}
	}
 
	
	/**
	 * 设置用户标签内保留时间接口
	 * 
	 * @param	int $firstRechargeTime 		首次充值时间(UNIX时间戳)  如果不想修改数据库中的这个值则传0
	 * @param	int $timeExt2				保留时间(UNIX时间戳) 如果不想修改数据库中的这个值则传0
	 */
	public function setTagTime( $firstRechargeTime = 0 , $timeExt2 = 0 , $userId = null )
	{
		if( self::$client )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$client->set_tag_time( $userId , $firstRechargeTime , $timeExt2 );
		}
		
		if( self::$udpClient )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$udpClient->set_tag_time( $userId , $firstRechargeTime , $timeExt2 );
		}
	}
	
	/**
	 * 设置用户标签内扩展字段接口
	 * 
	 * @param	int $firstRechargeTime 		首次充值时间(UNIX时间戳)  如果不想修改数据库中的这个值则传0
	 * @param	int $timeExt2				保留时间(UNIX时间戳) 如果不想修改数据库中的这个值则传0
	 */
	public function setTagData( $field , $value , $userId = null )
	{
		if( self::$client )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$client->set_tag_data( $userId , $field , $value );
		}
		
		if( self::$udpClient )
		{
			if( $userId === null )
			{
				$userId = $this->userId;
			}
			self::$udpClient->set_tag_data( $userId , $field , $value );
		}
	}
	
	/**
	 * 析构
	 */
	public function __destruct()
	{
		if( self::$client )
		{
			self::$client->send();
		}
		
		if( self::$udpClient )
		{
			self::$udpClient->send();
		}
	}
}

?>