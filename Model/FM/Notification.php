<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * FM 通知类
 * @author	Luckyboys
 */
class FM_Notification
{
	/**
	 * 单例模式对象
	 * @var	FM_Notification
	 */
	protected static $singletonObject = null;
	
	/**
	 * C++通知服务客户端
	 * @var	fm_msg
	 */
	protected $client = null;
	
	/**
	 * 通知系统配置
	 * @var	array
	 */
	protected $config;
	
	/**
	 * 实例化
	 */
	protected function __construct()
	{
		$this->config = & Common::getConfig( 'notificationServer' );
		try
		{
			if( class_exists( 'fm_msg' ) && $this->config['host'] && $this->config['port'] )
			{
				$this->client = new fm_msg( $this->config['host'] , $this->config['port'] );
			}
		}
		catch( Exception $ex )
		{
			;
		}
	}
	
	/**
	 * 获取一个Http代理类实例
	 * @return	FM_Notification
	 */
	public static function & getInstance()
	{
		if( self::$singletonObject == null )
		{
			self::$singletonObject = new FM_Notification();
		}
		
		return self::$singletonObject;
	}
	
	/**
	 * 用户登陆
	 * @param	int $userId	用户ID
	 * @param	string $sessionKey	Session Key
	 * @return	boolean
	 */
	public function login( $userId , $sessionKey )
	{
		if( $this->client )
		{
			return $this->client->login( $userId , $sessionKey );
		}
		return true;
	}
	
	/**
	 * 发送消息给指定用户
	 * @param	int $userId	用户ID
	 * @param	int|int[] $targetUserId	目标用户ID
	 * @param	string $message	信息
	 * @param	boolean $canIReceive	是否我自己也会接收到信息
	 * @return	boolean
	 */
	public function sendMessage( $userId , $targetUserIds , $message , $canIReceive = false )
	{
		if( $this->client )
		{
			if( is_array( $targetUserIds ) )
			{
				return $this->client->multicast( $userId , $targetUserIds , $canIReceive , $message );
			}
			else 
			{
				return $this->client->unicast( $userId , $targetUserIds , $canIReceive , $message );
			}
		}
		return true;
	}
	
	/**
	 * 发送消息给在线好友
	 * @param	int $userId	用户ID
	 * @param	string $message	信息
	 * @param	boolean $canIReceive	是否我自己也会接收到信息
	 * @return	boolean
	 */
	public function sendMessageToOnlineFriend( $userId , $message , $canIReceive = false )
	{
		if( $this->client )
		{
			return $this->client->friend_broadcast( $userId , $canIReceive , $message );
		}
		return true;
	}
	
	/**
	 * 发送消息给所有在线用户
	 * @param	int $userId	用户ID
	 * @param	string $message	信息
	 * @param	boolean $canIReceive	是否我自己也会接收到信息
	 * @return	boolean
	 */
	public function sendMessageToAllOnlineUser( $userId , $message , $canIReceive = false )
	{
		if( $this->client )
		{
			return $this->client->world_broadcast( $userId , $canIReceive , $message );
		}
		return true;
	}
	
	/**
	 * 获取在线好友ID
	 * @param	int $userId	用户ID
	 * @return	int[]
	 */
	public function getOnlineFriendIds( $userId )
	{
		if( $this->client )
		{
			$data = json_decode( $this->client->get_online_ids( $userId ) , true );
			return $data ? $data : array();
		}
		return array();
	}
	
	/**
	 * 检查用户是否在线
	 * @param	int $userId	用户ID
	 * @return	boolean
	 */
	public function isOnline( $userId )
	{
		if( $this->client )
		{
			return json_decode( $this->client->check_online( $userId ) , true );
		}
		return false;
	}
}

?>
