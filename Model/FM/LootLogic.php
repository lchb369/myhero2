<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class FM_LootLogic
{
	/**
	 * 单例模式对象
	 * @var	FM_Notification
	 */
	protected static $singletonObject = null;
	
	/**
	 * C++通知服务客户端
	 * @var	fm_msg
	 */
	protected static $client = null;
	
	/**
	 * C++通知服务客户端（从属）
	 * @var	fm_msg
	 */
	protected static $slaveClient = null;
	
	/**
	 * 通知系统配置
	 * @var	array
	 */
	protected $config;
	
	/**
	 * 实例化
	 */
	protected function __construct()
	{
		$this->config = & Common::getConfig( 'lootLogicServer' );
		try
		{
			if( class_exists( 'fm_logic' ) && $this->config['host'] && $this->config['port'] )
			{
				self::$client = new fm_logic( $this->config['host'] , $this->config['port'] );
				if( isset( $this->config['slaveHost'] ) && isset( $this->config['slavePort'] ) )
				{
					self::$slaveClient = new fm_logic( $this->config['slaveHost'] , $this->config['slavePort'] );
				}
			}
		}
		catch( Exception $ex )
		{
			;
		}
	}
	
	/**
	 * 获取一个Http代理类实例
	 * @return	FM_LootLogic
	 */
	public static function & getInstance()
	{
		if( self::$singletonObject == null )
		{
			self::$singletonObject = new FM_LootLogic();
		}
		
		return self::$singletonObject;
	}
	
	/**
	 * 用户登陆
	 * @param	int $userId	用户ID
	 * @param	int $level	用户等级
	 * @param	int $arenaScore	新的战斗评分
	 * @return	string
	 */
	public function login( $userId , $level ,  $arenaScore , $ext = '{}' )
	{
		if( self::$slaveClient )
		{
			self::$slaveClient->login( $userId , $level ,  $arenaScore , $ext );
		}
		
		if( self::$client )
		{
			return self::$client->login( $userId , $level , $arenaScore , $ext );
		}
		return 'old';
	}
	
	/**
	 * 竞技场登陆
	 * @param	int $userId	用户ID
	 * @param	int $level	用户等级
	 * @param	int $arenaScore	新的战斗评分
	 * @return	string
	 */
	public function arenaLogin( $userId , $level ,  $arenaScore , $ext = '{}' )
	{
		if( self::$slaveClient )
		{
			self::$slaveClient->arena_login( $userId , $level ,  $arenaScore , $ext );
		}
		
		if( self::$client )
		{
			return self::$client->arena_login( $userId , $level , $arenaScore , $ext );
		}
		return 'old';
	}
	
	/**
	 * 取得竞技场对手
	 * @param	int $userId	用户ID
	 * @param	int $level	用户等级
	 * @param	int $arenaScore	新的战斗评分
	 * @return array
	 */
	public function getSeasonArenaObject( $userId , $level , $arenaScore )
	{
		if( self::$client )
		{
			$result = self::$client->arena_get_obj( $userId , $level , $arenaScore );
			if( !$result )
			{
				return array();
			}
			return intval($result);
		}
		return true;
	}
	
	public function getSeasonArenaUserCount( $level )
	{
		if( self::$client )
		{
			$result = self::$client->arena_user_count( $level );
			if( !$result )
			{
				return array();
			}
			else 
			{
				$tmp = str_replace( ":", '":', $result );
				$tmp = '{"'.$tmp.'}';
				$result = json_decode( $tmp , true );
				return $result['arena_list_num'][0];
			}
		}
		return true;
	}
	/**
	 * 更新竞技场信息
	 * @param	int $userId	用户ID
	 * @param	int $level	用户等级
	 * @param	int $arenaScore	新的战斗评分
	 * @param 	bool $isOnline 是否在线
	 * @return array
	 */
	public function updateSeasonArenaAttr( $userId , $level , $arenaScore , $isOnline , $ext = '{}' )
	{
		if( self::$client )
		{
			$result = self::$client->arena_update_attr( $userId , $level , $arenaScore , $ext , $isOnline );
			if( !$result )
			{
				return array();
			}
			return json_decode( $result , true );
		}
		return true;
	}
	/**
	 * 批量添加物品
	 * @param	int $userId	用户ID
	 * @param	int $itemIds	道具列表
	 * @return	boolean
	 */
	public function addItemList( $userId , $itemIds )
	{
		if( self::$client )
		{
			return self::$client->add_item_list( $userId , count( $itemIds ) , $itemIds );
		}
		return true;
	}
	
	/**
	 * 更新用户属性
	 * @param	int $userId	用户ID
	 * @param	int $level	用户等级
	 * @param	int $arenaScore	新的战斗评分
	 * @return	boolean
	 */
	public function updateUser( $userId ,  $level , $arenaScore , $ext = '{}' )
	{
		if( self::$slaveClient )
		{
			self::$slaveClient->update_attribute( $userId , $level , $arenaScore , $ext );
		}
		
		if( self::$client )
		{
			return self::$client->update_attribute( $userId , $level , $arenaScore , $ext );
		}
		return true;
	}
	
	/**
	 * 更新用户单个物品
	 * @param	int $userId	用户ID
	 * @param	int $itemId	道具ID
	 * @param	boolean $haveItemChanged	道具数量是否发生了拥有状态变更（true => 从拥有0个变成拥有1或多个；false => 从拥有1或多个变成拥有0个）
	 * @return	boolean
	 */
	public function updateItem( $userId , $itemId , $haveItemChanged )
	{
		if( self::$client )
		{
			return self::$client->update_item( $userId , $itemId , $haveItemChanged ? 1 : 0 );
		}
		return true;
	}
	
	/**
	 * 获取可以打劫用户列表
	 * @param	int $userId	用户ID
	 * @param	int $level	用户等级
	 * @param	int $arenaScore	新的战斗评分
	 * @param	int $itemId	道具ID
	 * @return	boolean
	 */
	public function getRecommendLootList( $userId , $level , $arenaScore , $items )
	{
		if( self::$client )
		{
			$result = self::$client->get_plunder_list( $userId , $level , $arenaScore , count( $items ) , $items );
			if( !$result )
			{
				return array();
			}
			return json_decode( $result , true );
		}
		return true;
	}
	
	/**
	 * 设置打劫成功
	 * @param	int $lootUserId	打劫者的用户ID
	 * @param	int $beLootUserId	被打劫者的用户ID
	 * @param	int $itemId	被打劫的道具ID
	 * @param	boolean $havingItems	被打劫者是否还拥有被打劫的道具（ true => 还拥有被打劫的道具；false => 被打劫的道具被打劫完了）
	 * @return	boolean
	 */
	public function setBeLoot( $lootUserId , $beLootUserId )
	{
		if( self::$slaveClient )
		{
			self::$slaveClient->plunder_success( $beLootUserId , $lootUserId );
		}
		if( self::$client )
		{
			return self::$client->plunder_success( $beLootUserId , $lootUserId );
		}
		return true;
	}
	
	/**
	 * 检查是否在线
	 * @param	int $userId	用户ID
	 * @return	boolean
	 */
	public function checkOnline( $userId )
	{
		if( self::$client )
		{
			return self::$client->check_online( $userId );
		}
		return false;
	}
	
	/**
	 * 移除物品列表
	 * @param	int $userId	用户ID
	 * @param	array $itemList 物品列表
	 * @return	boolean
	 */
	public function deleteItemList( $userId , $itemList )
	{
		if( self::$client )
		{
			$num = count( $itemList );
			if( $num > 0 )
			{
				return self::$client->delete_item_list( $userId , $num , $itemList );
			}
			else 
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 获取服务器信息
	 * @return	array
	 */
	public function getServiceInfo()
	{
		if( self::$client )
		{
			$data = self::$client->service_info();
			$data['server_status'] = json_decode( $data['server_status'] , true );
			$data['server_details'] = json_decode( $data['server_details'] , true );
			return $data;
		}
		return array();
	}
	
	/**
	 * 实时修改服务器配置信息
	 * @param	int	$safeIntervalTime	打劫保护期间隔时间(-1为不改变当前系统值)
	 * @param	int $safeTimeClearFlag	调用login接口时,是否清楚用户打劫保护期(1:清除 0:不清除) .-1为不改变当前系统值
	 * @return	boolean
	 */
	public function setServerConfig( $topWaterMark , $bottomWaterMark , $safeIntervalTime , $safeTimeClearFlag )
	{
		if( self::$client )
		{
			return self::$client->set_server_config( $topWaterMark , $bottomWaterMark , $safeIntervalTime , $safeTimeClearFlag );
		}
		return true;
	}
	
	/**
	 * 获取一定战斗积分所在分段的人数
	 * @param	int	$arenaScore	积分段
	 * @return	int
	 */
	public function getLootUserCount( $arenaScore )
	{
		if( self::$client )
		{
			return self::$client->get_plunder_user_count( $arenaScore );
		}
		return 0;
	}
	
	/**
	 * 
	 * 大作战埋伏登入接口
	 * @param int $userId
	 * @param int $level
	 * @param int $popularityLevel
	 */
	public function ambushLogin( $userId , $level , $popularityLevel , $ext = '' ) 
	{
		if( self::$client )
		{
			return self::$client->loot_user_login( $userId , $level , $popularityLevel , $ext );
		}
	}
	/**
	 * 获取打劫人
	 * @param int $userId
	 * @param int $level
	 * @param int $popularityLevel
	 */
	public function getAmbushUser(  $userId , $level , $popularityLevel )
	{
		if( self::$client )
		{
			return self::$client->get_loot_user( $userId , $level , $popularityLevel );
		}
		return 0;
	}
	
	/**
	 * 获取一定实力评分所在分段的人数
	 */
	public function getTackleFightUserCount( $fightScore )
	{
		if( self::$client )
		{
			return self::$client->get_loot_user_count( $fightScore );
		}
		return 0;
	}
}

?>
