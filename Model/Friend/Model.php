<?php
/**
 * 好友模块
 * 
 * @name Model.php
 * @author Liuchangbing
 * @since 2013-1-14
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Friend_Model extends Logical_Abstract
{
	public static $giftBonus = array(
		'gold' => 100,
	);
	
	public static $inviteThirdNumBonus = array(
		10 => array('gold' => 10000),
		20 => array('gold' => 30000),
		40 => array('coin' => 30),
	); 
	
	const CACHE_KEY_FRIEND_THIRD_BE_INVITED = "friend_third_be_invited_";
	const CACHE_KEY_FRIEND_THIRD_INVITE = "friend_third_invite_";
	const CACHE_KEY_FRIEND_SEND_GIFT = "friend_send_gift_";
	const CACHE_KEY_FRIEND_SEND_GIFT_RECEIVE = "friend_send_gift_receive_";
	
	const LOCK_KEY_ACTIVITY_INVITE = "activity_invite_";
	
	const INVITE_THIRD_LIMIT_DAY = 30;
	const INVITE_THIRD_LIMIT_MONTH = 40;
	
	const SEND_GIFT_LIMIT_DAY = 10;
	const GET_GIFT_LIMIT_DAY = 10;
	
	/**
	 * 单例对象
	 * @var	Friend_Model[]
	 */
	protected static $singletonObjects;

	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Friend_Model
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 是否好友
	 * @param int $fId
	 */
	public function areFriend( $fId )
	{
		return Data_Friend_Model::getInstance( $this->userId )->areFriend( $fId );
	}
	
	/**
	 * 邀请好友请求
	 * 将我加入到好友的请求列表中
	 * @param int $fId
	 */
	public function invite( $fId )
	{
		self::checkFriendlimit( $this->userId );
		self::checkFriendlimit( $fId , false , true );
		//我是否已经在对方的好友列表里
		if(Friend_Model::getInstance($fId)->areFriend($this->userId))
		{
			//直接加为好友
			Data_Friend_Model::getInstance( $this->userId , true )->addFriend( $fId );
		}
		else
		{
			//发送好友请求
			Data_Friend_Request::getInstance( $fId , true )->addReq( $this->userId );
		}
	}
	
	
	/**
	 * 接受好友的邀请
	 * @param int $fid 好友ID
	 */
	public function accept( $fId )
	{
		self::checkFriendlimit( $this->userId );
		self::checkFriendlimit( $fId , false , true );
		//好友是否向我发起过申请请求，是否在我的请求列表中
		$requestList = $this->getFriendReqs();
		if( !$requestList[$fId] )
		{
			throw new Friend_Exception( Friend_Exception::STATUS_NO_REQUEST );
		}

		//加为好友
		Data_Friend_Model::getInstance( $this->userId , true )->addFriend( $fId );
		
		//互加好友
		Data_Friend_Model::getInstance( $fId , true )->addFriend( $this->userId );
		
		//删掉申请请求
		Data_Friend_Request::getInstance( $this->userId , true )->removeReq( $fId );
	
	}
	
	/**
	 * 批量接受好友的邀请
	 * @param int $fid 好友ID
	 */
	public function acceptAll()
	{
		$ret = array('success' => 0, 'fail' => 0);
		
		do{
			
			$requestList = $this->getFriendReqs();
			if(empty($requestList)) break;
			
			foreach($requestList as $fId => $req)
			{
				$checkFriendLimit = self::checkFriendlimit($fId,true);
				$checkMyFriendLimit = self::checkFriendlimit($this->userId, true); 
				if($checkFriendLimit || $checkMyFriendLimit)
				{
					$ret['fail']++;
					continue;
				}
				
				$ret['success']++;
				//加为好友
				Data_Friend_Model::getInstance( $this->userId , true )->addFriend( $fId );
				
				//互加好友
				Data_Friend_Model::getInstance( $fId , true )->addFriend( $this->userId );
				
				//删掉申请请求
				Data_Friend_Request::getInstance( $this->userId , true )->removeReq( $fId );
			}
		}while(0);
		
		return $ret;
	}
	
	/**
	 * 拒绝好友的邀请
	 * @param int $fid 好友ID
	 */
	public function reject( $fId )
	{
		//好友是否向我发起过申请请求，是否在我的请求列表中
		$requestList = $this->getFriendReqs();
		if( !$requestList[$fId] )
		{
			throw new Friend_Exception( Friend_Exception::STATUS_NO_REQUEST );
		}
		//删掉申请请求
		Data_Friend_Request::getInstance( $this->userId , true )->removeReq( $fId );
	}
	
	
	/**
	 * 移除一个好友
	 * @param int $fid 好友ID
	 */
	public function removeFriend( $fId )
	{
		Data_Friend_Model::getInstance( $this->userId , true )->removeFriend( $fId );
	}
	
	
	/**
	 * 是否达到好友上限
	 */
	public static function checkFriendlimit( $userId , $ignoreErr = false , $isOpposite = false )
	{
		$userInfo = User_Info::getInstance( $userId )->getData();
		$friends = Data_Friend_Model::getInstance( $userId )->getData();
	
		$ret = false;
		if( count( $friends ) >= $userInfo['maxFriendNum'] )
		{
			if( !$ignoreErr )
			{
				//是否为对方好友超上限
				if( $isOpposite ) throw new User_Exception( User_Exception::STATUS_OPPOSITE_FRIEND_MAX );
				
				throw new User_Exception( User_Exception::STATUS_FRIEND_MAX );
			}
			$ret = true;
		}
		return $ret;
	}
	
	/**
	 * 获取好友列表
	 */
	public function getFriends()
	{
		$friends = Data_Friend_Model::getInstance( $this->userId )->getData();
		foreach($friends as $friendId => &$friend)
		{
			if(!User_Model::exist($friendId)) unset($friends[$friendId]);
		}
		return $friends;
	}
	
	/**
	 * 获取好友请求列表
	 */
	public function getFriendReqs()
	{
		$friendReqs = Data_Friend_Request::getInstance( $this->userId )->getData();
		foreach($friendReqs as $friendId => &$friend)
		{
			if(!User_Model::exist($friendId)) unset($friendReqs[$friendId]);
		}
		return $friendReqs;
	}
	
	/**
	 * 第三方平台好友邀请历史记录
	 * @param unknown $userId
	 * @return multitype:NULL multitype:multitype:number   mixed
	 */
	public static function inviteThirdHistory($userId)
	{
		
		$actInvite = Data_Activity_Model::getInstance($userId)->getActivity(Activity_Model::ACTIVE_INVITE_THIRD);
		
		$cache = Common::getCache();
		$keyInvite = self::CACHE_KEY_FRIEND_THIRD_INVITE.$userId;
		$inviteData = $cache->get($keyInvite);
		$dailyLimit = false;
		$monthLimit = false;
		
		if(!empty($inviteData))
		{
			//每日邀请
			$dayNum = 0;
			$dayStartTime = Helper_Date::getDayStartTime();
			//每月邀请
			$monthNum = 0;
			$monthStartTime = Helper_Date::getMonthStartTime();
				
			$verify = true;
			foreach($inviteData as $inviteId => &$data)
			{
				//每日限制
				if($data > $dayStartTime && ++$dayNum > self::INVITE_THIRD_LIMIT_DAY){$dailyLimit = true;}
				//每月限制
				if($data > $monthStartTime && ++$monthNum > self::INVITE_THIRD_LIMIT_MONTH){$monthLimit = true;}
				
				
				//30天1个人只能邀请一次
				$can = !($data > $_SERVER['REQUEST_TIME'] - 30 * 86400);
				
				$data = array(
					'can' => $can,
					'time' => $data
				);
			}
		}
		
		return array(
			'dailyLimit' => $dailyLimit,
			'monthLimit' => $monthLimit,
			'inviteData' => $inviteData ? $inviteData : null,
			'inviteNum' => intval($actInvite['data']['num']),
			'inviteBonusConf' => self::$inviteThirdNumBonus,
			'inviteBonus' => $actInvite['data']['numBonus'],
		);
	}
	
	/**
	 * 第三方平台好友邀请
	 */
	public static function inviteThird($userId, $thirdId)
	{
		$ret = array( 'errorCode' => Friend_Exception::STATUS_INVITE_FAIL );
		
		do{
			$cache = Common::getCache();
			
			//邀请人加锁
			$keyInvite = self::CACHE_KEY_FRIEND_THIRD_INVITE.$userId;
			if(!!$lockInvite = Helper_Common::addLock($keyInvite))
			{
				if(!!$inviteData = $cache->get($keyInvite))
				{
					//30天1个人只能邀请一次
					if($inviteData[$thirdId] && $inviteData[$thirdId] > $_SERVER['REQUEST_TIME'] - 30 * 86400) break;
					
					//每日邀请
					$dayNum = 0;
					$dayStartTime = Helper_Date::getDayStartTime();
					//每月邀请
					$monthNum = 0;
					$monthStartTime = Helper_Date::getMonthStartTime();
					
					$verify = true;
					foreach($inviteData as $inviteId => &$data)
					{
						//每日限制
						if($data > $dayStartTime && ++$dayNum >= self::INVITE_THIRD_LIMIT_DAY){$verify = false;break;}
						//每月限制
						if($data > $monthStartTime && ++$monthNum >= self::INVITE_THIRD_LIMIT_MONTH){$verify = false;break;}
						//32天以外的邀请都清除
						if($data <= $_SERVER['REQUEST_TIME'] - 32 * 86400) unset($inviteData[$inviteId]);
					}
					if(!$verify) break;
				}
				//保存邀请信息
				$inviteData[$thirdId] = $_SERVER['REQUEST_TIME'];
				$cache->set($keyInvite, $inviteData);

				//发送邀请就算奖励
				$keyActInvite = self::LOCK_KEY_ACTIVITY_INVITE.$thirdId;
				if(!!$lockActInvite = Helper_Common::addLock($keyActInvite))
				{
					//获得邀请信息
					$active = Data_Activity_Model::getInstance($userId)->getActivity(Activity_Model::ACTIVE_INVITE_THIRD);
					$active['data']['num']++;
					//总邀请数奖励
					foreach(self::$inviteThirdNumBonus as $bonusNum => &$bonusConf)
					{
						if($active['data']['num'] < $bonusNum) break;
						if(isset($active['data']['numBonus'][$bonusNum])) continue;
				
						$active['data']['numBonus'][$bonusNum] = 0;
					}
					Data_Activity_Model::getInstance($userId, true)->updateRecord(Activity_Model::ACTIVE_INVITE_THIRD, $active['data']);
				}
				if($lockActInvite) Helper_Common::delLock($keyActInvite);
				
				
				//保存被邀请信息
				//被邀请人加锁
// 				$keyBeInvited = self::CACHE_KEY_FRIEND_THIRD_BE_INVITED.$thirdId;
// 				if(!!$lockBeInvited = Helper_Common::addLock($keyBeInvited))
// 				{
// 					//被哪些人邀请了
// 					$beInvitedData = $cache->get($keyBeInvited);
// 					$beInvitedData[$userId] = $_SERVER['REQUEST_TIME'];
					
// 					$cache->set($keyBeInvited, $beInvitedData);
					
// 					Helper_Common::delLock($keyBeInvited);
// 				}
				
				$ret = array('tId' => $thirdId);
				
			}
			
		}while(0);
		
		if($lockInvite) Helper_Common::delLock($keyInvite);
		
		return $ret;
	}
	
	/**
	 * @deprecated
	 * 获得领取第三方平台好友邀请奖励的资格
	 */
	public static function inviteThirdBonus($thirdId)
	{
		$ret = false;
		$cache = Common::getCache();
		
		//被邀请人加锁
		$keyBeInvited = self::CACHE_KEY_FRIEND_THIRD_BE_INVITED.$thirdId;
		if(!!$lockBeInvited = Helper_Common::addLock($keyBeInvited))
		{
			//被哪些人邀请了
			if(!!$beInvitedData = $cache->get($keyBeInvited))
			{
				//只给第一个邀请的人奖励
				foreach($beInvitedData as $inviteId => &$beData)
				{
					if(User_Model::exist($inviteId))
					{
						$keyActInvite = self::LOCK_KEY_ACTIVITY_INVITE.$inviteId;
						if(!!$lockActInvite = Helper_Common::addLock($keyActInvite))
						{
							//获得邀请信息
							$active = Data_Activity_Model::getInstance($inviteId)->getActivity(Activity_Model::ACTIVE_INVITE_THIRD);
							$active['data']['num']++;
							//总邀请数奖励
							foreach(self::$inviteThirdNumBonus as $bonusNum => &$bonusConf)
							{
								if($active['data']['num'] < $bonusNum) break;
								if(isset($active['data']['numBonus'][$bonusNum])) continue;
								
								$active['data']['numBonus'][$bonusNum] = 0;
							}
							Data_Activity_Model::getInstance($inviteId, true)->updateRecord(Activity_Model::ACTIVE_INVITE_THIRD, $active['data']);
							$ret = true;
						}
						if($lockActInvite) Helper_Common::delLock($keyActInvite);
						break;
					}
				}
				//被邀请人信息清除
				$cache->delete($keyBeInvited);
			}
			
			Helper_Common::delLock($keyBeInvited);
		}
		
		return $ret;
	}
	
	
	/**
	 * 领取第三方平台好友邀请奖励
	 */
	public static function getInviteThirdBonus($userId, $inviteNum)
	{
		$ret = array( 'errorCode' => Friend_Exception::STATUS_GET_INVITE_THIRD_BONUS_FAIL );
	
		$keyActInvite = self::LOCK_KEY_ACTIVITY_INVITE.$userId;
		if(!!$lockActInvite = Helper_Common::addLock($keyActInvite))
		{
			do{
				//获得邀请信息
				$active = Data_Activity_Model::getInstance($userId)->getActivity(Activity_Model::ACTIVE_INVITE_THIRD);
				//是否领过
				if(!self::$inviteThirdNumBonus[$inviteNum] 
					|| !isset($active['data']['numBonus'][$inviteNum]) 
					|| $active['data']['numBonus'][$inviteNum] != 0) break;
				
				//领取总邀请数奖励
				Activity_Model::getInstance($userId)->sendPackage(self::$inviteThirdNumBonus[$inviteNum], 'friendInvite', '好友邀请奖励');
				
				$active['data']['numBonus'][$inviteNum] = $_SERVER['REQUEST_TIME'];
				Data_Activity_Model::getInstance($userId, true)->updateRecord(Activity_Model::ACTIVE_INVITE_THIRD, $active['data']);
				
				$ret = array('inviteNum' => $inviteNum);
				
			}while(0);
			
		}
		if($lockActInvite) Helper_Common::delLock($keyActInvite);
	
		return $ret;
	}
	
	/**
	 * 赠送礼物
	 * @param unknown $userId
	 * @param unknown $toId
	 * @return Ambigous <multitype:unknown , multitype:number >
	 */
	public static function sendGift($userId, $toId, $giftPack)
	{
		$ret = array( 'errorCode' => Friend_Exception::STATUS_SEND_GIFT_FAIL );
		
		do{
			$cache = Common::getCache();

			//发送者
			$keySendFrom = self::CACHE_KEY_FRIEND_SEND_GIFT.$userId;
			if(!!$lockSendFrom = Helper_Common::addLock($keySendFrom))
			{
				
				if(!!$dataSendFrom = $cache->get($keySendFrom))
				{
					//已经送过了
					if($dataSendFrom[$toId]) break;
					//每日送礼限制
					if(count($dataSendFrom) >= self::SEND_GIFT_LIMIT_DAY) break;
				}
				
				$dataSendFrom[$toId] = $_SERVER['REQUEST_TIME'];
				$cache->set($keySendFrom, $dataSendFrom, Helper_Date::getTodayLeftSeconds());
		
				//接受者
				$keySendTo = self::CACHE_KEY_FRIEND_SEND_GIFT_RECEIVE.$toId;
				if(!!$lockSendTo = Helper_Common::addLock($keySendTo))
				{
					$dataSendTo = $cache->get($keySendTo);
					$dataSendTo[$userId] = array(
						'time' => 0,
						'gift' => $giftPack,
					);
					
					$cache->set($keySendTo, $dataSendTo, Helper_Date::getTodayLeftSeconds());
					
					Helper_Common::delLock($keySendTo);
				}
				
				$ret = array('toId' => $toId);
		
			}
				
		}while(0);
		
		if($lockSendFrom) Helper_Common::delLock($keySendFrom);
		
		return $ret;
	}
	
	/**
	 * 获得礼物
	 * @param unknown $userId
	 * @param unknown $fromId
	 * @return Ambigous <multitype:unknown , multitype:number >
	 */
	public static function getGift($userId, $fromId)
	{
		$ret = array( 'errorCode' => Friend_Exception::STATUS_GET_GIFT_FAIL );
	
		do{
			$cache = Common::getCache();
	
			//接受者
			$keySendTo = self::CACHE_KEY_FRIEND_SEND_GIFT_RECEIVE.$userId;
			if(!!$lockSendTo = Helper_Common::addLock($keySendTo))
			{
				if(!!$dataSendTo = $cache->get($keySendTo))
				{
					//每日收礼限制
					if(count($dataSendTo) >= self::GET_GIFT_LIMIT_DAY) break;
					//是否已经领过
					if(!isset($dataSendTo[$fromId])
						|| $dataSendTo[$fromId]['time'] != 0) break;

					Activity_Model::getInstance($userId)->sendPackage($dataSendTo[$fromId]['gift'], 'friendGift', '好友送礼');
					$dataSendTo[$fromId]['time'] = $_SERVER['REQUEST_TIME'];
					
					$cache->set($keySendTo, $dataSendTo, Helper_Date::getTodayLeftSeconds());

					$ret = array('fromId' => $fromId);
				}
			}

		}while(0);
	
		if($lockSendTo) Helper_Common::delLock($keySendTo);
		
		return $ret;
	}
	
	/**
	 * 发送礼物历史记录
	 * @return Ambigous <multitype:, mixed>
	 */
	public static function sendGiftHistory($userId)
	{
		$cache = Common::getCache();
		//发送者
		$keySendFrom = self::CACHE_KEY_FRIEND_SEND_GIFT.$userId;
		
		$dataSendFrom = $cache->get($keySendFrom);
		return !empty($dataSendFrom) ? $dataSendFrom : array();
	}
	
	/**
	 * 接受礼物历史记录
	 * @return Ambigous <multitype:, mixed>
	 */
	public static function getGiftHistory($userId)
	{
		$cache = Common::getCache();
		//发送者
		$keySendTo = self::CACHE_KEY_FRIEND_SEND_GIFT_RECEIVE.$userId;
	
		$dataSendTo = $cache->get($keySendTo);
		return !empty($dataSendTo) ? $dataSendTo : array();
	}
	
	public static function getAppFriend($userId, $fIds)
	{
		$ret = array();
		if(empty($fIds)) return $ret;
		
		foreach($fIds as $tid)
		{
			if(empty($tid)) continue;
			$tid = addslashes($tid);
			$uid = User_Model::getIdByThird($tid);
			if($uid)
			{
				$profile = User_Profile::getInstance($uid)->getData();
				$info = User_Info::getInstance($uid)->getData();
				$ret[$tid] = array(
					'uid' => intval($uid),
					'nick' => strval($profile['nickName']),
					'level' => intval($info['level']),
					'friend' => Friend_Model::getInstance($userId)->areFriend($uid),
				);
			}
			else
			{
				$ret[$tid]['uid'] = 0;
			}
		}
		
		return $ret;
	}
	
}