<?php

class Friend_Exception extends GameException
{
	/**
	 * 已经是好友了
	 * @var	int
	 */
	const STATUS_ALREADY_FRIEND = 400;
	/**
	 * 没有好友请求
	 * @var	int
	 */
	const STATUS_NO_REQUEST = 401;
	/**
	 * 不是好友
	 * @var	int
	 */
	const STATUS_NOT_FRIEND = 402;
	/**
	 * 第三方好友邀请失败
	 * @var	int
	 */
	const STATUS_INVITE_FAIL = 403;
	
	/**
	 * 好友送礼失败
	 * @var unknown
	 */
	const STATUS_SEND_GIFT_FAIL = 404;
	
	/**
	 * 获得好友送礼失败
	 * @var unknown
	 */
	const STATUS_GET_GIFT_FAIL = 405;
	
	/**
	 * 获得平台邀请奖励失败
	 * @var unknown
	 */
	const STATUS_GET_INVITE_THIRD_BONUS_FAIL = 406;
}

?>