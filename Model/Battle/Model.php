<?php
/**
 * 战场模块
 * @name Model.php
 * @author Liuchangbing
 * @since 2013-1-18
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Battle_Model extends Logical_Abstract
{	
	
	/**
	 * 义军备选列表cache key
	 * @var unknown_type
	 */
	const HELPERS_CACHE_KEY = "helper_user_list";
	
	const SK_AUTO_TYPE_BONUS_GOLD = 21;	//额外获得金钱
	
	const SK_AUTO_TYPE_GROP_RATE = 22;	//武将掉率
	
	const SK_AUTO_TYPE_BONUS_EXP = 23;	//额外获得经验
	
	public static $confAutoSkill = array(
		47 => array('type' => self::SK_AUTO_TYPE_BONUS_GOLD, 'effect' => 1.25),
		48 => array('type' => self::SK_AUTO_TYPE_BONUS_GOLD, 'effect' => 1.5),
		66 => array('type' => self::SK_AUTO_TYPE_GROP_RATE, 'effect' => 1.25),
		67 => array('type' => self::SK_AUTO_TYPE_GROP_RATE, 'effect' => 1.5),
		59 => array('type' => self::SK_AUTO_TYPE_BONUS_EXP, 'effect' => 1.25),
		60 => array('type' => self::SK_AUTO_TYPE_BONUS_EXP, 'effect' => 1.5),
	);
	
	/**
	 * 单例对象
	 * @var	User_Model[]
	 */
	protected static $singletonObjects;

	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Battle_Model
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 将最新登录的玩家加入到义军列表
	 * 每个等级100个
	 * array(
	 * 	1 => array( uid1 , uid2 ..... ),
	 *  ........
	 * )
	 * 
	 * @param unknown_type $userId
	 */
	public static function addHelperUser( $userId )
	{
		$helperUser = User_Info::getInstance( $userId )->getData();
		$cache = Common::getCache();
		
		$levelUserNumberLimit = 10;
		$helperList = $cache->get( self::HELPERS_CACHE_KEY );
		$helperLevelList = $helperList[$helperUser['level']] ? $helperList[$helperUser['level']] :array();
		
		//如果不在这个队列，则进队
		if( !in_array( $userId , $helperLevelList ) )
		{
			array_push( $helperLevelList , $userId );
			if( count( $helperLevelList ) > $levelUserNumberLimit )
			{
				array_shift( $helperLevelList );
			}
			
			$helperList[$helperUser['level']] = $helperLevelList;
			$cache->set( self::HELPERS_CACHE_KEY , $helperList );
		}
	}
	/**
	 * 获取3个义军
	 * 规则：在一定等级范围内随机查找三个义军
	 */
	public function getOtherHelpers()
	{
	
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		$cache = Common::getCache();
		$helperList = $cache->get( self::HELPERS_CACHE_KEY );
		if( !$helperList )
		{
			return array();
		}

		
		//在我上下多少级之内随机搜索
		$upperLimit = 5;
		$lowerLimit = 5;
		
		//最大查找次数
		$loopTimes = 0;
		$helperResult = array();
		while( $loopTimes < 10000 )
		{
			//如果找了100次还找不到，就全等级找0-2000级，足够了
			if( $loopTimes > 100 )
			{
				$commonConfig = Common::getConfig("common");
				$maxLevel = $commonConfig['maxLevel'];
				if( empty( $maxLevel ) ) $maxLevel = 120;
				 $lowerLimit = $maxLevel;
				 $upperLimit = $maxLevel;
			}
			$loopTimes++;
			$lowerLevel = ( $userInfo['level'] - $lowerLimit > 0 ) ? $userInfo['level'] - $lowerLimit : 1;
			$upperLevel = $userInfo['level'] + $upperLimit;
			$randLevel = mt_rand( $lowerLevel ,  $upperLevel );
			
			if( !$helperList[$randLevel] )
			{
				continue;
			} 
			
			$helperKey = array_rand( $helperList[$randLevel] );
			$hUserId = $helperList[$randLevel][$helperKey];
			$hUserInfo = User_Info::getInstance( $hUserId )->getData();
			//如果等级对不上了，就删了
			if( $hUserInfo['level'] > $upperLevel || $hUserInfo['level'] < $lowerLevel )
			{
				unset( $helperList[$randLevel][$helperKey] );
				$cache->set( self::HELPERS_CACHE_KEY , $helperList );
				continue;
			}
			
			//如果ID等于自己也不算
			if( $hUserId == $this->userId )
			{
				continue;
			}
			
			//如果是好友也不算
			if( Friend_Model::getInstance( $this->userId )->areFriend( $hUserId ) )
			{
				continue;
			}
			
			//如果leaderCard不对也不算
			$cardInfo = Card_Model::getInstance(  $hUserId  )->getLeaderCard();
			if( !$cardInfo )
			{
				continue;
			}
		
			//如果已经在名单里了，也不算
			if( in_array( $hUserId  , $helperResult ))
			{
				continue;
			}
			array_push( $helperResult , $hUserId );
			
			if( count( $helperResult ) == 3 )
			{
				break;
			}
		}
		return $helperResult;
	}
	
	/**
	 * 获取友军列表
	 */
	public function getFriendHelpers()
	{
		$friends = Friend_Model::getInstance( $this->userId )->getFriends();
		$helperResult = array();
		foreach ( $friends as $fId => $info )
		{
			if( $info['hasHelp'] == 0 )
			{
				$helperResult[] = $fId;
			}
		}
		return $helperResult;
	}
	/**
	 * 获取战场配置
	 * @param unknown_type $floorId
	 * @param unknown_type $roomId
	 */
	public static function getBattleConfig(  $floorId , $roomId , $type )
	{
		//读取战场配
		$battleConfig = Common::getConfig( $type );
		return $battleConfig[$floorId]['rooms'][$roomId];
		
	}
	
	/**
	 * 获取怪物列表
	 */
	public function getMonster(  $floorId , $roomId , $helperId , $type )
	{

		$roomConfig = self::getBattleConfig( $floorId, $roomId , $type );
		if( !$roomConfig )
		{
			throw new Battle_Exception( Battle_Exception::STATUS_NOT_ROOM_CONFIG );
		}
		
		//判断体力值是否够
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		$needStamina = intval( $roomConfig['stamina'] );
		$stamina = Data_User_Info::getInstance( $this->userId )->getStamina();
		if( $stamina < $needStamina )
		{
			throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_STAMINA );
		}
		
		
		$cardList = Card_Model::getInstance( $this->userId )->getData();
		if( count( $cardList ) >= $userInfo['maxCardNum'] )
		{
			throw new Card_Exception( Card_Exception::STATUS_CARD_NUM_MAX );
		}
		
		//扣体力
		$needStamina = intval( $roomConfig['stamina'] );
		Data_User_Info::getInstance( $this->userId , true )->changeStamina( -$needStamina );
		
		//掉落包(废弃)
		//$dropPackageId = $roomConfig['package'];
		$dropGold = $roomConfig['dropGold'];
		$dropGoldRate = $roomConfig['dropGoldRate'];
		//多波小怪，每波随机2-4个
		$monsterConfig = Common::getConfig( "monster" );
		$monsters = array();
		//小怪波数 
		//$roomConfig['stepsMonster'] = array( 3 , 4 );
		
		//第几波怪=>怪的数量
		foreach (  $roomConfig['stepsMonster'] as $i => $monsterNum )
		{
			//则随机出怪
			//$monsterNum = array( 3 , 4 );
			if( !is_array( $monsterNum ) )
			{
				//每波怪怪物数量
				//$monsterNum = mt_rand( 2, 4 );
				$monsterStr = $roomConfig['monsters'];
				//$monsterStr = "5,3,7,8,11,22";
				$monsterArr = explode( ",", $monsterStr );
				
				$stepMonsterKeys = array_rand( $monsterArr , $monsterNum );
				
				//判断是否会出现乱入怪
				$mstRand = mt_rand( 1, 100 );
				//10%的概率会出乱入怪
				if( $roomConfig['randMonster'] &&  $mstRand <= 10 )
				{
					//出如果出去顶替掉一个小怪位置
					$replaceRandKey = rand( 1, count( $stepMonsterKeys ) ) - 1;
					$monsterArr[$replaceRandKey] =  $randMonsterId =  $roomConfig['randMonster'];
					$randMonsterDrop = $roomConfig['randDrop'];
				}
				
			}
			//则出固定怪array( 333, 44444 )
			else
			{
				$monsterArr = $monsterNum;
				$stepMonsterKeys = array_keys( $monsterArr );
			}
	
			//第几个怪会掉(废弃)
			//$dropKey = mt_rand( 1 , count( $stepMonsterKeys ) ) - 1;
			
			$stepMonsters = $this->getStepMonsters($stepMonsterKeys, $monsterArr, $monsterConfig, $dropGold, $dropGoldRate, $helperId);
			$monsters[] = array( 'monsters' => $stepMonsters , 'type' => 'common');
		}
		
		//最后一波BOSS，随机一个
		$bossArr = $roomConfig['boss'];
		$bossKeys = array_keys( $bossArr );
	
		//第几个怪会掉(废弃)
		//$dropKey = array_rand( $bossArr );
		
		$stepMonsters = $this->getStepMonsters( $bossKeys, $bossArr , $monsterConfig, $dropGold, $dropGoldRate, $helperId);
		$monsters[] = array( 'monsters' => $stepMonsters , 'type' => 'boss');
		
		$roomData = $this->getDataBattleModel( $type )->getHistory();
		$roomData['monster'] = $monsters;
		$roomData['floor'] = $floorId;
		$roomData['room'] = $roomId;

		
		if( $type == "special" ||  $type == "weekly")
		{
			$battleConfig = Common::getConfig( $type );
	
			if( $type == "special" )
			{
				$startTime = $battleConfig[$floorId ]['start_time'];
				$endTime = $battleConfig[$floorId ]['end_time'];
				
				if( $_SERVER['REQUEST_TIME'] < strtotime( $startTime ) || $_SERVER['REQUEST_TIME'] >= strtotime( $endTime ) )
				{
					throw new Battle_Exception( Battle_Exception::STATUS_NOT_IN_ACTIVE_TIME );
				}
			}

			if( $type == "weekly" )
			{
				$inDayTime = false;
				if( intval($startTime) == 0 && intval($endTime) == 0)
				{
				 	$inDayTime = true;
				}
				else
				{
					$ymd = date( 'Y-m-d' , $_SERVER['REQUEST_TIME'] );
					if( strtotime( $ymd." ".$startTime) <= $_SERVER['REQUEST_TIME'] && strtotime( $ymd." ".$endTime) > $_SERVER['REQUEST_TIME'] )
					{
						$inDayTime = true;
					}
				}
				
				$weekDay =  $battleConfig[$floorId ]['week_day'] ;
				if( date("N" , $_SERVER['REQUEST_TIME']) != $weekDay || !$inDayTime)
				{
					throw new Battle_Exception( Battle_Exception::STATUS_NOT_IN_ACTIVE_TIME );
				}
			}
			
			
			$battlePassed = $this->getDataBattleModel( $type , true )->getData();
			$exist = 0;
			if( is_array( $battlePassed ) )
			{
				foreach( $battlePassed as $id => $passed )
				{
				   if( $floorId == $passed['floor'] && $roomId == $passed['room'] )
				   {
						$exist = 1;
						break;
				   }
				}
			}
			
			if( $exist == 0  )
			{
			    $addBattleInfo = array(
					'floor' => $floorId,
					'room' => $roomId,
					'pass' => 0 ,
			    );
			    $this->getDataBattleModel( $type , true )->setData( $addBattleInfo );		
			}	
		}
		else
		{
			$battleMax = $this->getDataBattleModel( $type , true )->getData();
			if( $floorId > $battleMax['floor'] )
            {
               $battleMax['pass'] = 0;
               $battleMax['floor']  = $floorId;
               $battleMax['room'] = 1;
	    	}
            elseif(  $floorId == $battleMax['floor'] )
            {
               if( $roomId > $battleMax['room'] )
               {
                 $battleMax['pass'] = 0;
               }	 
               $battleMax['room'] = ( $roomId > $battleMax['room'] )? $roomId :  $battleMax['room'];  	 
            }
			$this->getDataBattleModel( $type , true )->setData(  $battleMax );
		}


        $roomData['type'] = $type;
        $roomData['helperId'] = $helperId;
			
	    $this->getDataBattleModel( $type , true )->setHistory( $roomData );
		$cache = Common::getCache();
		$cache->set( $this->userId."_room_monster_type" , $type  );
		return $monsters;
	}
	
	/**
	 * 获取战场对象
	 * @param unknown $type
	 * @param string $lock
	 * @return Ambigous <Data_User_Profile, multitype:User_Model >
	 */
	public function getDataBattleModel( $type , $lock = false )
	{
		if( $type == "normal" )
		{
			return Data_Battle_Normal::getInstance( $this->userId , $lock );
		}
		elseif(  $type == "special" )
		{
			return Data_Battle_Special::getInstance( $this->userId , $lock );
		}
		elseif(  $type == "weekly" )
		{
			return Data_Battle_Weekly::getInstance( $this->userId , $lock );
		}
	}
		
	
	/**
	 * 设置上次推送的援军信息
	 */
	public function setHelperHistory(  $otherHelpers , $friendHelpers  )
	{
		$cache = Common::getCache();
		$data = array(
			'friend' => $friendHelpers,
			'other' => 	$otherHelpers,	
		);
		$cache->set( $this->userId."_helper_histrory" , $data );
	}
	
	/**
	 * 获取援军信息
	 */
	public function getHelperHistory()
	{
		$cache = Common::getCache();
		return $cache->get( $this->userId."_helper_histrory" );
	}

	/**
	 * 领取奖励
	 */
	public function reward( $damage )
	{	
		
		$damageStatus = $this->_checkDamage( $damage );
		if( $damageStatus == false )
		{
			throw new Battle_Exception( Battle_Exception::STATUS_OVER_ATTACK_LIMIT );
		}
			

		$cache = Common::getCache();
		$type = $cache->get( $this->userId."_room_monster_type" );
		$roomBattleData = $this->getDataBattleModel( $type )->getHistory();	
		$helperId = $roomBattleData['helperId'];
		$type = $roomBattleData['type'];
		if( !$roomBattleData )
		{
			throw new Battle_Exception( Battle_Exception::STATUS_NOT_BATTLE_INFO );
		}
		//扣体力
		$addExp = 0;
		$addGold = 0;
		$boxGold = 0;
		$monsterConfig = Common::getConfig( "monster" );
	
		
		$getCards = array();
		foreach ( $roomBattleData['monster'] as $roomStep )
		{
			foreach ( $roomStep['monsters'] as $monster )
			{
				$monConfig = $monsterConfig[$monster['mid']];	
				$addExp += $monConfig['drop_exp'];
				$addGold += $monConfig['drop_gold'];
				//发卡
			
				if( $monster['drop_cid'] > 0 )
				{
					$cardId = Card_Model::getInstance( $this->userId , true )->addCard( $monster['drop_cid'] , $monster['drop_lv'] , true , "battleReward" );					
					if( $cardId )
					{
						$cardInfo = Card_Model::getInstance( $this->userId )->getCardInfo( $cardId );
						$getCards[] = array(
							'cid' => $cardInfo['cardId'],
							'exp' => $cardInfo['exp'],
							'is_first' => 0,
							'lv' => $cardInfo['level'],
							'ucid' => strval( $cardId ),		
						);
					}
				}
				elseif(  $monster['drop_gold'] > 0  ) 
				{
					$boxGold += $monster['drop_gold'];
					
				}
			}
		}

		//计算援军点数
		$helperIsFriend = false;
		$helpers = $this->getHelperHistory();
		if( in_array( $helperId , $helpers['other'] ) )
		{
			$gacha = 5;
			Data_User_Help::getInstance( $helperId , true )->setOtherHelp( $gacha );
		}
		elseif(  in_array( $helperId , $helpers['friend'] ) )
		{
			$gacha = 10;
			Data_User_Help::getInstance( $helperId , true )->setFriendHelp( $gacha );
			//设置此好友已经帮助过了
			Data_Friend_Model::getInstance( $this->userId , true )->setHelped( $helperId , 1 );
			$helperIsFriend = true;
		}
		else
		{
			throw new Battle_Exception( Battle_Exception::STATUS_NOT_HELPER_HISTORY );
		}
		
		
		Data_User_Info::getInstance( $this->userId , true )->changeGachaPoint( $gacha );
		
		//发经验
		$bonusExp = 0;
		//主将被动技能加成
		$effect = $this->getAutoSkillEffectByType(Battle_Model::SK_AUTO_TYPE_BONUS_EXP);
		if($effect > 0) $bonusExp += ($effect - 1) * $addExp;
		
		//友军主将被动技能加成
		if($helperIsFriend)
		{
			$effect = Battle_Model::getInstance($helperId)->getAutoSkillEffectByType(Battle_Model::SK_AUTO_TYPE_BONUS_EXP);
			if($effect > 0) $bonusExp += ($effect - 1) * $addExp;
		}
		
		//限时活动加成
		if(!!$actConf = Activity_Model::isInTime(Activity_Model::ACTIVE_IN_TIME_BONUS_EXP))
		{
			foreach($actConf as $conf)
			{
				$bonusExp += ($conf['exp'] - 1) * $addExp;
				break;
			}
		}
		
		//经验书buff
		$effect = Data_User_Info::getInstance( $this->userId )->getBuffEffVal( 'battleExp' , 1 );
		if($effect > 0) $bonusExp += ($effect - 1) * $addExp;
		
		$addExpRet = User_Info::getInstance($this->userId)->addExp(intval($addExp + $bonusExp), true, true);
		$levelUpBonus = $addExpRet['levelUpBonus'];
		$level = $addExpRet['level'];
		
		//发金币
		$oldGold = $addGold + $boxGold;
		$bonusGold = 0;
		//集钱袋buff
		$effect = Data_User_Info::getInstance( $this->userId )->getBuffEffVal( 'battleGold' , $oldGold ) - $oldGold;
		if($effect > 0) $bonusGold += $effect;
		
		//主将被动技能加成
		$effect = $this->getAutoSkillEffectByType(Battle_Model::SK_AUTO_TYPE_BONUS_GOLD);
		if($effect > 0) $bonusGold += $oldGold * ($effect - 1);
		
		//友军主将被动技能加成
		if($helperIsFriend)
		{
			$effect = Battle_Model::getInstance($helperId)->getAutoSkillEffectByType(Battle_Model::SK_AUTO_TYPE_BONUS_GOLD);
			if($effect > 0) $bonusGold += $oldGold * ($effect - 1);
		}
		
		User_Info::getInstance( $this->userId )->changeGold( intval($bonusGold + $oldGold) , '副本奖励' );
		
		//扣除耐力
		$floorId = $roomBattleData['floor'];
		$roomId = $roomBattleData['room'];
		$roomConfig = self::getBattleConfig( $floorId , $roomId , $type );
		
	
		$battleConfig = Common::getConfig( $type );
		$lastRoom = count(  $battleConfig[$floorId]['rooms'] );
		
		$firstPass = 0;
		if( $type == "normal" )
		{
			$battleMax = $this->getDataBattleModel( $type )->getData();
			//$battleMax['floor']  $battleMax['room']  是当前可以打最大关卡ID
			if( $floorId == $battleMax['floor'] && $roomId == $battleMax['room'] && $battleMax['pass'] == 0 && $roomId == $lastRoom )
			{
				  $firstPass = 1;
			}


			if( $floorId == $battleMax['floor'] && $roomId == $battleMax['room'] && $battleMax['pass'] == 0 )
            {
				$battleMax['pass'] = 1;
                $this->getDataBattleModel( $type , true )->setData( $battleMax  );
            }
		}
		else
		{
			$firstPass = 0;
		    $passedInfo =  $this->getDataBattleModel( $type )->getData();
		  
		    $prePass = array();
		    foreach( $passedInfo as $id => $passed )
		    {
 			   if( $passed['floor'] != $floorId  || $passed['pass'] != 1 )
			   {
				continue;	
			   }
			   
  			   $prePass[$floorId][$passed['room']] = $passed['pass'];		
		       }
		       
		       $preValue = count( $prePass[$floorId] );
		       $this->getDataBattleModel( $type , true )->setPassed( $floorId, $roomId );
		       if( $preValue < $lastRoom )
		       {
		       	
			 	$passedInfo =  $this->getDataBattleModel( $type )->getData();
				$pass = array();
		
				foreach( $passedInfo as $id => $passed )
				{
					if( $passed['floor'] != $floorId || $passed['pass'] != 1 )
					{
						continue;
					}
					
					$pass[$floorId][$passed['room']] = $passed['pass'];
				}
			
				$currValue = count( $pass[$floorId] );
				
		      	if(  $currValue == $lastRoom )
				{
					 $firstPass = 1;
				}
		     }
		}
		
		//如果第一次通关，加元宝
		if(   $firstPass == 1 )
		{
			$commonConfig = Common::getConfig( "common" );
			//firstPass type->floorId->roomId
			User_Info::getInstance( $this->userId )->changeCoin( $commonConfig['dungeon_clear_coin'] , "首次通关" , 1 , $type."->".$floorId."->".$roomId );
		}
		

		if( $type == "normal" )
		{	
			$battleInfo = array(
				'dungon_type' => $type,
				'floor_id' => $floorId,
				'room_id' => $roomId,
				'normal_current' => $this->getNormalCurrent(),
			);
		}
		else
		{
			$floorStatus = 0;
			if( $preValue == $lastRoom || $currValue == $lastRoom )
			{
				//第一次通关
				$floorStatus = 2;		
			}
			
			$battleInfo = array(
                    'dungeon_type' => $type,
                    'floor_id' =>  $floorId,
                    'room_id' => $roomId,
			        'room_status' => 2,
					'floor_status' => $floorStatus,	
            );
		}
		
		//删除缓存
		$this->getDataBattleModel( $type , true )->setHistory( null );
		
		//BattleController里面记得也要改掉
		$returnData = array(
			'getCards' => $getCards,
			'battleInfo' => $battleInfo, 
			'addGold' => $oldGold,
			'bonusGold' => $bonusGold,
			'addExp' => $addExp,
			'bonusExp' => $bonusExp,
			'levelUpBonus' => $levelUpBonus,
			'level' => $level,
		);
		return $returnData;
	}

	 /**
         * 获取特殊战场进度
         */
        public function getSpecial()
        {
                $passedInfo =  $this->getDataBattleModel( "special" )->getData();

                $specialCnf = Common::getConfig( "special" );
                foreach( $specialCnf as $floor => $floorCnf )
                {
                        $rooms = array();
                        $floorStatus = 0;
                        foreach( $floorCnf['rooms'] as $roomId => $roomCnf )
                        {
                                //$rooms[strval($roomId)] = 0;
                                if( $passedInfo  )
                                {
                                        foreach(  $passedInfo as $id => $passed )
                                        {
                                                if( $floor == $passed['floor'] && $roomId == $passed['room'] && $passed['pass'] == 1 )
                                                {
                                                         $rooms[strval($roomId)] = 2;
                                                }
                                        }
                                }
                        }

                        if( empty( $rooms ) )
                        {
                                continue;
                        }
                        $totalNum = count( $floorCnf['rooms'] ) * 2;
                        $roomSum = array_sum( $rooms );

                        if( $roomSum == $totalNum )
                        {
                                $floorStatus = 2;
                        }
                        $returnData[$floor] = array(
                                'rooms' => $rooms,
                                'status' => $floorStatus,
                        );
                }
                return $returnData;
        }


	/**
	 * 获取周长战场进度
	 */
	public function getWeekly()
	{
		$passedInfo =  $this->getDataBattleModel( "weekly" )->getData();
		$specialCnf = Common::getConfig( "weekly" );
		foreach( $specialCnf as $floor => $floorCnf )
		{
			//0表示都没通
			$floorStatus = 0;
			unset( $rooms );
			if( $floorCnf['rooms']  )
			{
				foreach( $floorCnf['rooms'] as $roomId => $roomCnf )
				{
					$rooms[strval($roomId)] = 0;
					if( $passedInfo )
					{
						foreach(  $passedInfo as $id => $passed )
						{
							if( $floor == $passed['floor'] && $roomId == $passed['room'] && $passed['pass'] == 1 )
							{
								$rooms[strval($roomId)] = 2;
							}
						}
					}
				}
			}
						
			$totalNum = count( $floorCnf['rooms']) * 2;
			$roomSum = array_sum( $rooms );
			if( $roomSum == $totalNum )
			{
				$floorStatus = 2;
			}
						
			$returnData[$floor] = array(
				'rooms' => $rooms,
				'status' => $floorStatus,
			);
		}
		return $returnData;
	}
	
	/**
	 * 获取当前战场
	 */
	public function getNormalCurrent()
	{
		$cache = Common::getCache();
		$battleMax = $this->getDataBattleModel( "normal" , true )->getData();
		$floorId = $battleMax['floor'];
		$roomId = $battleMax['room'];
		$type = "normal";

		if( $battleMax['pass'] == 1 )
		{	
			//0表示new 	
			$status = 0;
			$nextRoomConfig = self::getBattleConfig( $floorId , $roomId +1  , $type ); 
			$nextFloorConfig = self::getBattleConfig( $floorId+1 , $roomId  , $type );
			
			if( $nextRoomConfig )
			{
				$nextFloorId = $floorId;
				$nextRoomId = $roomId + 1;
			}
			else
			{
				if( $nextFloorConfig )
				{
					$nextFloorId = $floorId+1;
					$nextRoomId = 1;
				}
			}
		   $nextFloorId = $nextFloorId ? strval( $nextFloorId ) : strval( $floorId ) ;
           $nextRoomId = $nextRoomId ? strval( $nextRoomId ) : strval( $roomId );
		
		   if( $nextFloorId == $floorId && $nextRoomId == $roomId )
		   {
   				$status = 2;
		   }

		}
		else
		{
			$status = 0;
			$nextFloorId = $floorId;
			$nextRoomId = $roomId;
		}
		
		$nextFloorId = strval( $nextFloorId ) ;
		$nextRoomId = strval( $nextRoomId );
		
		return array(
			'floor_id' => $nextFloorId ? $nextFloorId : "1",
			'room_id' => $nextRoomId ? $nextRoomId : "1",
			'status' => $status ,	//打过
		);
		
	}
	
	/**
	 * @deprecated
	 */
	public function fillDropMonster( &$monsterInfo , $randMonsterId , $randMonsterDrop , $dropPackageId , $dropGold , $dropGoldRate )
	{
		$cardInfo = array();
		//乱入怪物
		if( !empty( $randMonsterId ) )
		{
			$cardInfo = $this->drawPackage( $randMonsterDrop , $dropGold , $dropGoldRate  );
		}
			
			
		if( empty( $cardInfo ) )
		{
			$cardInfo = $this->drawPackage( $dropPackageId , $dropGold , $dropGoldRate  );
		}
			
		
		if( $cardInfo['cardId'] )
		{
			$monsterInfo['drop_cid'] = $cardInfo['cardId'];
			$monsterInfo['drop_lv'] = $cardInfo['cardLevel'];
		}
		
			
		if( $cardInfo['dropGold'] > 0 )
		{
			$monsterInfo['drop_gold'] = $cardInfo['dropGold'];
		}
	}
	
	public function getData()
	{
		return Data_Card_Model::getInstance( $this->userId )->getData();
	}
	
	public function setInfo()
	{
		$type = "normal";
		$battleMax = $this->getDataBattleModel( $type , true )->getData();
		$floorId = 23;
		$roomId = 5;
		$this->getDataBattleModel( $type , true )->setData( $battleMax  );
	}
	
	/**
	 * 主将被动技能加成
	 * @param unknown_type $type
	 */
	public function getAutoSkillEffectByType($type)
	{
		$ret = 0;
		
		$teamInfo = Data_Card_Team::getInstance($this->userId)->getAll();
		$curTeam = $teamInfo['teams'][$teamInfo['cur']];
		$cardInfo = Data_Card_Model::getInstance($this->userId)->getCardInfo($curTeam[0]);
		
		$cardConf = Common::getConfig("card");
		
		do
		{
			if(!$skillConf = self::$confAutoSkill[$cardConf[$cardInfo['cardId']]['leader_skid']]) break;
			if($skillConf['type'] != $type) break;
			
			$ret = $skillConf['effect'];
		}
		while(0);
		
		return $ret;
	}
	
	/**
	 * 是否有非战斗系的武将被动技能
	 */
	public function getAutoSkill()
	{
		$autoSkill = array();
		
		$teamInfo = Data_Card_Team::getInstance($this->userId)->getAll();
		$curTeam = $teamInfo['teams'][$teamInfo['cur']];
		$cardInfo = Data_Card_Model::getInstance($this->userId)->getCardInfo($curTeam[0]);
		
		$cardConf = Common::getConfig("card");
		
		do
		{
			$leader_skid = $cardConf[$cardInfo['cardId']]['leader_skid'];
			if(!$skillConf = self::$confAutoSkill[$leader_skid]) break;
				
			$ret = $skillConf['effect'];
			$autoSkill = $skillConf;
			$autoSkill['id'] = $leader_skid;
		}
		while(0);
		
		return $autoSkill;
	}
	
	//private 
	
/**
	 * 获得每波怪物信息，掉落信息
	 * @param unknown $stepMonsterKeys
	 * @param unknown $monsterArr
	 * @param unknown $monsterConfig
	 * @param unknown $dropGold
	 * @param unknown $dropGoldRate
	 * @throws Battle_Exception
	 * @return multitype:multitype:number unknown Ambigous <unknown> Ambigous <string, unknown>
	 */
	private function getStepMonsters( &$stepMonsterKeys , &$monsterArr , &$monsterConfig , $dropGold , $dropGoldRate, $helperId)
	{
		if( empty( $stepMonsterKeys ) || count( $stepMonsterKeys ) < 1 ) return array();
		
		//武将掉率加成
		$bonusDropRate = 0;
		//主将被动技能加成
		$effect = $this->getAutoSkillEffectByType(Battle_Model::SK_AUTO_TYPE_GROP_RATE);
		if($effect > 1) $bonusDropRate += ($effect - 1);
		
		$helpers = $this->getHelperHistory();
		//友军主将被动技能加成
		if(in_array($helperId , $helpers['friend'] ))
		{
			$effect = Battle_Model::getInstance($helperId)->getAutoSkillEffectByType(Battle_Model::SK_AUTO_TYPE_GROP_RATE);
			if($effect > 1) $bonusDropRate += ($effect - 1);
		}
		//招募令buff
		$effect = Data_User_Info::getInstance( $this->userId )->getBuffEffVal( 'cardDrop' , 1 );
		if($effect > 1) $bonusDropRate += ($effect - 1);
		
		//道具掉率加成
		$bonusDropItemRate = 0;
		//聚宝盆buff
		$effect = Data_User_Info::getInstance( $this->userId )->getBuffEffVal( 'itemDrop' , 1 );
		if($effect > 1) $bonusDropItemRate += ($effect - 1); 
		
		$j = 0;
		$stepMonsters = array();
		//初始化这波怪物的信息
		foreach ( $stepMonsterKeys as $mkey )
		{
			//判断这个怪物的配置是否存在
			$monsterId = $monsterArr[$mkey]."_monster";
			if( !$monConf = $monsterConfig[$monsterId] )
			{
				throw new Battle_Exception( Battle_Exception::STATUS_NOT_MONSTER_CONFIG );
			}
				
			$monsterInfo =  array(
					'mid' => $monsterId,
					'dropCard' => $monConf['dropCard'],
					'cardLv' => $monConf['cardLv'],
					'dropRate' => $monConf['dropRate'],
					'dropPriority' => $monConf['dropPriority'],
			);
		
			//根据dropPackage判断掉落(废弃)
// 			if( $j == $dropKey )
// 			{
// 				$this->fillDropMonster($monsterInfo, $randMonsterId, $randMonsterDrop, $dropPackageId, $dropGold, $dropGoldRate);
// 			}
		
			$temp_monsters[] = array( "id"=>$j ) + $monsterInfo;
			$stepMonsters[] = $monsterInfo;
			$j++;
		}
		
		//根据权重判断谁掉，如果权重相同，随机一个
		if( !function_exists( "sort_dropPriority" ) )
		{
			function sort_dropPriority( $a , $b )
			{
				if ( $a['dropPriority'] == $b['dropPriority'] ) return 0;
				return ( $a['dropPriority'] >  $b['dropPriority'] ) ? -1 : 1;
			}
		}
		shuffle( $temp_monsters );
		usort( $temp_monsters , "sort_dropPriority");
		
		//你说这波怪掉的是钱不
		$dropGoldData = $this->getDropGold( $dropGold , $dropGoldRate );
		$dropKey = -1;
		//如果掉的是钱，必须有个怪要掉
		if( $dropGoldData )
		{
			$dropKey = $temp_monsters[0]['id'];
		}
		//不掉钱掉卡
		else
		{
			$cardConfig = Common::getConfig('card');
			foreach( $temp_monsters as $monsterInfo )
			{
				//额外掉率加成
				//道具掉落
				if($cardConfig[$monsterInfo['dropCard']]['category'] > 6)
				{
					if($bonusDropItemRate > 0) $monsterInfo['dropRate'] *= (1 + $bonusDropItemRate);
				}
				//武将掉落
				else
				{
					if($bonusDropRate > 0) $monsterInfo['dropRate'] *= (1 + $bonusDropRate);
				}
				
				if( mt_rand( 1 , 10000 ) <= intval($monsterInfo['dropRate']) )
				{
					$dropKey = $monsterInfo['id'];
					$dropCardData = array(
						"dropCard" => $monsterInfo['dropCard'],
						"cardLv" => $monsterInfo['cardLv'],
					);
					break;
				}
			}
		}
		
		//130531:根据单个怪物的对应掉落判断
		$temp = array();
		//填充怪物信息
		foreach( $stepMonsters as $k => $monsterInfo )
		{
			$monster_temp = array(
					'init_attack_turn' => mt_rand( 1 , intval( $monsterConfig[$monsterInfo['mid']]['attack_turn'] ) ),
					'mid' => $monsterInfo['mid'],
			);
			if( $dropKey == $k )
			{
				if( $dropGoldData )
				{
					$monster_temp['drop_gold'] = $dropGoldData['dropGold'];
				}
				if( $dropCardData )
				{
					$monster_temp['drop_cid'] = $dropCardData['dropCard'];
					$monster_temp['drop_lv'] = $dropCardData['cardLv'];
				}
			}
			$temp[] = $monster_temp;
		}
		$stepMonsters = $temp;
		return $stepMonsters;
	}
	
	/**
	 * 计算掉落金币信息
	 * @param unknown $dropGold
	 * @param unknown $dropGoldRate
	 * @return multitype:unknown |boolean
	 */
	private function getDropGold( $dropGold , $dropGoldRate )
	{
		$randNum = mt_rand( 1 , 10000 );
		if( $randNum <= $dropGoldRate  )
		{
			return array( 'dropGold' => intval($dropGold) );
		}
		return false;
	}
	
	/**
	 * 掉落包掉落(废弃)
	 * @deprecated
	 * @param unknown_type $packId
	 * @throws Battle_Exception
	 */
	private function drawPackage( $packId , $dropGold , $dropGoldRate  )
	{
		$packageConfig = Common::getConfig( "dropPackage" );
	
		if( !!$dropGoldData = $this->getDropGold( $dropGold , $dropGoldRate ) )
		{
			return $dropGoldData;
		}
		
		if( !$packageConfig[$packId] )
		{
			throw new Battle_Exception( Battle_Exception::STATUS_NOT_PACKAGE_CONFIG );
		}
		
		$preRate = $rate = 0;
		$randNum = mt_rand( 1 , 10000 );
		foreach ( $packageConfig[$packId] as $pack )
		{
			$rate += $pack['rate'];
			if( $randNum >= $preRate && $randNum < $rate )
			{
				return $pack;
			}
			$preRate += $pack['rate'];
		}
		return array();
	}
	
	/**
	 * 攻击力校验
	 * @param unknown $damages
	 */
	private function _checkDamage( $damages )
	{
		//TODO::先不校验
		return true;
		if( !is_array( $damages ) || count( $damages ) < 1 || empty( $damages[0] ) )
		{
			return false;
		}
		$teamInfo = Data_Card_Team::getInstance( $this->userId )->getData();
		$teamArr = array();
		$cardId = 0;
		$attacks = array();
		if( $teamInfo['cards'] )
		{
			$teamArr = explode( "," , $teamInfo['cards'] );
			foreach (  $teamArr as $cardId )
			{
				$cardInfo = Card_Model::getInstance( $this->userId )->getCardInfo( $cardId );
				$cardConfig = Common::getConfig( "card" );
				$cardConf = $cardConfig[$cardInfo['cardId']];
				
				$attacks[] = $cardConf['attack'] * 30;
			}
		}
		
		$maxAttack = max( $attacks );
		foreach ( $damages as $damage )
		{
			if( $damage > $maxAttack )
			{
				return false;
			}
		}
		return true;
	}
	
	
}
