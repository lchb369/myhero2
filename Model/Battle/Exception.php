<?php

class Battle_Exception extends GameException
{
	/**
	 * 不存在该房间
	 * @var	int
	 */
	const STATUS_NOT_ROOM_CONFIG = 600;
	/**
	 * 不存在该怪物
	 * @var	int
	 */
	const STATUS_NOT_MONSTER_CONFIG = 601;
	/**
	 * 没有掉落包配置
	 * @var	int
	 */
	const STATUS_NOT_PACKAGE_CONFIG = 602;
	/**
	 * 没有挑战信息
	 * @var	int
	 */
	const STATUS_NOT_BATTLE_INFO = 603;
	/**
	 * 没有支援友军信息
	 * @var	int
	 */
	const STATUS_NOT_HELPER_HISTORY = 604;
	
	/**
	 * 不在活动时间内
	 * @var unknown_type
	 */
	const STATUS_NOT_IN_ACTIVE_TIME = 605;
	
	/**
	 * 战斗回合过长
	 * @var unknown
	 */
	const STATUS_OVER_ROUND_LIMIT = 606;
	
	/**
	 * 攻击数值无效
	 * @var unknown
	 */
	const STATUS_OVER_ATTACK_LIMIT = 607;
	
}

?>
