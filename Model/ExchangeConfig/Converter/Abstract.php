<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

abstract class ExchangeConfig_Converter_Abstract
{
	protected $filePath;
	
	protected $data;
	
	public function __construct( $filePath )
	{
		$this->filePath = $filePath;
		$this->data = null;
	}
	
	public function getData()
	{
		if( $this->data == null )
		{
			$this->readFile();
		}
		return $this->data;
	}
	
	abstract protected function readFile();
}
?>