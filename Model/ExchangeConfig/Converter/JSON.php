<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class ExchangeConfig_Converter_JSON extends ExchangeConfig_Converter_Abstract
{
	protected function readFile()
	{
		$this->data = array(
			'columns' => array() ,
			'datas' => json_decode( file_get_contents( $this->filePath ) , true ) , 
		);
	}
}
?>