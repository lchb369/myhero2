<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * @author Lucky
 */
abstract class ExchangeConfig_Converter_Excel_Abstract extends ExchangeConfig_Converter_Abstract
{

	/**
	 * 
	 * @see ExchangeConfig_Converter_Abstract::readFile()
	 */
	protected function readFile()
	{
		ini_set( 'memory_limit' , '512M' );
		
		/** PHPExcel */
		require_once MOD_DIR .'/PHPExcel/PHPExcel.php';
		PHPExcel_Autoloader::Register();
		
		/** PHPExcel_IOFactory */
		require_once MOD_DIR .'/PHPExcel/PHPExcel/IOFactory.php';
		
		$objReader = $this->getReader();
		$objReader->setReadDataOnly( true );
		
		$objPHPExcel = $objReader->load( $this->filePath );
		$objWorksheet = $objPHPExcel->getActiveSheet();
		
		$iRow = 0;
		$indexKeys = array();
		$this->data = array(
			'columns' => array() ,
			'datas' => array() ,
		);
		foreach( $objWorksheet->getRowIterator() as $row )
		{
			if( $iRow == 0 )
			{
				$iRow++;
				continue;
			}
		
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false); // This loops all cells,
			// even if it is not set.
			// By default, only cells
			// that are set will be
			// iterated.
			$iCell = -1;
			
			foreach( $cellIterator as $cell )
			{
				$iCell++;
				if( $iRow == 1 )
				{
					$this->data['columns'][] = $indexKeys[$iCell] = trim( $cell->getValue() );
					continue;
				}
				$this->data['datas'][$iRow - 2][$indexKeys[$iCell]] = $cell->getValue();
				if( substr( $this->data['datas'][$iRow - 2][$indexKeys[$iCell]] , 0 , 1 ) == '=' )
				{
					echo "The cell {$iRow}:{$iCell} has expression string -> {$this->data['datas'][$iRow - 2][$indexKeys[$iCell]]}\n";
				}
			}

			$iRow++;
		}
		
		spl_autoload_register( '__autoload' );
	}
	
	/**
	 * 获取Excel读取器
	 */
	abstract protected function getReader();
}
?>