<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class ExchangeConfig_Converter_Excel_Excel5 extends ExchangeConfig_Converter_Excel_Abstract
{
	/**
	 * @see ExchangeConfig_Converter_Excel_Abstract::getReader()
	 */
	protected function getReader()
	{
		return PHPExcel_IOFactory::createReader( 'Excel5' );
	}
}
?>