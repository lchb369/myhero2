<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class ExchangeConfig_Converter_Excel_Excel2007 extends ExchangeConfig_Converter_Excel_Abstract
{
	/**
	 * @see ExchangeConfig_Converter_Excel_Abstract::getReader()
	 */
	protected function getReader()
	{
		return PHPExcel_IOFactory::createReader( 'Excel2007' );
	}
}
?>