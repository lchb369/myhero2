<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 配置验证错误提示列表
 * @author Lucky
 */
class ExchangeConfig_ValidateErrorList extends ArrayObject
{
	/**
	 * 错误列表所属文件名
	 * @var string
	 */
	private $_fileName;
	
	/**
	 * @return string
	 */
	public function getFileName()
	{
		return $this->_fileName;
	}

	public function __construct( $fileName = '' )
	{
		parent::__construct( array() );
		$this->_fileName = $fileName;
	}
	
	/**
	 * 添加错误信息
	 * @param ExchangeConfig_ValidateError $error
	 * @return ExchangeConfig_ValidateErrorList
	 */
	public function addError( ExchangeConfig_ValidateError $error )
	{
		if( $this->_fileName )
		{
			$error->setFileName( $this->_fileName );
		}
		$this->append( $error );
		return $this;
	}
	
	/**
	 * 添加错误信息列表
	 * @param ExchangeConfig_ValidateError $error
	 * @return ExchangeConfig_ValidateErrorList
	 */
	public function addErrorList( ExchangeConfig_ValidateErrorList $errorList )
	{
		foreach( $errorList as $error )
		{
			if( $this->_fileName )
			{
				$error->setFileName( $this->_fileName );
			}
			$this->append( $error );
		}
		return $this;
	}
	
	/**
	 * 清空错误列表
	 */
	public function clean()
	{
		$this->exchangeArray( array() );
	}
}
?>