<?php

/** 
 * 游戏基本配置
 * @author liuchangbing
 */
class ExchangeConfig_Exchanger_Excel_GameBase extends ExchangeConfig_Exchanger_Excel_Abstract
{
	public function __construct()
	{
		parent::__construct( 'gameBase.xls' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_Excel_Abstract::validateExcelFile()
	 */
	public function validateExcelFile()
	{
		
	}
}
?>