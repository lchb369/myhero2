<?php

/** 
 * 怪物配置
 * @author liuchangbing
 */
class ExchangeConfig_Exchanger_Excel_Monster extends ExchangeConfig_Exchanger_Excel_Abstract
{
	public function __construct()
	{
		parent::__construct( 'monster.xls' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_Excel_Abstract::validateExcelFile()
	 */
	public function validateExcelFile()
	{
	}
}
?>