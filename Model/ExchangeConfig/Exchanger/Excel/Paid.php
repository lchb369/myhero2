<?php

/** 
 * 人物等级配置
 * @author liuchangbing
 */
class ExchangeConfig_Exchanger_Excel_Paid extends ExchangeConfig_Exchanger_Excel_Abstract
{
	public function __construct()
	{
		parent::__construct( 'paid.xls' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_Excel_Abstract::validateExcelFile()
	 */
	public function validateExcelFile()
	{
	}
}
?>