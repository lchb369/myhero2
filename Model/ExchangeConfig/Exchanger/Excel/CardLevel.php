<?php

/** 
 * 武将卡升级信息
 * @author liuchangbing
 */
class ExchangeConfig_Exchanger_Excel_CardLevel extends ExchangeConfig_Exchanger_Excel_Abstract
{
	public function __construct()
	{
		parent::__construct( 'cardLevel.xls' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_Excel_Abstract::validateExcelFile()
	 */
	public function validateExcelFile()
	{
	}
}
?>