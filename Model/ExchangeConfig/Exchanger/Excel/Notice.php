<?php

/** 
 * 滚动公告
 * @author liuchangbing
 */
class ExchangeConfig_Exchanger_Excel_Notice extends ExchangeConfig_Exchanger_Excel_Abstract
{
	public function __construct()
	{
		parent::__construct( 'notice.xls' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_Excel_Abstract::validateExcelFile()
	 */
	public function validateExcelFile()
	{
	}
}
?>