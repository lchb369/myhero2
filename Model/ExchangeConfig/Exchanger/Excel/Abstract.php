<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * Excel数据表转换器
 * @author Lucky
 */
abstract class ExchangeConfig_Exchanger_Excel_Abstract
{
	/**
	 * 保存Excel表解释后的表数据
	 * @var	array
	 */
	protected $table;
	
	/**
	 * 错误提示列表
	 * @var ExchangeConfig_ValidateErrorList
	 */
	protected $validateError;
	
	/**
	 * 最后修改日期
	 * @var int
	 */
	private $_lastModifyTime;
	
	/**
	 * Excel表文件名
	 * @var	string
	 */
	private $_fileName;
	
	/**
	 * 文件类型
	 * @var int
	 */
	private $_fileType;
	
	/**
	 * 文件类型（EXCEL5——Office 1997-2003版本）
	 * @var int
	 */
	const FILE_TYPE_EXCEL5 = 0;
	
	/**
	 * 文件类型（Office 2007版本）
	 * @var int
	 */
	const FILE_TYPE_EXCEL2007 = 1;
	
	/**
	 * 文件类型（JSON）
	 * @var int
	 */
	const FILE_TYPE_JSON = 2;
	
	/**
	 * @return ExchangeConfig_ValidateErrorList
	 */
	public function getValidateError()
	{
		return $this->validateError;
	}

	/**
	 * 最后修改日期
	 * @return int
	 */
	public function getLastModifyTime()
	{
		return $this->_lastModifyTime;
	}
	
	/**
	 * 获取表数据
	 * @return array
	 */
	public function getTable()
	{
		return $this->table;
	}

	/**
	 * 设置表数据
	 * @param array $table	表数据
	 */
	public function setTable( $filePath )
	{
		$this->table = $this->_getFileReader( $filePath )->getData();
		$this->_writeFile();
	}
	
	/**
	 * 设置数据
	 * @param array $datas	数据
	 * @param boolean $isWriteDataToFile	是否保存数据到文件
	 */
	public function setDatas( $datas , $isWriteDataToFile = true )
	{
		$this->table = array(
			'columns' => array() ,
			'datas' => $datas ,
		);
		
		if( is_array( $datas ) )
		{
			foreach( $datas as $data )
			{
				foreach( array_keys( $data ) as $key )
				{
					if( !in_array( $key , $this->table['columns'] ) )
					{
						$this->table['columns'][] = $key;
					}
				}
			}
		}
		
		if( $isWriteDataToFile )
		{
			$this->_writeFile();
		}
	}

	/**
	 * 获取Excel表文件名
	 * @return string
	 */
	public function getFileName()
	{
		return $this->_fileName;
	}

	/**
	 * 实例化Excel数据表对象（文件名：*.xls）
	 * @param string $fileName	Excel表文件名
	 */
	public function __construct( $fileName )
	{
		$this->_fileName = $fileName;
		$this->_initFileType();
		$this->_readFile();
		$this->validateError = new ExchangeConfig_ValidateErrorList( $fileName );
	}
	
	/**
	 * 验证Excel数据表内容是否有错
	 */
	abstract public function validateExcelFile();
	
	/**
	 * 验证数据行是否空白
	 * @param array $line
	 */
	protected function isEmptyLine( $line )
	{
		$isEmptyLine = true;
		foreach( $line as $key => $value )
		{
			if( strlen( trim( $value ) ) > 0 )
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 读取数据文件
	 */
	private function _readFile()
	{
		if( !file_exists( $this->_getFilePath() ) )
		{
			$this->table = array();
			$this->_lastModifyTime = 0;
			return;
		}
		
		$this->table = eval( rtrim( ltrim( file_get_contents( $this->_getFilePath() ) , '<?php ' ) , '?>' ) );
		$this->_updateLastModifyTime();
	}
	
	/**
	 * 保存数据文件
	 */
	private function _writeFile()
	{
		@chmod( dirname( $this->_getFilePath() ) , 0777 );
		
		if( file_exists( $this->_getFilePath() ) )
		{
			@chmod( $this->_getFilePath() , 0777 );
		}
		else
		{
			//mkdir( $this->_getFilePath()  , 0777 );
			//$this->_writeFile();
		}
		file_put_contents( $this->_getFilePath() , "<?php\nreturn ". var_export( $this->table , true ) .";\n?>" );
		$this->_updateLastModifyTime();
	}
	
	/**
	 * 获取文件路径
	 * @return string
	 */
	private function _getFilePath()
	{
		return CONFIG_DIR.Common::getGameConfPath().'/ExchangedExcel/'.$this->_fileName.'.php';
	}
	
	/**
	 * 更新最后文件修改时间
	 */
	private function _updateLastModifyTime()
	{
		$this->_lastModifyTime = filemtime( $this->_getFilePath() );
	}
	
	/**
	 * 获取文件读取器
	 * @return	ExchangeConfig_Converter_Abstract
	 */
	private function _getFileReader( $filePath )
	{
		switch( $this->_fileType )
		{
			case self::FILE_TYPE_EXCEL5:
				
				return new ExchangeConfig_Converter_Excel_Excel5( $filePath );
				
			case self::FILE_TYPE_EXCEL2007:
				
				return new ExchangeConfig_Converter_Excel_Excel2007( $filePath );
				
			case self::FILE_TYPE_JSON:
				
				return new ExchangeConfig_Converter_JSON( $filePath );
		}
	}
	
	/**
	 * 初始化文件类型
	 */
	private function _initFileType()
	{
		if( strrpos( $this->_fileName , '.xlsx' ) !== false )
		{
			$this->_fileType = self::FILE_TYPE_EXCEL2007;
		}
		else if( strrpos( $this->_fileName , '.xls' ) !== false )
		{
			$this->_fileType = self::FILE_TYPE_EXCEL5;
		}
		else if( strrpos( $this->_fileName , '.json' ) !== false )
		{
			$this->_fileType = self::FILE_TYPE_JSON;
		}
	}
}
?>
