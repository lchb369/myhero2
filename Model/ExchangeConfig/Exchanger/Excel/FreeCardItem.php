<?php

/** 
 * 免费武将卡信息
 * @author liuchangbing
 */
class ExchangeConfig_Exchanger_Excel_FreeCardItem extends ExchangeConfig_Exchanger_Excel_Abstract
{
	public function __construct()
	{
		parent::__construct( 'freeCardItem.xls' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_Excel_Abstract::validateExcelFile()
	 */
	public function validateExcelFile()
	{
	}
}
?>