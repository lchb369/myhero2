<?php

/** 
 * 武将卡信息
 * @author liuchangbing
 */
class ExchangeConfig_Exchanger_Excel_GWOrder extends ExchangeConfig_Exchanger_Excel_Abstract
{
	public function __construct()
	{
		parent::__construct( 'gworder.xls' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_Excel_Abstract::validateExcelFile()
	 */
	public function validateExcelFile()
	{
	}
}
?>