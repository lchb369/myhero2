<?php

/** 
 * 掉落包配置
 * @author liuchangbing
 */
class ExchangeConfig_Exchanger_Excel_DropPackage extends ExchangeConfig_Exchanger_Excel_Abstract
{
	public function __construct()
	{
		parent::__construct( 'dropPackage.xls' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_Excel_Abstract::validateExcelFile()
	 */
	public function validateExcelFile()
	{
	}
}
?>