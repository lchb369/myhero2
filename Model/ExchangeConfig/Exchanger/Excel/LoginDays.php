<?php

/** 
 * 普通战场
 * @author liuchangbing
 */
class ExchangeConfig_Exchanger_Excel_LoginDays extends ExchangeConfig_Exchanger_Excel_Abstract
{
	public function __construct()
	{
		parent::__construct( 'loginDays.xls' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_Excel_Abstract::validateExcelFile()
	 */
	public function validateExcelFile()
	{
	}
}
?>