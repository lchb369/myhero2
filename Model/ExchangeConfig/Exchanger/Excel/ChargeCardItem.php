<?php

/** 
 * 人民币武将卡信息
 * @author liuchangbing
 */
class ExchangeConfig_Exchanger_Excel_ChargeCardItem extends ExchangeConfig_Exchanger_Excel_Abstract
{
	public function __construct()
	{
		parent::__construct( 'chargeCardItem.xls' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_Excel_Abstract::validateExcelFile()
	 */
	public function validateExcelFile()
	{
	}
}
?>