<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 游戏基本配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Common extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'common.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildCommon();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'common.xls' ,
		);
	}
	
	/**
	 * 建立药品数据
	 */
	private function _buildCommon()
	{
		foreach( $this->tables['common.xls'] as $config )
		{
			$this->phpConfig = $config;
		}
	}
	
}
?>