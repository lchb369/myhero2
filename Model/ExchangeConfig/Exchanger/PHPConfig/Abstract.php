<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * PHP配置转换器
 * @author Lucky
 */
abstract class ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	/**
	 * Excel数据表
	 * @var	array
	 */
	protected $tables;
	
	/**
	 * PHP配置文件名
	 * @var	string
	 */
	protected $fileName;
	
	/**
	 * PHP配置内容
	 * @var array
	 */
	protected $phpConfig;
	
	/**
	 * 最后修改日期
	 * @var int
	 */
	protected $lastModifyTime;
	
	/**
	 * 生成配置错误提示列表
	 * @var ExchangeConfig_ValidateErrorList
	 */
	protected $errorList;
	
	/**
	 * 文件读写适配器
	 * @var ExchangeConfig_Exchanger_PHPConfig_Adapter_Interface
	 */
	private $_fileRWAdapter;
	
	/**
	 * 文件类型
	 * @var int
	 */
	private $_fileType;
	
	/**
	 * 是否需要验证道具ID
	 * @var boolean
	 */
	private $_isNeedValidateItemId = false;
	
	/**
	 * 生成后的文件类型（PHP配置文件）
	 * @var unknown_type
	 */
	const FILE_TYPE_CONFIG = 0;
	
	/**
	 * 生成后的文件类型（PHP配置文件）
	 * @var unknown_type
	 */
	const FILE_TYPE_SQL = 1;
	
	/**
	 * 返回文件类型
	 * @return int
	 */
	public function getFileType()
	{
		return $this->_fileType;
	}

	/**
	 * 返回PHP配置
	 * @return array
	 */
	public function getPHPConfig()
	{
		return $this->phpConfig;
	}

	/**
	 * @return ExchangeConfig_ValidateError
	 */
	public function getErrorList()
	{
		return $this->errorList;
	}
	
	/**
	 * 获取是否生成过程中出错
	 * @return	boolean
	 */
	public function hasError()
	{
		return count( $this->errorList ) > 0 ? true : false;
	}

	/**
	 * 最后修改日期
	 * @return int
	 */
	public function getLastModifyTime()
	{
		return $this->lastModifyTime;
	}

	/**
	 * 获取PHP配置文件名
	 * @return string
	 */
	public function getFileName()
	{
		return $this->fileName;
	}

	/**
	 * 设置Excel数据表
	 * @param string $excelFileName	Excel数据表名称
	 * @param array $table
	 * @return ExchangeConfig_Exchanger_PHPConfig_aExchanger
	 */
	public function setTable( $excelFileName , $table )
	{
		$this->tables[$excelFileName] = $table['datas'];
		return $this;
	}
	
	/**
	 * 实例化PHP配置转换器
	 * @param string $fileName	配置文件名
	 * @param int $fileType	文件类型
	 * @param boolean $needValidateItemId	是否需要验证道具ID
	 */
	protected function __construct( $fileName , $fileType = self::FILE_TYPE_CONFIG , $needValidateItemId = false )
	{
		$this->fileName = $fileName;
		$this->_fileType = $fileType;
		$this->_isNeedValidateItemId = $needValidateItemId;
		$this->_fileRWAdapter = self::_getFileRWAdapterMethod( $this->_fileType );
		$this->errorList = new ExchangeConfig_ValidateErrorList( $fileName );
		$this->_readFile();
	}
	
	/**
	 * 保存PHP配置文件
	 */
	public function toSaveFile()
	{
		$this->_fileRWAdapter->writeFile( $this->_getFilePath() , $this->phpConfig );
		$this->_updateLastModifyTime();
	}
	
	/**
	 * 被同步配置文件（只能够ExchangeConfig_Exchanger_List_PHPConfig调用）
	 * @param array $phpConfig
	 */
	public function friendlyBeSynchronizeConfig( $phpConfig )
	{
		$this->phpConfig = $phpConfig;
	}
	
	/**
	 * 验证数据行是否空白
	 * @param array $line
	 */
	protected function isEmptyLine( $line )
	{
		$isEmptyLine = true;
		foreach( $line as $key => $value )
		{
			if( strlen( trim( $value ) ) > 0 )
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 读取PHP配置文件
	 */
	private function _readFile()
	{
		$this->phpConfig = $this->_fileRWAdapter->readFile( $this->_getFilePath() );
		$this->_updateLastModifyTime();
	}
	
	private function _getFilePath()
	{
		return CONFIG_DIR.Common::getGameConfPath().'/OperationData/'.$this->fileName;
	}
	
	/**
	 * 更新最后文件修改时间
	 */
	private function _updateLastModifyTime()
	{
		$this->lastModifyTime = $this->_fileRWAdapter->getFileModifyTime( $this->_getFilePath() );
	}
	
	private static function _getFileRWAdapterMethod( $fileType )
	{
		switch( $fileType )
		{
			case self::FILE_TYPE_CONFIG:
				return new ExchangeConfig_Exchanger_PHPConfig_Adapter_Config();
			case self::FILE_TYPE_SQL:
				return new ExchangeConfig_Exchanger_PHPConfig_Adapter_SQL();
		}
	}
	
	protected function getAllItemIds()
	{
		$itemIds = array();
		//预加载无功能道具
		if( !empty( $this->tables['sundry.xls'] ) )
		{
			foreach( $this->tables['sundry.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		if( !empty( $this->tables['baseItem.xls'] ) )
		{
			foreach( $this->tables['baseItem.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		//预加载装备
		if( !empty( $this->tables['equipment.xls'] ) )
		{
			foreach( $this->tables['equipment.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		//预加载强化石
		if( !empty( $this->tables['enchantstone.xls'] ) )
		{
			foreach( $this->tables['enchantstone.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		//预加载神恩石
		if( !empty( $this->tables['safeamulet.xls'] ) )
		{
			foreach( $this->tables['safeamulet.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		//预加载幸运符
		if( !empty( $this->tables['luckamulet.xls'] ) )
		{
			foreach( $this->tables['luckamulet.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		//预加载合成幸运石
		if( !empty( $this->tables['mixtrueamulet.xls'] ) )
		{
			foreach( $this->tables['mixtrueamulet.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		//预加载油漆罐
		if( !empty( $this->tables['paint.xls'] ) )
		{
			foreach( $this->tables['paint.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		//预加载药瓶
		if( !empty( $this->tables['prop.xls'] ) )
		{
			foreach( $this->tables['prop.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}

		//预加载熔合石
		if( !empty( $this->tables['transferstone.xls'] ) )
		{
			foreach( $this->tables['transferstone.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}

		//预加载任务物品
		if( !empty( $this->tables['collection.xls'] ) )
		{
			foreach( $this->tables['collection.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		//预加载沙漏
		if( !empty( $this->tables['hourglass.xls'] ) )
		{
			foreach( $this->tables['hourglass.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		//预加载礼盒
		if( !empty( $this->tables['giftbox.xls'] ) )
		{
			foreach( $this->tables['giftbox.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		//预加载宝石
		if( !empty( $this->tables['jewel.xls'] ) )
		{
			foreach( $this->tables['jewel.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		//预加载镶嵌符
		if( !empty( $this->tables['inlayLuckyRune.xls'] ) )
		{
			foreach( $this->tables['inlayLuckyRune.xls'] as $line => $item )
			{
				$itemIds[] = (integer)$item['stuffId'];
			}
		}
		
		return $itemIds;
	}
	
	/**
	 * 申请使用Excel数据表名称
	 */
	abstract protected function needExcelFileToRegister();
	
	/**
	 * 抽象类需要申请使用Excel数据表名称
	 */
	public function needExcelFilesToRegister()
	{
		$needExcelFiles = $this->needExcelFileToRegister();
		if( $this->_isNeedValidateItemId )
		{
			$needExcelFiles = array_merge( $needExcelFiles , array(
					'sundry.xls' , 'baseItem.xls' , 'equipment.xls' , 'enchantstone.xls' ,
					'safeamulet.xls' , 'luckamulet.xls' , 'mixtrueamulet.xls' , 'paint.xls' , 'prop.xls' ,
					'transferstone.xls' , 'collection.xls' , 'hourglass.xls' , 'giftbox.xls' , 'inlayLuckyRune.xls' , 'jewel.xls' ,
				)
			);
			$needExcelFiles = array_unique( $needExcelFiles );
		}
		return $needExcelFiles;
	}
	
	/**
	 * 生成PHP配置文件
	 */
	abstract public function buildPHPConfigFile();
	
	/**
	 * 验证PHP生成后的PHP文件
	 */
	abstract public function validatePHPConfigFile();
}
?>