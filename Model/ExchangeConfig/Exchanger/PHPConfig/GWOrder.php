<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_GWOrder extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'gworder.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildLevel();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'gworder.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildLevel()
	{
		foreach( $this->tables['gworder.xls'] as $config )
		{
			
			$tstr = trim(  $config['time'], "_" );
			$t = strtotime( $tstr );
			
			$this->phpConfig[] = array(
				'time' => $t,
				'price' => $config['price'],
				'pf' => $config['pf'],
				'third' => trim( $config['third'] ),
			);
		}
	
	
	}
	
	
	
	
}
?>
