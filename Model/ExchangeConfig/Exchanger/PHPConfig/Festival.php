<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 首页活动配置
 */
class ExchangeConfig_Exchanger_PHPConfig_Festival extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'festival.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildConfig();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'festival.xls' ,
		);
	}
	
	/**
	 * 生成数据
	 */
	private function _buildConfig()
	{
		foreach( $this->tables['festival.xls'] as $config )
		{
			$this->phpConfig[intval($config['id'])] = array(
				'id' => intval($config['id']),
				'name' => $config['name'],
				'type' => intval($config['type']),	
				'start_time' => $config['start_time'], 
				'end_time' => $config['end_time'],
				'desc' => $config['desc'],
				'rewardDesc' => $config['rewardDesc'],
				'getTimes' => intval($config['getTimes']),
				'rewardType' => intval($config['rewardType']),
				'rewardValue' => strval($config['rewardValue']),
				'level' => intval($config['level']),
				'instanceType' => intval($config['instanceType']),
				'instanceID' => intval($config['instanceID']),
			);
		}
		krsort($this->phpConfig);
	}
	
}
?>