<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Goods extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'goods.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildGoods();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'goods.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildGoods()
	{
		foreach( $this->tables['goods.xls'] as $config )
		{
			$this->phpConfig[$config['goodsId']] = array(
				'goodsId' => strval( $config['goodsId'] ),
				'goodsName' => strval( $config['goodsName'] ),
				'goodsDesc' => strval( $config['goodsDesc'] ),
				'price' => floatval( $config['price'] ),
				'coin' => intval( $config['coin'] ),
				'limit' => intval( $config['limit']),
				//各种渠道
				//公共
				'GooglePlay' => floatval( $config['GooglePlay'] ),
				'GooglePlayId' => strval( $config['GooglePlayId'] ),
				'AppStore' => floatval( $config['AppStore'] ),
				'AppStoreId' => strval( $config['AppStoreId'] ),
				//台服
				'MyCard' => intval( $config['MyCard'] ),
				'IMoney' => intval( $config['IMoney'] ),
				'Telepay' => floatval( $config['Telepay'] ),
				'TelepayId' => strval( $config['TelepayId'] ),
				'H3G' => floatval($config['H3G']),
				'H3GId' => strval($config['H3GId']),
				//韩服
				'Naver' => floatval($config['Naver']),
				'NaverId' => strval( $config['NaverId'] ),
				'TStore' => floatval($config['TStore']),
				'TStoreId' => strval( $config['TStoreId'] ),
				//越南 ThienPhu
				'TPCard' => floatval($config['TPCard']),
				'TPCardId' => strval($config['TPCardId']),
				'TPSms' => floatval($config['TPSms']),
				'TPSmsId' => strval($config['TPSmsId']),
			);
		}
	}
	
}
?>