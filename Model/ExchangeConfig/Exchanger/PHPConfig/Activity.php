<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Activity extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'activity.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildConfig();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'activity.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildConfig()
	{
		foreach( $this->tables['activity.xls'] as $config )
		{
			$this->phpConfig[$config['type']][$config['id']] = array(
				'subType' => $config['subType'],
				'desc' => $config['desc'],
				'start_time' => $config['start_time'], 
				'end_time' => $config['end_time'],
				'coin' => intval( $config['coin'] ),
				'gold' => intval( $config['gold'] ),
				'gachaPoint' => intval( $config['gachaPoint'] ),
				'exp' => floatval($config['exp']),
				'pattern' => intval($config['pattern']),
				'custom' => strval($config['custom']),
			);
		}
	}
	
}
?>