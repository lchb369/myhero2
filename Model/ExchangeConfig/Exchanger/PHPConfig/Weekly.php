<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Weekly extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'weekly.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildWeekly();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'weekly.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildWeekly()
	{
		$rooms = array();
		foreach( $this->tables['weekly.xls'] as $config )
		{
			if( !$this->phpConfig[$config['weekId']] )
			{
				$this->phpConfig[$config['weekId']] = array(
					'name' => strval( $config['name'] ),
					'start_time' => strval( $config['start_time']),		
					'end_time' => strval( $config['end_time']),
					'week_day' => $config['week_day'],
					'effect' => array(),
					'diamond_type' => explode( ",", $config['diamond_type'] ),
					'dungeon_type' => strval( $config['dungeon_type'] ),
				);
			}
			
			$stepsInfo = array();
			//计算怪的波数和每波数量
			for( $i = 1 ; $i<= 9; $i++ )
			{
				if( $config[$i."steps"] )
				{
					if( preg_match( "/^\[/" , $config[$i."steps"] ))
					{
						$stepsInfo[$i] = json_decode( $config[$i."steps"] , true );
					}
					else
					{
						$stepsInfo[$i] = $config[$i."steps"];
					}
				}
					
			}
			$this->phpConfig[$config['weekId']]['rooms'][$config['roomId']] = array(
				'name' => $config['roomName'],
				'stamina' => $config['roomStamina'],
				'steps' => $config['roomSteps'],
				'stepsMonster' =>  $stepsInfo,
				'monsters' => $config['monsters'],
				'boss' => explode( ",", $config['boss'] ),
				'package' => $config['drop_package'],
				'randMonster' => $config['randomMonster'], //乱入怪,10%的机率会出，占据一个小怪位
				'randDrop' => $config['randomDrop'],	//乱入怪的掉落
				'dropGold' => $config['dropGold'] ? $config['dropGold'] : 0 ,
				'dropGoldRate' => $config['dropGoldRate'] ?  $config['dropGoldRate']  : 0 ,
			);
		}
	}
	
}
?>