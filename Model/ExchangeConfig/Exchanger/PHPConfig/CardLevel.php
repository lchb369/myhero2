<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_CardLevel extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'cardLevel.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildCardLevel();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'cardLevel.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildCardLevel()
	{
		foreach( $this->tables['cardLevel.xls'] as $config )
		{
			$key = intval( $config['key'] );
			$value = intval( $config['value'] );
			$this->phpConfig['exp_type'][$config['exp_type']][$key] = $value;
		}
		
		$types = array_keys( $this->phpConfig['exp_type'] );
		foreach ( $types as $type )
		{
			ksort( $this->phpConfig['exp_type'][$type] );
		}
		
		
	}
	
}
?>