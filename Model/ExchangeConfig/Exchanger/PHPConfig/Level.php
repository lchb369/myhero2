<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Level extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'level.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildLevel();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'level.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildLevel()
	{
		foreach( $this->tables['level.xls'] as $config )
		{
			$this->phpConfig[$config['level']] = array(
				'exp' => (int)$config['exp'],
				'maxStamina' => (int)$config['maxStamina'],
				'maxfriends' => (int)$config['maxfriends'],
				'leaderShip' => (int)$config['leaderShip'],
				'bonus' => empty($config['bonus']) ? array() : json_decode($config['bonus'], true),
			);
		}
	}
	
}
?>