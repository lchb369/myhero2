<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_ShopItem extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'shopItem.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildShopItems();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'shopItem.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildShopItems()
	{
		foreach( $this->tables['shopItem.xls'] as $config )
		{
			$this->phpConfig[$config['itemId']] = array(
				'itemId' => strval( $config['itemId'] ),
				'itemName' => strval( $config['itemName'] ),
				'itemDesc' => strval( $config['itemDesc'] ),
				'num' => intval( $config['num'] ),
				//原价
				'price' => intval( $config['price'] ),
				'coin' => intval( $config['coin'] ),
				//效果类型
				'type' =>  strval( $config['type'] ),
				'effectTime' =>  intval( $config['effectTime'] ),
				'effectRate' =>  floatval( $config['effectRate'] ),
				'effectValue' =>  strval( $config['effectValue'] ),
			);
		}
	}
	
}
?>