<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Effect extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'effect.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildConfig();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'effect.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildConfig()
	{
		foreach( $this->tables['effect.xls'] as $config )
		{
			if( $config['typeName1'] == "stamina" )
			{
				$config['typeValue1'] = floatval( $config['typeValue1'] );
			}
			elseif(  $config['typeName1'] == "gold" )
			{
				$config['typeValue1'] = intval( $config['typeValue1'] );
			}
			elseif(  $config['typeName1'] == "card" )
			{
				$config['typeValue1'] = floatval( $config['typeValue1'] );
			}
			
			
			$this->phpConfig[$config['effectId']] = array(
				'name' => strval( $config['name'] ),
				'type' => array(
						$config['typeName1'] => $config['typeValue1']		
				),	
			);
		}
	}
	
}
?>