<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_UpgradeSkill extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'upgradeSkill.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildUpgradeSkill();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'upgradeSkill.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildUpgradeSkill()
	{
		foreach( $this->tables['upgradeSkill.xls'] as $config )
		{
			$this->phpConfig[$config['id']] = array(
				'id' => (int)$config['id'],
				'skillName' => (string)$config['skillname'],
				'upType' => (string)$config['upType'],
				'skillType' => (string)$config['skillType'],
				'maxLevel' => (int)$config['maxLevel'],
				'initCd' => intval( $config['initCd'] ),
				'cd' => intval( $config['cd'] ),
				'target' => floatval( $config['target'] ),
				'tarSelf' => floatval( $config['tarSelf']),
				'element' => intval( $config['element'] ),
				'effectBase' => floatval( $config['effectBase'] ),
				'effect' => floatval( $config['effect'] ),
				'duration' => intval( $config['duration'] ),
				'type' => intval( $config['type'] ),
			);
		}	
	}
	
}
?>