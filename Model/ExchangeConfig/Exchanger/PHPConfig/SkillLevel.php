<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_SkillLevel extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'skillLevel.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildSkillLevel();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'skillLevel.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildSkillLevel()
	{
		foreach( $this->tables['skillLevel.xls'] as $config )
		{
			$key = intval( $config['level'] );
			$value = intval( $config['value'] );
			$this->phpConfig[$config['Type']][$key] = $value;
		}
		
		$types = array_keys( $this->phpConfig );
		foreach ( $types as $type )
		{
			ksort( $this->phpConfig[$type] );
		}
		
		
	}
	
}
?>