<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 公告配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Notice extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'notice.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildConfig();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'notice.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildConfig()
	{
		$rooms = array();
		foreach( $this->tables['notice.xls'] as $config )
		{
			$this->phpConfig[$config['id']] = array(
				'enable' => intval($config['enable']),
				'start_time' => strval( $config['start_time']),		
				'end_time' => strval( $config['end_time']),
				'interval' => intval($config['interval']),
				'msg' => strval( $config['msg'] ),
			);
		}
	}
	
}
?>
