<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Welfare extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'welfare.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildSkillLevel();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'welfare.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildSkillLevel()
	{
		foreach( $this->tables['welfare.xls'] as $config )
		{
			$this->phpConfig[intval($config['type'])][intval($config['id'])] = array(
				"desc" => $config['desc'],
				"startTime" => $config['startTime'],
				"endTime" => $config['endTime'],
				"rankType" => intval($config['rankType']),
				"rank" => $config['rank'],
				"paidWelfare" => intval($config['paidWelfare']),
				"prizeType" => intval($config['prizeType']),
				"prizeCard" => $config['prizeCard'],
				"prizeNum" => intval($config['prizeNum']),
				"prizeDesc" => $config['prizeDesc'],
			);
		}
		
		$types = array_keys( $this->phpConfig );
		foreach ( $types as $type )
		{
			ksort( $this->phpConfig[$type] );
		}
		
		
	}
	
}
?>