<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_LoginDays extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'loginDays.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildPaid();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'loginDays.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildPaid()
	{
		foreach( $this->tables['loginDays.xls'] as $config )
		{
			$this->phpConfig[$config['id']] = array(
				'id' => (int)$config['id'],
				'prizeCoin' => (int)$config['prizeCoin'],
				'prizeGold' => (int)$config['prizeGold'],
				'prizePoint' => (int)$config['prizePoint'],
			
			);
		}
	}
	
}
?>