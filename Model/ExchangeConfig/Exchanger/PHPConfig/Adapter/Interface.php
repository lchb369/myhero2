<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * PHP配置文件读写方式接口
 * @author Lucky
 */
interface ExchangeConfig_Exchanger_PHPConfig_Adapter_Interface
{
	/**
	 * 读取文件
	 * @param string $filePath	文件路径
	 * @return	array
	 */
	public function readFile( $filePath );
	
	/**
	 * 写入文件
	 * @param string $filePath	文件路径
	 * @param array $phpConfig	PHP配置
	 * @return	boolean 
	 */
	public function writeFile( $filePath , $phpConfig );
	
	/**
	 * 获取文件修改时间
	 * @param string $filePath	文件路径
	 * @return	integer
	 */
	public function getFileModifyTime( $filePath );
}
?>