<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * @author Lucky
 */
class ExchangeConfig_Exchanger_PHPConfig_Adapter_Config implements ExchangeConfig_Exchanger_PHPConfig_Adapter_Interface
{
	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Adapter_Interface::getFileModifyTime()
	 */
	public function getFileModifyTime( $filePath )
	{
		if( file_exists( $filePath ) )
		{
			return filemtime( $filePath );
		}
		return 0;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Adapter_Interface::writeFile()
	 */
	public function writeFile( $filePath , $phpConfig )
	{
		@chmod( dirname( $filePath ) , 0777 );
		
		if( file_exists( $filePath ) )
		{
			@chmod( $filePath , 0777 );
		}
		
		return file_put_contents( $filePath , "<?php\nreturn ". var_export( $phpConfig , true ) .";\n?>" ) ? true : false;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Adapter_Interface::readFile()
	 */
	public function readFile( $filePath )
	{
		if( file_exists( $filePath ) )
		{
			return include( $filePath );
		}
		return array();
	}
}
?>