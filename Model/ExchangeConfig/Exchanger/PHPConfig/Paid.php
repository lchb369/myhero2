<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Paid extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'paid.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildPaid();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'paid.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildPaid()
	{
		foreach( $this->tables['paid.xls'] as $config )
		{
			$this->phpConfig[(int)$config['type']][(int)$config['id']] = array(
				'id' => (int)$config['id'],
				'type' => (int)$config['type'],
				'desc' => (string)$config['desc'],
				'startTime' => (string)$config['startTime'],
				'endTime' => (string)$config['endTime'],
				'paidCoin' => (int)$config['paidCoin'],
				'prizeCard' => (string)$config['prizeCard'],
				'prizeCardNum' => (int)$config['prizeCardNum'],
				'prizeDesc' => (string)$config['prizeDesc'],
			);
		}
	}
	
}
?>