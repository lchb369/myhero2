<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 掉落包生成规则
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_DropPackage extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'dropPackage.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildConfig();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'dropPackage.xls' ,
		);
	}
	
	/**
	 * 生成掉落包规则
	 */
	private function _buildConfig()
	{

		foreach( $this->tables['dropPackage.xls'] as $config )
		{
	
			
			$this->phpConfig[$config['id']][] = array(
					'cardId' => $config['cardId'],
					'cardLevel' => $config['cardLevel'],
					'rate' => $config['rate'],
			);
			
			
		//	$this->phpConfig[$config['id']] = $item;
		}
	}
	
}
?>