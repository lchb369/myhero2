<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Normal extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'normal.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildConfig();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'normal.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildConfig()
	{
		$rooms = array();
		foreach( $this->tables['normal.xls'] as $config )
		{
			if( !$this->phpConfig[$config['id']] )
			{
				$this->phpConfig[$config['id']] = array(
					'name' => strval( $config['name'] ),
					'effect' => array(
						'start_time' => $config['effect_start_time'],
						'end_time' => $config['effect_end_time'],
						'number' => (string)$config['effect_number'],		
					),
					'dungeon_type' => strval( $config['dungeon_type'] ),
				);
			}
			
			$stepsInfo = array();
			//计算怪的波数和每波数量
			for( $i = 1 ; $i<= 9; $i++ )
			{
				if( $config[$i."steps"] )
				{
				
					if( preg_match( "/^\[/" , $config[$i."steps"] ))
					{
						$stepsInfo[$i] = json_decode( $config[$i."steps"] , true );
					}
					else
					{
						$stepsInfo[$i] = $config[$i."steps"];
					}
				}
			}
			
			$this->phpConfig[$config['id']]['rooms'][$config['roomId']] = array(
				'name' => $config['roomName'],
				'stamina' => $config['stamina'],
				'steps' => $config['steps'],
				'stepsMonster' =>  $stepsInfo,
				'monsters' => $config['monsters'],
				'boss' => explode( ",",  $config['boss'] ),
				'package' => $config['drop_package'],
				'randMonster' => $config['randomMonster'], //乱入怪,10%的机率会出，占据一个小怪位
				'randDrop' => $config['randomDrop'],	//乱入怪的掉落
				'dropGold' => $config['dropGold'] ? $config['dropGold'] : 0 ,
				'dropGoldRate' => $config['dropGoldRate'] ?  $config['dropGoldRate']  : 0 ,
			);
		}
	}
	
}
?>