<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Card extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'card.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildCard();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'card.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildCard()
	{
		foreach( $this->tables['card.xls'] as $config )
		{
			$upgNeed = array();
			if( $config['upg_need'] )
			{
				$upgNeed = explode( ",", $config['upg_need'] );
			}
			
			$this->phpConfig[$config['cardId']] = array(
				'category' => strval( $config['category'] ),	
				'upg_gold' => intval( $config['upg_gold'] ),
				'star' => strval( $config['star'] ),
				'name' => strval( $config['name']),
				'base_exp' => floatval( $config['base_exp']),
				'recover_growth' => floatval( $config['recover_growth']),
				'hp' => intval( $config['hp'] ),
				'hp_growth' => floatval( $config['hp_growth'] ),
				'ctype' => strval( $config['ctype'] ),
				'skid' => strval( $config['skid'] ),
				'exp_type' => strval( $config['exp_type'] ),
				'width' => intval( $config['width']),
				'attack'  => intval( $config['attack']),
				'cost'  => intval( $config['cost']),
				'leader_skid' => strval( $config['leader_skid'] ),
				'attack_growth' => floatval( $config['attack_growth'] ),
				'sell_gold' => intval( $config['sell_gold'] ),
				'recover' => intval( $config['recover'] ),
				'max_lv' => intval( $config['max_lv'] ),
				'upg_target' => strval( $config['upg_target'] ),
				'upg_need' => $upgNeed ? $upgNeed : array(),
				'skill_type' =>  strval( $config['skillType'] ),
				'skill_value' => intval( $config['skillValue'] ),
				'drop' => strval( $config['dropItemSource'] ),
			);
		}
	}
	
}
?>