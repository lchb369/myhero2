<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 等级配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_SuperChargeCard extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'superChargeCard.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildCard();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'superChargeCard.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildCard()
	{
		foreach( $this->tables['superChargeCard.xls'] as $config )
		{
			$this->phpConfig[$config['id']] = array(
				'cardId' => $config['cardId'],
				'cardLevel' => (int)$config['cardLevel'],
				'rate' => (int)$config['rate'],
			);
		}
	}
	
}
?>