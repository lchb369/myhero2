<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 怪物配置
 * @author Liuchangbing
 */
class ExchangeConfig_Exchanger_PHPConfig_Monster extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'monster.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildConfig();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'monster.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildConfig()
	{
		foreach( $this->tables['monster.xls'] as $config )
		{
			$this->phpConfig[$config['id']] = array(
					'drop_gold' => $config['drop_gold'],
					'cid' => $config['cid'],
					'hp' => $config['hp'],
					'attack' => $config['attack'],
					'defense' => $config['defense'],
					'attack_turn' => $config['attack_turn'],
					'drop_exp' => $config['drop_exp'],
					'dropCard' => $config['dropCard'],
					'cardLv' => intval( $config['cardLv'] ),
					'dropRate' => intval( $config['dropRate'] ),
					'dropPriority' => intval( $config['dropPriority'] ),
			);
		}
	}
	
}
?>