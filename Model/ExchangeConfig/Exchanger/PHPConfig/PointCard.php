<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/** 
 * 点将台
 */
class ExchangeConfig_Exchanger_PHPConfig_PointCard extends ExchangeConfig_Exchanger_PHPConfig_Abstract
{
	public function __construct()
	{
		parent::__construct( 'pointCard.php' );
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::buildPHPConfigFile()
	 */
	public function buildPHPConfigFile()
	{
		$this->phpConfig = array();
		$this->_buildConfig();
		return $this->phpConfig;
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::validatePHPConfigFile()
	 */
	public function validatePHPConfigFile()
	{
	}

	/**
	 * 
	 * @see ExchangeConfig_Exchanger_PHPConfig_Abstract::needExcelFileToRegister()
	 */
	protected function needExcelFileToRegister()
	{
		return array(
			'pointCard.xls' ,
		);
	}
	
	/**
	 * 生成等级数据
	 */
	private function _buildConfig()
	{
		foreach( $this->tables['pointCard.xls'] as $config )
		{
			//id:类型2位, classesID:子类型3位
			$this->phpConfig[Activity_PointCard::makePointCardId($config['id'], $config['classesID'])] = array(
				'id' => intval($config['id']),
				'classesID' => intval($config['classesID']),
				'desc' => strval($config['desc']),
				'start_time' => $config['start_time'], 
				'end_time' => $config['end_time'],
				'source' => $this->_getPackage(explode('$', $config['source']), explode('$', $config['sourceNum'])),
				'target' => $this->_getPackage(explode('$', $config['target']), explode('$', $config['targetNum'])),
				'limit' => intval($config['limit']),
			);
		}
	}
	
	private function _getPackage($itemArr, $numArr)
	{
		$ret = array();
		if(!empty($itemArr) && !empty($numArr))
		$idx = 0;
		foreach($itemArr as $item)
		{
			$ret[$item] = intval($numArr[$idx++]); 
		}
		return $ret;
	}
	
}
?>