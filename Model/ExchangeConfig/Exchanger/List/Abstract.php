<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * Excel转换器列表
 * @author Lucky
 */
abstract class ExchangeConfig_Exchanger_List_Abstract extends ArrayObject
{
	/**
	 * 转换器类的路径
	 * @var string
	 */
	private $_classPath;
	
	/**
	 * 所有模块的路径
	 * @var string
	 */
	private $_modelPath;
	
	/**
	 * 实例化Excel转换器列表
	 */
	public function __construct( $classPath )
	{
		ini_set( 'memory_limit' , '512M' );
		$this->_classPath = str_replace( "\\" , '/' , $classPath );
		$this->_modelPath = str_replace( "\\" , '/' , MOD_DIR ) .'/';
		parent::__construct( $this->_constructAllExchanger() );
	}
	
	/**
	 * 创建所有转换器
	 */
	private function _constructAllExchanger()
	{
		$exchangers = array();
		foreach( $this->_getAllExchangerNames() as $fileName )
		{
			
			$exchangerName = $this->_getClassName( $fileName );
			$exchanger = new $exchangerName;
			$exchangers[$exchanger->getFileName()] = $exchanger;
		}
		return $exchangers;
	}
	
	/**
	 * 获取所有转换器名称
	 */
	private function _getAllExchangerNames()
	{
		$exchangerNames = array();
		$dir = dir( $this->_classPath );
		
		while( false !== ( $fileName = $dir->read() ) )
		{
			
			if( !is_file( $this->_classPath . $fileName ) )
			{
				continue;
			}
			
			if( strpos( $fileName , '.' ) === 0 )
			{
				continue;
			}
			
			if( strpos( $fileName , '.php' ) === false )
			{
				continue;
			}
			
			if( $fileName == 'Abstract.php' )
			{
				continue;
			}
			
			
			$exchangerNames[] = $fileName;
		}
		return $exchangerNames;
	}
	
	private function _getClassName( $fileName )
	{
		return $this->_getClassBaseName() . substr( $fileName , 0 , strrpos( $fileName , '.php' ) );
	}
	
	private function _getClassBaseName()
	{
		$classPathLength = strlen( $this->_classPath );
		$modelPathLength = strlen( $this->_modelPath );
		
		$relativePath = substr( $this->_classPath , $modelPathLength , $classPathLength - $modelPathLength );
		
		return str_replace( '/' , '_' , $relativePath ); 
	}
}
?>