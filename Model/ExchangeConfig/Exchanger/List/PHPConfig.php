<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * PHP配置转换器列表
 * @author Lucky
 */
class ExchangeConfig_Exchanger_List_PHPConfig extends ExchangeConfig_Exchanger_List_Abstract
{
	/**
	 * 错误列表
	 * @var ExchangeConfig_ValidateErrorList
	 */
	private $_errorList;
	
	/**
	 * 获取错误列表
	 * @return ExchangeConfig_ValidateErrorList
	 */
	public function getErrorList()
	{
		return $this->_errorList;
	}
	
	/**
	 * 获取是否有错误
	 * @return	boolean
	 */
	public function hasError()
	{
		return count( $this->_errorList ) > 0 ? true : false;
	}

	/**
	 * 实例化PHP配置转换器列表
	 */
	public function __construct()
	{
		parent::__construct( dirname( dirname( __FILE__ ) ) .'/PHPConfig/' );
		$this->_errorList = new ExchangeConfig_ValidateErrorList();
	}
	
	/**
	 * 生成配置
	 */
	public function buildConfigurations()
	{
		$excelList = new ExchangeConfig_Exchanger_List_Excel();
		$this->_errorList->clean();
		$platform = Common::getConfig( 'platform');
		foreach( $this as $exchanger )
		{
			$needFiles = $exchanger->needExcelFilesToRegister();
			$isEnoughFiles = true;
			foreach( $needFiles as $fileName )
			{
				if( !isset( $excelList[$fileName] ) || !$excelList[$fileName]->getTable() )
				{
					$this->_errorList->addError(
						new ExchangeConfig_ValidateError(
							'The @exchanger:'. $exchanger->getFileName() .' need '. $fileName .' not exist.' ,
							ExchangeConfig_ValidateError::ERR_ERROR
						)
					);
					$isEnoughFiles = false;
					break;
				}
				
				$exchanger->setTable( $fileName , $excelList[$fileName]->getTable() );
			}
			
			if( $isEnoughFiles )
			{
				$exchanger->buildPHPConfigFile();
				$exchanger->validatePHPConfigFile();
				
				if( $exchanger->hasError() )
				{
					$this->_errorList->addErrorList( $exchanger->getErrorList() );
					continue;
				}
				
				$exchanger->toSaveFile();
			}
		}
	}
	
	/**
	 * 同步配置
	 */
	public function synchronizeConfig()
	{
		$phpConfigs = array();
		//制作同步参数
		foreach( $this as $exchanger )
		{
			if( $exchanger->getFileType() == ExchangeConfig_Exchanger_PHPConfig_Abstract::FILE_TYPE_CONFIG )
			{
				$phpConfigs[$exchanger->getFileName()] = $exchanger->getPHPConfig();
			}
			else if( $exchanger->getFileType() == ExchangeConfig_Exchanger_PHPConfig_Abstract::FILE_TYPE_SQL )
			{
				/*
				$sqls = explode( ";\n" , $exchanger->getPHPConfig() );
				if( is_array( $sqls ) )
				{
					$systemConfig = Common::getConfig( 'mysqlDb' );
					$db = new MysqlDb( $systemConfig['taskIndex'] );
					foreach( $sqls as $sql )
					{
						$db->query( $sql );
					}
				}
				Task_Command_User::getInstance()->updateDefine();
				*/
			}
		}
	}
	
	/**
	 * 被同步配置（囧）
	 * @param array $phpConfig
	 */
	public function beSynchronizeConfig( $phpConfigs )
	{
		foreach( $phpConfigs as $fileName => $phpConfig )
		{
			$this[$fileName]->friendlyBeSynchronizeConfig( $phpConfig );
			$this[$fileName]->toSaveFile();
		}
	}
}
?>