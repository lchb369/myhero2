<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * Excel转换器列表
 * @author Lucky
 */
class ExchangeConfig_Exchanger_List_Excel extends ExchangeConfig_Exchanger_List_Abstract
{
	/**
	 * 实例化Excel转换器列表
	 */
	public function __construct()
	{
		parent::__construct( dirname( dirname( __FILE__ ) ) .'/Excel/' );
	}
}
?>