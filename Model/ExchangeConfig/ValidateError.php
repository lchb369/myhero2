<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 配置错误提示
 * @author Lucky
 */
class ExchangeConfig_ValidateError
{
	/**
	 * 错误等级
	 * @var	int
	 */
	private $_errorLevel;
	
	/**
	 * 错误信息
	 * @var	string
	 */
	private $_errorMessage;
	
	/**
	 * 错误所属文件名
	 * @var string
	 */
	private $_fileName;
	
	/**
	 * 错误
	 * @var	int
	 */
	const ERR_ERROR = 2;
	
	/**
	 * 警告
	 * @var	int
	 */
	const ERR_WARNING = 1;
	
	/**
	 * 提示
	 * @var	int
	 */
	const ERR_NOTICE = 0;

	/**
	 * @param string $_fileName
	 */
	public function setFileName( $fileName )
	{
		$this->_fileName = $fileName;
	}

	/**
	 * 实例化
	 * @param string $errorMessage	错误消息
	 * @param int $errorLevel	错误等级
	 */
	public function __construct( $errorMessage , $errorLevel = self::ERR_NOTICE , $fileName = null )
	{
		$this->_errorMessage = $errorMessage;
		$this->_errorLevel = $errorLevel;
	}
	/**
	 * 获取错误等级
	 * @return int
	 */
	public function getErrorLevel()
	{
		return $this->_errorLevel;
	}

	/**
	 * 获取错误信息
	 * @return string
	 */
	public function getErrorMessage()
	{
		return $this->_errorMessage;
	}
	
	public function __toString()
	{
		$str = $this->_fileName ? "[{$this->_fileName}]" : '';
		$str .= '[';
		switch( $this->_errorLevel )
		{
			case self::ERR_NOTICE:
				$str .= 'NOTICE';
				break;
				
			case self::ERR_WARNING:
				$str .= 'WARNING';
				break;
				
			case self::ERR_ERROR:
				$str .= 'ERROR';
				break;
				
			default:
				$str .= 'UNKNOWN CLASS';
				break;
		}
		$str .= '] '. $this->_errorMessage;
		return $str;
	}
}
?>