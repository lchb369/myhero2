<?php 
/**
 * 通用功能类
 */
class Common
{
	/**
	 * 普通数据的数据库引擎
	 * @var array(
	 * 			{dbKey}:array(
	 * 				{dbId}:MysqlDb
	 * 			)
	 * 		)
	 */
	private static $_dbNormalEngine = array();
	
	/**
	 * 解除转义
	 * @param	mixed $var
	 * @return	mixed
	 */
	public static function prepareGPCData( & $var )
	{
		if( is_array( $var ) )
		{
			while( ( list( $key , $val ) = each( $var ) ) != null )
			{
				$var[$key] = self::prepareGPCData( $val );
			}
		}
		else 
		{
			$var = stripslashes( $var );
		}
		
		return $var;
	}
	
	/**
	 * 获取系统配置信息
	 * @param	string $key		配置文件项
	 * @return	array
	 */
	public static function & getConfig( $key = '' )
	{
		static $config = array();
		if( !$config )
		{
			$sysConfig = self::getSysConfig();
			$config = self::getConfigFile( $sysConfig );
		}

		if( !isset( $config[$key] ) )
		{
			$config[$key] = self::getConfigFile( $key );
		}
		
		if( $key )		
			return $config[$key];
		
		return $config;
	}
	
	/**
	 * 设置系统配置
	 * @param	string $key		配置文件项
	 * @param	mixed $value	配置值
	 */
	public static function setConfig( $key , $value )
	{
		$content = "<?php\n";
		$content .= "return ". var_export( $value , true ) .";\n";
		$content .= "?>";
		$fp = fopen( CONFIG_DIR . "/{$key}.php" , "w" );
		fwrite( $fp , $content );
		fclose( $fp );
	}
	
	/**
	 * 获取Cache实例
	 * @param	string $param	Cache服务器名称
	 * @return	iCache
	 */
	public static function & getCache( $param = 'data' )
	{
		static $cache = array();
		if( empty( $cache[$param] ) )
		{
			$config = self::getConfig( 'memcache' );
			if( empty( $config[$param] ) )
			{
				$cache[$param] = false;
				
				//系统错误,缺少必要的Memcache配置
				return false;
			}
			else
			{
				$memcacheClass = self::getConfig( 'memcacheClass' );
				$cache[$param] = new $memcacheClass( $config[$param] );
			}
		}
		
		return $cache[$param];
	}
	
	/**
	 * 获取DB实例
	 * @param	string $dbName	DB名称
	 * @return	iDber
	 */
	public static function & getDB( $uId )
	{
		$dbClassName = Common::getConfig( 'dbClassName' );
		$uId = ltrim( $uId , '0' );
		switch ( $dbClassName )
		{
			case 'MysqlPool':
			
				return MysqlPool::getInstance( $uId );
			case 'MysqlDber':
				return MysqlDber::getInstance( $uId );
			case 'Dber':
				return Dber::getInstance( $uId );
		}
		throw new GameException( 101 );
	}
	
	/**
	 * 获取框架客户端
	 * @param	string $serverName	服务器名称
	 * @return	RestClient
	 */
	public static function & getClient( $serverName = 'frameServer' )
	{
		static $client = array();
		if( empty( $client[$serverName] ) )
		{
			$config = Common::getConfig( $serverName );
			$client[$serverName] = new RestClient( $config['url'] , $config['protocalType'] , $config['timeout'] );
		}
		return $client[$serverName];
	}

	/**
	 * 获取Task对象
	 * @param	int $uId			用户ID
	 * @return	FM_TaskSystem
	 */
	public static function & getTask( $uId )
	{
		return FM_TaskSystem::getInstance( $uId );
	}

	/**
	 * 获取统计器对象
	 * @param	int $uId			用户ID
	 * @return	FM_Accumulator
	 */
	public static function & getAccumulator( $uId )
	{
		return FM_Accumulator::getInstance( $uId );
	}
	
	/**
	 * 计算最小的不重复值
	 * @param	array $ids			数字
	 * @param	int $min			最小值
	 * @return	int
	 */
	public static function computeMinUnique( $ids , $min = 1 )
	{
		array_multisort( $ids , SORT_ASC );
		foreach ( $ids as $item )
		{
			if( $min == $item )
			{
				$min++;
			}
		}
		return $min;
	}
	
	/**
	 * 根据概率计算结果
	 * @param	int $probability( 0 ~ 100 支持两位小数)
	 * @return	boolean
	 */
	public static function computeResult( $probability )
	{
		$seed = rand( 1 , 10000 );
		if( $seed <= $probability * 100 )
			return true;
		return false;
	}
	
	public static function getCurrentUserId()
	{
		return (integer)$GLOBALS['uId'];
	} 
	
	public static function & getConfigFile( $configKey )
	{
		static $config = array();
		
		if($config[$configKey]) return $config[$configKey];
		
		$gameConfKey = CONFIG_DIR.Common::getGameConfPath()."/OperationData/{$configKey}.php";
		$sysConfKey = CONFIG_DIR . "/SystemConfig/{$configKey}.php";
		if( file_exists( $gameConfKey ) )
		{
			$config[$configKey] = require( $gameConfKey );
		}
		else if( file_exists( $sysConfKey ) )
		{
			$config[$configKey] = require( $sysConfKey );
		}
		
		return $config[$configKey];
	}
	
	/**
	 * 获取普通数据的数据库引擎
	 * @param string $dbKey	数据库的键
	 * @return	MysqlDb
	 */
	public static function & getNormalDatabaseEngine( $dbKey )
	{
		if( !isset( self::$_dbNormalEngine[$dbKey] ) )
		{
			$dbConfig = Common::getConfig( 'mysqlDb' );
			self::$_dbNormalEngine[$dbKey] = new MysqlDb( $dbConfig[$dbKey] );
		}
		return self::$_dbNormalEngine[$dbKey]; 
	}
	
	public static function getSysConfig()
	{
		$sysConfig = "Sys";
		$pf = $GLOBALS['appPlatform'] ? $GLOBALS['appPlatform'] : $_SESSION['appPlatform'];
		$sid = $GLOBALS['appSid'] ? $GLOBALS['appSid'] : $_SESSION['appSid'] ;

		if( !$pf || !$sid )
		{
			echo '';
		}
		$sysConfig .= strtoupper( $pf ).'_'.$sid;
		return $sysConfig;
	}
	
	
	public static function _getDbEngine( $userId )
	{
		$result['dbId'] = 1;
		$dbConfig = Common::getConfig( "mysqlDb" );
		
		static $dbEngines = array();
		if( !isset( $dbEngines[$result['dbId']] ) )
		{
			$dbEngines[$result['dbId']] = new MysqlDb( 
				array(
					'host' => $dbConfig['game']['host'],
					'port' => $dbConfig['game']['port'],
					'user' =>  $dbConfig['game']['user'],
					'passwd' =>  $dbConfig['game']['passwd'],
					'name' =>  $dbConfig['game']['name'],
					)
				);
		}
		return $dbEngines[$result['dbId']];
	}
	
	public static function getServerAddr()
	{
		static $serverAddr = "";
		if( empty( $serverAddr ) )
		{
			preg_match( "/^\/(.*)\/Web\/(.*)/",$_SERVER['REQUEST_URI']  , $reqUrlArr);
			$serverAddr = "http://".$_SERVER['SERVER_ADDR']."/".$reqUrlArr[1]."/";
		}		
		return $serverAddr;
	}
	
	public static function getGameConfPath()
	{
		$appPlatform = $_SESSION['appPlatform'] ? $_SESSION['appPlatform'] : $GLOBALS['appPlatform'];
		$appSid = $_SESSION['appSid'] ? $_SESSION['appSid'] : $GLOBALS['appSid'];
		return '/GameConfig/'.strtoupper( $appPlatform ).'_'.$appSid;
	}
	
	public static function getServerConfBySid($sid)
	{
		$serverConf = Common::getConfigFile('ServerConfig');
		foreach($serverConf as &$config)
		{
			if($config['sid'] == $sid)
			{
				return $config;
			}
		}
		return null;
	}
	
}



function output($response)
{
	header('Cache-Control: no-cache, must-revalidate');
	header("Content-Type: text/plain; charset=utf-8");
	ob_start('ob_gzip');
	echo $response;
	ob_end_flush();
}

//这是ob_gzip压缩机
function ob_gzip($content)
{
	if( !headers_sent() &&
			extension_loaded("zlib") &&
			isset($_SERVER["HTTP_ACCEPT_ENCODING"]) &&
			strstr($_SERVER["HTTP_ACCEPT_ENCODING"],"gzip"))
	{
		$content = gzencode($content."",9);

		header("Content-Encoding: gzip");
		header("Vary: Accept-Encoding");
		header("Content-Length: ".strlen($content));
	}
	return $content;
}




/**
 * 自动加载类文件
 * @param string $classname
 */
function __autoload( $classname ) 
{
	$classname = str_replace( '_' , '/' , $classname );

	//在模块文件夹搜索
	if( file_exists( MOD_DIR . "/{$classname}.php" ) )
	{
		include_once( MOD_DIR . "/{$classname}.php" );
		return ;
	}
	
	//在控制器文件夹搜索
	if( file_exists( CON_DIR . "/{$classname}.php" ) )
	{
		include_once( CON_DIR . "/{$classname}.php" );
		return ;
	}
}