<?php
/**
 * 用户游戏信息
 * 
 * @name Info.php
 * @author Liuchangbing
 * @since 2013-1-14
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Order_Model extends Logical_Abstract
{
	
	
	const CURRENCY_CNY_2_TWD = 5;
	
	
	const PROMOTION_PACK_1 = 101;
	
	/**
	 * 单例对象
	 * @var	Order_Model[]
	 */
	protected static $singletonObjects;

	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Order_Model
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	
	public function checkOrderStatus( $orderId )
	{
		$rs = Data_Order_Model::getInstance( $this->userId  )->checkOrderStatus( $orderId );
		return $rs;
	}
	
	/**
	 * 支付宝订单
	 */
	public function addOrder( $orderParams )
	{
		return Data_Order_Model::getInstance( $this->userId , true )->addOrder( $orderParams );
	}
	
	/**
	 * 通用发货接口
	 * @param unknown $goodsId
	 * @param unknown $GoodsCount
	 */
	public function SendGoods( $goodsId , $GoodsCount , $payType , $payCoin = 0 , $orderId = '' )
	{
		$goodsConfig = Common::getConfig( "goods" );
		$goodsConf = $goodsConfig[$goodsId];
		if( $goodsConf || $payCoin > 0 )
		{
			$GoodsCount = $GoodsCount ? $GoodsCount : 1;
			$addCoin = $GoodsCount * $goodsConf['coin'];
	
			if( $payCoin > 0 && $goodsConf['price'] != $payCoin  )
			{
				$addCoin = $payCoin;
			}

			if( !$goodsConf['price'] )
			{
				$goodsConf['price'] = $payCoin;
			}
			
		
			$payLog = new ErrorLog( "addCoin" );

			
			
			//促销礼包不参与首冲翻倍
			if( $goodsId != Order_Model::PROMOTION_PACK_1 && Data_Order_Model::getInstance( $this->userId )->isFirstRecharge() )
			{
				$payLog->addLog(  "uid:".$this->userId."addCoin*2:".$addCoin."firstRecharge" );
				$addCoin = $addCoin * 2;
			}
			
			$payLog->addLog(  "uid:".$this->userId."addCoin:".$addCoin."orderCount".$orderCount );
			
			/**
			 * 运营统计：充值记录
			 */
			Stats_Model::addPayUser( 
				$this->userId  , 
				$_SERVER['REQUEST_TIME'] , 
				$goodsConf['price'] ,	//价格 
				$addCoin , 	//元宝数
				$goodsId ,	//商口ID
				$GoodsCount,
				$payType
			);
			
			/**
			 * EDAC数据中心统计
			 */
			Stats_Edac::recharge( $this->userId, $orderId , $payCoin, $addCoin, $payType );
			
			return User_Info::getInstance( $this->userId )->changeCoin( $addCoin , '充值' );
		}
		return false;
	}
	
	/**
	 * 更新订单
	 * @param unknown $orderParams
	 */
	public function updateOrder( $orderParams )
	{
		$rs = Data_Order_Model::getInstance( $this->userId , true )->updateOrder( $orderParams );
		return $rs;
	}
	
	
	public function getData()
	{
		return Data_Order_Model::getInstance( $this->userId )->getData();
	}
	
	public function getOrder( $orderId )
	{
		return Data_Order_Model::getInstance( $this->userId )->getOrder( $orderId );
	}
}
