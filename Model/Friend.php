<?php

class Friend
{
	/**
	 * 用户ID
	 * @var	int
	 */
	protected $userId;
	
	/**
	 * 单例对象
	 * @var	Friend[]
	 */
	protected static $singletonObjects = array();
	
	protected static $config = null;
	
	/**
	 * 状态：作为担保人（用于悬赏关卡）
	 */
	const TYPE_BE_GUARANTOR = 1;
	
	/**
	 * 状态：作为雇佣者（用于水手医院）
	 */
	const TYPE_BE_EMPLOYER = 2;
	
	/**
	 * 获取实例
	 * @param	int $userId	用户ID
	 * @return	Friend
	 */
	public static function getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new Friend( $userId );
		}
		
		if( !isset( self::$config ) )
		{
			self::$config = Common::getConfig();
		}
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 实例化
	 * @param	int $userId	用户ID
	 */
	protected function __construct( $userId )
	{
		$this->userId = $userId;
	}
	
	/**
	 * 获取好友ID
	 * 
	 * @param boolean $isIncludeMe 是否包括我自己
	 * @return	int[]
	 */
	public function getFriendIds( $isIncludeMe = true , $version = 0 )
	{
		
		if( self::$config['etc']['friendSystem'] == 'neighbor' && Common::getConfig( "platform") != "minik" && Common::getConfig( "platform") != "qzone")
		{
			$friendIds = Neighbor::getInstance()->getNeighborIds( $this->userId , $version );
		}
		else 
		{
			$friendIds = FM_FriendServer::getInstance( $this->userId )->getAppIds();
		}
		
		
		if( !$friendIds )
		{
			$friendIds = array();
		}
		
		if( !$isIncludeMe )
		{
			if( in_array( $this->userId , $friendIds ) )
			{
				$friendIds = Helper_Array::array_diff_fast( $friendIds , array( $this->userId ) );
			}
			return $friendIds;
		}
		
		if( !in_array( $this->userId , $friendIds ) )
		{
			$friendIds[] = $this->userId;
		}
		
		return $friendIds;
	}
	
	/**
	 * 获取App好友
	 * @return	int[]
	 */
	public function getAppFriendIds()
	{
		$friendIds = FM_FriendServer::getInstance( $this->userId )->getAppIds();
		
		if( !$friendIds )
		{
			$friendIds = array();
		}
		
		if( !in_array( $this->userId , $friendIds ) )
		{
			$friendIds[] = $this->userId;
		}
		
		return $friendIds;
	}


	/**
	 * 获取好友资料
	 *  @return	array
	 */
	public function getFriendInfo()
	{
		$friendInfo = FM_FriendServer::getInstance( $this->userId )->getFriendList();
		if( !$friendInfo ) return array();
		$platform = Common::getConfig( 'platform' );
		if( $platform == 'weiyouxi' )
		{
			foreach( $friendInfo as $key => $val ) $friendInfo[$key]['verified'] = ( $friendInfo[$key]['verified'] ? 0 : 3 );
		}
		return $friendInfo;
		
	}
	
	/**
	 * 获取列表版本号（最后更新时间）
	 */
	public function getVersion()
	{
		if( self::$config['etc']['friendSystem'] == 'neighbor' )
		{
			return Neighbor::getInstance()->getLastUpdateTime( $this->userId );
		}
		else
		{
			return FM_FriendServer::getInstance( $this->userId )->getVersion();
		}
	}

	/**
	 * 获取多个用户的用户信息
	 * @param	int[] $userIds 用户ID
	 * @return	array(
	 * 				{userId}:array(
	 * 					userName:string
	 * 					headPic:string
	 * 				) ,
	 * 				...
	 * 			)
	 */
	public function getMultiUserInfo( $userIds )
	{
		$returnValue = array();
		$platform = Common::getConfig( 'platform');
		//这个是临时实现方案
		foreach( $userIds as $userId )
		{
			$userInfo = FM_FriendServer::getInstance( $userId )->getUserInfo();
			if( is_array(  $userInfo  )  && isset( $userInfo['headPic'] ) )
			{
				$returnValue[$userId] = FM_FriendServer::getInstance( $userId )->getUserInfo();
			}
			else 
			{
				continue;
			}
			
			if( $platform == 'qzone' )
			{
				$returnValue[$userId]['vipInfo'] = FM_FriendServer::getInstance( $userId )->getVipInfo( $userId );
			}
			elseif( $platform == 'weiyouxi' )
			{
				$returnValue[$userId]['verified'] = ( $returnValue[$userId]['verified'] ? 0 : 3 );
			}
		}
		
		return $returnValue;
	}
	
	
}
