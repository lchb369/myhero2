<?php
/**
 * 邮件
 * 
 * @name Model.php
 * @since 2013-08-23
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Mail_Model extends Logical_Abstract
{
	
	const LOG_TYPE_GET_BONUS = "邮件领取";
	
	/**
	 * 单例对象
	 * @var	User_Model[]
	 */
	protected static $singletonObjects;

	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Mail_Model
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	public function getList($page, $size, $type)
	{
		return Data_Mail_Model::getInstance($this->userId)->getList($page, $size, $type);
	}
	
	public function getUnreadNum()
	{
		return Data_Mail_Model::getInstance($this->userId)->getUnreadNum();
	}
	
	public function sendMail($fromId, $title, $content, $bonus = null, $deadTime = null, $delOld = false)
	{
		$id = Data_Mail_Model::getInstance($this->userId, true)->sendMail($fromId, $title, $content, $bonus, $deadTime);
		if($delOld) $this->delAll($_SERVER['REQUEST_TIME'] - 32 * 86400);
		return $id;
	}
	
	public function readMail($id)
	{
		$ret = Data_Mail_Model::getInstance($this->userId, true)->readMail($id);
		return $ret;
	}
	
	public function getBonus($id)
	{
		$ret = false;
		do
		{
			$lockKey = "Mail.getBonus"."_".$this->userId."_".$id;
			//加锁
			if( !$lock = Helper_Common::addLock( $lockKey ) ) break;
			
			//只有已读状态可以领取
			if(!$getBonus = Data_Mail_Model::getInstance($this->userId, true)->getBonus($id)) break;
			
			$mail = Data_Mail_Model::getInstance($this->userId)->getMail($id);
			if(empty($mail['bonus'])) break;
			$bonus = json_decode($mail['bonus'], true);
			if(empty($bonus)) break;
			if(!empty($bonus['gold']))
			{
				User_Info::getInstance($this->userId)->changeGold($bonus['gold'], Mail_Model::LOG_TYPE_GET_BONUS);
			}
			if(!empty($bonus['coin']))
			{
				User_Info::getInstance($this->userId)->changeCoin($bonus['coin'], Mail_Model::LOG_TYPE_GET_BONUS);
			}
			if(!empty($bonus['card']))
			{
				foreach($bonus['card'] as $_id => $_num)
				{
					for($_i = 0; $_i < $_num; $_i++)
					{
						Card_Model::getInstance($this->userId)->addCard($_id, 1, true, Mail_Model::LOG_TYPE_GET_BONUS);
					}
				}
			}
			
			$ret = $getBonus;
		}while(0);

		if($lock) Helper_Common::delLock( $lockKey );
		
		return $ret;
	}
	
	public function delMail($id)
	{
		$ret = Data_Mail_Model::getInstance($this->userId, true)->delMail($id);
		return $ret;
	}
	
	/**
	 * 前面不要带有Data_Mail_Model操作，批量删除不会删除缓存
	 * @param number $deadTime
	 */
	public function delAll($deadTime = 0)
	{
		$dbEngine = Common::getDB( 1 );
		$cache = Common::getCache();
		
		if(empty($deadTime))
		{
			$sql = "delete from `mail` where `uid`=".$this->userId." and `lock`<>1 and `status`<>1";
		}
		else
		{
			$sql = "delete from `mail` where `uid`=".$this->userId." and `sendTime`<".$deadTime."";
		}
		$dbEngine->query( array( $sql ) );
		$cache->delete( $this->userId."_mail");
		return array('id' => 0);
	}
	
	public function lockMail($id)
	{
		$ret = Data_Mail_Model::getInstance($this->userId, true)->lockMail($id);
		return $ret;
	}
	
	public function unLockMail($id)
	{
		$ret = Data_Mail_Model::getInstance($this->userId, true)->unLockMail($id);
		return $ret;
	}
	
}
