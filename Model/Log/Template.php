<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 日志模板类
 * 
 * @author Simon
 */
class Log_Template
{
	/**
	 * 合并-正常
	 */
	const MERGE_MODE_NORMAL = 1;	
	
    /**
     * 模板配制文件
     * @var array
     */
    private $_config = null;
    
    /**
     * 使用的模板ID
     * @var int
     */
    private $_useTemplateId = null;
    
    /**
     * 实例化
     */
    public function __construct()
    {
        $this->_config = Common::getConfig( 'LogTemplatesConfig' );
    }
    
    /**
     * 设置使用哪块模板进行数据操作
     * @param int $templateId
     */
    public function specifyUseTemplate( $templateId )
    {
        if( !isset( $this->_config[ $templateId ] ) )
        {
            throw new Exception('templatesId invalid');
        }
        $this->_useTemplateId = $templateId;
    }
    
    /**
     * 返回当前模板所使用的ID
     * @return int
     */
    public function getTemplateId()
    {
        return $this->_useTemplateId;
    }
    
	/**
     * 返回当前模板配制
     */
    public function getTemplateConfig()
    {
        return $this->_config[ $this->_useTemplateId ];
    }
    
    /**
     * 返回子模板块的集合
     */
    protected function getSubTemplates()
    {
        return $this->_config[ $this->_useTemplateId ]['subTemplates'];
    }
   
    
    /**
     * 效验文本类型的参数
     * @param string|int $item
     */
    protected function validDataStyleText( $item )
    {
        return is_int( $item ) || is_string( $item ) ? true : false;
    }
    
    /**
     * 效验用户名类型的参数
     * @param array $users
     */
    protected function validDataStyleUser( $users )
    {
        if( !is_array( $users ) )
        {
            return false;
        }
        foreach( $users as $userId )
        {
            if( !is_numeric( $userId ) || $userId <= 0 )
            {
                return false;             
            }
        }
        return true;
    }
    
    
    /**
     * 返回是否合并
     * @return boolean
     */
    public function isMerge()
    {
        return $this->_config[ $this->_useTemplateId ]['merge']['enable'];
    }
    
    
	/**
     * 按模板格式合并数据
     * 
     * return array
     */
    public function mergeData( $data )
    {
    	
    }
}