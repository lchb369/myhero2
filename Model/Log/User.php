<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 独立用户的log存储类
 * 
 * @author Simon
 */
class Log_User extends Log_Abstract
{
    /**
     * 限制日志长度
     * @var int
     */
    public $_logLengthLimit = 50; 
    
    public function __construct( $uid )
    {
        $this->setStoreKey( $uid );
        parent::__construct();
    }
    
    /**
     * 添加一条记录
     * @param Log_Row_Interface $row
     */
    public function addLog( Log_Row_Interface  $row )
    {        
        //合并
        if( $row->getTemplate()->isMerge() == true )
        {
            $row = $this->mergeLog( $row ); 
        }
       
        if(  empty( $this->_logs ) )
        {
        	$this->_logs[1] = $row->toArray(); 
        	 
        }
        else
        {
        	$this->_logs[] = $row->toArray();
        }

        return $this->saveLogsToStore( $this->_logs );
    }
    
    /**
     * 返回全部日志记录
     */
    public function getLogs()
    {
        //将所有日志设为己读并存储
        if( is_array( $this->_logs ) && count( $this->_logs ) > 0 )
        {
        	foreach( $this->_logs as $key => $log )
        	{
        		$this->_logs[$key]['status'] = 1;
        	}
        	
            $this->saveLogsToStore( $this->_logs );
        }
        
        return $this->_logs;
    }
    
    
    /**
     * 返回未读数
     * @return int
     */
    public function getUnreadNum()
    {
        $logs = $this->_logs;
        
        $unreadNum = 0;
        if( is_array( $logs ) )
        {
	        foreach( $logs as $log )
	        {
	            $log['status'] == 0 ? $unreadNum++ : null;
	        }
        }
        
        return $unreadNum;
    }
    
    
    protected function setStoreKey( $key )
    {
        $this->_storeKey = $key . '_userlogs';
    }
    
    /**
     * 合并一条记录
     * 
     * @return Log_Row_User
     */
    protected function mergeLog( Log_Row_User $row )
    {
        $logs = $this->_logs;
        $template = $row->getTemplate();
        
        $currentRowTemplateId = $template->getTemplateId();
        $currentRowTemplateConfig = $template->getTemplateConfig(); 
        
        
        foreach($logs as $pos => $log)
        {
            //合并
            if( $log['templatesId'] == $currentRowTemplateId )
            {
            	
            	switch( $currentRowTemplateConfig['merge']['mode'] )
            	{
            		//覆盖
            		case Log_Template::MERGE_MODE_OVERLAY :
            			unset( $this->_logs[$pos] );
            			break;
            			
            		//偷取 - 当前合并只针对模板1和模板2
            		case Log_Template::MERGE_MODE_STEALING :
            	 		$rowData = $row->getData();
            	 		if( $log['data']['user'][0] == $rowData['user'][0] )
            	 		{
            	 			$rowData['food'] += $log['data']['food'];
            	 		}
            	 		$row->setData( $rowData );
            	 		unset( $this->_logs[$pos] );
            	 		break;
            			
            		//默认不做操作	
            		case Log_Template::MERGE_MODE_NORMAL :
            		default:
            	}

            	$data = $template->mergeData( $data );
            }
        }
        
        //$row->setData( $data );
        
        return $row;
    }
    
    /**
     * 更新日志
     * @param  $logId 日志id
     * @param $data 日志需要更新的数据
     */
    public function update( $logId , $data )
    {
    	if( !isset( $this->_logs[$logId] ) )
    	{
    		return false;
    	}
    	$this->_logs[$logId]['data'] = Helper_Array::arrayMergeRecursiveDistinct( $this->_logs[$logId]['data'] , $data );
    }
}