<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 日志抽象类
 * 
 * @author Simon
 */
abstract class Log_Abstract
{
    /**
     * 日志的全部记录
     * @var array
     */
    protected $_logs = array();
    
    
    /**
     * 存储器的键值
     * @var string
     */
    protected $_storeKey = '';
    
    
    /**
     * 限制日志长度
     * 为null不限制, 如指定数量则会限制长度, 删除按队列方式.
     * @var null|int
     */
    protected $_logLengthLimit = 50;
    
    
    /**
     * 添加日志到log记录集
     * @param $log array
     */
    abstract public function addLog( Log_Row_Interface $row );
    
    /**
     * 设置存储器的键
     */
    abstract protected function setStoreKey( $key );
    
    /**
     * 初始化
     */
    public function __construct()
    {
        $this->_logs = $this->getLogsFromStore();
    }
    
    /**
     * 如果记录集超出指定长度则删除较早的记录
     * 
     * @param $logs array 记录集
     * @param $limit int 指定的长度
     * @return array
     */
    private function cutLogs( $logs , $limit )
    { 
        $len = count( $logs );
        for( $i = 0; $i < $len - $limit; $i++ )
        {
            Helper_Array::arrayKeyShift( $logs , false );
        }

        return $logs;
    }
    
    
    /**
     * 存储全部日志记录
     * @param string $key
     * @param array $store
     */
    protected function saveLogsToStore( $logs )
    {
        //截取日志长度
        if( $this->_logLengthLimit > 0 && count( $logs ) > $this->_logLengthLimit )
        {
            $logs = $this->cutLogs( $logs , $this->_logLengthLimit );
        }
        
        $storeLogKey = $this->getStoreKey();
        return $this->getStore()->set( $storeLogKey, $logs );
    }
    
    
    /**
     * 从存储器内返回全部日志记录
     * @param string $key
     * @return array
     */
    protected function getLogsFromStore()
    {
        $storeLogKey = $this->getStoreKey();
        return $this->getStore()->get( $storeLogKey );
    }
    
    /**
     * 返回存储器的键
     * @return string
     * @throws Exception
     */
    protected function getStoreKey()
    {
        if( empty( $this->_storeKey ) )
        {
            throw new Exception('Store key not exists');
        }

        return $this->_storeKey;
    }
    
    /**
     * 返回存储器
     * @return InuMemcache
     */
    protected function getStore()
    {
        return Common::getCache();
    }
    
    
	/**
	 * 删除所有日志
	 */
	public function removeAll()
	{
		$storeLogKey = $this->getStoreKey();
        return $this->getStore()->set( $storeLogKey, array() );
	}
}