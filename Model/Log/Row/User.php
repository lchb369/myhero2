<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 日志记录行类
 * 
 * @author Simon
 */
class Log_Row_User implements Log_Row_Interface
{
    
    /**
     * 是否已读
     * @var boolean
     */
    protected $_status = 0;
    
    /**
     * 日志的创建时间
     * @var int
     */
    protected $_createDate;
    
    /**
     * 日志子模块的具体数据
     */
    protected $_data;
    
    /**
     * 模板
     * @var Log_Template
     */
    protected $_template = null;
    
    /**
     * 设置当前记录的数据
     * @param int $templateId 模板ID
     * @param int $createTime 创建时间
     * @param array $data 子模板集
     * @throws Exception
     */
    public function setParams( $data, $createTime )
    {
        $this->_createDate = $createTime;
        $this->_data = $data;
    }
    
    /**
     * 返回此条日志的数据
     */
    public function getData()
    {
    	return $this->_data;
    }
    
    /**
     * 设置此条日志的数据
     */
    public function setData( $data )
    {
    	$this->_data = $data;
    	return $this;
    }
    
	/**
     * 设置当前模板
     * @param $template
     */
    public function setTemplate( Log_Template $template)
    {
        $this->_template = $template;
    }
    
    
	/**
     * 返回当前模板
     * @return Log_Template
     */
    public function getTemplate()
    {
        return $this->_template;
    }
    
    
    /**
     * 将当前记录转为数组格式
     * 
     * @return array
     * array(
	 *		'templatesId' 	=>     int,
	 *		'data'       	=>  	array(
	 *                 					'user'	=>	array(int),
	 *									'food'	=>	int
	 *								),
	 *      'status'		=> 0 or 1,
	 *  	'createDate'	=> stamptime
     * )
     */
    public function toArray()
    {
         return array(
        			'templatesId' 	=>    $this->_template->getTemplateId(),
        	    	'data'       	=>    $this->_data,
        	    	'status'		=>    0,
        			'createDate'	=>    $this->_createDate,
                );
    }
}
