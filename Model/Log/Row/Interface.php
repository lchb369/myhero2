<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 记录行接口
 * @author Simon
 */
interface Log_Row_Interface
{
    /**
     * 将当前记录转为数组格式
     */
    public function toArray();
}