<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 日志操作中转类
 */
class Log_Model
{
    /**
     * 返回模板静态实例
     * @return Log_Template
     */
    protected static function getTemplate()
    {
        static $logTemplate = null;
        
        if( $logTemplate === null )
        {
            $logTemplate = new Log_Template();
        }
        return $logTemplate;
    }
    
    /**
     * 返回用户记录类静态实例
     * @param int $uid
     * @return Log_User
     */
    protected static function getLogUser( $uid )
    {
        static $log = array();
        
        if( !isset( $log[ $uid ] ) )
        {
            $log[ $uid ] = new Log_User( $uid );
        }
        return $log[ $uid ];
    }
   
    /**
     * 添加用户日志
     */
    public static function addLog( $userId, $templatesId, $data )
    {
    	if( defined( 'IN_UNIT_TEST' ) )
    	{
    		return;
    	}
        $resultFlag = false;
        try
        {
            //当前记录指定的模板
            $logTemplate = self::getTemplate();
            $logTemplate->specifyUseTemplate( $templatesId );
            //新建记录行
            $row = new Log_Row_User();
            $row->setTemplate( $logTemplate );
            $row->setParams( $data, $_SERVER['REQUEST_TIME'] );
            
            //将记录行添加到记录集内
            $log = self::getLogUser( $userId );
            $resultFlag = $log->addLog( $row );
		}
		catch ( Exception $e )
		{
		    $errorMessage = $e->getMessage();
		}

		return array('result' => $resultFlag, 'message' => $errorMessage);
    }
    
    /**
     * 更新日志
     * @param int $logId
     * @param array $data
     */
    public static function updateLog( $userId , $logId , $data )
    {
    	$log = self::getLogUser( $userId );
    	return $log->update( $logId , $data );
    }
    
    /**
     * 返回一个用户的全部日志
     */
    public static function getLog( $uid )
    {
        $log = self::getLogUser( $uid );
        $logs = $log->getLogs();
        
        return $logs;
    }
    
    /**
     * 返回一个用户的全部日志
     */
    public static function getUnreadNum( $uid )
    {
        $log = self::getLogUser( $uid );
        
        return $log->getUnreadNum();
    }
    
    /**
     * 删除用户全部数据
     * @param int $uId
     */
    public static function remove( $uId )
    {
    	$log = self::getLogUser( $uId );
    	$log->removeAll();
    }
}