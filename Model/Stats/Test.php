<?php
/**
 * 测试工具
 * @name Test.php
 * @author admin
 * @since 2013-1-14
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Stats_Test
{

	//竞技场测试工具
	public static function attackTest( $cCard , $bCard , $times , $isFirst )
	{
		$testMsg = "";
		if(!empty($cCard) && !empty($bCard) && !empty($times))
		{
			$returnData = array();
			$challengerWin = 0;
			$uid1 = 157468483;
			$uid2 = 183259877;
			$cCardList = array();
			$bCardList = array();
			foreach($cCard as $card)
			{
				if(!empty($card['val']))
				{
					$cCardList[] = array(
						'cardId' => $card['val'],
						'level' => empty($card['lv']) ? 1 : $card['lv'],
						'skillLevel' => 1,
					);
				}
			}
			foreach($bCard as $card)
			{
				if(!empty($card['val']))
				{
					$bCardList[] = array(
						'cardId' => $card['val'],
						'level' => empty($card['lv']) ? 1 : $card['lv'],
						'skillLevel' => 1,
					);
				}
			}
			if(!empty($cCardList) && !empty($bCardList))
			{
				//1水,2火,3木,4光,5暗
				$elementType = array("none", "水", "火", "木", "光", "暗");
				$battleStr = "";
				$disBattle = $times < 2;
				
				for( $i = 0 ; $i < $times ; $i++)
				{
					$battleInfo = Arena_Normal::getInstance( $uid1 )->attackTest( "user" , intval($uid2) , $cCardList , $bCardList , $isFirst );
					//print_r(json_encode($battleInfo));die;
					if($battleInfo['challengerWin'] == 1) $challengerWin++;
					$challengerCard = &$battleInfo['challenger']['cards'];
					$beChallengerCard = &$battleInfo['beChallenger']['cards'];
					$battleRound = 0;
					foreach($battleInfo['roundRecord'] as &$roundRecord)
					{
						if(empty($roundRecord)) continue;
						foreach($roundRecord as &$record)
						{
							if(empty($record['attackerUid'])) continue;
							if($record['attackerUid'] == $battleInfo['challenger']['uid'])
							{
								$aName = "挑战方";
								$dName = "被挑战方";
							}
							else
							{
								$aName = "被挑战方";
								$dName = "挑战方";
							}
							if($disBattle) {
								$battleStr .= "~".(++$battleRound).":<br/>";
								$skillConfigs = Common::getConfig("upgradeSkill");
								if(!empty($record['buff']))
								{
									foreach($record['buff'] as $_id => $_info)
									{
										$skillConfig = $skillConfigs[$_id];
										$battleStr .= "~~~"."[".$skillConfig['id'].":".$skillConfig['skillName']."]"
										."对[".$aName."][".$record['aid']."](".$_info["hp"].")"
										."造成[".$_info['dmg']."]点伤害<br/>";
									}	
								}
								if(!empty($record['skillId']))
								{
									$skillConfig = $skillConfigs[$record['skillId']];
									$battleStr .= "~~[".$aName."][".$record['aid']."](".$record["aHp"].")"
									."施放了[".$skillConfig['id'].":".$skillConfig['skillName']."]<br/>";
								}
								if(!empty($record['def']))
								{
									foreach($record['def'] as $_id => $_info)
									{
										if(isset($_info["hp"]))
										{
											$battleStr .= "~~~"
											."对[".$dName."][".$_id."](".$_info["hp"].")造成[".$_info['dmg']."]点伤害<br/>";
										}
									}
								}
								if(!empty($record['atk']))
								{
									foreach($record['atk'] as $_id => $_info)
									{
										if(isset($_info["hp"]))
										{
											$battleStr .= "~~~"
											."对[".$aName."][".$_id."](".$_info["hp"].")造成[".$_info['heal']."]点治疗<br/>";
										}
									}
								}
								if(!empty($record['did']))
								{
									$battleStr .= "~~[".$aName."][".$record['aid']."](".$record["aHp"].")"
									."对[".$dName."][".$record['did']."](".$record["bHp"].")造成[".$record['damage']."]点伤害<br/>";	
								}
								if(!empty($record['dmgBack']))
								{
									$battleStr .= "~~[".$aName."][".$record['aid']."](".$record["aHp"].")"
									."受到[".$record['dmgBack']."]点反弹伤害<br/>";	
								}
							}
						}
					}
					foreach($challengerCard as $card)
					{
						if($card['hp'] > 0)
						{
							$returnData['cCard'][$card['id']]++;
						}
					}
					foreach($beChallengerCard as $card)
					{
						if($card['hp'] > 0)
						{
							$returnData['bCard'][$card['id']]++;
						}
					}
				}
				$returnData['challengerWin'] = $challengerWin;
				$cardInfoStr = $cCardStr = $bCardStr = "";
				if($disBattle) $cardInfoStr .= "[挑战方武将信息]<br/>";
				foreach($challengerCard as $card)
				{
					$cnt = !empty($returnData['cCard'][$card['id']]) ? $returnData['cCard'][$card['id']] : 0; 
					$cCardStr .= "<".$card['id'].":".$card['cardId'].">".$cnt." , ";
					if($disBattle) $cardInfoStr .= "<".$card['id'].":".$card['cardId'].":".$elementType[$card['ctype']].">hp:".$card['maxHp'].",att:".$card['attack'].",def:".$card['defense'].",crit:".$card['crit'].",dodge:".$card['dodge']."<br/>";
				}
				if($disBattle) $cardInfoStr .= "[被挑战方武将信息]<br/>";
				foreach($beChallengerCard as $card)
				{
					$cnt = !empty($returnData['bCard'][$card['id']]) ? $returnData['bCard'][$card['id']] : 0; 
					$bCardStr .= "<".$card['id'].":".$card['cardId'].">".$cnt." , ";
					if($disBattle) $cardInfoStr .= "<".$card['id'].":".$card['cardId'].":".$elementType[$card['ctype']].">hp:".$card['maxHp'].",att:".$card['attack'].",def:".$card['defense'].",crit:".$card['crit'].",dodge:".$card['dodge']."<br/>";
				}
				$testMsg = (
					"挑战方胜利次数:".$returnData['challengerWin']
					."<br/> 挑战方武将存活：".$cCardStr
					."<br/>被挑战方武将存活：".$bCardStr
					."<br/>"
				);
				if($disBattle)
				{
					$testMsg .= ($cardInfoStr);
					$testMsg .= ($battleStr);
				}
			}
			else
			{
				$testMsg = ("empty card");	
			}
		}
		else
		{
			$testMsg = ("error params");
		}
		return $testMsg;
	}
	
	
	//台湾元宝获得统计
	public static function statsIncomeCoin()
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
		
		$startDate = strtotime( "2013-07-03" );
		$endDate = strtotime( "2013-07-21" );
		 
		for( $time = $startDate ; $time <= $endDate ; $time += 86400 )
		{
			$keys = array( "uid" => 1 );
			$initial = array( "count" => 0 , "firstPass" => 0 , "intime" => 0 , "totalLogin" => 0 , "consecutive" => 0 );
			$reduce = '
function(obj, prev) {
   prev.count++;
   var coin = parseInt(obj.coin);
   if(obj.gid == "firstPass"){
      prev.firstPass += coin;
   }else if(obj.gid == "activity"){
      if(obj.subType == 1){
         prev.consecutive += coin;
      }else if(obj.subType == 2){
         prev.totalLogin += coin;
      }else if(obj.subType == 200){
         prev.intime += coin;
      }
   }
}';
			$condition = array('condition' => array(
					'addTime' => array(
							'$gte' => $time,
							'$lt' => $time + 86400,
					),
					//'platform' => Common::getConfig( "platform" ),
					//'$or' => array( array( "gid" => "firstPass" ) , array( "gid" => "activity" ) ),
			));
			 
			$result = Stats_Model::group( "incomeCoin" , $keys , $initial , $reduce , $condition );
			$statData = array();
			foreach( $result as $k => $v )
			{
				//firstPass
				//台湾活动 intime
				//consecutive 原厂连续登陆
				//totalLogin 原厂累计登陆
				foreach( $v as $type => $vv )
				{
					if( $type != "count"  && $type != "uid" && $vv > 0 )
					{
						$statData[$type]['count']++;
						$statData[$type]['sum'] += $vv;
					}
				}
				 
			}
			 
			$totalStatData[$time] = $statData;
		}
		
		$str =  "时间"
				."\t"
				."首次通關/獲得人次"
				."\t"
				."累計登入/獲得人次"
				."\t"
				."活躍登入/獲得人次"
				."\t"
				."台灣運營活動贈送/獲得人次"
				."\n";
			
		foreach( $totalStatData as $time => $statData )
		{
			$str .=  date( 'Y-m-d' , $time )
					."\t"
					.$statData['firstPass']['sum']."/".$statData['firstPass']['count']
					."\t"
					.$statData['totalLogin']['sum']."/".$statData['totalLogin']['count']
					."\t"
					.$statData['consecutive']['sum']."/".$statData['consecutive']['count']
					."\t"
					.$statData['intime']['sum']."/".$statData['intime']['count']
					."\n";
		}
			
		self::exportExcel( "incomeCoinLog" , $str );
		 
		exit;
	}
	
	public static function statRevive()
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
		 
		$startDate = strtotime( "2013-11-15 12:00:00" );
		$endDate = strtotime( "2013-11-21 12:00:00" );
		
		$keys = array( "uid" => 1 );
		$initial = array( "count" => 0 );
		$reduce = '
function(obj, prev) {
   prev.count++;
}';
		$condition = array('condition' => array(
				'buyTime' => array(
						'$gte' => $startDate,
						'$lt' => $endDate,
				),
				'gid' => 'revive',
				'platform' => Common::getConfig( "platform" ),
		));
		 
		$result = Stats_Model::group( "buyHistory" , $keys , $initial , $reduce , $condition );
		$statData = array();
		foreach( $result as $k => &$v )
		{
			$statData[$v['count']][] = $v['uid'];
		}
		ksort($statData);
		$str =  "次数"
				."\t"
				."id"
						."\n";
		
		foreach( $statData as $cnt => $data )
		{
			$str .=  $cnt
			."\t"
					.json_encode($data)
					."\n";
		}
		
		self::exportExcel( "reviveLog" , $str );
	}
	
	//个人元宝获得统计
	public static function statsIncomeCoinUser()
	{
		$userId = 270195658;
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
	
		$startDate = strtotime( "2013-11-08" );
		$endDate = strtotime( "2013-11-23" );
		
		$evtActType = array();
		
		for( $time = $startDate ; $time <= $endDate ; $time += 86400 )
		{
			$keys = array( "evtAct" => 1 );
			$initial = array( "count" => 0 , "coin" => 0 );
			$reduce = '
function(obj, prev) {
   prev.count++;
   prev.coin += parseInt(obj.coin);
}';
			$condition = array('condition' => array(
				'serverTime' => array(
					'$gte' => $time,
					'$lt' => $time + 86400,
				),
				'uid' => $userId,
				//'platform' => Common::getConfig( "platform" ),
			));
	
			//$result = Stats_Model::group( "incomeCoin" , $keys , $initial , $reduce , $condition );
			
			$config = Common::getConfig( 'mongoDb' );
			$mongoServerStr  =  $config['statsDB']['host'];
			$connect = true;
			$mongoDB = new Mongo(  $mongoServerStr , array( 'connect' => $connect ) );
			$dbname =  'Data_App_1003';
			$collection = 's19_gainCoin';
			if( $condition )
			{
				$rs = $mongoDB->$dbname->$collection->group($keys, $initial, $reduce , $condition );
			}
			else
			{
				$rs = $mongoDB->$dbname->$collection->group($keys, $initial, $reduce );
			}
			$result = &$rs['retval'];
			
			$statData = array();
			foreach( $result as $k => $v )
			{
				$evtActType[$v['evtAct']] = 1;
				$statData[$v['evtAct']] = array(
					'count' => $v['count'],
					'coin' => $v['coin'],
				);
			}
	
			$totalStatData[$time] = $statData;
		}
	
		$str =  "时间";
		
		foreach($evtActType as $evtAct => $v)
		{
			$str .= "\t".$evtAct;
		}
		
		$str .= "\n";
			
		foreach( $totalStatData as $time => $statData )
		{
			$str .= date( 'Y-m-d' , $time );
			foreach($evtActType as $evtAct => $v)
			{
				$str .= "\t".intval($statData[$evtAct]['coin']);
			}
			
			$str .= "\n";
		}
			
		self::exportExcel( "statsIncomeCoinUser_".$userId, $str );
			
		exit;
	}
	
	//竞技场积分补偿
	public static function sendArenaBonus()
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
		 
		$dbEngine = Common::getDB( 1 );
		$sql = "SELECT uid FROM arena_normal WHERE normalDate>=UNIX_TIMESTAMP('2013-07-15')";
		$users = $dbEngine->findQuery($sql);
		 
		$sql = "update arena_normal set normalScore=normalScore+240 WHERE normalDate>=UNIX_TIMESTAMP('2013-07-15')";
		$dbEngine->query( array( $sql ) );
		 
		$cache = Common::getCache();
		$idx = 0;
		foreach( $users as $user )
		{
			//清除竞技场信息
			$cache->delete( $user[0]."_arena_normal");
			//清除敌人信息
			$cache->delete("arena_normal_enemy_history_".$user[0]);
			$idx++;
		}
		//清除竞技场排行
		$cache->delete( "arenaNormalRank" );
		print_r($idx);
		exit;
	}
	
	//元宝补偿
	public static function sendCoinBonus($coin)
	{ 
		//元宝补偿
		if(!defined('IS_CLI'))
		{
			ini_set( 'memory_limit' , '512M' );
			ini_set( "max_execution_time", "300" );
		}
		 
		$dbEngine = Common::getDB( 1 );
		$sql = "select uid from user_info";
		$users = $dbEngine->findQuery($sql);
		 
		//$sql = "update user_info set coin=coin+".intval($coin)." ";
		//$dbEngine->query( array( $sql ) );
		 
		//$cache = Common::getCache();
		$idx = 0;
		$cnt = 0;
		foreach( $users as $user )
		{
			//$cache->delete( $user[0]."_user_info");
			User_Info::getInstance($user[0])->changeCoin($coin, '元宝补偿');			 
			$idx++;
			//30条sql提交一次
			if(++$cnt >= 30)
			{
				ObjectStorage::save();
				$cnt = 0;
			}
		}
		
		if($cnt > 0) ObjectStorage::save();
		
		print_r($idx);
		exit;
	}
	
	//用户登陆奖励补偿
	public static function sendUserLoginBonus()
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
		 
		$keys = array( "uid" => 1 );
		$initial = array( "count"=>0 );
		$reduce = "function (obj, prev) { prev.count++; }";
		$condition = array('condition' => array(
				'loginTime' => array(
						'$gte' => strtotime('2013-12-13'),
						'$lt' => strtotime('2013-12-20'),
				),
				'platform' => Common::getConfig( "platform" ),
		));
			
		$result = Stats_Model::group( "userLoginLog" , $keys , $initial , $reduce , $condition );
		$idx=0;
		foreach( $result as $login )
		{
			$idx++;
			Data_User_Info::getInstance( $login['uid'] , true )->changeCoin( 6  );
		}
		print_r($idx);
		ObjectStorage::save();
		exit;
	}
	
	//清除指定时间内数据
	public static function delUserByTime()
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
		
		$time = strtotime( '2013-04-25' );
		
		$dbEngine = Common::getDB( 1 );
		$sql = "select uid from user_profile where registerTime<".$time."";
		$users = $dbEngine->findQuery($sql);
		
		$userIds = "";
        	foreach( $users as $user )
		{
			if( $userIds != "" ) $userIds .= ",";
			$userIds .= $user[0];
		}
		
		if( !empty( $userIds ) )
		{
			$tableConfig = array(
				array("table"=>"activity","cache"=>"activity_model"),
				array("table"=>"activity_consume","cache"=>"activity_consume"),
				array("table"=>"arena_normal","cache"=>"arena_normal"),
				array("table"=>"battle_normal","cache"=>"battle_normal"),
				array("table"=>"battle_special","cache"=>"battle_special"),
				array("table"=>"battle_weekly","cache"=>"battle_weekly"),
				array("table"=>"card","cache"=>"card_model"),
				array("table"=>"card_team","cache"=>"card_team"),
				array("table"=>"card_unlock","cache"=>"card_unlock"),
				array("table"=>"code_invite_outline","cache"=>"code_invite_outline"),
				array("table"=>"friend","cache"=>"friend_model"),
				array("table"=>"friend_request","cache"=>"friend_request"),
				array("table"=>"invite_code","cache"=>"invite_code"),
				array("table"=>"mail","cache"=>"mail_model"),
				array("table"=>"order","cache"=>"order_model"),
				array("table"=>"user_help","cache"=>"user_help"),
				array("table"=>"user_info","cache"=>"user_info"),
				array("table"=>"user_profile","cache"=>"user_profile"),
			);
			
			$cache = Common::getCache();
			foreach( $tableConfig as $conf )
			{
				$sql = "delete from ".$conf['table']." where uid in (".$userIds.")";
				$dbEngine->query( array( $sql ) );
				
				foreach( $users as $user )
				{
				$cache->delete( $user[0]."_".$conf['cache']);
				}	
			}
		}
		print_r(count($users));
        exit;
	}
	
	//等级排行奖励
	public static function sendRankBonus()
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
		$rankList = Rank_Model::getInstance( 1 )->getRank( 'level' , 200 );
		foreach( $rankList as $info )
		{
			$cardId = "";
			switch ( intval( ( $info['rank'] -1 ) / 50 ) ) {
				case 0:
				$cardId = "149_card";
				break;
				case 1:
				$cardId = "144_card";
				break;
				case 2:
				$cardId = "146_card";
				break;
				case 3:
				$cardId = "23_card";
				break;
			}
			if( $cardId != "" )
			{
				print_r( $info['uid']."==".$info['rank']."==".$cardId );
				print_r("<br/>");
				Card_Model::getInstance( $info['uid'] )->addCard( $cardId , 1 , true  , "rankBonus" );
			}
		}
		ObjectStorage::save();
		exit;
	}
	
	//登录次数, 充值人数
	public static function statsLoginPay()
	{
		//登录次数, 消费人数
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
		 
		$startDate = strtotime( "2013-07-01" );
		$endDate = strtotime( "2013-09-01" );
		
		for( $time = $startDate ; $time <= $endDate ; $time += 86400 )
		{
			//登录次数
// 			$keys = array( "type" => 1 );
// 			$initial = array( "count" => 0  );
// 			$reduce = '
// function(obj, prev) {
//    prev.count++;
// }';
			$condition = array('condition' => array(
					'addTime' => array(
							'$gte' => $time,
							'$lt' => $time + 86400,
					),
					'platform' => 'wk',
					'type' => 'Scene.init'
			));
			 
// 			$result = Stats_Model::group( "userActionLog" , $keys , $initial , $reduce , $condition );
// 			$statData = array();
// 			foreach( $result as $k => $v )
// 			{
// 				$statData[$v['type']]['count'] = $v['count'];
// 			}
			$result = Stats_Model::count("userActionLog", $condition['condition']);
			$statData['Scene.init']['count'] = $result;
			
			//充值人数
			$dbEngine = Common::getDB( 1 );
			$sql = "SELECT count(distinct uid) cnt FROM `order` WHERE status=1 and addTime>="
					.$time." and addTime<".($time + 86400);
			$users = $dbEngine->findQuery($sql);
			$statData['pay']['count'] = $users[0][0];
			$totalStatData[$time] = $statData;
		}
		$str =  "时间"
				."\t"
				."登录次数"
				."\t"
				."充值人数"
				."\n";
		
		foreach( $totalStatData as $time => $statData )
		{
			$str .=  date( 'Y-m-d' , $time )
			."\t"
			.intval($statData['Scene.init']['count'])
			."\t"
			.intval($statData['pay']['count'])
			."\n";
		}
		
		self::exportExcel( "loginAndPayLog" , $str );
	}
	
	//统计过关人数
	public static function statsStagePass()
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
		
		$normalConfig = Common::getConfig( "normal" );
		
		
		$dbEngine = Common::getDB( 1 );
		
		$str =  "关卡编号"
				."\t"
				."名称"
				."\t"
				."通关人数"
				."\n";
				
		foreach( $normalConfig as $floorId => $floor )
		{
			foreach( $floor['rooms'] as $roomId => $room )
			{
				
				$sql = "select count(1) from battle_normal where "
				." floor*100+room*10+pass >= ".$floorId.$roomId."1";
				
			
				$cnt = $dbEngine->findQuery($sql);
				$str .= $floorId.".".$roomId
						."\t"
						.$floor['name']."-".$room['name']
						."\t"
						.$cnt[0][0]
						."\n";
			}
		}
		self::exportExcel( "statsStagePass" , $str );

        exit;
	}
	
	public static function statsRefreshNormalTimes()
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
		
		$startDate = strtotime( "2013-09-13 10:00:00" );
		$endDate = strtotime( "2013-09-23 10:00:00" );
			
			$keys = array( "uid" => 1 );
			$initial = array( "count" => 0 );
			$reduce = '
function(obj, prev) {
   prev.count++;
}';
			$condition = array('condition' => array(
					'buyTime' => array(
							'$gte' => $startDate,
							'$lt' => $endDate,
					),
					'platform' => Common::getConfig( "platform" ),
					'gid' => 'arenaRefreshTimes',
			));
		
			$result = Stats_Model::group( "buyHistory" , $keys , $initial , $reduce , $condition );

		$str =  "uid"
				."\t"
				."昵称"
				."\t"
				."刷新次数"
				."\n";
			
		foreach( $result as $statData )
		{
			$info = MakeCommand_User::userInfo($statData['uid']);
			$str .=  $statData['uid']
			."\t"
			.$info['username']
			."\t"
			.$statData['count']
			."\n";
		}
			
		self::exportExcel( "incomeCoinLog" , $str );
			
		exit;
	}
	
	//统计金币分布
	public static function statsUserGold()
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
	
		$dbEngine = Common::getDB( 1 );
	
		$str =  "等级分布"
				."\t"
				."玩家数量"
				."\t"
				."金币总值"
				."\t"
				."金币平均值"
				."\n";
	
		$start  = 11;
		$end = 240;
		$step = 10;
		
		for($i = $start;$i < $end;$i += $step)
		{
	
				$sql = "select count(1) cnt,sum(`gold`) sgold,avg(`gold`) agold from `user_info` where "
						." `level`>=".$i." and `level`<".($i + $step);
	
				$cnt = $dbEngine->findQuery($sql);
				$str .= $i."-".($i + $step - 1)
				."\t"
				.floatval($cnt[0][0])
				."\t"
				.floatval($cnt[0][1])
				."\t"
				.floatval($cnt[0][2])
				."\n";
		}
		self::exportExcel( "statsUserGold" , $str );
	
		exit;
	}
	
	public static function arenaRank()
	{
		$arenaRank = Data_Arena_Rank::getInstance( 1 )->getRankResult( 1 , 1379260800 );
        $str = "排名\tuid\t昵称\n";
		foreach( $arenaRank as $rank => $uid )
		{
	        if( $uid['uid'] > 0 )
	        {
                $info = MakeCommand_User::userInfo($uid['uid']);
	        }
	        else
	        {
                $info = $uid;   
	        } 
	        $str .= ( $rank + 1 )."\t".$uid['uid']."\t".$info['username']."\n";
		}
		Stats_Test::exportExcel("arenaRank", $str);
		exit;
	}
	
	public static function arenaGetBonusUsers()
	{
		$weekStart = Helper_Date::getWeekStartTime() + 11 * 3600;
        $rank = Data_Arena_Rank::getInstance(1)->getRankResult(1,1376841600);
        $getBonusUsers = array();
        foreach($rank as $user)
        {
        	$arena_info = Data_Arena_Normal::getNormalInfo($user['uid']);
        	if(!empty($arena_info['normalBonus']))
        	{
        		$allBonus = json_decode($arena_info['normalBonus'],true);
        		foreach($allBonus as $bonus)
        		{
        			if($bonus['t'] >= $weekStart)
        			{
        				$user += $bonus;
        				$getBonusUsers[] = $user;
        				break;
        			}
        		}
        	}
        }
        print_r(json_encode($getBonusUsers));
        exit;
	}
	
	public static function exportExcel( $fileName , $str )
	{
		header("Content-Disposition:attachment;filename=".$fileName.
			"_".Common::getConfig('platform')."_".date('Ymd_His').".csv");
			
		//header("Content-type:text/csv;charset=UTF-8");
			
		header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
		header('Expires:0');
		header('Pragma:public');
			
		//$str =  iconv('UTF-8','GBK',$str );
		if(function_exists('mb_convert_encoding')){
			header('Content-type: text/csv; charset=UTF-16LE');
			//输出BOM
			echo(chr(255).chr(254));
			echo(mb_convert_encoding($str,"UTF-16LE","UTF-8"));
			exit;
		}
	}
	
	//设置用户通关
	public static function setUserNewbieMax()
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
			
		$dbEngine = Common::getDB( 1 );
		$sql = "SELECT uid FROM user_info where newbieStep>=7 and newbieStep<>99";
		$users = $dbEngine->findQuery($sql);
			
		$sql = "update user_info set newbieStep=99 where newbieStep>=7 and newbieStep<>99";
		$dbEngine->query( array( $sql ) );
			
		$cache = Common::getCache();
		$idx = 0;
		foreach( $users as $user )
		{
			//清除竞技场信息
			$cache->delete( $user[0]."_user_info");
			$idx++;
		}
		print_r($idx);
		exit;
	}
	
	//删除指定时间之前的账号
	public static function delUsersByTime($time)
	{
		ini_set( 'memory_limit' , '512M' );
		ini_set( "max_execution_time", "300" );
			
		$dbEngine = Common::getDB( 1 );
		$sql = "select uid from user_profile where registerTime<".strtotime($time);
		$users = $dbEngine->findQuery($sql);
		
		$cache = Common::getCache();
		$idx = 0;
		foreach( $users as $user )
		{
			User_Model::deleteUser($user[0]);
			$idx++;
		}
		print_r($idx);
		exit;
	}
}