<?php
/**
 * 支付统计
 * 
 * @name Pay.php
 * @author admin
 * @since 2013-07-05
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Stats_Pay
{
/**
	 * 根据付费时间，查询
	 * @param unknown_type $startDay
	 * @param unknown_type $endDay
	 */
	public static function _findPayByPayTimeAndRefer( $startDay , $endDay , $refer )
	{
		
		//Stats_Model::Index( "payUser" , array( 'payTime' => 1 ,  'times' => 1  ) );
		//Stats_Model::Index( "payUser" , array( 'refer' => 1  ) );
		//Stats_Model::Index( "payUser" , array( 'payTime' => 1  ) );
		
		$startStr = $startDay." 00:00:00";
		$endStr = $endDay." 23:59:59";
		
		$queryCond = array(
				'platform' => Common::getConfig( "platform" ),
				'payTime' => array(
						'$gte' => strtotime( $startStr ),
						'$lte' => strtotime( $endStr ),
				)
				
		);
	
		if( $refer != 'all' )
		{
			$queryCond['refer'] = $refer;
		}
		
		$stats = Stats_Model::count( "paymentOrder" , $queryCond );
		return $stats;
		
	}
	
	public static function getChannelStatsPay( $statsName , $serverTime )
	{
		$monthMaxDay = date( "t" , $serverTime );
		$searchYm = date( "Ym" , $serverTime );
		$platform = Common::getConfig( "platform");
		
		$cache = Common::getCache();
		if( !$statsData = $cache->get( $statsName."_".$searchYm ) )
		{
			$statsData = array();
			for( $i = 1 ; $i <= $monthMaxDay ; $i++ )
			{
				if( strtotime( date( "Y-m-d" , $_SERVER['REQUEST_TIME'] ) ) <  strtotime( date( "Y-m-" , $serverTime ).$i ) )
				{
					break;
				}
				$startStr = date( "Y-m-1" ,  $serverTime );
				$startTime = strtotime(  $startStr  ) + ( $i - 1 ) * 86400;
				
				$endTime = $startTime + 86400;
				
				$keys = array( "type" => 1 );
				$initial = array( "sum"=>0 );
				$reduce = "function (obj, prev) { prev.sum += parseInt(obj.rmb); }";
				$condition = array('condition' => array(
					'payTime' => array(
						'$gte' => $startTime,
						'$lt' => $endTime,
					),
					'platform' => $platform
				));
				
				$statsData[$startTime] = Stats_Model::group( "paymentOrder" , $keys , $initial , $reduce , $condition );
			}
	
			//非当前月份留存档
			if( !empty( $statsData ) && intval( date("Ym" , $_SERVER['REQUEST_TIME'] ) ) > intval( date( "Ym" , $serverTime ) ) )
			{
				$cache->set( $statsName."_".$searchYm , $statsData );
			}
			//$cache->delete( $statsName."_".$searchYm );
		}
		//print_r($statsData);die;
		return $statsData;
	}
	
	/**
	 * 新版渠道统计
	 * @param unknown $serverTime
	 * @return multitype:Ambigous <void, unknown>
	 */
	public static function getChannelPay( $serverTime )
	{
		$platform = Common::getConfig( "platform");
		$statsData = array();
		
		$startTime = strtotime( date( "Y-m-d" , $serverTime ) );
		$endTime = $startTime + 86400;
		$statDate = date("Ymd" , $startTime );
		
		$keys = array( "type" => 1 );
		$initial = array( "sum" => 0 );
		$reduce = "function (obj, prev) { prev.sum += parseInt(obj.rmb); }";
		$condition = array('condition' => array(
				'payTime' => array(
						'$gte' => $startTime,
						'$lt' => $endTime,
				),
				'platform' => $platform
		));
		$statsData[$statDate] = Stats_Model::group( "paymentOrder" , $keys , $initial , $reduce , $condition );
		return $statsData;
	}
	
	
	public static function getStatsPay()
	{
		//注册用户数
		//小于("$lt")、小于等于("$lte")、大于("$gt")、大于等于("$gte")、不等于("$ne")
		//查询时间大于开始时间，小于结束时间
		$days = date( "t" , $_SERVER['REQUEST_TIME']);
		$monthStats = array();
		//活跃类型:日活跃，周活跃，月活跃
		$statsTypes = array(  "firstDay" , "firstWeek" , "firstMonth" );
		$referType = array(  "all" , /*"91"*/ );
		
		
		$cache = Common::getCache();
		/**
		 * $monthStats = array(
		 * 	 	01 => array(
		 * 		'activeDay' => 10034,
		 * 		'activeWeek' => 133300,
		 * 		'activeMonth' => 1231234,
		 * 		'relogin1' => 1333,
		 * 	),
		 * 	...
		 * );
		 *
		 */
		$monthStats = $cache->get( "monthPayStats".date("Ym") );
		//每天循环查10次，实际上，每天只执行当天查询
		for ( $i = 1;$i <= $days; $i++ )
		{
			$i = $i<10 ? '0'.$i : $i;
			if( $i > date("d"))
			{
				break;
			}
	
			//如果做了定时执行任务
			if( $monthStats[$i] )
			{
				//continue;
			}
	
			if( $i != date("d"))
			{
				continue;
			}

			
			
			/**
			 * 日新增付费用户
			 * @var unknown_type
			 */
			$today = date( "Y-m-{$i}" , $_SERVER['REQUEST_TIME']);
			foreach ( $referType as $refer )
			{
				$monthStats[$i][$refer]['firstDay'] = Stats_Pay::_findPayByPayTimeAndRefer( $today , $today , $refer );
			}
		
			/**
			 * 周新增付费用户
			 * @var unknown_type
			 */
			$weekDay = date( "w" , strtotime( $today ) );
			$weekDay = $weekDay == 0 ? 7 : $weekDay;
			$weekDayPres = $weekDay-1;
			$weekDayNexs = 7-$weekDay;
			$weekStart = strtotime( "-{$weekDayPres} day" , strtotime( $today ) );
			$weekEnd = strtotime( "+{$weekDayNexs} day" , strtotime( $today ) );
				
			$weekStart = date( "Y-m-d" , $weekStart );
			$weekEnd = date( "Y-m-d" , $weekEnd );
			
			foreach ( $referType as $refer )
			{
				$monthStats[$i][$refer]['firstWeek'] = Stats_Pay::_findPayByPayTimeAndRefer( $weekStart , $weekEnd , $refer );
			}
			
			/**
			 * 月新增付费用户
			 */
			$monthStart = date( "Y-m-")."01";
			$monthEnd = date( "Y-m-t");
			foreach ( $referType as $refer )
			{
				$monthStats[$i][$refer]['firstMonth'] = Stats_Pay::_findPayByPayTimeAndRefer( $monthStart , $monthEnd , $refer );
			}	
		}

		$cache->set( "monthPayStats".date("Ym") , $monthStats );
		return $monthStats;
	}
	
}