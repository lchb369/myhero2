<?php
/**
 * 数据统计显示
 * 
 * @name Login.php
 * @author admin
 * @since 2013-07-05
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Stats_Display
{
	/**
	 * 菜单栏
	 * @var unknown
	 */
	static $menu = array(
		'newUser' =>		array( 'titleText' => '日注册用户' , 'yAxisText' => '用户数量' , 'tpl' => 'newUser'  ),
		'loginUser' =>		array( 'titleText' => '日登录用户' , 'yAxisText' => '用户数量' , 'tpl' => 'newUser'  ),
		'userRefer' =>      array( 'titleText' => '新注册用户来源统计' , 'yAxisText' => '渠道来源用户数量' , 'tpl' => 'newUser'  ),
		'keepLogin2Days' => array( 'titleText' => '次日留存率'   , 'yAxisText' => '留存率(%)' , 'tpl' => 'newUser'  ),
		'keepLogin3Days' => array( 'titleText' => '3日留存率'    , 'yAxisText' => '留存率(%)' , 'tpl' => 'newUser'  ),
		'keepLogin4Days' => array( 'titleText' => '4日留存率'    , 'yAxisText' => '留存率(%)' , 'tpl' => 'newUser'  ),
		'keepLogin5Days' => array( 'titleText' => '5日留存率'    , 'yAxisText' => '留存率(%)' , 'tpl' => 'newUser'  ),
		'keepLogin6Days' => array( 'titleText' => '6日留存率'    , 'yAxisText' => '留存率(%)' , 'tpl' => 'newUser'  ),
		'keepLogin7Days' => array( 'titleText' => '7日留存率'    , 'yAxisText' => '留存率(%)' , 'tpl' => 'newUser'  ),
		'keepLogin8Days' => array( 'titleText' => '8日留存率'    , 'yAxisText' => '留存率(%)' , 'tpl' => 'newUser'  ),
		'keepLogin14Days' => array( 'titleText' => '14日留存率'    , 'yAxisText' => '留存率(%)' , 'tpl' => 'newUser'  ),
		'keepLogin30Days' => array( 'titleText' => '30日留存率'    , 'yAxisText' => '留存率(%)' , 'tpl' => 'newUser'  ),
		'channelPay' =>      array( 'titleText' => '渠道付费'  , 'yAxisText' => '付费金额' , 'tpl' => 'newUser'  ),
		'consumeCoinUsers' =>     array( 'titleText' => '元宝消耗人数统计'  , 'yAxisText' => '人数' , 'tpl' => 'newUser'  ),
	);
	
	//模块名
	static $action;
	
	//查询开始日期
	static $startDate;
	
	//查询结束日期
	static $endDate;
	
	//返回数据
	static $returnData = array();
	
	/**
	 * 显示统计数据
	 * @param unknown $action
	 */
	public static function doDisplay( $action , $startDate , $endDate )
	{
		
		//Stats_Query::run();
		//exit;
		
		if( !self::$menu[$action])
		{
			exit( "访问地址不存在!");
		}
		self::$action = $action;
		self::$startDate = $startDate ? $startDate : date( "Ymd" , strtotime( "-7 day" ) );
		self::$endDate = $endDate ? $endDate : date( "Ymd" );
		//查询统计数据
		self::_queryShowData();
		//格式化图表显示数据
		self::_formatData( $action );
		return self::$returnData;
	}
	
	
	/**
	 * 查询显示数据
	 */
	private static function _queryShowData()
	{
		//初始化查询数据
		$queryCond['pf'] = Common::getConfig( 'platform' );
		$queryCond['date'] = array(
				'$gte' => self::$startDate,
				'$lte' => self::$endDate,
		);
		
		$data = Stats_Model::find( "displayData" , $queryCond );
		
		$startDate = self::$startDate;
		while( $startDate <= self::$endDate )
		{
			foreach ( $data as $eachData )
			{
				if( $eachData['date'] == $startDate )
				{
					//留存率
					if( preg_match( "/keepLogin/", self::$action))
					{
						self::$returnData['data'][$startDate] = $eachData[self::$action] ? $eachData[self::$action] : 0;
						self::$returnData['data'][$startDate] = (float)sprintf( "%.2f" ,  self::$returnData['data'][$startDate] / $eachData['newUser'] * 100 );
						self::$returnData['data'][$startDate] = self::$returnData['data'][$startDate] > 100 ? 100.00 : self::$returnData['data'][$startDate];
					}
					else
					{
						self::$returnData['data'][$startDate] = $eachData[self::$action] ? $eachData[self::$action] : 0;
					}
				}
			}
			
			if( !self::$returnData['data'][$startDate]  )
			{
				self::$returnData['data'][$startDate] = 0;
			}
			
			//日期加1天
			$startDate = date( "Ymd" , strtotime( $startDate ) + 86400 );	
		}
	}
	
	
	/**
	 * 格式化图表显示数据
	 */
	private static function _formatData()
	{
		
		self::$returnData['titleText'] = self::$menu[self::$action]['titleText'];
		self::$returnData['yAxisText'] = self::$menu[self::$action]['yAxisText'];
		self::$returnData['xAxis']['categories'] = array_keys( self::$returnData['data'] );
		
		if( self::$action == 'userRefer' || self::$action == 'channelPay' )
		{
			self::_formatMutiData();
		}
		else 
		{
			self::$returnData['series'][0]['name'] = self::$menu[self::$action]['yAxisText'];
			self::$returnData['series'][0]['data'] = array_values( self::$returnData['data'] );
		}
		
	} 
	
	/**
	 * 格式化用户来源数据,渠道付费
	 */
	private static function _formatMutiData()
	{
		$types = array( 'total' );
		
		foreach ( self::$returnData['data'] as $date => $info )
		{
			if( $info )
			{
				foreach ( $info as $in )
				{
					if( !in_array( $in['type'] , $types ))
					{
						$types[] = $in['type'];
					}
				}
			}
		}
		
		$series = array();
		foreach ( $types as $index => $type )
		{
			$referName = MakeCommand_Config::getReferName( $type );
			$series[$index]['name'] = $referName;

			foreach ( self::$returnData['data'] as $date => $info )
			{
				if( $info )
				{
					$total = 0;
					$flag = 0;
					foreach ( $info as $in )
					{
						if( self::$action == 'channelPay' && Helper_Common::inPlatform( array( "wk" ) ) )
						{
							$in['count'] *= Order_Model::CURRENCY_CNY_2_TWD;
						}

						if( $in['type'] == $type && $type != 'total' )
						{
							$series[$index]['data'][] = $in['count'];
							$flag = 1;	
						}
						$total += $in['count'];
					}
					
					if( $flag == 0 &&  $type != 'total' )
					{
						$series[$index]['data'][] = 0;
					}
					
					if( $type == 'total' )
					{
						if( self::$action == 'channelPay' )
						{
							$series[$index]['name'] = "总金额";
						}
						else 
						{
							$series[$index]['name'] = "总用户数";
						}
							
						$series[$index]['data'][]  = $total;
					}	
				}
				else
				{			
					$series[$index]['data'][] = 0;
				}
			}
		}		
		self::$returnData['series'] = $series;
	}
	
	
}
