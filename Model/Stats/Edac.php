<?php
/**
 * EDAC数据中心API接入
 * @name Model.php
 * @author admin
 * @since 2013-1-14
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Stats_Edac
{
	
	static $appId = 1003;
	/**
	 * 1,初始化应用
	 */
	public static function init()
	{
		$cache = Common::getCache();
		if( $cache == false )
		{
			return;
		}
		$key = 'edac_init';
		if( $cache->get( $key ) != 1 )
		{
			global $appSid;
			
			$svrConf = array();
			$serverConfig = Common::getConfig( 'ServerConfig' );
			foreach($serverConfig as $conf)
			{
				if($conf['sid'] == $appSid)
				{
					$svrConf = $serverConfig[strtolower($conf['pf']).'_'.$appSid];
					break;
				}
			}
			
			$params = array(
				'method' => 'Stat.init',
				'appId' => self::$appId,
				'sid' => $appSid,
				'serverName' => $svrConf['statsName'],
				'coinName' => '元宝',
				'goldName' => '金币',
				'maxNewbieStep' => 6,
				'maxLevel' => 240 ,
			);
			self::_pushQueue( $params );
			$cache->set( $key , 1 );
		}
	}
	
	
	/**
	 * 2,注册用户
	 */
	public static function newUser( $uid  , $mac = 0 )
	{
		global $appSid;
		
		$userProfile = Data_User_Profile::getInstance( $uid )->getData();
		$userInfo = Data_User_Info::getInstance( $uid )->getData();
		
		$params = array(
			'method' => 'Stat.newUser',
			'appId' => self::$appId,
			'sid' => $appSid,
			'uid' => $uid,
			'nickName' => $userProfile['nickName'],
			'downRefer' => self::_getRefer( $uid ),
			'partner' => self::_getPartner($uid),
			'level' => 1,
			'newbie' => 0,
			'mac' => $mac ? $mac : $uid,
			'ip' => Helper_IP::getCurrentIP(),
			'registerTime' => $userProfile['registerTime'],
		);
		self::_pushQueue( $params );
	}
	
	/**
	 * 3,登录统计
	 * @param unknown $uid
	 */
	public static function userLogin( $uid )
	{
		global $appSid;
		$userProfile = Data_User_Profile::getInstance( $uid )->getData();
		$userInfo = Data_User_Info::getInstance( $uid )->getData();
		
		$params = array(
			'method' => 'Stat.userLogin',
			'appId' => self::$appId,
			'sid' => $appSid,
			'uid' => $uid,
			'nickName' => $userProfile['nickName'],
			'downRefer' => self::_getRefer( $uid ),
			'partner' => self::_getPartner($uid),
			'level' => $userInfo['level'],
			'mac' => $mac ? $mac : $uid,
			'ip' => Helper_IP::getCurrentIP(),
			'loginTime' => $_SERVER['REQUEST_TIME'],
			'registerTime' => $userProfile['registerTime'],
		);
		self::_pushQueue( $params );
	}
	
	/**
	 * 4,付费统计
	 * @param unknown $uid 用户ID
	 * @param unknown $orderId　订单ID
	 * @param unknown $rmb 人民币
	 * @param unknown $coin 元宝
	 * @param unknown $payType 支付类型
	 */
	public static function recharge( $uid , $orderId , $rmb , $coin , $payType )
	{
		global $appSid;
		$userProfile = Data_User_Profile::getInstance( $uid )->getData();
		$userInfo = Data_User_Info::getInstance( $uid )->getData();
		
		$params = array(
			'method' => 'Stat.recharge',
			'appId' => self::$appId,
			'uid' => $uid,
			'nickName' => $userProfile['nickName'],
			'sid' => $appSid,
			'downRefer' => self::_getRefer( $uid ),
			'partner' => self::_getPartner($uid),
			'orderId' => $orderId,
			'rmb' => $rmb,
			'coin' => $coin,
			'mac' => $mac ? $mac : $uid,
			'ip' => Helper_IP::getCurrentIP(),
			'payType' => $payType,
			'registerTime' => $userProfile['registerTime'],
			'serverTime' => $_SERVER['REQUEST_TIME'],
		);
		self::_pushQueue( $params );
	}
	
	/**
	 * 5,在线统计
	 * num 人数
	 * time 时间
	 */
	public static function userOnline( $num , $time  )
	{
		global $appSid;
		$params = array(
			'method' => 'Stat.userOnline',
			'appId' => self::$appId,
			'sid' => $appSid,
			'num' => $num,
			'serverTime' => $time,
		);
		self::_pushQueue( $params );		
	}
	
	
	/**
	 * 6,获取冲值币统计
	 * @param unknown $uid 用户id
	 * @param unknown $coin 获得元宝数
	 * @param unknown $totalCoin 总元宝数
	 * @param unknown $evtAct 事件名称
	 * @param unknown $evtObj 事件目标
	 * @param unknown $evtNum 事件发生次数/数量等
	 */
	public static function gainCoin( $uid , $coin , $evtAct , $evtObj = '' , $evtNum = 1 )
	{
		global $appSid;
		
		if( $evtObj == 'first' )
		{
			$userInfo['coin'] = $coin;
		}
		else
		{
			$userInfo = Data_User_Info::getInstance( $uid )->getData();
		}
		
		$params = array(
			'method' => 'Stat.gainCoin',
			'appId' => self::$appId,
			'sid' => $appSid,
			'uid' => $uid,
			'coin' => $coin,
			'totalCoin' => $userInfo['coin'] ? $userInfo['coin'] : 0,
			'evtAct' => $evtAct ? $evtAct : 'other' ,
			'evtObj' => $evtObj,
			'evtNum' => $evtNum,
			'serverTime' => $_SERVER['REQUEST_TIME'],
		);
		
		self::_pushQueue( $params );
	}
	
	
	/**
	 * 7,消耗冲值币统计
	 * @param unknown $uid 用户id
	 * @param unknown $coin 用掉元宝数
	 * @param unknown $totalCoin 还剩元宝数
	 * @param unknown $evtAct 事件名称
	 * @param unknown $evtObj 事件目标
	 * @param unknown $evtNum 事件发生次数/数量等
	 */
	public static function consumeCoin( $uid , $coin , $evtAct , $evtObj = '' , $evtNum = 1 )
	{
		if( abs( $coin ) == 0 )
		{
			return;
		}
		
		global $appSid;
		
		$userInfo = Data_User_Info::getInstance( $uid )->getData();
	
		$params = array(
			'method' => 'Stat.consumeCoin',
			'appId' => self::$appId,
			'sid' => $appSid,
			'uid' => $uid,
			'coin' => abs( $coin ),
			'totalCoin' => $userInfo['coin'] ? $userInfo['coin'] : 0,
			'evtAct' => $evtAct ? $evtAct : 'other' ,
			'evtObj' => $evtObj,
			'evtNum' => $evtNum,
			'serverTime' => $_SERVER['REQUEST_TIME'],
		);
	
		self::_pushQueue( $params );
	}
	
	
	/**
	 * 8.获取金币统计
	 * @param unknown $uid 用户id
	 * @param unknown $gold 获得金币数
	 * @param unknown $evtAct 事件名称
	 * @param unknown $evtObj 事件目标
	 * @param unknown $evtNum 事件发生次数/数量等
	 */
	public static function gainGold( $uid , $gold , $evtAct , $evtObj = '' , $evtNum = 1 )
	{
		global $appSid;
		if( $evtObj == 'first' )
		{
			$userInfo['gold'] = $gold;
		}
		else
		{
			$userInfo = Data_User_Info::getInstance( $uid )->getData();
		}
	
		$params = array(
			'method' => 'Stat.gainGold',
			'appId' => self::$appId,
			'sid' => $appSid,
			'uid' => $uid,
			'gold' => $gold,
			'totalGold' => $userInfo['gold'] ? $userInfo['gold'] : 0,
			'evtAct' => $evtAct ? $evtAct : 'other' ,
			'evtObj' => $evtObj,
			'evtNum' => $evtNum,
			'serverTime' => $_SERVER['REQUEST_TIME'],
		);
		self::_pushQueue( $params );
	}
	
	
	/**
	 * 9.消耗金币统计
	 * @param unknown $uid 用户id
	 * @param unknown $coin 用掉元宝数
	 * @param unknown $totalCoin 还剩元宝数
	 * @param unknown $evtAct 事件名称
	 * @param unknown $evtObj 事件目标
	 * @param unknown $evtNum 事件发生次数/数量等
	 */
	public static function consumeGold( $uid , $gold , $evtAct , $evtObj = '' , $evtNum = 1 )
	{
		$userInfo = Data_User_Info::getInstance( $uid )->getData();
		$params = array(
			'method' => 'Stat.consumeGold',
			'appId' => self::$appId,
			'sid' => self::_getSid(),
			'uid' => $uid,
			'gold' => abs( $gold ),
			'totalGold' => $userInfo['gold'] ? $userInfo['gold'] : 0,
			'evtAct' => $evtAct ? $evtAct : 'other' ,
			'evtObj' => $evtObj,
			'evtNum' => $evtNum,
			'serverTime' => $_SERVER['REQUEST_TIME'],
		);
		self::_pushQueue( $params );
	}
	
	
	/**
	 * 10,升级
	 * @param unknown $uid 用户ID
	 * @param unknown $level 等级
	 */
	public static function upLevel( $uid  , $level )
	{
		$params = array(
			'method' => 'Stat.upLevel',
			'appId' => self::$appId,
			'sid' => self::_getSid(),
			'uid' => $uid,
			'level' => $level,
		);
		self::_pushQueue( $params );
	}
	
	/**
	 *11, 新手引导
	 * @param unknown $uid 用户ID
	 * @param unknown $level 等级
	 */
	public static function upNewbie( $uid  , $step = 0 )
	{
		$params = array(
			'method' => 'Stat.upNewbie',
			'appId' => self::$appId,
			'sid' => self::_getSid(),
			'uid' => $uid,
			'newbie' => $step,
		);
		self::_pushQueue( $params );
	}
	
	
	/**
	 * 获取道具
	 */
	public static function gainItem( $uid , $itemId , $num = 1 , $evtDesc = '' )
	{
		$cardConfig = Common::getConfig( 'card' );
		//只记录3星以上的武将
		if(intval($cardConfig[$itemId]['star']) < 3) return false;
		
		$cardName = $cardConfig[$itemId]['star']."星".$cardConfig[$itemId]['name'];
		$userProfile = Data_User_Profile::getInstance( $uid )->getData();
		
		$params = array(
			'method' => 'Stat.gainItem',
			'appId' => self::$appId,
			'sid' => self::_getSid(),
			'uid' => $uid,
			'nickName' =>  strval( $userProfile['nickName'] ),
			'itemId' => strval( $itemId ),
			'itemName' => strval( $cardName ),
			'itemNum' => intval( $num ),
			'evtDesc' => strval( $evtDesc ),
			'serverTime' => $_SERVER['REQUEST_TIME'],
		);
		self::_pushQueue( $params );
	}
	
	/**
	 * 使用道具
	 */
	public static function lostItem( $uid , $itemId , $num = 1 , $evtDesc = '' )
	{
		$cardConfig = Common::getConfig( 'card' );
		//只记录3星以上的武将
		if(intval($cardConfig[$itemId]['star']) < 3) return false;
		
		$cardName = $cardConfig[$itemId]['star']."星".$cardConfig[$itemId]['name'];
		$userProfile = Data_User_Profile::getInstance( $uid )->getData();
		
		$params = array(
			'method' => 'Stat.lostItem',
			'appId' => self::$appId,
			'sid' => self::_getSid(),
			'uid' => $uid,
			'nickName' =>  strval( $userProfile['nickName'] ),
			'itemId' => strval( $itemId ),
			'itemName' => strval( $cardName ),
			'itemNum' => intval( $num ),
			'evtDesc' => strval( $evtDesc ),
			'serverTime' => $_SERVER['REQUEST_TIME'],
		);
		self::_pushQueue( $params );
	}
	

	/**
	 * 同步剩余冲值币总数 
	 */
	public static function haveCoin( $num )
	{
		if( $num <= 0 )
		{
			return;
		}
		$params = array(
			'method' => 'Stat.haveCoin',
			'appId' => self::$appId,
			'sid' => self::_getSid(),
			'num' => $num,
			//统计的是昨天的留存
			'serverTime' => $_SERVER['REQUEST_TIME'] - 86400,
		);
		self::_pushQueue( $params );
	}
	
	
	/**
	 * 同步剩余游戏币总数
	 */
	public static function haveGold( $num )
	{
		if( $num <= 0 )
		{
			return;
		}
		$params = array(
			'method' => 'Stat.haveGold',
			'appId' => self::$appId,
			'sid' => self::_getSid(),
			'num' => $num,
			//统计的是昨天的留存
			'serverTime' => $_SERVER['REQUEST_TIME'] - 86400,
		);
		self::_pushQueue( $params );
	}
	
	
	
	/**
	 * 玩法参与度分析
	 */
	public static function playMethod( $uid , $methodName )
	{
		if( empty( $methodName ) )
		{
			return;
		}
		$params = array(
			'method' => 'Stat.playMethod',
			'appId' => self::$appId,
			'sid' => self::_getSid(),
			'uid' => $uid,
			'methodName' => $methodName,
			'serverTime' => $_SERVER['REQUEST_TIME'],
		);
		self::_pushQueue( $params );
	}
	
	
	/**
	 * 自定义流水事件
	 * 1，竞技场流水
	 */
	public static function customAction( $uid , $act , $obj , $num = 1 , $ext = '' )
	{
		if( empty( $act ) )
		{
			return;
		}
		$params = array(
			'method' => 'Stat.customAction',
			'appId' => self::$appId,
			'sid' => self::_getSid(),
			'uid' => $uid,
			'act' => $act,
			'obj' => $obj,
			'num' => $num,
			'ext' => $ext,
			'serverTime' => $_SERVER['REQUEST_TIME'],
		);
		self::_pushQueue( $params );
	}
	
	
	
	
	/*********************************
	 * 
	 * 以下都是私有方法
	 * 
	 * *********************************
	 */
	/**
	 * 获取区服ID
	 * @return number
	 */
	private static function _getSid()
	{
		global $appSid;
		return $appSid;
	}
	
	/**
	 * 获取下载来源
	 * @param unknown $uid
	 * @return Ambigous <number, unknown>
	 */
	private static function _getRefer( $uid )
	{
		$userProfile = Data_User_Profile::getInstance( $uid )->getData();
		$refer = $userProfile['refer'] ? $userProfile['refer'] : $_REQUEST['refer'];
		$refer = $refer > 0 ? $refer : 0;
		if( $userProfile['refer'] == 0 && $userProfile['refer'] != $_REQUEST['refer'] )
		{
			Data_User_Profile::getInstance( $uid , true )->setDownRefer( $refer );
		}
		
		$chanalConf = Common::getConfig( 'ChanalConfig' );
		$chanalName = $chanalConf[$refer]['id'] ? $chanalConf[$refer]['id'] : $refer;
		return $chanalName;
	}
	
	private static function _getPartner($uid)
	{
		//第三方信息
		$thirdInfo = Data_Activity_Model::getInstance($uid)->getActivity(Activity_Model::ACTIVE_THIRD_INFO);
		
		$partner = strval($thirdInfo['data']['partner']);
		
		return $partner;
	}
	
	/**
	 * 消息入列
	 * @param unknown $params
	 */
	private static function _pushQueue( $params )
	{
		$status = CacheQueue::getInstance()->put( $params );
		if( $status == false )
		{
			$status = CacheQueue::getInstance()->put( $params );
			//$jsonParams = json_encode( $params );
			//file_put_contents( "/tmp/log1" , "--put failed:{$jsonParams}\n", FILE_APPEND );
		}
		
	}
	
}
