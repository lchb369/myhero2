<?php
/**
 * 
 * 每天订时执行，生成统计数据
 * @name Do.php
 * @author admin
 * @since 2013-07-05
 */

if( !defined( 'IN_INU' ) )
{
    return;
}

class Stats_Query
{
	/**
	 * 当日数据结果集
	 * @var unknown
	 */
	static $resultData;

	/**
	 * 执行统计的时间
	 * @var unknown
	 */
	static $todaySec;
	
	/**
	 * 开始运行统计的结果数据
	 * @param number $currTime 以当前时间运行，可以根据此时间，生成老数据
	 */
	public static function run( $currTime = 0 )
	{
		$serverTime = ( $currTime > 0 ) ? $currTime : $_SERVER['REQUEST_TIME'];
		self::$todaySec = $serverTime - 1*86400;
		
		//统计新注册用户
		self::_userStat();
		
		//用户来源统计
		self::_userRefer();
		
		//渠道付费统计
		self::_channelPay();
		
		//更新结果到结果表
		self::_updateToResultCollect();
	}
	
	/**
	 * 生成用户统计,包括了新注册，登录，留存率
	 */
	private static function _userStat()
	{
		$loginData = Stats_Active::getStatsUser( self::$todaySec );
		self::$resultData['userStats'] = $loginData;
	}
	
	/**
	 * 生成用户来源数据
	 */
	private static function _userRefer()
	{
		$statDay = date( 'Ymd' , self::$todaySec );
		$referData = Stats_Active::getUserRefer( self::$todaySec  );
		
		/*
		$referData[$statDay] = array(
			array(
				'refer' => 100,
				'count' => rand(1,100),
			),
			array(
				'refer' => 24,
				'count' => rand( 10 , 200),
			)			
		);
		*/
		
		
		foreach ( $referData[$statDay] as $key => $info )
		{
			$referData[$statDay][$key] = array( 'type' => $info['refer'] , 'count' => $info['count'] );
		}
		
		self::$resultData['userRefer'] = $referData;
	}
	
	/**
	 * 付费渠道统计
	 */
	private static function _channelPay()
	{
		$mongoData = Stats_Pay::getChannelPay( self::$todaySec );
		//$mongoData = json_decode( '{"20130708":[{"type":"gw","sum":1},{"type":"ali","sum":6}]}' , true );
		$statDay = date( 'Ymd' , self::$todaySec );
		
		foreach ( $mongoData[$statDay] as $key => $info )
		{
			$mongoData[$statDay][$key] = array( 'type' => $info['type'] , 'count' => $info['sum'] );
		}
		self::$resultData['channelPay'] = $mongoData;
	}
	
	/**
	 * 更新到统计结果集中
	 */
	private static function _updateToResultCollect()
	{
		//新增当日数据
		$statDay = date( 'Ymd' , self::$todaySec );
		$mongoData = array(
			'pf' => Common::getConfig( 'platform' ),
			'date' => $statDay,
			'newUser' => self::$resultData['userStats']['registerNum'],
			'loginUser' => self::$resultData['userStats']['loginNum'],
			'userRefer' => self::$resultData['userRefer'][$statDay],
			'channelPay' => self::$resultData['channelPay'][$statDay],
			'consumeCoin' => self::$resultData['consumeCoin'][$statDay],
			'keepLogin2Days' => 0 , 
			'keepLogin3Days' => 0 ,
		    'keepLogin4Days' => 0 ,
			'keepLogin5Days' => 0 ,
			'keepLogin6Days' => 0 ,
			'keepLogin7Days' => 0 ,
			'keepLogin8Days' => 0 ,
			'keepLogin15Days' => 0 ,
			'keepLogin30Days' => 0 ,
		);
		
		//新增今日注册，登录数据
		$rs = Stats_Model::update( 'displayData', array( 'pf' => Common::getConfig( 'platform' ), 'date' => $mongoData['date'] ), $mongoData );
		if( $rs == false )
		{
			Stats_Model::add( 'displayData',  $mongoData );
		}
		
		
		//更新留存1-30日
		$keepDays = array( 2,3,4,5,6,7,8,15,30 );
		unset( self::$resultData['userStats']['registerNum'] );
		unset( self::$resultData['userStats']['loginNum'] );
		foreach ( self::$resultData['userStats'] as $date => $info )
		{
			foreach ( $info as $times => $data )
			{
				if( $data['num'] > 0 )
				{
					$record = Stats_Model::findOne( 'displayData' , array(  'pf' => Common::getConfig( 'platform' ), 'date' => "$date" ) );
					if( $record['_id'] )
					{
						$recordKey = "keepLogin{$times}Days";
						$record[$recordKey] = $data['num'];
						Stats_Model::update( 'displayData', array(  'pf' => Common::getConfig( 'platform' ), 'date' => "$date" ), $record );
					}
				}
			}
		}
	}
	
	
}