<?php
/**
 * 单个玩家信息统计
 * 
 * @name User.php
 * @author admin
 * @since 2013-7-5
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Stats_User
{

	public static function getCoinLog( $currPage , $pageSize , & $totalNum )
	{
		$start = ($currPage-1)*$pageSize;
		
		$getStartTime =  $_GET['startTime'] ;
		$getEndTime = $_GET['endTime'];
		
		if( !empty( $_GET['date'] ) )
		{
			$getStartTime = $_GET['date'];
			$getEndTime = $getStartTime." 23:59:59";
		}
		
		if( !empty( $getStartTime ))
		{
			$startTime = strtotime( $getStartTime );
		}
		
		if( !empty($getEndTime ))
		{
			$endTime = strtotime($getEndTime );
		}
		$endTime = $endTime ? $endTime : $_SERVER['REQUEST_TIME'];
		$endTime =   $endTime > $_SERVER['REQUEST_TIME'] ? $_SERVER['REQUEST_TIME'] :  $endTime;

		$startTime = $startTime ? $startTime : 0;
		
		$statsData = array();
		if( !empty( $_GET['byUid'] ) )
		{
			$byUid = strval( trim( $_GET['byUid'] ) );
			
			$condition = array(
					"uid" => $byUid , 
					"platform" => Common::getConfig( "platform" ),
				);
				
			if( $_GET['coinType'] == "income" )
			{
				$timeColName = "addTime";
				$collName = "incomeCoin";
			}
			elseif( $_GET['coinType'] == "pay" )
			{
				$timeColName = "payTime";
				$collName = "paymentOrder";
			}
			//default consume
			else 
			{
				$timeColName = "buyTime";
				$collName = "buyHistory";
			}
			
			$condition[$timeColName] = array( '$lt' => $endTime , '$gte' => $startTime );
			$totalNum = Stats_Model::count( $collName , $condition );
			$statsData = Stats_Model::find( $collName ,
				$condition,
				array(
					"start" => $start ,
					"limit" => $pageSize,
					"sort" => array( $timeColName => -1 )
				)
			);
			
			$temp = array();
			foreach( $statsData as $coinLog)
			{
				if( $collName == "buyHistory" )
				{
					$coinLog['addTime'] = $coinLog[$timeColName];
					unset( $coinLog['buyTime'] );
				}
				elseif( $collName == "paymentOrder" )
				{
					$coinLog = array(
						'uid' => $coinLog['uid'],
						'coin' => $coinLog['coin'],
						'gid' => $collName,
						'addTime' => $coinLog[$timeColName],
						'platform' => $coinLog['platform'],
						'subType' => $coinLog['type'],
					);
					unset( $coinLog['payTime'] );
				}
				$temp[] = $coinLog;
			}
			$statsData = $temp;
			
		}
		
		return $statsData;
	}
	
	public static function getItemLog( $currPage , $pageSize , & $totalNum )
	{
		$start = ($currPage-1)*$pageSize;
		
		$getStartTime =  $_GET['startTime'] ;
		$getEndTime = $_GET['endTime'];
		
		if( !empty( $_GET['date'] ) )
		{
			$getStartTime = $_GET['date'];
			$getEndTime = $getStartTime." 23:59:59";
		}
		
		if( !empty( $getStartTime ))
		{
			$startTime = strtotime( $getStartTime );
		}
		
		if( !empty($getEndTime ))
		{
			$endTime = strtotime($getEndTime );
		}
		$endTime = $endTime ? $endTime : $_SERVER['REQUEST_TIME'];
		$endTime =   $endTime > $_SERVER['REQUEST_TIME'] ? $_SERVER['REQUEST_TIME'] :  $endTime;

		$startTime = $startTime ? $startTime : 0;
		
		$statsData = array();
		if( !empty( $_GET['byUid'] ) )
		{
			$byUid = strval( trim( $_GET['byUid'] ) );
			
			$condition = array(
					"uid" => $byUid , 
					"platform" => Common::getConfig( "platform" ),
					"addTime" => array( '$lt' => $endTime , '$gte' => $startTime ),
				);
				
			$collName = "userItemLog";
			$totalNum = Stats_Model::count( $collName , $condition );

			$statsData = Stats_Model::find( $collName ,
				$condition,
				array(
					"start" => $start ,
					"limit" => $pageSize,
				)
			);
			//$statsData = iterator_to_array( $statsData );
			
		}
		
		return $statsData;
	}
	
	public static function getArenaLog( $currPage , $pageSize , & $totalNum )
	{
		$start = ($currPage-1)*$pageSize;
		
		$getStartTime =  $_GET['startTime'] ;
		$getEndTime = $_GET['endTime'];
		
		if( !empty( $_GET['date'] ) )
		{
			$getStartTime = $_GET['date'];
			$getEndTime = $getStartTime." 23:59:59";
		}
		
		if( !empty( $getStartTime ))
		{
			$startTime = strtotime( $getStartTime );
		}
		
		if( !empty($getEndTime ))
		{
			$endTime = strtotime($getEndTime );
		}
		$endTime = $endTime ? $endTime : $_SERVER['REQUEST_TIME'];
		$endTime =   $endTime > $_SERVER['REQUEST_TIME'] ? $_SERVER['REQUEST_TIME'] :  $endTime;

		$startTime = $startTime ? $startTime : 0;
		
		$statsData = array();
		if( !empty( $_GET['byUid'] ) )
		{
			$byUid = strval( trim( $_GET['byUid'] ) );
			
			$condition = array(
					"uid" => $byUid , 
					"platform" => Common::getConfig( "platform" ),
					"addTime" => array( '$lt' => $endTime , '$gte' => $startTime ),
				);
				
			$collName = "userArenaLog";
			$totalNum = Stats_Model::count( $collName , $condition );
			$statsData = Stats_Model::find( $collName ,
				$condition,
				array(
					"start" => $start ,
					"limit" => $pageSize,
				)
			);
			//$statsData = iterator_to_array( $statsData );
			
		}
		
		return $statsData;
	}
	
	public static function checkBanList( $userId )
	{
// 		if(Helper_Common::inPlatform(array('vn')))
// 		{
// 			$whiteList = array(112671, 112675, 112675, 117231, 115180, 730133545, 118716);
// 			if(!in_array($userId, $whiteList))
// 				throw new User_Exception( User_Exception::STATUS_ON_BAN_LIST );
// 		}
		
		$onBanList = Stats_Model::count( "banList" , array(
			"uid"=> strval( $userId ),
			"platform" => Common::getConfig( "platform" ),
		) );
		if( $onBanList > 0 ) throw new User_Exception( User_Exception::STATUS_ON_BAN_LIST );
	}
	
	public static function getBanList( $currPage , $pageSize , & $totalNum )
	{
		$start = ($currPage-1)*$pageSize;
		
		$getStartTime =  $_GET['startTime'] ;
		$getEndTime = $_GET['endTime'];
		
		if( !empty( $_GET['date'] ) )
		{
			$getStartTime = $_GET['date'];
			$getEndTime = $getStartTime." 23:59:59";
		}
		
		if( !empty( $getStartTime ))
		{
			$startTime = strtotime( $getStartTime );
		}
		
		if( !empty($getEndTime ))
		{
			$endTime = strtotime($getEndTime );
		}
		$endTime = $endTime ? $endTime : $_SERVER['REQUEST_TIME'];
		$endTime =   $endTime > $_SERVER['REQUEST_TIME'] ? $_SERVER['REQUEST_TIME'] :  $endTime;

		$startTime = $startTime ? $startTime : 0;
		
		$condition = array(
			'platform' => Common::getConfig( "platform" ),
		);
		if( !empty( $_GET['byUid'] ) )
		{
			$byUid = strval ( trim( $_GET['byUid'] ) );
			//$condition['$or'] = array( array( 'uid' => intval( $byUid ) ) , array( 'uid' => $byUid ) );
		}
		
		$totalNum = Stats_Model::count( "banList" , $condition );
			
		$statsData = Stats_Model::find( "banList" ,
			$condition,
			array(
				"start" => $start ,
				"limit" => $pageSize,
			)
		);
		
		return $statsData;
	}
	
	public static function banUser( $byUid )
	{
		if( Stats_Model::count( "banList" , array(
				"uid" => $byUid,
				"platform" => Common::getConfig( "platform" ),
			) ) <= 0 )
		{
			Stats_Model::add( "banList" , array(
				"uid" => $byUid,
				"addTime" => $_SERVER['REQUEST_TIME'],
				"platform" => Common::getConfig( "platform" ),
			) );
		}
	}
	
	public static function releaseUser( $byUid )
	{
		if( Stats_Model::count( "banList" , array(
					"uid" => $byUid,
					"platform" => Common::getConfig( "platform" ),
				) ) > 0 )
		{
			Stats_Model::remove( "banList" , array(
				"uid" => $byUid,
				"platform" => Common::getConfig( "platform" ),
			) );
		}
	}
	
	public static function getActiveCodeHistory( $currPage , $pageSize , & $totalNum )
	{
		$start = ($currPage-1)*$pageSize;
	
		$getStartTime =  $_GET['startTime'] ;
		$getEndTime = $_GET['endTime'];
	
		if( !empty( $_GET['date'] ) )
		{
			$getStartTime = $_GET['date'];
			$getEndTime = $getStartTime." 23:59:59";
		}
	
		if( !empty( $getStartTime ))
		{
			$startTime = strtotime( $getStartTime );
		}
	
		if( !empty($getEndTime ))
		{
			$endTime = strtotime($getEndTime );
		}
		$endTime = $endTime ? $endTime : $_SERVER['REQUEST_TIME'];
		$endTime =   $endTime > $_SERVER['REQUEST_TIME'] ? $_SERVER['REQUEST_TIME'] :  $endTime;
	
		$startTime = $startTime ? $startTime : 0;
	
		$byUid = intval($_GET['byUid']);
		$byCode = trim($_GET['byCode']);
		
		if(!empty($byUid) || !empty($byCode))
		{
			if(!empty($byUid))
			{
				$conSql = "`uid`={$byUid}";
			}
			else
			{
				$conSql = "`id`='{$byCode}'";
			}
	
			$dbEngine = Common::getDB(1);
			
			$getSql = "select count(1) from active_code where ".$conSql;
			$statsData = $dbEngine->findQuery( $getSql  );
			$totalNum = intval($statsData[0][0]);
	
			$getSql = "select * from active_code where ".$conSql." limit {$start},{$pageSize}";
			$statsData = $dbEngine->findQuery( $getSql  );
		}
		
		if(empty($statsData)) $statsData = array();
	
		return $statsData;
	}
	
	public static function getUserList( $currPage , $pageSize , & $totalNum )
	{
		$start = ($currPage-1)*$pageSize;
	
		$startTime =  $_GET['startTime'] ;
		$endTime = $_GET['endTime'];
	
		$conSql = "";
		
		do
		{
			$type = $_GET['searchType'];
			if($type == "level"
				||$type == "coin"
				||$type == "gold"
				||$type == "benefit"
			)
			{
				$tableName = "user_info";
				if($type == "benefit") $tableName = "arena_normal";
				$conSql = "1=1"
					.self::getConSql($type, ">=", $startTime)
					.self::getConSql($type, "<", $endTime)
					.($type == "benefit" ? self::getConSql('uid', '>=', 1) : "")
					." order by `".$type."` asc";
			}
			elseif($type == "registerTime"
				||$type == "loginTime")
			{
				$tableName = "user_profile";
				$conSql = "1=1"
					.self::getConSql($type, ">=", $startTime, 'date')
					.self::getConSql($type, "<", $endTime, 'date')
					." order by `".$type."` asc";
			}
			elseif($type == "cardId")
			{
				$cardConf = Common::getConfig('card');
				if(empty($cardConf[$startTime])) break;
				
				$tableName = "card";
				$conSql = "1=1"
					.self::getConSql($type, "=", $startTime, 'str');
				$cntSelSql = "COUNT(DISTINCT(`uid`))";
				$selSql = "DISTINCT(`uid`)";
			}
		}while(0);
		
		if(empty($conSql)) return array();
	
		$expireTime = 3600;
		$dbEngine = Common::getDB(1);
		
		$cache = Common::getCache();
		$cacheKey = 'admin_userList_';
		
		$getCntSql = "select ".(empty($cntSelSql) ? "count(1)" : $cntSelSql)
			." from `".$tableName."` where ".$conSql;
		if(isset($_REQUEST['debug'])) print_r($getCntSql);
		if(!$statsData = $cache->get($cacheKey.$getCntSql))
		{
			$statsData = $dbEngine->findQuery($getCntSql);
			if(!empty($statsData)) $cache->set($cacheKey.$getCntSql, $statsData, $expireTime);
		}
		
		$totalNum = intval($statsData[0][0]);

		$getSql = "select ".(empty($selSql) ? "`uid`" : $selSql)
			." from `".$tableName."` where ".$conSql." limit {$start},{$pageSize}";
		if(isset($_REQUEST['debug'])) print_r("<br/>".$getSql);
		if(!$statsData = $cache->get($cacheKey.$getSql))
		{
			$statsData = $dbEngine->findQuery($getSql);
			if(!empty($statsData)) $cache->set($cacheKey.$getSql, $statsData, $expireTime);
		}
	
		if(empty($statsData)) $statsData = array();
	
		return $statsData;
	}
	
	private static function getConSql($col, $con, $val, $type = '')
	{
		if(empty($val)) return "";
		if($type == "str")
		{
			$val = "'".addslashes($val)."'";
		}
		elseif($type == "date")
		{
			$val = strtotime($val);
		}
		else
		{
			$val = intval($val);
		}
		
		return " and `".$col."`".$con."".$val;
	}
	
}