<?php
/**
 * 统计基类
 * 
 * @name Base.php
 * @author admin
 * @since 2013-7-5
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Stats_Base
{
	
	private static $mongoDB;
	
	/**
	 * 往表中，增加一个记录
	 * @param unknown $collectName 集合名，表名
	 * @param unknown $record 记录数组,同一个表字段要一至
	 */
	public static function add( $collectName ,  $record )
	{
		self::init();
		if( !self::chkInit() ) return;
		$status = self::$mongoDB->insert( $collectName, $record );
	}
	
	/**
	 * 在指定集合中，查询
	 * @param unknown $collectName 集合名,相当于mysql的表名
	 * @param unknown $queryCondition 查询条件数据，见test
	 * @param unknown $resultCondition 过滤结果的条件
	 * @param unknown $fields 结果集中显示的字段
	 */
	public static function find( $collectName , $queryCondition  = array(), $resultCondition = array() ,  $fields = array() )
	{
		self::init();
		if( !self::chkInit() ) return;
		$rs = self::$mongoDB->find(  $collectName , $queryCondition , $resultCondition , $fields );
		return  $rs;
	}
	
/**
	 * 在指定集合中，查询一条数据
	 * @param unknown $collectName 集合名,相当于mysql的表名
	 * @param unknown $queryCondition 查询条件数据，见test
	 * @param unknown $fields 结果集中显示的字段
	 */
	public static function findOne( $collectName , $queryCondition  = array() ,  $fields = array() )
	{
		self::init();
		if( !self::chkInit() ) return;
		$rs = self::$mongoDB->findOne(  $collectName , $queryCondition , $fields );
		return  $rs;
	}
	
	/**
	 * 在指定集合中，更新
	 * @param unknown $collectName
	 * @param unknown $queryCondition
	 * @param unknown $newdata
	 */
	public static function update( $collectName , $queryCondition , $newdata )
	{
		self::init();
		if( !self::chkInit() ) return;
		$status = self::$mongoDB->update( $collectName , $queryCondition, $newdata );
		return $status;
	}
	
	/**
	 * 在指定集合中，删除
	 * @param unknown $collectName
	 * @param unknown $queryCondition
	 */
	public static function remove( $collectName , $queryCondition )
	{
		self::init();
		if( !self::chkInit() ) return;
		$status = self::$mongoDB->remove( $collectName , $queryCondition );
	}
	
	public static function count(  $collectName , $queryCondition  = array() )
	{
		self::init();
		if( !self::chkInit() ) return;
		$rs = self::$mongoDB->count( $collectName , $queryCondition );
		return  $rs;
	}
	
	public static function group( $collection , $keys , $initial , $reduce , $condition  = null )
	{
		self::init();
		if( !self::chkInit() ) return;
		$rs = self::$mongoDB->group( $collection , $keys , $initial , $reduce , $condition );
		return  $rs;
	}

	public static function index(  $collectName , $index )
	{
		self::init();
		if( !self::chkInit() ) return;
		$rs = self::$mongoDB->ensureIndex( $collectName , $index );
		return  $rs;
	}
	
	/**
	 * 初始化mongodb实例，选择一个数据库
	 */
	private static function init()
	{
		$config = Common::getConfig( 'mongoDb' );
		self::$mongoDB = InuMongoDB::getInstance( $config['statsDB']['host'] );
		$dbName =  $config['statsDB']['dbname'];
		self::$mongoDB->selectDb(  $dbName );
		return self::$mongoDB;	
	}
	
	private static function chkInit()
	{
		if( !self::$mongoDB )
		{
			echo "请执行initDB和setDBName";
			return false;
		}
		return true;
	}
	
}