<?php
/**
 * 统计
 * 
 * @name Model.php
 * @author admin
 * @since 2013-1-14
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Stats_Model extends Stats_Base
{
		
	/**
	 * 每日首次登录日志
	 * @param unknown $uid  用户ID
	 * @param unknown $registerTime 注册时间
	 * @param unknown $loginTime 登录时间
	 * @param unknown $loginDays 连续登录天数
	 * @param unknown $clientType 客户端类型（安卓1，ios2,）,默认为安卓
	 * @param unknown $clientCode 客户端机器码
	 * 
	 * exp:
	 * $userProfile = Data_User_Profile::getInstance( $this->userId )->getData();
	 *
	 *	Stats_Model::addUserLogin(
	 *		$this->userId ,
	 *		$userProfile['registerTime'],
	 *		$_SERVER['REQUEST_TIME'],
	 *		$userProfile['loginDays']
	 *	);
	 *	
	 *	$stats = Stats_Model::find( "userLogin"  );
	 *	print_r( $stats );exit;
	 * 
	 * 
	 * 
	 */
	public static function addUserLogin( $uid , $registerTime , $loginTime , $loginDays , $clientType = 1 , $clientCode = ''  )
	{
		if(!Helper_Common::inPlatform(array('wk'))) return;
		$userProfile = Data_User_Profile::getInstance( $uid )->getData();
		//下载来源
		$refer = $userProfile['refer'] ? $userProfile['refer'] : $_REQUEST['refer'];
		$refer = $refer > 0 ? $refer : 0;
		if( $userProfile['refer'] == 0 && $userProfile['refer'] != $_REQUEST['refer'] )
		{
			Data_User_Profile::getInstance( $uid , true )->setDownRefer( $refer );
		}
		
		$platform = Common::getConfig( "platform");
		if( empty( $userProfile['platform'] )  && !empty(  $platform )  )
		{
			Data_User_Profile::getInstance( $uid , true )->setPlatform( $platform  );
		}
		
		$records = array(
				'uid' => strval( $uid ),
				'registerTime' => $registerTime,
				'loginTime' => $loginTime,
				'loginDays' => $loginDays,
				'clientType' => $clientType ? $clientType : 'default',
				'platform' => $platform,
				'refer' => $refer ? $refer : 0,
				'ip' => Helper_IP::getCurrentIP(),
		);
		self::add( "userLogin", $records );
	}
	
	/**
	 * 用户登陆日志,每次登录都记录
	 * @param unknown $uid  用户ID
	 * @param unknown $loginTime 登录时间
	 */
	public static function addUserLoginLog( $uid , $loginTime )
	{
		if(!Helper_Common::inPlatform(array('wk'))) return;
		$records = array(
				'uid' => strval( $uid ),
				'loginTime' => $loginTime,
				'platform' => Common::getConfig( "platform"),
				'ip' => Helper_IP::getCurrentIP(),
		);
		self::add( "userLoginLog", $records );
	}
	
	/**
	 *  充值统计
	 * @param unknown $uid
	 * @param unknown $payTime 
	 * @param unknown $rmb 人民币
	 * @param unknown $coin 元宝数
	 */
	public static function addPayUser( $uid , $payTime , $rmb , $coin , $gid , $number , $type )
	{
		$goodsConfig = Common::getConfig( "goods" );
		$goodsConf = $goodsConfig[$gid];
		if( $goodsConf )
		{
			Activity_Model::getInstance( $uid )->consume( $goodsConf['coin'] , Activity_Model::PAID_TYPE_SINGLE_PAY );
			Activity_Model::getInstance( $uid )->consume( $goodsConf['coin'] , Activity_Model::PAID_TYPE_SUM_PAY );
		}
		
		$orderList = Data_Order_Model::getInstance( $uid )->getData();
		$times = 0;
		foreach ( $orderList as $order )
		{
			if( $order['status'] == 1 )
			{
				$times += 1;
			}
		}
		
		//下载来源
		$userProfile = Data_User_Profile::getInstance( $uid )->getData();
		$refer = $userProfile['refer'] ? $userProfile['refer'] : 0;
		
		$platform = Common::getConfig( "platform");
		$records = array(
			'uid' => strval( $uid ),
			'rmb' => $rmb,
			'coin' => $coin,
			'gid' => $gid,
			'num' => $number,
			'payTime' => $payTime,
			'refer' => $refer,
			'platform' => $platform,
			'times' => $times,
			'type' => $type,
			'regisTime' => $userProfile['registerTime'],
		);
		self::add( "paymentOrder", $records );
	}
	
	/**
	 * 元宝消耗
	 * @param unknown $uid
	 * @param unknown $gid 消耗类型
	 * @param unknown $number 次数
	 * @param unknown $coin 花掉元宝数量
	 */
	public static function buyHistory( $uid , $gid , $number , $coin , $subType = "" )
	{
		Activity_Model::getInstance( $uid )->consume( $coin );
		
		if(!Helper_Common::inPlatform(array('wk'))) return;
		$platform = Common::getConfig( "platform");
		
		$records = array(
				'uid' => strval( $uid ),
				'coin' => $coin,
				'gid' => $gid,
				'num' => $number,
				'buyTime' => $_SERVER['REQUEST_TIME'],
				'platform' => $platform,
		);
		if( !empty( $subType ) ) $records['subType'] = $subType;
		self::add( "buyHistory", $records );
	}
	
	/**
	 * 元宝获得
	 * @deprecated
	 * @param unknown $uid
	 * @param unknown $gid 获得类型
	 * @param unknown $coin 获得元宝数量
	 * @param string $subType 子类型
	 */
	public static function incomeCoin( $uid , $gid , $coin , $subType = "" )
	{
		$platform = Common::getConfig( "platform");
		
		$records = array(
				'uid' => strval( $uid ),
				'coin' => $coin,
				'gid' => $gid,
				'addTime' => $_SERVER['REQUEST_TIME'],
				'platform' => $platform,
		);
		if( !empty( $subType ) ) $records['subType'] = $subType;
		self::add( "incomeCoin", $records );
	}
	
	/**
	 * 金币收入
	 * @deprecated
	 * @param unknown $uid
	 * @param unknown $gold
	 */
	public static function incomeGold( $uid , $gold )
	{
		$platform = Common::getConfig( "platform");
	
		$records = array(
				'uid' => strval( $uid ),
				'gold' => $gold,
				'addTime' => $_SERVER['REQUEST_TIME'],
				'platform' => $platform,
		);
		self::add( "incomeGold", $records );
	}
	
	/**
	 * 金币消耗
	 * @deprecated
	 * @param unknown $uid
	 * @param unknown $gold
	 */
	public static function consumeGold( $uid , $gold )
	{
		$platform = Common::getConfig( "platform");
	
		$records = array(
				'uid' => strval( $uid ),
				'gold' => $gold,
				'addTime' => $_SERVER['REQUEST_TIME'],
				'platform' => $platform,
		);
		self::add( "consumeGold", $records );
	}
	
	/**
	 * 道具流水
	 * @param unknown_type $uid 用户id
	 * @param unknown_type $type 道具类型 ("card")
	 * @param string|array $id 道具编号
	 * @param unknown_type $num 数量
	 * @param unknown_type $action 操作 (add,del)
	 * @param unknown_type $logType 来源 ("activity")
	 */
	public static function addUserItemLog( $uid , $type , $id , $num , $action , $logType )
	{
		self::add( "userItemLog" , array(
			'uid' => strval( $uid ),
			'type' => $type,
			'id' => $id,
			'num' => $num,
			'action' => $action,
			'logType' => $logType,
			'platform' => Common::getConfig( "platform" ),
			'addTime' => $_SERVER['REQUEST_TIME'],
		) );
	}
	
	/**
	 * 竞技场流水
	 * @deprecated
	 * @param string $uid 用户id
	 * @param string $type 竞技场类型 普通,精英
	 * @param int $cUid 挑战者id
	 * @param int $bUid 被挑战者id
	 * @param int $cScore 挑战者积分
	 * @param int $bScore 被挑战者积分
	 * @param array $bonusType 奖励
	 */
	public static function addUserArenaLog( $uid , $type , $cUid , $bUid , $cScore , $bScore , $bonus )
	{
		self::add( "userArenaLog" , array(
			'uid' => strval( $uid ),
			'type' => $type,
			'cUid' => intval( $cUid ),
			'bUid' => intval( $bUid ),
			'cScore' => $cScore,
			'bScore' => $bScore,
			'bonus' => $bonus,
			'platform' => Common::getConfig( "platform" ),
			'addTime' => $_SERVER['REQUEST_TIME'],
		) );
	}
	
	/**
	 * 用户行为统计
	 * @deprecated
	 * @param unknown_type $uid
	 * @param unknown_type $type
	 * @param unknown_type $subType
	 */
	public static function addUserActionLog( $uid , $type , $subType = "" )
	{
		self::add( "userActionLog" , array(
			'uid' => strval( $uid ),
			'type' => $type,
			'subType' => $subType,
			'platform' => Common::getConfig( "platform" ),
			'addTime' => $_SERVER['REQUEST_TIME'],
		) );
	}
	
	/**
	 * gm修改数据日志 
	 * @param unknown_type $userId
	 * @param unknown_type $type
	 * @param unknown_type $subType
	 * @param unknown_type $amount
	 * @param unknown_type $comment
	 */
	public static function addGMActionLog( $userId , $type , $subType , $amount , $comment )
	{
		self::add( "GMActionLog" , array(
			'id' => $_SESSION['admin']['id'],
			'loginName' => $_SESSION['admin']['loginName'],
			'uid' => strval( $userId ),
			'type' => $type,
			'subType' => $subType,
			'amount' => floatval( $amount ),
			'comment' => $comment,
			'platform' => Common::getConfig( "platform" ),
		) );
	}
	
	/**
	 * 上周统计
	 * Enter description here ...
	 * @param unknown_type $uid
	 */
	public static function taskWeekly( $uid )
	{
		Arena_Normal::getInstance( $uid )->taskWeekly();
		
		$cache = Common::getCache();
		$updateTime = $cache->get( "weeklyUpdateTime" );
		
		$curMon = Helper_Date::getWeekStartTime();
		if( !empty( $updateTime ) && $updateTime >=  $curMon ) return false;

		//-----------------------任务开始
		
		//-----------------------任务结束
		
		$cache->set( "weeklyUpdateTime" , $_SERVER['REQUEST_TIME'] );
		
		return true;
	}
	
	/**
	 * 昨日相关统计
	 * @param unknown_type $uid
	 */
	public static function taskDaily( $uid )
	{
		if( !!$lock = Helper_Common::addLock( "taskDaily" , 1 ) )
		{
			$platform = Common::getConfig( 'platform' );
			
			$cache = Common::getCache();
			$cacheKey = 'dailyUpdateTime_'.$platform;
			
			$updateTime = $cache->get( $cacheKey );
			
			$todayStart = strtotime( date( 'Y-m-d' , $_SERVER['REQUEST_TIME'] ) );
			if( !empty( $updateTime ) && $updateTime >=  $todayStart ) return false;
			
			//-----------------------任务开始
			
			//元宝, 金币留存
			$dbEngine = Common::getDB( 1 );
			//7天内没有登入过的玩家不计入统计
			$getSql = "select sum(`coin`),sum(`gold`) from `user_info` inner join `user_profile` using(`uid`) where `platform`='"
					.$platform."' and `loginTime`>".($todayStart - 86400 * 7);
			$data = $dbEngine->findQuery($getSql);
			
			/**
			 * EDAC数据中心，统计
			 */
			Stats_Edac::haveCoin(floatval($data[0][0]));
			Stats_Edac::haveGold(floatval($data[0][1]));
			
			//-----------------------任务结束
			
			$cache->set( $cacheKey , $_SERVER['REQUEST_TIME'] );
		}
		
		if($lock) Helper_Common::delLock( "taskDaily" );
		
		return true;
	}
	
	/**
	 * 每隔5分钟的统计(**分平台**)
	 * Enter description here ...
	 * @param unknown_type $uid
	 */
	public static function taskPer5Min( $uid )
	{
		if( !!$lock = Helper_Common::addLock( "taskPer5Min" , 1 ) )
		{
				
			$cache = Common::getCache();
			$cacheKey = 'stats5minUpdateTime_'.Common::getConfig('platform');
			$updateTime = $cache->get($cacheKey);
			
			$last5minTime = strtotime( date( 'Y-m-d H:00:00' , $_SERVER['REQUEST_TIME'] ) ) + intval( date( 'i' , $_SERVER['REQUEST_TIME'] ) / 5 ) * 5 * 60;
			if( !empty( $updateTime ) && $updateTime >= $last5minTime ) return false;
			
			//-------------------任务开始
			
			self::taskOnlineStats( $uid , $last5minTime );
			
			//-------------------任务结束
			
			$cache->set($cacheKey, $_SERVER['REQUEST_TIME'] );
		}
		
		if($lock) Helper_Common::delLock( "taskPer5Min" );
		
		return true;
	}
	
	/**
	 * 在线人数统计
	 * Enter description here ...
	 * @param unknown_type $uid
	 * @param unknown_type $time
	 */
	private static function taskOnlineStats( $uid , $time )
	{
		if( !!$lock = Helper_Common::addLock( "online" ) )
		{
			$cache = Common::getCache();
			$online = $cache->get( "online" );
				
			if( !empty( $online ) )
			{
				$onlineNum = 0;
				$platform = Common::getConfig( "platform" );
				
				foreach( $online as $key => $heartBeatTime )
				{
					if(is_array($heartBeatTime))
					{
						$pf = $heartBeatTime['pf'];
						$heartBeatTime = $heartBeatTime['t'];
						
						if($pf != $platform) continue;
					}
					
					//心跳时间在5分钟内记做在线
					if( $heartBeatTime > $_SERVER['REQUEST_TIME'] - 300 )
					{
						$onlineNum++;
					}
					//清除多余的用户
					else
					{
						unset($online[$key]);
					}
				}
				
				$cache->set( "online" , $online , 300 );
					
				/**
				 * EDAC数据中心，统计
				*/
				Stats_Edac::userOnline( $onlineNum ,  $_SERVER['REQUEST_TIME'] );
					
				$dayTime = strtotime( date( 'Y-m-d' , $_SERVER['REQUEST_TIME'] ) );
					
				$statsOnline = self::findOne( "statsOnline" ,array(
						"platform" => $platform,
						"time" => $dayTime ,
				) );
					
				if( $statsOnline )
				{
					self::update( "statsOnline" , array(
							"platform" => $platform,
							"time" => $dayTime ,
					) , array( '$set' => array( "data.".$time => $onlineNum ) ) );
				}
				else
				{
					self::add( "statsOnline" , array(
							"platform" => $platform,
							"time" => $dayTime ,
							"data" => array( $time => $onlineNum ),
					) );
				}
			}
			
			Helper_Common::delLock( "online" );
		}
		
		$payLog = new ErrorLog( "heartbeat" );
		$payLog->addLog( "taskOnlineStats: ".date('Y-m-d H:i:s', $time).', onlineNum: '.intval($onlineNum).", pf : ".Common::getConfig('platform'));
	}
	
}