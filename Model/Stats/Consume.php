<?php
/**
 * 消费统计
 * 
 * @name Consume.php
 * @author admin
 * @since 2013-7-5
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Stats_Consume
{
	
	public static $consumeConf = array(
		'recoverStamina' => array( 'cond' => array( 'gid' => 'recoverStamina' ) , 'name' => '恢复体力' ),
		'revive' => array( 'cond' => array( 'gid' => 'revive' ) , 'name' => '复活' ),
		'expandCamp' => array( 'cond' => array( 'gid' => 'expandCamp' ) , 'name' => '扩充军营' ),
		'expandFriend' => array( 'cond' => array( 'gid' => 'expandFriend' ) , 'name' => '扩充好友' ),
		'draw1Card' => array( 'cond' => array( 'gid' => 'drawCard' , '$or' => array( array( 'num' => 1) , array( 'num' => 2 ) ) ) ,
											'name' => '抽神将' ),
		'draw10Card' => array( 'cond' => array( 'gid' => 'drawCard' , 'num' => 10 ) , 'name' => '10连抽' ),
		'arenaRefreshEnemy' => array( 'cond' => array( 'gid' => 'arenaRefreshEnemy' ) , 'name' => '刷新对手' ),
		'arenaRefreshTimes' => array( 'cond' => array( 'gid' => 'arenaRefreshTimes' ) , 'name' => '刷新次数' ),
	);

	public static function getCoinConsume( $statsName , $serverTime )
	{
		$result = array();
		
		$maxDay = date( "t" , $serverTime );
		for( $i = 1 ; $i <= $maxDay ; $i++ )
		{
			if( strtotime( date( "Y-m-d" , $_SERVER['REQUEST_TIME'] ) ) <=  strtotime( date( "Y-m-" , $serverTime ).$i ) )
			{
				break;
			}

			$dayStart = strtotime( date( "Y-m-$i" , $serverTime ) );
			$dayEnd = $dayStart + 86400;
			
			$currDay = date( "Y-m-d" , $dayStart );
			
			//尝试从统计表中拉取数据
			$statsResult = Stats_Model::findOne( "statsResult" , array(
				"type" => $statsName,
				"platform" => Common::getConfig( 'platform' ),
				"time" => $dayStart
			) );
			
			if( isset( $statsResult['data'] ) )
			{
				$result[$currDay] = $statsResult['data'];
			}
			else
			{
				$queryCondBase = array(
						'platform' => Common::getConfig( 'platform' ),
						'buyTime' => array(
							'$gte' =>$dayStart,
							'$lte' =>$dayEnd,
						)
				);
				
				$result[$currDay]['num'] = $result[$currDay]['sum'] = 0; 
				foreach( self::$consumeConf as $consumeName => $conf )
				{
					$queryCond = $queryCondBase + $conf['cond'];
					$stats = Stats_Model::find( "buyHistory" , $queryCond );
					$result[$currDay][$consumeName.'Num'] =  $result[$currDay][$consumeName.'Sum'] =  0;
					foreach ( $stats as $data )
					{
						$result[$currDay][$consumeName.'Num']  += 1;
						$result[$currDay][$consumeName.'Sum'] += $data['coin'];	
					}
					
					$result[$currDay]['num'] += $result[$currDay][$consumeName.'Num'];
					$result[$currDay]['sum'] += $result[$currDay][$consumeName.'Sum'];
				}
				
				//保存统计记录
				Stats_Model::add( "statsResult" , array(
					"type" => $statsName,
					"format" => "Ymd", 
					"platform" => Common::getConfig( 'platform' ),
					"time" => $dayStart,
					"data" => $result[$currDay]
				) );
			}
			
		}
		return $result;
	}
	
	public static function getCoinKeep( $statsName , $serverTime )
	{
		$result = array();
		
		$maxDay = date( "t" , $serverTime );
		for( $i = 1 ; $i <= $maxDay ; $i++ )
		{
			if( strtotime( date( "Y-m-d" , $_SERVER['REQUEST_TIME'] ) ) <=  strtotime( date( "Y-m-" , $serverTime ).$i ) )
			{
				break;
			}

			$dayStart = strtotime( date( "Y-m-$i" , $serverTime ) );
			$dayEnd = $dayStart + 86400;
			
			$currDay = date( "Y-m-d" , $dayStart );
			
			//尝试从统计表中拉取数据
			$statsResult = Stats_Model::findOne( "statsResult" , array(
				"type" => $statsName,
				"platform" => Common::getConfig( 'platform' ),
				"time" => $dayStart
			) );
			
			if( isset( $statsResult['data'] ) )
			{
				$result[$currDay] = $statsResult['data'];
			}
			else
			{
				//元宝消耗
				$type = "cost";
				$keys = array( "platform" => 1 );
				$initial = array( "sum" => 0 , "count" => 0 );
				$reduce = '
function(obj, prev) {
   prev.count++;
   prev.sum += parseInt(obj.coin);
}';
				$condition = array('condition' => array(
						'buyTime' => array(
								'$gte' => $dayStart,
								'$lt' => $dayEnd,
						),
						'platform' => Common::getConfig( "platform" ),
				));
				 
				$groupRet = Stats_Model::group( "buyHistory" , $keys , $initial , $reduce , $condition );
				$groupRet = $groupRet[0];
				$result[$currDay][$type.'Sum'] = $groupRet['sum'];
				$result[$currDay][$type.'Num'] = $groupRet['count'];
				
				
				//元宝获得
				$type = "income";
				$keys = array( "platform" => 1 );
				$initial = array( "sum" => 0 , "count" => 0 );
				$reduce = '
function(obj, prev) {
   prev.count++;
   prev.sum += parseInt(obj.coin);
}';
				$condition = array('condition' => array(
						'addTime' => array(
								'$gte' => $dayStart,
								'$lt' => $dayEnd,
						),
						'platform' => Common::getConfig( "platform" ),
				));
				 
				$groupRet = Stats_Model::group( "incomeCoin" , $keys , $initial , $reduce , $condition );
				$groupRet = $groupRet[0];
				$result[$currDay][$type.'Sum'] = $groupRet['sum'];
				$result[$currDay][$type.'Num'] = $groupRet['count'];
				
				
				//充值获得
				$type = "pay";
				$keys = array( "platform" => 1 );
				$initial = array( "sum" => 0 , "count" => 0 );
				$reduce = '
function(obj, prev) {
   prev.count++;
   prev.sum += parseInt(obj.coin);
}';
				$condition = array('condition' => array(
						'payTime' => array(
								'$gte' => $dayStart,
								'$lt' => $dayEnd,
						),
						'platform' => Common::getConfig( "platform" ),
				));
				 
				$groupRet = Stats_Model::group( "paymentOrder" , $keys , $initial , $reduce , $condition );
				$groupRet = $groupRet[0];
				$result[$currDay][$type.'Sum'] = $groupRet['sum'];
				$result[$currDay][$type.'Num'] = $groupRet['count'];
				
				//新用户注册获得
				$type = "register";
				$keys = array( "platform" => 1 );
				$initial = array( "count" => 0 );
				$reduce = '
function(obj, prev) {
   prev.count++;
}';
				$condition = array('condition' => array(
						'loginTime' => array(
								'$gte' => $dayStart,
								'$lt' => $dayEnd,
						),
						'registerTime' => array(
								'$gte' => $dayStart,
								'$lt' => $dayEnd,
						),
						'platform' => Common::getConfig( "platform" ),
				));
				 
				$groupRet = Stats_Model::group( "userLogin" , $keys , $initial , $reduce , $condition );
				$groupRet = $groupRet[0];
				$result[$currDay][$type.'Sum'] = $groupRet['count'] * Data_User_Info::$defaultCoin;
				$result[$currDay][$type.'Num'] = $groupRet['count'];
				
				//保存统计记录
				Stats_Model::add( "statsResult" , array(
					"type" => $statsName,
					"format" => "Ymd", 
					"platform" => Common::getConfig( 'platform' ),
					"time" => $dayStart,
					"data" => $result[$currDay]
				) );
				
			}
			
			//剩余元宝总量
			$dailyStats = Stats_Model::findOne( "statsResult" , array(
				"type" => "daily",
				"platform" => Common::getConfig( 'platform' ),
				"time" => $dayStart
			) );
			$result[$currDay]['keepSum'] = isset( $dailyStats['data'] ) ? $dailyStats['data']['coin'] : 0;
			
		}
		return $result;
	}
	
}