<?php
/**
 * 登录统计
 * 
 * @name Login.php
 * @author admin
 * @since 2013-07-05
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Stats_Active
{

	public static function getActiveData( &$monthStats , &$useTime )
	{
		//注册用户数
		//小于("$lt")、小于等于("$lte")、大于("$gt")、大于等于("$gte")、不等于("$ne")
		//查询时间大于开始时间，小于结束时间
		$days = date( "t" , $_SERVER['REQUEST_TIME']);
		$monthStats = array();
		//活跃类型:日活跃，周活跃，月活跃
		$activeTypes = array(  "activeDay" , "activeWeek" , "activeMonth" );
		$reloginType = array( 1, 2, 3, 4, 5, 6, 7 );
		$startTime = microtime( true );
		
		
		$cache = Common::getCache();
		/**
		 * $monthStats = array(
		 * 	 	01 => array(
		 * 		'activeDay' => 10034,
		 * 		'activeWeek' => 133300,
		 * 		'activeMonth' => 1231234,
		 * 		'relogin1' => 1333,
		 * 	),
		 * 	...
		 * );
		 *
		*/
		$monthStats = $cache->get( "monthActiveStats".date("Ym") );
		//每天循环查10次，实际上，每天只执行当天查询
		for ( $i = 1;$i <= $days; $i++ )
		{
			$i = $i<10 ? '0'.$i : $i;
			if( $i > date("d"))
			{
				break;
			}
		
			//如果做了定时执行任务
			if( $monthStats[$i] )
			{
				//continue;
			}
		
			if( $i != date("d"))
			{
				continue;
			}
		
				
			/**
			 * 日活跃
			 * @var unknown_type
			 */
			$today = date( "Y-m-{$i}" , $_SERVER['REQUEST_TIME']);
			$monthStats[$i]['activeDay'] = self::_findStatsByLoginTime( $today , $today );
			/**
			 * 自然周活跃
			 * @var unknown_type
			*/
			$weekDay = date( "w" , strtotime( $today ) );
			$weekDay = $weekDay == 0 ? 7 : $weekDay;
			$weekDayPres = $weekDay-1;
			$weekDayNexs = 7-$weekDay;
			$weekStart = strtotime( "-{$weekDayPres} day" , strtotime( $today ) );
			$weekEnd = strtotime( "+{$weekDayNexs} day" , strtotime( $today ) );
				
			$weekStart = date( "Y-m-d" , $weekStart );
			$weekEnd = date( "Y-m-d" , $weekEnd );
			$monthStats[$i]['activeWeek'] = self::_findStatsByLoginTime( $weekStart , $weekEnd );
			/**
			 * 自然月活跃
			*/
			$monthStart = date( "Y-m-")."01";
			$monthEnd = date( "Y-m-t");
			$monthStats[$i]['activeMonth'] = self::_findStatsByLoginTime( $monthStart , $monthEnd );
				
			/**
			 * 一日连续登录
			*/
			for( $days = 1 ; $days<= 7; $days++ )
			{
				$monthStats[$i]['relogin'.$days] = self::_findStatsByReLogin( $days );
			}
		
		
		}
		$cache->set( "monthActiveStats".date("Ym") , $monthStats );
		
		$endTime = microtime( true );
		$useTime = sprintf( "%6.fS" , ( $endTime - $startTime )  );
	}
	
	public static function getNewStatsLogin( $statsName , $serverTime )
	{
		$monthMaxDay = date( "t" , $serverTime );
		$searchYm = date( "Ym" , $serverTime );
		$platform = Common::getConfig( "platform");
		
		$cache = Common::getCache();
		if( !$statsData = $cache->get( $statsName."_".$searchYm ) )
		{
			$statsData = array();
			for( $i = 1 ; $i <= $monthMaxDay ; $i++ )
			{
				if( strtotime( date( "Y-m-d" , $_SERVER['REQUEST_TIME'] ) ) <=  strtotime( date( "Y-m-" , $serverTime ).$i ) )
				{
					break;
				}
				$startStr = date( "Y-m-1" ,  $serverTime );
				$startTime = strtotime(  $startStr  ) + ( $i - 1 ) * 86400;
		
				$endTime = $startTime + 86400;
		
				$keys = array( "refer" => 1 );
				$initial = array( "count" => 0 );
				$reduce = "function (obj, prev) { prev.count++; }";
				$condition = array('condition' => array(
					'loginTime' => array(
						'$gte' => $startTime,
						'$lt' => $endTime,
					),
					'registerTime' => array(
						'$gte' => $startTime,
						'$lt' => $endTime,
					),
					'platform' => $platform
				));
		
				$statsData[$startTime] = Stats_Model::group( "userLogin" , $keys , $initial , $reduce , $condition );
			}
		
			//非当前月份留存档
			if( !empty( $statsData ) && intval( date("Ym" , $_SERVER['REQUEST_TIME'] ) ) > intval( date( "Ym" , $serverTime ) ) )
			{
				$cache->set( $statsName."_".$searchYm , $statsData , 86400 * 7 );
			}
			//$cache->delete( $statsName."_".$searchYm );
		}
		//print_r($statsData);die;
		return $statsData;
	}
	
	/**
	 * 用户来源统计
	 * @param unknown $statsName
	 * @param unknown $serverTime
	 * @return Ambigous <mixed, multitype:Ambigous <void, unknown> >
	 */
	public static function getUserRefer( $serverTime )
	{
		$platform = Common::getConfig( "platform");
		$statsData = array();

		$startTime = strtotime( date( "Y-m-d" , $serverTime ) );
		$endTime = $startTime + 86400;
		$statDate = date("Ymd" , $startTime );

		$keys = array( "refer" => 1 );
		$initial = array( "count" => 0 );
		$reduce = "function (obj, prev) { prev.count++; }";
		$condition = array('condition' => array(
				'loginTime' => array(
						'$gte' => $startTime,
						'$lt' => $endTime,
				),
				'registerTime' => array(
						'$gte' => $startTime,
						'$lt' => $endTime,
				),
				'platform' => $platform
		));
		
		$statsData[$statDate] = Stats_Model::group( "userLogin" , $keys , $initial , $reduce , $condition );
		return $statsData;
	}
	
	/**
	 * 统计登录
	 * @param unknown $statsName
	 * @param unknown $serverTime
	 * @param unknown $do
	 * @param number $userId
	 * @return Ambigous <multitype:, string, void, unknown>
	 */
	public static function getStatsLogin( $statsName , $serverTime , $do , $userId = 0 )
	{
		$byMonth = 0;
		$cache = Common::getCache();
		if( $do == "query" )
		{
				$refer = $_POST['refer'];
				$os = $_POST['os'];
		
				$startTime = $serverTime;
				$endTime = $serverTime + 86400;
		}
		else 
		{
				//上个月的最后一天
			   // $lastmonthday = strtotime("-1 month",  $_SERVER['REQUEST_TIME'] );
			   // $startStr = date("Y-m-t", $lastmonthday);
				//本月第一天
				$startStr = date( "Y-m-1" ,  $serverTime );
				$startTime = strtotime(  $startStr  );
		
				$endStr = date( "Y-m-d" , $startTime )." 23:59:59";
				$endTime = strtotime( $endStr );
				
				$monthStats = $cache->get( "monthStats".date("Ym") );
		}
		//注册用户数
		//小于("$lt")、小于等于("$lte")、大于("$gt")、大于等于("$gte")、不等于("$ne") 
		//查询时间大于开始时间，小于结束时间
		$monthMaxDay = date( "t" , $serverTime );
		
		$reloginDays = array( 1, 2, 3, 4, 5, 6, 7, 14 ,30 );
		$loginData = array();
		$dbEngine = Common::_getDbEngine( 1 );

		for( $i = 1 ; $i <= $monthMaxDay ; $i++ )
		{
			if( date("m" , $_SERVER['REQUEST_TIME'] ) == date( "m" , $serverTime  ) && $i > date( "d") )
			{
				break;
			}
			
			if( date( "m" , $serverTime  )  >   date("m" , $_SERVER['REQUEST_TIME'] ) )
			{
				break;
			}


			$registerStart = $startTime;
			$registerEnd = $startTime + 86400;
			
			$registerDay = date( "Y-m-d" , $startTime );       	
			//查询这天注册的玩家，在当天（就是注册量）,第二天，第三天.....登录的数量
			/*
			$sql = "select count(uid) as count from user_profile where registerTime >= $registerStart and  registerTime <= $registerEnd ";
			$result = $dbEngine->fetchArray( $sql );
			$loginData[$registerDay]['registerNum'] = $result[0]['count'];
			*/
			
			//查询新注册用户
			$queryCond['registerTime'] = array(
					'$gte' => $registerStart,
					'$lte' => $registerEnd,
			);
			$queryCond['platform'] = Common::getConfig( "platform" );
			$queryCond['loginTime'] = array(
					'$gte' => $registerStart,
					'$lte' => $registerEnd,
			);
			//	echo json_encode( $queryCond );exit;
			$loginData[$registerDay]['registerNum'] = Stats_Model::count( "userLogin" , $queryCond );
			
			$queryCond = array();
			//查询这天登录的玩家数量
			$queryCond['loginTime'] = array(
					'$gte' => $registerStart,
					'$lte' => $registerEnd,
			);
			$queryCond['platform'] = Common::getConfig( "platform" );	
			$loginData[$registerDay]['loginNum'] = Stats_Model::count( "userLogin" , $queryCond );
			                
			//查询留存率    	
			foreach ( $reloginDays as $days )
			{
				$reloginStart = $registerStart + $days  * 86400;
				$reloginEnd = $registerStart + ( $days +1 ) * 86400;
				      					
				$queryCond = array();
				$queryCond['registerTime'] = array(
					'$gte' => $registerStart,
					'$lte' => $registerEnd,
				);
				      					
				$queryCond['loginTime'] = array(
					'$gte' => $reloginStart,
					'$lte' => $reloginEnd,
				);
			      					
				$queryCond['platform'] = Common::getConfig( "platform" );
				 
				//$stats = Stats_Model::find( "userLogin" , $queryCond );
				$count = Stats_Model::count( "userLogin" , $queryCond );
				if( $loginData[$registerDay]['registerNum'] > 0 )
				{
					$loginData[$registerDay][$days] = sprintf( "%.2f" ,  $count/$loginData[$registerDay]['registerNum'] * 100 )."%";
				}
				else 
				{
					$loginData[$registerDay][$days] = '0.00';
				}
			}
		                	
			$startTime += 86400;
		}
		           
		$key = "monthStats".date("Ym" , $serverTime ) ;
		           
		$cache->set(  $key , $loginData  );
                
		return $loginData;
	}
	
	public static function getUserLoginLog( $currPage , $pageSize , & $totalNum )
	{
		$start = ($currPage-1)*$pageSize;
		
		$getStartTime =  $_GET['startTime'] ;
		$getEndTime = $_GET['endTime'];
		
		if( !empty( $_GET['date'] ) )
		{
			$getStartTime = $_GET['date'];
			$getEndTime = $getStartTime." 23:59:59";
		}
		
		if( !empty( $getStartTime ))
		{
			$startTime = strtotime( $getStartTime );
		}
		
		if( !empty($getEndTime ))
		{
			$endTime = strtotime($getEndTime );
		}
		$endTime = $endTime ? $endTime : $_SERVER['REQUEST_TIME'];
		$endTime =   $endTime > $_SERVER['REQUEST_TIME'] ? $_SERVER['REQUEST_TIME'] :  $endTime;

		$startTime = $startTime ? $startTime : 0;
		
		$statsData = array();
		if( !empty( $_GET['byUid'] ) )
		{
			$byUid = strval ( trim( $_GET['byUid'] ) );
			
			$condition = array(
					"uid" => $byUid , 
					"loginTime" => array( '$lt' => $endTime , '$gte' => $startTime ) ,
					"platform" => Common::getConfig( "platform" ),
				);
				
			$totalNum = Stats_Model::count( "userLoginLog" , $condition );
			
			$statsData = Stats_Model::find( "userLoginLog" ,
				$condition,
				array(
					"start" => $start ,
					"limit" => $pageSize,
				)
			);
		}
		
		return $statsData;
	}
	
	public static function getStatsUserOnline( $statsName , $startTime ) 
	{
		$serverTime = $startTime;
		$searchYm = date( "Ym" , $serverTime );
		$platform = Common::getConfig( "platform");		
		
		//查询2小时内数据
		$endTime = $startTime + 1*60*60;
		//当天0点时间
		$dayTime = strtotime( date( "Ymd" , $serverTime ) );
		$date = date("Ymd" , $dayTime );
			
	
		$statsData = array();
		$result = Stats_Model::findOne( "statsOnline" , array( 
			"platform" => $platform , "time" => $dayTime
		) );
		
	
		$per5minArr = array();
		$displayArr = array();
		for ( $i = $startTime ; $i <= $endTime ;  )
		{
			$min = date( "m/d H:i" , $i );
			$per5minArr[$min] = $result['data'][$i] ? $result['data'][$i] : 0;	
			
			
			$displayArr[] = array(
				'x' => $i * 1000,
				'y' => $per5minArr[$min],
			);
			$i += 300;
		}
		
		$statsData['data'] = $per5minArr;
		$statsData['display'] = json_encode( $displayArr );
		return $statsData;
	}
	
	public static function getStatsUserOnlineByMonth( $statsName , $serverTime ) 
	{
		$monthMaxDay = date( "t" , $serverTime );
		$searchYm = date( "Ym" , $serverTime );
		$platform = Common::getConfig( "platform");
		
		$cache = Common::getCache();
		if( !$statsData = $cache->get( $statsName."_".$searchYm ) )
		{
			$statsData = array();
			for( $i = 1 ; $i <= $monthMaxDay ; $i++ )
			{
				if( strtotime( date( "Y-m-d" , $_SERVER['REQUEST_TIME'] ) ) <  strtotime( date( "Y-m-" , $serverTime ).$i ) )
				{
					break;
				}
				$startStr = date( "Y-m-1" ,  $serverTime );
				$startTime = strtotime(  $startStr  ) + ( $i - 1 ) * 86400;
		
				$endTime = $startTime + 86400;
				
				$result = Stats_Model::findOne( "statsOnline" , array( 
					"platform" => $platform , "time" => $startTime
				) );
				
				$perHourOnline = array();
				if( isset( $result['data'] ) )
				{
					foreach( $result['data'] as $per5min => $onlineNum )
					{
						//0-23小时
						$hour = intval( ( $per5min - $startTime ) / 3600 );
						$perHourOnline[$hour] += $onlineNum;
					}
				}
				$statsData[$startTime] = $perHourOnline;
			}
		
			//非当前月份留存档
			if( !empty( $statsData ) && intval( date("Ym" , $_SERVER['REQUEST_TIME'] ) ) > intval( date( "Ym" , $serverTime ) ) )
			{
				$cache->set( $statsName."_".$searchYm , $statsData , 86400 * 7 );
			}
			//$cache->delete( $statsName."_".$searchYm );
		}
		//print_r($statsData);die;
		return $statsData;
	}
	
	
	/**
	 * 用户统计 
	 * @param unknown $statsName
	 * @param unknown $serverTime
	 * @param unknown $do
	 * @param number $userId
	 * @return Ambigous <multitype:, string, void, unknown>
	 */
	public static function getStatsUser( $serverTime )
	{
		
		$startTime = strtotime(date( "Ymd" , $serverTime ));
		$endTime = strtotime(date( "Ymd" , $serverTime + 86400 ));

		//注册用户数
		//小于("$lt")、小于等于("$lte")、大于("$gt")、大于等于("$gte")、不等于("$ne")
		//查询时间大于开始时间，小于结束时间
		$reloginDays = array(  2, 3, 4, 5, 6, 7, 8, 14 ,30 );
		$loginData = array();
		

		$registerStart = $loginStart = $startTime;
		$registerEnd = $loginEnd = $startTime + 86400;
				
	    $registerDay = date( "Y-m-d" , $startTime );
			
				
		//查询新注册用户
		$queryCond['registerTime'] = array(
				'$gte' => $registerStart,
				'$lte' => $registerEnd,
		);
		$queryCond['platform'] = Common::getConfig( "platform" );
		$queryCond['loginTime'] = array(
					'$gte' => $registerStart,
					'$lte' => $registerEnd,
		);
		//	echo json_encode( $queryCond );exit;
		$loginData['registerNum'] = Stats_Model::count( "userLogin" , $queryCond );
			
		$queryCond = array();
		//查询这天登录的玩家数量
		$queryCond['loginTime'] = array(
			'$gte' => $registerStart,
			'$lte' => $registerEnd,
		);
		$queryCond['platform'] = Common::getConfig( "platform" );
		$loginData['loginNum'] = Stats_Model::count( "userLogin" , $queryCond );
		
		//$loginData['registerNum'] = 20;
		//$loginData['loginNum'] = 50;
		//查询留存率
		//23号的1日留存，24号的，25号的,26号的--23号注册，24号登录
		//23号的2日留存，24号的，25号的,26号的--23号注册，25号登录
		//23号的30日留存，24号的，25号的,26号的--23号注册，下月22号登录
		//...
		foreach ( $reloginDays as $days )
		{
			//$days = $days - 1;
			$registerStart = $startTime - ( $days-1 )  * 86400;
			$registerEnd = $registerStart + 86400;
			
			$queryCond = array();
			//统计日期7.8日，统计前一天7.7日数据，7.6-7.7注册，7.7-7.8登录的为7.6的次日留存
			$queryCond['registerTime'] = array(
					'$gte' => $registerStart,
					'$lte' => $registerEnd,
			);
							 
			$queryCond['loginTime'] = array(
					'$gte' => $loginStart,
					'$lte' => $loginEnd,
			);
	
			//$queryCond['platform'] = Common::getConfig( "platform" );
				
			//$stats = Stats_Model::find( "userLogin" , $queryCond );
			$count = Stats_Model::count( "userLogin" , $queryCond );
			$pregisDay = date( "Ymd" , $registerStart );
			$loginData[$pregisDay][$days]['num'] = $count;
			if( $loginData['registerNum'] > 0 )
			{
				//登录的/新注册的
				$loginData[$pregisDay][$days]['rate'] = sprintf( "%.2f" ,  $count/$loginData['registerNum'] * 100 )."%";
				
			}
			else
			{
				$loginData[$pregisDay][$days]['rate'] = '0.00';
			}
			
			//更新到统计结果集中
		}
		return $loginData;
	}
	
	/**
	 * 新手引导统计
	 * Enter description here ...
	 * @param unknown_type $statsName
	 * @param unknown_type $serverTime
	 */
	public static function getStatsUserGuide( $statsName , $serverTime )
	{
		$monthMaxDay = date( "t" , $serverTime );
		$searchYm = date( "Ym" , $serverTime );
		$platform = Common::getConfig( "platform");
		
		$cache = Common::getCache();
		if( !$statsData = $cache->get( $statsName."_".$searchYm ) )
		{
			$statsData = array();
			for( $i = 1 ; $i <= $monthMaxDay ; $i++ )
			{
				if( strtotime( date( "Y-m-d" , $_SERVER['REQUEST_TIME'] ) ) <=  strtotime( date( "Y-m-" , $serverTime ).$i ) )
				{
					break;
				}

				$startStr = date( "Y-m-1" ,  $serverTime );
				$startTime = strtotime(  $startStr  ) + ( $i - 1 ) * 86400;
				$endTime = $startTime + 86400;
				
				$statsData[$startTime] = $cache->get( $statsName."_".$searchYm."_".$i );
				if( !is_array( $statsData[$startTime] ) )
				{
					//新手引导每步用户
					$keys = array( "subType" => 1 );
					$initial = array( "count"=>0 );
					$reduce = "function (obj, prev) { prev.count++; }";
					$condition = array('condition' => array(
						'addTime' => array(
							'$gte' => $startTime,
							'$lt' => $endTime,
						),
						'type' => "User.setNewbie",
						'platform' => $platform
					));
					
					$newbieLog = Stats_Model::group( "userActionLog" , $keys , $initial , $reduce , $condition );
					$tempNewbie = array();
					foreach( $newbieLog as $newbie )
					{
						$tempNewbie[$newbie['subType']] = $newbie;
					}
					$newbieLog = $tempNewbie;
					if( isset( $newbieLog['0'] ) )
					{
						//注册用户数
						$dbEngine = Common::getDB( 1 );
						$sql = "SELECT count(1) FROM user_profile WHERE registerTime>=".$startTime." and registerTime<".$endTime;
						$userCnt = $dbEngine->findQuery($sql);
						$userCnt = $userCnt[0][0];
						$newbieLog['0']['count'] = $userCnt;	
					}
					ksort($newbieLog);
					$statsData[$startTime] = $newbieLog;
					//当天之前的留存档
					//if( !empty( $statsData[$startTime] ) && intval( date("Ymd" , $_SERVER['REQUEST_TIME'] ) ) > intval( date( "Ymd" , $startTime ) ) )
					//{
						$cache->set( $statsName."_".$searchYm."_".$i , $statsData[$startTime] );
					//}
				}
				
			}
	
			//非当前月份留存档
			if( !empty( $statsData ) && intval( date("Ym" , $_SERVER['REQUEST_TIME'] ) ) > intval( date( "Ym" , $serverTime ) ) )
			{
				$cache->set( $statsName."_".$searchYm , $statsData );
			}
			//$cache->delete( $statsName."_".$searchYm );
		}
		//print_r($statsData);die;
		return $statsData;
	}
	
	/**
	 * 按登录时间查询
	 * @param unknown_type $startDay
	 * @param unknown_type $endDay
	 */
	private static function _findStatsByLoginTime( $startDay , $endDay )
	{
		$startStr = $startDay." 00:00:00";
		$endStr = $endDay." 23:59:59";
		$queryCond = array(
				'platform' => Common::getConfig( "platform" ),
				'loginTime' => array(
						'$gte' => strtotime( $startStr ),
						'$lte' => strtotime( $endStr ),
				)
		);
		
		$stats = Stats_Model::count( "userLogin" , $queryCond );
		return $stats;
	}
	
	/**
	 * 连续登录用户数 
	 * @param unknown_type $days
	 */
	private static function _findStatsByReLogin( $days )
	{
		//Stats_Model::Index( "userLogin" , array( 'loginDays' => 1  ) );
		$queryCond = array(
				'platform' => Common::getConfig( "platform" ),
				'loginDays' => $days,
		);
		$stats = Stats_Model::count( "userLogin" , $queryCond );
		return $stats;
	}
	
}
