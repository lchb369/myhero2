<?php
/**
 * Map接口
 *
 */
interface iMap
{
	/**
	 * 移动
	 * @param	array $coordinate　坐标  
	 * @return	boolean
	 */
	public function move( $coordinate );
	
	/**
	 * 获取怪物属性
	 * @param	int	$monsterPointId  怪物位置ID
	 * @param	int	$mapId		   地图ID
	 * @return	array
	 */
	public function getMonsterAttribute( $monsterPointId , $mapId = null );
	
	/**
	 * 设置怪物血量 设置怪物受伤，死亡
	 * @param	int	$monsterPointId  怪物位置ID 
	 * @param	int	$hp			  血量
	 * @return	void
	 */
	public function setMonsterHP( $monsterPointId , $hp );
	
	/**
	 * 设置复仇成功
	 * @param int	$targetUserId	对手ID	
	 * @return	void
	 */
	public function setReprisalSuccess( $targetUserId );
	
	/**
	 * 设置朋友帮我出头的残像
	 * @param	int $friendId	//好友ID
	 * @param	int $enemyId	//敌人ID
	 * @param	int $itemId	//物品ID
	 * @param	boolean $isSuccess	//是否出头成功
	 */
	public function setFriendHelpRecords( $friendId , $enemyId , $itemId , $gold , $isSuccess );
	
	/**
	 * 是否是BOSS
	 * @param　int	$monsterPointId	怪物位置ID
	 *　@return	boolean
	 */
	public function isBossMonster( $monsterPointId );
	
	/**
	 * 设置玩家任务操作
	 *
	 * @param int $actionId
	 * @param int $object
	 * @param int $value
	 * @param int $lang
	 */
	public function actionIncrement( $actionId , $object = 0 , $value = 1 );
	
	/**
	 * 增加战斗残像记录
	 * @param	int $logType	记录类型（0 => 打怪；1 => 被打劫；2 => 出头；3 => 报仇）
	 * @param	array $datas	数据
	 * 							//在打怪类型时
	 * 							monsterPointId:int,	//怪物坐标ID
	 * 							result:boolean	//战斗是否胜利
	 * 
	 * 							//在被打劫时
	 * 							lootUserId:int	//打劫者ID
	 * 							lootItemId:int	//打劫了的物品ID
	 * 							lootGold:int	//打劫了的金币
	 * 
	 * 							//当记录类型为出头时，会出现的参数
	 * 							lootUserId:int	//打劫者ID
	 * 							lootItemId:int	//打劫了的物品ID
	 * 							lootGold:int	//打劫了的金币
	 * 							helpAttackUserId:int	//出头的用户ID
	 * 
	 * 							//当记录类型为报仇时，会出现的参数
	 * 							lootUserId:int	//打劫者ID
	 * 							lootItemId:int	//打劫了的物品ID
	 * 							lootGold:int	//打劫了的金币
	 * @param	boolean	$attackResult	战斗结果
	 */
	public function addBattleLog( $logType , $datas , $attackResult );
	
	 /**
	 * 给队友播放残像
	 * @return   array
	 */
	public function playLogs();
}
		
?>