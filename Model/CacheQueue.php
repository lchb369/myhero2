<?php
class CacheQueue
{
   
    private $cache;
    private $write_id = 1;
    private $read_id = 1;

    public $name = 'name1';
    public $prefix = 'queue_';
    private $cache_prefix;
    
    
    private $rLockKey = 'queue_rlock_key';
    
    static $cache_lifetime = 36000;
    static $instance = null;
    
    
  	public static function getInstance()
  	{
  		if( $instance == null )
  		{
  			$queueConfig = array(
				'prefix' => 'cache_queue',
				'name' => 'edac_stats'
			);
  			$instance = new CacheQueue( $queueConfig );
  		}
  		return $instance;
  	}
    
  	
    protected function  __construct( $config )
    {
        if(!empty($config['name'])) $this->name = $config['name'];
        if(!empty($config['prefix'])) $this->prefix = $config['prefix'];
        $this->cache = new Memcache;
		$this->cache->connect( '127.0.0.1', 11211 );
      
        $this->init();
    }

    
    
    
    private function init()
    {
     
        $this->cache_prefix = $this->prefix.$this->name.'_';
        //队列起始ID
         $this->write_id = $this->cache->get( $this->cache_prefix.'write' );
        if( !$this->write_id )
        {
            $this->cache->add( $this->cache_prefix.'write' , 1  );
            $this->write_id =  1;
        }
        
        
        //队列结束ID
        $this->read_id = $this->cache->get($this->cache_prefix.'read');
        if( !$this->read_id )
        {
            $this->cache->add( $this->cache_prefix.'read' , 1  );
            $this->read_id =  1;
        }
    

        //echo "init write_id:{$this->write_id}\n";
        //echo "init read_id:{$this->read_id}\n";

    }

    function put($data)
    {
    	if( !$this->cache->add( $this->rLockKey , 1 , 1 ,1 ))
    	{
    		return false;
    	}
    	
        $c_id = $this->cache->increment( $this->cache_prefix.'write' , 1 );
        $c_id -= 1;
  		
        $this->cache->set($this->cache_prefix.$c_id,$data,self::$cache_lifetime);
        $this->cache->delete(  $this->rLockKey );
        
        return true;
    }
    
    
    function get()
    {
        $r_id = $this->cache->get($this->cache_prefix."read" );
        $w_id = $this->cache->get($this->cache_prefix."write" );
        $data = $this->cache->get($this->cache_prefix.$r_id);

        if(  $r_id == $w_id  && $w_id > 1 )
        {
             $this->cache->set( $this->cache_prefix.'write' , 1  );
             $this->cache->set( $this->cache_prefix.'read' , 1  );
        }
        elseif( $r_id > $w_id &&  $w_id > 1  )
        {
            $this->cache->set( $this->cache_prefix.'read' , 1  );
            $r_id = 1;
        }

        if( $r_id < $w_id )
        {
            $this->cache->increment( $this->cache_prefix.'read' , 1 );
            $this->cache->delete($this->cache_prefix.$c_id);
	  		// file_put_contents( "/tmp/log2" , $this->cache_prefix.$r_id."===>".json_encode( $data )."\n" , FILE_APPEND );
            return $data;
        }


    }
}
?>
