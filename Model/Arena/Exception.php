<?php

class Arena_Exception extends GameException
{
	/**
	 * 挑战次数不足
	 * @var	int
	 */
	const STATUS_NOT_ENOUGH_CHALLENGE_TIMES = 700;
	
	/**
	 * 挑战等级不足
	 * @var	int
	 */
	const STATUS_NOT_ENOUGH_CHALLENGE_LEVEL = 701;
	
	/**
	 * 维护中
	 * @var	int
	 */
	const STATUS_IN_UPDATING = 702;
	
}

?>
