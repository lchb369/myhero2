<?php
/**
 *  普通JJC
 * @name Normal.php
 * @author Liuchangbing
 * @since 2013-1-14
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Arena_Normal extends Logical_Abstract
{
	/**
	 * 单例对象
	 * @var	Arena_Normal[]
	 */
	protected static $singletonObjects;

	const NPC_LEVEL = 15;
	
	const CACHE_KEY_NORMAL_ENEMY_HISTORY = "arena_normal_enemy_history_";
	
	const NUMBER_NOT_IN_RANK = 9999999;
	
	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Arena_Normal
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 获得普通竞技场排名
	 * @param boolean $includeRank 是否包含排名信息，或者只有个人信息
	 * @return array
	 * */
	public function getNormalRank($includeRank = true)
	{
		$rank = $this->getNormalRankTotal();
		$returnData = array();
		
		//如果玩家为前5名，则只显示前10名信息
		$myInfo = Data_Arena_Normal::getNormalInfo( $this->userId );
		$myInfo['rank'] = $myRank = -1;
		$cnt = count($rank);
		for( $i = 0 ; $i < $cnt ; $i++ )
		{
			if($includeRank)
			{
				if($i<10)
				{
					//self::resetInfo( $rank[$i]['uid'] );
					$returnData[$i] = Data_Arena_Normal::getNormalInfo($rank[$i]['uid']);
					$returnData[$i]['rank'] = $i + 1;
				}
			}
			if($rank[$i]['uid'] == $this->userId)
			{
				$myRank = $i;
				$myInfo['rank'] = $myRank + 1;
			}
		}
		if($includeRank)
		{
			//排行榜信息显示玩家排名前后各5名的玩家以及排行榜前10名玩家的信息
			if($myRank > 5)
			{
				for( $i = 0 ; $i < 11 ; $i++ )
				{
					$idx = $myRank - 5 + $i;
					if(!$returnData[$idx] && $idx < $cnt)
					{
						//self::resetInfo( $rank[$idx]['uid'] );
						$returnData[$idx] = Data_Arena_Normal::getNormalInfo($rank[$idx]['uid']);
						$returnData[$idx]['rank'] = $idx + 1;
					}
				}
			}
			
			//添加武将信息，昵称
			$this->addDetailInfo($returnData);
			$returnData = array_merge( $returnData );
			
			return array('rankList' => $returnData , 'myInfo' => $myInfo);
		}
		else
		{
			return $myInfo;
		}
	}
	
	/**
	 * 添加武将信息，昵称
	 * @param array $enemyList 竞技场信息列表
	 * */
	public function addDetailInfo( &$enemyList , $cardsLen = 3 )
	{
		if(!empty($enemyList))
		{
			//添加武将信息
			$cardConfig = Common::getConfig( "card" );
		
			foreach($enemyList as $k => &$v)
			{
				$v = Data_Arena_Normal::getInstance( $this->userId )->formatData( $v );
				//玩家
				if( $v['uid'] > 0)
				{
					$v['type'] = $v['uid'] == $this->userId ? 'me' : 'user';
					$user_info = MakeCommand_User::userInfo( $v['uid'] );
					$v['nickname'] = $user_info['username'];
					$v['cardList'] = MakeCommand_Card::getCardsTeamDetailInfo( $v['uid'] , $cardsLen );
				}
				else
				//AI
				{
					if( !isset($enemyAIList) ) $enemyAIList = Data_Arena_Ai::getInstance( $this->userId )->getEnemyAI();
					$v['type'] = 'ai';
					$npcId = $v['uid'];
					$cardList = array();
					$nickname = '';
					
					foreach ( $enemyAIList[$npcId] as $cardId )
					{
						if($nickname == '')
						{
							$cardInfo = $cardConfig[$cardId];
							$nickname = $cardInfo['name'];
						}
							
						$cardList[] = array(
								'cardId' => $cardId,
								'level' => self::NPC_LEVEL,
								'skillLevel' => 1,
						);
					}
						
					$v +=  array(
							'nickname' => $nickname,
							'cardList' => $cardList,
					);
				}
			}
		}
	}
	
	/**
	 * 清除普通竞技场对手信息
	 * */
	public function delEnemyHistory()
	{
		$cache = Common::getCache();
		$cacheKey = self::CACHE_KEY_NORMAL_ENEMY_HISTORY.$this->userId;
		$cache->delete( $cacheKey );
	}
	
	/**
	 * 获取普通竞技场对手信息
	 * @param boolean $isRefresh 是否需要刷新对手
	 * @return array
	 */
	public function getEnemy()
	{
		$cache = Common::getCache();
		$cacheKey = self::CACHE_KEY_NORMAL_ENEMY_HISTORY.$this->userId;
		if( !$enemyList = $cache->get( $cacheKey ) )
		{
			//先从排行榜中获取
			$enemyCnt = 3;
			//$rank = $this->getNormalRankTotalExceptMyself();
			$rank = $this->getNormalRankNearBy();
			//对手不足3个
			if( count( $rank ) < $enemyCnt )
			{
				Data_Arena_Ai::getInstance( $this->userId )->initEnemyAI();
				$cache->delete( "arenaNormalRank" );
				//$rank = $this->getNormalRankTotalExceptMyself();
				$rank = $this->getNormalRankNearBy();
			}
			$enemyList = array();
			$keys = array_rand( $rank , $enemyCnt );
			foreach($keys as $k)
			{
				$enemy['uid'] = $rank[$k]['uid'];
				//$enemy['rank'] = $k + 1;
				$enemy['rank'] = -1;
				$enemyList[] = $enemy;
			}
			$this->addRank( $enemyList );
			if( !empty( $enemyList ) )
			{
				$cache->set( $cacheKey ,  $enemyList , 1800 );
			}
		}
		
		$this->addEnemyInfo( $enemyList );
		return $enemyList;
	}
	
	/**
	 * 攻击目标
	 * @param unknown $type  玩家还是，AI
	 * @param unknown $enemyId 敌人ID
	 */
	public function attack( $type , $enemyId )
	{
		$challenger = array();
		$beChallenger = array();
		//初始化，挑战者和被挑战者
		$this->_initMember( $type , $enemyId , $challenger , $beChallenger );
		
		$challengerWin = 0;
		$roundRecord = Arena_Fight::_mutiRound( $challenger , $beChallenger , $challengerWin );
		
		$returnData['challengerWin'] = $challengerWin;
		$returnData['challenger'] = $challenger;
		$returnData['beChallenger'] = $beChallenger;
		$returnData['roundRecord'] = $roundRecord['battleRecord'];
		$returnData['overMaxRound'] = $roundRecord['overMaxRound'];
		
		return $returnData;
	}
	
/**
	 * 攻击目标(测试)
	 * @param unknown $type  玩家还是，AI
	 * @param unknown $enemyId 敌人ID
	 * @param array $cCardList 挑战方阵容
	 * @param array $bCardList 被挑战方阵容
	 * @param array $isFirst 出手先后(1为挑战方先出手)
	 */
	public function attackTest( $type , $enemyId , $cCardList , $bCardList , $isFirst)
	{
		$challenger = array();
		$beChallenger = array();
		//初始化，挑战者和被挑战者
		//判断出手先后
		$beChallenger['uid'] = $enemyId;
	
		$challenger['first'] = $isFirst;
		$challenger['win'] = 0;
		$challenger['cards']  =  $this->_initEnemyCardInfo( $cCardList , $this->userId  );
		$challenger['uid'] = intval($this->userId);
		
		$beChallenger['first'] = $isFirst ? 0 : 1;
		$beChallenger['win'] = 0;
		
		$beChallenger['cards'] = $this->_initEnemyCardInfo( $bCardList , $beChallenger['uid']  );
		
		$challengerWin = 0;
		$roundRecord = Arena_Fight::_mutiRound(   $challenger , $beChallenger , $challengerWin );
		
		$returnData['challengerWin'] = $challengerWin;
		$returnData['challenger'] = $challenger;
		$returnData['beChallenger'] = $beChallenger;
		$returnData['roundRecord'] = $roundRecord['battleRecord'];
		$returnData['overMaxRound'] = $roundRecord['overMaxRound'];
	
		return $returnData;
	}
	
	
	
	/**
	 * 获取福利兑换信息
	 * @return array
	 * */
	public function getBenefitExchg()
	{
		$list = array();
		$conf = Common::getConfig( "welfare" );
		//普通福利奖励
		$myArenaInfo = Data_Arena_Normal::getNormalInfo( $this->userId );
		$benefit = intval($myArenaInfo['benefit']);
		
		if(!empty($conf[1]))
		{
			foreach($conf[1] as $k => $v)
			{
// 				if( strtotime($v['startTime']) <= $_SERVER['REQUEST_TIME'] && strtotime($v['endTime']) > $_SERVER['REQUEST_TIME'])
// 				{
					$list[] = array(
						'id' => $k,
						'type' => $v['prizeType'],
						'cost' => $v['paidWelfare'],
						'card' => $v['prizeCard'],
						'num' => $v['prizeNum'],
						'desc' => $v['prizeDesc'],
						'enable' => $benefit >= $v['paidWelfare'] ? 1 : 0,
					 );
// 				}
			}
		}
		
		return array("bonusList" => $list,"benefit" => $benefit);
	}
	
	/**
	 * 福利兑换
	 * @param int $type 兑换类型
	 * @param int $id 兑换编号
	 * @return array
	 * */
	public function benefitExchg($type, $id)
	{
		$ret = array("succ" => 0 , "error" => "incorrect params");
		
		if(!empty($type) && !empty($id))
		{
			$conf = Common::getConfig( "welfare" );
			if(!!$conf = $conf[$type][$id]){
				$myArenaInfo = Data_Arena_Normal::getNormalInfo( $this->userId );
				$benefit = intval($myArenaInfo['benefit']);
				
// 				if( strtotime($conf['startTime']) <= $_SERVER['REQUEST_TIME'] && strtotime($conf['endTime']) > $_SERVER['REQUEST_TIME'])
// 				{
					//普通福利兑换
					if($type == 1)
					{
						if($benefit >= $conf['paidWelfare'])
						{
							if(!!$succ = $this->sendBonus($conf))
							{
								//扣除福利
								$benefit -= $conf['paidWelfare'];
								Data_Arena_Normal::setNormalInfo( $this->userId , array(
									'benefit' => $benefit 
								));
								$ret = array(
									"succ" => 1 ,
									"benefit" => $benefit ,
									"user_info" => MakeCommand_User::userInfo( $this->userId ),
								);
							}
						}
						else
						{
							$ret['error'] = "not enough benefit";
						}
					}
// 				}
// 				else
// 				{
// 					$ret['error'] = "invaild exchg info";
// 				}
			}
		}
		
		return $ret;
	}
	
	/**
	 * 获取排行奖励信息
	 * @param int $rankType 排行类型 1普通, 2精英
	 * @return array
	 * */
	public function rankBonusInfo( $rankType )
	{
		$list = array();
		$conf = Common::getConfig( "welfare" );
		
		if(!empty($conf[2]))
		{
			//获得上周的排行信息
			$curMon = Helper_Date::getWeekStartTime();
			$lastMon = $curMon - 7 * 86400;
			
			//获得我的领取信息
			$myArenaInfo = Data_Arena_Normal::getNormalInfo( $this->userId );
			$myBonusHis = $this->getMyBonusHis( $myArenaInfo , $rankType );
			
			//获得我的排行信息
			$rankResult = Data_Arena_Rank::getInstance( $this->userId)->getRankResult( $rankType , $lastMon , $this->userId );
			$myRank = $rankResult['rank'];
			
			foreach($conf[2] as $k => $v)
			{
				if( $v['rankType'] == $rankType )
				{
					$enable = $isGet = false;
					
					//判断是否符合领取条件
					if( $this->isInRank( $myRank , $v['rank'] ) && $this->isInLevel() )
					{
						$enable = true;
					}
					
					//排行奖励每周只能领一次
					//判断条件：有该项领取记录 且 领取时间大于等于结算时间(这周一10点我去)
					if( !empty( $myBonusHis[$k]['t'] ) && $myBonusHis[$k]['t'] >= $curMon + 10 * 3600 )
					{
						$isGet = true;
					}
					$list[] = array(
							'id' => $k,
							'rankType' => $v['rankType'],
							'rank' => $v['rank'],
							'type' => $v['prizeType'],
							'card' => $v['prizeCard'],
							'num' => $v['prizeNum'],
							'prizeDesc' => $v['prizeDesc'],
							'desc' => $v['desc'],
							'enable' => $enable ? 1 : 0,
							'isGet' => $isGet ? 1 : 0,
					);
				}
			}
		}
		
		return array("bonusList" => $list);
	}
	
	/**
	 * 领取排行奖励
	 * @param int $type 类型
	 * @param int $id 编号
	 * @return array
	 * */
	public function getRankBonus( $type , $id )
	{
		$ret = array("succ" => 0 , "error" => "incorrect params");

		if(!empty($type) && !empty($id))
		{
			$conf = Common::getConfig( "welfare" );
			if(!!$conf = $conf[$type][$id]){
				$rankType = $conf['rankType'];
				
				//获得上周的排行信息
				$curMon = Helper_Date::getWeekStartTime();
				$lastMon = $curMon - 7 * 86400;
					
				//获得我的领取信息
				$myArenaInfo = Data_Arena_Normal::getNormalInfo( $this->userId );
				$myBonusHis = $this->getMyBonusHis( $myArenaInfo , $rankType );
				//获得我的排行信息
				$rankResult = Data_Arena_Rank::getInstance( $this->userId )->getRankResult( $rankType , $lastMon , $this->userId );
				$myRank = $rankResult['rank'];
				
				$enable = $isGet = false;
					
				//判断是否符合领取条件
				if( $this->isInRank( $myRank , $conf['rank'] ) && $this->isInLevel() )
				{
					$enable = true;
				}
					
				//排行奖励每周只能领一次
				//判断条件：有该项领取记录 且 领取时间大于等于结算时间(这周一10点我去)
				//如果是周一10点前领取则算上周一10点
				$deadline = $curMon + 10 * 3600;
				if( $_SERVER['REQUEST_TIME'] >= $curMon && $_SERVER['REQUEST_TIME'] < $curMon + 10 * 3600)
				{
					$deadline = $lastMon + 10 * 3600;
				}
				if( !empty( $myBonusHis[$id]['t'] ) && $myBonusHis[$id]['t'] >=  $deadline )
				{
					$isGet = true;
				}
				
				if(!$enable)
				{
					$ret['error'] = "条件不符合";
				}
				elseif($isGet)
				{
					$ret['error'] = "已经领取过";
				}
				else
				{
					//排行奖励
					if($type == 2)
					{
						if(!!$succ = $this->sendBonus($conf))
						{
							//添加领取记录
							$myBonusHis[$id]['t'] = $_SERVER['REQUEST_TIME'];
							
							Data_Arena_Normal::setNormalInfo( $this->userId , array(
								$this->_getRankTypeStr( $rankType )."Bonus" => json_encode( $myBonusHis ),
							));
							
							$ret = array(
								"succ" => 1,
								"user_info" => MakeCommand_User::userInfo( $this->userId ),
							);
						}
					}
				}
			}
		}
		
		return $ret;
	}
	
	/**
	 * 判断时间是否在维护时间中
	 * @return boolean
	 */
	public function isInUpdating()
	{
		$closeTime = Helper_Date::getWeekStartTime() + 10 * 3600;
		if( $closeTime <= $_SERVER['REQUEST_TIME'] && $_SERVER['REQUEST_TIME'] < $closeTime + 3600 )
		{
			return true;
		}
		return false;
	}
	
	/**
	 * 判断等级
	 */
	public function isInLevel()
	{
		//判断等级
		$userLvl = User_Info::getInstance( $this->userId )->getLevel();
		$lvlLimit = 5;
		if( $userLvl < $lvlLimit )
		{
			return false;
		}
		return true;
	}
	
	/**
	 * 每周定时任务
	 *
	 * */
	public function taskWeekly()
	{
		//保存上周竞技场结果
		$curMon = Helper_Date::getWeekStartTime();
		if( $_SERVER['REQUEST_TIME'] >= $curMon + 10 * 3600 )
		{
			$lastMon = $curMon - 7 * 86400;
			$rankResult = Data_Arena_Rank::getInstance( $this->userId )->getRankResult( 0 , $lastMon );
			if( empty( $rankResult ) )
			{
				$normalRank = $this->getNormalRankTotal();
				//保存普通竞技场排行
				Data_Arena_Rank::getInstance( $this->userId )->setRankResult( 1 , $lastMon, $normalRank);
				
				$dbEngine = Common::getDB( $this->userId );
				ini_set( 'memory_limit' , '512M' );
				ini_set( "max_execution_time", "600" );
				
				//清除积分缓存
				$sql = "select uid from `arena_normal` where uid>0";
				$uids = $dbEngine->findQuery( $sql );
				
				//初始化每周积分
				$sql = "update `arena_normal` set normalScore=0 where uid>0";
				$dbEngine->query( array( $sql ) );
		
				$cache = Common::getCache();
				foreach($uids as $v)
				{
					$cache->delete( $v[0]."_arena_normal" );
				}
				
				//清除竞技场排行缓存
				$cache->delete( "arenaNormalRank" );
			}
		}

	}
	
	/**
	 * 重置每周信息
	 */
	public static function resetInfo( $uid )
	{
		$myInfo = Data_Arena_Normal::getNormalInfo( $uid );
		$closeTime = Helper_Date::getWeekStartTime() + 10 * 3600;
		
		//重置每周积分
		if( $_SERVER['REQUEST_TIME'] > $closeTime && $myInfo['normalDate'] < $closeTime )
		{
			$setData['normalDate'] = $_SERVER['REQUEST_TIME'];
			$setData['normalScore'] = 0;
			Data_Arena_Normal::setNormalInfo( $uid , $setData );
			//重置挑战对手
			Arena_Normal::getInstance( $uid )->delEnemyHistory();
		}
	}
	
	/**
	 * 获得跟我积分最接近的前后各15个人，取3个
	 * */
	private function getNormalRankNearBy()
	{
		$arenaData = Data_Arena_Normal::getNormalInfo( $this->userId );
		$dbEngine = Common::getDB( $this->userId );
		$sql = "( select `uid` from `arena_normal` where `level`>=10 and `normalScore`>=".$arenaData['normalScore']." and `uid`<>".$this->userId." order by `normalScore` asc limit 15 )";
		$sql .= " union all ";
		$sql .= "( select `uid` from `arena_normal` where `level`>=10 and `normalScore`<".$arenaData['normalScore']." order by `normalScore` desc limit 15 )";

		$rank = $dbEngine->findQuery( $sql );
		
		foreach($rank as &$v)
		{
			$v = array("uid"=>$v[0]);
		}
		return $rank;
	}
	
	/**
	 * 获得普通竞技场排名总值(除我之外)
	 * @return array
	 * */
	private function getNormalRankTotalExceptMyself()
	{
		$rank = $this->getNormalRankTotal();
		//剔除自己
		$myIdx = -1;
		foreach($rank as $k => $v)
		{
			if($v['uid'] == $this->userId)
			{
				$myIdx = $k;
				break;	
			}
		}
		if($myIdx > -1) array_splice($rank , $myIdx , 1);
		return $rank;
	}
	
	/**
	 * 获得普通竞技场排名总值
	 * @return array
	 * */
	private function getNormalRankTotal()
	{
		//get cache
		$cache = Common::getCache();
		$key = "arenaNormalRank";
		if(!$rank = $cache->get($key))
		{
			//save db and cache while cache no found
			//只计算前1000名玩家
			$sql = "select `uid` from `arena_normal` where `level`>=10 and `normalScore`>0 order by `normalScore` desc,`benefit` desc,`level` desc,`uid` asc limit 1000";
			$dbEngine = Common::getDB( $this->userId );
			$rank = $dbEngine->findQuery( $sql  );
			foreach($rank as &$v)
			{
				$v = array("uid"=>$v[0]);
			}
			if( !empty( $rank ) )
			{
				$cache->set( $key , $rank , 120);
			}
		}
		return $rank;
	}
	
	/**
	 * 添加对手详细信息
	 * @param unknown $enemyList
	 */
	private function addEnemyInfo( &$enemyList , $cardsLen = 3 )
	{
		if( !empty( $enemyList ) )
		{
			foreach( $enemyList as &$enemy)
			{
				//self::resetInfo( $enemy['uid'] );
				$enemy += Data_Arena_Normal::getNormalInfo( $enemy['uid'] );
			}
			
			$this->addDetailInfo( $enemyList , $cardsLen );
		}
	}
	
	/**
	 * 添加排行信息
	 * @param array $enemyList
	 * @return array
	 */
	private function addRank( &$enemyList )
	{
		if( !empty( $enemyList ) )
		{
			$rankTotal = $this->getNormalRankTotal();
			foreach( $rankTotal as $k=>$rank )
			{
				foreach( $enemyList as &$enemy )
				{
					if( $enemy['uid'] == $rank['uid'])
					{
						$enemy['rank'] = $k + 1;
						break;
					}
				}
			}
		}
	}
	
	/**
	 * 发送奖励
	 * @param array $conf 奖励配置
	 * @return boolean 
	 * */
	private function sendBonus($conf)
	{
		$ret = false;
		//卡片奖励
		if($conf['prizeType'] == 1)
		{
			for( $i = 0 ; $i < $conf['prizeNum'] ; $i++)
			{
				Card_Model::getInstance( $this->userId )->addCard( $conf['prizeCard']  , 1 , true , "arenaBonus" );
			}
			$ret = true;
		}
		//金币奖励
		else if($conf['prizeType'] == 2)
		{
			if($conf['prizeNum'] > 0)
			{
				User_Info::getInstance( $this->userId )->changeGold( $conf['prizeNum'] , '福利兑换' );
				$ret = true;
			}
		}
		return $ret;
	}
	
	private function _getRankTypeStr( $rankType )
	{
		$ret = 0;
		switch ($rankType)
		{
			case 1:
				$ret = "normal";
			break;
			case 2:
				$ret = "advanced";
			break;
		}
		return $ret;
	}
	
	/**
	 * 获得我的领取历史
	 * @param array $data 竞技场数据
	 * @param int $rankType 排名类型 1普通 , 2精英
	 * @return array
	 * */
	private function getMyBonusHis( $data , $rankType )
	{
		$rankTypeStr = $this->_getRankTypeStr( $rankType )."Bonus";
		$myBonusHis = empty( $data[$rankTypeStr] ) ? array() : json_decode( $data[$rankTypeStr] , true );
		
		return $myBonusHis;
	}
	
	/**
	 * 判断用户是否在排行设置中
	 * @param int $rank 排行
	 * @param string $rankConf 排行设置
	 * @return boolean
	 * */
	private function isInRank( $rank , $rankConf)
	{
		$enable = false;

		if( $rank > -1 )
		{
			if( intval( $rankConf ) == 0 )
			{
				$enable = true;
			}
			else
			{
				$rankRange = explode( "," , $rankConf );
				if( count( $rankRange ) == 2 )
				{
					if( $rank >= $rankRange[0] && $rank <= $rankRange[1] )
					{
						$enable = true;
					}
				}
				elseif( count( $rankRange ) == 1 )
				{
					if( $rank == $rankRange[0] )
					{
						$enable = true;
					}
				}
			}
		}
		return $enable;
	}
	
	/**
	 * 初始化攻击者和防守者数据
	 * @param unknown $type
	 * @param unknown $enemyId
	 * @param unknown $attacker 发起挑战方为挑战方，敌人为防守方，和出手先后无关
	 * @param unknown $defender
	 */
	private function _initMember(  $type , $enemyId , &$challenger ,& $beChallenger  )
	{
		$checkEnemy = 0;
		
		$cache = Common::getCache();
		if( !!$enemyList = $cache->get( self::CACHE_KEY_NORMAL_ENEMY_HISTORY.$this->userId  ))
		{
			$this->addEnemyInfo( $enemyList , 5 );
			//检查传过来的对手信息是否有效
			foreach ( $enemyList as $enemyInfo )
			{
				if( $enemyInfo['type'] == $type && $enemyInfo['uid'] == $enemyId )
				{
					$cardList = $enemyInfo['cardList'];
					$checkEnemy = 1;
				}
			}
		}
		
		if( $checkEnemy == 0 )
		{
			throw new Battle_Exception( Battle_Exception::STATUS_NOT_BATTLE_INFO );
		}
		
		//判断出手先后
		$isFirst = 1;
		$userLvl = Data_User_Info::getInstance( $this->userId )->getLevel();
		
		if( $type == "user" )
		{
			$beChallenger['uid'] = intval($enemyId);
			$enemyLvl = Data_User_Info::getInstance( $enemyId )->getLevel();
		}
		else 
		{
			//-1表示AI怪物
			$beChallenger['uid'] = -1;
			$enemyLvl = self::NPC_LEVEL;
		}
		
		if( $userLvl < $enemyLvl )
		{
			$isFirst = 0;
		}
		
		$challenger['first'] = $isFirst;
		$challenger['win'] = 0;
		$challenger['cards']  =  $this->_initUserCardInfo();
		$challenger['uid'] = intval($this->userId);
		
		$beChallenger['first'] = $isFirst ? 0 : 1;
		$beChallenger['win'] = 0;
		
		$beChallenger['cards'] = $this->_initEnemyCardInfo( $cardList , $beChallenger['uid']  );
	}
	
	/**
	 * 设置调用者武将卡信息
	 */
	private function _initUserCardInfo()
	{
		$cardTeam = Data_Card_Team::getInstance( $this->userId )->getData();

		$cardConfig = Common::getConfig( "card" );
		$cardArr = explode( ",",   $cardTeam['cards'] );
		$cardList = array();
		$key = 1;
		foreach ( $cardArr as $cardId )
		{
			if( !$cardId || count( $cardList ) >= 5 )
			{
				continue;
			}
			$cardInfo = Data_Card_Model::getInstance( $this->userId )->getCardInfo( $cardId );
			$cardConf = $cardConfig[$cardInfo['cardId']];
		
			$cardList[$key] = array(
					'uid' => intval($this->userId),
					'id' => $key,
					'cardId' => $cardInfo['cardId'],
					'level' => intval($cardInfo['level']),
					'skillLevel' => intval($cardInfo['skillLevel']),
			);
			$this->_addCardBattleAttr($cardConf, $cardList[$key]);
			$key++;
		}
		//我的武将信不能为空
		if( empty( $cardList ) )
		{
			throw new Battle_Exception( Battle_Exception::STATUS_NOT_BATTLE_INFO );
		}
		return $cardList;
	}
	
	/**
	 * 初始化对手武将卡信息，不管是人还是怪
	 * @param unknown $cardList
	 */
	private function _initEnemyCardInfo(  $cardList , $uid  )
	{
			$cardConfig = Common::getConfig( "card" );
			$returnList = array();
			foreach ( $cardList as $key =>  $cardInfo )
			{
				$cardConf = $cardConfig[$cardInfo['cardId']];
				$cardInfo['uid'] = intval($uid);
				$cardInfo['id'] = $key+1;
				$this->_addCardBattleAttr($cardConf, $cardInfo);
				$returnList[$key+1] = $cardInfo;
			}
			return $returnList;
	}

	/**
	 * 增加武将战斗属性
	 * @param array $cardConf 武将卡配置
	 * @param array $cardInfo 武将信息
	 * */
	private function _addCardBattleAttr( $cardConf , &$cardInfo )
	{
		$battleConf = Common::getConfig( "common" );
		$cardInfo['maxHp'] = $cardInfo['hp']  =  intval( pow( $cardConf['hp'] + ( $cardInfo['level'] - 1 ) * $cardConf['hp_growth'] , intval($battleConf['hpCoefficient1']) ) / floatval($battleConf['hpCoefficient2']) * floatval($battleConf['hpCoefficient3']) );
		$cardInfo['attack'] = intval( pow(  $cardConf['attack'] + ( $cardInfo['level'] - 1 ) * $cardConf['attack_growth'] , intval($battleConf['atkCoefficient1']) ) / floatval($battleConf['atkCoefficient2']) * floatval($battleConf['atkCoefficient3']) );
		$cardInfo['defense'] = intval( pow(  $cardConf['recover'] + ( $cardInfo['level'] - 1 ) * $cardConf['recover_growth'] ,  intval($battleConf['defCoefficient1']) ) / floatval($battleConf['defCoefficient2']) * floatval($battleConf['defCoefficient3']) );
		$cardInfo['crit'] = ceil( $cardInfo['defense'] * floatval($battleConf['criCoefficient1']) / floatval($battleConf['criCoefficient2']) );
		$cardInfo['dodge'] = ceil( $cardInfo['defense'] * floatval($battleConf['dodCoefficient1']) / floatval($battleConf['dodCoefficient2']) );
		$cardInfo['ctype'] = intval($cardConf['ctype']);
		//技能属性
		$cardInfo['skid'] = intval($cardConf['skid']);
	}
	
}