<?php
/**
 * 竞技场战斗逻辑
 * @name Fight.php
 * @since 2013-08-15
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Arena_Fight
{

	const SK_TYPE_ATK = 1;				//攻击系:atk
	const SK_TYPE_GRAVITY = 2;			//重力:atk
	const SK_TYPE_DELAY = 3;			//加回合:atk
	const SK_TYPE_BREAK = 4;			//破甲:atk
	const SK_TYPE_POISON = 5;			//毒:atk
	const SK_TYPE_ATK_DOWN = 11;		//降攻:atk
	const SK_TYPE_RECOVER = 6;			//回复:ref
	const SK_TYPE_ARMOR = 7;			//免伤:ref
	const SK_TYPE_ATK_UP = 12;			//加攻:ref
	const SK_TYPE_ATK_BACK = 13;			//反弹伤害:ref
//	const SK_TYPE_STONETRANS = 8;		//变宝石
//	const SK_TYPE_STONERAND = 9;		//宝石刷新
//	const SK_TYPE_LUANSHI = 10;			//乱世
	
	const SK_ID_GUAGULIAOSHANG = 59; //刮骨疗伤
	const SK_ID_JUEDIFANJI = 94; //絕地反擊:下一次受到敌方伤害不会使你阵亡，并且有机率反弹多倍伤害(反弹10倍伤害)

	const ATK_TYPE_NORMAL = 0;	//一般攻击
	const ATK_TYPE_SKILL = 1;	//技能攻击
	const ATK_TYPE_BUFF = 2;	//buff攻击
	const ATK_TYPE_DMG_BACK = 3; //伤害反弹
	
	/**
	 * 挑战者和被挑战者
	 * 多回合攻击过程
	 * @param unknown $challenger
	 * @param unknown $beChallenger
	 */
	public static function _mutiRound(   &$challenger , &$beChallenger , & $challengerWin  )
	{
			//round 为回合数，A打B，与B打A各算一回合
			$round  = 0;	
			
			//初始化攻击方和防守方
			if( $challenger['first'] == 1 )
			{
				$attacker = &$challenger;
				$defender = &$beChallenger;
			}
			else 
			{
				$attacker = &$beChallenger;
				$defender = &$challenger;
			}
		
			//挑战方胜利,也就是我玩家胜利
			$battleRecord = array();
			$overMaxRound = 0;
			
			//终止循环的条件是，攻击方和被攻击方有一位胜出win=1
			while(  1 )
			{
				//每回合的,攻击方的1.3.5攻击对手，对手的2.4反击
				$roundInfo = array();
				//每回合双方共进行五次攻击
				for ( $i = 1 ; $i <= 5 ; $i++ )
				{
					
					if( $i == 1 || $i == 3 || $i==5 )
					{
						//由攻击方攻击
						//判断攻击方是否可打，如果攻击方血为0或不存在，则不能出手
						//判断被攻方是否可被打,如果被攻击方血为0,或不存在，要重新选择一个打
						$atkCards = &$attacker['cards'];
						$defCards = &$defender['cards'];
					}
					else 
					{
						//由被攻击方攻击
						$atkCards = &$defender['cards'];
						$defCards = &$attacker['cards'];
					}
					
					$record = self::_singleRound( $atkCards , $defCards , $i , $round );
					
					if( $record &&  $record['attackerWin'] != 1 )
					{
						$roundInfo[$i] = $record;
					}
					
					if( $record['attackerWin'] == 1 )
					{
						//判断胜负
						$challengerWin = self::_isChallengerWin( $attacker , $defender , $challenger['uid']);
						$attacker['win'] = 1;
						 break;					 
					}
				}
				
				//计算cd
				self::_calcCD( $attacker['cards'] , $round );
				self::_calcCD( $defender['cards'] , $round );
				
				if( $roundInfo ) $battleRecord[] = $roundInfo;
				
				if( $attacker['win'] == 1 ) break;
			
				//攻击方和被攻击方对调位置
				$defenderTmp = &$defender;
				$defender = &$attacker;
				$attacker = &$defenderTmp;
				
				if(  $round++ >= 30 )
				{
					//判断胜负
					$challengerWin = self::_isChallengerWin( $attacker , $defender , $challenger['uid']);
					$overMaxRound = 1;
					break;
 				} 
	
			}
			return array( "battleRecord" => $battleRecord , "overMaxRound" => $overMaxRound);
	}

	/**
	 * 根据剩余血量获得挑战者是否胜利
	 * @param array $attackerCards
	 * @param array $defenderCards
	 * @param int $challengerUid
	 * @param boolean $challengerWin
	 * @return 
	 * */
	private static function _isChallengerWin( &$attacker , &$defender , $challengerUid )
	{
		$challengerWin = 0;
		//计算双方总血量
		$aHaveHp = self::_getCardsTotalHp( $attacker['cards'] );
		$dHaveHp = self::_getCardsTotalHp( $defender['cards'] );
			
		if( ( $aHaveHp > $dHaveHp &&  $attacker['uid'] == $challengerUid )
		|| ( $dHaveHp > $aHaveHp &&  $defender['uid'] == $challengerUid ) )
		{
			$challengerWin = 1;
		}
		return $challengerWin;
	}
	
	private static function _getCardsTotalHp( &$cards )
	{
		$hp = 0;
		foreach(  $cards as &$card )
		{
			$hp += $card['hp'];
		}
		return $hp;
	}

	/**
	 * 一对一攻击
	 * @param unknown $aCard 攻击卡
	 * @param unknown $dCard	被攻击卡
	 * @param int $i 攻击方武将索引
	 * @param ini $round 当前回合
	 */
	private static function _singleRound( & $attackerCards , & $defenderCards , $i , $round )
	{
		if(!self::_canAttack($attackerCards[$i])) return array();
		
		$aCard = &$attackerCards[$i];
		//获取$dCard,如果对应的目标死了，或者不存在先在前面找，再后面找，找不到就说明，对手已经战败
		$dCard = self::_getDefenderCard( $defenderCards , $i );
		if( empty( $dCard ) ) return array( 'attackerWin' => 1 );
		$dCard = &$defenderCards[$dCard['id']];
		
		$skillId = 0;
		//a -- attacker     d--defender
		$record  = array(
				'attackerUid' => intval($aCard['uid']),
				'aid' => $aCard['id'],	//攻击武将ID
				'did' => $dCard['id'],//被攻击武将ID
				'damage' => 0,	//造成的伤害
				'aHp' => $aCard['hp'],	//攻击方当前血量
				'bHp' => $dCard['hp'],	//被攻击方当前血量
				'status' => 0, //1,爆击,2闪避,默认为0,-1为技能
				'skillId' => $skillId,	//是否使用了技能,技能ID
		);
		
		$buffRoundInfo = self::_calcBuff($aCard, $dCard, $attackerCards, $defenderCards, $record);
		if($buffRoundInfo['rc']) return array();

		//看我挂了没
		if(!self::_canAttack($aCard)) return $record;

		if($aCard['skid'] > 0)
		{
			$skillConfigs = Common::getConfig( "upgradeSkill" );
			$skillConfig = $skillConfigs[$aCard['skid']];
			if(!empty($skillConfig['initCd']) && $round > $skillConfig['initCd'] - 1 && (int)$aCard['cd'] == 0)
			{
				$skillId = $aCard['skid'];
			}
		}

		//开始放技能
		if($skillId)
		{
			$aCard['cd'] = $skillConfig['cd'];
			
			$record['did'] = 0;
			$record['bHp'] = 0;
			$record['status'] = -1;
			$record['skillId'] = $skillId;
			
			$dIds = self::getTargetByType($defenderCards, $skillConfig['target'], $dCard['id']);			
			$aIds = self::getTargetByType($attackerCards, $skillConfig['tarSelf'], $aCard['id']);
			
			/**
			    10	enManSkillTp_luanshi        //乱世  目前没有
			 */
			
			//作用于敌方的效果
			foreach($dIds as $dId)
			{
				//攻击系
				if($skillId == Arena_Fight::SK_ID_GUAGULIAOSHANG 
				|| in_array($skillConfig['type'] , array(
					Arena_Fight::SK_TYPE_ATK, 
					Arena_Fight::SK_TYPE_GRAVITY,
					Arena_Fight::SK_TYPE_DELAY,)))
				{
					//技能伤害算防御,属性
					self::_calcDamage($skillId, $aCard, $defenderCards[$dId], $attackerCards, $defenderCards, $record, Arena_Fight::ATK_TYPE_SKILL);
					//看我挂了没
					if(!self::_canAttack($aCard)) break;
				}
				
				//上buff:atk
				if($skillConfig['duration'] > 0 && in_array($skillConfig['type'] , array(
					Arena_Fight::SK_TYPE_ATK, 
					Arena_Fight::SK_TYPE_GRAVITY,
					Arena_Fight::SK_TYPE_DELAY,
					Arena_Fight::SK_TYPE_BREAK,
					Arena_Fight::SK_TYPE_POISON,
					Arena_Fight::SK_TYPE_ATK_DOWN,
				)))
				{
					$record['def'][$dId]['buff'] = $skillId;
					//持续时间
					$defenderCards[$dId]['buff'][$skillId]['dur'] = $skillConfig['duration'] + 1;
					//施法对象
					$defenderCards[$dId]['buff'][$skillId]['aId'] = $aCard['id'];
				}
			}
			
			//作用于我方的效果
			foreach($aIds as $aId)
			{
				//6	enManSkillTp_recover,       //回复
				if(in_array($skillConfig['type'] , array(Arena_Fight::SK_TYPE_RECOVER)))
				{
					//固定數值作爲基數
					$damage = $skillConfig['effectBase'] * $skillConfig['effect'];
					//6: 玩家回复力
					//默认施法者的恢复力
					if((int)$skillConfig['effectBase'] == 6)
					{
						//$damage = intval($attackerCards[$aId]['defense'] * $skillConfig['effect']);
						$damage = intval($attackerCards[$aCard['id']]['defense'] * $skillConfig['effect']);
					}
					
					$record['atk'][$aId]['heal'] = $damage;
					$attackerCards[$aId]['hp'] += $damage;
					$attackerCards[$aId]['hp'] = min($attackerCards[$aId]['maxHp'], $attackerCards[$aId]['hp']);
					$record['atk'][$aId]['hp'] = $attackerCards[$aId]['hp'];
				}
				
				//上buff:ref
				if($skillConfig['duration'] > 0 && in_array($skillConfig['type'] , array(
					Arena_Fight::SK_TYPE_RECOVER, 
					Arena_Fight::SK_TYPE_ARMOR,
					Arena_Fight::SK_TYPE_ATK_UP,
					Arena_Fight::SK_TYPE_ATK_BACK,
				)))
				{
					$record['atk'][$aId]['buff'] = $skillId;
					//持续时间
					$attackerCards[$aId]['buff'][$skillId]['dur'] = $skillConfig['duration'] + 1;
					
					//绝地反击持续时间单独计算
					if($skillId == Arena_Fight::SK_ID_JUEDIFANJI) $attackerCards[$aId]['buff'][$skillId]['dur']--;
					
					//施法对象
					$attackerCards[$aId]['buff'][$skillId]['aId'] = $aCard['id'];
				}
			}	
			
		}
		//普通攻击
		else 
		{
			//计算造成的伤害
			self::_calcDamage($skillId, $aCard, $dCard, $attackerCards, $defenderCards, $record, Arena_Fight::ATK_TYPE_NORMAL);
		}
		
		$record['ownBuff'] = $aCard['buff'];
		
		return $record;
		
	}
	
	/**
	 * 计算属性相克比例:
	 * 1水,2火,3木,4光,5暗
	 * 水->火->木->水, 光<->暗
	 * 返回 相克2 ,没有相克关系1,被克0.5
	 * @param unknown_type $aCardType
	 * @param unknown_type $bCardType
	 */
	private static function _calcElementRate( $aCardType , $bCardType)
	{
		$ret = 1;
		if($aCardType == -1) return $ret;
		
		//分隔编号之前依次相克, 之后的互克
		$splitIdx = 3;
		if($aCardType > $splitIdx && $bCardType > $splitIdx && $aCardType != $bCardType)
		{
			$ret = 2;
		}
		if($aCardType <= $splitIdx && $bCardType <= $splitIdx)
		{
			if($aCardType - $bCardType == -1
			|| ($aCardType == $splitIdx && $bCardType == 1))
			{
				$ret = 2;
			}
			elseif($aCardType - $bCardType == 1
			 || ($aCardType == 1 && $bCardType == $splitIdx))
			{
				$ret = 0.5;
			}
		}
		return $ret;
	}
	
	/**
	 * 刷新武将技能cd , buff
	 * Enter description here ...
	 * @param unknown_type $cards
	 * @param unknown_type $round
	 */
	private static function _calcCD( & $cards , $round )
	{
		//$skillConfigs = Common::getConfig( "upgradeSkill" );
		foreach($cards as &$card)
		{
			if(isset($card['cd']) && $card['cd'] > 0 )
			{
				$card['cd']--;
			}
			if(isset($card['buff']))
			{
				$t_buff = array();
				foreach($card['buff'] as $buffId => &$buff)
				{
					//$skillConfig = $skillConfigs[$buffId];
					//绝地反击在收到伤害时才减持续时间
					if($buffId != Arena_Fight::SK_ID_JUEDIFANJI) $buff['dur']--;
					if($buff['dur'] > 0) $t_buff[$buffId] = $buff;
				}
				$card['buff'] = $t_buff;
			}
		}
	}
	
	/**
	 * 根据效果基础值计算效果值
	 * 小数部分代表元素代码,属性代码1-5,6表示全部
	 * eg:3.25,3.5
	 * @param unknown_type $effectBase
	 * @param unknown_type $aCards
	 * @param unknown_type $dCards
	 * @param unknown_type $col
	 */
	private static function _calcEffectBaseByElement($effectBase, &$aCards, &$dCards)
	{
		$effect = 0;
		$col = 'attack';
		if((int)$effectBase == 8) $col = 'maxHp';
		
		$effectBaseArr = explode(".", strval($effectBase));
		
		if(count($effectBaseArr) > 1)
		{
			$elementCode = $effectBaseArr[1];
			
			foreach($dCards as &$_card)
			{
				if($elementCode === '6' 
				|| strpos($elementCode, strval($_card['ctype'])) !== false
				)
				{
					$effect += $_card[$col];
				}
			}
		}
		return $effect;
	}
	
	/**
	 * 获取被攻击者卡
	 * @param unknown $defenderCards
	 * @param unknown $i
	 */
	private static function _getDefenderCard( &$defenderCards , $cardId  )
	{
			if( $defenderCards[$cardId]['hp'] > 0  )
			{
				return  $defenderCards[$cardId];
			}
			
			//如果这个位不能被打，那么看另外4个是不是可以打
			for( $i = 1 ; $i < $cardId ; $i++ )
			{
				if( $cardId-$i > 0 &&   $defenderCards[$cardId-$i]['hp']  > 0  )
				{
					return  $defenderCards[$cardId-$i];
				}
			}
			
			for( $i = 1 ; $i < 5 ; $i++ )
			{
				if( $cardId+$i <= 5 &&   $defenderCards[$cardId+$i]['hp']  > 0  )
				{
					return  $defenderCards[$cardId+$i];
				}
			}
			//到这一步，说明被攻击方，已经全军覆没了
			return array();
	}
	/**
	 * 攻击方是否可以攻击
	 * @param unknown $attackerInfo
	 */
	private static function _canAttack(  $attackerInfo )
	{
		return $attackerInfo['hp'] > 0;
	}
	
	/**
	 * 计算伤害值
	 * Enter description here ...
	 */
	private static function _calcDamage($skillId, &$aCard, &$dCard, &$aCards, &$dCards, &$record, $atkType, $addConf = null)
	{
		do{
			$status = 0;
			$damage = 0;
			
			$ignoreDef = false;
			$ignoreAtk = false;
			$cannotdie = false;
			
			$atk = $aCard['attack'];
			$def = $dCard['defense'];
			
			$skillConfigs = Common::getConfig( "upgradeSkill" );
			
			if($skillId == 0)
			{
				//2,是否躲闪了
				if( rand( 1, 100 ) <= $dCard['dodge'] )
				{
					$status = 2;
					break;
				}
				
				//3，如果命中,是否爆击
				if( rand( 1, 100 )  <= $aCard['crit'] )
				{
					$atk *= 2;
					$status = 1;
				}
				
				$atk *= self::_calcElementRate($aCard['ctype'] , $dCard['ctype']);
			}
			//技能
			else
			{
				$skillConfig = $skillConfigs[$skillId];
				//固定數值作爲基數
				$atk = $skillConfig['effectBase'] * $skillConfig['effect'];
				
				//1	enManSkillTp_atk,           //攻击-atk:卡牌攻击力|常数
				if($skillConfig['type'] == Arena_Fight::SK_TYPE_ATK)
				{
					//1: 卡牌攻击力
					if((int)$skillConfig['effectBase'] == 1)
					{
						$atk = $aCard['attack'] * $skillConfig['effect'];
					}
					
					$atk *= self::_calcElementRate($aCard['ctype'] , $dCard['ctype']);
				}
				//2	enManSkillTp_gravity,       //重力-atk:敌方hp:无视防御
				//59 刮骨疗伤HP最少的武将血量回复发动技能武将回复力7倍的血量，并减少对方全体10%的血量
				elseif($skillId == Arena_Fight::SK_ID_GUAGULIAOSHANG 
					|| $skillConfig['type'] == Arena_Fight::SK_TYPE_GRAVITY) 
				{
					if($skillId == Arena_Fight::SK_ID_GUAGULIAOSHANG)
					{
						$skillConfig['effect'] = 0.1;
						$skillConfig['effectBase'] = 2;
					}
						
					//2: 敌人HP
					if((int)$skillConfig['effectBase'] == 2)
					{
						$atk = intval($dCard['hp'] * $skillConfig['effect']);
						$ignoreDef = true;
					}
				}
				//5	enManSkillTp_poison,        //毒:卡牌攻击力:无视防御
				elseif($skillConfig['type'] == Arena_Fight::SK_TYPE_POISON)
				{
					//1: 卡牌攻击力
					if((int)$skillConfig['effectBase'] == 1)
					{
						$atk = $aCard['attack'] * $skillConfig['effect'];
					}
					$ignoreDef = true;
				}
			}
			
			//buff加成
			if(!empty($aCard['buff']))
			{
				foreach($aCard['buff'] as $buffId => &$buff)
				{
					$skillConfig = $skillConfigs[$buffId];
					//11 降攻
					if($skillConfig['type'] == Arena_Fight::SK_TYPE_ATK_DOWN)
					{
						$effect = $skillConfig['effectBase'] * $skillConfig['effect'];
						//1: 卡牌攻击力
						if((int)$skillConfig['effectBase'] == 1)
						{
							$effect = $atk * $skillConfig['effect'];
							if($skillConfig['effect'] == 1)	$ignoreAtk = true;
						}
						$atk -= $effect;
						if($atk < 0) $atk = 0;
					}
		    		//12 加攻
					elseif($skillConfig['type'] == Arena_Fight::SK_TYPE_ATK_UP)
					{
						$effect = $skillConfig['effectBase'] * $skillConfig['effect'];
						//1: 卡牌攻击力
						if((int)$skillConfig['effectBase'] == 1)
						{
							$effect = $aCard['attack'] * $skillConfig['effect'];	
						}
						//6: 玩家回复力
						elseif((int)$skillConfig['effectBase'] == 6)
						{
							$effect = $aCard['defense'] * $skillConfig['effect'];
						}
						//3:敌人攻击力
						elseif((int)$skillConfig['effectBase'] == 3)
						{
							$effect = $dCard['attack'];
							$effect += self::_calcEffectBaseByElement($skillConfig['effectBase'], $aCards, $dCards);
														
							$effect = $effect * $skillConfig['effect'];
						}
						$atk += $effect;
						if($atk < 0) $atk = 0;
					}
				}
			}
			
			if(!empty($dCard['buff']))
			{
				foreach($dCard['buff'] as $buffId => &$buff)
				{
					$skillConfig = $skillConfigs[$buffId];
					
					//4	enManSkillTp_break,         //破甲: 降防
					if($skillConfig['type'] == Arena_Fight::SK_TYPE_BREAK)
					{
						$effect = $skillConfig['effectBase'] * $skillConfig['effect'];
						//5: 敌人防御力 (减少百分比)
						if((int)$skillConfig['effectBase'] == 5)
						{
							$effect = $def * $skillConfig['effect'];
							if($skillConfig['effect'] == 1)	$ignoreDef = true;
						}
						$def -= $effect;
						if($def < 0) $def = 0;
					}
				}
			}
			
			//计算伤害
			if(!empty($addConf['dmg']))
			{
				$damage = $addConf['dmg'];
			}
			else 
			{
				if(!empty($addConf['atk']))
				{
					$atk = $addConf['atk'];
					$ignoreAtk = false;
				}
				if(!empty($addConf['def']))
				{
					$def = $addConf['def'];
					$ignoreDef = false;
				}
				
				$damage = ($ignoreAtk ? 0 : $atk) - ($ignoreDef ? 0 : $def);
				$damage = max($damage, 1);
			}
			
			//伤害计算后的加成
			if(!empty($aCard['buff']))
			{
				foreach($aCard['buff'] as $buffId => &$buff)
				{
					$skillConfig = $skillConfigs[$buffId];
					//目前还木有
				}
			}
			
			if(!empty($dCard['buff']))
			{
				foreach($dCard['buff'] as $buffId => &$buff)
				{
					$skillConfig = $skillConfigs[$buffId];
					
					//7	enManSkillTp_armor,         //免伤
					if($skillConfig['type'] == Arena_Fight::SK_TYPE_ARMOR)
					{
						$effect = $skillConfig['effectBase'] * $skillConfig['effect'];
						//7: 敌方伤害
						if((int)$skillConfig['effectBase'] == 7)
						{
							$effect = $damage * $skillConfig['effect'];	
						}
						$damage -= $effect;
					}
					elseif($skillConfig['type'] == Arena_Fight::SK_TYPE_ATK_BACK)
					{
						$effect = $skillConfig['effectBase'] * $skillConfig['effect'];
						//7: 敌方伤害
						if((int)$skillConfig['effectBase'] == 7)
						{
							$effect = $damage * $skillConfig['effect'];
						}
						$damageBack += $effect;
					}
					
					//绝地反击
					if($buffId == Arena_Fight::SK_ID_JUEDIFANJI)
					{
						$cannotdie = true;
						//触发后减持续时间
						$buff['dur']--;
					}
				}
			}
			
			$damage = max($damage, 1);
			//薄葬
			if($cannotdie) $damage = min($dCard['hp'] - 1, $damage);
			
		}while(0);
		
		$damage = intval($damage);
		
		//buff
		if($atkType == Arena_Fight::ATK_TYPE_BUFF)
		{
			$record['buff'][$skillId]['dmg'] = $damage;
			$dCard['hp'] -= $damage;
			$dCard['hp'] = max($dCard['hp'], 0);
			$record['buff'][$skillId]['hp'] = $dCard['hp'];
		}
		elseif($atkType == Arena_Fight::ATK_TYPE_DMG_BACK)
		{
			$record['dmgBack'] = $damage;
			
			$dCard['hp'] -= $damage;
			$dCard['hp'] = max($dCard['hp'], 0);
		}
		else
		{
			if($skillId > 0)
			{
				$record['def'][$dCard['id']]['dmg'] = $damage;
				$dCard['hp'] -= $damage;
				$dCard['hp'] = max(0, $dCard['hp']);
				$record['def'][$dCard['id']]['hp'] = $dCard['hp'];
			}
			else
			{
				$record['status'] = $status;
				$record['damage'] = $damage;
		
				$dCard['hp'] -= $damage;
				$dCard['hp'] = max(0, $dCard['hp']);
				
				$record['bHp'] = $dCard['hp'];
			}
			
			if(!empty($damageBack))
			{
				$damageBack *= self::_calcElementRate($dCard['ctype'] , $aCard['ctype']);
				$addConf['atk'] = $damageBack;
				self::_calcDamage(0, $dCard, $aCard, $dCards, $aCards, $record, Arena_Fight::ATK_TYPE_DMG_BACK, $addConf);
			}
			
		}
		
		$record['aHp'] = $aCard['hp'];
		
		return array('dmg' => $damage, 'status' => $status);
	}
	
	private function _calcBuff(&$aCard, &$dCard, &$aCards, &$dCards, &$record)
	{
		$ret = array('rc' => 0);
		//我身上的debuff在我的攻击回合才生效
		if(!empty($aCard['buff']))
		{
			$skillConfigs = Common::getConfig( "upgradeSkill" );
			
			foreach($aCard['buff'] as $buffId => &$buff)
			{
				$buffConfig = $skillConfigs[$buffId];
				
				//目前还没有持续回血的技能
				//5	enManSkillTp_poison,        //毒:卡牌攻击力:无视防御
				if($buffConfig['type'] == Arena_Fight::SK_TYPE_POISON)
				{
					self::_calcDamage($buffId, $dCards[$buff['aId']], $aCard, $dCards, $aCards, $record, Arena_Fight::ATK_TYPE_BUFF);
					//看我挂了没
					if(!self::_canAttack($aCard)) break;
				}
				//3	enManSkillTp_delay,         //加回合
				elseif($buffConfig['type'] == Arena_Fight::SK_TYPE_DELAY)
				{
					//这回合啥也不干
					$ret['rc'] = 1;
					break;
				}
				//6 enManSkillTp_recover,       //回复
				elseif($buffConfig['type'] == Arena_Fight::SK_TYPE_RECOVER)
				{
					$effect = $buffConfig['effectBase'] * $buffConfig['effect'];
					//6: 玩家回复力
					if((int)$buffConfig['effectBase'] == 6)
					{
						$effect = $aCard['defense'] * $buffConfig['effect'];
					}
					//8.我方HP上限
					elseif((int)$buffConfig['effectBase'] == 8)
					{
						$effect = $dCard['attack'];
						$effect += self::_calcEffectBaseByElement($buffConfig['effectBase'], $aCards, $dCards);
													
						$effect = $effect * $buffConfig['effect'];
					}
					
					$damage = $effect;
					$record['buff'][$buffId]['heal'] = $damage;
					$aCard['hp'] += $damage;
					$aCard['hp'] = min($aCard['hp'], $aCard['maxHp']);
					$record['buff'][$buffId]['hp'] = $aCard['hp'];
				}
				
			}
		}
		
		return $ret;
	}
	
	private function getTargetByType(&$cards, $type, $id)
	{
		$ids = array();
		
		//单体技能
		if($type == 0)
		{
			$ids = array($id);
		}
		//全体技能
		elseif((int)$type == 1)
		{
			$ids = array_keys($cards);
			
			$elementCode = ($type - (int)$type) * 10;
			if($elementCode > 0)
			{
				$ids = array();
				foreach($cards as &$_card)
				{
					if($_card['ctype'] == $elementCode)
					{
						$ids[] = $_card['id'];
					}
				}
			}
		}
		//hp最少的武将
		elseif($type == 11)
		{
			$minHp = 0;
			foreach($cards as &$_card)
			{
				if($minHp == 0 || $_card['hp'] < $minHp)
				{
					$ids[0] = $_card['id'];
					$minHp = $_card['hp'];
				}
			}
		}
		
		//判断挂了没
		$_ids = array();
		foreach($ids as $_id)
		{
			if(self::_canAttack($cards[$_id]))
			{
				$_ids[] = $_id;
			}
		}
		$ids = $_ids;
		
		return $ids;
	}
	
}