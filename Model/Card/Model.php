<?php
/**
 * 将领模块
 * 
 * @name Model.php
 * @author Liuchangbing
 * @since 2013-1-14
 */
if( !defined( 'IN_INU' ) )
{
    return;
}

class Card_Model extends Logical_Abstract
{
	
	//强化一般成功
	const REINFORCE_SUCCESS_TYPE_NORMAL = "normal";
	
	//强化大成功
	const REINFORCE_SUCCESS_TYPE_BIG = "big";
	
	//强化超级成功
	const REINFORCE_SUCCESS_TYPE_SUPER = "super";
	
	const CACHE_KEY_NOTICE_MSG_QUEUE = "notice_msg_queue";
	
	const TEAM_NUM = 5;
	
	const TEAM_CARD_NUM = 5;
	
	/**
	 * 单例对象
	 * @var	User_Model[]
	 */
	protected static $singletonObjects;

	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Card_Model
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 添加武将卡
	 * @param int $cardCode 武将卡序列号
	 * @param int $level 武将等级
	 * @param int $unlimit 不限制添加，强制加
	 * @param string $logType 流水类型
	 */
	public function addCard( $cardCode , $level = 1 , $unlimit = false , $logType = "" )
	{
		$cardConfig = Common::getConfig( "card" );
		if( !$cardConfig[$cardCode] )
		{
			return false;
		}
		
		$dataModel = Data_Card_Model::getInstance( $this->userId , true );		
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		
		//如果武将数量超过上限
		if( count( $dataModel->getData() ) >= $userInfo['maxCardNum'] && $unlimit == false )
		{
			return false;
		}

		$cardExp = 0;
		if( $level > 1 )
		{
			$cardConf = $cardConfig[$cardCode];
			$expType = $cardConf['exp_type'];
			$cardLevelConfig = Common::getConfig( "cardLevel" );
			$cardLevel = $cardLevelConfig['exp_type'][$expType];
			foreach ( $cardLevel as $cLevel => $cExp )
			{
				if( $level == $cLevel )
				{
					$cardExp = $cExp;
				}
			}
		}
	
		$cardInfo = array(
			'cardId' => $cardCode,
			'exp' => $cardExp,
			'level' => $level,
			'skillLevel' => 1,		
		);
		
		$cardId = $dataModel->addCard( $cardInfo );
		if( $cardId > 0 )
		{
			/**
			 * 解锁武将卡
			 */
			$cardLog = new ErrorLog( "cardHistory" );
			$msg = "addCard>>userId:{$this->userId};cardId:{$cardCode};addTime:{$_SERVER['REQUEST_TIME']}";
			$cardLog->addLog( $msg );
			Data_Card_Unlock::getInstance( $this->userId , true )->unlockCard( $cardCode );
			/**
			 * 加入点将台信息
			 * 点将台兑换的武将不计
			 */
			if($logType != 'pointCard') Activity_PointCard::getInstance($this->userId)->addPointCardData($cardCode);
			/**
			 * 添加道具流水
			 */
			if( !empty( $logType ) )
			{
				//Stats_Model::addUserItemLog( $this->userId , "card" , $cardCode , 1 , "add" , $logType );
				/**
				 * EDAC统计::获得道具
				 */
				$logType = MakeCommand_Config::getItemLogType( $logType );
				Stats_Edac::gainItem( $this->userId , $cardCode , 1 ,  $logType );
			}
		}
		return $cardId;
	}
	
	/**
	 * 武将编队
	 * @param string $cards  ex:"1,2,3"
 	 */
	public function formatTeam( $cardStr )
	{
		if( !empty( $cardStr ) )
		{
			Data_Card_Team::getInstance( $this->userId , true )->setCards( $cardStr );
		}
		return Data_Card_Team::getInstance( $this->userId )->getData();
	}
	
	/**
	 * 获取第一个卡信息
	 */
	public function getLeaderCard()
	{
		
		$teamInfo = Data_Card_Team::getInstance( $this->userId )->getData();
		$teamArr = array();
		$cardId = 0;
		if( $teamInfo['cards'] )
		{
			$teamArr = explode( "," , $teamInfo['cards'] );
			$cardId = $teamArr[0];
		}
		
		if( $cardId == 0 )
		{
			return array();
		}
		return $this->getCardInfo( $cardId );
	
	}
	
	/**
	 * 武将强化
	 * @param int $cardId 武将唯一ID
	 * @param array $cards 武将唯一ID数组
	 */
	public function reinforce( $cardId , $cards )
	{
		$cardInfo = $this->getCardInfo( $cardId );
		if( !$cardInfo )
		{
			throw new Card_Exception( Card_Exception::STATUS_NOT_THIS_CARD );
		}
		
		$cardConfig = Common::getConfig( "card" );
		/**
		 * 计算需要的金币
		 */
		$needGold = $cardInfo['level'] * 100 * count( $cards );
		$result = Data_User_Info::getInstance( $this->userId , true )->changeGold( -$needGold , '武将强化' );
		if( $result == false )
		{
			throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_GOLD );
		}
		
		$mainCardConfig = $cardConfig[$cardInfo['cardId']];
		//计算经验
		$totalExp = 0;
		$rate15Times = 0;
		foreach ( $cards as $cId )
		{
			$eachInfo = $this->getCardInfo( $cId );
			$eachConfig = $cardConfig[$eachInfo['cardId']];
			if( !$eachConfig )
			{
				throw new Card_Exception( Card_Exception::STATUS_NOT_CARD_CONFIG );
			}
			
			if( $eachConfig['ctype'] == $mainCardConfig['ctype'] )
			{
				$totalExp += $eachConfig['base_exp'] * 1.5 * $eachInfo['level'];
				$rate15Times += 1;
			}
			else
			{
				$totalExp += $eachConfig['base_exp'] * 1 * $eachInfo['level'];
			}
			//删除武将
			$delCardInfo = $this->getCardInfo( $cId );
			$delCardIds[] = $delCardInfo['cardId'];
			Data_Card_Model::getInstance( $this->userId , true )->removeCard( $cId );
			
			/**
			 * EDAC统计::使用道具
			 */
			Stats_Edac::lostItem( $this->userId , $eachInfo['cardId'] , 1 , "武将强化消耗" );
			
		}
		//if(isset($delCardIds)) Stats_Model::addUserItemLog( $this->userId , "card" , $delCardIds , count( $delCardIds ) , "del" , "reinforce" );
		
		
		$successType = self::REINFORCE_SUCCESS_TYPE_NORMAL;
		//成功程度
		if( $rate15Times > 2 &&  $rate15Times <= 4 )
		{
			if( rand( 1 , 100 ) <= 20 )
			{
				$totalExp = $totalExp * 1.5;
				$successType = self::REINFORCE_SUCCESS_TYPE_BIG;
			}
		}
		elseif( $rate15Times > 4 )
		{
			//活动期间超成功几率*2
			$super_rate = 10;
			if( Activity_Model::isInTime( Activity_Model::ACTIVE_ENHANCE_WEAPON_RATE_DOUBLE ) )
				$super_rate = 20; 
			if( rand( 1 , 100 ) <= $super_rate )
			{
				$totalExp = $totalExp * 2;
				$successType = self::REINFORCE_SUCCESS_TYPE_SUPER;
			}
		}
		
		//加经验收
		$this->addExp( $cardId , intval( $totalExp ) );
		return $successType;
	}
	
	/**
	 * 武将技能升级
	 * @param unknown $cardId
	 * @param unknown $cards
	 * @param unknown $enforce 必成功
	 * @throws Card_Exception
	 * @throws User_Exception
	 * @return string
	 */
	public function upgradeSkill(  $cardId , $cards , $enforce = false )
	{
		$cardInfo = $this->getCardInfo( $cardId );
		if( !$cardInfo )
		{
			throw new Card_Exception( Card_Exception::STATUS_NOT_THIS_CARD );
		}
		
		$cardConfig = Common::getConfig( "card" );
		$upgradeSkillConfig = Common::getConfig( "upgradeSkill" );
		$skillLevelConfig = Common::getConfig( "skillLevel" );
		/**
		 * 计算需要的金币
		*/
		$needGold = 5000 + ( $cardInfo['skillLevel'] -1 ) * 500;
		if( Data_User_Info::getInstance( $this->userId , true )->changeGold( -$needGold , '武将技能升级' ) == false )
		{
			throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_GOLD );
		}
		 
		
		$mainCardConfig = $cardConfig[$cardInfo['cardId']];
		
		//升级武将技能ID
		$mainSkillId = $mainCardConfig['skid'];
		//技能类型
		$mainSkillType = $upgradeSkillConfig[$mainSkillId]['skillType'];
		//升级需要的点数
		$upType = $upgradeSkillConfig[$mainSkillId]['upType'];
		$upValue = $skillLevelConfig[$upType][$cardInfo['skillLevel']];
		$maxLevel = $upgradeSkillConfig[$mainSkillId]['maxLevel'];
	

		
		if( !$mainSkillId  ||  !$mainSkillType || !$upType  || !$upValue )
		{
			throw new Card_Exception( Card_Exception::STATUS_NOT_CARD_CONFIG );
		}
	
		$addPoint = 0;
		foreach ( $cards as $cId )
		{
			//武将卡数据信息
			$eachInfo = $this->getCardInfo( $cId );
			//武将卡配置信息
			$eachConfig = $cardConfig[$eachInfo['cardId']];
			if( !$eachConfig )
			{
				throw new Card_Exception( Card_Exception::STATUS_NOT_CARD_CONFIG );
			}
			$eachSkillId = $eachConfig['skid'];
			$eachSkillType = $eachConfig['skill_type'];
			$eachSkillValue = $eachConfig['skill_value'];
			
			if( $mainSkillType == $eachSkillType )
			{
				$addPoint += $eachSkillValue * 2;
			}
			else
			{
				$addPoint += $eachSkillValue ;
			}	
			//删除武将
			$delCardInfo = $this->getCardInfo( $cId );
			$delCardIds[] = $delCardInfo['cardId'];
			Data_Card_Model::getInstance( $this->userId , true )->removeCard( $cId );
			
			
			/**
			 * EDAC统计::使用道具
			 */
			Stats_Edac::lostItem( $this->userId , $eachInfo['cardId'] , 1 , "武将技能升级消耗" );
		}
		
		$ret = 0;
		$randPoint = $enforce ? 0 : mt_rand( 1 , $upValue );
		if( $randPoint <= $addPoint )
		{
			//升级技能
			$status = Data_Card_Model::getInstance( $this->userId , true )->upSkillLevel( $cardId  , $maxLevel  );
			if( $status == false )
			{
				throw new Card_Exception( Card_Exception::STATUS_SKILL_LEVEL_MAX );
			}
			$ret = 1;
		}
		//if( isset( $delCardIds ) ) Stats_Model::addUserItemLog( $this->userId , "card" , $delCardIds , count( $delCardIds ) , "del" , "upgradeSkill" );
		return $ret;
	}
	
	
	/**
	 * 武将加经验
	 * @param unknown_type $exp
	 */
	public function addExp( $cardId , $exp )
	{
		$cardInfo = $this->getCardInfo( $cardId );
		$cardConfig = Common::getConfig( "card" );
		$cardConfig = $cardConfig[$cardInfo['cardId']];
		$expType = $cardConfig['exp_type'];
		
		$cardLevelConfig = Common::getConfig( "cardLevel" );
		$levelConfig = $cardLevelConfig['exp_type'][$expType];
		
		$cardExp = $cardInfo['exp'] + $exp;
		$cardLevel = $cardInfo['level'];
		ksort( $levelConfig );
		$maxExp = max(  $levelConfig  );
		$cardExp = $cardExp > $maxExp ? $maxExp : $cardExp;
		
		
		foreach ( $levelConfig as $eachLevel => $eachExp )
		{
			if( $cardExp >= $eachExp && $eachLevel <= $cardConfig['max_lv'] )
			{
				$cardLevel = $eachLevel;
			}
			else
			{
				break;
			}	
		}
		
		if( $cardExp > $cardInfo['exp']  || $cardLevel > $cardInfo['level'] )
		{
			$cardInfo['exp'] = $cardExp;
			$cardInfo['level'] = $cardLevel;
			Data_Card_Model::getInstance( $this->userId , true )->setCardInfo( $cardId , $cardInfo );
		}
	}
	
	/**
	 * 武将转生
	 * @param int $cardId
	 */
	public function rebirth( $cardId , $cards )
	{

		$cardConfig = Common::getConfig( "card" );
		$cardInfo = $this->getCardInfo( $cardId );
		$targetCard = $cardConfig[$cardInfo['cardId']]['upg_target'];
		$upgradeNeed = $cardConfig[$cardInfo['cardId']]['upg_need'];
		
		$needGold = $cardConfig[$cardInfo['cardId']]['upg_gold'];
	
		if( Data_User_Info::getInstance( $this->userId , true )->changeGold( -$needGold , '武将转生' ) ==false )
		{
			throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_GOLD );
		}
		
		
		if( !$targetCard || !$upgradeNeed )
		{
			throw new Card_Exception( Card_Exception::STATUS_NOT_CARD_CONFIG );
		}
		
		$cardInfos = array();
		foreach ( $cards as $eachId )
		{
			$eachCard = $this->getCardInfo( $eachId );
			if( !$eachCard )
			{
				throw new Card_Exception( Card_Exception::STATUS_NOT_THIS_CARD );
			}
			$cardInfos[$eachCard['cardId']] += 1;
		}
		
		$needCardCounts = array_count_values( $upgradeNeed );
		foreach ( $needCardCounts  as  $eachCard => $number )
		{	
			if( $cardInfos[$eachCard] < $number )
			{
				throw new Card_Exception( Card_Exception::STATUS_NOT_THIS_CARD );
			}
		}
		
		$targetCardConf = $cardConfig[$targetCard];
		$cardConf = $cardConfig[$cardInfo['cardId']];
		if( $targetCardConf['skid'] == $cardConf['skid'] )
		{
			$targetSkillLevel = $cardInfo['skillLevel'];
		}
		else 
		{
			$targetSkillLevel = 1;
		}
		
		$cardInfo = array(
				'id' => $cardId,
				'cardId' => $targetCard,
				'exp' => 0,
				'level' => 1,
				'skillLevel' => $targetSkillLevel,
		);
		Data_Card_Model::getInstance( $this->userId , true )->setCardInfo( $cardId , $cardInfo );
		/**
		 * 解锁武将卡
		 */
		Data_Card_Unlock::getInstance( $this->userId , true )->unlockCard( $targetCard );
		//删除要用掉了武将了
		foreach ( $cards as $eachId )
		{
			//删除武将
			$delCardInfo = $this->getCardInfo( $eachId );
			$delCardIds[] = $delCardInfo['cardId'];
			Data_Card_Model::getInstance( $this->userId , true )->removeCard( $eachId );
			
			/**
			 * EDAC统计::使用道具
			 */
			Stats_Edac::lostItem( $this->userId , $delCardInfo['cardId'] , 1 , "武将转生消耗" );
			
		}
		//if( isset( $delCardIds ) ) Stats_Model::addUserItemLog( $this->userId , "card" , $delCardIds , count( $delCardIds ) , "del" , "rebirth" );
	}
	
	/**
	 * 出售武将
	 * @param array $cards
	 */
	public function sell( $cards )
	{
		//判断有没有加锁
		$gold = 0;
		$cardConfig = Common::getConfig( "card" );
		foreach ( $cards as $cardId )
		{
			$cardInfo = $this->getCardInfo( $cardId );
			if( $cardInfo['lock'] == 1 )
			{
				throw new Card_Exception( Card_Exception::STATUS_CARD_LOCKED );
			}
			$cardConf = $cardConfig[$cardInfo['cardId']];
			$gold = $gold + intval( $cardConf['sell_gold'] ) +   ( $cardInfo['level'] - 1 ) * 10;
			//删除武将
			$delCardInfo = $this->getCardInfo( $cardId );
			$delCardIds[] = $delCardInfo['cardId'];
			Data_Card_Model::getInstance( $this->userId , true )->removeCard( $cardId );
			
			
			/**
			 * EDAC统计::使用道具
			 */
			Stats_Edac::lostItem( $this->userId , $delCardInfo['cardId'] , 1 , "出售武将" );
			
		}
		//if( isset( $delCardIds ) ) Stats_Model::addUserItemLog( $this->userId , "card" , $delCardIds , count( $delCardIds ) , "del" , "sell" );
		
		Data_User_Info::getInstance( $this->userId , true )->changeGold( $gold , '出售武将' );
		return $gold;
	}
	
	/**
	 * 删除武将，在编队中的武将不能被删
	 * @param unknown $cardId
	 */
	public function delCard( $cardId )
	{
		$cardTeam = MakeCommand_Card::cardsTeam( $this->userId );
		if( in_array( $cardId , $cardTeam ) )
		{
			return;
		}
		$delCardInfo = $this->getCardInfo( $cardId );
		Data_Card_Model::getInstance( $this->userId , true )->removeCard( $cardId );
		
		$delCardIds = array($delCardInfo['cardId']);
        if( isset( $delCardIds ) ) Stats_Model::addUserItemLog( $this->userId , "card" , $delCardIds , count( $delCardIds ) , "del" , "GM" );
	}
	
	/**
	 * 加锁，加了锁的武将不能出售
	 * @param int $cardId
	 */
	public function lock( $cardId )
	{
		Data_Card_Model::getInstance( $this->userId , true )->lock( $cardId );
	}
	
	/**
	 * 解锁,解了锁才能出售
	 */
	public function unlock( $cardId )
	{
		Data_Card_Model::getInstance( $this->userId , true )->unlock( $cardId );
	}
	
	
	/**
	 *  付费抽清除时间
	 */
	public function clearFreeDrawTime()
	{
		//Data_User_Info::getInstance( $this->userId , true )->clearFreeDrawTime();
		
		
	}

	/**
	 * 求将功能
	 * @param bool $type 1为求良将，2,求神将， 3,为求良器，4,求神器
	 * @param int $times 次数
	 * @param bool $freeDrawSuperCard 免费抽神将
	 * @param int $isNewbie 是否为新手
	 */
	public function drawCard($type ,$times = 1 ,$freeDrawSuperCard = 0, $isNewbie = false)
	{
		
		/**
		 * EDAC统计
		 */
		$typeNames = array(
			1 => '求良将',
			2 => '求神将',
			3 => '求良器',
			4 => '求神器',
		);
		$freeName = ( $freeDrawSuperCard > 0 ) ? '免费' : '付费';
		if($isNewbie) $freeName = '新手';
		$timesName = ( $times == 10 ) ? '10连' : '';
		$actionName = $freeName.$timesName.$typeNames[$type];
		Stats_Edac::playMethod( $this->userId , $actionName );
		
		
		
		$userInfo = Data_User_Info::getInstance( $this->userId )->getData();
		$commonConfig = Common::getConfig( "common" );

		if( $type == 1 || $type == 3  )
		{
			if( $type == 1 )
			{
				//抽良将
				$cardLotteryConfig = Common::getConfig( "freeCard" );
				//扣除援军点数
				$needGacha = $commonConfig['gacha_cost_pt'] * $times;
			}
			elseif ( $type == 3 )
			{
				//抽良器
				$cardLotteryConfig = Common::getConfig( "freeCardItem" );
				//扣除援军点数
				$needGacha = $commonConfig['weapon_cost_pt'] * $times;
			}
			
			if( $userInfo['gachaPoint'] <  $needGacha   )
			{
				throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_GACHA );
			}
			Data_User_Info::getInstance( $this->userId , true )->changeGachaPoint( -$needGacha );
			//标记第一次免费抽
			Data_User_Info::getInstance( $this->userId , true )->setFirstFreeDraw();
		}
		elseif( $type == 2 || $type == 4  )
		{
			//抽神将
			if( $type == 2 )
			{
				$needCoin = $commonConfig['gacha_cost_coin'] * $times;
				if($isNewbie) $needCoin = 30;
				if($times == 10 && !empty($commonConfig['gacha_cost_10_coin'])) $needCoin = $commonConfig['gacha_cost_10_coin']; 
				
				//免费抽神将
				if( $freeDrawSuperCard == 1 )
				{
					$cardLotteryConfig = Common::getConfig( "freeChargeCard" );
				}
				//付费抽神将
				else 
				{
					$cardLotteryConfig = Common::getConfig( "chargeCard" );
					//神将1+1
					if( $times == 1 && Activity_Model::isInTime( Activity_Model::ACTIVE_DRAW_CARD_AND_ONE ) )
					{
						$times = 2;
						$needCoin = $commonConfig['gacha_cost_coin'];
					}
					
				}
			
			}
			//付费抽神器
			elseif ( $type == 4 )
			{
				$cardLotteryConfig = Common::getConfig( "chargeCardItem" );
				
				$needCoin = $commonConfig['weapon_cost_coin'] * $times;
				if($times == 10 && !empty($commonConfig['weapon_cost_10_coin'])) $needCoin = $commonConfig['weapon_cost_10_coin'];
				
				//神器1+1
				if( $times == 1 && Activity_Model::isInTime( Activity_Model::ACTIVE_DRAW_ITEM_AND_ONE ) )
				{
					$times = 2;
					$needCoin = $commonConfig['weapon_cost_coin'];
				}
			}
			
			Data_User_Info::getInstance( $this->userId , true )->setFirstChargeDraw();
			
			if( $userInfo['coin'] < $needCoin &&  $freeDrawSuperCard == 0 )
			{
				throw new User_Exception( User_Exception::STATUS_NOT_ENOUGH_COIN );
			}
			//扣钱
			if( $freeDrawSuperCard != 1 )
			{
				$status = User_Info::getInstance( $this->userId )->changeCoin( -$needCoin , $actionName , $times );
			}
			else 
			{
				//如果是免费抽神将要判断时间,抽神器没有此功能
				if( Data_User_Info::getInstance( $this->userId , true )->freeDrawSuperCard() == false )
				{
					throw new Card_Exception( Card_Exception::STATUS_FREE_DRAW_SUPER_NOT_COOL );
				}
			}
		}
		$cardIds = array();
		for ( $i = 0 ; $i < $times ; $i++ )
		{
			//最多十连抽
			//抽神将时，其中有一个是在另一张表中抽
			if( $times == 10 && $i == 9 && $type == 2 )
			{
				$cardLotteryConfig = Common::getConfig( "superChargeCard" );
			}
			
			$cardId = 0;
			$randNum = mt_rand( 1 , 10000 );
			$preRate = $rate = 0;
			foreach ( $cardLotteryConfig as $card )
			{
				$rate += $card['rate'];
				if( $randNum > $preRate && $randNum <= $rate )
				{
					$cardCode = $card['cardId'];
					$cardLevel = $card['cardLevel'];
				}
				$preRate += $card['rate'];
			}
			if( !empty( $cardCode ) )
			{
				$record[$cardCode] += 1;
				
				$cardLevel = $cardLevel > 0 ? $cardLevel : 1;
				
				$cardId = $this->addCard( $cardCode , $cardLevel , true , "drawCard" );
				if( !$cardId )
				{
					throw new Card_Exception( Card_Exception::STATUS_CARD_NUM_MAX );
				}
				//玩家抽到5星武将即滚动公告发送
				$cardConfig = Common::getConfig( "card" );
				if( $cardConfig[$cardCode] && intval( $cardConfig[$cardCode]['star'] ) == 5 )
				{
					$userProfile = Data_User_Profile::getInstance( $this->userId )->getData();
					$this->pushCardMsg( $userProfile['nickName'] , $cardCode );
				}
				
				$cardIds[] = $cardId;
				unset( $cardCode );
			}
		}
		return $cardIds;
	}
	
	/**
	 * 增加一个主将
	 * @deprecated
	 * @param string $leaderCardId
	 */
	public function addLeaderCard( $cardId )
	{
		$cardId = $this->addCard( $cardId );
		if( $cardId > 0 )
		{
			User_Info::getInstance( $this->userId , true )->setLeaderCard( $cardId );
		}
	}
	
	/**
	 * 根据武将ID获取武将信息
	 * @param int $leaderId
	 */
	public function getCardInfo( $cardId )
	{
		return Data_Card_Model::getInstance( $this->userId )->getCardInfo( $cardId );
	}
	
	/**
	 * 获取解锁的武将卡
	 */
	public function getUnlockCard()
	{
		$data = Data_Card_Unlock::getInstance( $this->userId )->getData();
		$returnData = array();
		$cardList = $this->getData();

		foreach ( $data as $cardCode )
		{
			//默认解锁了，但没
			$returnData[$cardCode] =  1;
			foreach ( $cardList as $cardInfo )
			{
				if( $cardInfo['cardId'] == $cardCode )
				{
					$returnData[$cardCode] =  2;
				}
			}
			//没有0
			//解锁了但没有1
			//有就是2
		}
		return $returnData;
	}
	/**
	 * 抽武将消息
	 */
	public function pushCardMsg( $nickName , $cardCode )
	{
		$newMsg = array(
			'type' => 1,
			'nickName' => $nickName,
			'cardId' => $cardCode,
		);
		$this->pushMsgQueue($newMsg);
	}
	
	/**
	 * 文字公告
	 * @param unknown $msg
	 */
	public function pushWordMsg($msg, $getVal = false)
	{
		$newMsg = array(
			'type' => 2,
			'msg' => $msg,
		);
		if($getVal) return $newMsg;
		
		$this->pushMsgQueue($newMsg);
	}
	
	public function pushMsgQueue($msg)
	{
		$cache = Common::getCache();
		if( !$msgList = $cache->get( self::CACHE_KEY_NOTICE_MSG_QUEUE ))
		{
			$msgList = array();
		}
		$msgList[$_SERVER['REQUEST_TIME']] = $msg;
		if( count( $msgList ) > 100 ) $this->arrayShiftKeepKey( $msgList );
		$cache->set( self::CACHE_KEY_NOTICE_MSG_QUEUE , $msgList , 86400 );
	}
	
	/**
	 * 每隔5分钟获取一次
	 * 获取消息
	 */
	public function getMsgQueue()
	{
		$returnData = array();
		$cache = Common::getCache();
		if( !!$msgList = $cache->get( self::CACHE_KEY_NOTICE_MSG_QUEUE ) )
		{
			$myMsgList = $cache->get( $this->userId."_".self::CACHE_KEY_NOTICE_MSG_QUEUE);
			foreach( $msgList as $msgTime => $msg )
			{
				//只取半小时内的未读消息
				if( $_SERVER['REQUEST_TIME'] - 1800 < $msgTime && empty( $myMsgList[$msgTime] ) )
				{
					$myMsgList[$msgTime] = $msg;
					$returnData[] = $msg;
				}
			}
			if( !empty( $returnData ))
			{
				if( count( $myMsgList ) > 100 ) $this->arrayShiftKeepKey( $myMsgList );
				$cache->set( $this->userId."_".self::CACHE_KEY_NOTICE_MSG_QUEUE , $myMsgList , 86400 );
			}
		}
		$returnData = array_merge($returnData, Activity_Notice::getInstance($this->userId)->getNotice());
		return $returnData;
	}
	
	public function getData()
	{
		return Data_Card_Model::getInstance( $this->userId )->getData();
	}
	
	private function arrayShiftKeepKey( &$arr )
	{
		foreach( $arr as $k => $v )
		{
			unset( $arr[$k] );
			break;
		}
	}
	
	public function getLastId()
	{
		return Data_Card_Model::getInstance( $this->userId )->getLastId();
	}
	
}
