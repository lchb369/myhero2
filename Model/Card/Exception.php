<?php

class Card_Exception extends GameException
{
	/**
	 * 不存在该卡配置
	 * @var	int
	 */
	const STATUS_NOT_CARD_CONFIG = 300;
	/**
	 * 不存在该卡
	 * @var	int
	 */
	const STATUS_NOT_THIS_CARD = 301;
	/**
	 * 该武将已经加锁
	 * @var	int
	 */
	const STATUS_CARD_LOCKED = 302;
	/**
	 * 武将数量达到上限
	 * @var	int
	 */
	const STATUS_CARD_NUM_MAX = 303;
	
	/**
	 * 技能等级达到上限
	 * @var unknown
	 */
	const STATUS_SKILL_LEVEL_MAX = 304;
	
	/**
	 * 免费抽神将冷却时间没到
	 * @var unknown
	 */
	const STATUS_FREE_DRAW_SUPER_NOT_COOL = 305;
	
	/**
	 * 点将失败
	 * @var unknown_type
	 */
	const STATUS_POINT_CARD_FAIL = 306;
}

?>