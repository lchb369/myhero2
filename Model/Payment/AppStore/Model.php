<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

class Payment_AppStore_Model
{
	
	/**
	 * 创建订单
	 */
	public static function createOrder( $userId , $gid , $orderId )
	{
		//去苹果验证
		$ser = (isset( $_REQUEST['sandbox'] ) ) ? "sandbox" : "buy";
		$ser = "buy";
		if(Helper_Common::inPlatform(array('test')) || in_array($GLOBALS['appPlatform'], array('ugeiostest')) )
		{
			$ser = "sandbox";
		}
		$verifyUrl = "https://".$ser.".itunes.apple.com/verifyReceipt";
		$postData = json_encode( array('receipt-data' => $orderId ) );
		
		$verifyReceipt = Helper_WWW::post( $verifyUrl , $postData );
		
		$payLog = new ErrorLog( "appStoreBuy" );
		$payLog->addLog( "ser:".$ser.", verifyReceipt:".$verifyReceipt );
		
		if( empty( $verifyReceipt) ) return false;
		$verifyReceipt = json_decode( $verifyReceipt , true );
		if( !is_array( $verifyReceipt ) || $verifyReceipt['status'] !== 0 ) return false;
		
		//是否存在商品
		$itemExist = false;
		$goodsConfig = Common::getConfig( "goods" );
		foreach($goodsConfig as $goodsId => $conf)
		{
			if($goodsId == $gid && $conf['AppStoreId'] == $verifyReceipt['receipt']['product_id'])
			{
				$goodsConfig[$gid]['price'] = $conf['AppStore']; 
				$itemExist = true;
				break;	
			}
		}
		if(!$itemExist)
		{
			$payLog = new ErrorLog( "appStoreBuy" );
			$payLog->addLog( "createOrder: fail , goods not exists" );
			return false;
		}
// 		else
// 		{
// 			$product_id = explode( "." , $verifyReceipt['receipt']['product_id'] );
// 			$product_id = intval( $product_id[count($product_id)-1] );
// 			if( $product_id != intval( $gid ) ) return false;
// 		}
		//是否已经发送过
		$thirdId = $verifyReceipt['receipt']['transaction_id'];
		$payType = "AppStore";
		$checkOrder = Data_Order_Model::getInstance( $userId )->checkOrderStatusForThird( $thirdId , $payType );
		if( $checkOrder == 1 )
		{
			return false;
		}
				
		$price = $goodsConfig[$gid]['price'];
		$str = $userId.$_SERVER['REQUEST_TIME'].$gid;
		$orderId = md5( $str );
		
		$order = array(
				'id' => $orderId,
				'goodsName' => $gid,
				'goodsNum' => 1,
				'price' => $price,
				'discount' => 0,
				'totalPrice' => $price,
				'buyerId' => $userId,
				'tradeTime' => $_SERVER['REQUEST_TIME'],
				'addTime' => $_SERVER['REQUEST_TIME'],
				'email' => '',
				'platform' => $payType,
				'status' => 1,
				'thirdId' =>  $thirdId,
		);
		$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		if( $rs == true )
		{
			$payLog = new ErrorLog( "appStoreBuy" );
			$payLog->addLog( "order:".json_encode( $order ) );
			Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , $payType , $price , $orderId );
			
			//$userProfile = User_Profile::getInstance( $userId )->getData();
			$platform = Common::getConfig( "platform" );
			if( $platform == "zq" )
			{
				//发货后回调掌趣验证
				$postData = array(
					"Username" => $userId,
					"gameid" => "g0063",
					"serverid" => "s1",
					"sid" => "",
					"money" => $price,
					"pay_time" => $_SERVER['REQUEST_TIME'],
				);
				$verifyCallback = Helper_WWW::post( "http://App.gamebean.net/api/api2.php?act=appSuccess" , http_build_query( $postData ) );
				
				//if( $verifyCallback != '1' ) return false;
			}
			
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
}

?>
