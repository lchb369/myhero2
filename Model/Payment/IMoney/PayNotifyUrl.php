<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

//---------------------------------------------------------
//统振
//---------------------------------------------------------
class Payment_IMoney_PayNotifyUrl
{
	
	private static $account_no = "joifun";
	private static $game_no = "统振测试";
	private static $key = "joifun";
	
	private static $sandboxSerIP = '60.248.165.68';
	private static $serIP = '124.9.5.188';
	
	/**
	 * 客户端请求向I-Money验证
	 * para需要 userId
	 */
	public static function verify( $userId )
	{
		$orderId = md5( "order_IMoney_".$userId."_".$_SERVER['REQUEST_TIME']."_".mt_rand( 1, 10000 ) );
		$para = array(
			"userId" => $userId,
			"orderId" => $orderId,
		);
		$account_no = self::$account_no;
		$game_no = self::$game_no;
		$user_no = $userId;
		$para = urlencode( json_encode( ( $para ) ) );
		$key = self::$key;
		$customer_no = $orderId;
		$mode = "mobile";
		
		$return_url = Common::getServerAddr()."Web/Api/Pay/IMoneyReturnUrl.php?orderId=".$orderId."&userId=".$userId;
		
		$receiptData = '<html>
<body>
   <form action="http://'.( isset( $_REQUEST['sandbox'] ) ? self::$sandboxSerIP : self::$serIP ).'/api" accept-charset="UTF-8" method="post" name="form1" id="form1">
   <input type="hidden" name="account_no" id="account_no" value="'.$account_no.'"/>
   <input type="hidden" name="game_no" id="game_no" value="'.$game_no.'"/>
   <input type="hidden" name="user_no" id="user_no" value="'.$user_no.'"><br>
   <input type="hidden" name="para" id="para" value="'.$para.'"/>
   <input type="hidden" name="key" id="key" value="'.$key.'"/>
   <input type="hidden" name="customer_no" id="customer_no" value="'.$customer_no.'"/>
   <input type="hidden" name="return_url" id="return_url" value="'.$return_url.'"/>
   <input type="hidden" name="mode" id="mode" value="'.$mode.'"/>
   </form>
   <script>
   document.getElementById("form1").submit();
   </script>
</body>
</html>';
		return $receiptData;
	}
	
	/**
	 * 统振的回调
	 */
	public static function notify()
	{
		$action_type    = $_POST["action_type"];
		$order_no       = $_POST["order_no"];
		$game_no        = $_POST["game_no"];
		$order_amt      = $_POST["order_amt"];
		$key            = self::$key;
		$para           = $_POST["para"];
		$user_no        = $_POST["user_no"];
		$verify_code    = $_POST["verify_code"];
		$status_code    = $_POST["status_code"];
		$Message        = $_POST["Message"];
		
		if($action_type == "1" && $status_code == "000")
		{
			$md5_str = strtoupper(MD5($order_no.$game_no.$order_amt.$key.$para.$user_no));
			if($md5_str == $verify_code)
			{
				$ret = "1|".$order_no."|".$game_no."|".$order_amt;
				
				//orderCheck 存档备份, 补点用
				$paraArr = json_decode( urldecode( $para ) ,true );
				Stats_Model::add( "orderCheck",
					array(
						"type" => "IMoney",
						"data" => array(
							"user_no" => $user_no,
							"order_no" => $order_no,
							"paraArr" => $paraArr,
							"order_amt" => $order_amt,
						),
					)
				);
				
			}
			else
			{
				$ret = "0|".$order_no."|".$game_no."|".$order_amt."|".$md5_str.print_r($_POST);
			}
		}
		else if($action_type == "2" && $status_code == "000")
		{
			$md5_str = strtoupper(MD5($order_no.$game_no.$order_amt.$key.$para.$user_no));
			if($md5_str == $verify_code)
			{
				$payLog = new ErrorLog( "IMoneyOrder" );
				$payLog->addLog( "finalVerifyPass:".json_encode( $_POST ) );

				$paraArr = json_decode( urldecode( $para ) ,true );
				$ret = self::createOrder( $paraArr , $order_amt , $order_no );
				
				//orderCheck 交易成功, 删除
				Stats_Model::remove( "orderCheck", 
					array(
						"type" => "IMoney",
						"data.order_no" => $order_no
					)
				);
				
			}
			$ret = !empty( $ret ) ? "1" : "0";			
		}
		
		return $ret;
		
	}
	
	//TODO: 提供廠商再次確認訂單交易狀態
	public static function checkOrder( $order_no )
	{
		$verify = false;
		
		$check = Stats_Model::find( "orderCheck" , array( "type" => "IMoney" , "data.order_no" => $order_no ) );
		if( count( $check ) == 1 )
		{
			$check = $check[0];
			$user_no = $check['data']["user_no"];
			
			//查詢訂單狀態
			//測試機
			$url = "http://".( isset( $_REQUEST['sandbox'] ) ? self::$sandboxSerIP : self::$serIP )."/api/service";
			$data = array(
				"account_no" => self::$account_no,
				"user_no" => $user_no,
				"order_no" => $order_no,
				"mode" => "checkorder",
			);
	
			$endVerify = Helper_WWW::post( $url , http_build_query( $data ) );
			if( !empty( $endVerify ) )
			{
				$endVerify = json_decode( $endVerify , true );
				if( isset( $endVerify['Status'] ) )
				{
					if( !empty( $endVerify['Status'] ) ) $verify = true;
					Stats_Model::remove( "orderCheck",
						array(
							"type" => "IMoney",
							"data.order_no" => $order_no
						)
					);
				}
			}
			if( $verify )
			{
				$paraArr = $check['data']['paraArr'];
				//补款
				$order_amt = $check['data']['order_amt'];
				$order_no = $check['data']['order_no'];
				$verify = self::createOrder( $paraArr , $order_amt , $order_no );
			}
		}
		return $verify;
	}
	
	/**
	 * 交易完成后的返回界面
	 * Enter description here ...
	 * @param unknown_type $userId
	 * @param unknown_type $orderId
	 */
	public function returnUrl( $userId , $orderId )
	{
			if( empty( $userId ) || empty( $orderId ) ) $status = 0;
			else $status = Order_Model::getInstance( $userId )->checkOrderStatus( $orderId );
			
        	if( $status == 1 )
        	{
        		$content = "儲值成功，請返回遊戲獲取你的元寶吧。";
        	}
        	else
        	{
        		$content = "儲值失败";
        	}
        	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
.container
{
	height:auto; 
	width:auto;
	text-align:center;
	font-size:30px;
	font-family:Arial, Helvetica, sans-serif;
}
</style>
</head>
<body>
<div class="container">'.$content.'</div>
</body>
</html>';
        exit;
	}
	
	private static function createOrder( $para , $order_amt , $thirdOrderId )
	{
		$userId = $para['userId'];
		$orderId = $para['orderId'];
		//不能通过gid判断，要通过面额来判断
			
		//是否存在商品
		$goodsConfig = Common::getConfig( "goods" );
		foreach( $goodsConfig as $conf )
		{
			if( !empty( $conf['IMoney'] ) && $conf['IMoney'] == $order_amt )
			{
				$gid = $conf['goodsId'];
				break;
			}
		}
		if( empty( $gid ) )
		{
			return false;
		}
			
		//是否已经发送过
		$status = Order_Model::getInstance(  $userId )->checkOrderStatus( $orderId );
		if( $status == 1 )
		{
			return false;
		}
			
		$price = $goodsConfig[$gid]['price'];
			
		$order = array(
				'id' => $orderId,
				'goodsName' => $gid,
				'goodsNum' => 1,
				'price' => $price,
				'discount' => 0,
				'totalPrice' => $price,
				'buyerId' => $userId,
				'tradeTime' => $_SERVER['REQUEST_TIME'],
				'addTime' => $_SERVER['REQUEST_TIME'],
				'email' => '',
				'platform' => "IMoney",
				'status' => 1,
				'thirdId' => $thirdOrderId,
		);
		$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		if( $rs == true )
		{
			$payLog = new ErrorLog( "IMoneyOrder" );
			$payLog->addLog( "createOrder:".json_encode( $order ) );
			Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , "IMoney" , $price , $orderId );
			return true;
		}
		else
		{
			return false;
		}
		return true;
	}
	
}

?>