<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

//---------------------------------------------------------
//CMG 移动基地
//---------------------------------------------------------
class Payment_CMG_PayNotifyUrl
{
	/**
	 * 
	 * @param unknown $thirdUid
	 * @param unknown $consumeCode
	 * @param unknown $cpParam
	 * @param unknown $transIDO
	 * @return boolean
	 */	
	public static function verifyAndCreateOrder($userId, $consumeCode , $transIDO)
	{
		$payLog = new ErrorLog( "CMGOrder" );
		
		$ret = false;
		do
		{
			$lockKey = "Payment.CMGNotify"."_".$userId;
			
			//加锁
			if( !$lock = Helper_Common::addLock( $lockKey ) ) break;
			
			if(!User_Model::exist($userId))
			{
				$payLog->addLog( "verifyError: user not exist ".$userId);
				break;
			}

			$ret = self::_createOrder( $userId , $consumeCode, $transIDO );
			
		}while(0);
		
		if($lock) Helper_Common::delLock( $lockKey );
		
		return $ret;
	}
	
	private static function _createOrder( $userId, $consumeCode, $transIDO )
	{
		$payLog = new ErrorLog( "CMGOrder" );
		//是否存在商品
		$gid = 0;
		$goodsConfig = Common::getConfig( "goods" );
		foreach($goodsConfig as &$conf)
		{
			if($conf['goodsName'] == $consumeCode)
			{
				$gid = $conf['goodsId'];
				break;
			}
		}
		if( !$goodsConfig[$gid] )
		{
			$payLog->addLog( "createOrderError: goodsConfig not exist ".$gid);
			return false;
		}

		$price = $goodsConfig[$gid]['price'];
		$orderId = md5( "order_CMG_".$userId."_".$_SERVER['REQUEST_TIME']."_".mt_rand( 1, 10000 ) );
		
		$order = array(
			'id' => $orderId,
			'goodsName' => $gid,
			'goodsNum' => 1,
			'price' => $price,
			'discount' => 0,
			'totalPrice' => $price,
			'buyerId' => $userId,
			'tradeTime' => $_SERVER['REQUEST_TIME'],
			'addTime' => $_SERVER['REQUEST_TIME'],
			'email' => '',
			'platform' => "CMG",
			'status' => 1,
			'thirdId' => $transIDO,
		);
		$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		if( $rs == true )
		{
			$payLog->addLog( "createOrder:".json_encode( $order ) );
			Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , "CMG" , $price , $order['id'] );
			return true;
		
		}
		else
		{
			$payLog->addLog( "createOrderError: add order fail");
			return false;
		}
	}
	
	public function userInfo()
	{
		return "0";	
	}
	
}

?>