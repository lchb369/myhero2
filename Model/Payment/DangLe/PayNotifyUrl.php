<?php

//---------------------------------------------------------
//当乐网即时到帐支付后台回调示例，商户按照此文档进行开发即可
//---------------------------------------------------------
class Payment_DangLe_PayNotifyUrl
{
	
	public static function proccess()
	{
		//支付密钥
		$paymentKey = "NIhmYdfPe05f";
		
		$result = $_GET['result'];
		$money = $_GET['money'];
		$orderNo = $_GET['order'];
		$memberId = $_GET['mid'];
		$dateTime = $_GET['time'];
		$signature = $_GET['signature'];
		$ext = $_GET['ext'];
		
		$orderId = trim( $ext );
		
		$string = "order=".$orderNo."&money=".$money."&mid=".$memberId."&time=".$dateTime."&result=".$result."&ext=".$ext."&key=".$paymentKey;
		$sigString = strtolower( md5( $string ) );
		
		if( $result && $sigString == $signature  || 1 )
		{
			if( $result == "1" )
			{
			
				$cache = Common::getCache();
				$orderData = $cache->get( $orderId );
				if( empty( $orderData ) )
				{
					return false;
				}

				$orderInfo = json_decode( $orderData , true );
				$userId = $orderInfo['uid'];
				$goodsId = $orderInfo['goodsName'];
				//创建定单
				$order = array(
						//'id' => $_POST['trade_no'].rand( 1 , 100000 ),
						'id' => $orderId,
						'goodsName' => $orderInfo['goodsName'],
						'goodsNum' =>  1,
						'price' => $money,
						'discount' => 0,
						'totalPrice' =>  $money,
						'buyerId' => $orderInfo['uid'],
						'tradeTime' =>$_SERVER['REQUEST_TIME'],
						'addTime' => $_SERVER['REQUEST_TIME'],
						'email' =>'',
						'platform' => "dl",
						'status' => 1,
						'thirdId' => $orderNo,
				);
				
				
				$rs = Order_Model::getInstance( $userId )->addOrder( $order );
				
				if( $rs == true )
				{
					Order_Model::getInstance(  $userId  )->SendGoods( $goodsId , 1 , "dl" , $money , $orderId );
					return true;
				
				}
				else
				{
					return true;
				}
				
				
				//支付成功
				return true;
			}
			else 
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		
	}
}

?>