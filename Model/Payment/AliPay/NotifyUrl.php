<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
</head>
<?php
/*
 *功能：安全致富服务器异步通知页面
*版本：1.0
*日期：2011-09-15
*说明：
*以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
*该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
*/

///////////页面功能说明///////////////
// 创建该页面文件时，请留心该页面文件中无任何HTML代码和空格
// 该页面不能在本机电脑测试，请到服务器上做测试。请确保外部可以访问该页面
// TRADE_FINISHED(表示交易已经成功结束)
/////////////////////////////////////
require_once("AlipayConfig.php");
require_once("AlipayFunction.php");
class Payment_AliPay_NotifyUrl
{
	
		public static function processCallBack()
		{
			$result = array();
			
			//$a = '{"method":"Payment.aliTradeCallBack","sign":"YcTu5w5QzFl0GeOwMF+avxEtLTuEd3ZXpp8YAgC6NEeQ9K3U5VaQU\/y7IOGBRQaEP7kzfjS7CroBfsOsaVO7hqo7jtFkHamZswVDrYG\/ChtJZKT\/6OTlld3vFP4kUMIAebenyZfLC6WDkFBX9WD226yrImNj2P58HXsJSz25JMs=","sign_type":"RSA","notify_data":"<notify><partner>2088901306787456<\/partner><discount>0.00<\/discount><payment_type>1<\/payment_type><subject>5<\/subject><trade_no>2013032856667146<\/trade_no><buyer_email>vkyii@foxmail.com<\/buyer_email><gmt_create>2013-03-28 13:29:04<\/gmt_create><quantity>1<\/quantity><out_trade_no>703168860-032813285717269<\/out_trade_no><seller_id>2088901306787456<\/seller_id><trade_status>TRADE_FINISHED<\/trade_status><is_total_fee_adjust>N<\/is_total_fee_adjust><total_fee>0.01<\/total_fee><gmt_payment>2013-03-28 13:29:05<\/gmt_payment><seller_email>zhouxiaoling@ecngame.com<\/seller_email><gmt_close>2013-03-28 13:29:05<\/gmt_close><price>0.01<\/price><buyer_id>2088302113140467<\/buyer_id><use_coupon>N<\/use_coupon><\/notify>"}';
			//$_POST = json_decode( $a , true );
		
			
			//获取notify_data，需要添加notify_data=
			//不需要解密，是明文的格式
			//$notify_data = "notify_data=" . $_POST["notify_data"]；
			$notify_data = "notify_data=" . $_POST["notify_data"];
			
			//获取sign签名
			//$sign = $_POST["sign"];
			$sign = $_POST["sign"];
				
			
		//	$sign = "S0FfBSarXBVvY/H6/bDV8rqu7JGHadQK3rqo2VRoKvhncBJhg3wK4Wb5NWp7l95XD/AoPbLM8LVCjE6VoVVRquX82y4cEqsrdKy3pYnzwG18DDcohDeeXM5ive3qC/Ct93W1mwD1mkzgSMyBAtAQyoiitAe/E3Fc8PHmQR+YfJI=";
		//	$notify_data = "notify_data:notify_data=<notify><partner>2088901306787456</partner><discount>0.00</discount><payment_type>1</payment_type><subject>5</subject><trade_no>2013032550495446</trade_no><buyer_email>vkyii@foxmail.com</buyer_email><gmt_create>2013-03-25 14:17:22</gmt_create><quantity>1</quantity><out_trade_no>395509563-0325141715-1199</out_trade_no><seller_id>2088901306787456</seller_id><trade_status>TRADE_FINISHED</trade_status><is_total_fee_adjust>N</is_total_fee_adjust><total_fee>0.01</total_fee><gmt_payment>2013-03-25 14:17:22</gmt_payment><seller_email>zhouxiaoling@ecngame.com</seller_email><gmt_close>2013-03-25 14:17:22</gmt_close><price>0.01</price><buyer_id>2088302113140467</buyer_id><use_coupon>N</use_coupon></notify>";
			
			//$notify_data = "notify_data:notify_data=<notify><partner>2088901306787456</partner><discount>0.00</discount><payment_type>1</payment_type><subject>5</subject><trade_no>2013032550495446</trade_no><buyer_email>vkyii@foxmail.com</buyer_email><gmt_create>2013-03-25 14:17:22</gmt_create><quantity>1</quantity><out_trade_no>395509563-0325141715-1199</out_trade_no><seller_id>2088901306787456</seller_id><trade_status>TRADE_FINISHED</trade_status><is_total_fee_adjust>N</is_total_fee_adjust><total_fee>0.01</total_fee><gmt_payment>2013-03-25 14:17:22</gmt_payment><seller_email>zhouxiaoling@ecngame.com</seller_email><gmt_close>2013-03-25 14:17:22</gmt_close><price>0.01</price><buyer_id>2088302113140467</buyer_id><use_coupon>N</use_coupon></notify>";

			//$notify_data = "";
			//验证签名
			$isVerify = verify($notify_data, $sign);
			
			$payLog = new ErrorLog( "aliPaymentLog" );
			//如果验签没有通过
			
			if( !$isVerify )
			{
				$result['status'] = "fail";
				$payLog->addLog( "sign:".$sign );
				$payLog->addLog( "notify_data:".$notify_data."\n" );
				$payLog->addLog( "isVerify:bad!!\n" );
				
				return $result;
			}
			
			//获取交易状态
			$trade_status = getDataForXML($_POST["notify_data"] , '/notify/trade_status');
			
			$payLog->addLog( "trade_status:".$trade_status );
			
			//判断交易是否完成
			if($trade_status == "TRADE_FINISHED" )
			{
				$result['status'] = "success";
				$tradeNo = getDataForXML($_POST["notify_data"] , '/notify/trade_no');
				
				//这是我们自己定义的
				$outTradeNo = getDataForXML($_POST["notify_data"] , '/notify/out_trade_no');
				
				$tradeNoArr = explode( "-", $outTradeNo );
				$result['uid'] = $tradeNoArr[0];
				//在此处添加您的业务逻辑，作为收到支付宝交易完成的依据
				$result['order'] = array(
					'id' => strval( $outTradeNo ),
					'goodsName' => (int)getDataForXML($_POST["notify_data"] , '/notify/subject'),
					'goodsNum' => (int)getDataForXML($_POST["notify_data"] , '/notify/quantity'),
					'email' => (string)getDataForXML($_POST["notify_data"] , '/notify/buyer_email'),
					'price' => (float)getDataForXML($_POST["notify_data"] , '/notify/price'),
					'discount' => (float)getDataForXML($_POST["notify_data"] , '/notify/discount'),
					'totalPrice' => (float)getDataForXML($_POST["notify_data"] , '/notify/total_fee'),
					'buyerId' => (string)getDataForXML($_POST["notify_data"] , '/notify/buyer_id'),
					'tradeTime' => (int)getDataForXML($_POST["notify_data"] , '/notify/gmt_payment'),
					'addTime' => $_SERVER['REQUEST_TIME'],
					'thirdId' => (string)$tradeNo,
					'platform' => "alipay",
					'status' => 1,
				);
				
			}
			else
			{
				$payLog->addLog( "trade_status_xx:fail trade status :".$trade_status );
				$result['status'] = "fail";
				
			}
			return $result;
		}
}
?>