<?php

//---------------------------------------------------------
//mo9即时到帐支付后台回调示例，商户按照此文档进行开发即可
//---------------------------------------------------------
class Payment_Mo9_PayNotifyUrl
{
	
	public static function proccess()
	{
		
		//"e87d4164bafd433385a4c9875d3bd3f0"; //测试Key
	    //"0619359d2bcf4a8fa357de1ccffedca1"; //正式Key
		
		$key = "0619359d2bcf4a8fa357de1ccffedca1";
		$sign = $_POST['sign'];
		unset( $_POST['sign'] );
		ksort( $_POST );
		
		$signPars = array();
		foreach($_POST as $k => $v)
		{
			if( "sign" != $k && "method" != $k ) 
			{
				$signPars[] = $k."=".$v;
			}	
		}
		
		$signString = implode( "&", $signPars );
		$signString .= $key;
		$makeSign = strtoupper( md5( $signString ) );
		
		if( $sign == $makeSign   )
		{
			if( $_POST['trade_status'] == "TRADE_SUCCESS" )
			{
				//$_POST['item_name'] =1;
				//$_POST['payer_id'] = 1143699465;
				$userId = $_POST['payer_id'];	
				$order = array(
						'id' => $_POST['trade_no'],
						//'id' => rand( 1 ,10000000 ),
						'goodsName' => $_POST['item_name'],
						'goodsNum' => 1,
						'price' => $_POST['amount'],
						'discount' => 0,
						'totalPrice' => $_POST['req_amount'],
						'buyerId' => $_POST['payer_id'],
						'tradeTime' => $_SERVER['REQUEST_TIME'],
						'addTime' => $_SERVER['REQUEST_TIME'],
						'email' => $_POST['pay_to_email'],
						'platform' => "mo9",
						'status' => 1,
						'thirdId' => "",
				);
				$rs = Order_Model::getInstance( $userId )->addOrder( $order );
				if( $rs == true )
				{
					Order_Model::getInstance( $userId )->SendGoods( $_POST['item_name'] , 1 , "mo9" , $order['price'] , $order['id'] );
					echo "OK";
				}
				else 
				{
					echo "bad";
				}
			}
			else
			{
				echo "bad";
			}
		}
		else
		{
			echo "bad";
		}
		
		
	}
}

?>