<?php

//---------------------------------------------------------
//财付通即时到帐支付后台回调示例，商户按照此文档进行开发即可
//---------------------------------------------------------

require ("classes/ResponseHandler.class.php");
require ("classes/WapNotifyResponseHandler.class.php");

class Payment_TenPay_PayNotifyUrl
{
	
	public static function proccess()
	{
		
		$payLog = new ErrorLog( "TenpayNotifyLog" );
		$message = $_SERVER['REQUEST_URI'];
		$payLog->addLog( $message );
		
		/* 商户号 */
		$partner = "1215481101";
		
		/* 密钥 */
		$key = "24d02e06e15cfe989464df57ab7e81e4";
		
		
		/* 创建支付应答对象 */
		$resHandler = new WapNotifyResponseHandler();
		$resHandler->setKey($key);
		
		//判断签名
		if($resHandler->isTenpaySign() ) 
		{
			
			//商户订单号
			$bargainor_id = $resHandler->getParameter("bargainor_id");
			
			//财付通交易单号
			$transaction_id = $resHandler->getParameter("transaction_id");
			//金额,以分为单位
			$total_fee = $resHandler->getParameter("total_fee");
			
			//支付结果
			$pay_result = $resHandler->getParameter("pay_result");
			
			$sp_billno = $resHandler->getParameter( "sp_billno" );
			
			if( "0" == $pay_result  )
			{
				//------------------------------
				//处理业务开始
				//------------------------------
				//uid*goodsid*number
				$goodStr = $resHandler->getParameter("attach");
				$goodsArr = explode( "*", $goodStr );
				$userId = (int)$goodsArr[0];
				$gid = $goodsArr[1];
				$number = $goodsArr[2];
			
				$order = array(
						'id' => $transaction_id,
						'goodsName' => $gid,
						'goodsNum' => $number,
						'price' => $total_fee/100,
						'discount' => 0,
						'totalPrice' => $total_fee/100,
						'buyerId' => $userId,
						'tradeTime' => $_SERVER['REQUEST_TIME'],
						'addTime' => $_SERVER['REQUEST_TIME'],
						'platform' => "tenpay",
						'status' => 1,
						'thirdId' => $sp_billno,
				);
				$rs = Order_Model::getInstance( $userId )->addOrder( $order );
				if( $rs == true )
				{
					$status = Order_Model::getInstance( $userId )->SendGoods( $gid , $number , "tenpay" , $order['price'] , $order['id'] );
					echo 'success';
				}
				echo 'fail';
			}
			else
			{
				echo 'fail';
			} 
			
		} else {
			//回调签名错误
			echo "fail";
		}
	}
}

?>