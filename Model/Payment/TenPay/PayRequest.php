<?php
//---------------------------------------------------------
//财付通即时到帐支付请求示例，商户按照此文档进行开发即可
//---------------------------------------------------------

require_once ("classes/RequestHandler.class.php");
require ("classes/client/ClientResponseHandler.class.php");
require ("classes/client/TenpayHttpClient.class.php");

class Payment_TenPay_PayRequest
{

	public static function getToken( $userId , $gid , $num )
	{
		$payLog = new ErrorLog( "TenpayRequestLog" );
		$message = $_SERVER['REQUEST_URI'];
		$payLog->addLog( $message );
		
		/* 商户号 */
		$partner = "1215481101";
		
		/* 密钥 */
		$key = "24d02e06e15cfe989464df57ab7e81e4";
		
		$attach =  $userId."*".$gid."*".$num;
		
		//4位随机数
		$randNum = rand(1000, 9999);
		
		//订单号，此处用时间加随机数生成，商户根据自己情况调整，只要保持全局唯一就行
		$out_trade_no = $userId. date("YmdHis") . $randNum;
		
		
		
		/* 创建支付请求对象 */
		$reqHandler = new RequestHandler();
		$reqHandler->init();
		$reqHandler->setKey($key);
		//$reqHandler->setGateUrl("https://gw.tenpay.com/gateway/pay.htm");
		//设置初始化请求接口，以获得token_id
		$reqHandler->setGateUrl("http://wap.tenpay.com/cgi-bin/wappayv2.0/wappay_init.cgi");
		
		
		$httpClient = new TenpayHttpClient();
		//应答对象
		$resHandler = new ClientResponseHandler();
		//----------------------------------------
		//设置支付参数 
		//----------------------------------------
		$goodsConfig = Common::getConfig( "goods" );
		$goodsConf = $goodsConfig[$gid];
		if( !$goodsConf )
		{
			return "";
		}
	
		$reqHandler->setParameter("total_fee", $goodsConf['price'] *100 );  //总金额
		//一分钱
		//$reqHandler->setParameter("total_fee", $goodsConf['price'] );  //总金额
		//用户ip
		$reqHandler->setParameter("spbill_create_ip", $_SERVER['REMOTE_ADDR']);//客户端IP
		$reqHandler->setParameter("ver", "2.0");//版本类型
		$reqHandler->setParameter("bank_type", "0"); //银行类型，财付通填写0
		
		$reqHandler->setParameter("callback_url", Common::getServerAddr()."Web/Api/api_game.php?method=Payment.tenPayReturn");//交易完成后跳转的URL
		//$reqHandler->setParameter("callback_url", "http://211.144.68.31/liuchangbing/myhero_dev/Web/Api/api_game.php?method=Payment.tenPayReturn");//交易完成后跳转的URL
		//$reqHandler->setParameter("callback_url", "http://www.qq.com");//交易完成后跳转的URL
		$reqHandler->setParameter("bargainor_id", $partner); //商户号
		$reqHandler->setParameter("sp_billno", $out_trade_no); //商户订单号
		//$reqHandler->setParameter("notify_url", "http://211.144.68.31/liuchangbing/myhero_dev/Web/Api/Pay/tenNotify.php");//接收财付通通知的URL，需绝对路径
		$reqHandler->setParameter("notify_url", Common::getServerAddr()."Web/Api/Pay/tenNotify.php");//接收财付通通知的URL，需绝对路径
		$reqHandler->setParameter("desc", $goodsConf['goodsDesc']  );
		$reqHandler->setParameter("attach", $attach );
		
		
		$httpClient->setReqContent($reqHandler->getRequestURL());
		
		//后台调用
		if($httpClient->call()) {
		
			$resHandler->setContent($httpClient->getResContent());
			//获得的token_id，用于支付请求
			$token_id = $resHandler->getParameter('token_id');
			$reqHandler->setParameter("token_id", $token_id);
			
			//请求的URL
			//$reqHandler->setGateUrl("https://wap.tenpay.com/cgi-bin/wappayv2.0/wappay_gate.cgi");
			//此次请求只需带上参数token_id就可以了，$reqUrl和$reqUrl2效果是一样的
			//$reqUrl = $reqHandler->getRequestURL(); 
			//$reqUrl = "http://wap.tenpay.com/cgi-bin/wappayv2.0/wappay_gate.cgi?token_id=".$token_id;
				
		}
		return $token_id;
	}

}
?>