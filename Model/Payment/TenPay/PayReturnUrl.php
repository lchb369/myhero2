<?php


//---------------------------------------------------------
//财付通即时到帐支付页面回调示例，商户按照此文档进行开发即可
//---------------------------------------------------------
require_once ("classes/ResponseHandler.class.php");
require_once ("classes/WapResponseHandler.class.php");

class Payment_TenPay_PayReturnURL
{
	
	public static function proccess()
	{
		
		$payLog = new ErrorLog( "TenpayReturnLog" );
		$message = $_SERVER['REQUEST_URI'];
		$payLog->addLog( $message );
		
		
		/* 密钥 */
		$key = "24d02e06e15cfe989464df57ab7e81e4";
		
		
		/* 创建支付应答对象 */
		$resHandler = new WapResponseHandler();
		$resHandler->setKey($key);
		
		//判断签名
		if($resHandler->isTenpaySign()) {
		
			//商户订单号
			$bargainor_id = $resHandler->getParameter("bargainor_id");
			//财付通交易单号
			$transaction_id = $resHandler->getParameter("transaction_id");
			//金额,以分为单位
			$total_fee = $resHandler->getParameter("total_fee");
			//支付结果
			$pay_result = $resHandler->getParameter("pay_result");
		
			if( "0" == $pay_result  ) {
				
				$string = "<br/>" . "支付成功" . "<br/>";
			
			} else {
				//当做不成功处理
				$string =  "<br/>" . "支付失败" . "<br/>";
			}
			
		} else {
			$string =  "<br/>" . "认证签名失败" . "<br/>";
		}
		echo $string;
	}
}

?>
