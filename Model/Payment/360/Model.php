<?php

/**
 * 360
 * @author Administrator
 *
 */
class Payment_360_Model
{	
	
	
	public static function getAccessToken( $code )
	{
		$url = "https://openapi.360.cn/oauth2/access_token";
		$params = array(
				'grant_type' => 'authorization_code',
				'code' => $code,
				'client_id' => 'f8689bbce83af3b9bd5e7f291b2f0411',
				'client_secret' => 'e859217c8aed8a71d7c175fcd6c7c9a1',
				'redirect_uri' => 'oob',
		);
	
		$rs = self::post_request( $url , $params );
		$result = json_decode( $rs , true );
		return $result;
	
	}
	
	/**
	 * 获取用户信息
	 * @param unknown $token
	 * @return mixed
	 */
	public static function getUserInfo( $token )
	{
		$url = "https://openapi.360.cn/user/me.json";
		$params = array(
				'access_token' => $token,
				'fields' => 'id,name,avatar,sex,area',
		);
		
		$rs = self::post_request( $url , $params );
		$result = json_decode( $rs , true );
		return $result;
		
	}
	
	
	/**
	 * 提交请求
	 * @param unknown $url
	 * @param unknown $params
	 * @return Ambigous <mixed, string>
	 */
	protected  static  function post_request( $url , $params)
	{
		$post_string = self::create_post_string(  $params );
		$ch = curl_init();
		curl_setopt( $ch , CURLOPT_POSTFIELDS , $post_string );
		curl_setopt( $ch , CURLOPT_RETURNTRANSFER , true );
		curl_setopt( $ch , CURLOPT_CONNECTTIMEOUT ,10  );
		curl_setopt( $ch , CURLOPT_TIMEOUT , 10  );
		curl_setopt( $ch , CURLOPT_POST, 1);
	
		$useragent = 'REST API PHP5 Client 1.0 (curl) ' . phpversion();
		curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
		curl_setopt( $ch , CURLOPT_URL , $url  );
		$result = curl_exec($ch);
		if( curl_errno( $ch ) > 0 )
		{
			$result = curl_error( $ch );
		}
		curl_close($ch);
		return $result;
	}
	/**
	 * 构造参数
	 * @param unknown $params
	 * @return string
	 */
	protected static function create_post_string( $params )
	{
		$post_params = array();
		foreach ($params as $key => &$val)
		{
			if( $key == "validate_string" )
			{
				$post_params[] = $key.'='.urlencode($val);
			}
			else
			{
				$post_params[] = $key.'='.$val;
			}
		}
		return implode( '&', $post_params );
	}
	
	
	
	/**
	 * 支付回调
	 */
	public static function payNotify()
	{
		 $response = $_GET; 
		 $secret = 'b5b8d8f9725d4721';
		 $sign=md5("orderId={$response['orderId']}&uid={$response['uid']}&amount={$response['amount']}&actualAmount={$response['amount']}&extraInfo={$response['extraInfo']}&success={$response['success']}&secret={$secret}");



		 if( $sign != $response['sign'] )
		 {
		 	return false;
		 }
		 else 
		 {
		 	$payLog = new ErrorLog( "PayNotify51" );
		 	$payLog->addLog(  "payNotify:OK" );
		 	
		 	if( $response['success'] == 1 )
		 	{
		 		return false;
		 	}
		 	
			$ugArr = explode( "&" , $response['extraInfo']);
		 	$uArr = explode( "=" , $ugArr[0]);
			$gArr = explode( "=" , $ugArr[1]);
			$userId = $uArr[1];
			$addCoin = $amount =  $response['amount'];

		//	$response['orderId'] = rand( 1 ,10000000);
			//创建定单
		 	$order = array(
		 			'id' =>$response['orderId'],
		 			'goodsName' => 9999,
		 			'goodsNum' => 1,
		 			'price' => $response['amount'],
		 			'discount' => 0,
		 			'totalPrice' =>  $response['actualAmount'],
		 			'buyerId' => $response['uid'],
		 			'tradeTime' => $_SERVER['REQUEST_TIME'],
		 			'addTime' => $_SERVER['REQUEST_TIME'],
		 			'email' =>'',
		 			'platform' => "51",
		 			'status' => 1,
		 			'thirdId' =>"",
		 	);
		 	
		 	
		 	$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		 	if( $rs == true )
		 	{
		 		$payLog = new ErrorLog( "addCoin" );
		 		$orderData = Data_Order_Model::getInstance( $userId )->getData();
		 		$orderCount = count( $orderData );
		 		
		 		if( Data_Order_Model::getInstance( $this->userId )->isFirstRecharge()   )
		 		{
		 			$payLog->addLog(  "uid:".$userId."addCoin*2:".$addCoin."firstRecharge" );
		 			$addCoin = $addCoin * 2;
		 		
		 		}
		 			
		 		$payLog->addLog(  "uid:".$userId."addCoin:".$addCoin."orderCount".$orderCount );
		 		/**
		 		 * 运营统计：充值记录
		 		*/
		 		Stats_Model::addPayUser(
		 		$userId ,
		 		$_SERVER['REQUEST_TIME'] ,
		 		$amount  ,	//价格
		 		$addCoin , 	//元宝数
		 		"999" ,	//商口ID
		 		$orderCount,
		 		"51"
		 		);
		 		
		 		/**
				 * EDAC数据中心统计
		 		 */
		 		Stats_Edac::recharge( $userId, $response['orderId'], $amount, $addCoin, "360" );
		 		
		 		
		 		$status = User_Info::getInstance( $userId )->changeCoin( $addCoin , '充值' );
		 		if( $status  == true )
		 		{
		 			return true;
		 		}
		 		else 
		 		{
		 			return false;
		 		}
		 			
		 	}
		 	else
		 	{
		 		return true;
		 	}
		 	return false;
		 }
		 
		
	}
	
	
	

}
