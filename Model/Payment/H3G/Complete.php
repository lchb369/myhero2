<?php include( __DIR__.'/TplHeader.php'); ?>
<body>
<div class="gridContainer clearfix">
  <div id="div1" class="fluid">
    <div class="title01">    
                    <table width="100%" border="0">
                    <tr>
                    <td id="btn" width="10%"><!-- <a href="#"><img src="<?php echo self::$srcPath; ?>images/refresh.png"  alt=""/></a> --></td>
                    <td width="70%"></td>
                    <td id="btn" width="10%"><!-- <a href="#"><img src="<?php echo self::$srcPath; ?>images/home.png"  alt=""/></a> --></td>
                    </tr>
                    </table>
      </div>
        <div class="title02">消費資訊確認</div>
        <div>
          <p class="title03"><?php echo $tradeTitle; ?></p>
        </div>
            <div class="title03">
            <table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" class="table01">
      <tr>
        <td class="word03" width="35%">交易序號：</td>
        <td class="word05" width="65%"><?php echo $tradeId; ?></tr>
      <tr>
        <td class="word03" >交易代碼：</td>
        <td class="word05"><?php echo $tradeCode; ?></td>
      </tr>
      <tr>
        <td class="word03" >交易訊息：</td>
        <td class="word05"><?php echo $tradeInfo; ?></td>
      </tr>
      <tr>
        <td class="word03" >儲值時間：</td>
        <td class="word05"><?php echo $tradeTime ? date('Y年m月d日 H:i', $tradeTime) : ""; ?></td>
      </tr>
      <tr style="display: none;"><td>
     	 <?php include( __DIR__.'/TplUserReqData.php'); ?>
     	 <input type="text" id="method" name="method" value="Payment.Complete" >
      </td></tr>
      </table>
  </div>
  <div class="word04"><?php echo $tradeDesc; ?></div>
  <div class="space"></div>
  </div>
</div>
</body>
</html>
