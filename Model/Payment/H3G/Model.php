<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

//---------------------------------------------------------
//H3G
//---------------------------------------------------------
class Payment_H3G_Model extends Logical_Abstract
{
	
	private static $srcPath = 'Pay/H3G/';
	private static $wsdl = 'http://wsp3.three.com.hk/axis/services/messaging?wsdl';
	private static $FromMobile = '63355000';
	
	private static $AccountID_PRE = 'WECAN';
	private static $UserID = 'wecan';
	private static $Passwd_SUF = 'token';
//	private static $ServiceID = 'CHG/DIP';
	private static $ServiceID = 'DIP';
	private static $ServiceIDSMS = 'SMS';
	
	private static $CP = 'gameperhit';
	private static $CPrefid = '001';
	private static $Sid = '232452';
	private static $Aid = '1';
	private static $ID = 'sghmg_token';
	
	private $goodsInfo = array();
	private $userReqData = array();
	
	/**
	 * 单例对象
	 * @var	User_Model[]
	 */
	protected static $singletonObjects;
	
	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
		
		$this->userReqData = array(
			'os' => strval($_REQUEST['os']),
			'refer' => strval($_REQUEST['refer']),
			'timestamp' => intval($_REQUEST['timestamp']),
			'arg' => strval($_REQUEST['arg']),
			'sig' => strval($_REQUEST['sig']),
			'sid' => intval($_REQUEST['sid']),
			'pf' => strval($_REQUEST['pf']),
			'method' => strval($_REQUEST['method']),
			'uid' => intval($_REQUEST['uid']),
			'session' => strval($_REQUEST['session']),
			'gid' => intval($_REQUEST['gid']),
		);
		
		$goodsConfig = Common::getConfig('goods');
		$this->goodsInfo = $goodsConfig[$this->userReqData['gid']];
		
		$this->verify();
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Payment_H3G_Model
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	public function confirm()
	{
		$tradePrice = $this->goodsInfo['H3G'];
		$tradeProductName = $this->goodsInfo['goodsDesc'];
		
		$userProfile = User_Profile::getInstance($this->userId)->getData();
		$tradeNick = $userProfile['nickName'];
		
		include( __DIR__.'/Confirm.php');
	}

	public function mobileNumber()
	{
		$tradePrice = $this->goodsInfo['H3G'];
		$tradeProductName = $this->goodsInfo['goodsDesc'];
		
		include( __DIR__.'/MobileNumber.php');
	}
	
	public function auth($mobileNumber)
	{
		do
		{
			$authCode = $this->getAuthCode();
			if(!$authCode) $this->verify('校驗碼獲取失敗');
			
			$ret = $this->sendSMS($mobileNumber, '您的消費交易密碼為'.$authCode, true);
			if(!$ret) break;

			include( __DIR__.'/Auth.php');
			
			return true;
			
		}while(0);
		
		$this->verify('異常的手機號碼');
	}
	
	public function complete($mobileNumber, $authCode)
	{
		
		$tradeDesc = '儲值失敗';
		$tradeTitle = '交易完成';
		$tradeInfo = '交易失败';
		
		do
		{
			if(empty($mobileNumber)) $this->verify('無效的手機號碼');
			//3 客户在指定时间内 於app填写验证码
			if(empty($authCode)) $this->verify('無效的驗證碼');
			
			if(!$this->checkAuthCode($authCode)) break;
			
			$webService = self::getWebServiceInstance(self::$wsdl);
			
			//4 验证码正确的话，就购买完成 (6.17 用来通知我们系统向该门号收费)
			$func = 'MWBillingCCG';
			$funcArgs = array_merge($this->getCommonArgs(), array(
				'Msisdn' => strval($mobileNumber),
				'CP' => self::$CP,
				'CPrefid' => self::$CPrefid,
				'Sid' => self::$Sid,
				'SAsuffix' => $this->goodsInfo['H3GId'],
				'Aid' => self::$Aid,
				'ID' => self::$ID,
			));
			$payLog = new ErrorLog( "H3GOrder" );
			$payLog->addLog( "__soapCall: [".$func."]".json_encode($funcArgs) );
			$result = $webService->__soapCall($func, $funcArgs);
			$result = $this->checkWebServiceResult($result, $func);
			if(!$result) break;
			$payLog->addLog("notifyPaySuccess");
			
			if(!$this->createOrder()) break;
			
			//5 (最好)完成後再补简讯提示客户购买成功 (6.1 & 6.2)
			$tradePrice = $this->goodsInfo['H3G'];
			$tradeProductName = $this->goodsInfo['goodsDesc'];
				
			$userProfile = User_Profile::getInstance($this->userId)->getData();
			$tradeNick = $userProfile['nickName'];
				
			$this->sendSMS($mobileNumber, '您在串燒三國中暱稱為【'.$tradeNick.'】的帳號成功購買了【'.$tradeProductName.'】');
			
			$tradeDesc = '※ 你已經成功儲值 HK$'.$tradePrice.'　<br/>費用會於您的3香港流動帳單收取，謝謝 ※';
			$tradeTitle = '交易完成';
			$tradeInfo = '交易成功';
			
		}while(0);
		
		$tradeTime = $_SERVER['REQUEST_TIME'];
		
		include( __DIR__.'/Complete.php');
	}
	
	private function verify($errorMsg = '')
	{
		do{
			if(!empty($errorMsg))
			{
				$tradeDesc = $errorMsg;
				break;
			}
			
			if(empty($this->userId))
			{
				$tradeDesc = '無效的帳號';
				break;
			}
			
			$gid = $this->userReqData['gid'];
			$goodsConfig = Common::getConfig( "goods" );
			$goodsInfo = $goodsConfig[$gid];
			if(empty($goodsInfo['H3G']))
			{
				$tradeDesc = '不存在的商品';
				break;
			}
			
			return true;
		}while(0);
		
		$tradeTitle = '交易失敗';
		$tradeInfo = '交易失敗';
			
		include( __DIR__.'/Complete.php');
		exit;
	}
	
	private function createOrder()
	{
		$payLog = new ErrorLog( "H3GOrder" );
		
		//充值
		$orderId = Payment_Model::getInstance($this->userId)->getOrderId($this->goodsInfo['goodsId']);
		
		if(empty($orderId)) return 0;
		
		$thirdOrderId = '';
			
		$gid = $this->goodsInfo['goodsId'];
		$price = $this->goodsInfo['price'];
		$userId = $this->userId;
			
		$order = array(
			'id' => $orderId,
			'goodsName' => $gid,
			'goodsNum' => 1,
			'price' => $price,
			'discount' => 0,
			'totalPrice' => $price,
			'buyerId' => $userId,
			'tradeTime' => $_SERVER['REQUEST_TIME'],
			'addTime' => $_SERVER['REQUEST_TIME'],
			'email' => '',
			'platform' => "H3G",
			'status' => 1,
			'thirdId' => $thirdOrderId,
		);
		$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		
		if( $rs == true )
		{
			$payLog->addLog( "createOrder:".json_encode( $order ) );
			Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , "H3G" , $order['price'] , $order['id'] );
			return 1;
		}
		else
		{
			return 0;
		}
		return 0;
		
	}
	
	private static function & getWebServiceInstance($url)
	{
		static $instances = array();
		
		if(!isset($instances[$url]))
		{
			$instances[$url] = new SoapClient($url);
		}
		
		return $instances[$url];
	}
	
	private function getCommonArgs($type = '')
	{
		if($type == 'sms')
		{
			$ret = array(
				'AccountID' => 'WECANSMS',
				'UserID' => self::$UserID,
				'Passwd' => 'sms',
				'ServiceID' => self::$ServiceIDSMS,
			);
		}
		else
		{
			$ret = array(
				'AccountID' => 'WECAN'.str_pad($this->goodsInfo['coin'], 3, '0', STR_PAD_LEFT),
				'UserID' => self::$UserID,
				'Passwd' => $this->goodsInfo['coin'].'token',
				'ServiceID' => self::$ServiceID,
			);
		}
		return $ret;
	}
	
	private function getAuthCode()
	{
		$cache = Common::getCache();
		$key = 'H3GAuthCode_'.$this->userId;
		if(!!$lock = Helper_Common::addLock($key))
		{
			$maxTryTime = 10;
			$tryTime = 0;
			while(1)
			{
				$authCode = Helper_String::randStr(6, true);
				$lastCode = $cache->get($key);
				if(!$lastCode || $lastCode != $authCode)
				{
					$cache->set($key, $authCode, 60 * 10);
					break;
				}
				unset($authCode);
				if(++$tryTime > $maxTryTime) break;
			}
		}
		
		if($lock) Helper_Common::delLock($key);
		
		return $authCode;
	}
	
	private function checkAuthCode($authCode)
	{
		$ret = false;
		
		do
		{
			$cache = Common::getCache();
			$key = 'H3GAuthCode_'.$this->userId;
			$lastCode = $cache->get($key);
			if($lastCode && $lastCode == $authCode)
			{
				$ret = true;
				$cache->delete($key);
			}
			
		}while(0);
		
		return $ret;
	}
	
	private function sendSMS($mobileNumber, $msg, $forceExit = false)
	{
		$ret = false;
		
		do
		{
			$webService = self::getWebServiceInstance(self::$wsdl);
				
			//1 校验手机号 (6.30 来确定该客人是否3香港客户 还有该门号是否已经被冻结)
			$func = 'dipOperatorAndStatus';
			$funcArgs = array_merge($this->getCommonArgs(), array(
				'Msisdn' => strval($mobileNumber),
			));
			$payLog = new ErrorLog( "H3GOrder" );
			$payLog->addLog( "__soapCall: [".$func."]".json_encode($funcArgs) );
			$result = $webService->__soapCall($func, $funcArgs);
			$result = $this->checkWebServiceResult($result, $func);
			if(!$result) break;
		
			//2 你们发验证码简讯给客户 (6.1)
			$func = 'sendSMS';
			$funcArgs = array_merge($this->getCommonArgs('sms'), array(
				'FromMobile' => self::$FromMobile,
				'ToMobile' => strval($mobileNumber),
				'Message' => $msg,
			));
			$payLog = new ErrorLog( "H3GOrder" );
			$payLog->addLog( "__soapCall: [".$func."]".json_encode($funcArgs) );
			$result = $webService->__soapCall($func, $funcArgs);
			$result = $this->checkWebServiceResult($result);
			if(!$result)
			{
				if($forceExit) $this->verify('短信發送失敗');
				break;
			}
		
			//3 校验短信是否发送成功 (6.2)
			//要等8-10s发送，短链接那就不做了额
// 			$func = 'getSMSByReference';
// 			$funcArgs = array_merge($this->getCommonArgs(), array(
// 				'MessageID' => $result['reference'],
// 			));
// 			$payLog = new ErrorLog( "H3GOrder" );
// 			$payLog->addLog( "__soapCall: [".$func."]".json_encode($funcArgs) );
// 			$result = $webService->__soapCall($func, $funcArgs);
// 			$result = $this->checkWebServiceResult($result);
// 			if(!$result)
// 			{
// 				if($forceExit) $this->verify('短信發送校驗失敗');
// 				break;
// 			}
		
			$ret = true;
				
		}while(0);
		
		return $ret;
	}
	
	
	private function checkWebServiceResult($resultStr, $func = '')
	{
		$payLog = new ErrorLog( "H3GOrder" );
		
		$auth = false;
		do{
			if(empty($resultStr))
			{
				$payLog->addLog( "authFail: return null" );
				break;
			}
			$xml = simplexml_load_string($resultStr);
			if(empty($xml))
			{
				$payLog->addLog( "authFail: convert to xml fail: ".$resultStr );
				break;
			}
			foreach($xml->result->attributes() as $k => $v) $result[$k] = strval($v);
			
			if($func == 'MWBillingCCG')
			{
				if(empty($result) || $result['desc'] != 'success')
				{
					$payLog->addLog( "authFail: return desc is not successful: ".$resultStr );
					break;
				}
			}
			elseif($func == 'dipOperatorAndStatus')
			{
				if(empty($result) || $result['code'] != '1000' || $result['desc'] == 'Not 3')
				{
					$payLog->addLog( "authFail: return desc is not successful: ".$resultStr );
					break;
				}
			}
			else
			{
				if(empty($result) || $result['code'] != '1000')
				{
					$payLog->addLog( "authFail: return code is not successful: ".$resultStr );
					break;
				}
			}
			
			$auth = $result;

		}while(0);
		
		if($auth) $payLog->addLog( "authSuccess: ".$resultStr );
		
		return $auth;
	}
	
}

?>