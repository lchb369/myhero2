<?php include( __DIR__.'/TplHeader.php'); ?>
<body>
<script type="text/javascript">
function confirm(){
	document.getElementById('btnSubmit').disabled = true;
	document.getElementById('mainForm').submit();
}

function resetAuthCode()
{
	document.getElementById('btnReSendSMS').disabled = true;
	document.getElementById('method').value = 'Payment.H3GAuth';
	confirm();		
}
</script>
<div class="gridContainer clearfix">
  <div id="div1" class="fluid">
        <div class="title01">    
                    <table width="100%" border="0">
                    <tr>
                    <td id="btn" width="10%"><!-- <a href="#"><img src="<?php echo self::$srcPath; ?>images/refresh.png"  alt=""/></a> --></td>
                    <td width="70%"></td>
                    <td id="btn" width="10%"><!-- <a href="#"><img src="<?php echo self::$srcPath; ?>images/home.png"  alt=""/></a> --></td>
                    </tr>
                    </table>
        </div>
        <div class="title02">消費資訊確認</div>
        <div>
            <p class="title03">請輸入交易密碼</p>
        </div>
        <div class="title03">
         <form action="?" method="get" id="mainForm">
            <table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" class="table01">
            <tr>
            <td class="word03" width="35%">手機號碼：</td>
            <td class="word05" width="65%"><?php echo $mobileNumber; ?></tr>
            <tr>
            <td class="word03" >交易密碼：</td>
            <td><input class="word05" name="authCode" type="text" size="15" maxlength="10"></td>
            </tr>
            <tr>
            <td colspan="2" class="word04" >※ 請將6位數密碼輸入「交易密碼」欄內(密碼由短訊傳送)※ </td>
            </tr>
            <tr>
            <td>
            </td>
            </tr>
            <tr style="display: none;"><td>
            	<?php include( __DIR__.'/TplUserReqData.php'); ?>
            	<input type="text" id="method" name="method" value="Payment.H3GComplete" >
            	<input type="text" name="mobileNumber" value="<?php echo $mobileNumber; ?>" >
            </td></tr>
            </table>
        </div>
        </form>
        <div class="title03">          
            <table class="table02" width="100%" border="0" align="center" cellpadding="10">
            <tr>
            <td style="text-align: center"><a href="#" class="css_btn_class01" onclick="confirm();" id="btnSubmit">確定</a></td>
            <td style="text-align: center"><a href="#" class="css_btn_class01">取消</a></td>
            </tr>
            <tr>
              <td colspan="2" style="text-align: center" class="word03" >
              	<a href="#" class="css_btn_class02" onclick="resetAuthCode();" id="btnReSendSMS">重發短訊</a>
              	</td>
              </tr>
            </table>
        </div>
    <div class="space"></div>
    <div>
          <p class="table03">溫馨提示</p>
     </div>
     <div>
		<p class="word06">請輸入短信內提供的交易密碼並按「確認」，才能成功購買。閣下於收到短訊後需於10分鐘內填寫密碼，10分鐘後密碼將會失效，閣下可選擇「重發短訊」再次取得交易密碼。
		</p>
    </div>
  </div>
</div>
</body>
</html>
