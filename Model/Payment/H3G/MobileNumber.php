<?php include( __DIR__.'/TplHeader.php'); ?>
<body>
<script type="text/javascript">
function confirm(){
	document.getElementById('btnSubmit').disabled = true;
	document.getElementById('mainForm').submit();
}
</script>
<div class="gridContainer clearfix">
  <div id="div1" class="fluid">
    <div class="title01">    
                    <table width="100%" border="0">
                    <tr>
                    <td id="btn" width="10%"><!-- <a href="#"><img src="<?php echo self::$srcPath; ?>images/refresh.png"  alt=""/></a> --></td>
                    <td width="70%"></td>
                    <td id="btn" width="10%"><!-- <a href="#"><img src="<?php echo self::$srcPath; ?>images/home.png"  alt=""/></a> --></td>
                    </tr>
                    </table>
      </div>
    <div class="title02">消費資訊確認</div>
    <div>
      <p class="title03">請輸入您的手機號碼</p>
    </div>
    <div class="title03">
    <form action="?" method="get" id="mainForm">
    <table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" class="table01">
      <tr>
        <td class="word03" width="35%">結算金額：</td>
        <td class="word05" width="65%">HK$<?php echo $tradePrice; ?>
      </tr>
      <tr>
        <td class="word03" >商品名稱：</td>
        <td class="word05" ><?php echo $tradeProductName; ?></td>
      </tr>
      <tr>
        <td class="word03" >電信公司：</td>
        <td class="word05" >3香港(香港電信門號)</td>
      </tr>
      <tr>
        <td class="word03" >手機號碼：</td>
        <td><input class="word05" type="text" name="mobileNumber" size="15" maxlength="20" ></td>
      </tr>
      <tr style="display: none;"><td>
      	<?php include( __DIR__.'/TplUserReqData.php'); ?>
      	<input type="text" id="method" name="method" value="Payment.H3GAuth" >
      </td></tr>
    </table>
    </form>
    <div class="agree" style="text-align: center"></div>
  </div>
    <div class="btn_space">
    <a href="#" class="css_btn_class01" onclick="confirm();" id="btnSubmit">確 定</a>
    </div>
    <div class="word04">※ 此步驟需在3香港手機網絡內，方可成功購買 ※<br>請輸入8位數字的手機號碼
    </div>
    <div class="space"></div>
    <div>
          <p class="table03">溫馨提示</p>
     </div>
     <div>
		<p class="word06">系統將會發送免費的交易密碼短訊至閣下填寫的「3香港」手機號碼，在讀取短訊後，請開啟此瀏覽器來回到購買頁面，並填上交易密碼後才可購買成功。
收到交易密碼短訊後需於10分鐘內填寫密碼，10分鐘後密碼將會失效，閣下可選擇「重發短訊」再次取得交易密碼。</p>
    </div>
  </div>
</div>
</body>
</html>
