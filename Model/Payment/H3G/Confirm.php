<?php include( __DIR__.'/TplHeader.php'); ?>
<body>
<script type="text/javascript">
function confirm(){
	document.getElementById('btnSubmit').disabled = true;
	document.getElementById('mainForm').submit();
}
</script>
<div class="gridContainer clearfix">
  <div id="div1" class="fluid">
          <div class="title01">    
                    <table width="100%" border="0">
                    <tr>
                    <td id="btn" width="10%"><!-- <a href="#"><img src="<?php echo self::$srcPath; ?>images/refresh.png"  alt=""/></a> --></td>
                    <td width="70%"></td>
                    <td id="btn" width="10%"><!-- <a href="#"><img src="<?php echo self::$srcPath; ?>images/home.png"  alt=""/></a> --></td>
                    </tr>
                    </table>
          </div>
          <div class="title02">消費資訊確認</div>
          <div>
          <p class="table03">請確認你的消費資訊</p>
          </div>
    <div calss="text01">
    <form action="?" method="get" id="mainForm">
    <table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" class="table01">
      <tr>
        <td class="word03" width="35%">購買金額：</td>
        <td class="word05" width="65%">HK$<?php echo $tradePrice; ?>
      </tr>
      <tr>
        <td class="word03" >付費類型：</td>
        <td class="word05" >3香港 手機流動帳單</td>
      </tr>
      <tr>
        <td class="word03" >儲值遊戲：</td>
        <td class="word05" >串燒三國</td>
      </tr>
      <tr>
        <td class="word03" >角色名稱：</td>
        <td class="word05" ><?php echo $tradeNick; ?></td>
      </tr>
      <tr>
        <td class="word03" >商品名稱：</td>
        <td class="word05" ><?php echo $tradeProductName; ?></td>
      </tr>
      <tr style="display: none;"><td>
     	 <?php include( __DIR__.'/TplUserReqData.php'); ?>
     	 <input type="text" id="method" name="method" value="Payment.H3GMobileNumber" >
	  </td></tr>
    </table>
    </form>
  </div>
    <div class="btn_space"><a href="#" class="css_btn_class01" onclick="confirm();" id="btnSubmit">確 定</a></div>
    <div class="space"></div>
    <div>
          <p class="table03">溫馨提示</p>
     </div>
     <div>
		<p class="word06">請確定以上的消費資訊，並於下一頁輸入閣下正在使用的「3香港」手機號碼，系統將會發送免費的短訊，短訊將提供是次的交易密碼，閣下需回到購買頁面填寫正確的交易密碼後，才能成功購買。閣下於收到短訊後需於10分鐘內填寫密碼，10分鐘後密碼將會失效，閣下可選擇「重發短訊」再次取得交易密碼。
		</p>
    </div>
  </div>
</div>
</body>
</html>
