<!doctype html>
<!--[if lt IE 7]> <html class="ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Untitled Document</title>
<link href="css/boilerplate.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<!-- 
To learn more about the conditional comments around the html tags at the top of the file:
paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/

Do the following if you're using your customized build of modernizr (http://www.modernizr.com/):
* insert the link to your js here
* remove the link below to the html5shiv
* add the "no-js" class to the html tags at the top
* you can also remove the link to respond.min.js if you included the MQ Polyfill in your modernizr build 
-->
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="js/respond.min.js"></script>
</head>
<body>
<div class="gridContainer clearfix">
  <div id="div1" class="fluid">
    <div class="title01">    
                    <table width="100%" border="0">
                    <tr>
                    <td id="btn" width="10%"><a href="#"><img src="images/refresh.png"  alt=""/></a></td>
                    <td width="70%"></td>
                    <td id="btn" width="10%"><a href="#"><img src="images/home.png"  alt=""/></a></td>
                    </tr>
                    </table>
      </div>
        <div class="title02">消費資訊確認</div>
        <div>
          <p class="title03">交易完成</p>
        </div>
            <div class="title03">
            <table class="table01" width="100%" border="0" align="center" cellpadding="10">
      <tr>
        <td class="word03" width="30%">交易序號：</td>
        <td class="word05" width="61%">0000000000</tr>
      <tr>
        <td class="word03" >交易代碼：</td>
        <td class="word05">0000000000</td>
      </tr>
      <tr>
        <td class="word03" >交易訊息：</td>
        <td class="word05">交易成功</td>
      </tr>
      <tr>
        <td class="word03" >儲值時間：</td>
        <td class="word05">2013年6月26日 下午 09:56</td>
      </tr>
      </table>
  </div>
  <div class="word04">※ 你已經成功儲值 HK$XXX　費用會於您的3HK流動帳單收取，謝謝 ※</div>
  <div class="space"></div>
  </div>
</div>
</body>
</html>
