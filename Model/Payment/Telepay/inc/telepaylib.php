<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

	global $_POST;
	global $BasicSet, $ServerSet;
	
	$BasicSet = array();
	
	// Mandatory Parameter
	$BasicSet["Version"]			= "1.0.0";
	$BasicSet["ClientType"]			= "Telepay-1.0.0";
	
	// Optional Parameter
	$BasicSet["Charset"]			= "Big5";
	$BasicSet["PreferedCharset"]	= "Big5";
	$BasicSet["PreferedLanguage"]	= "tw";
	
	///////////////////////////////////
	// Setting for Danal Test
	$ServerSet = array();
	$ServerSet["Protocol"]			= "http"; 			// YeZin Protocol
	$ServerSet["Host"]				= "210.244.224.5";	// YeZin Host
	$ServerSet["Port"]				= 8080;				// YeZin Port
	$ServerSet["URL"]				= "/yezin/telepay";	// YeZin URL

	///////////////////////////////////
	// Change it
	$ServerSet["CURL"] 	= "/usr/bin/curl -k ";
	$ServerSet["DefaultMsg"]		= "Can't connect Telepay";		// Setting CP's default error message.
	$ServerSet["LogURL"]			= "logs/";			// Setting CP's log directory path

	function CallYeZin($arr,$debug=false) {
		global $BasicSet, $ServerSet;
		global $SignSet;
	
		$url = MakeURL();
		$option = MakeParam($BasicSet) . "&" . MakeParam($arr);
		$arg = $url.$option;
		
		$resYeZin = Parsor(HttpExecute($arg, $debug));
		return $resYeZin;
	}
	
	function HttpExecute($arg, $debug=false){
		global $ServerSet;
		
		$cmd = $ServerSet["CURL"] . "--connect-timeout 5 --max-time 60 \"" . $arg . "\"";
		exec($cmd, $out, $ret);
		
		if ( $debug ) {
			echo("Exec : ".trim($cmd)."\n");
			echo("Ret : $ret\n");
			
			for ( $i=0 ; $i < count($out) ; $i++ ) 
				echo("Out Line$i: ".trim($out[$i])."\n");
		}
		
		if($ret)
			return "Result=-2&ReasonCode=299900000000000&ReasonText=" . $ServerSet["DefaultMsg"];
		
		return $out;		
	}
	
	function MakeURL() {
		global $BasicSet, $ServerSet;
		global $ErrorCode, $ErrorMessage;
		global $_POST;
	
		$PROTOSET = array("http","https");
	
		if ( !in_array($ServerSet["Protocol"],$PROTOSET) ) {
			$ErrorCode = 101;
			$ErrorMessage  = "Invalid Protocol";
			return "";
		}
	
		if ( strlen(trim($ServerSet["Port"])) != 0 )
			$Ports = ":".$ServerSet["Port"];
		else
			$Ports = "";

		if ( isset($_POST["SessionID"]) ) 
			$ret = $ServerSet["Protocol"] . "://" . $ServerSet["Host"] . $Ports . $ServerSet["URL"]
				. ";jsessionid=" . $_POST["SessionID"] . "?";
		else
			$ret = $ServerSet["Protocol"] . "://" . $ServerSet["Host"] . $Ports . $ServerSet["URL"] . "?";
	
		return $ret;
	}
	
	function MakeParam($arr,$ext=array(),$sort=false) {
		$ret = array();
		$keys = array_keys($arr);
	
		if ( $sort ) sort($keys);
	
		for($i=0;$i<count($keys);$i++) {
			$key = $keys[$i];
			if ( in_array($key,$ext) || trim($key) =="" ) continue;
			array_push($ret,$key."=".JavaURLEncode($arr[$key]));
		}
	
		return join("&",$ret);
	}
	
	function Parsor($str, $sep1="&", $sep2="=", $decode=true) {
	
		$Out=array();
		$in="";
	
		if ( (string)$str == "Array" ) {
			for( $i=0;$i<count($str);$i++)
				$in .=$str[$i].$sep1;
		} else $in = $str;
	
		$tok = explode($sep1,$in);
	
		for($i=0;$i<count($tok);$i++) {
			$tmp = explode($sep2,$tok[$i]);
	
			$NAME = trim($tmp[0]);
			$VALUE = trim($tmp[1]);
			for($j=2;$j<count($tmp);$j++)
				$VALUE.= $sep2.trim($tmp[$j]);
	
			$Out[$NAME] = $decode?urldecode($VALUE):$VALUE;
		}
		return $Out;
	}
	
	function JavaURLEncode($str) {
		return str_replace("%2A","*",urlencode($str));
	}

	function MakeNewArray($Arr,$ext=array()) {
		$Out = array();
		$KEYS = array_keys($Arr);
		for($i=0;$i<count($KEYS);$i++) {
			$key=$KEYS[$i];
			if(!in_array($key,$ext))
				$Out[$key] = $Arr[$key];
		}

		return $Out;
	}

	function MakeFormInput($arr,$ext=array(), $deg=false) {
		$keys = array_keys($arr);

		for($i=0;$i<count($keys);$i++) {

			$key = $keys[$i];
			if ( trim($key) == "" ) continue;
			else if( trim($key) == "MPAuthenURL") continue;

			if ( !in_array($key,$ext) ){
				echo("<INPUT TYPE=\"HIDDEN\" NAME=\"".$key."\" VALUE=\"".$arr[$key]."\">\n");
				if($deg) fputs($fp, $key . "-" . $arr[$key]."\n");
			}
		}
	}

	function MakeAddtionalInput($Trans,$HTTPVAR,$Names) {
		while( $name = array_pop($Names) )
			$Trans[$name] = $HTTPVAR[$name];

		return $Trans;
	}
	
?>
