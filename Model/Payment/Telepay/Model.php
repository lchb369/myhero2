<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

//---------------------------------------------------------
//Telepay
//---------------------------------------------------------
class Payment_Telepay_Model
{
	
	public static function ready( $uId , $gid , $orderId )
	{
		$goodsConfig = Common::getConfig( "goods" );
		
		//没2000面额，用1500代替
		if( intval( $gid ) == 14 ) $gid = 13;
		 
		$goodsInfo = $goodsConfig[$gid];
		
		if( !empty( $goodsInfo['Telepay'] ) )
		{
			//$itemCode = "AA0019999900000";
			$itemCode = $goodsInfo['TelepayId'];
			$goodsDesc = $goodsInfo['goodsDesc'];
			$goodsAmount = $goodsInfo['Telepay'];
		}
		
		include( __DIR__.'/Ready.php');
	}
	
	public static function cpcgi()
	{
		include( __DIR__.'/CPCGI.php');
	}

	public static function success()
	{
		include( __DIR__.'/Success.php');
	}
	
	private static function createOrder( $data )
	{
		//充值
		$orderId = $data['orderId'];
		
		$thirdOrderId = $data['thirdId'];

		//订单是否存在
		$cache = Common::getCache();
		$orderCache = $cache->get( $orderId );
		if( empty( $orderCache ) ) return 0;
		$orderCache = json_decode( $orderCache , true );
		if( empty( $orderCache ) ) return 0;

		//用户是否存在
		//$userId = intval( $Character_ID );
		$userId = intval( $orderCache['uid'] );
		if( User_Model::exist( $userId ) == false ) return 0;
		
			
		//是否存在商品
		$goodsConfig = Common::getConfig( "goods" );
		$gid=$orderCache['goodsName'];
		if( empty( $goodsConfig[$gid] ) )
		{
			return 0;
		}
			
		//是否已经发送过
		$status = Data_Order_Model::getInstance(  $userId )->checkOrderStatus( $orderId );
		if( $status == 1 )
		{
			return 2;
		}
			
		$price = $goodsConfig[$gid]['price'];
			
		$order = array(
				'id' => $orderId,
				'goodsName' => $gid,
				'goodsNum' => 1,
				'price' => $price,
				'discount' => 0,
				'totalPrice' => $price,
				'buyerId' => $userId,
				'tradeTime' => $_SERVER['REQUEST_TIME'],
				'addTime' => $_SERVER['REQUEST_TIME'],
				'email' => '',
				'platform' => "Telepay",
				'status' => 1,
				'thirdId' => $thirdOrderId,
		);
		$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		
		if( $rs == true )
		{
			$payLog = new ErrorLog( "telepayLog" );
			$payLog->addLog( "createOrder:".json_encode( $order ) );
			Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , "Telepay" , $order['price'] , $order['id'] );
			return 1;
		}
		else
		{
			return 0;
		}
		return 0;
		
	}
	
}

?>