<?php
if( !defined( 'IN_INU' ) )
{
	return;
}
	
	header("Pragma: No-Cache");

	include( __DIR__."/inc/telepaylib.php" );

	$TransP = array();
	
	/////////////////////////////////////////////////////////////////////////////////////////
	// Billing step - set cp's information
	/////////////////////////////////////////////////////////////////////////////////////////
	$TransP["TxType"]		= "P";

	// CP infomation
	$TransP["P_CPID"]		= "001001000416000"; //this cpid is test cpid
	$TransP["P_CPPassword"]	= "74eTU36P";
	$TransP["P_CPSiteURL"]	= "http://www.telepay.com.tw"; // this url is cp's master URL

	// Item Infomation
	$TransP["P_Currency"]	= "TWD";
	$TransP["P_OrderID"]		= $orderId; // cp's order id
	$TransP["P_OrderStamp"]	= date("YmdHis");	// cp's order stamp
	$TransP["P_ItemName"]	= iconv("UTF-8","big5", $goodsDesc ); // item name
	$TransP["P_ItemCode"]	= $itemCode; // item's code//this code is testItem.
	$TransP["P_Amount"]		= $goodsAmount; // item's cost
	$TransP["P_VATOption"]	= "A"; // VAT Type A or B or C - See Document
	$TransP["P_CPUserID"]	= $uId; // user's id in cp site

	// CP's Last URL
	// You must change...
	//$TransP["P_RelayURL"]	= "http://www.telepay.com.tw/CPCGI.php";
	$TransP["P_RelayURL"]	= Common::getServerAddr()."Web/Api/Pay/TelepayCPCGI.php";

	$BackURL 	= "#"; // link URL when User click "Cancel"
	$Buffer 	= "This Transaction is Test"; // You can get Buffer in CPCGI Page.
	///////////////////////
	// Values for Display
	$Disp = array();
	$Disp["Buffer"]=$Buffer;
	$Disp["BackURL"]=$BackURL;

	$Res = array( 'Result' => 1 , "ReasonText" => iconv("UTF-8","big5","�Τᤣ�s�b") );
	if( User_Model::exist( $uId ) )
	{
		$Res = CallYeZin($TransP, false);
	}
	
?>
<?php
	if ( $Res["Result"] === "0" )
	{
		$toDeliver = MakeNewArray($Res,array("Result","ReasonCode","ReasonText","ReasonTextCharset","ReasonTextLang"));
		$toDeliver = array_merge($toDeliver,$Disp);
		//http://web.telepay.com.tw/mobileTelepay/taiwan/Start.php
		//http://web.telepay.com.tw/Telepay/Start.php3
?>
	<body>
		<form name='ReadyForm' action='http://web.telepay.com.tw/mobileTelepay/taiwan/Start.php' method='post'>
			<?php MakeFormInput($toDeliver); ?>
		</FORM>
		<SCRIPT LANGUAGE="JavaScript">
			document.ReadyForm.submit();
		</SCRIPT>
	</body>
<?php

	}
	else
	{
		$ErrorCode = $Res["ReasonCode"];
		$ErrorMessage = $Res["ReasonText"];
		include( __DIR__."/Error.php" );
	}
?>
</html>
