<?php 
if( !defined( 'IN_INU' ) )
{
	return;
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=big5">
		<title>Result</title>
		<link href="./telepay/images/Error.css" rel="stylesheet" type="text/css">
		<script language="JavaScript" src="./telepay/js/Common.js"></script>
	</head>
	<body>
		<table width="100%" height="100%" cellpadding=0 cellspacing=0>
		<tr><td align="center">

		<table cellpadding=0 cellspacing=0>
		<tr><td align="left" background="./telepay/images/Bg.gif" style="background-repeat: no-repeat;" height=600>

			<div id="Wrap">
				<!-- Guide & FAQ Button -->
				<div id="TopBtn">
					<a href="JavaScript:CallUse();"><img src="./telepay/images/Btn_top01.gif" border="0"></a>
					<a href="JavaScript:CallFAQ();"><img src="./telepay/images/Btn_top02.gif" border="0"></a>
				</div>

				<!-- Gray-colored Layer -->
				<div id="InfoTable">
					<!-- Succeed or Failed -->
					<div id="Success">
						<font color=#FF0000 size="3">
						&nbsp;&nbsp;<b>��&nbsp;��&nbsp;��&nbsp;��</b>&nbsp;&nbsp;
						</font>
					</div>
					<div id="ResultMessage" align=right>
						<table border = 0 width=100% height=100%>
						<tr><td>
						
						<table border = 0 width="300">
						<tr> <td align="left" colspan=2><b>Result: </td> </tr>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td align="left"><?=$ErrorCode?></td>
							</tr>
							<tr> <td colspan=2>&nbsp;</td> </tr>
							<tr> <td align="left" colspan=2><b>Message: </td> </tr>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;</td>
								<td align="left"><?=$ErrorMessage?></td>
							</tr>
						</table>
						</td></tr>
						</table>
					</div><!-- #End of ResultMessage -->
				</div><!-- #End of InfoTable -->

				<!-- Close Button -->
				<div id="CloseBtn"><a href="javascript:self.close();">
					<img src="./telepay/images/Btn_Close.gif" border="0"></a>
				</div>
			</div><!-- #End of Wrap -->
		</td></tr>
		</table>

		</td></tr>
		</table>
	</body>
</html>
