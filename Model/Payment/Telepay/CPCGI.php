<?php
if( !defined( 'IN_INU' ) )
{
	return;
}
	
	header("Pragma: No-Cache");

	include( __DIR__."/inc/telepaylib.php" );

	$TransU = array();

	$Addtional = array("TID","SessionID");
	$TransU = MakeAddtionalInput($TransU,$_POST,$Addtional);

	// Billing step
	$TransU["TxType"]       = "BC";
	
	$Res = CallYeZin($TransU,false);

	$payLog = new ErrorLog( "telepayLog" );
	$payLog->addLog( "ResultSet:".$_SERVER['REQUEST_URI'].json_encode( $Res ) );
	
	$success = 0;
	if($Res["Result"] == "0"){
		//交易成功
		$success = self::createOrder( array(
			"orderId" => $Res['B_OrderID'],
			"thirdId" => $Res['TID'],
		) );
		if( $success !== 1 )
		{
			$Res = array( 'Result' => 1 , "ReasonText" => iconv("UTF-8","big5","交易失敗") );
		}
	}

	if( $Res["Result"] == "0" )
	{
		echo("
			<form name = f1 action = '".Common::getServerAddr()."Web/Api/Pay/TelepaySuccess.php' method = post>
			</form>
			<script language='javascript'>
			document.f1.submit();
			</script>
		");
	}
	else 
	{
		$ErrorCode = $Res["ReasonCode"];
		$ErrorMessage = $Res["ReasonText"];
		
		include( __DIR__."/Error.php" );
	}
?>
