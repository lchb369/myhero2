<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

//---------------------------------------------------------
//MyCard 智冠
//---------------------------------------------------------
class Payment_MyCard_Model
{
	
	//private static $key = "mygame2011";
	private static $key = "Taiwan1@3$";
	
	/**
	 * 智冠回调
	 */
	public static function isEligible()
	{
		if( empty( $_POST['DATA'] ) ) return false;
		$data = json_decode( stripslashes( $_POST['DATA'] ) , true );
		if( empty( $data )) return false;
		
		$CP_TxID = $data['CP_TxID'];
		$SecurityKey = $data['SecurityKey'];
		$Account = $data['Account'];
		$Amount = $data['Amount'];
		$Realm_ID = isset( $data['Realm_ID'] ) ? $data['Realm_ID'] : "";
		$Character_ID = isset( $data['Character_ID'] ) ? $data['Character_ID'] : "";
		
		if( sha1( self::$key . $CP_TxID . $Account . $Amount . $Realm_ID . $Character_ID ) != $SecurityKey ) return false;
		
		$payLog = new ErrorLog( "MyCardOrder" );
		$payLog->addLog( "isEligible-VerifySuccess:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
		
		return true;
	}
	
	public static function bridge()
	{
		$retData = array(
			"ResultCode" => 0, //0:儲值失敗, 1:儲值成功, 2:重複儲值
			"Description" => "",
		);
		
		$verify = 0;
		$data = array();
		if( !empty( $_POST['DATA'] ) )
		{
			$data = json_decode( stripslashes( $_POST['DATA'] ) , true );
			if( !empty( $data ))
			{
				$verify = self::createOrder( $data );
			}
		}
		
		$retData['ResultCode'] = $verify;
		$retData['CP_TxID'] = $data['CP_TxID'];
		$retData['AUTH_CODE'] = $data['AUTH_CODE'];
		$retData['MG_TxID'] = $data['MG_TxID'];
		
		return $retData; 
	}

	/**
	 * 向第三方验证订单
	 * @param unknown $orderId
	 * @return 0 失败 ， 1成功 ， 2订单不存在
	 */
	public static function checkOrder( $orderId )
	{
		$cache = Common::getCache();
		if( !!$thirdOrder = $cache->get( "order_MyCard_".$orderId ) )
		{
			$SecurityKey = sha1( self::$key . $thirdOrder['AUTH_CODE'] . $thirdOrder['MG_TxID'] . $thirdOrder['Account']);
			$postData = array(
				"DATA" => json_encode( array(
					"SecurityKey" => $SecurityKey,
					"AUTH_CODE" => $thirdOrder['AUTH_CODE'],
					"MG_TxID" => $thirdOrder['MG_TxID'],
					"Account" => $thirdOrder['Account'],
				))
			);
			if( isset( $_REQUEST['sandbox'] ) )
			{
				//測試環境:
				$MyCardMobileDomain = "https://devmobile.mygame.com.tw"; 
			}
			else
			{ 
				//正式環境:
				$MyCardMobileDomain = "https://mobile.mygame.com.tw";
			}
			$check = Helper_WWW::post( $MyCardMobileDomain."/billing/RECEIVE.aspx" , http_build_query( $postData ) );
			
			$payLog = new ErrorLog( "MyCardOrder" );
			$payLog->addLog( "checkOrderBack:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST )."==".$check );
			
			if( empty( $check ) ) return 0;
			$check = json_decode( $check );
			if( empty( $check ) || $check['ResultCode'] == 0 )
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
				
		return 2;
	}
	
	private static function createOrder( $data )
	{
		$payLog = new ErrorLog( "MyCardOrder" );
		
		$CP_TxID = $data['CP_TxID'];
		$SecurityKey = $data['SecurityKey'];
		$MG_TxID = $data['MG_TxID'];
		$Account = $data['Account'];
		$Amount = $data['Amount'];
		$Realm_ID = isset( $data['Realm_ID'] ) ? $data['Realm_ID'] : "";
		$Character_ID = isset( $data['Character_ID'] ) ? $data['Character_ID'] : "";
		$Tx_Time = $data['Tx_Time'];
		$AUTH_CODE = $data['AUTH_CODE'];
		$MyCardProjectNo = $data['MyCardProjectNo'];
		$MyCardType = $data['MyCardType'];
		
		if( sha1( self::$key .$CP_TxID . $MG_TxID . $Account . $Amount . $Realm_ID . $Character_ID 
				. $Tx_Time . $AUTH_CODE . $MyCardProjectNo . $MyCardType ) != $SecurityKey )
		{
			$payLog->addLog( "createOrderCheck:"."sha1 auth fail" );
			return 0;
		}
		
		//充值
		$orderId = $CP_TxID;
		
		$thirdOrderId = $MG_TxID;

		//订单是否存在
		$cache = Common::getCache();
		$orderCache = $cache->get( $orderId );
		if( empty( $orderCache ) )
		{
			$payLog->addLog( "createOrderCheck:"."orderCache not exist" );
			return 0;
		}
		$orderCache = json_decode( $orderCache , true );
		if( empty( $orderCache ) )
		{
			$payLog->addLog( "createOrderCheck:"."orderCache not exist" );
			return 0;
		}

		//用户是否存在
		//$userId = intval( $Character_ID );
		$userId = intval( $orderCache['uid'] );
		if( User_Model::exist( $userId ) == false )
		{
			$payLog->addLog( "createOrderCheck:"."user not exist" );
			return 0;
		}
		
		//不能通过gid判断，要通过面额来判断
		
		//是否存在商品
		$goodsConfig = Common::getConfig( "goods" );
		
		do{
			//是否为限购礼包活动
			$packId = Order_Model::PROMOTION_PACK_1;
			if( $orderCache['goodsName'] == $packId && $Amount == $goodsConfig[$packId]['MyCard'] )
			{
				$gid = $packId;
				break;
			}
			
			foreach( $goodsConfig as $conf )
			{
				if( !empty( $conf['MyCard'] ) && $conf['MyCard'] == $Amount )
				{
					$gid = $conf['goodsId'];
					break;
				}
			}
		}while(0);
		
		if( empty( $gid ) )
		{
			$payLog->addLog( "createOrderCheck:"."empty gid" );
			return 0;
		}
			
		//是否已经发送过
		$status = Data_Order_Model::getInstance(  $userId )->checkOrderStatus( $orderId );
		if( $status == 1 )
		{
			return 2;
		}
			
		$price = $goodsConfig[$gid]['price'];
			
		$order = array(
				'id' => $orderId,
				'goodsName' => $gid,
				'goodsNum' => 1,
				'price' => $price,
				'discount' => 0,
				'totalPrice' => $price,
				'buyerId' => $userId,
				'tradeTime' => $_SERVER['REQUEST_TIME'],
				'addTime' => $_SERVER['REQUEST_TIME'],
				'email' => '',
				'platform' => "MyCard",
				'status' => 1,
				'thirdId' => $thirdOrderId,
		);
		$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		
		$cache = Common::getCache();
		$cache->set( "order_MyCard_".$orderId , $data , 86400 * 5 );
		
		if( $rs == true )
		{
			$payLog->addLog( "createOrder:".json_encode( $order ) );
			Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , "MyCard" , $order['price'] , $order['id'] );
			return 1;
		}

		$payLog->addLog( "createOrderCheck:"."addOrderFail" );
		return 0;
		
	}
	
}

?>