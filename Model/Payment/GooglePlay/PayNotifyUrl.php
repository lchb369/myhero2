<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

//---------------------------------------------------------
//googleplay
//---------------------------------------------------------
class Payment_GooglePlay_PayNotifyUrl
{
	
	private static $pub_keys = array(
		"wk" => 		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqD/y8KD4LVkSWTl5H5mLSeVdNMePRgr8VXCunu3XYsRhN29Fy95nJLEOW+gw0vaFL0hwO3vB53nQZ4UbizfnE3AWdWR45nWoUucdzn3H+E0CNcJ940jkmZfhOZ3A4MNFVGh3v2ySg1JPoXUdQC+Cf9r09nsNhDQvOv99ws1yFhEGk3sYU16MyQh3DQDMmVFoJ/kR91Nkr8g2HB0ORICL2yN8yJqpVzoG/1hoAbaXDxhHhc1w9xBh8VNxIWlljjjER+fcjK2IOcWp6bvR3Kh4jC2XhU2OOKUkS9PdcqeKNzJbQDfERYSsGNpbvc7zzraP64otTAgso6E0edO/M2q5lQIDAQAB",
		"kr" => 		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtRs4vRlCSyAgPJsc7jemVWEZB95BD0IgVPgdXEAxRljYFtQZK+3LuAKFm0ZeX3LBdKoPMx/h9CAdOqLQrGdWM6cAuatHROKrVxoqmHi4zvQsYq+Bc7VTe5sZOFPTw52V7wDOXILcUNI2uFywnovPq6rkEBVKHFDXH2ncSCU9ffAXUp0ryFzgcMnoqqHqwGcr0t3NRMtEGBKV9BRuIl9r77s2kfEKv7Vx4o30EDYKJocui9tfqejLPdRGmdgSehjiZB3AJeo53TyHTZYwzPKBWAXpev3nfk/pB0734i3erwZC6lpl97OtJbEZeH6zKJRFZPXWL9ZUA2XWap536lykRwIDAQAB",
		"krkk" => 		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtzlkjQxw1Gw0b0v6LJ6X0tzLUHfqeqjR8lEvCl2E5Ckwj33RZ52W+XV3Evp3OS3L8EmOWgNL5qacNoPPfSe5qwWVxDa2J1T8jJAutvNPcQ887nF+K8TP1zj3awyvfVJHqNRu0hDTPqS1kum1Ln5/4Pf66UGtU5c/y+y/1zsGIanuq70uIR17Dc63TnUW7xhsQe/l0Yw86xua0IUyJ/d3rI6jQNuHTcnAUlu2HHf/8KMjT8BpW5yXgQGByk2niHrOJXasSqZKmR7B1JWzxFfxJXhCpLTLYvsBNwNtUq58LJdYvB2AEVfUVul7e1e1ChJAKAt1nhh7Ls2IUAQjNn5qvwIDAQAB",
	); 
	
	private static $payType = "GooglePlay";
	
	/**
	 * 新版本
	 * @param unknown $userId
	 * @param unknown $signdata
	 * @param unknown $signature
	 */
	public static function verifyAndCreateOrder( $userId , $orderId , $signdata , $signature )
	{
		$payLog = new ErrorLog( "GooglePlayOrder" );
		
		$cache = Common::getCache();
		$orderId = strval( $orderId );
		
		$orderInfo = $cache->get( $orderId );
		if( empty( $orderInfo ) )
		{
			$payLog->addLog( "verifyError: emptyOrderInfo, orderId ".$orderId);
			return false;
		}
		$orderInfo = json_decode( $orderInfo , true );
		if( empty( $orderInfo ) )
		{
			$payLog->addLog( "verifyError: decode orderInfo fail, orderInfo ".$orderInfo);
			return false;
		}		
		
		$signdata = urldecode( $signdata );
		$signature = urldecode( $signature );
		$payType = !empty($_REQUEST['payType']) ? $_REQUEST['payType'] : Common::getConfig('platform');
		$pub_key_id=self::$pub_keys[$payType];
		if(empty($pub_key_id))
		{
			$payLog->addLog( "verifyError: pub_key_id is empty, payType: ".$payType);
			return false;
		}
		//$pub_key_id=base64_decode($pub_key_id);
		
		//$data1='{"orderId":"12999763169054705758.1312673213470265","packageName":"com.happiplay.nine","productId":"com.happiplay.nine.testm601","purchaseTime":1357554078000,"purchaseState":0,"purchaseToken":"ialkwupdnzgvslhrmrbsomxc.AO-J1OwZ5Q6dcnsNkfkg6FBws8v5L4gv7EZGCm0OH--chtj9rrEC6aHB6e0Ax9QPH9mbceYQuTusRJLkXqjRCOgHUnhDOQzirznCx1CiVv-cojXlINEp41ItPKiTCNMulKlNPi9Y1uxl"}';
		$signdata = str_replace('\"' ,'"', $signdata);
		
		//$signature1='G5dshmdvmGkKJDo+5jgRhy5Exhf/QNr5lhcEXJYZFk9zAF8JHh9WWCB8acJcdQ6evPIq2FRtGvDpC0zhGy9fCxupjdh2XrOLvQjbq0Fo1qFUNjp833/FQg1IXe16phh2EimKBnDV61WHN304Gmns82lKxDRcDR4XdnsILoFEFu3zEMN7sI3/ESgtonZ+VrtpTFU6qe7Bfl4HOOeFMlnXOmwBHcUyqUW+75+s8qiouC+eQoC3B3gxFcLHmkv5y4vuekyYh22RwZmMLIpYk7RX8gb9cOSicVBvylZ6zBLGv0la050Ll7dQkE0larx4pl0UKV/xqUil0yUBiQjknGmgVQ==';
		$signature=str_replace(' ', '+', $signature);
		
		$pub_key_id = self::der2pem($pub_key_id);
		try{
			$success = openssl_verify( $signdata , base64_decode( $signature ), openssl_get_publickey( $pub_key_id ) );
			openssl_free_key($pub_key_id);
		}catch (Exception $e){
			return false;
		}
		
		if($success==1)
		{
			$payLog->addLog( "verifySuccess:".$_SERVER['REQUEST_URI'].json_encode( $orderInfo ) );
			//{"orderId":"12999763169054705758.1354762149247128",
			//"packageName":"com.happiplay.nine",
			//"productId":"com.happiplay.nine.m601",
			//"purchaseTime":1357270267000,
			//"purchaseState":0,
			//"purchaseToken":"xcaiacjhaogkjhyntahefnam.AO-J1OyqzmP_ncR8JK1S-ofOwGptybpEkiQkBK-3rxgbKMXs-VVZCbHZiWzrA08Ye-SrDe4o8Enlss7rj93VQSa-iu_SijPqfZm9UiIKvn-qLm2qyIEus2y8fXyW_BCWO5Gwn0SEmocZ"}
			$info=json_decode( $signdata , true );
			if( empty( $info ) )
			{
				$payLog->addLog( "verifyError: decode signdata fail");
				return false;
			}
			
			$goodsConfig = Common::getConfig( "goods" );
			$gid = 0;
			foreach ( $goodsConfig as $goodsInfo ){
				if( $goodsInfo['GooglePlayId'] == $info['productId'] )
				{
					$gid = $goodsInfo['goodsId'];
					break;
				}
			
			}
			return self::_createOrder( $userId , $gid , $orderInfo , $info['orderId'] );
		}
		else
		{
			$payLog->addLog( "verifyError: openssl_verify fail");
			return false;
		}
		
	}
	
	private static function _createOrder( $userId , $gid , $orderInfo , $thirdId )
	{
		$payLog = new ErrorLog( "GooglePlayOrder" );
		//是否存在商品
		$goodsConfig = Common::getConfig( "goods" );
		if( !$goodsConfig[$gid] )
		{
			$payLog->addLog( "createOrderError: goodsConfig not exist ".$gid);
			return false;
		}
		
		//是否已经发送过
		//$status = Order_Model::getInstance(  $userId )->checkOrderStatus( $orderInfo['id'] );
		$status = Data_Order_Model::getInstance($userId)->checkOrderStatusForThird($thirdId, self::$payType);
		if( $status == 1 )
		{
			$payLog->addLog( "createOrderError: already send");
			return false;
		}
		
		$price = $goodsConfig[$gid]['price'];
		
		$order = array(
				'id' => $orderInfo['id'],
				'goodsName' => $gid,
				'goodsNum' => 1,
				'price' => $price,
				'discount' => 0,
				'totalPrice' => $price,
				'buyerId' => $userId,
				'tradeTime' => $_SERVER['REQUEST_TIME'],
				'addTime' => $orderInfo['addTime'],
				'email' => '',
				'platform' => self::$payType,
				'status' => 1,
				'thirdId' => $thirdId,
		);
		$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		if( $rs == true )
		{
			$payLog->addLog( "createOrder:".json_encode( $order ) );
			Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , self::$payType , $price , $order['id'] );
			return true;
		}
		else
		{
			$payLog->addLog( "createOrderError: add order fail");
			return false;
		}
	}
	
	private static function der2pem($der_data) {
		$pem = chunk_split($der_data, 64, "\n");
		$pem = "-----BEGIN PUBLIC KEY-----\n".$pem."-----END PUBLIC KEY-----\n";
		return $pem;
	}
}

?>