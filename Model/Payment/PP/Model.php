<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/*
	pp充值平台：http://pay.25pp.com/
	以下为示例代码
*/
include('Rsa.php');
include('MyRsa.php');

class Payment_PP_Model
{
	public static function Appreturn()
	{
		$ret = "fail";
		
		$notify_data = $_POST;
		$chkres = self::chk($notify_data);
		//error_log(date("Y-m-d h:i:s")."result ".serialize($chkres)."\r\n",3,'rsa.log');
		if($chkres) {
			//验证通过
			//--------业务处理----------
			$payLog = new ErrorLog( "ppPaymentLog" );
       		$payLog->addLog( "verifyPass:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
       		
			$orderId = strval( $notify_data['billno'] );
			$amount = intval( $notify_data['amount'] );
			$zone = intval( $notify_data['zone'] );
			$userId = intval( $notify_data['roleid'] );
			
			$cache = Common::getCache();
			//订单不存在
			if( !$orderCache = $cache->get( $orderId ) ) return $ret;
			$orderCache = json_decode( $orderCache , true);
			if( empty( $orderCache['goodsName'] ) ) return $ret;
			
			//用户不存在
			if( !User_Model::exist( $userId ) ) return $ret;
			
			//商品不存在
			$gid = $orderCache['goodsName'];
			$goodsConfig = Common::getConfig( "goods" );
			if( !$goodsConf = $goodsConfig[$gid] ) return $ret;
			
			//订单已完成
			if( Order_Model::getInstance( $userId )->checkOrderStatus( $orderId ) == 1 ) return $ret;
			
			$order = array(
					'id' => $orderId,
					'goodsNum' => 1,
					'email' => "",
					'price' => $goodsConf['price'],
					'discount' => 0.00,
					'totalPrice' => $goodsConf['price'],
					'buyerId' => strval( $notify_data['account'] ),
					'tradeTime' => $_SERVER['REQUEST_TIME'],
					'addTime' => $_SERVER['REQUEST_TIME'],
					'status' => 1,
					'platform' => 'pp',
					'thirdId' => strval( $notify_data['order_id'] ),
			);
			
			$status = Order_Model::getInstance( $userId )->addOrder( $order );		
			if( $status == 1 )
			{
				$status = Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , "pp"  , $order['price'] , $order['id'] );
				if( !$status )
				{
					//发货失败
					return $ret;
				}
			}
			else 
			{
				//订单更新失败
				return $ret;
			}
			
			//
			$ret = "success";
		}
		return $ret;
	}
	
	public static function chk($notify_data)
	{
		$privatedata = $notify_data['sign'];
		//error_log(date("Y-m-d h:i:s")." ".serialize($privatedata)."\r\n",3,'rsa.log');
		
		$privatebackdata = base64_decode($privatedata);
		//error_log(date("Y-m-d h:i:s")."base64_decode ".serialize($privatebackdata)."\r\n",3,'rsa.log');
		$MyRsa = new MyRsa;
		$data = $MyRsa->publickey_decodeing($privatebackdata, MyRsa::public_key);
		//error_log(date("Y-m-d h:i:s")."publickey_decodeing ".$data."\r\n",3,'rsa.log');
		/*
		$ex_data = explode("&",$data);
		$rs = array();
		foreach($ex_data as $v){
			$k_v = explode("=",$v);
			$rs[$k_v[0]] = $k_v[1];
		}
		*/
		$rs = json_decode($data,true);
		//error_log(date("Y-m-d h:i:s")."rs ".serialize($rs)."\r\n",3,'rsa.log');
		if(empty($rs)||empty($notify_data))return false;
		if($rs["billno"] == $notify_data['billno']&&$rs["amount"] == $notify_data['amount']&&$rs["status"] == $notify_data['status']) {
			//0正常状态 ; 1已兑换过并成功返回
			if( $rs["status"] != 0 ) return false;
			return true; 
		}else{
			return false;
		}
	}

}
?>