<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 支付
 */
class Payment_Model extends Logical_Abstract
{
	
	/**
	 * 单例对象
	 * @var	User_Model[]
	 */
	protected static $singletonObjects;
	
	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Payment_Model
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	public function getOrderId($goodsId)
	{
		$payLog = new ErrorLog( "getOrderId" );
		
		$userProfile = User_Profile::getInstance( $this->userId )->getData();
		
		//是否超过购买限制
		if( Data_Order_Model::getInstance( $this->userId )->itemHasExceedLimit( $goodsId ) )
		{
			$payLog->addLog( "itemHasExceedLimit:".$_SERVER['REQUEST_URI'].'['.$goodsId.']' );
			return array( 'order' => '' );
		}
		
		$str = "order_".$userProfile['platform']."_".$this->userId."_".microtime()."_".$goodsId;
		$orderId = md5( $str );
		
		$order = array(
				'id' => $orderId,
				'goodsName' => $goodsId,
				'addTime' => $_SERVER['REQUEST_TIME'],
				'platform' => $userProfile['platform'],
				'uid' => $this->userId,
		);
		
		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $order ) );
		
		$cache = Common::getCache();
		//保存五天
		$cache->set( $orderId , json_encode( $order ) , 24*3600*5 );
		
		return $orderId;
	}
	
}