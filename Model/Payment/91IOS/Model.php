<?php
/**
 * PHP SDK for  OpenAPI
 *
 * @version 1.0
 * @author dev.91.com
 */

header("Content-type: text/html; charset=utf-8");
/**
 * 应用服务器接收91服务器端发过来支付购买结果通知的接口DEMO
 * 当然这个DEMO只是个参考，具体的操作和业务逻辑处理开发者可以自由操作
 */
/*
 * 这里的MyAppId和MyAppKey是我们自己做测试的
 * 开发者可以自己根据自己在dev.91.com平台上创建的具体应用信息进行修改
 */

//$Res = pay_result_notify_process($MyAppId,$MyAppKey);


class Payment_91IOS_Model
{
	
	/**
	 * 此函数就是接收91服务器那边传过来传后进行各种验证操作处理代码
	 * @param int $MyAppId 应用Id
	 * @param string $MyAppKey 应用Key
	 * @return json 结果信息
	 */
	public static function payResultNotifyProcess()
	{
		$MyAppId = 108352; 
		$MyAppKey = '55e556e7b8c9e0b3d39b8fca49f7adead7163af215b5019b';
	
		$Result = array();//存放结果数组
		
		if(empty($_GET)||!isset($_GET['AppId'])||!isset($_GET['Act'])||!isset($_GET['ProductName'])||!isset($_GET['ConsumeStreamId'])
			||!isset($_GET['CooOrderSerial'])||!isset($_GET['Uin'])||!isset($_GET['GoodsId'])||!isset($_GET['GoodsInfo'])||!isset($_GET['GoodsCount'])||!isset($_GET['OriginalMoney'])
			||!isset($_GET['OrderMoney'])||!isset($_GET['Note'])||!isset($_GET['PayStatus'])||!isset($_GET['CreateTime'])||!isset($_GET['Sign']))
		{
			
			$Result['ErrorCode'] =  0;
			$Result['ErrorDesc'] =  urlencode('接收失败');
			$Res = json_encode($Result);
			return urldecode($Res);
		}
		
		$AppId 				= $_GET['AppId'];//应用ID
		$Act	 			= $_GET['Act'];//操作
		$ProductName		= $_GET['ProductName'];//应用名称
		$ConsumeStreamId	= $_GET['ConsumeStreamId'];//消费流水号
		$CooOrderSerial	 	= $_GET['CooOrderSerial'];//商户订单号
		$Uin			 	= $_GET['Uin'];//91帐号ID
		$GoodsId		 	= $_GET['GoodsId'];//商品ID
		$GoodsInfo		 	= $_GET['GoodsInfo'];//商品名称
		$GoodsCount		 	= $_GET['GoodsCount'];//商品数量
		$OriginalMoney	 	= $_GET['OriginalMoney'];//原始总价（格式：0.00）
		$OrderMoney		 	= $_GET['OrderMoney'];//实际总价（格式：0.00）
		$Note			 	= $_GET['Note'];//支付描述
		$PayStatus		 	= $_GET['PayStatus'];//支付状态：0=失败，1=成功
		$CreateTime		 	= $_GET['CreateTime'];//创建时间
		$Sign		 		= $_GET['Sign'];//91服务器直接传过来的sign
		
		
		//因为这个DEMO是接收验证支付购买结果的操作，所以如果此值不为1时就是无效操作
		if($Act != 1){
			$Result['ErrorCode'] =  3;
			$Result['ErrorDesc'] =  urlencode('Act无效');
			$Res = json_encode($Result);
			return urldecode($Res);
		}
		
		//如果传过来的应用ID开发者自己的应用ID不同，那说明这个应用ID无效
		if($MyAppId != $AppId){
			$Result['ErrorCode'] =  2;
			$Result['ErrorDesc'] =  urlencode('AppId无效');
			$Res = json_encode($Result);
			return urldecode($Res);
		}
		
		//按照API规范里的说明，把相应的数据进行拼接加密处理
		$sign_check = md5($MyAppId.$Act.$ProductName.$ConsumeStreamId.$CooOrderSerial.$Uin.$GoodsId.$GoodsInfo.$GoodsCount.$OriginalMoney.$OrderMoney.$Note.$PayStatus.$CreateTime.$MyAppKey);
		
		//当本地生成的加密sign跟传过来的sign一样时说明数据没问题
		if($sign_check == $Sign )
		{ 
			$order = array(
					'id' => $CooOrderSerial,
					'goodsNum' => $GoodsCount,
					'email' => "",
					'price' => $OrderMoney,
					'discount' => 0.00,
					'totalPrice' => $OriginalMoney,
					'buyerId' => $Uin,
					'tradeTime' => $_SERVER['REQUEST_TIME'],
					'addTime' => $_SERVER['REQUEST_TIME'],
					'status' => 1,
					'platform' => '91_ios',
					'thirdId' => $ConsumeStreamId,
			);
		
			$cache = Common::getCache();
			//保存五天
			$cacheData = $cache->get( $CooOrderSerial  );
			$orderData = json_decode( $cacheData , true );
			
			
			//如果memache过期，那么订单的用户信息和商品信息，从Note里提取
			$NoteArr = explode( "&", $Note );
		
			$UidArr = explode( "=", $NoteArr[0] );
			$userId = intval( $UidArr[1]  );
			if( $userId > 0 )
			{
				$orderData['uid'] = $userId;
				
			}
			else 
			{
				$Result['ErrorCode'] =  2;
				$Result['ErrorDesc'] =  urlencode('用户ID不正确' );
				return json_encode( $Result);
			}
			
		
			$goodsArr = explode( "=", $NoteArr[1] );
			$order['goodsName'] = $orderData['goodsName'] = intval( $goodsArr[1]  );
			if( $orderData['goodsName'] <= 0  )
			{
				$Result['ErrorCode'] =  3;
				$Result['ErrorDesc'] =  urlencode('商品ID不正确' );
				return json_encode( $Result);
			}
			
			//以上得到正确uid,goodsid*///
			//如果订单已经完成，则不再处理
			$status = Order_Model::getInstance(  $orderData['uid'] )->checkOrderStatus( $order['id'] );
			if( $status == 1 )
			{
				$Result['ErrorCode'] =  3;
				$Result['ErrorDesc'] =  '此订单已经完成';
				return json_encode( $Result);
			}

			$goodsConfig = Common::getConfig( "goods" );
			$goodsConf = $goodsConfig[$orderData['goodsName']];
			$price = $goodsConf['price'];
// 			if( $price != $OriginalMoney )
// 			{
// 				$Result['ErrorCode'] =  3;
// 				$Result['ErrorDesc'] =  urlencode('支付金额不正确' );
// 				return json_encode( $Result);
					
// 			}
			
			$status = Order_Model::getInstance( $orderData['uid'] )->addOrder( $order );		
			if( $status == 1 )
			{
				$status = Order_Model::getInstance( $orderData['uid'] )->SendGoods( $orderData['goodsName'] , 1 , "91_ios" , $OrderMoney , $CooOrderSerial );
				if( !$status )
				{
					$Result['ErrorCode'] =  4;
					$Result['ErrorDesc'] =  '发货失败';
					return json_encode( $Result);
				}
			}
			else 
			{
				$Result['ErrorCode'] =  6;
				$Result['ErrorDesc'] =  '订单更新失败';
				return json_encode( $Result);
			}
			
			/*
			 * 
			 * 开发者可以在此处进行订单号是否合法、商品是否正确等一些别的订单信息的验证处理
			 * 相应的别的错误用不同的代码和相应说明信息，数字和信息开发者可以自定义（数字不能重复）
			 * 如果所有的信息验证都没问题就可以做出验证成功后的相应逻辑操作
			 * 不过最后一定要返回上面那样格式的json数据
			 * 
			 */
			
			
			$Result['ErrorCode'] =  1;
			$Result['ErrorDesc'] =  urlencode('接收成功');
			$Res = json_encode($Result);
			return urldecode($Res);
		}else{
			$Result['ErrorCode'] =  5;
			$Result['ErrorDesc'] =  urlencode('Sign无效');
			$Res = json_encode($Result);
			return urldecode($Res);
		}
		
	}

}








?>
