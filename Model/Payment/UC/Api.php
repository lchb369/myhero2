<?php
include "HttpClient.php";
include "Method/payCallback.php";
include "Method/sidInfo.php";
include "Method/ucidBindCreate.php";


class Payment_UC_Api
{	
	private $serverUrl = "";	
	private $cpId = 0;	
	private $gameId = 0;	
	private $channelId = "";	
	private $serverId = 0;	
	private $apiKey = "";
	private $config = array();
	/**
	 * 构造函数
	 */
	public function __construct($config){
	
		$test = new AppTest($config);
		
		$this->config = $config;
		if(is_array($this->config)&& $this->config==null){
				throw new exception('配置为空');
		}
	}

	public static  function sidInfo( $sid )
	{
		////////////////////////////////////////////
		try{
			//$sid = "56be11d2-dfdc-40ac-8bc1-cb88fd36a011133581";//request.Params["sid"];从游戏客户端的请求中获取的sid值
			$config = include "config.inc.php";
			$sidClass = new sidInfo( $config );
			$result = $sidClass->sidInfoMethod($sid);
			return $result;
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	public static  function payCallBack()
	{
	
		try{
			$config = include "config.inc.php";
			$payClass = new payCallBack( $config );
			$result = $payClass->payCallBackMethod();
			return $result;
		 }
		 catch(Exception $e)
		 {
				echo $e->getMessage();
		 }
			///////////////////////////////////////////
	}
	/**
	 * 主函数
	 */
	public function main(){
//		echo "[serverUrl]:".$this->serverUrl;
//		echo "[cpId]:".$this->cpId;
//		echo "[gameId]:".$this->gameId;
//		echo "[channelId]:".$this->channelId;
//		echo "[serverId]:".$this->serverId;
//		echo "[apiKey]:".$this->apiKey;

		////////////////////////////////////////////
		try{
//			$sid = "56be11d2-dfdc-40ac-8bc1-cb88fd36a011133581";//request.Params["sid"];从游戏客户端的请求中获取的sid值
//			$sidClass = new sidInfo($this->config);
//			$result = $sidClass->sidInfoMethod($sid);
//			
//			$gameUser = "dd";//request.Params["gameUser"];从游戏客户端的请求中获取的gameUser值
//			$ucidClass = new ucidBindCreate($this->config);//调用ucid和游戏官方帐号绑定接口
//			$result = $ucidClass->ucidBindCreateMethod($gameUser);
//			

			$payClass = new payCallBack($this->config);
			$result = $payClass->payCallBackMethod();
			return $result;
		}catch(Exception $e){
			echo $e->getMessage();
		}
		///////////////////////////////////////////
	}
	
}
