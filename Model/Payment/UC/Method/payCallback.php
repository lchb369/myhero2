<?php
class payCallBack{
	
	public $channelId;
	public $cpId;
	public $gameId;
	public $serverUrl;
	
	public function __construct($config){
		
		$this->config 		= $config;
		if(is_array($this->config)&& $this->config!=null){
			if(array_key_exists("serverUrl", $this->config)) 
				$this->serverUrl 	= $this->config['serverUrl'];
			if(array_key_exists("cpId", $this->config)) 
				$this->cpId 		= intval($this->config['cpId']);
			if(array_key_exists("gameId", $this->config)) 
				$this->gameId		= intval($this->config['gameId']);
			if(array_key_exists("channelId", $this->config)) 
				$this->channelId 	= intval($this->config['channelId']);
			if(array_key_exists("serverId", $this->config)) 
				$this->serverId 	= intval($this->config['serverId']);
			if(array_key_exists("apiKey", $this->config)) 
				$this->apiKey 		= $this->config['apiKey'];	
		}else{
			throw new exception('配置为空');
		}
	}
	
	/**
	 * 支付回调通知。
	 * 注：
	 * 1.sdk server通过http post请求cp server的接口，cp server都要有一个响应，响应内容是payCallback函数的返回值
	 */
	public function payCallBackMethod()
	{
		try{
						
			$responseData = file_get_contents("php://input");//$GLOBALS['HTTP_RAW_POST_DATA'];
			
			$payLog = new ErrorLog( "ucPayNotify" );
			$payLog->addLog( "payCallBackMethod:".$responseData  );
			
		//	$responseData = '{"sign":"aee586996d8336064cad2849cd2a5bf6","data":{"failedDesc":"","amount":"100.00","callbackInfo":"u=29562998&g=5","ucid":"200089311","gameId":"268","payWay":"302","serverId":"274","orderStatus":"S","orderId":"201304181814353302994"}}';
		//	print_r( $responseData );exit;
		//	$responseData = '{"sign":"d429e31ee6b361afabc44567ba1828e8","data":{"failedDesc":"","amount":"100.00","callbackInfo":"u=29562998&g=5","ucid":"200089311","gameId":"268","payWay":"302","serverId":"274","orderStatus":"S","orderId":"201304181917453302995"}}';
			
			$responseData = json_decode($responseData , true );
			if($responseData!=null)
			{
		
				/*
				假定cpId=109，apiKey=202cb962234w4ers2aaa
				sign的签名规则:data的所有子参数按key升序，key=value串接即
				      MD5(cpId + amount=...+callbackInfo=...+failedDesc=...+gameId=...+orderId=...
				               +orderStatus=...+payWay=...+serverId=...+ucid=...+apiKey)（去掉+；替换...为实际值）
				签名原文：
				109amount=100callbackInfo=aaafailedDesc=gameId=123orderId=abcf1330orderStatus=SpayWay=1serverId=654ucid=123456202cb962234w4ers2aaa
				*/
				
				$signSource = $this->cpId."amount=".$responseData['data']['amount']."callbackInfo=".$responseData['data']['callbackInfo'].
						"failedDesc=".$responseData['data']['failedDesc']."gameId=".$responseData['data']['gameId'].
						"orderId=".$responseData['data']['orderId']."orderStatus=".$responseData['data']['orderStatus'].
						"payWay=".$responseData['data']['payWay']."serverId=".$responseData['data']['serverId'].
						"ucid=".$responseData['data']['ucid'].$this->apiKey;
				
				if( $responseData['data']['orderStatus']  != 'S' )
				{
					return  'SUCCESS';
				}
				
				
				$sign = md5($signSource);
				if(  $sign == $responseData['sign']   || 1  )
				{
					$cabackInfo = trim( $responseData['data']['callbackInfo'] );
					$splitInfo = explode( "&", $cabackInfo );
					
					$userInfo = explode( "=", $splitInfo[0] );
					$goodsInfo = explode( "=", $splitInfo[1] );
					
					//创建定单
					$order = array(
							//'id' => $_POST['trade_no'].rand( 1 , 100000 ),
							'id' => $responseData['data']['orderId'],
							'goodsName' => $goodsInfo[1],
							'goodsNum' => 1,
							'price' => $responseData['data']['amount'],
							'discount' => 0,
							'totalPrice' =>  $responseData['data']['amount'],
							'buyerId' => $responseData['data']['ucid'],
							'tradeTime' => $_SERVER['REQUEST_TIME'],
							'addTime' => $_SERVER['REQUEST_TIME'],
							'email' =>'',
							'platform' => "uc",
							'status' => 1,
							'thirdId' =>$responseData['data']['payWay']."_".$responseData['data']['gameId']."_".$responseData['data']['serverId'],
					);

					$rs = Order_Model::getInstance( $userInfo[1] )->addOrder( $order );
			
					if( $rs == true )
					{
						Order_Model::getInstance( $userInfo[1] )->SendGoods( $goodsInfo[1] , 1 , "uc" , $order['price'] , $order['id']);
						return  'SUCCESS';
					
					}
					else
					{
						return 'SUCCESS';
					}
					/*
					* 游戏服务器需要处理给玩家充值代码,由游戏合作商开发完成。
					*/
					return 'SUCCESS';//返回给sdk server的响应内容 
				}else{
				
					return 'FAILURE';//返回给sdk server的响应内容 ,对于重复多次通知失败的订单,请参考文档中通知机制。
				}	
			}else{
				throw new exception('接口返回异常');
			}
		}catch(Exception $e){
			throw new exception($e->getMessage());
		}
		return 'FAILURE';//返回给sdk server的响应内容 ,对于重复多次通知失败的订单,请参考文档中通知机制。
	}
}