<?php
class ucidBindCreate{
	public $channelId;
	public $cpId;
	public $gameId;
	public $serverUrl;
	
	public function __construct($config){
		$this->config 		= $config;
		if(is_array($this->config)&& $this->config!=null){
			if(array_key_exists("serverUrl", $this->config)) 
				$this->serverUrl 	= $this->config['serverUrl'];
			if(array_key_exists("cpId", $this->config)) 
				$this->cpId 		= intval($this->config['cpId']);
			if(array_key_exists("gameId", $this->config)) 
				$this->gameId		= intval($this->config['gameId']);
			if(array_key_exists("channelId", $this->config)) 
				$this->channelId 	= intval($this->config['channelId']);
			if(array_key_exists("serverId", $this->config)) 
				$this->serverId 	= intval($this->config['serverId']);
			if(array_key_exists("apiKey", $this->config)) 
				$this->apiKey 		= $this->config['apiKey'];	
		}else{
			throw new exception('配置为空');
		}
	}
	
	/**
	 * ucid和游戏官方帐号绑定接口。
	 * @param gameUser 游戏官方帐号
	 */
	public function ucidBindCreateMethod($gameUser){
		echo "开始调用ucid和游戏官方账号绑定接口";
		///////////////////////////////////////////////////
		$gameParam = array();
		$gameParam['cpId']			= $this->cpId;//cpid是在游戏接入时由UC平台分配，同时分配相对应cpId的apiKey
		$gameParam['gameId']		= $this->gameId;//gameid是在游戏接入时由UC平台分配
		$gameParam['channelId']		= $this->channelId;//channelid是在游戏接入时由UC平台分配
		//serverid是在游戏接入时由UC平台分配，
		//若存在多个serverid情况，则根据用户选择进入的某一个游戏区而确定。
		//若在调用该接口时未能获取到具体哪一个serverid，则此时默认serverid=0
		$gameParam['serverId']		= $this->serverId;
		//////////////////////////////////////////////////
		$dataParam = array();
		$dataParam['gameUser']		= $gameUser;//游戏官方帐号
		//////////////////////////////////////////////////
		/*
		签名规则=cpId+签名内容+apiKey
		假定cpId=109,apiKey=202cb962234w4ers2aaa,sid=abcdefg123456
		那么签名原文109gameUser=abcd202cb962234w4ers2aaa
		签名结果6bf1e3aa96b6a7030228e0b55fce072e
		*/
		$signSource					= $this->cpId."gameUser=".$gameUser.$this->apiKey;//组装签名原文
		$sign						= md5($signSource);//MD5加密签名
		//////////////////////////////////////////////////		
		$requestParam = array();
		$requestParam["id"] 		= time();//当前系统时间
		$requestParam["service"] 	= "ucid.user.sidInfo";
		$requestParam['game']		= $gameParam;
		$requestParam['data']		= $dataParam;
		$requestParam['encrypt']	= "md5";
		$requestParam['sign']		= $sign;
		////////////////////////////////////////////////
		$requestString 				= json_encode($requestParam);//把参数序列化成一个json字符串
		echo "[请求参数]:".$requestString;
		echo "[签名原文]:".$signSource;
		echo "[签名结果]:".$sign;
		$result = HttpClient::quickPost($this->serverUrl,$requestString);//http post方式调用服务器接口,请求的body内容是参数json格式字符串
		echo "[响应结果]".$result;
		$responseData = json_decode($result);//结果也是一个json格式字符串
		$responseData = new ArrayObject($responseData);
		$state = new ArrayObject($responseData['state']);
		if(!empty($responseData['data'])){
			$data = new ArrayObject($responseData['data']);
			$responseData['data'] = $data;
		}
		$responseData['state'] = $state;
		if($responseData!=null){//反序列化成功，输出其对象内容
			echo "[id]:".$responseData['id'];
			echo "[code]:".$responseData['state']['code'];
			echo "[msg]:".$responseData['state']['msg'];
			echo "[ucid]:".$responseData['data']['ucid'];
			echo "[sid]:".$responseData['data']['sid'];
		}else{
			throw new Exception("[接口返回异常]");
		}
		echo "[调用ucid和游戏官方账号绑定接口结束]";
	}
}