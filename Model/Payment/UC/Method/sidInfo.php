<?php
class sidInfo{
	public $channelId;
	public $cpId;
	public $gameId;
	public $serverUrl;
	
	public function __construct($config){
		$this->config 		= $config;
		if(is_array($this->config)&& $this->config!=null){
			if(array_key_exists("serverUrl", $this->config)) 
				$this->serverUrl 	= $this->config['serverUrl'];
			if(array_key_exists("cpId", $this->config)) 
				$this->cpId 		= intval($this->config['cpId']);
			if(array_key_exists("gameId", $this->config)) 
				$this->gameId		= intval($this->config['gameId']);
			if(array_key_exists("channelId", $this->config)) 
				$this->channelId 	= intval($this->config['channelId']);
			if(array_key_exists("serverId", $this->config)) 
				$this->serverId 	= intval($this->config['serverId']);
			if(array_key_exists("apiKey", $this->config)) 
				$this->apiKey 		= $this->config['apiKey'];	
		}else{
			throw new exception('配置为空');
		}
	}
	/**
	 * sid用户会话验证。
	 * @param sid 从游戏客户端的请求中获取的sid值
	 */
	public function sidInfoMethod($sid){
		//echo "[开始调用sidInfo接口]";
		///////////////////////////////////////////////////
		$gameParam = array();
		$gameParam['cpId']			= $this->cpId;//cpid是在游戏接入时由UC平台分配，同时分配相对应cpId的apiKey
		$gameParam['gameId']		= $this->gameId;//gameid是在游戏接入时由UC平台分配
		$gameParam['channelId']		= $this->channelId;//channelid是在游戏接入时由UC平台分配
		//serverid是在游戏接入时由UC平台分配，
		//若存在多个serverid情况，则根据用户选择进入的某一个游戏区而确定。
		//若在调用该接口时未能获取到具体哪一个serverid，则此时默认serverid=0
		$gameParam['serverId']		= $this->serverId;
		//////////////////////////////////////////////////
		$dataParam = array();
		$dataParam['sid']			= $sid;//在uc sdk登录成功时，游戏客户端通过uc sdk的api获取到sid，再游戏客户端由传到游戏服务器
		//////////////////////////////////////////////////
		/*
		签名规则=cpId+签名内容+apiKey
		假定cpId=109,apiKey=202cb962234w4ers2aaa,sid=abcdefg123456
		那么签名原文109sid=abcdefg123456202cb962234w4ers2aaa
		签名结果6e9c3c1e7d99293dfc0c81442f9a9984
		*/
		$signSource					= $this->cpId."sid=".$sid.$this->apiKey;//组装签名原文
		$sign						= md5($signSource);//MD5加密签名
		//////////////////////////////////////////////////		
		$requestParam = array();
		$requestParam["id"] 		= time();//当前系统时间
		$requestParam["service"] 	= "ucid.user.sidInfo";
		$requestParam['game']		= $gameParam;
		$requestParam['data']		= $dataParam;
		$requestParam['encrypt']	= "md5";
		$requestParam['sign']		= $sign;
		/////////////////////////////////////////////////
		$requestString 				= json_encode($requestParam);//把参数序列化成一个json字符串
		/*
		echo "[请求参数]:".$requestString;
		echo "[签名原文]:".$signSource;
		echo "[签名结果]:".$sign;
		*/
		///////////////////////////////////////////////////
		$result = HttpClient::quickPost($this->serverUrl,$requestString);//http post方式调用服务器接口,请求的body内容是参数json格式字符串
        return $result;
		//echo "[响应结果]".$result;
		$responseData = json_decode($result , true );//结果也是一个json格式字符串
		$responseData = new ArrayObject($responseData);
		$state = new ArrayObject($responseData['state']);
		$responseData['state'] = $state;
		if(!empty($responseData['data'])){
			$data = new ArrayObject($responseData['data']);
			$responseData['data'] = $data;
		}
		if($responseData!=null){//反序列化成功，输出其对象内容
			return $responseData;
			/*
			echo "[id]:".$responseData['id'];
			echo "[code]:".$responseData['state']['code'];
			echo "[msg]:".$responseData['state']['msg'];
			echo "[ucid]:".$responseData['data']['ucid'];
			echo "[nickname]:".$responseData['data']['nickName'];
			*/
		}else{
			throw new Exception("[接口返回异常]");
		}
	
		//echo "[调用sidInfo接口结束]";
	}
}
