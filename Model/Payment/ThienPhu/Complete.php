<?php include( __DIR__.'/TplHeader.php'); ?>
<body>
<div class="gridContainer clearfix">
  <div id="div1" class="fluid">
    <div class="title01">    
                    <table width="100%" border="0">
                    <tr>
                    <td id="btn" width="10%"><!-- <a href="#"><img src="<?php echo self::$srcPath; ?>images/refresh.png"  alt=""/></a> --></td>
                    <td width="70%"></td>
                    <td id="btn" width="10%"><!-- <a href="#"><img src="<?php echo self::$srcPath; ?>images/home.png"  alt=""/></a> --></td>
                    </tr>
                    </table>
      </div>
		<!-- 消費資訊確認 -->
        <div class="title02">Thông tin giao dịch</div>
        <div>
          <p class="title03"><?php echo $tradeTitle; ?></p>
        </div>
            <div class="title03">
            <table width="100%" border="0" align="center" cellpadding="10" cellspacing="0" class="table01">
      <tr>
		<!-- 交易金额 -->
        <td class="word03" width="35%">Giá trị nạp：</td>
        <td class="word05" width="65%"><?php echo $tradePrice; ?></tr>
      <tr>
		<!-- 商品名称 -->
        <td class="word03" >KNB nhận được：</td>
        <td class="word05"><?php echo $tradeProductName; ?></td>
      </tr>
      <tr>
		<!-- 交易訊息 -->
        <td class="word03" >Tình trạng：</td>
        <td class="word05"><?php echo $tradeInfo; ?></td>
      </tr>
      <tr>
		<!-- 儲值時間 -->
        <td class="word03" >Thời gian：</td>
        <td class="word05"><?php echo $tradeTime ? date('Y-m-d H:i', $tradeTime) : ""; ?></td>
      </tr>
      <tr style="display: none;"><td>
     	 <?php include( __DIR__.'/TplUserReqData.php'); ?>
     	 <input type="text" id="method" name="method" value="Payment.Complete" >
      </td></tr>
      </table>
  </div>
  <div class="word04"><?php echo $tradeDesc; ?></div>
  <div class="space"></div>
  </div>
</div>
</body>
</html>
