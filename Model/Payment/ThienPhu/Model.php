<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

//---------------------------------------------------------
//H3G
//---------------------------------------------------------
class Payment_ThienPhu_Model extends Logical_Abstract
{
	
	private static $srcPath = 'Pay/ThienPhu/';
	private static $security = '9ebe933fb5445a2b4b4bd8376992705a';
	private static $partner = '102';
	private static $user = 'test1';
	private static $remoteIP = '210.245.89.51';
	
	private static $KEY_KCO = 'KCO';
	private static $KEY_MK = 'MK';
	
	private static $TYPE_PAY_CARD = 'ThienPhuCard';
	private static $TYPE_PAY_SMS = 'ThienPhuSms';
	
	private static $partnerId2keyword = array(
		'240' => 'KCO1',
		'241' => 'KCO2',
		'242' => 'KCO3',
		'246' => 'KCO4',
	);
	
	public $goodsInfo = array();
	public $userReqData = array();
	
	/**
	 * 单例对象
	 * @var	User_Model[]
	 */
	protected static $singletonObjects;
	
	/**
	 * 结构化对象
	 * @param	int $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId )
	{
		parent::__construct( $userId );
		
		$this->userReqData = array(
			'os' => strval($_REQUEST['os']),
			'refer' => strval($_REQUEST['refer']),
			'timestamp' => intval($_REQUEST['timestamp']),
			'arg' => strval($_REQUEST['arg']),
			'sig' => strval($_REQUEST['sig']),
			'sid' => intval($_REQUEST['sid']),
			'pf' => strval($_REQUEST['pf']),
			'method' => strval($_REQUEST['method']),
			'uid' => intval($_REQUEST['uid']),
			'session' => strval($_REQUEST['session']),
		);
		
		$this->verify();
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Payment_ThienPhu_Model
	 */
	public static function & getInstance( $userId )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId );
		}
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 储值卡支付输入页面
	 */
	public function card()
	{
		$userProfile = User_Profile::getInstance($this->userId)->getData();
		$tradeNick = $userProfile['nickName'];
		
		include( __DIR__.'/Card.php');
	}
	
	/**
	 * 储值卡支付提交
	 * @param unknown $serial
	 * @param unknown $code
	 * @param unknown $telco
	 */
	public function cardSubmit($serial, $code, $telco)
	{
		$tradeDesc = 'Nạp thất bại'; //储值失败
		$tradeTitle = 'Giao dịch hoàn thành'; //交易完成
		$tradeInfo = 'Thất bại'; //交易失败
		
		$userProfile = User_Profile::getInstance($this->userId)->getData();
		
		do
		{
			$payLog = new ErrorLog( "ThienPhuPayLog" );
			/**
			http://7x26.com:8081/charging/sms/card?
			serial={serial}
			&code={code}
			&telco={telco}
			&partner={partner}
			&security={security}
			 */
			$verifyUrl = "http://7x26.com:8081/charging/card?";
			$postData = array(
				'serial' => $serial,
				'code' => $code,
				'telco' => $telco,
				'partner' => $this->getPartnerId(),
				'security' => self::$security,
				'user' => $userProfile['thirdId'],
			);
			$payLog->addLog( "req: [cardSubmit]".json_encode($postData) );
			$verifyReceipt = Helper_WWW::get( $verifyUrl , $postData );
			$payLog->addLog( "ack: [cardSubmit]".$verifyReceipt );
			
			if( empty( $verifyReceipt) ) break;
			$verifyReceipt = json_decode( $verifyReceipt , true );
			//{"status":200,"message":"OK","amount":"amount"}	
			if( !is_array( $verifyReceipt ) || intval($verifyReceipt['status']) !== 200 ) break;
			
			$payLog->addLog("notifyPaySuccess");
			
			$gid = 0;
			$goodsConfig = Common::getConfig( "goods" );
			foreach($goodsConfig as $goodsId => $config)
			{
				if($config['TPCardId'] == trim($verifyReceipt['amount']))
				{
					$gid = $goodsId;
					break;
				}
			}
			if(empty($gid))
			{
				$payLog->addLog( "verify: [cardSubmit] no match amount" );
				break;
			}
				
			$this->userReqData['gid'] = $gid;
			$this->goodsInfo = $goodsConfig[$gid];;
			$this->goodsInfo['price'] = $this->goodsInfo['TPCard'];
			
			if(!$this->createOrder('', self::$TYPE_PAY_CARD)) break;
				
			$tradePrice = $this->goodsInfo['price'];
			$tradeProductName = $this->goodsInfo['goodsDesc'];
		
			$tradeNick = $userProfile['nickName'];
		
			//'您在串燒三國中暱稱為【'.$tradeNick.'】的帳號成功購買了【'.$tradeProductName.'】'
			$tradeDesc = 'Nhan vat 【'.$tradeNick.'】cua ban trong KCO nhan thanh cong 【'.$tradeProductName.'】';
			$tradeTitle = 'Giao dịch hoàn thành'; //交易完成
			$tradeInfo = 'Thành công'; //交易成功
				
		}while(0);
		
		$tradeTime = $_SERVER['REQUEST_TIME'];
		
		include( __DIR__.'/Complete.php');
	}
	
	public function ThienPhuSmsInit($gid)
	{
		$ret = array('shortcode' => '', 'message' => '');
		
		$goodsConfig = Common::getConfig( "goods" );
		$goodsInfo = $goodsConfig[$gid];
		if(empty($goodsInfo['TPSms']))
		{
			throw new GameException(GameException::PARAM_ERROR);
		}

		$this->userReqData['gid'] = $gid;
		$this->goodsInfo = $goodsInfo;
		
		$ret['shortcode'] = $this->goodsInfo['TPSmsId'];
		$ret['message'] = $this->getSmsMsgPre().' '.$this->userId.' '.$GLOBALS['appSid'];
		
		return $ret;
	}
	
	/**
	 * 短信支付回调
	 * @param unknown $security
	 * @param unknown $smsid
	 * @param unknown $keyword
	 * @param unknown $shortcode
	 * @param unknown $customer
	 * @param unknown $message
	 * @return multitype:number string multitype:
	 */
	public static function notify($security, $smsid, $keyword, $shortcode, $customer, $message)
	{
		//smsid=212&keyword=Kco&shortcode=7026&customer=0969986888&message=Kco+34796441
		//{"status":200,"message":"OK"}
		$ret = array('status' => 1, 'message' => 'param error');
		
		do
		{
			$payLog = new ErrorLog( "ThienPhuPayLog" );

// 			if($security != self::$security)
// 			{
// 				$payLog->addLog( "verify: [notify] security error" );
// 				break;
// 			}
			
			$remoteIP = Helper_IP::getCurrentIP();
			$payLog->addLog( "verify: [notify] remoteIP ".$remoteIP );
			if($remoteIP != self::$remoteIP)
			{
				$errorMsg = 'verify: [notify] remoteIP error';
				$payLog->addLog($errorMsg);
				$ret['message'] = $errorMsg;
				break;
			}
			
			$payLog->addLog("notifySuccess:".$keyword);
			$keyword = strtoupper($keyword);
			//支付
			if(preg_match("/^".self::$KEY_KCO."/", $keyword))
			{
				//keyword userId
				$msgArr = explode(" ", $message);
				$userId = $msgArr[1];
				if(empty($userId) || !User_Model::exist($userId))
				{
					$errorMsg = 'verify: [notify] user not exist:'.$userId;
					$payLog->addLog($errorMsg);
					$errorMsg = 'tai khoan '.$userId.' khong ton tai de nghi kiem tra lai tai khoan';
					$ret['status'] = 200;
					$ret['message'] = $errorMsg;
					break;
				}
				$gid = 0;
				if($shortcode == "7026") $gid = 7;
				$goodsConfig = Common::getConfig( "goods" );
				foreach($goodsConfig as $goodsId => $config)
				{
					if($config['TPSmsId'] == $shortcode)
					{
						$gid = $goodsId;
						break;
					}
				}
				if(empty($gid))
				{
					$errorMsg = 'verify: [notify] no match shortcode';
					$payLog->addLog($errorMsg);
					$ret['message'] = $errorMsg;
					break;
				}
					
				$model = Payment_ThienPhu_Model::getInstance($userId);
					
				$model->userReqData['gid'] = $gid;
				$model->goodsInfo = $goodsConfig[$gid];;
				$model->goodsInfo['price'] = $model->goodsInfo['TPSms'];
					
				if(!$model->createOrder(self::$TYPE_PAY_SMS.'_'.$smsid, self::$TYPE_PAY_SMS))
				{
					$errorMsg = 'createOrder fail';
					$payLog->addLog($errorMsg);
					$ret['message'] = $errorMsg;
					break;
				}
				
				$tradePrice = $model->goodsInfo['price'];
				$tradeProductName = $model->goodsInfo['goodsDesc'];
				
				$userProfile = User_Profile::getInstance($userId)->getData();
				$tradeNick = $userProfile['nickName'];
				
				//'您在串燒三國中暱稱為【'.$tradeNick.'】的帳號成功購買了【'.$tradeProductName.'】'
				$message = 'Nhan vat '.$tradeNick.'cua ban trong KCO nhan thanh cong '.$tradeProductName.'';
			}
			//找回密码
			elseif($keyword == self::$KEY_MK)
			{
				//keyword userName
				$msgArr = explode(" ", $message);
				$userName = $msgArr[1];
				if(empty($userName) || !User_Model::getIdByThird($userName))
				{
					$errorMsg = 'verify: [notify] username not exist:'.$userName;
					$payLog->addLog($errorMsg);
					$ret['message'] = $errorMsg;
					break;
				}
				//请求官网注册系统(通过登录帐号找回密码)
				$verifyUrl = User_Model::$ECN_LOGIN_SERVER.'?';
				//$verifyUrl = 'http://114.141.131.234/xuc_dev/Web/Api/api.php?';
				$checkSig = substr( md5( $userName ) , 0 , 6 )."4#ES535";
				$checkSig = substr( md5( $checkSig ) , 0 , 6 );
				$postData = array(
					'method' => 'User.findPwdForMsg',
					'loginName' => $userName,
					'sig' => $checkSig,
					'appId' => User_Model::$ECN_LOGIN_APPID,
					'sid' => $GLOBALS['appSid'],
				);
				$payLog->addLog( "req: [notify.findPwdForMsg]".json_encode($postData) );
				$retRegInfo = Helper_WWW::get($verifyUrl , $postData );
				$payLog->addLog( "ack: [notify.findPwdForMsg]".$retRegInfo );
				do{
					$errorMsg = true;
				
					if(empty($retRegInfo)) break;
					$retRegInfo = json_decode($retRegInfo, true);
					if(empty($retRegInfo) || $retRegInfo['status'] != 0) break;
				
					$errorMsg = false;
				}while(0);
					
				if($errorMsg)
				{
					$errorMsg = 'verify: [notify] get password fail';
					$payLog->addLog($errorMsg);
					$ret['message'] = $errorMsg;
					break;
				}
					
				$message = 'Mat khau tai khoan '.$userName.' cua ban trong KCO la '.$retRegInfo['data']['pwd'];
				$customer = $retRegInfo['data']['tel'];
				$verifyUrl = 'http://7x26.com:8081/charging/sms/send?';
				$postData = array(
					'smsid' => $smsid,
					'security' => self::$security,
					'partnerid' => self::$partner,
					'keyword' => $keyword,
					'shortcode' => $shortcode,
					'customer' => $customer,
					'message' =>  $message,
				);
				$message = 'ok';
				$payLog->addLog( "req: [notify.sendsms]".json_encode($postData) );
				$verifyReceipt = Helper_WWW::get( $verifyUrl , $postData );
				$payLog->addLog( "ack: [notify.sendsms]".$verifyReceipt );
			}
			else
			{				
				$errorMsg = 'verify: [notify] keyword no correct';
				$payLog->addLog($errorMsg);
				$ret['message'] = $errorMsg;
				break;
			}
			
			$ret['status'] = 200;
			$ret['message'] = $message;
			
		}while(0);
		
		return $ret;
	}
	
	private function verify($errorMsg = '')
	{
		do{
			if(!empty($errorMsg))
			{
				$tradeDesc = $errorMsg;
				break;
			}
			
			if(empty($this->userId))
			{
				$tradeDesc = 'invalid userid';
				break;
			}
			
			return true;
		}while(0);
		
		$tradeTitle = 'Thất bại'; //交易失败
		$tradeInfo = 'Thất bại'; //交易失败
			
		include( __DIR__.'/Complete.php');
		exit;
	}
	
	/**
	 * 创建订单，发送商品
	 * @param unknown $thirdOrderId 第三方流水
	 * @param unknown $type 支付类型
	 */
	private function createOrder($thirdOrderId, $payType)
	{
		$payLog = new ErrorLog( "ThienPhuPayLog" );
		
		//充值
		$orderId = Payment_Model::getInstance($this->userId)->getOrderId($this->goodsInfo['goodsId']);
		
		if(empty($orderId)) return 0;
		
		$gid = $this->goodsInfo['goodsId'];
		$price = $this->goodsInfo['price'];
		$userId = $this->userId;
			
		$order = array(
			'id' => $orderId,
			'goodsName' => $gid,
			'goodsNum' => 1,
			'price' => $price,
			'discount' => 0,
			'totalPrice' => $price,
			'buyerId' => $userId,
			'tradeTime' => $_SERVER['REQUEST_TIME'],
			'addTime' => $_SERVER['REQUEST_TIME'],
			'email' => '',
			'platform' => $payType,
			'status' => 1,
			'thirdId' => $thirdOrderId,
		);
		$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		
		if( $rs == true )
		{
			$payLog->addLog( "createOrder:".json_encode( $order ) );
			Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , $payType , $order['price'] , $order['id'] );
			return 1;
		}
		else
		{
			return 0;
		}
		return 0;
		
	}
	
	private function getSmsMsgPre()
	{
		$ret = self::$KEY_KCO;
		
		if(!!$keyword = self::$partnerId2keyword[$this->getPartnerId()])
		{
			$ret = $keyword;
		}
		
		return $ret;
	}
	
	private function getPartnerId()
	{
		//第三方信息
		$thirdInfo = Data_Activity_Model::getInstance($this->userId)->getActivity(Activity_Model::ACTIVE_THIRD_INFO);
		
		$partner = $thirdInfo['data']['partner'];
		if(empty($partner)) $partner = self::$partner;
		
		return $partner;
	}
	
}

?>