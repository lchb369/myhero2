<?php

/**
 * 宝软平台购买和支付
 * @author Administrator
 *
 */
class Payment_BR_Api
{	
	/**
	 * 构造函数
	 */
	
	public static function buy( $userId  ,  $cardNum , $cardPwd , $pmId , $pcId , $price )
	{
		$orderId = $userId."_".$_SERVER['REQUEST_TIME']."_".rand( 1 , 100000 ) ;
		
		$requestParam = array(
				'order_id' => $orderId,
				'order_time' => $_SERVER['REQUEST_TIME'],
				'game_id' => "141",
				'user_name' => 'xc',
				'password' => 'ae8b88d9',
				'amount' => $price,
				'cardnum' => $cardNum,
				'cardpwd' => $cardPwd,
				'currency' => 'RMB',
				'pm_id' => $pmId,
				'pc_id' => $pcId,
				'return_url' => "http://211.144.68.46/myhero_br/Web/Api/api_game.php?method=Payment.BRPayNotify",
		);
	
		$validateString = sprintf(  
				"order_id=%s&order_time=%s&game_id=%s&key=%s&amount=%s&cardnum=%s&cardpwd=%s&pm_id=%s&pc_id=%s",
				$requestParam['order_id'],
				$requestParam['order_time'],
				$requestParam['game_id'], 
				"e16be8bfe529cce126" , 
				$requestParam['amount'],
				$requestParam['cardnum'],
				$requestParam['cardpwd'],
				$requestParam['pm_id'],
				$requestParam['pc_id'] 
				);
		$requestParam['validate_string'] = self::encpw( $validateString );

		//$requestUrl = "http://221.179.175.55/app/netgame/api/card_pay.php";
		$requestUrl = "http://pay.baoruan.com/netgame/api/card_pay.php";
		$result = self::post_request( $requestUrl ,  $requestParam );
		return $result;
	}
	
	/**
	 * 支付回调
	 */
	public static function payNotify()
	{
		 $response = $_POST; 
		 $validateString = sprintf( "key=%s&result=%s&order_id=%s&amount=%s&pay_amount=%s&game_id=%s" , 
		 		"e16be8bfe529cce126" , $response['result'] , $response['order_id'] , $response['amount'] , $response['pay_amount'] , $response['game_id']
		 		); 
		 if( self::encpw( $validateString ) != $response['verifystr'] )
		 {
		 	return false;
		 }
		 else 
		 {
		 	$payLog = new ErrorLog( "BRPayNotify" );
		 	$payLog->addLog(  "payNotify:OK" );
		 	
		 	if( $response['result'] == 1 )
		 	{
		 		return false;
		 	}
		 	
		 		 	
		 	//创建定单
		 	$order = array(
		 			'id' =>$response['order_id'],
		 			'goodsName' => 9999,
		 			'goodsNum' => 1,
		 			'price' => $response['pay_amount'],
		 			'discount' => 0,
		 			'totalPrice' =>  $response['pay_amount'],
		 			'buyerId' =>"",
		 			'tradeTime' => $_SERVER['REQUEST_TIME'],
		 			'addTime' => $_SERVER['REQUEST_TIME'],
		 			'email' =>'',
		 			'platform' => "br",
		 			'status' => 1,
		 			'thirdId' =>"",
		 	);
		 	
		 	
		 	$orderArr = explode( "_", $response['order_id'] );
		 	$userId = $orderArr[0];
		 	
		 	$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		 	if( $rs == true )
		 	{
		 		$payLog = new ErrorLog( "addCoin" );
		 		$addCoin = (int)$response['pay_amount'];
		 		$orderData = Data_Order_Model::getInstance( $userId )->getData();
		 		$orderCount = count( $orderData );
		 		if( Data_Order_Model::getInstance( $this->userId )->isFirstRecharge() )
		 		{
		 			$payLog->addLog(  "uid:".$userId."addCoin*2:".$addCoin."firstRecharge" );
		 			$addCoin = $addCoin * 2;
		 		
		 		}
		 			
		 		$payLog->addLog(  "uid:".$userId."addCoin:".$addCoin."orderCount".$orderCount );
		 		/**
		 		 * 运营统计：充值记录
		 		*/
		 		Stats_Model::addPayUser(
		 		$userId ,
		 		$_SERVER['REQUEST_TIME'] ,
		 		$response['pay_amount'] ,	//价格
		 		$addCoin , 	//元宝数
		 		"999" ,	//商口ID
		 		$orderCount,
		 		"br"
		 		);
		 		
		 		/**
		 		 * EDAC数据中心统计
		 		 */
		 		Stats_Edac::recharge( $userId, $response['order_id'] , $response['pay_amount'], $addCoin, "宝软" );
		 		
		 		$status = User_Info::getInstance( $userId )->changeCoin( $addCoin , '充值' );
		 		if( $status  == true )
		 		{
		 			return true;
		 		}
		 		else 
		 		{
		 			return false;
		 		}
		 			
		 	}
		 	else
		 	{
		 		return true;
		 	}
		 	return false;
		 }
		 
		
	}
	
	
	/**
	 * 提交请求
	 * @param unknown $url
	 * @param unknown $params
	 * @return Ambigous <mixed, string>
	 */
	protected  static  function post_request( $url , $params)
	{
		$post_string = self::create_post_string(  $params );
		$ch = curl_init();
		curl_setopt( $ch , CURLOPT_POSTFIELDS , $post_string );
		curl_setopt( $ch , CURLOPT_RETURNTRANSFER , true );
		curl_setopt( $ch , CURLOPT_CONNECTTIMEOUT ,10  );
		curl_setopt( $ch , CURLOPT_TIMEOUT , 10  );
		curl_setopt( $ch , CURLOPT_POST, 1);
	
		$useragent = 'REST API PHP5 Client 1.0 (curl) ' . phpversion();
		curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
		curl_setopt( $ch , CURLOPT_URL , $url  );	
		$result = curl_exec($ch);
		if( curl_errno( $ch ) > 0 )
		{
			$result = curl_error( $ch );
		}
		curl_close($ch);
		return $result;
	}
	/**
	 * 构造参数
	 * @param unknown $params
	 * @return string
	 */
	protected static function create_post_string( $params )
	{
			$post_params = array();
			foreach ($params as $key => &$val) 
			{
				if( $key == "validate_string" )
				{
					$post_params[] = $key.'='.urlencode($val);
				}
				else
				{
					$post_params[] = $key.'='.$val;
				}	
			}
			return implode( '&', $post_params );
	}
	
	
	protected  static  function encpw( $src )
	{
		$spw=$src;
		//$spw=base64_decode($src);
		$Xbox1 = array( chr(75), chr(99), chr(200), chr(24),  chr(64),  chr(10), chr(23), chr(52) );
		$Xbox2 = array( chr(12), chr(28), chr(21),  chr(100), chr(29),  chr(44), chr(87), chr(23) );
		$Xbox3 = array( chr(23), chr(11), chr(33),  chr(134), chr(123), chr(29), chr(12), chr(12));
		$Xbox4 = array( chr(39), chr(22), chr(19),  chr(103), chr(145), chr(199),chr(20), chr(77) );
		$len = strlen( $spw);
		for ( $c = 0; $c <$len; $c++ )
		{
			$index = $c % 8;
			$spw[$c] =$spw[$c]^ $Xbox1[$index];
			$spw[$c] =$spw[$c]^ $Xbox2[$index];
			$spw[$c] =$spw[$c]^ $Xbox3[$index];
			$spw[$c] =$spw[$c]^ $Xbox4[$index];
		}
		for (  $pos=0; $pos < $len;  $pos++ )
		{
			if ( ord($spw[$pos])==0)
			{
				$spw[$pos] = $src[$pos];
			}
		}
		return base64_encode($spw);
	}
	

}
