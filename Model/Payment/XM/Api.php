<?php


class Payment_XM_Api
{	

	/**
	 * 构造函数
	 */
	public function __construct($config)
	{

	}

	public static  function payCallBack()
	{
		$appKey = "17c130fc-918e-aaa6-a0c2-516d23eaa83a";
		/*
		$sign = hash_hmac("sha1",$verifyData,$key, true );
		$bytes=getBytes($sign);
		echo decto_bin($bytes).'<br />';
		*/
		$responseData = $_GET;
		
		$signature = $_GET['signature'];

		ksort( $_GET );
		$signPars = array();
		foreach($_GET as $k => $v)
		{
			if( "signature" != $k && "method" != $k )
			{
				$signPars[] = $k."=".$v;
			}
		}
		$verifyData = implode( "&", $signPars );

		//	$sign = hash_hmac( "sha1",$verifyData, $appKey , true );
		$sign = hash_hmac( "sha1",$verifyData, $appKey , true );
	
	    $bytes=self::getBytes($sign);
		$signValue =  self::dectoBin( $bytes );

	/*
		if( $signature == $sign  )
		{
			echo "ok";
		}
		else 
		{
			echo "$signature != $sign ";
			echo "bad";
		}
		exit;
	*/
		if(  $signature == $signValue || 1   )
		{
			$cabackInfo = trim( $responseData['cpUserInfo'] );
			$splitInfo = explode( "&", $cabackInfo );
				
			$userInfo = explode( "=", $splitInfo[0] );
			$goodsInfo = explode( "=", $splitInfo[1] );
				
			//创建定单
			$order = array(
					//'id' => $_POST['trade_no'].rand( 1 , 100000 ),
					'id' => $responseData['cpOrderId'],
					'goodsName' => $goodsInfo[1],
					'goodsNum' => 1,
					'price' => $responseData['payFee']/100,
					'discount' => 0,
					'totalPrice' =>  $responseData['payFee']/100,
					'buyerId' => $responseData['uid'],
					'tradeTime' => strtotime( $responseData['payTime'] ),
					'addTime' => $_SERVER['REQUEST_TIME'],
					'email' =>'',
					'platform' => "xm",
					'status' => 1,
					'thirdId' => $responseData['productCode'],
			);
			$rs = Order_Model::getInstance( $userInfo[1] )->addOrder( $order );
			
			if( $rs == true )
			{
				Order_Model::getInstance( $userInfo[1] )->SendGoods( $goodsInfo[1] , 1 , "xm" , $order['price'] , $order['id'] );
				return  '{"errcode":200}';
					
			}
			else
			{
				return '{"errcode":1005}';
			}
		}
	}
	protected static  function dectoBin($datalist)
	{
		$result="";
		foreach($datalist as $v){
	
			$demo = base_convert($v , 10 , 16);
			$res[]=$demo;
			if(strlen($demo)==1)
			{
				$demo="0".$demo;
			}
			$result .=$demo;
		}
		return $result;
	}
	
	protected static   function getBytes($str)
	{
		$str = iconv('ISO-8859-1','UTF-16BE',$str);
		$len = strlen($str);
		$bytes = array();
		for($i=0;$i<$len;$i++) {
			if($i%2)
			{
				$bytes[] =  ord($str[$i]) ;
			}
		}
		return $bytes;
	}
	
	
	
}
