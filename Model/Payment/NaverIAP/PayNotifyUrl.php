<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

//---------------------------------------------------------
//Naver
//---------------------------------------------------------
class Payment_NaverIAP_PayNotifyUrl
{
	
	private static $pub_keys = array(
		"kr" => 	"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCHL4THGWANskHa9CuXHgr50KbnNjlcSYCFUN4bSbqnpEN3vcGHrYKM2LHDB5Gt7qxfU8JQGDkZSRaUgkE2JPlk2o0NPnc+Q7Fkw89fqEktyIFLnrxxvaESCf90ZFMW6szZqmx7yJpCfpTSmREF+myT+s22Ty3gNnqP1SwUrp6rxQIDAQAB",
	);
	
	/**
	 * 新版本
	 * @param unknown $userId
	 * @param unknown $signdata
	 * @param unknown $signature
	 */
	public static function verifyAndCreateOrder( $userId , $orderId , $signdata , $signature )
	{
		$payLog = new ErrorLog( "NaverOrder" );
		
		$cache = Common::getCache();
		$orderId = strval( $orderId );
		
		$orderInfo = $cache->get( $orderId );
		if( empty( $orderInfo ) )
		{
			$payLog->addLog( "verifyError: emptyOrderInfo, orderId ".$orderId);
			return false;
		}
		$orderInfo = json_decode( $orderInfo , true );
		if( empty( $orderInfo ) )
		{
			$payLog->addLog( "verifyError: decode orderInfo fail, orderInfo ".$orderInfo);
			return false;
		}
		
		$signdata = urldecode( $signdata );
		$signature = urldecode( $signature );
		$pub_key_id=self::$pub_keys[Common::getConfig('platform')];
		
		$signdata = str_replace('\"' ,'"', $signdata);
		$signature=str_replace(' ', '+', $signature);
		
		$pub_key_id = self::der2pem($pub_key_id);
		
		try{
			$success = openssl_verify( $signdata , base64_decode( $signature ), openssl_get_publickey( $pub_key_id ) );
		}catch (Exception $e){
			$payLog->addLog( "verifyError: openssl_verify throw error");
			return false;
		}
		
		if($success==1)
		{
			$payLog->addLog( "verifySuccess:".$_SERVER['REQUEST_URI'].json_encode( $orderInfo ) );
			//signdata的内容
// 			{
// 				"receipt": {
// 					"extra": "cd00de7757f488d6959ad663d04d1406",
// 					"environment": "TEST",
// 					"productCode": "1000002899",
// 					"paymentSeq": "1000723315",
// 					"paymentTime": 1381976868986,
// 					"approvalStatus": "APPROVED"
// 				},
// 				"nonce": 9.2386909701591e+16
// 			}
			$info=json_decode( $signdata , true );
			if( empty( $info['receipt'] ) ){
				$payLog->addLog( "verifyError: decode signdata fail");
				return false;
			}
			
			$info = $info['receipt'];
			if($orderId != $info['extra'])
			{
				$payLog->addLog( "verifyError: invalid orderId");
				return false;
			}
			
			$goodsConfig = Common::getConfig( "goods" );
			$gid = 0;
			foreach ( $goodsConfig as $goodsInfo ){
				if( $goodsInfo['NaverId'] == $info['productCode'] )
				{
					$gid = $goodsInfo['goodsId'];
					break;
				}
			
			}
			return self::_createOrder( $userId , $gid , $orderInfo , $info['orderId'] );
		}
		else
		{
			$payLog->addLog( "verifyError: openssl_verify fail");
			return false;
		}
		
	}
	
	private static function _createOrder( $userId , $gid , $orderInfo , $thirdId )
	{
		$payLog = new ErrorLog( "NaverOrder" );
		//是否存在商品
		$goodsConfig = Common::getConfig( "goods" );
		if( !$goodsConfig[$gid] )
		{
			$payLog->addLog( "createOrderError: goodsConfig not exist ".$gid);
			return false;
		}
		
		//是否已经发送过
		$status = Order_Model::getInstance(  $userId )->checkOrderStatus( $orderInfo['id'] );
		if( $status == 1 )
		{
			$payLog->addLog( "createOrderError: already send");
			return false;
		}
		
		$price = $goodsConfig[$gid]['price'];
		
		$order = array(
			'id' => $orderInfo['id'],
			'goodsName' => $gid,
			'goodsNum' => 1,
			'price' => $price,
			'discount' => 0,
			'totalPrice' => $price,
			'buyerId' => $userId,
			'tradeTime' => $_SERVER['REQUEST_TIME'],
			'addTime' => $orderInfo['addTime'],
			'email' => '',
			'platform' => "Naver",
			'status' => 1,
			'thirdId' => $thirdId,
		);
		$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		if( $rs == true )
		{
			$payLog->addLog( "createOrder:".json_encode( $order ) );
			Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , "Naver" , $price , $order['id'] );
			return true;
		}
		else
		{
			$payLog->addLog( "createOrderError: add order fail");
			return false;
		}
	}
	
	private function der2pem($der_data) {
		$pem = chunk_split($der_data, 64, "\n");
		$pem = "-----BEGIN PUBLIC KEY-----\n".$pem."-----END PUBLIC KEY-----\n";
		return $pem;
	}
	
}

?>