<?php

class Payment_JF_Api
{	
	/**
	 * 构造函数
	 */
	public function __construct()
	{
	
	}
	
	public static  function payCallBack()
	{
		
		//获取post过来的数据
		$response= file_get_contents("php://input");
		$payLog = new ErrorLog( "JFPayNotify" );
		$payLog->addLog( "JFPayNotify Response :".$response );
		
		//3.1.3 订单主动通知接口
		$time=$_REQUEST['time'];
		$sign=$_REQUEST['sign'];
		
		$result="<response><ErrorCode>0</ErrorCode><ErrorDesc>Success</ErrorDesc></response>";
		
		//8082863654145655537L
		$userId='79808218';//请配置用户ID
		
		//正确接收到相应的数据
		if(!empty($time)&&!empty($sign)  || 1 )
		{
			//
			$encodeString=md5($userId.$time);
			 
			//通过签名
			if( 1 || $sign==$encodeString){
	
				$payLog->addLog( "JFPayNotify checkSign : $sign != $encodeString"  );
				
			//获取post过来的数据
			$response= file_get_contents("php://input");
			
		//	$response = '<response ><order_id>00002010090922</order_id><cost>10</cost><appkey>79808218</appkey><create_time>12345678</create_time></response>';
			
			//创建解析器
			$parser = xml_parser_create();
			//解析到数组
			xml_parse_into_struct($parser, $response, $values, $index);
			//释放解析器
			xml_parser_free($parser);
			
		
			
			$orderId=$values[1]['value'];
			//
			$appkey=$values[2]['value'];
			//
			$cost=$values[3]['value']/10;
			//
			$createTime=$values[4]['value'];
		
			if(!empty($orderId)&&!empty($appkey)&&!empty($cost)&&!empty($createTime))
			{
			
			
			/*	
				$order = array(
						'id' => $orderId,
						'goodsName' => $goodsId,
						'addTime' => $_SERVER['REQUEST_TIME'],
						'platform' => $userProfile['platform'],
						'uid' => $this->userId,
				);
				*/
				$cache = Common::getCache();
				$orderData = $cache->get( $orderId );
				if( empty( $orderData ) )
				{
					return "no order info";
				}
			
			
				
				$orderInfo = json_decode( $orderData , true );
				$userId = $orderInfo['uid'];
				$goodsId = $orderInfo['goodsName'];
				//创建定单
				$order = array(
						//'id' => $_POST['trade_no'].rand( 1 , 100000 ),
						'id' => $orderId,
						'goodsName' => $orderInfo['goodsName'],
						'goodsNum' =>  1,
						'price' => $cost,
						'discount' => 0,
						'totalPrice' =>  $cost,
						'buyerId' => $orderInfo['uid'],
						'tradeTime' => $createTime,
						'addTime' => $_SERVER['REQUEST_TIME'],
						'email' =>'',
						'platform' => "jf",
						'status' => 1,
						'thirdId' => "",
				);
				
				
				$rs = Order_Model::getInstance( $userId )->addOrder( $order );
				
				if( $rs == true )
				{
					Order_Model::getInstance(  $userId  )->SendGoods( $goodsId , 1 , "jf" , $cost , $orderId );
					return  'SUCCESS';
						
				}
				else
				{
					return 'FAILURE';
				}
					//处理你的业务
					//$result='<response><ErrorCode>1</ErrorCode><ErrorDesc>Success</ErrorDesc></response>';
			}
		}
		}
		//返回信息给机锋服务器
		echo $result;
	
	}
	
}
