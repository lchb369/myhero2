<?php 

/**
 * 悠悠村回调地址
 * @author liuchangbing
 *
 */

class Payment_UUCun_Model
{

	/**
	 * 处理回调
	 */
	public static function process()
	{
		/**
appkey：djQJM1NO4yo4RwJ0s1AziBRnO7H0Lk5n
应用密钥：FIwmuQ，DES密钥：JIiMjrgC
		 */
		
		$appKey = "djQJM1NO4yo4RwJ0s1AziBRnO7H0Lk5n";
		$desKey = "JIiMjrgC";
	
		/*	
		$j = '{"callback_rsp":"87e563046ded705298f50567f1745dc19775a    50ff6f3136dfc148e0658472be501a764c22de3c1bc9f792802f3cd2b5a9292ff7749121fb445a03d1412e0f5e225494eb26b2c62ef23ece9bdc784e27244067b9140426ddec1fbc42eaa8995710c0ab4eef4585c6ae6    d51c17f5b30d018091a9425ceca3ae406a6a3d225887ec0d479cdfa0a376b5a2c5b687380841e60f59aee4530ac20703e7fa5408caa817d88aa550ab3137a3a1dd7d0b3702fafb7c63367108a108955502045711d944c    8aba00b8b2e6557a227ca5d68fea045259abf7672c6920bc409c6931d3ccdff68d49173703f7072bfc5ce1753bb25e495","callback_user":"d19907db9e9011fe7bf2caaf8ea353180fdd04b02cac97850029f8037    6f58d977c159531247ce819afe024e22fd46f0e","callback_appkey":"852fa1663a06b2faba9c347d6e397247b60c365add1693b6b328eba64730185922e2a45432df6703"}';		
		
		$_POST = json_decode(  $j , true );
		*/

		$callback_rsp = $_POST['callback_rsp'];
		$callback_user = $_POST['callback_user'];	
		$callback_appkey = $_POST['callback_appkey'];
		

		$callback_rsp = str_replace( " ", "", $callback_rsp );
		$callback_user = str_replace( " ", "", $callback_user );
		
		
		$crypt = new DESCrypt( $desKey , 0 );
		$callback_rsp_str = $crypt->decrypt( $callback_rsp );
		$callback_user_str = $crypt->decrypt( $callback_user );
		
		
		$crypt = new DESCrypt( "2SoXIhFB" );
		$callback_appkey_str = $crypt->decrypt( $callback_appkey );
		
		
		if( $callback_appkey_str != $appKey )
		{
			echo "appkey error";
			return ;
		}
		
		
		$rspParams = self::parseRsp(  $callback_rsp_str );
		$userParams = self::parseUserStr( $callback_user_str );
		
		/**
		 * 订单处理
		 */
		
		if( $rspParams['rsp_code'] != '000000')
		{
			return;
		}
		
		//单位元
		$price = intval( $rspParams['actual_txn_amt'] / 100 );
		
		//用户ID
		$userId = $userParams['indexNameID'];
		$goodsConfig = Common::getConfig( 'goods');
		
		$goodsId = 0;
		foreach ( $goodsConfig as $goodConf )
		{
			if( $goodConf['price'] == $price )
			{
				$goodsId = $goodConf['goodsId'];
			}
		}
		
		
		//创建定单	
		$order = array(
				'id' => $rspParams['order_id'],
				'goodsName' => $goodsId,
				'goodsNum' => 1,
				'email' => "",
				'price' => $price,
				'discount' => 0.00,
				'totalPrice' => $price,
				'buyerId' => $userId,
				'tradeTime' => $rspParams['txn_time'],
				'addTime' => $_SERVER['REQUEST_TIME'],
				'status' => 1,
				'platform' => 'uuc',
				'thirdId' => $rspParams['order_id'],
		);
		
		
		$status = Order_Model::getInstance( $userId )->addOrder( $order );
		if( $status == 1 )
		{
			$status = Order_Model::getInstance( $userId )->SendGoods( $order['goodsName'] , 1 , "uuc" , $price , $order['id'] );
			if( !$status )
			{
				$Result['ErrorCode'] =  4;
				$Result['ErrorDesc'] =  '发货失败';
				return json_encode( $Result);
			}
		}
		else
		{
			$Result['ErrorCode'] =  6;
			$Result['ErrorDesc'] =  '订单更新失败';
			return json_encode( $Result);
		}
	}
	
	
	/**
	 * 解析字符串
	 * @param unknown $str
	 * @return multitype:Ambigous <>
	 */
	public static function parseRsp( $str )
	{
		$strSplit = explode( "&", $str );
		$params = array();
		foreach ( $strSplit as $eachStr )
		{
			$paramArr = explode( "=", $eachStr );
			$params[$paramArr[0]] = $paramArr[1];
		}
		return $params;
	}
	
	
	/**
	 * 解析字符串
	 * @param unknown $str
	 * @return multitype:Ambigous <>
	 */
	public static function parseUserStr( $str )
	{
		$strSplit = explode( "#", $str );
		$params = array();
		foreach ( $strSplit as $eachStr )
		{
			$paramArr = explode( "=", $eachStr );
			$params[$paramArr[0]] = $paramArr[1];
		}
		return $params;
	}
	
	
}



class DESCrypt {
	var $key;
	var $iv;
	function DESCrypt($key, $iv = 0) {
		$this->key = $key;
		if ($iv == 0) {
			$this->iv = $key;
		} else {
			$this->iv = $iv;
		}
	}

	function encrypt($str) {
		$size = mcrypt_get_block_size(MCRYPT_DES, MCRYPT_MODE_CBC);
		$str = $this->pkcs5Pad($str, $size);
		return strtoupper( bin2hex(mcrypt_cbc(MCRYPT_DES, $this->key, $str, MCRYPT_ENCRYPT, $this->iv)));
	}

	function decrypt($str) {
		$strBin = $this->hex2bin(strtolower($str));
		$str = mcrypt_cbc(MCRYPT_DES, $this->key, $strBin, MCRYPT_DECRYPT, $this->iv);
		$str = $this->pkcs5Unpad($str);
		return $str;
	}
	function hex2bin($hexData) {
		$binData = "";
		for($i = 0; $i  < strlen($hexData); $i += 2) {
			$binData .= chr(hexdec(substr($hexData, $i, 2)));
		}
		return $binData;
	}
	 
	function pkcs5Pad($text, $blocksize) {
		$pad = $blocksize - (strlen($text) % $blocksize);
		return $text.str_repeat(chr($pad), $pad);
	}

	function pkcs5Unpad($text) {
		$pad = ord($text{strlen($text) - 1});
		if ($pad > strlen($text)) return false;
		if (strspn($text, chr($pad), strlen($text) - $pad) != $pad)   return false;
		return substr ($text, 0, - 1 * $pad);
	}

}



//此处KEY是演示用，实际的des key请问商务索取
/*
$key = "2SoXIhFB";
$crypt = new DESCrypt($key, 0);

$input = "A6720456D80AB9D06E79A87424F8BC164307DB51BEA731A2EED78CF6FA2202515D220936B7E9577FD57B651CD411E4379777DA22CBDAE7BCAAE5106C7802C8D4230E4704C618EC1F57720686E91C7F6C29CE3C5D0D8A57641ECFECD525E5419B49BA2E92C4884F1240637D3220FF3585EB75DE3CF47BF24F338E5F5471A8F3C8F15DA18A54E2DD8424A1570F161DFCF6D8456F38CDA3F399566108A187B48643E48E1B5DBEEFC4B322D398374978EF25AFAFDE612595C26355A954D9B42CFFEDF140F875A0D4AC93AC9BE637F72E9CEB";
echo "decode-->". $crypt->decrypt($input);

*/


?>

