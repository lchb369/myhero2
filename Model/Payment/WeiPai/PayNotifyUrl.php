<?php

//---------------------------------------------------------
//mo9即时到帐支付后台回调示例，商户按照此文档进行开发即可
//---------------------------------------------------------
class Payment_WeiPai_PayNotifyUrl
{
	
	public static function proccess()
	{
		$cache = Common::getCache();
		$orderId = "weipaiOrderId".strval( $_GET['bookNo'] );
		$cache->set( $orderId , $_GET );
	}
	
	/**
	 * 创建定单
	 */
	public static function createOrder( $userId , $gid , $orderId )
	{
		
		$cache = Common::getCache();
		$orderId = "weipaiOrderId".strval( $orderId );
		$orderInfo = $cache->get( $orderId );
		
		if( empty( $orderInfo ) )
		{
			return false;
		}
		
		$goodsConfig = Common::getConfig( "goods" );
		if( !$goodsConfig[$gid] )
		{
			return false;
		}
		
		$price = $goodsConfig[$gid]['price'];
		
		$order = array(
				//'id' => $_POST['trade_no'].rand( 1 , 100000 ),
				'id' => $orderInfo['bookNo'],
				'goodsName' => $gid,
				'goodsNum' => 1,
				'price' => $price,
				'discount' => 0,
				'totalPrice' => $price,
				'buyerId' => $orderInfo['tel'],
				'tradeTime' => $_SERVER['REQUEST_TIME'],
				'addTime' => $_SERVER['REQUEST_TIME'],
				'email' => $orderInfo['tel'],
				'platform' => "weipai",
				'status' => 1,
				'thirdId' =>  $orderInfo['bookNo'],
		);
		$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		if( $rs == true )
		{
			
			$payLog = new ErrorLog( "createWeiPaiOrder" );
			$payLog->addLog( "callbackParam:".json_encode( $order ) );
			Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , "weipai" , $price , $order['id'] );
			return true;
				
		}
		else
		{
			return false;
		}
		
		
		
		
	}
	
}

?>