<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

//---------------------------------------------------------
//TStore
//---------------------------------------------------------
class Payment_TStore_PayNotifyUrl
{
	
	
	/**
	 * @param unknown $userId
	 * @param unknown $signdata
	 * @param unknown $signature
	 */
	public static function verifyAndCreateOrder($userId, $goodsId, $orderId, $sign)
	{
		$payLog = new ErrorLog( "TStoreOrder" );
		
		$key = "myhero_OA00415027#!@";
		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
		 
		if( $sign != md5( $key.$goodsId.$orderId ) )
			throw new GameException(   GameException::PARAM_ERROR  );
		 
		$cache = Common::getCache();
		if( !$order = $cache->get( $orderId ) )
			throw new GameException(   GameException::PARAM_ERROR  );
		 
		$orderInfo = $cache->get( $orderId );
		if( empty( $orderInfo ) )
		{
			$payLog->addLog( "verifyError: emptyOrderInfo, orderId ".$orderId);
			return false;
		}
		$orderInfo = json_decode( $orderInfo , true );
		if( empty( $orderInfo ) )
		{
			$payLog->addLog( "verifyError: decode orderInfo fail, orderInfo ".$orderInfo);
			return false;
		}

		return self::_createOrder( $userId , $goodsId , $orderInfo );
	}
	
	private static function _createOrder( $userId , $gid , $orderInfo , $thirdId = "")
	{
		$payLog = new ErrorLog( "TStoreOrder" );
		//是否存在商品
		$goodsConfig = Common::getConfig( "goods" );
		if( !$goodsConfig[$gid] )
		{
			$payLog->addLog( "createOrderError: goodsConfig not exist ".$gid);
			return false;
		}
		
		//是否已经发送过
		$status = Order_Model::getInstance(  $userId )->checkOrderStatus( $orderInfo['id'] );
		if( $status == 1 )
		{
			$payLog->addLog( "createOrderError: already send");
			return false;
		}
		
		$price = $goodsConfig[$gid]['price'];
		
		$order = array(
			'id' => $orderInfo['id'],
			'goodsName' => $gid,
			'goodsNum' => 1,
			'price' => $price,
			'discount' => 0,
			'totalPrice' => $price,
			'buyerId' => $userId,
			'tradeTime' => $_SERVER['REQUEST_TIME'],
			'addTime' => $orderInfo['addTime'],
			'email' => '',
			'platform' => "TStore",
			'status' => 1,
			'thirdId' => $thirdId,
		);
		$rs = Order_Model::getInstance( $userId )->addOrder( $order );
		if( $rs == true )
		{
			$payLog->addLog( "createOrder:".json_encode( $order ) );
			Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , "TStore" , $price , $order['id'] );
			return true;
		
		}
		else
		{
			$payLog->addLog( "createOrderError: add order fail");
			return false;
		}
	}
	
}

?>