<?php
abstract class Logical_Abstract
{
	protected $userId;
	
	protected static $dataObjects = array();
	
	protected static $configs = array();
	
	protected function __construct( $userId )
	{
		$this->userId = $userId;
	}
	
	/**
	 * 设置对象
	 * @param string $objectName	对象名
	 * @param object $object	对象
	 */
	public static function setObject( $userId , $objectName , $object )
	{
		self::$dataObjects[$userId][$objectName] = $object;
	}
	
	/**
	 * 获取配置
	 * @param string $configKey	配置文件键
	 */
	protected static function & getConfig( $configKey )
	{
		if( !isset( self::$configs[$configKey] ) )
		{
			self::$configs[$configKey] = Common::getConfig( $configKey );
		}
		return self::$configs[$configKey];
	}
	
	/**
	 * 设置配置
	 * @param string $configKey	配置文件键
	 * @param array $config
	 */
	public static function setConfig( $configKey , $config )
	{
		self::$configs[$configKey] = $config;
	}
	
}
?>