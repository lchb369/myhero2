<?php
if( !defined( 'IN_INU' ) )
{
    return;
}

/**
 * 武将卡编队模块
 * @name Info.php
 * @author liuchangbing
 * @since 2013-1-14
 *
 */
class Data_Card_Team extends Data_Abstract
{
	/**
	 * 单例对象
	 * @var	Data_Card_Team[]
	 */
	protected static $singletonObjects;
	/**
	 * 结构化对象
	 * @param	string $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId , $lock = false  )
	{
		$this->dbColumns = array(
			'card_team' => array(
				'columns' => array(
					'cards'
				) ,
				'isNeedFindAll' => false ,
			) ,
		);
		parent::__construct( $userId , 'card_team' , $lock  );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Data_Card_Team
	 */
	public static function & getInstance( $userId , $lock = false  )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId , $lock  );
		}
		
		if( $lock && !self::$singletonObjects[$userId]->isLocked() )
		{
			self::$singletonObjects[$userId] = new self( $userId , $lock  );
		}
		
		if( $lock )
		{
			ObjectStorage::register( self::$singletonObjects[$userId] );
		}
		
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 格式化保存到数据库的数据
	 * @param	array $table	表名
	 * @param	array $data		数据
	 * @return	array
	 */
	protected function formatToDBData( $table , $data )
	{		
		if( $table == 'card_team' && $data )
		{
			foreach ( $data as $key => $value )
			{
				if( in_array( $key , $this->dbColumns['card_team']['columns'] ) )
				{
					$returnData[$key] = $value;
				}	
			}
		}
		return $returnData;
	}
	
	protected function formatFromDBData( $table , $data )
	{
		return array( 'cards' => $data[1] );
	}
	
	protected function emptyDataWhenloadFromDB( $table )
	{	
		$data = array(
			'cards' => "",
		);
		$this->updateToDb( 'card_team' , self::DATA_ACTION_ADD , $data );
		return $data;
	}
	
	/**
	 * 设置武将编队
	 * @param int $leaderId
	 */
	public function setCards( $cardStr )
	{
		$this->data['cards'] = $cardStr;
		$this->updateToDb( 'card_team' , self::DATA_ACTION_UPDATE , array( 'cards' => $cardStr ) );
	}
	
	public function setCardsMulti($teamsInfo)
	{
		//主编队,4个副编队的副将|当前编队索引
		$teamArr = array();
		foreach($teamsInfo['teams'] as $key => $team)
		{
			//if($key > 0) array_shift($team);
			$teamArr[] = implode(",", $team);
		}
		$cardMultiStr = implode(";", $teamArr)."|".$teamsInfo['cur'];
		$this->data['cards'] = $cardMultiStr;
		$this->updateToDb( 'card_team' , self::DATA_ACTION_UPDATE , array( 'cards' => $cardMultiStr ) );
	}
	
	/**
	 * 获得当前编队
	 * @see Data_Abstract::getData()
	 */
	public function getData()
	{
		$teamInfo = $this->getAll();
		$curTeam = $teamInfo['teams'][$teamInfo['cur']];
		
		return array('cards' => implode(",", $curTeam));
	}
	
	/**
	 * 获得所有编队信息（包括当前编队）
	 * Enter description here ...
	 */
	public function getAll()
	{
		//1,2,3,5,7;2,4,6,8;0,0,0,0;2,4,6,8;2,4,6,8|3
		$teamInfo = parent::getData();
		
		$ret = array();
		if( $teamInfo['cards'] )
		{
			$teamInfoArr = explode("|", $teamInfo['cards']);
			$teamStr = explode( ";" , $teamInfoArr[0] );
			
			for($i = 0; $i < Card_Model::TEAM_NUM; $i++)
			{
				if(!empty($teamStr[$i]))
				{
					$teamArr = explode( "," , $teamStr[$i] );
					foreach ( $teamArr as $cardId )
					{
						$ret[$i][] = strval( $cardId );
					}
					
					//副编队中插入主将(现在副编队有自己的主将)
					/*
					if($i == 0)
					{
						$leader = $ret[$i][0];
					}
					elseif(!empty($leader)) 
					{
						array_unshift($ret[$i], $leader);
					}
					*/
				}
				elseif($i > 0 && !empty($ret[0][0]))
				{
					$ret[$i] = array($ret[0][0]);
				}
				
				//少武将补0
				//for($j = count($ret[$i]); $j < Card_Model::TEAM_CARD_NUM; $j++) array_push($ret[$i], "0");
			}
			
		}
		
		return array(
			'teams' => $ret,
			'cur' => intval($teamInfoArr[1]),
		);
	}
	
}
