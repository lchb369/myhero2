<?php
if( !defined( 'IN_INU' ) )
{
    return;
}

/**
 * 邮箱
 * @name Model.php
 * @author yanghan
 * @since 2013-08-23
 *
 */
class Data_Mail_Model extends Data_Abstract
{
	const STATUS_UNREAD = 1;
	const STATUS_READ = 2;
	const STATUS_GOT_BONUS = 3;
	
	/**
	 * 单例对象
	 * @var	Data_Mail_Model[]
	 */
	protected static $singletonObjects;
	
	/**
	 * 结构化对象
	 * @param	string $userId	用户ID
	 * @param	boolean $lock	是否加锁（需要写的话一定要加锁）
	 */
	public function __construct( $userId , $lock = false  )
	{
		$this->tablename = 'mail';
		$this->dbColumns = array(
			$this->tablename => array(
				'columnsInfo' => array(
					'id' => 0,
					'fromId' => 0,
					'title' => '',
					'content' => '',
					'bonus' => '',
					'sendTime' => $_SERVER['REQUEST_TIME'],
					'readTime' => 0,
					'deadTime' => 0,	//领取过期时间
					'lock' => 0,
					'status' => 0,
				),
				'isNeedFindAll' => true ,
			) ,
		);
		$this->_setColumns();
		parent::__construct( $userId , $this->tablename , $lock  );
	}
	
	/**
	 * 获取实例化
	 * @param	int $userId	用户ID
	 * @return	Data_Mail_Model
	 */
	public static function & getInstance( $userId , $lock = false , $isNotReadData = false , $isMock = false  )
	{
		if( !isset( self::$singletonObjects[$userId] ) )
		{
			self::$singletonObjects[$userId] = new self( $userId , $lock , $isNotReadData , $isMock  );
		}
		
		if( $lock )
		{
			ObjectStorage::register( self::$singletonObjects[$userId] );
		}
		
		return self::$singletonObjects[$userId];
	}
	
	/**
	 * 格式化保存到数据库的数据
	 * @param	array $table	表名
	 * @param	array $data		数据
	 * @return	array
	 */
	protected function formatToDBData( $table , $data )
	{
		$returnData = array();
		if(!empty($data))
		{
			foreach ($data as $k=>$v)
			{
				if(in_array($k, array('title', 'content')))
				{
					$returnData[$k] = Helper_String::unicode_encode($v);
				}
				else
				{
					$returnData[$k] = $v;
				}
			}
		}
		
		return $returnData;
	}
	
	protected function formatFromDBData( $table , $data )
	{
		$returnData = array();
		if( !empty( $data ))
		{
			foreach ( $data as $row )
			{
				$idx = 0;
				$cols = &$this->dbColumns[$this->tablename]['columnsInfo'];
				foreach($cols as $k=>$v)
				{
					if(in_array($k, array('title', 'content')))
					{
						$returnData[$row[1]][$k] = Helper_String::unicode_decode($this->_getDataByType( $v , $row[++$idx]));
					}
					else 
					{
						$returnData[$row[1]][$k] = $this->_getDataByType( $v , $row[++$idx]);
					}
						
				}
			}
		}
		return $returnData;
	}
	
	protected function emptyDataWhenloadFromDB( $table )
	{
		return $this->data;
	}
	
	public function & getData()
	{
		return $this->data;
	}
	
	public function getMail($id)
	{
		return $this->data[$id];
	}
	
	public function sendMail($fromId, $title, $content, $bonus = null, $deadTime = null)
	{
		$id = 1;
		if( !empty( $this->data ) )
		{
			$ids = array_keys( $this->data );
			$id = max( $ids ) + 1;
		}

		$this->data[$id] = $this->dbColumns[$this->tablename]['columnsInfo'];
		$mail = &$this->data[$id];
		
		$mail['id'] = $id;
		$mail['fromId'] = $fromId;
		$mail['title'] = $title;
		$mail['content'] = $content;
		if($bonus) $mail['bonus'] = addslashes(json_encode($bonus));
		if($deadTime) $mail['deadTime'] = $deadTime;
		$mail['status'] = Data_Mail_Model::STATUS_UNREAD;
		
		$this->updateToDb( $this->tablename , self::DATA_ACTION_ADD , $this->data[$id] );
		return $id;
	}
	
	public function readMail($id)
	{
		$ret = false;
		$mail = &$this->data[$id];
		if(!empty($mail))
		{
			if($mail['status'] == Data_Mail_Model::STATUS_UNREAD)
			{
				$mail['status'] = Data_Mail_Model::STATUS_READ;
				$mail['readTime'] = $_SERVER['REQUEST_TIME'];
				$this->updateToDb( $this->tablename , self::DATA_ACTION_UPDATE , $this->data[$id] );
			}
			$ret = array('mail' => $this->data[$id]);
		}
		return $ret;
	}
	
	public function lockMail($id)
	{
		$ret = false;
		$mail = &$this->data[$id];
		if(!empty($mail) && $mail['lock'] == 0)
		{
			$mail['lock'] = 1;
			$this->updateToDb( $this->tablename , self::DATA_ACTION_UPDATE , $this->data[$id] );
			$ret = array('id' => $id);
		}
		return $ret;
	}
	
	public function unLockMail($id)
	{
		$ret = false;
		$mail = &$this->data[$id];
		if(!empty($mail) && $mail['lock'] == 1)
		{
			$mail['lock'] = 0;
			$this->updateToDb( $this->tablename , self::DATA_ACTION_UPDATE , $this->data[$id] );
			$ret = array('id' => $id);
		}
		return $ret;
	}
	
	public function getBonus($id)
	{
		$ret = false;
		$mail = &$this->data[$id];
		if(!empty($mail) && $mail['status'] == Data_Mail_Model::STATUS_READ 
			&& ($mail['deadTime'] == 0 || $mail['deadTime'] > $_SERVER['REQUEST_TIME']))
		{
			$mail['status'] = Data_Mail_Model::STATUS_GOT_BONUS;
			$this->updateToDb( $this->tablename , self::DATA_ACTION_UPDATE , $this->data[$id] );
			$ret = array('id' => $id);
		}
		return $ret;
	}
	
	public function delMail($id)
	{
		$ret = false;
		$mail = &$this->data[$id];
		if(!empty($mail))
		{
			$log = new ErrorLog( "mailHistory" );
			$platform = Common::getConfig("platform");
			$msg = "delMail>>platform:{$platform};userId:{$this->userId};mailId:{$this->data[$id]['cardId']};"
				."delTime:{$_SERVER['REQUEST_TIME']};"
				."content:".json_encode($this->data[$id]);
			$log->addLog( $msg );
			$this->updateToDb( $this->tablename , self::DATA_ACTION_DELETE , array( 'id' => $id ) );
			unset( $this->data[$id] );
			$ret = array('id' => $id);
		}
		return $ret;
	}
	
	public function getList($page, $size, $type)
	{
		$list = array();
		$_allData = array();
		$unreadList = array();
		$readList = array();
		foreach($this->data as &$mail)
		{
			if($type != 1 
			|| ($type == 1 && $mail['status'] == Data_Mail_Model::STATUS_UNREAD))
			{
				if($mail['status'] == 1)
				{
					$_temp = &$unreadList;
				}
				else
				{
					$_temp = &$readList;
				}
				//新邮件放最前面
				array_unshift($_temp, $mail['id']);
			}
		}
		
		if(!empty($unreadList) && !empty($readList))
		{
			$_allData = array_merge($unreadList, $readList);
		}
		elseif(empty($unreadList) && !empty($readList))
		{
			$_allData = $readList;
		}
		elseif(empty($readList) && !empty($unreadList))
		{
			$_allData = $unreadList;
		}
		
		
		$cnt = count($_allData);
		$totalPage = $cnt % $size == 0 ? $cnt / $size : ceil($cnt / $size);
		
		for($i = $page * $size; $i < ($page + 1) * $size; $i++)
		{
			if($i >= $cnt) break;
			//截取邮件内容
			$mail = &$this->data[$_allData[$i]];
			$mail['content'] = mb_substr($mail['content'], 0, 12, 'utf-8');
			$list[] = $mail;
		}
		
		return array('list' => $list, 'size' => $size, 'page' => $page, 'total' => $cnt, 'totalPage' => $totalPage);
	}
	
	public function getUnreadNum()
	{
		$cnt = 0;
		foreach($this->data as &$mail)
		{
			if($mail['status'] == Data_Mail_Model::STATUS_UNREAD) $cnt++;
		}
		return $cnt;
	}
	
}
