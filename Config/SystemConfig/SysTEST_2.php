<?php
/**
 * 系统配置(运维需要更改的配置)
 * 文件名为:Sys+pf+_sid
 * 根据客户端:pf , sid
 */
return array
(
	/**
	 * 系统配置,官网一区
	 */
	//游戏应用ID
	'appId' => 1003,
		
	//区服ID
	'sid' => 2,
		
	//平台
	'platform' => 'test2',
	//缩写
	'pf' => 'test',


	/**
	 * 服务器配置
	 */
	//memcache客户端类名（memcache:InuMemcache,libMemcache：InuMemcached）
	'memcacheClass' => 'InuMemcache' ,
	
	//Memcache配置
	'memcache' => array
	(
		//游戏数据集群
		'data' => array
		(
			array(	"host" => "127.0.0.1" , "port" => 11213 ) ,
		) ,
		
		//Session集群
		'session' => array
		(
			array(	"host" => "127.0.0.1" , "port" => 11211 ) ,
		) ,
		
		//etc集群
		'etc' => array
		(
			array(	"host" => "127.0.0.1" , "port" => 11211 ) ,
		) ,
		
		//lock集群
		'lock' => array
		(
			array(	"host" => "127.0.0.1" , "port" => 11211 ) ,
		) ,
		
		//游戏数据库索引集群（只有在dbClassName配置是MysqlDber或者使用到直接访问数据库才生效）
		'index' => array
		(
			array(	"host" => "127.0.0.1" , "port" => 11211 ) ,
		) ,
	) ,
		
	'mysqlPool' => array(
		'game' => array(
			//库1
			1 => "http://127.0.0.1:32003/services/worker.fcgi",
		), 
		'dbIndex' => "http://127.0.0.1:33002/services/worker.fcgi",
		'userIndex' => "http://127.0.0.1:34002/services/worker.fcgi",
	),
	
	//数据库类名(Dber:中间件数据库,MysqlDber:MYSQL数据库)
	'dbClassName' => 'MysqlPool' ,
		
	//Mysql数据库配置(在使用MYSQL数据库时配置)
	'mysqlDb' => array
	(
		'game' => array( 'host' => '127.0.0.1' , 'port' => '3306' , 'user' => 'root' , 'passwd' => 'mysqlpandora' , 'name' => 'myhero_test2' ) ,
		//用户ID索引，分表设计
		'dbIndex' => array( 'host' => '127.0.0.1' , 'port' => '3306' , 'user' => 'root' , 'passwd' => 'mysqlpandora' , 'name' => 'dbIndex' ) ,
		//用户ID转换库,仅作转换用
		'userIndex' => array( 'host' => '127.0.0.1' , 'port' => '3306' , 'user' => 'root' , 'passwd' => 'mysqlpandora' , 'name' => 'userIndex' ) ,
	),
	

	//mongodb配置
	'mongoDb' => array(
			'statsDB' => array( "host" => '192.168.1.3:27017', "dbname" => "myheroStatsDBTest" ),
	),
	
	//xhprof执行效率检查配置
	'xhprof' => array(
		'isOpen' => false ,	//是否使用执行效率检查；true => 使用；false => 不使用
		'logDir' => '/tmp/xhprof' ,
	) ,
	
	
	'cli' => array(
		'randLimit' => 1000,
		'checkDataDir' => '/',
	),
	
);
