<?php
/**
 *	区/服配置
 */
return array
(
	//测试服配置
	'test_1' =>  array(
		'sid' => 1,
		'serverName' => 'Sever 1  Ngụy Quốc',
		'statsName' => '内网测试',
		'pf' => 'test',
		'notice' => 'http://sg.ecngame.com/noticed.html',
		'serverIp' => array(
			'211.144.68.31',
		),
		'enable' => 1,
	),
		
	'test_2' =>  array(
		'sid' => 2,
		'serverName' => 'Sever 2  Thục Quốc',
		'statsName' => '内网测试',
		'pf' => 'test',
		'notice' => 'http://sg.ecngame.com/noticed.html',
		'serverIp' => array(
			'211.144.68.31',
		),
		'enable' => 1,
	),
		
	//台湾测试服
	'twtest_3' =>  array(
		'sid' => 3,
		'pf' => 'twtest',
		'serverName' => '一区 桃园结义',
		'statsName' => '台湾测试',
		'notice' => 'http://www.joifun.com.tw/External/SGH/Game/noticeft.html',
		'serverIp' => array(
			'211.144.68.31',
		),
		'enable' => 1,
	),
		
	//韩国测试服
	'krtest_4' =>  array(
		'sid' => 4,
		'pf' => 'krtest',
		'serverName' => '一区 桃园结义',
		'statsName' => '韩国测试',
		'notice' => 'http://sg.ecngame.com/notice.html',
		'serverIp' => array(
			'211.144.68.31',
		),
		'enable' => 1,
	),
		
	//越南测试服
	'vntest_5' =>  array(
		'sid' => 5,
		'pf' => 'vntest',
		'serverName' => '一区 桃园结义',
		'statsName' => '越南测试',
		'notice' => 'http://kco.vn/Ingame/Public.html',
		'serverIp' => array(
			'211.144.68.31',
		),
		'enable' => 1,
	),
	
	//官网ios测试服
	'ugeiostest_6' => array(
		'sid' => 6,
		'serverName' => '一区 桃园结义',
		'statsName' => '官网IOS测试',
		'pf' => 'ugeiostest',
		'notice' => 'http://sg.ecngame.com/notice.html',
		'serverIp' => array(
			'211.144.68.31',
		),
		'enable' => 1,
	),
	
	/**正式服*/
	'ugeios_18' => array(
		'sid' => 18,
		'serverName' => '一区 桃园结义',
		'statsName' => '官网IOS',
		'pf' => 'ugeios',
		'notice' => 'http://sg.ecngame.com/notice.html',
		'serverIp' => array(
			'211.144.68.46',
		),
		'clientVersion' => array(
			array('platform' => 'uge', 'refer' => '201', 'name' => 'MyHero_V1.0.4', 'url' => '', 'version' => '1',),
		),
		'enable' => 1,
	),
	
	'wk_19' => array(
		'sid' => 19,
		'serverName' => '一区 桃园结义',
		'statsName' => '唯客一区',
		'pf' => 'wk',
		'notice' => 'http://www.joifun.com.tw/External/SGH/Game/noticeft.html',
		'serverIp' => array(
			'210.71.211.11',
		),
		'clientVersion' => array(
			array('platform' => 'wk', 'refer' => '100', 'name' => '', 'url' => '', 'version' => '19',),
		),
		'edacWeb' => '210.71.211.11',
		'enable' => 1,
	),
	
	'wkios_20' => array(
		'sid' => 20,
		'serverName' => '一区 桃园结义',
		'statsName' => '唯客IOS一区',
		'pf' => 'wkios',
		'notice' => 'http://www.joifun.com.tw/External/SGH/Game/noticeft.html',
		'serverIp' => array(
			'210.71.211.11',
		),
		'clientVersion' => array(
			array('platform' => 'wk_ios', 'refer' => '101', 'name' => 'MyHero_V1.0.6', 'url' => '', 'version' => '14',),
		),
		'edacWeb' => '210.71.211.11',
		'enable' => 1,
	),
	
	//韩国安卓
	'kr_25' =>  array(
		'sid' => 25,
		'pf' => 'kr',
		'serverName' => '一区 桃园结义',
		'statsName' => 'G/N/T',
		'notice' => 'http://sg.ecngame.com/notice.html',
		'serverIp' => array(
			'183.110.209.74',
		),
		'clientVersion' => array(
			array('platform' => 'kr', 'refer' => '1001', 'name' => '', 'url' => '', 'version' => '1',),
			array('platform' => 'kr_G', 'refer' => '1004', 'name' => '', 'url' => '', 'version' => '1',),
			array('platform' => 'kr_N', 'refer' => '1005', 'name' => '', 'url' => '', 'version' => '1',),
			array('platform' => 'kr_T', 'refer' => '1006', 'name' => '', 'url' => '', 'version' => '1',),
		),
		'edacWeb' => '183.110.209.74',
		'enable' => 1,
	),
	
	//韩国ios
// 	'krios_26' =>  array(
// 		'sid' => 26,
// 		'pf' => 'krios',
// 		'serverName' => '一区 桃园结义',
// 		'statsName' => '韩国ios ',
// 		'notice' => 'http://sg.ecngame.com/notice.html',
// 		'serverIp' => array(
// 			'183.110.209.74',
// 		),
// 		'clientVersion' => array(
// 			array('platform' => 'krios', 'refer' => '1003', 'name' => '', 'url' => '', 'version' => '1',),
// 		),
// 		'enable' => 1,
// 	),
	
	//韩国kakao
	'krkk_27' =>  array(
		'sid' => 27,
		'pf' => 'krkk',
		'serverName' => '一区 桃园结义',
		'statsName' => 'Kakao安卓',
		'notice' => 'http://sg.ecngame.com/notice.html',
		'serverIp' => array(
			'183.110.209.74',
		),
		'clientVersion' => array(
			array('platform' => 'krkk', 'refer' => '1000', 'name' => '', 'url' => '', 'version' => '23',),
		),
		'edacWeb' => '183.110.209.74',
		'enable' => 1,
	),
	
	//韩国kakao ios
	'krkkios_28' =>  array(
		'sid' => 28,
		'pf' => 'krkkios',
		'serverName' => '一区 桃园结义',
		'statsName' => 'KakaoIOS',
		'notice' => 'http://sg.ecngame.com/notice.html',
		'serverIp' => array(
			'183.110.209.74',
		),
		'clientVersion' => array(
			array('platform' => 'krkkios', 'refer' => '1002', 'name' => '', 'url' => '', 'version' => '1',),
		),
		'edacWeb' => '183.110.209.74',
		'enable' => 1,
	),
	
	//越南
	'vn_29' =>  array(
		'sid' => 29,
		'pf' => 'vn',
		'serverName' => 'Sever 1  Ngụy Quốc',
		'statsName' => '越南',
		'notice' => 'http://kco.vn/Ingame/Public.html',
		'serverIp' => array(
			'210.245.94.59',
		),
		'clientVersion' => array(
			array('platform' => 'vn_ios', 		'refer' => '1102', 'name' => '', 'url' => '', 'version' => '3',),
			array('platform' => 'vn_ios_2', 	'refer' => '1103', 'name' => '', 'url' => '', 'version' => '1',),
			array('platform' => 'vn', 			'refer' => '1100', 'name' => '', 'url' => '', 'version' => '3',),
			array('platform' => 'vn_google', 	'refer' => '1101', 'name' => '', 'url' => '', 'version' => '1',),
		),
		'edacWeb' => '210.245.94.59',
		'enable' => 1,
	),
	
// 	'vn_30' =>  array(
// 		'sid' => 30,
// 		'pf' => 'vn',
// 		'serverName' => 'Sever 2  Thục Quốc',
// 		'statsName' => '越南',
// 		'notice' => 'http://kco.vn/Ingame/Public.html',
// 		'serverIp' => array(
// 			'210.245.94.59',
// 		),
// 		'clientVersion' => array(
// 			array('platform' => 'vn_ios', 'refer' => '1102', 'name' => '', 'url' => '', 'version' => '1',),
// 			array('platform' => 'vn_ios_2', 'refer' => '1103', 'name' => '', 'url' => '', 'version' => '1',),
// 			array('platform' => 'vn', 'refer' => '1100', 'name' => '', 'url' => '', 'version' => '1',),
// 			array('platform' => 'vn_google', 'refer' => '1101', 'name' => '', 'url' => '', 'version' => '1',),
// 		),
// 		'edacWeb' => '210.245.94.59',
// 		'enable' => 1,
// 	),
);
