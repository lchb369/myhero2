<?php
return array
(
	"100" => array( "id" => "wk" ,			 	"name" => "唯客安卓" ),
	"101" => array( "id" => "wk_ios" , 			"name" => "唯客IOS" ),

	"201" => array( "id" => "uge_ios" , 		"name" => "官网IOS" ),
	
	"1000" => array("id" => "kakao_android",	"name" => "kakao安卓"),
	"1001" => array("id" => "kr_android",		"name" => "G/N/T"),
	"1002" => array("id" => "kakao_ios",		"name" => "kakaoIOS"),
	"1003" => array("id" => "kr_ios",			"name" => "韩国IOS"),
	"1004" => array("id" => "kr_G",				"name" => "G"),
	"1005" => array("id" => "kr_N",				"name" => "N"),
	"1006" => array("id" => "kr_T",				"name" => "T"),

	"1100" => array("id" => "vn",				"name" => "越南安卓"),
	"1101" => array("id" => "vn_google",		"name" => "越南安卓Google"),
	"1102" => array("id" => "vn_ios",			"name" => "越南IOS越狱"),
	"1103" => array("id" => "vn_ios_2",			"name" => "越南IOS正版"),
);

?>