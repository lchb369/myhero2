<?php
return array (
  'columns' => 
  array (
    0 => 'level',
    1 => 'exp',
    2 => 'maxStamina',
    3 => 'leaderShip',
    4 => 'maxfriends',
    5 => 'bonus',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'level' => 1,
      'exp' => 0,
      'maxStamina' => 20,
      'leaderShip' => 20,
      'maxfriends' => 15,
      'bonus' => NULL,
    ),
    1 => 
    array (
      'level' => 2,
      'exp' => 85,
      'maxStamina' => 20,
      'leaderShip' => 21,
      'maxfriends' => 16,
      'bonus' => NULL,
    ),
    2 => 
    array (
      'level' => 3,
      'exp' => 200,
      'maxStamina' => 21,
      'leaderShip' => 22,
      'maxfriends' => 17,
      'bonus' => NULL,
    ),
    3 => 
    array (
      'level' => 4,
      'exp' => 400,
      'maxStamina' => 21,
      'leaderShip' => 23,
      'maxfriends' => 18,
      'bonus' => NULL,
    ),
    4 => 
    array (
      'level' => 5,
      'exp' => 800,
      'maxStamina' => 21,
      'leaderShip' => 24,
      'maxfriends' => 19,
      'bonus' => NULL,
    ),
    5 => 
    array (
      'level' => 6,
      'exp' => 1300,
      'maxStamina' => 22,
      'leaderShip' => 25,
      'maxfriends' => 20,
      'bonus' => NULL,
    ),
    6 => 
    array (
      'level' => 7,
      'exp' => 1900,
      'maxStamina' => 22,
      'leaderShip' => 26,
      'maxfriends' => 21,
      'bonus' => NULL,
    ),
    7 => 
    array (
      'level' => 8,
      'exp' => 2600,
      'maxStamina' => 22,
      'leaderShip' => 27,
      'maxfriends' => 22,
      'bonus' => NULL,
    ),
    8 => 
    array (
      'level' => 9,
      'exp' => 3600,
      'maxStamina' => 23,
      'leaderShip' => 28,
      'maxfriends' => 23,
      'bonus' => NULL,
    ),
    9 => 
    array (
      'level' => 10,
      'exp' => 5023,
      'maxStamina' => 23,
      'leaderShip' => 28,
      'maxfriends' => 24,
      'bonus' => NULL,
    ),
    10 => 
    array (
      'level' => 11,
      'exp' => 6639,
      'maxStamina' => 23,
      'leaderShip' => 29,
      'maxfriends' => 25,
      'bonus' => NULL,
    ),
    11 => 
    array (
      'level' => 12,
      'exp' => 8344,
      'maxStamina' => 24,
      'leaderShip' => 30,
      'maxfriends' => 26,
      'bonus' => NULL,
    ),
    12 => 
    array (
      'level' => 13,
      'exp' => 10280,
      'maxStamina' => 24,
      'leaderShip' => 31,
      'maxfriends' => 26,
      'bonus' => NULL,
    ),
    13 => 
    array (
      'level' => 14,
      'exp' => 12455,
      'maxStamina' => 24,
      'leaderShip' => 31,
      'maxfriends' => 27,
      'bonus' => NULL,
    ),
    14 => 
    array (
      'level' => 15,
      'exp' => 14878,
      'maxStamina' => 25,
      'leaderShip' => 32,
      'maxfriends' => 27,
      'bonus' => NULL,
    ),
    15 => 
    array (
      'level' => 16,
      'exp' => 17556,
      'maxStamina' => 25,
      'leaderShip' => 33,
      'maxfriends' => 28,
      'bonus' => NULL,
    ),
    16 => 
    array (
      'level' => 17,
      'exp' => 20496,
      'maxStamina' => 25,
      'leaderShip' => 34,
      'maxfriends' => 28,
      'bonus' => NULL,
    ),
    17 => 
    array (
      'level' => 18,
      'exp' => 23705,
      'maxStamina' => 26,
      'leaderShip' => 35,
      'maxfriends' => 29,
      'bonus' => NULL,
    ),
    18 => 
    array (
      'level' => 19,
      'exp' => 27188,
      'maxStamina' => 26,
      'leaderShip' => 36,
      'maxfriends' => 29,
      'bonus' => NULL,
    ),
    19 => 
    array (
      'level' => 20,
      'exp' => 30954,
      'maxStamina' => 26,
      'leaderShip' => 37,
      'maxfriends' => 30,
      'bonus' => NULL,
    ),
    20 => 
    array (
      'level' => 21,
      'exp' => 35008,
      'maxStamina' => 27,
      'leaderShip' => 37,
      'maxfriends' => 30,
      'bonus' => NULL,
    ),
    21 => 
    array (
      'level' => 22,
      'exp' => 39355,
      'maxStamina' => 27,
      'leaderShip' => 38,
      'maxfriends' => 31,
      'bonus' => NULL,
    ),
    22 => 
    array (
      'level' => 23,
      'exp' => 44002,
      'maxStamina' => 27,
      'leaderShip' => 39,
      'maxfriends' => 31,
      'bonus' => NULL,
    ),
    23 => 
    array (
      'level' => 24,
      'exp' => 48955,
      'maxStamina' => 28,
      'leaderShip' => 39,
      'maxfriends' => 31,
      'bonus' => NULL,
    ),
    24 => 
    array (
      'level' => 25,
      'exp' => 54218,
      'maxStamina' => 28,
      'leaderShip' => 40,
      'maxfriends' => 32,
      'bonus' => NULL,
    ),
    25 => 
    array (
      'level' => 26,
      'exp' => 59797,
      'maxStamina' => 28,
      'leaderShip' => 41,
      'maxfriends' => 32,
      'bonus' => NULL,
    ),
    26 => 
    array (
      'level' => 27,
      'exp' => 65698,
      'maxStamina' => 29,
      'leaderShip' => 41,
      'maxfriends' => 32,
      'bonus' => NULL,
    ),
    27 => 
    array (
      'level' => 28,
      'exp' => 71925,
      'maxStamina' => 29,
      'leaderShip' => 42,
      'maxfriends' => 33,
      'bonus' => NULL,
    ),
    28 => 
    array (
      'level' => 29,
      'exp' => 78483,
      'maxStamina' => 29,
      'leaderShip' => 43,
      'maxfriends' => 33,
      'bonus' => NULL,
    ),
    29 => 
    array (
      'level' => 30,
      'exp' => 85378,
      'maxStamina' => 34,
      'leaderShip' => 43,
      'maxfriends' => 33,
      'bonus' => NULL,
    ),
    30 => 
    array (
      'level' => 31,
      'exp' => 92613,
      'maxStamina' => 34,
      'leaderShip' => 44,
      'maxfriends' => 34,
      'bonus' => NULL,
    ),
    31 => 
    array (
      'level' => 32,
      'exp' => 100195,
      'maxStamina' => 34,
      'leaderShip' => 45,
      'maxfriends' => 34,
      'bonus' => NULL,
    ),
    32 => 
    array (
      'level' => 33,
      'exp' => 108126,
      'maxStamina' => 35,
      'leaderShip' => 45,
      'maxfriends' => 34,
      'bonus' => NULL,
    ),
    33 => 
    array (
      'level' => 34,
      'exp' => 116412,
      'maxStamina' => 35,
      'leaderShip' => 46,
      'maxfriends' => 35,
      'bonus' => NULL,
    ),
    34 => 
    array (
      'level' => 35,
      'exp' => 125057,
      'maxStamina' => 35,
      'leaderShip' => 47,
      'maxfriends' => 35,
      'bonus' => NULL,
    ),
    35 => 
    array (
      'level' => 36,
      'exp' => 134066,
      'maxStamina' => 36,
      'leaderShip' => 47,
      'maxfriends' => 35,
      'bonus' => NULL,
    ),
    36 => 
    array (
      'level' => 37,
      'exp' => 143442,
      'maxStamina' => 36,
      'leaderShip' => 48,
      'maxfriends' => 36,
      'bonus' => NULL,
    ),
    37 => 
    array (
      'level' => 38,
      'exp' => 153190,
      'maxStamina' => 36,
      'leaderShip' => 49,
      'maxfriends' => 36,
      'bonus' => NULL,
    ),
    38 => 
    array (
      'level' => 39,
      'exp' => 163314,
      'maxStamina' => 37,
      'leaderShip' => 49,
      'maxfriends' => 36,
      'bonus' => NULL,
    ),
    39 => 
    array (
      'level' => 40,
      'exp' => 173818,
      'maxStamina' => 37,
      'leaderShip' => 50,
      'maxfriends' => 37,
      'bonus' => NULL,
    ),
    40 => 
    array (
      'level' => 41,
      'exp' => 184706,
      'maxStamina' => 37,
      'leaderShip' => 51,
      'maxfriends' => 37,
      'bonus' => NULL,
    ),
    41 => 
    array (
      'level' => 42,
      'exp' => 195981,
      'maxStamina' => 38,
      'leaderShip' => 52,
      'maxfriends' => 37,
      'bonus' => NULL,
    ),
    42 => 
    array (
      'level' => 43,
      'exp' => 207648,
      'maxStamina' => 38,
      'leaderShip' => 53,
      'maxfriends' => 38,
      'bonus' => NULL,
    ),
    43 => 
    array (
      'level' => 44,
      'exp' => 219711,
      'maxStamina' => 38,
      'leaderShip' => 54,
      'maxfriends' => 38,
      'bonus' => NULL,
    ),
    44 => 
    array (
      'level' => 45,
      'exp' => 232172,
      'maxStamina' => 39,
      'leaderShip' => 55,
      'maxfriends' => 38,
      'bonus' => NULL,
    ),
    45 => 
    array (
      'level' => 46,
      'exp' => 245037,
      'maxStamina' => 39,
      'leaderShip' => 56,
      'maxfriends' => 39,
      'bonus' => NULL,
    ),
    46 => 
    array (
      'level' => 47,
      'exp' => 258308,
      'maxStamina' => 39,
      'leaderShip' => 56,
      'maxfriends' => 39,
      'bonus' => NULL,
    ),
    47 => 
    array (
      'level' => 48,
      'exp' => 271989,
      'maxStamina' => 40,
      'leaderShip' => 57,
      'maxfriends' => 39,
      'bonus' => NULL,
    ),
    48 => 
    array (
      'level' => 49,
      'exp' => 286084,
      'maxStamina' => 40,
      'leaderShip' => 58,
      'maxfriends' => 40,
      'bonus' => NULL,
    ),
    49 => 
    array (
      'level' => 50,
      'exp' => 300596,
      'maxStamina' => 40,
      'leaderShip' => 59,
      'maxfriends' => 40,
      'bonus' => NULL,
    ),
    50 => 
    array (
      'level' => 51,
      'exp' => 315529,
      'maxStamina' => 41,
      'leaderShip' => 60,
      'maxfriends' => 40,
      'bonus' => NULL,
    ),
    51 => 
    array (
      'level' => 52,
      'exp' => 330885,
      'maxStamina' => 41,
      'leaderShip' => 60,
      'maxfriends' => 41,
      'bonus' => NULL,
    ),
    52 => 
    array (
      'level' => 53,
      'exp' => 346669,
      'maxStamina' => 41,
      'leaderShip' => 61,
      'maxfriends' => 41,
      'bonus' => NULL,
    ),
    53 => 
    array (
      'level' => 54,
      'exp' => 362884,
      'maxStamina' => 42,
      'leaderShip' => 62,
      'maxfriends' => 41,
      'bonus' => NULL,
    ),
    54 => 
    array (
      'level' => 55,
      'exp' => 379532,
      'maxStamina' => 42,
      'leaderShip' => 63,
      'maxfriends' => 42,
      'bonus' => NULL,
    ),
    55 => 
    array (
      'level' => 56,
      'exp' => 396618,
      'maxStamina' => 42,
      'leaderShip' => 63,
      'maxfriends' => 42,
      'bonus' => NULL,
    ),
    56 => 
    array (
      'level' => 57,
      'exp' => 414145,
      'maxStamina' => 43,
      'leaderShip' => 64,
      'maxfriends' => 42,
      'bonus' => NULL,
    ),
    57 => 
    array (
      'level' => 58,
      'exp' => 432115,
      'maxStamina' => 43,
      'leaderShip' => 65,
      'maxfriends' => 43,
      'bonus' => NULL,
    ),
    58 => 
    array (
      'level' => 59,
      'exp' => 450531,
      'maxStamina' => 43,
      'leaderShip' => 66,
      'maxfriends' => 43,
      'bonus' => NULL,
    ),
    59 => 
    array (
      'level' => 60,
      'exp' => 469398,
      'maxStamina' => 48,
      'leaderShip' => 67,
      'maxfriends' => 43,
      'bonus' => NULL,
    ),
    60 => 
    array (
      'level' => 61,
      'exp' => 488718,
      'maxStamina' => 48,
      'leaderShip' => 68,
      'maxfriends' => 44,
      'bonus' => NULL,
    ),
    61 => 
    array (
      'level' => 62,
      'exp' => 508494,
      'maxStamina' => 48,
      'leaderShip' => 69,
      'maxfriends' => 44,
      'bonus' => NULL,
    ),
    62 => 
    array (
      'level' => 63,
      'exp' => 528729,
      'maxStamina' => 49,
      'leaderShip' => 70,
      'maxfriends' => 44,
      'bonus' => NULL,
    ),
    63 => 
    array (
      'level' => 64,
      'exp' => 549426,
      'maxStamina' => 49,
      'leaderShip' => 71,
      'maxfriends' => 45,
      'bonus' => NULL,
    ),
    64 => 
    array (
      'level' => 65,
      'exp' => 570588,
      'maxStamina' => 49,
      'leaderShip' => 72,
      'maxfriends' => 45,
      'bonus' => NULL,
    ),
    65 => 
    array (
      'level' => 66,
      'exp' => 592218,
      'maxStamina' => 50,
      'leaderShip' => 73,
      'maxfriends' => 45,
      'bonus' => NULL,
    ),
    66 => 
    array (
      'level' => 67,
      'exp' => 614319,
      'maxStamina' => 50,
      'leaderShip' => 74,
      'maxfriends' => 46,
      'bonus' => NULL,
    ),
    67 => 
    array (
      'level' => 68,
      'exp' => 636894,
      'maxStamina' => 50,
      'leaderShip' => 75,
      'maxfriends' => 46,
      'bonus' => NULL,
    ),
    68 => 
    array (
      'level' => 69,
      'exp' => 659946,
      'maxStamina' => 51,
      'leaderShip' => 76,
      'maxfriends' => 46,
      'bonus' => NULL,
    ),
    69 => 
    array (
      'level' => 70,
      'exp' => 683477,
      'maxStamina' => 51,
      'leaderShip' => 76,
      'maxfriends' => 47,
      'bonus' => NULL,
    ),
    70 => 
    array (
      'level' => 71,
      'exp' => 707490,
      'maxStamina' => 52,
      'leaderShip' => 77,
      'maxfriends' => 47,
      'bonus' => NULL,
    ),
    71 => 
    array (
      'level' => 72,
      'exp' => 731988,
      'maxStamina' => 52,
      'leaderShip' => 78,
      'maxfriends' => 47,
      'bonus' => NULL,
    ),
    72 => 
    array (
      'level' => 73,
      'exp' => 756975,
      'maxStamina' => 53,
      'leaderShip' => 79,
      'maxfriends' => 48,
      'bonus' => NULL,
    ),
    73 => 
    array (
      'level' => 74,
      'exp' => 782451,
      'maxStamina' => 53,
      'leaderShip' => 80,
      'maxfriends' => 48,
      'bonus' => NULL,
    ),
    74 => 
    array (
      'level' => 75,
      'exp' => 808422,
      'maxStamina' => 54,
      'leaderShip' => 81,
      'maxfriends' => 48,
      'bonus' => NULL,
    ),
    75 => 
    array (
      'level' => 76,
      'exp' => 834888,
      'maxStamina' => 54,
      'leaderShip' => 82,
      'maxfriends' => 49,
      'bonus' => NULL,
    ),
    76 => 
    array (
      'level' => 77,
      'exp' => 861852,
      'maxStamina' => 55,
      'leaderShip' => 83,
      'maxfriends' => 49,
      'bonus' => NULL,
    ),
    77 => 
    array (
      'level' => 78,
      'exp' => 889319,
      'maxStamina' => 55,
      'leaderShip' => 84,
      'maxfriends' => 49,
      'bonus' => NULL,
    ),
    78 => 
    array (
      'level' => 79,
      'exp' => 917289,
      'maxStamina' => 56,
      'leaderShip' => 85,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    79 => 
    array (
      'level' => 80,
      'exp' => 945765,
      'maxStamina' => 56,
      'leaderShip' => 85,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    80 => 
    array (
      'level' => 81,
      'exp' => 974751,
      'maxStamina' => 57,
      'leaderShip' => 86,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    81 => 
    array (
      'level' => 82,
      'exp' => 1004248,
      'maxStamina' => 57,
      'leaderShip' => 87,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    82 => 
    array (
      'level' => 83,
      'exp' => 1034260,
      'maxStamina' => 58,
      'leaderShip' => 88,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    83 => 
    array (
      'level' => 84,
      'exp' => 1064788,
      'maxStamina' => 58,
      'leaderShip' => 89,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    84 => 
    array (
      'level' => 85,
      'exp' => 1095836,
      'maxStamina' => 59,
      'leaderShip' => 90,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    85 => 
    array (
      'level' => 86,
      'exp' => 1127406,
      'maxStamina' => 59,
      'leaderShip' => 91,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    86 => 
    array (
      'level' => 87,
      'exp' => 1159499,
      'maxStamina' => 60,
      'leaderShip' => 92,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    87 => 
    array (
      'level' => 88,
      'exp' => 1192120,
      'maxStamina' => 60,
      'leaderShip' => 93,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    88 => 
    array (
      'level' => 89,
      'exp' => 1225269,
      'maxStamina' => 61,
      'leaderShip' => 94,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    89 => 
    array (
      'level' => 90,
      'exp' => 1258951,
      'maxStamina' => 61,
      'leaderShip' => 94,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    90 => 
    array (
      'level' => 91,
      'exp' => 1293166,
      'maxStamina' => 62,
      'leaderShip' => 95,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    91 => 
    array (
      'level' => 92,
      'exp' => 1327918,
      'maxStamina' => 62,
      'leaderShip' => 96,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    92 => 
    array (
      'level' => 93,
      'exp' => 1363208,
      'maxStamina' => 63,
      'leaderShip' => 97,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    93 => 
    array (
      'level' => 94,
      'exp' => 1399039,
      'maxStamina' => 63,
      'leaderShip' => 98,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    94 => 
    array (
      'level' => 95,
      'exp' => 1435414,
      'maxStamina' => 64,
      'leaderShip' => 99,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    95 => 
    array (
      'level' => 96,
      'exp' => 1472335,
      'maxStamina' => 64,
      'leaderShip' => 100,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    96 => 
    array (
      'level' => 97,
      'exp' => 1509804,
      'maxStamina' => 65,
      'leaderShip' => 101,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    97 => 
    array (
      'level' => 98,
      'exp' => 1547823,
      'maxStamina' => 65,
      'leaderShip' => 102,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    98 => 
    array (
      'level' => 99,
      'exp' => 1586395,
      'maxStamina' => 66,
      'leaderShip' => 103,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    99 => 
    array (
      'level' => 100,
      'exp' => 1625522,
      'maxStamina' => 66,
      'leaderShip' => 103,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    100 => 
    array (
      'level' => 101,
      'exp' => 1665206,
      'maxStamina' => 67,
      'leaderShip' => 104,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    101 => 
    array (
      'level' => 102,
      'exp' => 1705450,
      'maxStamina' => 67,
      'leaderShip' => 105,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    102 => 
    array (
      'level' => 103,
      'exp' => 1746255,
      'maxStamina' => 68,
      'leaderShip' => 106,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    103 => 
    array (
      'level' => 104,
      'exp' => 1787625,
      'maxStamina' => 68,
      'leaderShip' => 107,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    104 => 
    array (
      'level' => 105,
      'exp' => 1829560,
      'maxStamina' => 69,
      'leaderShip' => 108,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    105 => 
    array (
      'level' => 106,
      'exp' => 1872064,
      'maxStamina' => 69,
      'leaderShip' => 109,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    106 => 
    array (
      'level' => 107,
      'exp' => 1915136,
      'maxStamina' => 70,
      'leaderShip' => 110,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    107 => 
    array (
      'level' => 108,
      'exp' => 1958783,
      'maxStamina' => 70,
      'leaderShip' => 111,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    108 => 
    array (
      'level' => 109,
      'exp' => 2003005,
      'maxStamina' => 71,
      'leaderShip' => 112,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    109 => 
    array (
      'level' => 110,
      'exp' => 2047804,
      'maxStamina' => 71,
      'leaderShip' => 112,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    110 => 
    array (
      'level' => 111,
      'exp' => 2093181,
      'maxStamina' => 72,
      'leaderShip' => 113,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    111 => 
    array (
      'level' => 112,
      'exp' => 2139140,
      'maxStamina' => 72,
      'leaderShip' => 114,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    112 => 
    array (
      'level' => 113,
      'exp' => 2185683,
      'maxStamina' => 73,
      'leaderShip' => 115,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    113 => 
    array (
      'level' => 114,
      'exp' => 2232810,
      'maxStamina' => 73,
      'leaderShip' => 116,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    114 => 
    array (
      'level' => 115,
      'exp' => 2280526,
      'maxStamina' => 74,
      'leaderShip' => 117,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    115 => 
    array (
      'level' => 116,
      'exp' => 2328831,
      'maxStamina' => 74,
      'leaderShip' => 118,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    116 => 
    array (
      'level' => 117,
      'exp' => 2377727,
      'maxStamina' => 75,
      'leaderShip' => 119,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    117 => 
    array (
      'level' => 118,
      'exp' => 2427217,
      'maxStamina' => 75,
      'leaderShip' => 120,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    118 => 
    array (
      'level' => 119,
      'exp' => 2477303,
      'maxStamina' => 76,
      'leaderShip' => 121,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    119 => 
    array (
      'level' => 120,
      'exp' => 2527987,
      'maxStamina' => 76,
      'leaderShip' => 121,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    120 => 
    array (
      'level' => 121,
      'exp' => 2579270,
      'maxStamina' => 77,
      'leaderShip' => 122,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    121 => 
    array (
      'level' => 122,
      'exp' => 2631156,
      'maxStamina' => 77,
      'leaderShip' => 123,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    122 => 
    array (
      'level' => 123,
      'exp' => 2683645,
      'maxStamina' => 78,
      'leaderShip' => 124,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    123 => 
    array (
      'level' => 124,
      'exp' => 2736740,
      'maxStamina' => 78,
      'leaderShip' => 125,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    124 => 
    array (
      'level' => 125,
      'exp' => 2790442,
      'maxStamina' => 79,
      'leaderShip' => 126,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    125 => 
    array (
      'level' => 126,
      'exp' => 2844755,
      'maxStamina' => 79,
      'leaderShip' => 127,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    126 => 
    array (
      'level' => 127,
      'exp' => 2899679,
      'maxStamina' => 80,
      'leaderShip' => 128,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    127 => 
    array (
      'level' => 128,
      'exp' => 2955216,
      'maxStamina' => 80,
      'leaderShip' => 129,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    128 => 
    array (
      'level' => 129,
      'exp' => 3011370,
      'maxStamina' => 81,
      'leaderShip' => 130,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    129 => 
    array (
      'level' => 130,
      'exp' => 3068141,
      'maxStamina' => 81,
      'leaderShip' => 131,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    130 => 
    array (
      'level' => 131,
      'exp' => 3125531,
      'maxStamina' => 82,
      'leaderShip' => 133,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    131 => 
    array (
      'level' => 132,
      'exp' => 3183543,
      'maxStamina' => 82,
      'leaderShip' => 134,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    132 => 
    array (
      'level' => 133,
      'exp' => 3242178,
      'maxStamina' => 83,
      'leaderShip' => 135,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    133 => 
    array (
      'level' => 134,
      'exp' => 3301438,
      'maxStamina' => 83,
      'leaderShip' => 136,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    134 => 
    array (
      'level' => 135,
      'exp' => 3361325,
      'maxStamina' => 84,
      'leaderShip' => 137,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    135 => 
    array (
      'level' => 136,
      'exp' => 3421847,
      'maxStamina' => 84,
      'leaderShip' => 138,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    136 => 
    array (
      'level' => 137,
      'exp' => 3482994,
      'maxStamina' => 85,
      'leaderShip' => 139,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    137 => 
    array (
      'level' => 138,
      'exp' => 3544774,
      'maxStamina' => 85,
      'leaderShip' => 140,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    138 => 
    array (
      'level' => 139,
      'exp' => 3607188,
      'maxStamina' => 86,
      'leaderShip' => 141,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    139 => 
    array (
      'level' => 140,
      'exp' => 3670239,
      'maxStamina' => 86,
      'leaderShip' => 142,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    140 => 
    array (
      'level' => 141,
      'exp' => 3733928,
      'maxStamina' => 87,
      'leaderShip' => 144,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    141 => 
    array (
      'level' => 142,
      'exp' => 3798257,
      'maxStamina' => 87,
      'leaderShip' => 145,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    142 => 
    array (
      'level' => 143,
      'exp' => 3863228,
      'maxStamina' => 88,
      'leaderShip' => 146,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    143 => 
    array (
      'level' => 144,
      'exp' => 3928843,
      'maxStamina' => 88,
      'leaderShip' => 147,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    144 => 
    array (
      'level' => 145,
      'exp' => 3995103,
      'maxStamina' => 89,
      'leaderShip' => 148,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    145 => 
    array (
      'level' => 146,
      'exp' => 4062000,
      'maxStamina' => 89,
      'leaderShip' => 149,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    146 => 
    array (
      'level' => 147,
      'exp' => 4129557,
      'maxStamina' => 90,
      'leaderShip' => 150,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    147 => 
    array (
      'level' => 148,
      'exp' => 4197765,
      'maxStamina' => 90,
      'leaderShip' => 151,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    148 => 
    array (
      'level' => 149,
      'exp' => 4266632,
      'maxStamina' => 91,
      'leaderShip' => 152,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    149 => 
    array (
      'level' => 150,
      'exp' => 4336147,
      'maxStamina' => 91,
      'leaderShip' => 153,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    150 => 
    array (
      'level' => 151,
      'exp' => 4406318,
      'maxStamina' => 92,
      'leaderShip' => 155,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    151 => 
    array (
      'level' => 152,
      'exp' => 4477147,
      'maxStamina' => 92,
      'leaderShip' => 156,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    152 => 
    array (
      'level' => 153,
      'exp' => 4548635,
      'maxStamina' => 93,
      'leaderShip' => 157,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    153 => 
    array (
      'level' => 154,
      'exp' => 4620785,
      'maxStamina' => 93,
      'leaderShip' => 158,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    154 => 
    array (
      'level' => 155,
      'exp' => 4693599,
      'maxStamina' => 94,
      'leaderShip' => 159,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    155 => 
    array (
      'level' => 156,
      'exp' => 4767077,
      'maxStamina' => 94,
      'leaderShip' => 160,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    156 => 
    array (
      'level' => 157,
      'exp' => 4841222,
      'maxStamina' => 95,
      'leaderShip' => 161,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    157 => 
    array (
      'level' => 158,
      'exp' => 4916036,
      'maxStamina' => 95,
      'leaderShip' => 162,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    158 => 
    array (
      'level' => 159,
      'exp' => 4991832,
      'maxStamina' => 96,
      'leaderShip' => 163,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    159 => 
    array (
      'level' => 160,
      'exp' => 5068658,
      'maxStamina' => 96,
      'leaderShip' => 164,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    160 => 
    array (
      'level' => 161,
      'exp' => 5146160,
      'maxStamina' => 97,
      'leaderShip' => 166,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    161 => 
    array (
      'level' => 162,
      'exp' => 5224339,
      'maxStamina' => 97,
      'leaderShip' => 167,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    162 => 
    array (
      'level' => 163,
      'exp' => 5303197,
      'maxStamina' => 98,
      'leaderShip' => 168,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    163 => 
    array (
      'level' => 164,
      'exp' => 5382736,
      'maxStamina' => 98,
      'leaderShip' => 169,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    164 => 
    array (
      'level' => 165,
      'exp' => 5462957,
      'maxStamina' => 99,
      'leaderShip' => 170,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    165 => 
    array (
      'level' => 166,
      'exp' => 5543862,
      'maxStamina' => 99,
      'leaderShip' => 171,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    166 => 
    array (
      'level' => 167,
      'exp' => 5625453,
      'maxStamina' => 100,
      'leaderShip' => 172,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    167 => 
    array (
      'level' => 168,
      'exp' => 5708451,
      'maxStamina' => 100,
      'leaderShip' => 173,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    168 => 
    array (
      'level' => 169,
      'exp' => 5792177,
      'maxStamina' => 101,
      'leaderShip' => 174,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    169 => 
    array (
      'level' => 170,
      'exp' => 5876635,
      'maxStamina' => 101,
      'leaderShip' => 175,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    170 => 
    array (
      'level' => 171,
      'exp' => 5961827,
      'maxStamina' => 102,
      'leaderShip' => 177,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    171 => 
    array (
      'level' => 172,
      'exp' => 6047756,
      'maxStamina' => 102,
      'leaderShip' => 178,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    172 => 
    array (
      'level' => 173,
      'exp' => 6134425,
      'maxStamina' => 103,
      'leaderShip' => 179,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    173 => 
    array (
      'level' => 174,
      'exp' => 6221836,
      'maxStamina' => 103,
      'leaderShip' => 180,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    174 => 
    array (
      'level' => 175,
      'exp' => 6309992,
      'maxStamina' => 104,
      'leaderShip' => 181,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    175 => 
    array (
      'level' => 176,
      'exp' => 6398897,
      'maxStamina' => 104,
      'leaderShip' => 182,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    176 => 
    array (
      'level' => 177,
      'exp' => 6488552,
      'maxStamina' => 105,
      'leaderShip' => 183,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    177 => 
    array (
      'level' => 178,
      'exp' => 6578960,
      'maxStamina' => 105,
      'leaderShip' => 184,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    178 => 
    array (
      'level' => 179,
      'exp' => 6670125,
      'maxStamina' => 106,
      'leaderShip' => 185,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    179 => 
    array (
      'level' => 180,
      'exp' => 6762049,
      'maxStamina' => 106,
      'leaderShip' => 186,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    180 => 
    array (
      'level' => 181,
      'exp' => 6854735,
      'maxStamina' => 107,
      'leaderShip' => 188,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    181 => 
    array (
      'level' => 182,
      'exp' => 6948186,
      'maxStamina' => 107,
      'leaderShip' => 189,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    182 => 
    array (
      'level' => 183,
      'exp' => 7042404,
      'maxStamina' => 108,
      'leaderShip' => 190,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    183 => 
    array (
      'level' => 184,
      'exp' => 7137392,
      'maxStamina' => 108,
      'leaderShip' => 191,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    184 => 
    array (
      'level' => 185,
      'exp' => 7233154,
      'maxStamina' => 109,
      'leaderShip' => 192,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    185 => 
    array (
      'level' => 186,
      'exp' => 7329691,
      'maxStamina' => 109,
      'leaderShip' => 193,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    186 => 
    array (
      'level' => 187,
      'exp' => 7427006,
      'maxStamina' => 110,
      'leaderShip' => 194,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    187 => 
    array (
      'level' => 188,
      'exp' => 7525103,
      'maxStamina' => 110,
      'leaderShip' => 195,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    188 => 
    array (
      'level' => 189,
      'exp' => 7623984,
      'maxStamina' => 111,
      'leaderShip' => 196,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    189 => 
    array (
      'level' => 190,
      'exp' => 7723652,
      'maxStamina' => 111,
      'leaderShip' => 197,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    190 => 
    array (
      'level' => 191,
      'exp' => 7824109,
      'maxStamina' => 112,
      'leaderShip' => 199,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    191 => 
    array (
      'level' => 192,
      'exp' => 7925359,
      'maxStamina' => 112,
      'leaderShip' => 200,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    192 => 
    array (
      'level' => 193,
      'exp' => 8027404,
      'maxStamina' => 113,
      'leaderShip' => 201,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    193 => 
    array (
      'level' => 194,
      'exp' => 8130247,
      'maxStamina' => 113,
      'leaderShip' => 202,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    194 => 
    array (
      'level' => 195,
      'exp' => 8233891,
      'maxStamina' => 114,
      'leaderShip' => 203,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    195 => 
    array (
      'level' => 196,
      'exp' => 8338338,
      'maxStamina' => 114,
      'leaderShip' => 204,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    196 => 
    array (
      'level' => 197,
      'exp' => 8443591,
      'maxStamina' => 115,
      'leaderShip' => 205,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    197 => 
    array (
      'level' => 198,
      'exp' => 8549654,
      'maxStamina' => 115,
      'leaderShip' => 206,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    198 => 
    array (
      'level' => 199,
      'exp' => 8656528,
      'maxStamina' => 116,
      'leaderShip' => 207,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    199 => 
    array (
      'level' => 200,
      'exp' => 8764217,
      'maxStamina' => 116,
      'leaderShip' => 208,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    200 => 
    array (
      'level' => 201,
      'exp' => 8872724,
      'maxStamina' => 117,
      'leaderShip' => 210,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    201 => 
    array (
      'level' => 202,
      'exp' => 8982050,
      'maxStamina' => 117,
      'leaderShip' => 211,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    202 => 
    array (
      'level' => 203,
      'exp' => 9092200,
      'maxStamina' => 118,
      'leaderShip' => 212,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    203 => 
    array (
      'level' => 204,
      'exp' => 9203175,
      'maxStamina' => 118,
      'leaderShip' => 213,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    204 => 
    array (
      'level' => 205,
      'exp' => 9314979,
      'maxStamina' => 119,
      'leaderShip' => 214,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    205 => 
    array (
      'level' => 206,
      'exp' => 9427614,
      'maxStamina' => 119,
      'leaderShip' => 215,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    206 => 
    array (
      'level' => 207,
      'exp' => 9541083,
      'maxStamina' => 120,
      'leaderShip' => 216,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    207 => 
    array (
      'level' => 208,
      'exp' => 9655389,
      'maxStamina' => 120,
      'leaderShip' => 217,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    208 => 
    array (
      'level' => 209,
      'exp' => 9770534,
      'maxStamina' => 121,
      'leaderShip' => 218,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    209 => 
    array (
      'level' => 210,
      'exp' => 9886522,
      'maxStamina' => 121,
      'leaderShip' => 219,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    210 => 
    array (
      'level' => 211,
      'exp' => 10003355,
      'maxStamina' => 122,
      'leaderShip' => 221,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    211 => 
    array (
      'level' => 212,
      'exp' => 10121036,
      'maxStamina' => 122,
      'leaderShip' => 222,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    212 => 
    array (
      'level' => 213,
      'exp' => 10239567,
      'maxStamina' => 123,
      'leaderShip' => 223,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    213 => 
    array (
      'level' => 214,
      'exp' => 10358953,
      'maxStamina' => 123,
      'leaderShip' => 224,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    214 => 
    array (
      'level' => 215,
      'exp' => 10479194,
      'maxStamina' => 124,
      'leaderShip' => 225,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    215 => 
    array (
      'level' => 216,
      'exp' => 10600294,
      'maxStamina' => 124,
      'leaderShip' => 226,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    216 => 
    array (
      'level' => 217,
      'exp' => 10722256,
      'maxStamina' => 125,
      'leaderShip' => 227,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    217 => 
    array (
      'level' => 218,
      'exp' => 10845083,
      'maxStamina' => 125,
      'leaderShip' => 228,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    218 => 
    array (
      'level' => 219,
      'exp' => 10968777,
      'maxStamina' => 126,
      'leaderShip' => 229,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    219 => 
    array (
      'level' => 220,
      'exp' => 11093341,
      'maxStamina' => 126,
      'leaderShip' => 230,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    220 => 
    array (
      'level' => 221,
      'exp' => 11218778,
      'maxStamina' => 127,
      'leaderShip' => 232,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    221 => 
    array (
      'level' => 222,
      'exp' => 11345091,
      'maxStamina' => 127,
      'leaderShip' => 233,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    222 => 
    array (
      'level' => 223,
      'exp' => 11472282,
      'maxStamina' => 128,
      'leaderShip' => 234,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    223 => 
    array (
      'level' => 224,
      'exp' => 11600355,
      'maxStamina' => 128,
      'leaderShip' => 235,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    224 => 
    array (
      'level' => 225,
      'exp' => 11729311,
      'maxStamina' => 129,
      'leaderShip' => 236,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    225 => 
    array (
      'level' => 226,
      'exp' => 11859155,
      'maxStamina' => 129,
      'leaderShip' => 237,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    226 => 
    array (
      'level' => 227,
      'exp' => 11989887,
      'maxStamina' => 130,
      'leaderShip' => 238,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    227 => 
    array (
      'level' => 228,
      'exp' => 12121513,
      'maxStamina' => 130,
      'leaderShip' => 239,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    228 => 
    array (
      'level' => 229,
      'exp' => 12254033,
      'maxStamina' => 131,
      'leaderShip' => 240,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    229 => 
    array (
      'level' => 230,
      'exp' => 12387451,
      'maxStamina' => 131,
      'leaderShip' => 241,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    230 => 
    array (
      'level' => 231,
      'exp' => 12521770,
      'maxStamina' => 132,
      'leaderShip' => 243,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    231 => 
    array (
      'level' => 232,
      'exp' => 12656992,
      'maxStamina' => 132,
      'leaderShip' => 244,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    232 => 
    array (
      'level' => 233,
      'exp' => 12793120,
      'maxStamina' => 133,
      'leaderShip' => 245,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    233 => 
    array (
      'level' => 234,
      'exp' => 12930158,
      'maxStamina' => 133,
      'leaderShip' => 246,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    234 => 
    array (
      'level' => 235,
      'exp' => 13068107,
      'maxStamina' => 134,
      'leaderShip' => 247,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    235 => 
    array (
      'level' => 236,
      'exp' => 13206971,
      'maxStamina' => 134,
      'leaderShip' => 248,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    236 => 
    array (
      'level' => 237,
      'exp' => 13346752,
      'maxStamina' => 135,
      'leaderShip' => 249,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    237 => 
    array (
      'level' => 238,
      'exp' => 13487453,
      'maxStamina' => 135,
      'leaderShip' => 250,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    238 => 
    array (
      'level' => 239,
      'exp' => 13629077,
      'maxStamina' => 136,
      'leaderShip' => 251,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
    239 => 
    array (
      'level' => 240,
      'exp' => 13771626,
      'maxStamina' => 136,
      'leaderShip' => 252,
      'maxfriends' => 50,
      'bonus' => NULL,
    ),
  ),
);
?>