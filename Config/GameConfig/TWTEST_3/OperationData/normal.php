<?php
return array (
  1 => 
  array (
    'name' => '初涉乱世',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '开始征途',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '1,2,3,4,5,6',
        'boss' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
        ),
        'package' => 1,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '第一次挑战',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '1,2,3,4,5,6',
        'boss' => 
        array (
          0 => '8',
          1 => '9',
          2 => '10',
        ),
        'package' => 2,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '初踏戎生',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '1,2,3,4,5,6',
        'boss' => 
        array (
          0 => '11',
        ),
        'package' => 3,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  2 => 
  array (
    'name' => '红土荒野',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '荒野红巾军',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '12,13,14,15,16',
        'boss' => 
        array (
          0 => '17',
        ),
        'package' => 4,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '华雄之死',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '12,13,14,15,16',
        'boss' => 
        array (
          0 => '18',
        ),
        'package' => 5,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '乱世之奸雄',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '12,13,14,15,16',
        'boss' => 
        array (
          0 => '19',
        ),
        'package' => 6,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  3 => 
  array (
    'name' => '绿灵湖泊',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '1',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '湖畔绿巾军',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '20,21,22,23,24',
        'boss' => 
        array (
          0 => '25',
        ),
        'package' => 7,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '琅邪良将',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '20,21,22,23,24',
        'boss' => 
        array (
          0 => '26',
        ),
        'package' => 8,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '碧眼仲谋',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 4,
        ),
        'monsters' => '20,21,22,23,24',
        'boss' => 
        array (
          0 => '27',
        ),
        'package' => 9,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  4 => 
  array (
    'name' => '迷雾丛林',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '3',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '落日黄巾军',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '28,29,30,31,32',
        'boss' => 
        array (
          0 => '33',
        ),
        'package' => 10,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '得高而胜',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '28,29,30,31,32',
        'boss' => 
        array (
          0 => '34',
        ),
        'package' => 11,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '玄德的隐忍',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
        ),
        'monsters' => '28,29,30,31,32',
        'boss' => 
        array (
          0 => '35',
        ),
        'package' => 12,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  5 => 
  array (
    'name' => '落日峡谷',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '4',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '丛林紫巾军',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '36,37,38,39,40,41',
        'boss' => 
        array (
          0 => '42',
        ),
        'package' => 13,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '长安之乱',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '36,37,38,39,40,41',
        'boss' => 
        array (
          0 => '43',
        ),
        'package' => 14,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '太师之风',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '36,37,38,39,40,41',
        'boss' => 
        array (
          0 => '44',
        ),
        'package' => 15,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  6 => 
  array (
    'name' => '西荒流域',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '5',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '汉中之祸',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '45,46,47,48,49,50',
        'boss' => 
        array (
          0 => '51',
        ),
        'package' => 16,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '天命征讨',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '45,46,47,48,49,50',
        'boss' => 
        array (
          0 => '52',
        ),
        'package' => 17,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '校尉盟主',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 4,
        ),
        'monsters' => '45,46,47,48,49,50',
        'boss' => 
        array (
          0 => '53',
        ),
        'package' => 18,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  7 => 
  array (
    'name' => '试炼战场',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '鏖战汉中',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '54,55,56,57,58,59,60,61,62,63,64,65,66,67,68',
        'boss' => 
        array (
          0 => '69',
          1 => '70',
          2 => '71',
        ),
        'package' => 19,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '虎狼之师',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '54,55,56,57,58,59,60,61,62,63,64,65,66,67,68',
        'boss' => 
        array (
          0 => '72',
          1 => '73',
        ),
        'package' => 20,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '江表虎臣',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '54,55,56,57,58,59,60,61,62,63,64,65,66,67,68',
        'boss' => 
        array (
          0 => '74',
        ),
        'package' => 21,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  8 => 
  array (
    'name' => '焱火丘陵',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '随虎危机',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '75,76,77,78,79,80,81,82',
        'boss' => 
        array (
          0 => '83',
          1 => '83',
          2 => '83',
        ),
        'package' => 22,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '鬼计退敌',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '75,76,77,78,79,80,81,82',
        'boss' => 
        array (
          0 => '84',
          1 => '84',
          2 => '84',
        ),
        'package' => 23,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '夹尾潜逃',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '75,76,77,78,79,80,81,82',
        'boss' => 
        array (
          0 => '85',
        ),
        'package' => 24,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '姊妹齐力',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '75,76,77,78,79,80,81,82',
        'boss' => 
        array (
          0 => '86',
          1 => '86',
          2 => '86',
        ),
        'package' => 25,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '力挽狂澜',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '75,76,77,78,79,80,81,82',
        'boss' => 
        array (
          0 => '87',
        ),
        'package' => 26,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  9 => 
  array (
    'name' => '死亡沼泽',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '1',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '乱世争霸',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '88,89,90,91,92,93,94,95',
        'boss' => 
        array (
          0 => '96',
          1 => '96',
          2 => '96',
        ),
        'package' => 27,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '矛头向北',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '88,89,90,91,92,93,94,95',
        'boss' => 
        array (
          0 => '97',
          1 => '97',
          2 => '97',
        ),
        'package' => 28,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '千里寻将',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '88,89,90,91,92,93,94,95',
        'boss' => 
        array (
          0 => '98',
        ),
        'package' => 29,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '巾帼英姿',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '88,89,90,91,92,93,94,95',
        'boss' => 
        array (
          0 => '99',
          1 => '99',
          2 => '99',
        ),
        'package' => 30,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '与狼共舞',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '88,89,90,91,92,93,94,95',
        'boss' => 
        array (
          0 => '100',
        ),
        'package' => 31,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  10 => 
  array (
    'name' => '不归密林',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '3',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '与虎谋皮',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '101,102,103,104,105,106,107,108',
        'boss' => 
        array (
          0 => '109',
          1 => '109',
          2 => '109',
        ),
        'package' => 32,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '刚柔并收',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '101,102,103,104,105,106,107,108',
        'boss' => 
        array (
          0 => '110',
          1 => '110',
          2 => '110',
        ),
        'package' => 33,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '万军开道',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '101,102,103,104,105,106,107,108',
        'boss' => 
        array (
          0 => '111',
        ),
        'package' => 34,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '红颜使计',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '101,102,103,104,105,106,107,108',
        'boss' => 
        array (
          0 => '112',
          1 => '112',
          2 => '112',
        ),
        'package' => 35,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '聚力一击',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '101,102,103,104,105,106,107,108',
        'boss' => 
        array (
          0 => '113',
        ),
        'package' => 36,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  11 => 
  array (
    'name' => '封喉沙漠',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '4',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '智夺邺城',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '114,115,116,117,118,119,120',
        'boss' => 
        array (
          0 => '121',
          1 => '121',
          2 => '121',
        ),
        'package' => 37,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '光之魂的幽叹',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '114,115,116,117,118,119,120',
        'boss' => 
        array (
          0 => '122',
          1 => '122',
        ),
        'package' => 38,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '燃烧的能臣',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '114,115,116,117,118,119,120',
        'boss' => 
        array (
          0 => '123',
        ),
        'package' => 39,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '洛神倾世',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '114,115,116,117,118,119,120',
        'boss' => 
        array (
          0 => '124',
          1 => '124',
          2 => '124',
        ),
        'package' => 40,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '花招算尽',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '114,115,116,117,118,119,120',
        'boss' => 
        array (
          0 => '125',
        ),
        'package' => 41,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  12 => 
  array (
    'name' => '紫玉渡口',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '5',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '太史犯险',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '126,127,128,129,130,131,132',
        'boss' => 
        array (
          0 => '133',
          1 => '133',
          2 => '133',
        ),
        'package' => 42,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '回马奔袭',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '126,127,128,129,130,131,132',
        'boss' => 
        array (
          0 => '134',
          1 => '134',
        ),
        'package' => 43,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '渔蚌相争',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '126,127,128,129,130,131,132',
        'boss' => 
        array (
          0 => '135',
        ),
        'package' => 44,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '智慧女神',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '126,127,128,129,130,131,132',
        'boss' => 
        array (
          0 => '136',
          1 => '136',
          2 => '136',
        ),
        'package' => 45,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '城外演兵',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
        ),
        'monsters' => '126,127,128,129,130,131,132',
        'boss' => 
        array (
          0 => '137',
        ),
        'package' => 46,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  13 => 
  array (
    'name' => '魂将校场',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '襄阳城外',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
        ),
        'monsters' => '138,139,140,141,142,143,144,145,146,147,148,149,150,151,152',
        'boss' => 
        array (
          0 => '153',
          1 => '154',
          2 => '155',
        ),
        'package' => 47,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '悍将风格',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
        ),
        'monsters' => '138,139,140,141,142,143,144,145,146,147,148,149,150,151,152',
        'boss' => 
        array (
          0 => '156',
          1 => '157',
          2 => '158',
        ),
        'package' => 48,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '三公合谋',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
        ),
        'monsters' => '138,139,140,141,142,143,144,145,146,147,148,149,150,151,152',
        'boss' => 
        array (
          0 => '159',
          1 => '160',
          2 => '161',
        ),
        'package' => 49,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '偏将军发力',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '138,139,140,141,142,143,144,145,146,147,148,149,150,151,152',
        'boss' => 
        array (
          0 => '162',
          1 => '162',
        ),
        'package' => 50,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '无人可挡',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '138,139,140,141,142,143,144,145,146,147,148,149,150,151,152',
        'boss' => 
        array (
          0 => '163',
        ),
        'package' => 51,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  14 => 
  array (
    'name' => '恶来斗魂',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '魂将的控制',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '164,165,166,167,168,169',
        'boss' => 
        array (
          0 => '170',
        ),
        'package' => 52,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '枭姬柔情',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '164,165,166,167,168,169',
        'boss' => 
        array (
          0 => '171',
        ),
        'package' => 53,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '流星的尾炙',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '164,165,166,167,168,169',
        'boss' => 
        array (
          0 => '172',
          1 => '172',
        ),
        'package' => 54,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '魂之哀伤',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '164,165,166,167,168,169',
        'boss' => 
        array (
          0 => '173',
          1 => '174',
        ),
        'package' => 55,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '天人的宽恕',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '164,165,166,167,168,169',
        'boss' => 
        array (
          0 => '175',
        ),
        'package' => 56,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  15 => 
  array (
    'name' => '冰封城墙',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '1',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '雪原饿狼',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 4,
        ),
        'monsters' => '176,177,178,179,180,181',
        'boss' => 
        array (
          0 => '182',
        ),
        'package' => 57,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '择木而栖',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '176,177,178,179,180,181',
        'boss' => 
        array (
          0 => '183',
        ),
        'package' => 58,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '苍陨烛龙',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '176,177,178,179,180,181',
        'boss' => 
        array (
          0 => '184',
          1 => '184',
        ),
        'package' => 59,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '魂之衷肠',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '176,177,178,179,180,181',
        'boss' => 
        array (
          0 => '185',
          1 => '186',
        ),
        'package' => 60,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '末路之雄',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '176,177,178,179,180,181',
        'boss' => 
        array (
          0 => '187',
        ),
        'package' => 61,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  16 => 
  array (
    'name' => '翠绿之原',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '3',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '群战之狼',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 4,
        ),
        'monsters' => '188,189,190,191,192,193',
        'boss' => 
        array (
          0 => '194',
        ),
        'package' => 62,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '国色天香',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 4,
        ),
        'monsters' => '188,189,190,191,192,193',
        'boss' => 
        array (
          0 => '195',
        ),
        'package' => 63,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '莫邪螣蛇',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 4,
        ),
        'monsters' => '188,189,190,191,192,193',
        'boss' => 
        array (
          0 => '196',
          1 => '196',
        ),
        'package' => 64,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '魂之迷茫',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 4,
        ),
        'monsters' => '188,189,190,191,192,193',
        'boss' => 
        array (
          0 => '197',
          1 => '198',
        ),
        'package' => 65,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '无敌天下',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 4,
        ),
        'monsters' => '188,189,190,191,192,193',
        'boss' => 
        array (
          0 => '199',
        ),
        'package' => 66,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  17 => 
  array (
    'name' => '黄土堡垒',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '4',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '最强防御',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '200,201,202,203,204',
        'boss' => 
        array (
          0 => '205',
        ),
        'package' => 67,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '轻云蔽月',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '200,201,202,203,204',
        'boss' => 
        array (
          0 => '206',
        ),
        'package' => 68,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '镇邪凤凰',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '200,201,202,203,204',
        'boss' => 
        array (
          0 => '207',
          1 => '207',
        ),
        'package' => 69,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '魂之刚强',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '200,201,202,203,204',
        'boss' => 
        array (
          0 => '208',
          1 => '209',
        ),
        'package' => 70,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '权倾朝野的名门',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 2,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '200,201,202,203,204',
        'boss' => 
        array (
          0 => '210',
        ),
        'package' => 71,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  18 => 
  array (
    'name' => '幽暗冥殿',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '5',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '幽魂游荡',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '211,212,213,214,215',
        'boss' => 
        array (
          0 => '216',
        ),
        'package' => 72,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '千古才女',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '211,212,213,214,215',
        'boss' => 
        array (
          0 => '217',
        ),
        'package' => 73,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '仲古麒麟',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '211,212,213,214,215',
        'boss' => 
        array (
          0 => '218',
          1 => '218',
        ),
        'package' => 74,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '魂之不屈',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 2,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '211,212,213,214,215',
        'boss' => 
        array (
          0 => '219',
          1 => '220',
        ),
        'package' => 75,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '魔将肆掠',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '211,212,213,214,215',
        'boss' => 
        array (
          0 => '221',
        ),
        'package' => 76,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  19 => 
  array (
    'name' => '风岚星域',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '魂将战场',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237',
        'boss' => 
        array (
          0 => '238',
          1 => '239',
        ),
        'package' => 77,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '女将成魔',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 2,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237',
        'boss' => 
        array (
          0 => '240',
          1 => '241',
        ),
        'package' => 78,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '圣兽鲲鹏',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237',
        'boss' => 
        array (
          0 => '242',
          1 => '243',
          2 => '244',
        ),
        'package' => 79,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '魔魂将出',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237',
        'boss' => 
        array (
          0 => '245',
          1 => '246',
          2 => '247',
        ),
        'package' => 80,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '狂魔魂将',
        'stamina' => 11,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
          7 => 4,
          8 => 3,
          9 => 3,
        ),
        'monsters' => '222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237',
        'boss' => 
        array (
          0 => '248',
          1 => '249',
        ),
        'package' => 81,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  20 => 
  array (
    'name' => '烈焰王城',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '龙吟虎啸',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '424,425,426,427,428,429,430,431',
        'boss' => 
        array (
          0 => '432',
          1 => '433',
        ),
        'package' => 82,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '铁壁阻路',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '424,425,426,427,428,429,430,431',
        'boss' => 
        array (
          0 => '434',
        ),
        'package' => 83,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '猛将叫阵',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '424,425,426,427,428,429,430,431',
        'boss' => 
        array (
          0 => '435',
          1 => '436',
        ),
        'package' => 84,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '才貌兼具',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '424,425,426,427,428,429,430,431',
        'boss' => 
        array (
          0 => '437',
          1 => '438',
        ),
        'package' => 85,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '燃烧圣兽',
        'stamina' => 11,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 3,
          4 => 3,
          5 => 4,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 3,
        ),
        'monsters' => '424,425,426,427,428,429,430,431',
        'boss' => 
        array (
          0 => '439',
          1 => '440',
          2 => '441',
        ),
        'package' => 86,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  21 => 
  array (
    'name' => '狂海无量',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '1',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '反中奸计',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449',
        'boss' => 
        array (
          0 => '450',
          1 => '451',
        ),
        'package' => 87,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '义情久深',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449',
        'boss' => 
        array (
          0 => '452',
        ),
        'package' => 88,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '遗命凶险',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449',
        'boss' => 
        array (
          0 => '453',
          1 => '454',
        ),
        'package' => 89,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '美人英雄',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449',
        'boss' => 
        array (
          0 => '455',
          1 => '456',
        ),
        'package' => 90,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '碧水圣兽',
        'stamina' => 11,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449',
        'boss' => 
        array (
          0 => '457',
          1 => '458',
          2 => '459',
        ),
        'package' => 91,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  22 => 
  array (
    'name' => '巨木之林',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '3',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '四灵圣兽',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449,460,461',
        'boss' => 
        array (
          0 => '462',
          1 => '463',
          2 => '464',
        ),
        'package' => 92,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '搏战登顶',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449,460,461',
        'boss' => 
        array (
          0 => '465',
        ),
        'package' => 93,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '决战冀州',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449,460,461',
        'boss' => 
        array (
          0 => '466',
          1 => '467',
          2 => '468',
        ),
        'package' => 94,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '翠羽嫣然',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449,460,461',
        'boss' => 
        array (
          0 => '469',
          1 => '470',
        ),
        'package' => 95,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '古之圣兽',
        'stamina' => 11,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449,460,461',
        'boss' => 
        array (
          0 => '471',
          1 => '472',
          2 => '473',
          3 => '474',
        ),
        'package' => 96,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  23 => 
  array (
    'name' => '填百将图',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '火烧原野',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '475,476,477,478,479,480,481,482,483',
        'boss' => 
        array (
          0 => '484',
          1 => '485',
          2 => '486',
        ),
        'package' => 97,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '冰封万里',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 2,
        ),
        'monsters' => '475,476,477,478,479,480,481,482,483',
        'boss' => 
        array (
          0 => '487',
          1 => '492',
        ),
        'package' => 98,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '草木回春',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 2,
        ),
        'monsters' => '475,476,477,478,479,480,481,482,483',
        'boss' => 
        array (
          0 => '488',
          1 => '489',
        ),
        'package' => 99,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '满界光芒',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 2,
        ),
        'monsters' => '475,476,477,478,479,480,481,482,483',
        'boss' => 
        array (
          0 => '490',
          1 => '491',
        ),
        'package' => 100,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '混沌神力',
        'stamina' => 12,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 2,
          7 => 2,
          8 => 3,
          9 => 3,
        ),
        'monsters' => '475,476,477,478,479,480,481,482,483',
        'boss' => 
        array (
          0 => '493',
        ),
        'package' => 101,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  24 => 
  array (
    'name' => '烽火之境',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '锦绣繁星',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1100,1101,1102,1103,1104,1105,1106',
        'boss' => 
        array (
          0 => '1107',
          1 => '1108',
          2 => '1109',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '蜀中良臣',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1110,1111,1112,1113,111,1115,1116',
        'boss' => 
        array (
          0 => '1117',
          1 => '1118',
          2 => '1119',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '铜墙铁蹄',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1127,
            1 => 1128,
            2 => 1129,
          ),
        ),
        'monsters' => '1120,1121,1122,1123,1124,1125,1126',
        'boss' => 
        array (
          0 => '1130',
          1 => '1131',
          2 => '1132',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '山河壮丽',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1140,
            1 => 1141,
            2 => 1142,
          ),
        ),
        'monsters' => '1133,1134,1135,1136,1137,1138,1139',
        'boss' => 
        array (
          0 => '1143',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '长蛇封豕',
        'stamina' => 12,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1151,
            1 => 1152,
            2 => 1153,
          ),
        ),
        'monsters' => '1144,1145,1146,1147,1148,1149,1150',
        'boss' => 
        array (
          0 => '1154',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  25 => 
  array (
    'name' => '离水之境',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '1',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '离殇之役',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1155,1156,1157,1158,1159,1160,1161',
        'boss' => 
        array (
          0 => '1162',
          1 => '1163',
          2 => '1164',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '趋名附利',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1155,1156,1157,1158,1159,1160,1161',
        'boss' => 
        array (
          0 => '1172',
          1 => '1173',
          2 => '1174',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '垓下解围',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1182,
            1 => 1183,
            2 => 1184,
          ),
        ),
        'monsters' => '1175,1176,1177,1178,1179,1180,1181',
        'boss' => 
        array (
          0 => '1185',
          1 => '1186',
          2 => '1187',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '聪敏琉璃',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1195,
            1 => 1196,
            2 => 1197,
          ),
        ),
        'monsters' => '1188,1189,1190,1191,1192,1193,1194',
        'boss' => 
        array (
          0 => '1198',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '落凤之箭',
        'stamina' => 12,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1206,
            1 => 1207,
            2 => 1208,
          ),
        ),
        'monsters' => '1199,1200,1201,1202,1203,1204,1205',
        'boss' => 
        array (
          0 => '1209',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  26 => 
  array (
    'name' => '幻木之境',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '3',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => ' 浴血护主',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1210,1211,1212,1213,1214,1215,1216',
        'boss' => 
        array (
          0 => '1217',
          1 => '1218',
          2 => '1219',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '唇枪舌剑',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1220,1221,1222,1223,1224,1225,1226',
        'boss' => 
        array (
          0 => '1227',
          1 => '1228',
          2 => '1229',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '吴魏相争',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1237,
            1 => 1238,
            2 => 1239,
          ),
        ),
        'monsters' => '1230,1231,1232,1233,1234,1235,1236',
        'boss' => 
        array (
          0 => '1240',
          1 => '1241',
          2 => '1242',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '母仪天下',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1250,
            1 => 1251,
            2 => 1252,
          ),
        ),
        'monsters' => '1243,1244,1245,1246,1247,1248,1249',
        'boss' => 
        array (
          0 => '1253',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '龙象天下',
        'stamina' => 12,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1261,
            1 => 1262,
            2 => 1263,
          ),
        ),
        'monsters' => '1254,1255,1256,1257,1258,1259,1260',
        'boss' => 
        array (
          0 => '1264',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  27 => 
  array (
    'name' => '神光之境',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '4',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '虎胆雄魄',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1265,1266,1267,1268,1269,1270',
        'boss' => 
        array (
          0 => '1271',
          1 => '1272',
          2 => '1273',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '蜀中谋士',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1274,1275,1276,1277,1278,1279',
        'boss' => 
        array (
          0 => '1280',
          1 => '1281',
          2 => '1282',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '万里追杀',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1289,
            1 => 1290,
            2 => 1291,
          ),
        ),
        'monsters' => '1283,1284,1285,1286,1287,1288',
        'boss' => 
        array (
          0 => '1292',
          1 => '1293',
          2 => '1294',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '巾帼无双',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1301,
            1 => 1302,
            2 => 1303,
          ),
        ),
        'monsters' => '1295,1296,1297,1298,1299,1300',
        'boss' => 
        array (
          0 => '1304',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '鞭麟笞凤',
        'stamina' => 14,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1311,
            1 => 1312,
            2 => 1313,
          ),
        ),
        'monsters' => '1305,1306,1307,1308,1309,1310',
        'boss' => 
        array (
          0 => '1314',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  28 => 
  array (
    'name' => '暗黑之境',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '5',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '千锤百炼',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1315,1316,1317,1318,1319,1320',
        'boss' => 
        array (
          0 => '1321',
          1 => '1322',
          2 => '1323',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '雄辩之才',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1324,1325,1326,1327,1328,1329',
        'boss' => 
        array (
          0 => '1330',
          1 => '1331',
          2 => '1332',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '断金之力',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1339,
            1 => 1340,
            2 => 1341,
          ),
        ),
        'monsters' => '1333,1334,1335,1336,1337,1338',
        'boss' => 
        array (
          0 => '1342',
          1 => '1343',
          2 => '1344',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '恍惚飘然',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1351,
            1 => 1352,
            2 => 1353,
          ),
        ),
        'monsters' => '1345,1346,1347,1348,1349,1350',
        'boss' => 
        array (
          0 => '1354',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '天降祥瑞',
        'stamina' => 14,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1361,
            1 => 1362,
            2 => 1363,
          ),
        ),
        'monsters' => '1355,1356,1357,1358,1359,1360',
        'boss' => 
        array (
          0 => '1364',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  29 => 
  array (
    'name' => '无尽战场',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '良将帅才',
        'stamina' => 12,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1373,
            1 => 1374,
          ),
        ),
        'monsters' => '1365,1366,1367,1368,1369,1370,1371,1372',
        'boss' => 
        array (
          0 => '1375',
          1 => '1376',
          2 => '1377',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '音律无双',
        'stamina' => 12,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1386,
          ),
        ),
        'monsters' => '1378,1379,1380,1381,1382,1383,1384,1385',
        'boss' => 
        array (
          0 => '1387',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '雍容东渐',
        'stamina' => 12,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1396,
            1 => 1397,
          ),
        ),
        'monsters' => '1388,1389,1390,1391,1392,1393,1394,1395',
        'boss' => 
        array (
          0 => '1398',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '鹏游蝶梦',
        'stamina' => 12,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1407,
            1 => 1408,
          ),
        ),
        'monsters' => '1399,1400,1401,1402,1403,1404,1405,1406',
        'boss' => 
        array (
          0 => '1409',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '无双仲达',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1418,
            1 => 1419,
          ),
        ),
        'monsters' => '1410,1411,1412,1413,1414,1415,1416,1417',
        'boss' => 
        array (
          0 => '1420',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
);
?>