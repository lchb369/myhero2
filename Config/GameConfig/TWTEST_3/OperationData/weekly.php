<?php
return array (
  1 => 
  array (
    'name' => '周五限时战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 5,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '古卷之魔',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '355,356,357,358,359',
        'boss' => 
        array (
          0 => '360',
        ),
        'package' => 102,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  2 => 
  array (
    'name' => '周六限时战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 6,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级三色宝石',
        'stamina' => 10,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 4,
          3 => 4,
          4 => 4,
          5 => 4,
          6 => 4,
          7 => 4,
          8 => 4,
          9 => 4,
        ),
        'monsters' => '250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267',
        'boss' => 
        array (
          0 => '268',
          1 => '269',
          2 => '270',
        ),
        'package' => 103,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 10000,
      ),
      2 => 
      array (
        'name' => '中级三色宝石',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 4,
          3 => 3,
          4 => 4,
          5 => 3,
          6 => 4,
          7 => 3,
          8 => 4,
          9 => 3,
        ),
        'monsters' => '271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288',
        'boss' => 
        array (
          0 => '289',
          1 => '290',
          2 => '291',
        ),
        'package' => 104,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 2000,
        'dropGoldRate' => 10000,
      ),
      3 => 
      array (
        'name' => '上级三色宝石',
        'stamina' => 20,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 4,
          3 => 3,
          4 => 4,
          5 => 3,
          6 => 4,
          7 => 3,
          8 => 4,
          9 => 3,
        ),
        'monsters' => '292,293,294,295,296,297,298,299,300,301,302,303,304',
        'boss' => 
        array (
          0 => '305',
          1 => '306',
          2 => '307',
        ),
        'package' => 105,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 3000,
        'dropGoldRate' => 10000,
      ),
    ),
  ),
  3 => 
  array (
    'name' => '周日限时战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 7,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级三色宝石',
        'stamina' => 10,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 4,
          3 => 4,
          4 => 4,
          5 => 4,
          6 => 4,
          7 => 4,
          8 => 4,
          9 => 4,
        ),
        'monsters' => '250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267',
        'boss' => 
        array (
          0 => '268',
          1 => '269',
          2 => '270',
        ),
        'package' => 103,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 10000,
      ),
      2 => 
      array (
        'name' => '中级三色宝石',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 4,
          3 => 3,
          4 => 4,
          5 => 3,
          6 => 4,
          7 => 3,
          8 => 4,
          9 => 3,
        ),
        'monsters' => '271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288',
        'boss' => 
        array (
          0 => '289',
          1 => '290',
          2 => '291',
        ),
        'package' => 104,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 2000,
        'dropGoldRate' => 10000,
      ),
      3 => 
      array (
        'name' => '上级三色宝石',
        'stamina' => 20,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 4,
          3 => 3,
          4 => 4,
          5 => 3,
          6 => 4,
          7 => 3,
          8 => 4,
          9 => 3,
        ),
        'monsters' => '292,293,294,295,296,297,298,299,300,301,302,303,304',
        'boss' => 
        array (
          0 => '305',
          1 => '306',
          2 => '307',
        ),
        'package' => 105,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 3000,
        'dropGoldRate' => 10000,
      ),
    ),
  ),
  4 => 
  array (
    'name' => '周一限时战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 1,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '魂之玉',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '311,312,313',
        'boss' => 
        array (
          0 => '314',
          1 => '315',
        ),
        'package' => 106,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '魂之兽',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '565,566,567,568,569',
        'boss' => 
        array (
          0 => '568',
          1 => '569',
          2 => '569',
        ),
        'package' => 107,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  5 => 
  array (
    'name' => '周二限时战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 2,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '上级绯红魔刃',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '316,317,318,319,320,321',
        'boss' => 
        array (
          0 => '322',
          1 => '323',
          2 => '324',
        ),
        'package' => 108,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上级幽蓝魔刃',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '325,326,327,328,329,330',
        'boss' => 
        array (
          0 => '331',
          1 => '332',
          2 => '333',
        ),
        'package' => 109,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上级翠绿魔刃',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '334,335,336,337,338,339',
        'boss' => 
        array (
          0 => '340',
          1 => '341',
          2 => '342',
        ),
        'package' => 110,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  6 => 
  array (
    'name' => '周三限时战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 3,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '鬼之佩饰',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '343,344,345,346,347,348,349,350',
        'boss' => 
        array (
          0 => '349',
          1 => '350',
        ),
        'package' => 111,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  7 => 
  array (
    'name' => '周四限时战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 4,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '如意之灵',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '351,352,353,354',
        'boss' => 
        array (
          0 => '353',
          1 => '354',
        ),
        'package' => 112,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  8 => 
  array (
    'name' => '周一技能战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 1,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 攻击之技',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '778,779',
        'boss' => 
        array (
          0 => '780',
          1 => '780',
          2 => '780',
        ),
        'package' => 139,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 攻击之技',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '781,782,783',
        'boss' => 
        array (
          0 => '784',
          1 => '785',
          2 => '786',
        ),
        'package' => 140,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '高级 攻击之技',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '787,788,789',
        'boss' => 
        array (
          0 => '790',
          1 => '790',
          2 => '790',
        ),
        'package' => 141,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  9 => 
  array (
    'name' => '周二技能战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 2,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 恢复之技',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '791,792',
        'boss' => 
        array (
          0 => '793',
          1 => '793',
        ),
        'package' => 142,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 恢复之技',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '794,795,796',
        'boss' => 
        array (
          0 => '797',
          1 => '798',
          2 => '799',
        ),
        'package' => 143,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '高级 恢复之技',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '800,801,802',
        'boss' => 
        array (
          0 => '803',
          1 => '803',
          2 => '803',
        ),
        'package' => 144,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  10 => 
  array (
    'name' => '周三技能战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 3,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 变化之技',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '804,805',
        'boss' => 
        array (
          0 => '806',
          1 => '806',
        ),
        'package' => 145,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 变化之技',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '807,808,809',
        'boss' => 
        array (
          0 => '810',
          1 => '811',
          2 => '812',
        ),
        'package' => 146,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '高级 变化之技',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '813,814,815',
        'boss' => 
        array (
          0 => '816',
          1 => '816',
          2 => '816',
        ),
        'package' => 147,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  11 => 
  array (
    'name' => '周四技能战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 4,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 万物之技',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '817,818',
        'boss' => 
        array (
          0 => '819',
          1 => '819',
        ),
        'package' => 148,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 万物之技',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '820,821,822',
        'boss' => 
        array (
          0 => '823',
          1 => '824',
          2 => '825',
        ),
        'package' => 149,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '高级 万物之技',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '826,827,828',
        'boss' => 
        array (
          0 => '829',
          1 => '829',
          2 => '829',
        ),
        'package' => 150,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  12 => 
  array (
    'name' => '周五技能战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 5,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 技压群雄',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '830,831,832,833,834,835,836,837',
        'boss' => 
        array (
          0 => '838',
          1 => '839',
          2 => '840',
          3 => '841',
        ),
        'package' => 151,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 技压群雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '842,843,844,845,846,847,848,849,850,851,852,853',
        'boss' => 
        array (
          0 => '854',
          1 => '855',
          2 => '856',
          3 => '857',
        ),
        'package' => 152,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '高级 技压群雄',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 
          array (
            0 => 870,
            1 => 871,
          ),
        ),
        'monsters' => '858,859,860,861,862,863,864,865,866,867,868,869',
        'boss' => 
        array (
          0 => '870',
          1 => '871',
          2 => '872',
          3 => '873',
        ),
        'package' => 153,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  13 => 
  array (
    'name' => '周六技能战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 6,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 技压群雄',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '830,831,832,833,834,835,836,837',
        'boss' => 
        array (
          0 => '838',
          1 => '839',
          2 => '840',
          3 => '841',
        ),
        'package' => 151,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 技压群雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '842,843,844,845,846,847,848,849,850,851,852,853',
        'boss' => 
        array (
          0 => '854',
          1 => '855',
          2 => '856',
          3 => '857',
        ),
        'package' => 152,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '高级 技压群雄',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 
          array (
            0 => 870,
            1 => 871,
          ),
        ),
        'monsters' => '858,859,860,861,862,863,864,865,866,867,868,869',
        'boss' => 
        array (
          0 => '870',
          1 => '871',
          2 => '872',
          3 => '873',
        ),
        'package' => 153,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  14 => 
  array (
    'name' => '周日技能战场',
    'start_time' => '0',
    'end_time' => '0',
    'week_day' => 7,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 技压群雄',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '830,831,832,833,834,835,836,837',
        'boss' => 
        array (
          0 => '838',
          1 => '839',
          2 => '840',
          3 => '841',
        ),
        'package' => 151,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 技压群雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '842,843,844,845,846,847,848,849,850,851,852,853',
        'boss' => 
        array (
          0 => '854',
          1 => '855',
          2 => '856',
          3 => '857',
        ),
        'package' => 152,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '高级 技压群雄',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 
          array (
            0 => 870,
            1 => 871,
          ),
        ),
        'monsters' => '858,859,860,861,862,863,864,865,866,867,868,869',
        'boss' => 
        array (
          0 => '870',
          1 => '871',
          2 => '872',
          3 => '873',
        ),
        'package' => 153,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  15 => 
  array (
    'name' => '七色天书',
    'start_time' => '09:00:00',
    'end_time' => '21:00:00',
    'week_day' => 1,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 七色天书',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '760,761,762,763,764',
        'boss' => 
        array (
          0 => '765',
        ),
        'package' => 138,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 5000,
      ),
      2 => 
      array (
        'name' => '中级 七色天书',
        'stamina' => 15,
        'steps' => 4,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
        ),
        'monsters' => '766,767,768,769,770',
        'boss' => 
        array (
          0 => '771',
          1 => '771',
        ),
        'package' => 138,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1500,
        'dropGoldRate' => 5000,
      ),
      3 => 
      array (
        'name' => '高级 七色天书',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '772,773,774,775,776',
        'boss' => 
        array (
          0 => '777',
          1 => '777',
          2 => '777',
        ),
        'package' => 138,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 2000,
        'dropGoldRate' => 5000,
      ),
    ),
  ),
  16 => 
  array (
    'name' => '七色天书',
    'start_time' => '09:00:00',
    'end_time' => '21:00:00',
    'week_day' => 3,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 七色天书',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '760,761,762,763,764',
        'boss' => 
        array (
          0 => '765',
        ),
        'package' => 138,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 5000,
      ),
      2 => 
      array (
        'name' => '中级 七色天书',
        'stamina' => 15,
        'steps' => 4,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
        ),
        'monsters' => '766,767,768,769,770',
        'boss' => 
        array (
          0 => '771',
          1 => '771',
        ),
        'package' => 138,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1500,
        'dropGoldRate' => 5000,
      ),
      3 => 
      array (
        'name' => '高级 七色天书',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '772,773,774,775,776',
        'boss' => 
        array (
          0 => '777',
          1 => '777',
          2 => '777',
        ),
        'package' => 138,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 2000,
        'dropGoldRate' => 5000,
      ),
    ),
  ),
  17 => 
  array (
    'name' => '七色天书',
    'start_time' => '09:00:00',
    'end_time' => '21:00:00',
    'week_day' => 5,
    'effect' => 
    array (
    ),
    'diamond_type' => 
    array (
      0 => '0',
      1 => '1',
      2 => '2',
      3 => '3',
      4 => '4',
      5 => '5',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 七色天书',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '760,761,762,763,764',
        'boss' => 
        array (
          0 => '765',
        ),
        'package' => 138,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 5000,
      ),
      2 => 
      array (
        'name' => '中级 七色天书',
        'stamina' => 15,
        'steps' => 4,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
        ),
        'monsters' => '766,767,768,769,770',
        'boss' => 
        array (
          0 => '771',
          1 => '771',
        ),
        'package' => 138,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1500,
        'dropGoldRate' => 5000,
      ),
      3 => 
      array (
        'name' => '高级 七色天书',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '772,773,774,775,776',
        'boss' => 
        array (
          0 => '777',
          1 => '777',
          2 => '777',
        ),
        'package' => 138,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 2000,
        'dropGoldRate' => 5000,
      ),
    ),
  ),
);
?>