<?php
return array (
  'columns' => 
  array (
    0 => 'Type',
    1 => 'level',
    2 => 'value',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'Type' => 'a',
      'level' => 1,
      'value' => 400,
    ),
    1 => 
    array (
      'Type' => 'a',
      'level' => 2,
      'value' => 900,
    ),
    2 => 
    array (
      'Type' => 'a',
      'level' => 3,
      'value' => 1400,
    ),
    3 => 
    array (
      'Type' => 'a',
      'level' => 4,
      'value' => 2000,
    ),
    4 => 
    array (
      'Type' => 'a',
      'level' => 5,
      'value' => 2600,
    ),
    5 => 
    array (
      'Type' => 'a',
      'level' => 6,
      'value' => 3500,
    ),
    6 => 
    array (
      'Type' => 'a',
      'level' => 7,
      'value' => 4750,
    ),
    7 => 
    array (
      'Type' => 'a',
      'level' => 8,
      'value' => 6000,
    ),
    8 => 
    array (
      'Type' => 'a',
      'level' => 9,
      'value' => 12000,
    ),
    9 => 
    array (
      'Type' => 'a',
      'level' => 10,
      'value' => 24000,
    ),
    10 => 
    array (
      'Type' => 'a',
      'level' => 11,
      'value' => 36000,
    ),
    11 => 
    array (
      'Type' => 'a',
      'level' => 12,
      'value' => 50000,
    ),
    12 => 
    array (
      'Type' => 'a',
      'level' => 13,
      'value' => 110000,
    ),
    13 => 
    array (
      'Type' => 'a',
      'level' => 14,
      'value' => 170000,
    ),
    14 => 
    array (
      'Type' => 'a',
      'level' => 15,
      'value' => 235000,
    ),
    15 => 
    array (
      'Type' => 'a',
      'level' => 16,
      'value' => 300000,
    ),
    16 => 
    array (
      'Type' => 'a',
      'level' => 17,
      'value' => 375000,
    ),
    17 => 
    array (
      'Type' => 'a',
      'level' => 18,
      'value' => 468750,
    ),
    18 => 
    array (
      'Type' => 'a',
      'level' => 19,
      'value' => 585938,
    ),
    19 => 
    array (
      'Type' => 'a',
      'level' => 20,
      'value' => 732423,
    ),
    20 => 
    array (
      'Type' => 'b',
      'level' => 1,
      'value' => 2600,
    ),
    21 => 
    array (
      'Type' => 'b',
      'level' => 2,
      'value' => 3500,
    ),
    22 => 
    array (
      'Type' => 'b',
      'level' => 3,
      'value' => 4750,
    ),
    23 => 
    array (
      'Type' => 'b',
      'level' => 4,
      'value' => 6000,
    ),
    24 => 
    array (
      'Type' => 'b',
      'level' => 5,
      'value' => 12000,
    ),
    25 => 
    array (
      'Type' => 'b',
      'level' => 6,
      'value' => 24000,
    ),
    26 => 
    array (
      'Type' => 'b',
      'level' => 7,
      'value' => 36000,
    ),
    27 => 
    array (
      'Type' => 'b',
      'level' => 8,
      'value' => 50000,
    ),
    28 => 
    array (
      'Type' => 'b',
      'level' => 9,
      'value' => 110000,
    ),
    29 => 
    array (
      'Type' => 'b',
      'level' => 10,
      'value' => 170000,
    ),
    30 => 
    array (
      'Type' => 'b',
      'level' => 11,
      'value' => 235000,
    ),
    31 => 
    array (
      'Type' => 'b',
      'level' => 12,
      'value' => 300000,
    ),
    32 => 
    array (
      'Type' => 'b',
      'level' => 13,
      'value' => 375000,
    ),
    33 => 
    array (
      'Type' => 'b',
      'level' => 14,
      'value' => 468750,
    ),
    34 => 
    array (
      'Type' => 'b',
      'level' => 15,
      'value' => 585938,
    ),
    35 => 
    array (
      'Type' => 'b',
      'level' => 16,
      'value' => 732423,
    ),
    36 => 
    array (
      'Type' => 'b',
      'level' => 17,
      'value' => 915529,
    ),
    37 => 
    array (
      'Type' => 'b',
      'level' => 18,
      'value' => 1144412,
    ),
    38 => 
    array (
      'Type' => 'b',
      'level' => 19,
      'value' => 1430515,
    ),
    39 => 
    array (
      'Type' => 'b',
      'level' => 20,
      'value' => 1788144,
    ),
    40 => 
    array (
      'Type' => 'c',
      'level' => 1,
      'value' => 24000,
    ),
    41 => 
    array (
      'Type' => 'c',
      'level' => 2,
      'value' => 36000,
    ),
    42 => 
    array (
      'Type' => 'c',
      'level' => 3,
      'value' => 50000,
    ),
    43 => 
    array (
      'Type' => 'c',
      'level' => 4,
      'value' => 110000,
    ),
    44 => 
    array (
      'Type' => 'c',
      'level' => 5,
      'value' => 170000,
    ),
    45 => 
    array (
      'Type' => 'c',
      'level' => 6,
      'value' => 235000,
    ),
    46 => 
    array (
      'Type' => 'c',
      'level' => 7,
      'value' => 300000,
    ),
    47 => 
    array (
      'Type' => 'c',
      'level' => 8,
      'value' => 375000,
    ),
    48 => 
    array (
      'Type' => 'c',
      'level' => 9,
      'value' => 468750,
    ),
    49 => 
    array (
      'Type' => 'c',
      'level' => 10,
      'value' => 585938,
    ),
    50 => 
    array (
      'Type' => 'c',
      'level' => 11,
      'value' => 732423,
    ),
    51 => 
    array (
      'Type' => 'c',
      'level' => 12,
      'value' => 915529,
    ),
    52 => 
    array (
      'Type' => 'c',
      'level' => 13,
      'value' => 1144412,
    ),
    53 => 
    array (
      'Type' => 'c',
      'level' => 14,
      'value' => 1430515,
    ),
    54 => 
    array (
      'Type' => 'c',
      'level' => 15,
      'value' => 1788144,
    ),
    55 => 
    array (
      'Type' => 'd',
      'level' => 1,
      'value' => 732423,
    ),
    56 => 
    array (
      'Type' => 'd',
      'level' => 2,
      'value' => 915529,
    ),
    57 => 
    array (
      'Type' => 'd',
      'level' => 3,
      'value' => 1144412,
    ),
    58 => 
    array (
      'Type' => 'd',
      'level' => 4,
      'value' => 1430515,
    ),
    59 => 
    array (
      'Type' => 'd',
      'level' => 5,
      'value' => 1788144,
    ),
  ),
);
?>