<?php
return array (
  'columns' => 
  array (
    0 => 'id',
    1 => 'cardId',
    2 => 'cardLevel',
    3 => 'rate',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'id' => 1,
      'cardId' => '41_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1 => 
    array (
      'id' => 1,
      'cardId' => '43_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    2 => 
    array (
      'id' => 1,
      'cardId' => '45_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    3 => 
    array (
      'id' => 1,
      'cardId' => '47_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    4 => 
    array (
      'id' => 1,
      'cardId' => '49_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    5 => 
    array (
      'id' => 1,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 3000,
    ),
    6 => 
    array (
      'id' => 1,
      'cardId' => '54_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    7 => 
    array (
      'id' => 1,
      'cardId' => '57_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    8 => 
    array (
      'id' => 1,
      'cardId' => '68_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    9 => 
    array (
      'id' => 2,
      'cardId' => '41_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    10 => 
    array (
      'id' => 2,
      'cardId' => '43_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    11 => 
    array (
      'id' => 2,
      'cardId' => '45_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    12 => 
    array (
      'id' => 2,
      'cardId' => '47_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    13 => 
    array (
      'id' => 2,
      'cardId' => '49_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    14 => 
    array (
      'id' => 2,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 3000,
    ),
    15 => 
    array (
      'id' => 2,
      'cardId' => '57_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    16 => 
    array (
      'id' => 2,
      'cardId' => '68_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    17 => 
    array (
      'id' => 2,
      'cardId' => '70_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    18 => 
    array (
      'id' => 3,
      'cardId' => '41_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    19 => 
    array (
      'id' => 3,
      'cardId' => '43_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    20 => 
    array (
      'id' => 3,
      'cardId' => '45_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    21 => 
    array (
      'id' => 3,
      'cardId' => '47_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    22 => 
    array (
      'id' => 3,
      'cardId' => '49_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    23 => 
    array (
      'id' => 3,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 3000,
    ),
    24 => 
    array (
      'id' => 3,
      'cardId' => '92_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    25 => 
    array (
      'id' => 4,
      'cardId' => '41_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    26 => 
    array (
      'id' => 4,
      'cardId' => '51_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    27 => 
    array (
      'id' => 4,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    28 => 
    array (
      'id' => 4,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    29 => 
    array (
      'id' => 4,
      'cardId' => '160_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    30 => 
    array (
      'id' => 4,
      'cardId' => '42_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    31 => 
    array (
      'id' => 5,
      'cardId' => '41_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    32 => 
    array (
      'id' => 5,
      'cardId' => '51_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    33 => 
    array (
      'id' => 5,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    34 => 
    array (
      'id' => 5,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    35 => 
    array (
      'id' => 5,
      'cardId' => '160_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    36 => 
    array (
      'id' => 5,
      'cardId' => '88_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    37 => 
    array (
      'id' => 6,
      'cardId' => '41_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    38 => 
    array (
      'id' => 6,
      'cardId' => '51_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    39 => 
    array (
      'id' => 6,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    40 => 
    array (
      'id' => 6,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    41 => 
    array (
      'id' => 6,
      'cardId' => '160_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    42 => 
    array (
      'id' => 6,
      'cardId' => '1_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    43 => 
    array (
      'id' => 7,
      'cardId' => '43_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    44 => 
    array (
      'id' => 7,
      'cardId' => '54_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    45 => 
    array (
      'id' => 7,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    46 => 
    array (
      'id' => 7,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    47 => 
    array (
      'id' => 7,
      'cardId' => '161_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    48 => 
    array (
      'id' => 7,
      'cardId' => '44_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    49 => 
    array (
      'id' => 8,
      'cardId' => '43_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    50 => 
    array (
      'id' => 8,
      'cardId' => '54_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    51 => 
    array (
      'id' => 8,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    52 => 
    array (
      'id' => 8,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    53 => 
    array (
      'id' => 8,
      'cardId' => '161_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    54 => 
    array (
      'id' => 8,
      'cardId' => '92_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    55 => 
    array (
      'id' => 9,
      'cardId' => '43_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    56 => 
    array (
      'id' => 9,
      'cardId' => '54_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    57 => 
    array (
      'id' => 9,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    58 => 
    array (
      'id' => 9,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    59 => 
    array (
      'id' => 9,
      'cardId' => '161_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    60 => 
    array (
      'id' => 9,
      'cardId' => '5_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    61 => 
    array (
      'id' => 10,
      'cardId' => '45_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    62 => 
    array (
      'id' => 10,
      'cardId' => '57_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    63 => 
    array (
      'id' => 10,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    64 => 
    array (
      'id' => 10,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    65 => 
    array (
      'id' => 10,
      'cardId' => '162_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    66 => 
    array (
      'id' => 10,
      'cardId' => '46_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    67 => 
    array (
      'id' => 11,
      'cardId' => '45_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    68 => 
    array (
      'id' => 11,
      'cardId' => '57_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    69 => 
    array (
      'id' => 11,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    70 => 
    array (
      'id' => 11,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    71 => 
    array (
      'id' => 11,
      'cardId' => '162_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    72 => 
    array (
      'id' => 11,
      'cardId' => '113_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    73 => 
    array (
      'id' => 12,
      'cardId' => '45_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    74 => 
    array (
      'id' => 12,
      'cardId' => '57_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    75 => 
    array (
      'id' => 12,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    76 => 
    array (
      'id' => 12,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    77 => 
    array (
      'id' => 12,
      'cardId' => '162_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    78 => 
    array (
      'id' => 12,
      'cardId' => '9_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    79 => 
    array (
      'id' => 13,
      'cardId' => '47_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    80 => 
    array (
      'id' => 13,
      'cardId' => '60_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    81 => 
    array (
      'id' => 13,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    82 => 
    array (
      'id' => 13,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    83 => 
    array (
      'id' => 13,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    84 => 
    array (
      'id' => 13,
      'cardId' => '163_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    85 => 
    array (
      'id' => 13,
      'cardId' => '48_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    86 => 
    array (
      'id' => 14,
      'cardId' => '47_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    87 => 
    array (
      'id' => 14,
      'cardId' => '60_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    88 => 
    array (
      'id' => 14,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    89 => 
    array (
      'id' => 14,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    90 => 
    array (
      'id' => 14,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    91 => 
    array (
      'id' => 14,
      'cardId' => '163_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    92 => 
    array (
      'id' => 14,
      'cardId' => '94_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    93 => 
    array (
      'id' => 15,
      'cardId' => '47_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    94 => 
    array (
      'id' => 15,
      'cardId' => '60_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    95 => 
    array (
      'id' => 15,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    96 => 
    array (
      'id' => 15,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    97 => 
    array (
      'id' => 15,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    98 => 
    array (
      'id' => 15,
      'cardId' => '163_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    99 => 
    array (
      'id' => 15,
      'cardId' => '13_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    100 => 
    array (
      'id' => 16,
      'cardId' => '49_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    101 => 
    array (
      'id' => 16,
      'cardId' => '63_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    102 => 
    array (
      'id' => 16,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    103 => 
    array (
      'id' => 16,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    104 => 
    array (
      'id' => 16,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    105 => 
    array (
      'id' => 16,
      'cardId' => '164_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    106 => 
    array (
      'id' => 16,
      'cardId' => '50_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    107 => 
    array (
      'id' => 17,
      'cardId' => '49_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    108 => 
    array (
      'id' => 17,
      'cardId' => '63_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    109 => 
    array (
      'id' => 17,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    110 => 
    array (
      'id' => 17,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    111 => 
    array (
      'id' => 17,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    112 => 
    array (
      'id' => 17,
      'cardId' => '164_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    113 => 
    array (
      'id' => 17,
      'cardId' => '96_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    114 => 
    array (
      'id' => 18,
      'cardId' => '49_card',
      'cardLevel' => 4,
      'rate' => 2000,
    ),
    115 => 
    array (
      'id' => 18,
      'cardId' => '63_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    116 => 
    array (
      'id' => 18,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    117 => 
    array (
      'id' => 18,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    118 => 
    array (
      'id' => 18,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2250,
    ),
    119 => 
    array (
      'id' => 18,
      'cardId' => '164_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    120 => 
    array (
      'id' => 18,
      'cardId' => '17_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    121 => 
    array (
      'id' => 19,
      'cardId' => '41_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    122 => 
    array (
      'id' => 19,
      'cardId' => '43_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    123 => 
    array (
      'id' => 19,
      'cardId' => '45_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    124 => 
    array (
      'id' => 19,
      'cardId' => '47_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    125 => 
    array (
      'id' => 19,
      'cardId' => '49_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    126 => 
    array (
      'id' => 19,
      'cardId' => '51_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    127 => 
    array (
      'id' => 19,
      'cardId' => '54_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    128 => 
    array (
      'id' => 19,
      'cardId' => '57_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    129 => 
    array (
      'id' => 19,
      'cardId' => '60_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    130 => 
    array (
      'id' => 19,
      'cardId' => '63_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    131 => 
    array (
      'id' => 19,
      'cardId' => '66_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    132 => 
    array (
      'id' => 19,
      'cardId' => '68_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    133 => 
    array (
      'id' => 19,
      'cardId' => '70_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    134 => 
    array (
      'id' => 19,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2500,
    ),
    135 => 
    array (
      'id' => 19,
      'cardId' => '158_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    136 => 
    array (
      'id' => 19,
      'cardId' => '42_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    137 => 
    array (
      'id' => 19,
      'cardId' => '44_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    138 => 
    array (
      'id' => 19,
      'cardId' => '46_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    139 => 
    array (
      'id' => 20,
      'cardId' => '41_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    140 => 
    array (
      'id' => 20,
      'cardId' => '43_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    141 => 
    array (
      'id' => 20,
      'cardId' => '45_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    142 => 
    array (
      'id' => 20,
      'cardId' => '47_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    143 => 
    array (
      'id' => 20,
      'cardId' => '49_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    144 => 
    array (
      'id' => 20,
      'cardId' => '51_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    145 => 
    array (
      'id' => 20,
      'cardId' => '54_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    146 => 
    array (
      'id' => 20,
      'cardId' => '57_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    147 => 
    array (
      'id' => 20,
      'cardId' => '60_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    148 => 
    array (
      'id' => 20,
      'cardId' => '63_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    149 => 
    array (
      'id' => 20,
      'cardId' => '66_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    150 => 
    array (
      'id' => 20,
      'cardId' => '68_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    151 => 
    array (
      'id' => 20,
      'cardId' => '70_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    152 => 
    array (
      'id' => 20,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2500,
    ),
    153 => 
    array (
      'id' => 20,
      'cardId' => '158_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    154 => 
    array (
      'id' => 20,
      'cardId' => '94_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    155 => 
    array (
      'id' => 20,
      'cardId' => '96_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    156 => 
    array (
      'id' => 21,
      'cardId' => '41_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    157 => 
    array (
      'id' => 21,
      'cardId' => '43_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    158 => 
    array (
      'id' => 21,
      'cardId' => '45_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    159 => 
    array (
      'id' => 21,
      'cardId' => '47_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    160 => 
    array (
      'id' => 21,
      'cardId' => '49_card',
      'cardLevel' => 6,
      'rate' => 700,
    ),
    161 => 
    array (
      'id' => 21,
      'cardId' => '51_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    162 => 
    array (
      'id' => 21,
      'cardId' => '54_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    163 => 
    array (
      'id' => 21,
      'cardId' => '57_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    164 => 
    array (
      'id' => 21,
      'cardId' => '60_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    165 => 
    array (
      'id' => 21,
      'cardId' => '63_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    166 => 
    array (
      'id' => 21,
      'cardId' => '66_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    167 => 
    array (
      'id' => 21,
      'cardId' => '68_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    168 => 
    array (
      'id' => 21,
      'cardId' => '70_card',
      'cardLevel' => 6,
      'rate' => 150,
    ),
    169 => 
    array (
      'id' => 21,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 2500,
    ),
    170 => 
    array (
      'id' => 21,
      'cardId' => '158_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    171 => 
    array (
      'id' => 21,
      'cardId' => '128_card',
      'cardLevel' => 1,
      'rate' => 30,
    ),
    172 => 
    array (
      'id' => 22,
      'cardId' => '41_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    173 => 
    array (
      'id' => 22,
      'cardId' => '51_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    174 => 
    array (
      'id' => 22,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    175 => 
    array (
      'id' => 22,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    176 => 
    array (
      'id' => 22,
      'cardId' => '78_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    177 => 
    array (
      'id' => 22,
      'cardId' => '21_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    178 => 
    array (
      'id' => 22,
      'cardId' => '166_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    179 => 
    array (
      'id' => 22,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    180 => 
    array (
      'id' => 22,
      'cardId' => '42_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    181 => 
    array (
      'id' => 23,
      'cardId' => '41_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    182 => 
    array (
      'id' => 23,
      'cardId' => '51_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    183 => 
    array (
      'id' => 23,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    184 => 
    array (
      'id' => 23,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    185 => 
    array (
      'id' => 23,
      'cardId' => '78_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    186 => 
    array (
      'id' => 23,
      'cardId' => '21_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    187 => 
    array (
      'id' => 23,
      'cardId' => '166_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    188 => 
    array (
      'id' => 23,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    189 => 
    array (
      'id' => 23,
      'cardId' => '88_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    190 => 
    array (
      'id' => 24,
      'cardId' => '41_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    191 => 
    array (
      'id' => 24,
      'cardId' => '51_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    192 => 
    array (
      'id' => 24,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    193 => 
    array (
      'id' => 24,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    194 => 
    array (
      'id' => 24,
      'cardId' => '78_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    195 => 
    array (
      'id' => 24,
      'cardId' => '21_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    196 => 
    array (
      'id' => 24,
      'cardId' => '166_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    197 => 
    array (
      'id' => 24,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    198 => 
    array (
      'id' => 24,
      'cardId' => '22_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    199 => 
    array (
      'id' => 25,
      'cardId' => '41_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    200 => 
    array (
      'id' => 25,
      'cardId' => '51_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    201 => 
    array (
      'id' => 25,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    202 => 
    array (
      'id' => 25,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    203 => 
    array (
      'id' => 25,
      'cardId' => '78_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    204 => 
    array (
      'id' => 25,
      'cardId' => '21_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    205 => 
    array (
      'id' => 25,
      'cardId' => '166_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    206 => 
    array (
      'id' => 25,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    207 => 
    array (
      'id' => 25,
      'cardId' => '98_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    208 => 
    array (
      'id' => 26,
      'cardId' => '41_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    209 => 
    array (
      'id' => 26,
      'cardId' => '51_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    210 => 
    array (
      'id' => 26,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    211 => 
    array (
      'id' => 26,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    212 => 
    array (
      'id' => 26,
      'cardId' => '78_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    213 => 
    array (
      'id' => 26,
      'cardId' => '21_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    214 => 
    array (
      'id' => 26,
      'cardId' => '166_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    215 => 
    array (
      'id' => 26,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    216 => 
    array (
      'id' => 26,
      'cardId' => '22_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    217 => 
    array (
      'id' => 27,
      'cardId' => '43_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    218 => 
    array (
      'id' => 27,
      'cardId' => '54_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    219 => 
    array (
      'id' => 27,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    220 => 
    array (
      'id' => 27,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    221 => 
    array (
      'id' => 27,
      'cardId' => '80_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    222 => 
    array (
      'id' => 27,
      'cardId' => '25_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    223 => 
    array (
      'id' => 27,
      'cardId' => '167_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    224 => 
    array (
      'id' => 27,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    225 => 
    array (
      'id' => 27,
      'cardId' => '44_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    226 => 
    array (
      'id' => 28,
      'cardId' => '43_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    227 => 
    array (
      'id' => 28,
      'cardId' => '54_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    228 => 
    array (
      'id' => 28,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    229 => 
    array (
      'id' => 28,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    230 => 
    array (
      'id' => 28,
      'cardId' => '80_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    231 => 
    array (
      'id' => 28,
      'cardId' => '25_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    232 => 
    array (
      'id' => 28,
      'cardId' => '167_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    233 => 
    array (
      'id' => 28,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    234 => 
    array (
      'id' => 28,
      'cardId' => '90_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    235 => 
    array (
      'id' => 29,
      'cardId' => '43_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    236 => 
    array (
      'id' => 29,
      'cardId' => '54_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    237 => 
    array (
      'id' => 29,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    238 => 
    array (
      'id' => 29,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    239 => 
    array (
      'id' => 29,
      'cardId' => '80_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    240 => 
    array (
      'id' => 29,
      'cardId' => '25_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    241 => 
    array (
      'id' => 29,
      'cardId' => '167_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    242 => 
    array (
      'id' => 29,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    243 => 
    array (
      'id' => 29,
      'cardId' => '25_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    244 => 
    array (
      'id' => 30,
      'cardId' => '43_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    245 => 
    array (
      'id' => 30,
      'cardId' => '54_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    246 => 
    array (
      'id' => 30,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    247 => 
    array (
      'id' => 30,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    248 => 
    array (
      'id' => 30,
      'cardId' => '80_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    249 => 
    array (
      'id' => 30,
      'cardId' => '25_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    250 => 
    array (
      'id' => 30,
      'cardId' => '167_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    251 => 
    array (
      'id' => 30,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    252 => 
    array (
      'id' => 30,
      'cardId' => '101_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    253 => 
    array (
      'id' => 31,
      'cardId' => '43_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    254 => 
    array (
      'id' => 31,
      'cardId' => '54_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    255 => 
    array (
      'id' => 31,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    256 => 
    array (
      'id' => 31,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    257 => 
    array (
      'id' => 31,
      'cardId' => '80_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    258 => 
    array (
      'id' => 31,
      'cardId' => '25_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    259 => 
    array (
      'id' => 31,
      'cardId' => '167_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    260 => 
    array (
      'id' => 31,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    261 => 
    array (
      'id' => 31,
      'cardId' => '26_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    262 => 
    array (
      'id' => 32,
      'cardId' => '45_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    263 => 
    array (
      'id' => 32,
      'cardId' => '57_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    264 => 
    array (
      'id' => 32,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    265 => 
    array (
      'id' => 32,
      'cardId' => '76_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    266 => 
    array (
      'id' => 32,
      'cardId' => '82_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    267 => 
    array (
      'id' => 32,
      'cardId' => '29_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    268 => 
    array (
      'id' => 32,
      'cardId' => '168_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    269 => 
    array (
      'id' => 32,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    270 => 
    array (
      'id' => 32,
      'cardId' => '46_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    271 => 
    array (
      'id' => 33,
      'cardId' => '45_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    272 => 
    array (
      'id' => 33,
      'cardId' => '57_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    273 => 
    array (
      'id' => 33,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    274 => 
    array (
      'id' => 33,
      'cardId' => '76_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    275 => 
    array (
      'id' => 33,
      'cardId' => '82_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    276 => 
    array (
      'id' => 33,
      'cardId' => '29_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    277 => 
    array (
      'id' => 33,
      'cardId' => '168_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    278 => 
    array (
      'id' => 33,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    279 => 
    array (
      'id' => 33,
      'cardId' => '92_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    280 => 
    array (
      'id' => 34,
      'cardId' => '45_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    281 => 
    array (
      'id' => 34,
      'cardId' => '57_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    282 => 
    array (
      'id' => 34,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    283 => 
    array (
      'id' => 34,
      'cardId' => '76_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    284 => 
    array (
      'id' => 34,
      'cardId' => '82_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    285 => 
    array (
      'id' => 34,
      'cardId' => '29_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    286 => 
    array (
      'id' => 34,
      'cardId' => '168_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    287 => 
    array (
      'id' => 34,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    288 => 
    array (
      'id' => 34,
      'cardId' => '29_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    289 => 
    array (
      'id' => 35,
      'cardId' => '45_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    290 => 
    array (
      'id' => 35,
      'cardId' => '57_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    291 => 
    array (
      'id' => 35,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    292 => 
    array (
      'id' => 35,
      'cardId' => '76_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    293 => 
    array (
      'id' => 35,
      'cardId' => '82_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    294 => 
    array (
      'id' => 35,
      'cardId' => '29_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    295 => 
    array (
      'id' => 35,
      'cardId' => '168_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    296 => 
    array (
      'id' => 35,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    297 => 
    array (
      'id' => 35,
      'cardId' => '104_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    298 => 
    array (
      'id' => 36,
      'cardId' => '45_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    299 => 
    array (
      'id' => 36,
      'cardId' => '57_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    300 => 
    array (
      'id' => 36,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    301 => 
    array (
      'id' => 36,
      'cardId' => '76_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    302 => 
    array (
      'id' => 36,
      'cardId' => '82_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    303 => 
    array (
      'id' => 36,
      'cardId' => '29_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    304 => 
    array (
      'id' => 36,
      'cardId' => '168_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    305 => 
    array (
      'id' => 36,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    306 => 
    array (
      'id' => 36,
      'cardId' => '30_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    307 => 
    array (
      'id' => 37,
      'cardId' => '47_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    308 => 
    array (
      'id' => 37,
      'cardId' => '60_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    309 => 
    array (
      'id' => 37,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    310 => 
    array (
      'id' => 37,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    311 => 
    array (
      'id' => 37,
      'cardId' => '84_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    312 => 
    array (
      'id' => 37,
      'cardId' => '169_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    313 => 
    array (
      'id' => 37,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    314 => 
    array (
      'id' => 37,
      'cardId' => '48_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    315 => 
    array (
      'id' => 38,
      'cardId' => '47_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    316 => 
    array (
      'id' => 38,
      'cardId' => '60_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    317 => 
    array (
      'id' => 38,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    318 => 
    array (
      'id' => 38,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    319 => 
    array (
      'id' => 38,
      'cardId' => '84_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    320 => 
    array (
      'id' => 38,
      'cardId' => '169_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    321 => 
    array (
      'id' => 38,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    322 => 
    array (
      'id' => 38,
      'cardId' => '94_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    323 => 
    array (
      'id' => 39,
      'cardId' => '47_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    324 => 
    array (
      'id' => 39,
      'cardId' => '60_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    325 => 
    array (
      'id' => 39,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    326 => 
    array (
      'id' => 39,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    327 => 
    array (
      'id' => 39,
      'cardId' => '84_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    328 => 
    array (
      'id' => 39,
      'cardId' => '169_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    329 => 
    array (
      'id' => 39,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    330 => 
    array (
      'id' => 39,
      'cardId' => '30_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    331 => 
    array (
      'id' => 40,
      'cardId' => '47_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    332 => 
    array (
      'id' => 40,
      'cardId' => '60_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    333 => 
    array (
      'id' => 40,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    334 => 
    array (
      'id' => 40,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    335 => 
    array (
      'id' => 40,
      'cardId' => '84_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    336 => 
    array (
      'id' => 40,
      'cardId' => '169_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    337 => 
    array (
      'id' => 40,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    338 => 
    array (
      'id' => 40,
      'cardId' => '107_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    339 => 
    array (
      'id' => 41,
      'cardId' => '47_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    340 => 
    array (
      'id' => 41,
      'cardId' => '60_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    341 => 
    array (
      'id' => 41,
      'cardId' => '66_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    342 => 
    array (
      'id' => 41,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    343 => 
    array (
      'id' => 41,
      'cardId' => '84_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    344 => 
    array (
      'id' => 41,
      'cardId' => '169_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    345 => 
    array (
      'id' => 41,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    346 => 
    array (
      'id' => 41,
      'cardId' => '33_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    347 => 
    array (
      'id' => 42,
      'cardId' => '49_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    348 => 
    array (
      'id' => 42,
      'cardId' => '63_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    349 => 
    array (
      'id' => 42,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    350 => 
    array (
      'id' => 42,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    351 => 
    array (
      'id' => 42,
      'cardId' => '86_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    352 => 
    array (
      'id' => 42,
      'cardId' => '170_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    353 => 
    array (
      'id' => 42,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    354 => 
    array (
      'id' => 42,
      'cardId' => '50_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    355 => 
    array (
      'id' => 43,
      'cardId' => '49_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    356 => 
    array (
      'id' => 43,
      'cardId' => '63_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    357 => 
    array (
      'id' => 43,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    358 => 
    array (
      'id' => 43,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    359 => 
    array (
      'id' => 43,
      'cardId' => '86_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    360 => 
    array (
      'id' => 43,
      'cardId' => '170_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    361 => 
    array (
      'id' => 43,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    362 => 
    array (
      'id' => 43,
      'cardId' => '96_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    363 => 
    array (
      'id' => 44,
      'cardId' => '49_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    364 => 
    array (
      'id' => 44,
      'cardId' => '63_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    365 => 
    array (
      'id' => 44,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    366 => 
    array (
      'id' => 44,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    367 => 
    array (
      'id' => 44,
      'cardId' => '86_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    368 => 
    array (
      'id' => 44,
      'cardId' => '170_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    369 => 
    array (
      'id' => 44,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    370 => 
    array (
      'id' => 44,
      'cardId' => '37_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    371 => 
    array (
      'id' => 45,
      'cardId' => '49_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    372 => 
    array (
      'id' => 45,
      'cardId' => '63_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    373 => 
    array (
      'id' => 45,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    374 => 
    array (
      'id' => 45,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    375 => 
    array (
      'id' => 45,
      'cardId' => '86_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    376 => 
    array (
      'id' => 45,
      'cardId' => '170_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    377 => 
    array (
      'id' => 45,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    378 => 
    array (
      'id' => 45,
      'cardId' => '110_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    379 => 
    array (
      'id' => 46,
      'cardId' => '49_card',
      'cardLevel' => 4,
      'rate' => 2500,
    ),
    380 => 
    array (
      'id' => 46,
      'cardId' => '63_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    381 => 
    array (
      'id' => 46,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    382 => 
    array (
      'id' => 46,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    383 => 
    array (
      'id' => 46,
      'cardId' => '86_card',
      'cardLevel' => 4,
      'rate' => 150,
    ),
    384 => 
    array (
      'id' => 46,
      'cardId' => '170_card',
      'cardLevel' => 1,
      'rate' => 2300,
    ),
    385 => 
    array (
      'id' => 46,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    386 => 
    array (
      'id' => 46,
      'cardId' => '38_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    387 => 
    array (
      'id' => 47,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 100,
    ),
    388 => 
    array (
      'id' => 47,
      'cardId' => '78_card',
      'cardLevel' => 4,
      'rate' => 100,
    ),
    389 => 
    array (
      'id' => 47,
      'cardId' => '43_card',
      'cardLevel' => 4,
      'rate' => 1200,
    ),
    390 => 
    array (
      'id' => 47,
      'cardId' => '68_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    391 => 
    array (
      'id' => 47,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 100,
    ),
    392 => 
    array (
      'id' => 47,
      'cardId' => '80_card',
      'cardLevel' => 4,
      'rate' => 100,
    ),
    393 => 
    array (
      'id' => 47,
      'cardId' => '45_card',
      'cardLevel' => 4,
      'rate' => 1000,
    ),
    394 => 
    array (
      'id' => 47,
      'cardId' => '70_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    395 => 
    array (
      'id' => 47,
      'cardId' => '76_card',
      'cardLevel' => 4,
      'rate' => 100,
    ),
    396 => 
    array (
      'id' => 47,
      'cardId' => '82_card',
      'cardLevel' => 4,
      'rate' => 100,
    ),
    397 => 
    array (
      'id' => 47,
      'cardId' => '84_card',
      'cardLevel' => 4,
      'rate' => 100,
    ),
    398 => 
    array (
      'id' => 47,
      'cardId' => '63_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    399 => 
    array (
      'id' => 47,
      'cardId' => '86_card',
      'cardLevel' => 4,
      'rate' => 100,
    ),
    400 => 
    array (
      'id' => 47,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 1300,
    ),
    401 => 
    array (
      'id' => 47,
      'cardId' => '158_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    402 => 
    array (
      'id' => 47,
      'cardId' => '42_card',
      'cardLevel' => 1,
      'rate' => 100,
    ),
    403 => 
    array (
      'id' => 47,
      'cardId' => '44_card',
      'cardLevel' => 1,
      'rate' => 100,
    ),
    404 => 
    array (
      'id' => 47,
      'cardId' => '46_card',
      'cardLevel' => 1,
      'rate' => 100,
    ),
    405 => 
    array (
      'id' => 48,
      'cardId' => '72_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    406 => 
    array (
      'id' => 48,
      'cardId' => '78_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    407 => 
    array (
      'id' => 48,
      'cardId' => '43_card',
      'cardLevel' => 6,
      'rate' => 1200,
    ),
    408 => 
    array (
      'id' => 48,
      'cardId' => '68_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    409 => 
    array (
      'id' => 48,
      'cardId' => '74_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    410 => 
    array (
      'id' => 48,
      'cardId' => '80_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    411 => 
    array (
      'id' => 48,
      'cardId' => '45_card',
      'cardLevel' => 6,
      'rate' => 1000,
    ),
    412 => 
    array (
      'id' => 48,
      'cardId' => '70_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    413 => 
    array (
      'id' => 48,
      'cardId' => '76_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    414 => 
    array (
      'id' => 48,
      'cardId' => '82_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    415 => 
    array (
      'id' => 48,
      'cardId' => '84_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    416 => 
    array (
      'id' => 48,
      'cardId' => '63_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    417 => 
    array (
      'id' => 48,
      'cardId' => '86_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    418 => 
    array (
      'id' => 48,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 1300,
    ),
    419 => 
    array (
      'id' => 48,
      'cardId' => '158_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    420 => 
    array (
      'id' => 48,
      'cardId' => '88_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    421 => 
    array (
      'id' => 48,
      'cardId' => '90_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    422 => 
    array (
      'id' => 48,
      'cardId' => '92_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    423 => 
    array (
      'id' => 49,
      'cardId' => '72_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    424 => 
    array (
      'id' => 49,
      'cardId' => '78_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    425 => 
    array (
      'id' => 49,
      'cardId' => '43_card',
      'cardLevel' => 6,
      'rate' => 1200,
    ),
    426 => 
    array (
      'id' => 49,
      'cardId' => '68_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    427 => 
    array (
      'id' => 49,
      'cardId' => '74_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    428 => 
    array (
      'id' => 49,
      'cardId' => '80_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    429 => 
    array (
      'id' => 49,
      'cardId' => '45_card',
      'cardLevel' => 6,
      'rate' => 1000,
    ),
    430 => 
    array (
      'id' => 49,
      'cardId' => '70_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    431 => 
    array (
      'id' => 49,
      'cardId' => '76_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    432 => 
    array (
      'id' => 49,
      'cardId' => '82_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    433 => 
    array (
      'id' => 49,
      'cardId' => '84_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    434 => 
    array (
      'id' => 49,
      'cardId' => '63_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    435 => 
    array (
      'id' => 49,
      'cardId' => '86_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    436 => 
    array (
      'id' => 49,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 1300,
    ),
    437 => 
    array (
      'id' => 49,
      'cardId' => '158_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    438 => 
    array (
      'id' => 49,
      'cardId' => '9_card',
      'cardLevel' => 1,
      'rate' => 100,
    ),
    439 => 
    array (
      'id' => 49,
      'cardId' => '5_card',
      'cardLevel' => 1,
      'rate' => 100,
    ),
    440 => 
    array (
      'id' => 49,
      'cardId' => '1_card',
      'cardLevel' => 1,
      'rate' => 100,
    ),
    441 => 
    array (
      'id' => 50,
      'cardId' => '72_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    442 => 
    array (
      'id' => 50,
      'cardId' => '78_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    443 => 
    array (
      'id' => 50,
      'cardId' => '43_card',
      'cardLevel' => 6,
      'rate' => 1200,
    ),
    444 => 
    array (
      'id' => 50,
      'cardId' => '68_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    445 => 
    array (
      'id' => 50,
      'cardId' => '74_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    446 => 
    array (
      'id' => 50,
      'cardId' => '80_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    447 => 
    array (
      'id' => 50,
      'cardId' => '45_card',
      'cardLevel' => 6,
      'rate' => 1000,
    ),
    448 => 
    array (
      'id' => 50,
      'cardId' => '70_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    449 => 
    array (
      'id' => 50,
      'cardId' => '76_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    450 => 
    array (
      'id' => 50,
      'cardId' => '82_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    451 => 
    array (
      'id' => 50,
      'cardId' => '84_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    452 => 
    array (
      'id' => 50,
      'cardId' => '63_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    453 => 
    array (
      'id' => 50,
      'cardId' => '86_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    454 => 
    array (
      'id' => 50,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 1300,
    ),
    455 => 
    array (
      'id' => 50,
      'cardId' => '158_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    456 => 
    array (
      'id' => 50,
      'cardId' => '128_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    457 => 
    array (
      'id' => 51,
      'cardId' => '72_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    458 => 
    array (
      'id' => 51,
      'cardId' => '78_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    459 => 
    array (
      'id' => 51,
      'cardId' => '43_card',
      'cardLevel' => 6,
      'rate' => 1200,
    ),
    460 => 
    array (
      'id' => 51,
      'cardId' => '68_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    461 => 
    array (
      'id' => 51,
      'cardId' => '74_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    462 => 
    array (
      'id' => 51,
      'cardId' => '80_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    463 => 
    array (
      'id' => 51,
      'cardId' => '45_card',
      'cardLevel' => 6,
      'rate' => 1000,
    ),
    464 => 
    array (
      'id' => 51,
      'cardId' => '70_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    465 => 
    array (
      'id' => 51,
      'cardId' => '76_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    466 => 
    array (
      'id' => 51,
      'cardId' => '82_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    467 => 
    array (
      'id' => 51,
      'cardId' => '84_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    468 => 
    array (
      'id' => 51,
      'cardId' => '63_card',
      'cardLevel' => 6,
      'rate' => 500,
    ),
    469 => 
    array (
      'id' => 51,
      'cardId' => '86_card',
      'cardLevel' => 6,
      'rate' => 100,
    ),
    470 => 
    array (
      'id' => 51,
      'cardId' => '157_card',
      'cardLevel' => 1,
      'rate' => 1300,
    ),
    471 => 
    array (
      'id' => 51,
      'cardId' => '158_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    472 => 
    array (
      'id' => 51,
      'cardId' => '128_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    473 => 
    array (
      'id' => 52,
      'cardId' => '52_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    474 => 
    array (
      'id' => 52,
      'cardId' => '67_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    475 => 
    array (
      'id' => 52,
      'cardId' => '72_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    476 => 
    array (
      'id' => 52,
      'cardId' => '78_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    477 => 
    array (
      'id' => 52,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    478 => 
    array (
      'id' => 52,
      'cardId' => '171_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    479 => 
    array (
      'id' => 52,
      'cardId' => '88_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    480 => 
    array (
      'id' => 53,
      'cardId' => '52_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    481 => 
    array (
      'id' => 53,
      'cardId' => '67_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    482 => 
    array (
      'id' => 53,
      'cardId' => '72_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    483 => 
    array (
      'id' => 53,
      'cardId' => '78_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    484 => 
    array (
      'id' => 53,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    485 => 
    array (
      'id' => 53,
      'cardId' => '171_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    486 => 
    array (
      'id' => 53,
      'cardId' => '98_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    487 => 
    array (
      'id' => 54,
      'cardId' => '52_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    488 => 
    array (
      'id' => 54,
      'cardId' => '67_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    489 => 
    array (
      'id' => 54,
      'cardId' => '72_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    490 => 
    array (
      'id' => 54,
      'cardId' => '78_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    491 => 
    array (
      'id' => 54,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    492 => 
    array (
      'id' => 54,
      'cardId' => '171_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    493 => 
    array (
      'id' => 55,
      'cardId' => '52_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    494 => 
    array (
      'id' => 55,
      'cardId' => '67_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    495 => 
    array (
      'id' => 55,
      'cardId' => '72_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    496 => 
    array (
      'id' => 55,
      'cardId' => '78_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    497 => 
    array (
      'id' => 55,
      'cardId' => '159_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    498 => 
    array (
      'id' => 55,
      'cardId' => '171_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    499 => 
    array (
      'id' => 55,
      'cardId' => '113_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    500 => 
    array (
      'id' => 55,
      'cardId' => '116_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    501 => 
    array (
      'id' => 56,
      'cardId' => '52_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    502 => 
    array (
      'id' => 56,
      'cardId' => '67_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    503 => 
    array (
      'id' => 56,
      'cardId' => '72_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    504 => 
    array (
      'id' => 56,
      'cardId' => '78_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    505 => 
    array (
      'id' => 56,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    506 => 
    array (
      'id' => 56,
      'cardId' => '171_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    507 => 
    array (
      'id' => 56,
      'cardId' => '2_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    508 => 
    array (
      'id' => 57,
      'cardId' => '55_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    509 => 
    array (
      'id' => 57,
      'cardId' => '69_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    510 => 
    array (
      'id' => 57,
      'cardId' => '74_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    511 => 
    array (
      'id' => 57,
      'cardId' => '80_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    512 => 
    array (
      'id' => 57,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    513 => 
    array (
      'id' => 57,
      'cardId' => '172_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    514 => 
    array (
      'id' => 57,
      'cardId' => '90_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    515 => 
    array (
      'id' => 58,
      'cardId' => '55_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    516 => 
    array (
      'id' => 58,
      'cardId' => '69_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    517 => 
    array (
      'id' => 58,
      'cardId' => '74_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    518 => 
    array (
      'id' => 58,
      'cardId' => '80_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    519 => 
    array (
      'id' => 58,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    520 => 
    array (
      'id' => 58,
      'cardId' => '172_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    521 => 
    array (
      'id' => 58,
      'cardId' => '101_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    522 => 
    array (
      'id' => 59,
      'cardId' => '55_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    523 => 
    array (
      'id' => 59,
      'cardId' => '69_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    524 => 
    array (
      'id' => 59,
      'cardId' => '74_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    525 => 
    array (
      'id' => 59,
      'cardId' => '80_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    526 => 
    array (
      'id' => 59,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    527 => 
    array (
      'id' => 59,
      'cardId' => '172_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    528 => 
    array (
      'id' => 60,
      'cardId' => '55_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    529 => 
    array (
      'id' => 60,
      'cardId' => '69_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    530 => 
    array (
      'id' => 60,
      'cardId' => '74_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    531 => 
    array (
      'id' => 60,
      'cardId' => '80_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    532 => 
    array (
      'id' => 60,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    533 => 
    array (
      'id' => 60,
      'cardId' => '172_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    534 => 
    array (
      'id' => 60,
      'cardId' => '116_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    535 => 
    array (
      'id' => 60,
      'cardId' => '119_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    536 => 
    array (
      'id' => 61,
      'cardId' => '55_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    537 => 
    array (
      'id' => 61,
      'cardId' => '69_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    538 => 
    array (
      'id' => 61,
      'cardId' => '74_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    539 => 
    array (
      'id' => 61,
      'cardId' => '80_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    540 => 
    array (
      'id' => 61,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    541 => 
    array (
      'id' => 61,
      'cardId' => '172_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    542 => 
    array (
      'id' => 61,
      'cardId' => '6_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    543 => 
    array (
      'id' => 62,
      'cardId' => '58_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    544 => 
    array (
      'id' => 62,
      'cardId' => '71_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    545 => 
    array (
      'id' => 62,
      'cardId' => '76_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    546 => 
    array (
      'id' => 62,
      'cardId' => '82_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    547 => 
    array (
      'id' => 62,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    548 => 
    array (
      'id' => 62,
      'cardId' => '173_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    549 => 
    array (
      'id' => 62,
      'cardId' => '92_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    550 => 
    array (
      'id' => 63,
      'cardId' => '58_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    551 => 
    array (
      'id' => 63,
      'cardId' => '71_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    552 => 
    array (
      'id' => 63,
      'cardId' => '76_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    553 => 
    array (
      'id' => 63,
      'cardId' => '82_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    554 => 
    array (
      'id' => 63,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    555 => 
    array (
      'id' => 63,
      'cardId' => '173_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    556 => 
    array (
      'id' => 63,
      'cardId' => '104_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    557 => 
    array (
      'id' => 64,
      'cardId' => '58_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    558 => 
    array (
      'id' => 64,
      'cardId' => '71_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    559 => 
    array (
      'id' => 64,
      'cardId' => '76_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    560 => 
    array (
      'id' => 64,
      'cardId' => '82_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    561 => 
    array (
      'id' => 64,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    562 => 
    array (
      'id' => 64,
      'cardId' => '173_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    563 => 
    array (
      'id' => 65,
      'cardId' => '58_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    564 => 
    array (
      'id' => 65,
      'cardId' => '71_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    565 => 
    array (
      'id' => 65,
      'cardId' => '76_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    566 => 
    array (
      'id' => 65,
      'cardId' => '82_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    567 => 
    array (
      'id' => 65,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    568 => 
    array (
      'id' => 65,
      'cardId' => '173_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    569 => 
    array (
      'id' => 65,
      'cardId' => '119_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    570 => 
    array (
      'id' => 65,
      'cardId' => '122_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    571 => 
    array (
      'id' => 66,
      'cardId' => '58_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    572 => 
    array (
      'id' => 66,
      'cardId' => '71_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    573 => 
    array (
      'id' => 66,
      'cardId' => '76_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    574 => 
    array (
      'id' => 66,
      'cardId' => '82_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    575 => 
    array (
      'id' => 66,
      'cardId' => '159_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    576 => 
    array (
      'id' => 66,
      'cardId' => '173_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    577 => 
    array (
      'id' => 66,
      'cardId' => '10_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    578 => 
    array (
      'id' => 67,
      'cardId' => '61_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    579 => 
    array (
      'id' => 67,
      'cardId' => '69_card',
      'cardLevel' => 3,
      'rate' => 1250,
    ),
    580 => 
    array (
      'id' => 67,
      'cardId' => '72_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    581 => 
    array (
      'id' => 67,
      'cardId' => '84_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    582 => 
    array (
      'id' => 67,
      'cardId' => '174_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    583 => 
    array (
      'id' => 67,
      'cardId' => '94_card',
      'cardLevel' => 1,
      'rate' => 800,
    ),
    584 => 
    array (
      'id' => 68,
      'cardId' => '61_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    585 => 
    array (
      'id' => 68,
      'cardId' => '69_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    586 => 
    array (
      'id' => 68,
      'cardId' => '72_card',
      'cardLevel' => 8,
      'rate' => 1500,
    ),
    587 => 
    array (
      'id' => 68,
      'cardId' => '84_card',
      'cardLevel' => 8,
      'rate' => 1500,
    ),
    588 => 
    array (
      'id' => 68,
      'cardId' => '174_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    589 => 
    array (
      'id' => 68,
      'cardId' => '107_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    590 => 
    array (
      'id' => 69,
      'cardId' => '61_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    591 => 
    array (
      'id' => 69,
      'cardId' => '69_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    592 => 
    array (
      'id' => 69,
      'cardId' => '72_card',
      'cardLevel' => 8,
      'rate' => 1500,
    ),
    593 => 
    array (
      'id' => 69,
      'cardId' => '84_card',
      'cardLevel' => 8,
      'rate' => 1500,
    ),
    594 => 
    array (
      'id' => 69,
      'cardId' => '174_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    595 => 
    array (
      'id' => 70,
      'cardId' => '61_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    596 => 
    array (
      'id' => 70,
      'cardId' => '69_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    597 => 
    array (
      'id' => 70,
      'cardId' => '72_card',
      'cardLevel' => 8,
      'rate' => 1500,
    ),
    598 => 
    array (
      'id' => 70,
      'cardId' => '84_card',
      'cardLevel' => 8,
      'rate' => 1500,
    ),
    599 => 
    array (
      'id' => 70,
      'cardId' => '174_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    600 => 
    array (
      'id' => 70,
      'cardId' => '113_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    601 => 
    array (
      'id' => 70,
      'cardId' => '122_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    602 => 
    array (
      'id' => 71,
      'cardId' => '61_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    603 => 
    array (
      'id' => 71,
      'cardId' => '69_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    604 => 
    array (
      'id' => 71,
      'cardId' => '72_card',
      'cardLevel' => 8,
      'rate' => 1500,
    ),
    605 => 
    array (
      'id' => 71,
      'cardId' => '84_card',
      'cardLevel' => 8,
      'rate' => 1500,
    ),
    606 => 
    array (
      'id' => 71,
      'cardId' => '174_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    607 => 
    array (
      'id' => 71,
      'cardId' => '14_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    608 => 
    array (
      'id' => 72,
      'cardId' => '64_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    609 => 
    array (
      'id' => 72,
      'cardId' => '67_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    610 => 
    array (
      'id' => 72,
      'cardId' => '74_card',
      'cardLevel' => 10,
      'rate' => 1500,
    ),
    611 => 
    array (
      'id' => 72,
      'cardId' => '86_card',
      'cardLevel' => 10,
      'rate' => 1500,
    ),
    612 => 
    array (
      'id' => 72,
      'cardId' => '175_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    613 => 
    array (
      'id' => 72,
      'cardId' => '96_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    614 => 
    array (
      'id' => 73,
      'cardId' => '64_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    615 => 
    array (
      'id' => 73,
      'cardId' => '67_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    616 => 
    array (
      'id' => 73,
      'cardId' => '74_card',
      'cardLevel' => 10,
      'rate' => 1500,
    ),
    617 => 
    array (
      'id' => 73,
      'cardId' => '86_card',
      'cardLevel' => 10,
      'rate' => 1500,
    ),
    618 => 
    array (
      'id' => 73,
      'cardId' => '175_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    619 => 
    array (
      'id' => 73,
      'cardId' => '110_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    620 => 
    array (
      'id' => 74,
      'cardId' => '64_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    621 => 
    array (
      'id' => 74,
      'cardId' => '67_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    622 => 
    array (
      'id' => 74,
      'cardId' => '74_card',
      'cardLevel' => 10,
      'rate' => 1500,
    ),
    623 => 
    array (
      'id' => 74,
      'cardId' => '86_card',
      'cardLevel' => 10,
      'rate' => 1500,
    ),
    624 => 
    array (
      'id' => 74,
      'cardId' => '175_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    625 => 
    array (
      'id' => 75,
      'cardId' => '64_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    626 => 
    array (
      'id' => 75,
      'cardId' => '67_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    627 => 
    array (
      'id' => 75,
      'cardId' => '74_card',
      'cardLevel' => 10,
      'rate' => 1500,
    ),
    628 => 
    array (
      'id' => 75,
      'cardId' => '86_card',
      'cardLevel' => 10,
      'rate' => 1500,
    ),
    629 => 
    array (
      'id' => 75,
      'cardId' => '175_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    630 => 
    array (
      'id' => 75,
      'cardId' => '116_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    631 => 
    array (
      'id' => 75,
      'cardId' => '125_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    632 => 
    array (
      'id' => 76,
      'cardId' => '64_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    633 => 
    array (
      'id' => 76,
      'cardId' => '67_card',
      'cardLevel' => 3,
      'rate' => 1500,
    ),
    634 => 
    array (
      'id' => 76,
      'cardId' => '74_card',
      'cardLevel' => 10,
      'rate' => 1500,
    ),
    635 => 
    array (
      'id' => 76,
      'cardId' => '86_card',
      'cardLevel' => 10,
      'rate' => 1500,
    ),
    636 => 
    array (
      'id' => 76,
      'cardId' => '175_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    637 => 
    array (
      'id' => 76,
      'cardId' => '18_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    638 => 
    array (
      'id' => 77,
      'cardId' => '52_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    639 => 
    array (
      'id' => 77,
      'cardId' => '67_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    640 => 
    array (
      'id' => 77,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    641 => 
    array (
      'id' => 77,
      'cardId' => '78_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    642 => 
    array (
      'id' => 77,
      'cardId' => '55_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    643 => 
    array (
      'id' => 77,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    644 => 
    array (
      'id' => 77,
      'cardId' => '80_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    645 => 
    array (
      'id' => 77,
      'cardId' => '58_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    646 => 
    array (
      'id' => 77,
      'cardId' => '71_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    647 => 
    array (
      'id' => 77,
      'cardId' => '76_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    648 => 
    array (
      'id' => 77,
      'cardId' => '82_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    649 => 
    array (
      'id' => 77,
      'cardId' => '61_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    650 => 
    array (
      'id' => 77,
      'cardId' => '84_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    651 => 
    array (
      'id' => 77,
      'cardId' => '64_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    652 => 
    array (
      'id' => 77,
      'cardId' => '86_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    653 => 
    array (
      'id' => 77,
      'cardId' => '182_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    654 => 
    array (
      'id' => 77,
      'cardId' => '94_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    655 => 
    array (
      'id' => 77,
      'cardId' => '96_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    656 => 
    array (
      'id' => 78,
      'cardId' => '52_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    657 => 
    array (
      'id' => 78,
      'cardId' => '67_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    658 => 
    array (
      'id' => 78,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    659 => 
    array (
      'id' => 78,
      'cardId' => '78_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    660 => 
    array (
      'id' => 78,
      'cardId' => '55_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    661 => 
    array (
      'id' => 78,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    662 => 
    array (
      'id' => 78,
      'cardId' => '80_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    663 => 
    array (
      'id' => 78,
      'cardId' => '58_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    664 => 
    array (
      'id' => 78,
      'cardId' => '71_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    665 => 
    array (
      'id' => 78,
      'cardId' => '76_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    666 => 
    array (
      'id' => 78,
      'cardId' => '82_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    667 => 
    array (
      'id' => 78,
      'cardId' => '61_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    668 => 
    array (
      'id' => 78,
      'cardId' => '84_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    669 => 
    array (
      'id' => 78,
      'cardId' => '64_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    670 => 
    array (
      'id' => 78,
      'cardId' => '86_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    671 => 
    array (
      'id' => 78,
      'cardId' => '182_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    672 => 
    array (
      'id' => 78,
      'cardId' => '98_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    673 => 
    array (
      'id' => 78,
      'cardId' => '110_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    674 => 
    array (
      'id' => 79,
      'cardId' => '52_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    675 => 
    array (
      'id' => 79,
      'cardId' => '67_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    676 => 
    array (
      'id' => 79,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    677 => 
    array (
      'id' => 79,
      'cardId' => '78_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    678 => 
    array (
      'id' => 79,
      'cardId' => '55_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    679 => 
    array (
      'id' => 79,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    680 => 
    array (
      'id' => 79,
      'cardId' => '80_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    681 => 
    array (
      'id' => 79,
      'cardId' => '58_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    682 => 
    array (
      'id' => 79,
      'cardId' => '71_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    683 => 
    array (
      'id' => 79,
      'cardId' => '76_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    684 => 
    array (
      'id' => 79,
      'cardId' => '82_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    685 => 
    array (
      'id' => 79,
      'cardId' => '61_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    686 => 
    array (
      'id' => 79,
      'cardId' => '84_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    687 => 
    array (
      'id' => 79,
      'cardId' => '64_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    688 => 
    array (
      'id' => 79,
      'cardId' => '86_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    689 => 
    array (
      'id' => 79,
      'cardId' => '182_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    690 => 
    array (
      'id' => 80,
      'cardId' => '52_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    691 => 
    array (
      'id' => 80,
      'cardId' => '67_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    692 => 
    array (
      'id' => 80,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    693 => 
    array (
      'id' => 80,
      'cardId' => '78_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    694 => 
    array (
      'id' => 80,
      'cardId' => '55_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    695 => 
    array (
      'id' => 80,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    696 => 
    array (
      'id' => 80,
      'cardId' => '80_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    697 => 
    array (
      'id' => 80,
      'cardId' => '58_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    698 => 
    array (
      'id' => 80,
      'cardId' => '71_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    699 => 
    array (
      'id' => 80,
      'cardId' => '76_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    700 => 
    array (
      'id' => 80,
      'cardId' => '82_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    701 => 
    array (
      'id' => 80,
      'cardId' => '61_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    702 => 
    array (
      'id' => 80,
      'cardId' => '84_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    703 => 
    array (
      'id' => 80,
      'cardId' => '64_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    704 => 
    array (
      'id' => 80,
      'cardId' => '86_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    705 => 
    array (
      'id' => 80,
      'cardId' => '182_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    706 => 
    array (
      'id' => 81,
      'cardId' => '52_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    707 => 
    array (
      'id' => 81,
      'cardId' => '67_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    708 => 
    array (
      'id' => 81,
      'cardId' => '72_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    709 => 
    array (
      'id' => 81,
      'cardId' => '78_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    710 => 
    array (
      'id' => 81,
      'cardId' => '55_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    711 => 
    array (
      'id' => 81,
      'cardId' => '74_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    712 => 
    array (
      'id' => 81,
      'cardId' => '80_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    713 => 
    array (
      'id' => 81,
      'cardId' => '58_card',
      'cardLevel' => 4,
      'rate' => 500,
    ),
    714 => 
    array (
      'id' => 81,
      'cardId' => '71_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    715 => 
    array (
      'id' => 81,
      'cardId' => '76_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    716 => 
    array (
      'id' => 81,
      'cardId' => '82_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    717 => 
    array (
      'id' => 81,
      'cardId' => '61_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    718 => 
    array (
      'id' => 81,
      'cardId' => '84_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    719 => 
    array (
      'id' => 81,
      'cardId' => '64_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    720 => 
    array (
      'id' => 81,
      'cardId' => '86_card',
      'cardLevel' => 10,
      'rate' => 500,
    ),
    721 => 
    array (
      'id' => 81,
      'cardId' => '182_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    722 => 
    array (
      'id' => 82,
      'cardId' => '52_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    723 => 
    array (
      'id' => 82,
      'cardId' => '67_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    724 => 
    array (
      'id' => 82,
      'cardId' => '73_card',
      'cardLevel' => 3,
      'rate' => 1150,
    ),
    725 => 
    array (
      'id' => 82,
      'cardId' => '79_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    726 => 
    array (
      'id' => 82,
      'cardId' => '202_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    727 => 
    array (
      'id' => 82,
      'cardId' => '61_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    728 => 
    array (
      'id' => 82,
      'cardId' => '85_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    729 => 
    array (
      'id' => 82,
      'cardId' => '208_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    730 => 
    array (
      'id' => 83,
      'cardId' => '52_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    731 => 
    array (
      'id' => 83,
      'cardId' => '67_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    732 => 
    array (
      'id' => 83,
      'cardId' => '73_card',
      'cardLevel' => 3,
      'rate' => 1150,
    ),
    733 => 
    array (
      'id' => 83,
      'cardId' => '79_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    734 => 
    array (
      'id' => 83,
      'cardId' => '202_card',
      'cardLevel' => 3,
      'rate' => 1150,
    ),
    735 => 
    array (
      'id' => 83,
      'cardId' => '61_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    736 => 
    array (
      'id' => 83,
      'cardId' => '85_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    737 => 
    array (
      'id' => 83,
      'cardId' => '208_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    738 => 
    array (
      'id' => 83,
      'cardId' => '209_card',
      'cardLevel' => 3,
      'rate' => 75,
    ),
    739 => 
    array (
      'id' => 84,
      'cardId' => '52_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    740 => 
    array (
      'id' => 84,
      'cardId' => '67_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    741 => 
    array (
      'id' => 84,
      'cardId' => '73_card',
      'cardLevel' => 3,
      'rate' => 1150,
    ),
    742 => 
    array (
      'id' => 84,
      'cardId' => '79_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    743 => 
    array (
      'id' => 84,
      'cardId' => '202_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    744 => 
    array (
      'id' => 84,
      'cardId' => '61_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    745 => 
    array (
      'id' => 84,
      'cardId' => '85_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    746 => 
    array (
      'id' => 84,
      'cardId' => '208_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    747 => 
    array (
      'id' => 84,
      'cardId' => '113_card',
      'cardLevel' => 3,
      'rate' => 75,
    ),
    748 => 
    array (
      'id' => 84,
      'cardId' => '122_card',
      'cardLevel' => 3,
      'rate' => 75,
    ),
    749 => 
    array (
      'id' => 85,
      'cardId' => '52_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    750 => 
    array (
      'id' => 85,
      'cardId' => '67_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    751 => 
    array (
      'id' => 85,
      'cardId' => '73_card',
      'cardLevel' => 3,
      'rate' => 1150,
    ),
    752 => 
    array (
      'id' => 85,
      'cardId' => '79_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    753 => 
    array (
      'id' => 85,
      'cardId' => '202_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    754 => 
    array (
      'id' => 85,
      'cardId' => '61_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    755 => 
    array (
      'id' => 85,
      'cardId' => '85_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    756 => 
    array (
      'id' => 85,
      'cardId' => '208_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    757 => 
    array (
      'id' => 85,
      'cardId' => '98_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    758 => 
    array (
      'id' => 85,
      'cardId' => '107_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    759 => 
    array (
      'id' => 86,
      'cardId' => '52_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    760 => 
    array (
      'id' => 86,
      'cardId' => '67_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    761 => 
    array (
      'id' => 86,
      'cardId' => '73_card',
      'cardLevel' => 3,
      'rate' => 1150,
    ),
    762 => 
    array (
      'id' => 86,
      'cardId' => '79_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    763 => 
    array (
      'id' => 86,
      'cardId' => '202_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    764 => 
    array (
      'id' => 86,
      'cardId' => '61_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    765 => 
    array (
      'id' => 86,
      'cardId' => '85_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    766 => 
    array (
      'id' => 86,
      'cardId' => '208_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    767 => 
    array (
      'id' => 87,
      'cardId' => '55_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    768 => 
    array (
      'id' => 87,
      'cardId' => '69_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    769 => 
    array (
      'id' => 87,
      'cardId' => '75_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    770 => 
    array (
      'id' => 87,
      'cardId' => '81_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    771 => 
    array (
      'id' => 87,
      'cardId' => '204_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    772 => 
    array (
      'id' => 87,
      'cardId' => '64_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    773 => 
    array (
      'id' => 87,
      'cardId' => '87_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    774 => 
    array (
      'id' => 87,
      'cardId' => '210_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    775 => 
    array (
      'id' => 88,
      'cardId' => '55_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    776 => 
    array (
      'id' => 88,
      'cardId' => '69_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    777 => 
    array (
      'id' => 88,
      'cardId' => '75_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    778 => 
    array (
      'id' => 88,
      'cardId' => '81_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    779 => 
    array (
      'id' => 88,
      'cardId' => '204_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    780 => 
    array (
      'id' => 88,
      'cardId' => '64_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    781 => 
    array (
      'id' => 88,
      'cardId' => '87_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    782 => 
    array (
      'id' => 88,
      'cardId' => '210_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    783 => 
    array (
      'id' => 88,
      'cardId' => '211_card',
      'cardLevel' => 3,
      'rate' => 75,
    ),
    784 => 
    array (
      'id' => 89,
      'cardId' => '55_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    785 => 
    array (
      'id' => 89,
      'cardId' => '69_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    786 => 
    array (
      'id' => 89,
      'cardId' => '75_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    787 => 
    array (
      'id' => 89,
      'cardId' => '81_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    788 => 
    array (
      'id' => 89,
      'cardId' => '204_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    789 => 
    array (
      'id' => 89,
      'cardId' => '64_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    790 => 
    array (
      'id' => 89,
      'cardId' => '87_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    791 => 
    array (
      'id' => 89,
      'cardId' => '210_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    792 => 
    array (
      'id' => 89,
      'cardId' => '116_card',
      'cardLevel' => 3,
      'rate' => 75,
    ),
    793 => 
    array (
      'id' => 89,
      'cardId' => '125_card',
      'cardLevel' => 3,
      'rate' => 75,
    ),
    794 => 
    array (
      'id' => 90,
      'cardId' => '55_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    795 => 
    array (
      'id' => 90,
      'cardId' => '69_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    796 => 
    array (
      'id' => 90,
      'cardId' => '75_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    797 => 
    array (
      'id' => 90,
      'cardId' => '81_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    798 => 
    array (
      'id' => 90,
      'cardId' => '204_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    799 => 
    array (
      'id' => 90,
      'cardId' => '64_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    800 => 
    array (
      'id' => 90,
      'cardId' => '87_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    801 => 
    array (
      'id' => 90,
      'cardId' => '210_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    802 => 
    array (
      'id' => 90,
      'cardId' => '101_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    803 => 
    array (
      'id' => 90,
      'cardId' => '110_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    804 => 
    array (
      'id' => 91,
      'cardId' => '55_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    805 => 
    array (
      'id' => 91,
      'cardId' => '69_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    806 => 
    array (
      'id' => 91,
      'cardId' => '75_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    807 => 
    array (
      'id' => 91,
      'cardId' => '81_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    808 => 
    array (
      'id' => 91,
      'cardId' => '204_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    809 => 
    array (
      'id' => 91,
      'cardId' => '64_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    810 => 
    array (
      'id' => 91,
      'cardId' => '87_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    811 => 
    array (
      'id' => 91,
      'cardId' => '210_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    812 => 
    array (
      'id' => 92,
      'cardId' => '55_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    813 => 
    array (
      'id' => 92,
      'cardId' => '69_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    814 => 
    array (
      'id' => 92,
      'cardId' => '75_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    815 => 
    array (
      'id' => 92,
      'cardId' => '81_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    816 => 
    array (
      'id' => 92,
      'cardId' => '204_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    817 => 
    array (
      'id' => 92,
      'cardId' => '64_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    818 => 
    array (
      'id' => 92,
      'cardId' => '87_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    819 => 
    array (
      'id' => 92,
      'cardId' => '210_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    820 => 
    array (
      'id' => 92,
      'cardId' => '71_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    821 => 
    array (
      'id' => 92,
      'cardId' => '83_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    822 => 
    array (
      'id' => 93,
      'cardId' => '55_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    823 => 
    array (
      'id' => 93,
      'cardId' => '69_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    824 => 
    array (
      'id' => 93,
      'cardId' => '75_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    825 => 
    array (
      'id' => 93,
      'cardId' => '81_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    826 => 
    array (
      'id' => 93,
      'cardId' => '204_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    827 => 
    array (
      'id' => 93,
      'cardId' => '64_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    828 => 
    array (
      'id' => 93,
      'cardId' => '87_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    829 => 
    array (
      'id' => 93,
      'cardId' => '210_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    830 => 
    array (
      'id' => 93,
      'cardId' => '71_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    831 => 
    array (
      'id' => 93,
      'cardId' => '83_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    832 => 
    array (
      'id' => 93,
      'cardId' => '207_card',
      'cardLevel' => 3,
      'rate' => 150,
    ),
    833 => 
    array (
      'id' => 94,
      'cardId' => '55_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    834 => 
    array (
      'id' => 94,
      'cardId' => '69_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    835 => 
    array (
      'id' => 94,
      'cardId' => '75_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    836 => 
    array (
      'id' => 94,
      'cardId' => '81_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    837 => 
    array (
      'id' => 94,
      'cardId' => '204_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    838 => 
    array (
      'id' => 94,
      'cardId' => '64_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    839 => 
    array (
      'id' => 94,
      'cardId' => '87_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    840 => 
    array (
      'id' => 94,
      'cardId' => '210_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    841 => 
    array (
      'id' => 94,
      'cardId' => '71_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    842 => 
    array (
      'id' => 94,
      'cardId' => '83_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    843 => 
    array (
      'id' => 94,
      'cardId' => '113_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    844 => 
    array (
      'id' => 94,
      'cardId' => '116_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    845 => 
    array (
      'id' => 94,
      'cardId' => '119_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    846 => 
    array (
      'id' => 95,
      'cardId' => '55_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    847 => 
    array (
      'id' => 95,
      'cardId' => '69_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    848 => 
    array (
      'id' => 95,
      'cardId' => '75_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    849 => 
    array (
      'id' => 95,
      'cardId' => '81_card',
      'cardLevel' => 3,
      'rate' => 650,
    ),
    850 => 
    array (
      'id' => 95,
      'cardId' => '204_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    851 => 
    array (
      'id' => 95,
      'cardId' => '64_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    852 => 
    array (
      'id' => 95,
      'cardId' => '87_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    853 => 
    array (
      'id' => 95,
      'cardId' => '210_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    854 => 
    array (
      'id' => 95,
      'cardId' => '71_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    855 => 
    array (
      'id' => 95,
      'cardId' => '83_card',
      'cardLevel' => 3,
      'rate' => 450,
    ),
    856 => 
    array (
      'id' => 95,
      'cardId' => '98_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    857 => 
    array (
      'id' => 95,
      'cardId' => '104_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    858 => 
    array (
      'id' => 96,
      'cardId' => '55_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    859 => 
    array (
      'id' => 96,
      'cardId' => '69_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    860 => 
    array (
      'id' => 96,
      'cardId' => '75_card',
      'cardLevel' => 5,
      'rate' => 650,
    ),
    861 => 
    array (
      'id' => 96,
      'cardId' => '81_card',
      'cardLevel' => 5,
      'rate' => 650,
    ),
    862 => 
    array (
      'id' => 96,
      'cardId' => '204_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    863 => 
    array (
      'id' => 96,
      'cardId' => '64_card',
      'cardLevel' => 7,
      'rate' => 1150,
    ),
    864 => 
    array (
      'id' => 96,
      'cardId' => '87_card',
      'cardLevel' => 5,
      'rate' => 450,
    ),
    865 => 
    array (
      'id' => 96,
      'cardId' => '210_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    866 => 
    array (
      'id' => 96,
      'cardId' => '71_card',
      'cardLevel' => 7,
      'rate' => 750,
    ),
    867 => 
    array (
      'id' => 96,
      'cardId' => '83_card',
      'cardLevel' => 5,
      'rate' => 450,
    ),
    868 => 
    array (
      'id' => 97,
      'cardId' => '58_card',
      'cardLevel' => 7,
      'rate' => 1500,
    ),
    869 => 
    array (
      'id' => 97,
      'cardId' => '73_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    870 => 
    array (
      'id' => 97,
      'cardId' => '75_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    871 => 
    array (
      'id' => 97,
      'cardId' => '77_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    872 => 
    array (
      'id' => 97,
      'cardId' => '79_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    873 => 
    array (
      'id' => 97,
      'cardId' => '81_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    874 => 
    array (
      'id' => 97,
      'cardId' => '83_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    875 => 
    array (
      'id' => 97,
      'cardId' => '85_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    876 => 
    array (
      'id' => 97,
      'cardId' => '87_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    877 => 
    array (
      'id' => 97,
      'cardId' => '113_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    878 => 
    array (
      'id' => 97,
      'cardId' => '119_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    879 => 
    array (
      'id' => 97,
      'cardId' => '116_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    880 => 
    array (
      'id' => 98,
      'cardId' => '58_card',
      'cardLevel' => 7,
      'rate' => 1500,
    ),
    881 => 
    array (
      'id' => 98,
      'cardId' => '73_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    882 => 
    array (
      'id' => 98,
      'cardId' => '75_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    883 => 
    array (
      'id' => 98,
      'cardId' => '77_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    884 => 
    array (
      'id' => 98,
      'cardId' => '79_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    885 => 
    array (
      'id' => 98,
      'cardId' => '81_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    886 => 
    array (
      'id' => 98,
      'cardId' => '83_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    887 => 
    array (
      'id' => 98,
      'cardId' => '85_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    888 => 
    array (
      'id' => 98,
      'cardId' => '87_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    889 => 
    array (
      'id' => 99,
      'cardId' => '58_card',
      'cardLevel' => 7,
      'rate' => 1500,
    ),
    890 => 
    array (
      'id' => 99,
      'cardId' => '73_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    891 => 
    array (
      'id' => 99,
      'cardId' => '75_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    892 => 
    array (
      'id' => 99,
      'cardId' => '77_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    893 => 
    array (
      'id' => 99,
      'cardId' => '79_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    894 => 
    array (
      'id' => 99,
      'cardId' => '81_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    895 => 
    array (
      'id' => 99,
      'cardId' => '83_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    896 => 
    array (
      'id' => 99,
      'cardId' => '85_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    897 => 
    array (
      'id' => 99,
      'cardId' => '87_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    898 => 
    array (
      'id' => 99,
      'cardId' => '94_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    899 => 
    array (
      'id' => 99,
      'cardId' => '96_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    900 => 
    array (
      'id' => 100,
      'cardId' => '58_card',
      'cardLevel' => 7,
      'rate' => 1500,
    ),
    901 => 
    array (
      'id' => 100,
      'cardId' => '73_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    902 => 
    array (
      'id' => 100,
      'cardId' => '75_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    903 => 
    array (
      'id' => 100,
      'cardId' => '77_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    904 => 
    array (
      'id' => 100,
      'cardId' => '79_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    905 => 
    array (
      'id' => 100,
      'cardId' => '81_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    906 => 
    array (
      'id' => 100,
      'cardId' => '83_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    907 => 
    array (
      'id' => 100,
      'cardId' => '85_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    908 => 
    array (
      'id' => 100,
      'cardId' => '87_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    909 => 
    array (
      'id' => 100,
      'cardId' => '107_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    910 => 
    array (
      'id' => 100,
      'cardId' => '110_card',
      'cardLevel' => 1,
      'rate' => 75,
    ),
    911 => 
    array (
      'id' => 101,
      'cardId' => '58_card',
      'cardLevel' => 7,
      'rate' => 1500,
    ),
    912 => 
    array (
      'id' => 101,
      'cardId' => '73_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    913 => 
    array (
      'id' => 101,
      'cardId' => '75_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    914 => 
    array (
      'id' => 101,
      'cardId' => '77_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    915 => 
    array (
      'id' => 101,
      'cardId' => '79_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    916 => 
    array (
      'id' => 101,
      'cardId' => '81_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    917 => 
    array (
      'id' => 101,
      'cardId' => '83_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    918 => 
    array (
      'id' => 101,
      'cardId' => '85_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    919 => 
    array (
      'id' => 101,
      'cardId' => '87_card',
      'cardLevel' => 5,
      'rate' => 750,
    ),
    920 => 
    array (
      'id' => 102,
      'cardId' => '160_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    921 => 
    array (
      'id' => 102,
      'cardId' => '161_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    922 => 
    array (
      'id' => 102,
      'cardId' => '162_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    923 => 
    array (
      'id' => 102,
      'cardId' => '163_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    924 => 
    array (
      'id' => 102,
      'cardId' => '164_card',
      'cardLevel' => 1,
      'rate' => 1750,
    ),
    925 => 
    array (
      'id' => 102,
      'cardId' => '165_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    926 => 
    array (
      'id' => 103,
      'cardId' => '41_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    927 => 
    array (
      'id' => 103,
      'cardId' => '51_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    928 => 
    array (
      'id' => 103,
      'cardId' => '66_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    929 => 
    array (
      'id' => 103,
      'cardId' => '72_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    930 => 
    array (
      'id' => 103,
      'cardId' => '78_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    931 => 
    array (
      'id' => 103,
      'cardId' => '160_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    932 => 
    array (
      'id' => 103,
      'cardId' => '43_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    933 => 
    array (
      'id' => 103,
      'cardId' => '54_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    934 => 
    array (
      'id' => 103,
      'cardId' => '68_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    935 => 
    array (
      'id' => 103,
      'cardId' => '74_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    936 => 
    array (
      'id' => 103,
      'cardId' => '80_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    937 => 
    array (
      'id' => 103,
      'cardId' => '161_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    938 => 
    array (
      'id' => 103,
      'cardId' => '45_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    939 => 
    array (
      'id' => 103,
      'cardId' => '57_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    940 => 
    array (
      'id' => 103,
      'cardId' => '70_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    941 => 
    array (
      'id' => 103,
      'cardId' => '76_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    942 => 
    array (
      'id' => 103,
      'cardId' => '82_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    943 => 
    array (
      'id' => 103,
      'cardId' => '162_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    944 => 
    array (
      'id' => 103,
      'cardId' => '42_card',
      'cardLevel' => 2,
      'rate' => 75,
    ),
    945 => 
    array (
      'id' => 103,
      'cardId' => '44_card',
      'cardLevel' => 2,
      'rate' => 75,
    ),
    946 => 
    array (
      'id' => 103,
      'cardId' => '46_card',
      'cardLevel' => 2,
      'rate' => 75,
    ),
    947 => 
    array (
      'id' => 104,
      'cardId' => '41_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    948 => 
    array (
      'id' => 104,
      'cardId' => '51_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    949 => 
    array (
      'id' => 104,
      'cardId' => '66_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    950 => 
    array (
      'id' => 104,
      'cardId' => '72_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    951 => 
    array (
      'id' => 104,
      'cardId' => '78_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    952 => 
    array (
      'id' => 104,
      'cardId' => '166_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    953 => 
    array (
      'id' => 104,
      'cardId' => '43_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    954 => 
    array (
      'id' => 104,
      'cardId' => '54_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    955 => 
    array (
      'id' => 104,
      'cardId' => '68_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    956 => 
    array (
      'id' => 104,
      'cardId' => '74_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    957 => 
    array (
      'id' => 104,
      'cardId' => '80_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    958 => 
    array (
      'id' => 104,
      'cardId' => '167_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    959 => 
    array (
      'id' => 104,
      'cardId' => '45_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    960 => 
    array (
      'id' => 104,
      'cardId' => '57_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    961 => 
    array (
      'id' => 104,
      'cardId' => '70_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    962 => 
    array (
      'id' => 104,
      'cardId' => '76_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    963 => 
    array (
      'id' => 104,
      'cardId' => '82_card',
      'cardLevel' => 2,
      'rate' => 300,
    ),
    964 => 
    array (
      'id' => 104,
      'cardId' => '168_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    965 => 
    array (
      'id' => 104,
      'cardId' => '88_card',
      'cardLevel' => 3,
      'rate' => 75,
    ),
    966 => 
    array (
      'id' => 104,
      'cardId' => '90_card',
      'cardLevel' => 3,
      'rate' => 75,
    ),
    967 => 
    array (
      'id' => 104,
      'cardId' => '92_card',
      'cardLevel' => 3,
      'rate' => 75,
    ),
    968 => 
    array (
      'id' => 105,
      'cardId' => '52_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    969 => 
    array (
      'id' => 105,
      'cardId' => '67_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    970 => 
    array (
      'id' => 105,
      'cardId' => '72_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    971 => 
    array (
      'id' => 105,
      'cardId' => '78_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    972 => 
    array (
      'id' => 105,
      'cardId' => '171_card',
      'cardLevel' => 3,
      'rate' => 2000,
    ),
    973 => 
    array (
      'id' => 105,
      'cardId' => '55_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    974 => 
    array (
      'id' => 105,
      'cardId' => '69_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    975 => 
    array (
      'id' => 105,
      'cardId' => '74_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    976 => 
    array (
      'id' => 105,
      'cardId' => '80_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    977 => 
    array (
      'id' => 105,
      'cardId' => '58_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    978 => 
    array (
      'id' => 105,
      'cardId' => '71_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    979 => 
    array (
      'id' => 105,
      'cardId' => '76_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    980 => 
    array (
      'id' => 105,
      'cardId' => '82_card',
      'cardLevel' => 2,
      'rate' => 500,
    ),
    981 => 
    array (
      'id' => 105,
      'cardId' => '132_card',
      'cardLevel' => 4,
      'rate' => 75,
    ),
    982 => 
    array (
      'id' => 105,
      'cardId' => '136_card',
      'cardLevel' => 4,
      'rate' => 75,
    ),
    983 => 
    array (
      'id' => 105,
      'cardId' => '134_card',
      'cardLevel' => 4,
      'rate' => 75,
    ),
    984 => 
    array (
      'id' => 106,
      'cardId' => '166_card',
      'cardLevel' => 1,
      'rate' => 1500,
    ),
    985 => 
    array (
      'id' => 106,
      'cardId' => '167_card',
      'cardLevel' => 1,
      'rate' => 1500,
    ),
    986 => 
    array (
      'id' => 106,
      'cardId' => '168_card',
      'cardLevel' => 1,
      'rate' => 1500,
    ),
    987 => 
    array (
      'id' => 106,
      'cardId' => '169_card',
      'cardLevel' => 1,
      'rate' => 1500,
    ),
    988 => 
    array (
      'id' => 106,
      'cardId' => '170_card',
      'cardLevel' => 1,
      'rate' => 1500,
    ),
    989 => 
    array (
      'id' => 107,
      'cardId' => '197_card',
      'cardLevel' => 5,
      'rate' => 1125,
    ),
    990 => 
    array (
      'id' => 107,
      'cardId' => '198_card',
      'cardLevel' => 5,
      'rate' => 1125,
    ),
    991 => 
    array (
      'id' => 107,
      'cardId' => '199_card',
      'cardLevel' => 5,
      'rate' => 1125,
    ),
    992 => 
    array (
      'id' => 107,
      'cardId' => '200_card',
      'cardLevel' => 5,
      'rate' => 1125,
    ),
    993 => 
    array (
      'id' => 107,
      'cardId' => '201_card',
      'cardLevel' => 5,
      'rate' => 1125,
    ),
    994 => 
    array (
      'id' => 108,
      'cardId' => '78_card',
      'cardLevel' => 2,
      'rate' => 1200,
    ),
    995 => 
    array (
      'id' => 108,
      'cardId' => '84_card',
      'cardLevel' => 2,
      'rate' => 1200,
    ),
    996 => 
    array (
      'id' => 108,
      'cardId' => '86_card',
      'cardLevel' => 2,
      'rate' => 1200,
    ),
    997 => 
    array (
      'id' => 108,
      'cardId' => '155_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    998 => 
    array (
      'id' => 108,
      'cardId' => '156_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    999 => 
    array (
      'id' => 108,
      'cardId' => '152_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    1000 => 
    array (
      'id' => 109,
      'cardId' => '80_card',
      'cardLevel' => 2,
      'rate' => 1200,
    ),
    1001 => 
    array (
      'id' => 109,
      'cardId' => '84_card',
      'cardLevel' => 2,
      'rate' => 1200,
    ),
    1002 => 
    array (
      'id' => 109,
      'cardId' => '86_card',
      'cardLevel' => 2,
      'rate' => 1200,
    ),
    1003 => 
    array (
      'id' => 109,
      'cardId' => '155_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    1004 => 
    array (
      'id' => 109,
      'cardId' => '156_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    1005 => 
    array (
      'id' => 109,
      'cardId' => '153_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    1006 => 
    array (
      'id' => 110,
      'cardId' => '82_card',
      'cardLevel' => 2,
      'rate' => 1200,
    ),
    1007 => 
    array (
      'id' => 110,
      'cardId' => '84_card',
      'cardLevel' => 2,
      'rate' => 1200,
    ),
    1008 => 
    array (
      'id' => 110,
      'cardId' => '86_card',
      'cardLevel' => 4,
      'rate' => 1200,
    ),
    1009 => 
    array (
      'id' => 110,
      'cardId' => '155_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    1010 => 
    array (
      'id' => 110,
      'cardId' => '156_card',
      'cardLevel' => 3,
      'rate' => 750,
    ),
    1011 => 
    array (
      'id' => 110,
      'cardId' => '154_card',
      'cardLevel' => 4,
      'rate' => 750,
    ),
    1012 => 
    array (
      'id' => 111,
      'cardId' => '66_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    1013 => 
    array (
      'id' => 111,
      'cardId' => '74_card',
      'cardLevel' => 2,
      'rate' => 1200,
    ),
    1014 => 
    array (
      'id' => 111,
      'cardId' => '82_card',
      'cardLevel' => 2,
      'rate' => 1200,
    ),
    1015 => 
    array (
      'id' => 111,
      'cardId' => '171_card',
      'cardLevel' => 3,
      'rate' => 1000,
    ),
    1016 => 
    array (
      'id' => 111,
      'cardId' => '172_card',
      'cardLevel' => 3,
      'rate' => 1000,
    ),
    1017 => 
    array (
      'id' => 111,
      'cardId' => '173_card',
      'cardLevel' => 3,
      'rate' => 1000,
    ),
    1018 => 
    array (
      'id' => 111,
      'cardId' => '174_card',
      'cardLevel' => 3,
      'rate' => 1000,
    ),
    1019 => 
    array (
      'id' => 111,
      'cardId' => '175_card',
      'cardLevel' => 3,
      'rate' => 1000,
    ),
    1020 => 
    array (
      'id' => 112,
      'cardId' => '57_card',
      'cardLevel' => 1,
      'rate' => 1500,
    ),
    1021 => 
    array (
      'id' => 112,
      'cardId' => '157_card',
      'cardLevel' => 2,
      'rate' => 4500,
    ),
    1022 => 
    array (
      'id' => 112,
      'cardId' => '158_card',
      'cardLevel' => 3,
      'rate' => 2000,
    ),
    1023 => 
    array (
      'id' => 112,
      'cardId' => '159_card',
      'cardLevel' => 4,
      'rate' => 1000,
    ),
    1024 => 
    array (
      'id' => 113,
      'cardId' => '78_card',
      'cardLevel' => 8,
      'rate' => 1750,
    ),
    1025 => 
    array (
      'id' => 113,
      'cardId' => '90_card',
      'cardLevel' => 1,
      'rate' => 250,
    ),
    1026 => 
    array (
      'id' => 113,
      'cardId' => '166_card',
      'cardLevel' => 1,
      'rate' => 2500,
    ),
    1027 => 
    array (
      'id' => 113,
      'cardId' => '202_card',
      'cardLevel' => 8,
      'rate' => 1750,
    ),
    1028 => 
    array (
      'id' => 113,
      'cardId' => '90_card',
      'cardLevel' => 3,
      'rate' => 50,
    ),
    1029 => 
    array (
      'id' => 114,
      'cardId' => '78_card',
      'cardLevel' => 5,
      'rate' => 1750,
    ),
    1030 => 
    array (
      'id' => 114,
      'cardId' => '90_card',
      'cardLevel' => 1,
      'rate' => 250,
    ),
    1031 => 
    array (
      'id' => 114,
      'cardId' => '171_card',
      'cardLevel' => 1,
      'rate' => 2500,
    ),
    1032 => 
    array (
      'id' => 114,
      'cardId' => '202_card',
      'cardLevel' => 5,
      'rate' => 1750,
    ),
    1033 => 
    array (
      'id' => 114,
      'cardId' => '90_card',
      'cardLevel' => 3,
      'rate' => 50,
    ),
    1034 => 
    array (
      'id' => 115,
      'cardId' => '78_card',
      'cardLevel' => 3,
      'rate' => 1750,
    ),
    1035 => 
    array (
      'id' => 115,
      'cardId' => '90_card',
      'cardLevel' => 3,
      'rate' => 250,
    ),
    1036 => 
    array (
      'id' => 115,
      'cardId' => '197_card',
      'cardLevel' => 1,
      'rate' => 2500,
    ),
    1037 => 
    array (
      'id' => 115,
      'cardId' => '202_card',
      'cardLevel' => 3,
      'rate' => 1750,
    ),
    1038 => 
    array (
      'id' => 115,
      'cardId' => '90_card',
      'cardLevel' => 7,
      'rate' => 50,
    ),
    1039 => 
    array (
      'id' => 116,
      'cardId' => '52_card',
      'cardLevel' => 3,
      'rate' => 2000,
    ),
    1040 => 
    array (
      'id' => 116,
      'cardId' => '67_card',
      'cardLevel' => 7,
      'rate' => 2000,
    ),
    1041 => 
    array (
      'id' => 116,
      'cardId' => '79_card',
      'cardLevel' => 2,
      'rate' => 1000,
    ),
    1042 => 
    array (
      'id' => 116,
      'cardId' => '21_card',
      'cardLevel' => 1,
      'rate' => 2000,
    ),
    1043 => 
    array (
      'id' => 116,
      'cardId' => '212_card',
      'cardLevel' => 3,
      'rate' => 20,
    ),
    1044 => 
    array (
      'id' => 117,
      'cardId' => '52_card',
      'cardLevel' => 5,
      'rate' => 2000,
    ),
    1045 => 
    array (
      'id' => 117,
      'cardId' => '67_card',
      'cardLevel' => 5,
      'rate' => 2000,
    ),
    1046 => 
    array (
      'id' => 117,
      'cardId' => '79_card',
      'cardLevel' => 5,
      'rate' => 1000,
    ),
    1047 => 
    array (
      'id' => 117,
      'cardId' => '21_card',
      'cardLevel' => 1,
      'rate' => 2000,
    ),
    1048 => 
    array (
      'id' => 117,
      'cardId' => '2_card',
      'cardLevel' => 3,
      'rate' => 100,
    ),
    1049 => 
    array (
      'id' => 117,
      'cardId' => '107_card',
      'cardLevel' => 1,
      'rate' => 100,
    ),
    1050 => 
    array (
      'id' => 117,
      'cardId' => '212_card',
      'cardLevel' => 1,
      'rate' => 20,
    ),
    1051 => 
    array (
      'id' => 118,
      'cardId' => '113_card',
      'cardLevel' => 1,
      'rate' => 2000,
    ),
    1052 => 
    array (
      'id' => 119,
      'cardId' => '79_card',
      'cardLevel' => 10,
      'rate' => 2000,
    ),
    1053 => 
    array (
      'id' => 119,
      'cardId' => '202_card',
      'cardLevel' => 10,
      'rate' => 1000,
    ),
    1054 => 
    array (
      'id' => 119,
      'cardId' => '21_card',
      'cardLevel' => 10,
      'rate' => 2000,
    ),
    1055 => 
    array (
      'id' => 119,
      'cardId' => '2_card',
      'cardLevel' => 10,
      'rate' => 100,
    ),
    1056 => 
    array (
      'id' => 119,
      'cardId' => '107_card',
      'cardLevel' => 10,
      'rate' => 100,
    ),
    1057 => 
    array (
      'id' => 119,
      'cardId' => '212_card',
      'cardLevel' => 10,
      'rate' => 20,
    ),
    1058 => 
    array (
      'id' => 120,
      'cardId' => '202_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1059 => 
    array (
      'id' => 120,
      'cardId' => '66_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    1060 => 
    array (
      'id' => 120,
      'cardId' => '204_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1061 => 
    array (
      'id' => 120,
      'cardId' => '68_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    1062 => 
    array (
      'id' => 120,
      'cardId' => '206_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1063 => 
    array (
      'id' => 120,
      'cardId' => '70_card',
      'cardLevel' => 1,
      'rate' => 1200,
    ),
    1064 => 
    array (
      'id' => 120,
      'cardId' => '208_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1065 => 
    array (
      'id' => 120,
      'cardId' => '37_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1066 => 
    array (
      'id' => 120,
      'cardId' => '130_card',
      'cardLevel' => 3,
      'rate' => 50,
    ),
    1067 => 
    array (
      'id' => 120,
      'cardId' => '210_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1068 => 
    array (
      'id' => 120,
      'cardId' => '204_card',
      'cardLevel' => 3,
      'rate' => 350,
    ),
    1069 => 
    array (
      'id' => 120,
      'cardId' => '207_card',
      'cardLevel' => 3,
      'rate' => 250,
    ),
    1070 => 
    array (
      'id' => 120,
      'cardId' => '202_card',
      'cardLevel' => 3,
      'rate' => 350,
    ),
    1071 => 
    array (
      'id' => 121,
      'cardId' => '202_card',
      'cardLevel' => 3,
      'rate' => 500,
    ),
    1072 => 
    array (
      'id' => 121,
      'cardId' => '66_card',
      'cardLevel' => 3,
      'rate' => 1200,
    ),
    1073 => 
    array (
      'id' => 121,
      'cardId' => '204_card',
      'cardLevel' => 3,
      'rate' => 500,
    ),
    1074 => 
    array (
      'id' => 121,
      'cardId' => '68_card',
      'cardLevel' => 3,
      'rate' => 1200,
    ),
    1075 => 
    array (
      'id' => 121,
      'cardId' => '206_card',
      'cardLevel' => 3,
      'rate' => 500,
    ),
    1076 => 
    array (
      'id' => 121,
      'cardId' => '70_card',
      'cardLevel' => 3,
      'rate' => 1200,
    ),
    1077 => 
    array (
      'id' => 121,
      'cardId' => '208_card',
      'cardLevel' => 3,
      'rate' => 500,
    ),
    1078 => 
    array (
      'id' => 121,
      'cardId' => '37_card',
      'cardLevel' => 3,
      'rate' => 500,
    ),
    1079 => 
    array (
      'id' => 121,
      'cardId' => '130_card',
      'cardLevel' => 3,
      'rate' => 50,
    ),
    1080 => 
    array (
      'id' => 121,
      'cardId' => '210_card',
      'cardLevel' => 3,
      'rate' => 500,
    ),
    1081 => 
    array (
      'id' => 121,
      'cardId' => '202_card',
      'cardLevel' => 5,
      'rate' => 350,
    ),
    1082 => 
    array (
      'id' => 121,
      'cardId' => '207_card',
      'cardLevel' => 5,
      'rate' => 250,
    ),
    1083 => 
    array (
      'id' => 121,
      'cardId' => '204_card',
      'cardLevel' => 5,
      'rate' => 350,
    ),
    1084 => 
    array (
      'id' => 122,
      'cardId' => '67_card',
      'cardLevel' => 8,
      'rate' => 500,
    ),
    1085 => 
    array (
      'id' => 122,
      'cardId' => '202_card',
      'cardLevel' => 7,
      'rate' => 500,
    ),
    1086 => 
    array (
      'id' => 122,
      'cardId' => '69_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    1087 => 
    array (
      'id' => 122,
      'cardId' => '204_card',
      'cardLevel' => 7,
      'rate' => 1250,
    ),
    1088 => 
    array (
      'id' => 122,
      'cardId' => '71_card',
      'cardLevel' => 8,
      'rate' => 1250,
    ),
    1089 => 
    array (
      'id' => 122,
      'cardId' => '206_card',
      'cardLevel' => 5,
      'rate' => 1250,
    ),
    1090 => 
    array (
      'id' => 122,
      'cardId' => '208_card',
      'cardLevel' => 7,
      'rate' => 1250,
    ),
    1091 => 
    array (
      'id' => 122,
      'cardId' => '210_card',
      'cardLevel' => 7,
      'rate' => 1250,
    ),
    1092 => 
    array (
      'id' => 122,
      'cardId' => '37_card',
      'cardLevel' => 7,
      'rate' => 500,
    ),
    1093 => 
    array (
      'id' => 122,
      'cardId' => '130_card',
      'cardLevel' => 7,
      'rate' => 50,
    ),
    1094 => 
    array (
      'id' => 122,
      'cardId' => '205_card',
      'cardLevel' => 7,
      'rate' => 500,
    ),
    1095 => 
    array (
      'id' => 122,
      'cardId' => '130_card',
      'cardLevel' => 7,
      'rate' => 25,
    ),
    1096 => 
    array (
      'id' => 122,
      'cardId' => '205_card',
      'cardLevel' => 7,
      'rate' => 50,
    ),
    1097 => 
    array (
      'id' => 123,
      'cardId' => '170_card',
      'cardLevel' => 1,
      'rate' => 3000,
    ),
    1098 => 
    array (
      'id' => 123,
      'cardId' => '86_card',
      'cardLevel' => 3,
      'rate' => 2250,
    ),
    1099 => 
    array (
      'id' => 123,
      'cardId' => '210_card',
      'cardLevel' => 3,
      'rate' => 2250,
    ),
    1100 => 
    array (
      'id' => 123,
      'cardId' => '125_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    1101 => 
    array (
      'id' => 123,
      'cardId' => '125_card',
      'cardLevel' => 1,
      'rate' => 25,
    ),
    1102 => 
    array (
      'id' => 124,
      'cardId' => '175_card',
      'cardLevel' => 1,
      'rate' => 3000,
    ),
    1103 => 
    array (
      'id' => 124,
      'cardId' => '86_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1104 => 
    array (
      'id' => 124,
      'cardId' => '210_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1105 => 
    array (
      'id' => 124,
      'cardId' => '125_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    1106 => 
    array (
      'id' => 124,
      'cardId' => '125_card',
      'cardLevel' => 3,
      'rate' => 25,
    ),
    1107 => 
    array (
      'id' => 125,
      'cardId' => '201_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1108 => 
    array (
      'id' => 125,
      'cardId' => '87_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1109 => 
    array (
      'id' => 125,
      'cardId' => '210_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1110 => 
    array (
      'id' => 125,
      'cardId' => '125_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    1111 => 
    array (
      'id' => 125,
      'cardId' => '125_card',
      'cardLevel' => 7,
      'rate' => 25,
    ),
    1112 => 
    array (
      'id' => 126,
      'cardId' => '169_card',
      'cardLevel' => 1,
      'rate' => 3000,
    ),
    1113 => 
    array (
      'id' => 126,
      'cardId' => '84_card',
      'cardLevel' => 3,
      'rate' => 2250,
    ),
    1114 => 
    array (
      'id' => 126,
      'cardId' => '208_card',
      'cardLevel' => 3,
      'rate' => 2250,
    ),
    1115 => 
    array (
      'id' => 126,
      'cardId' => '122_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    1116 => 
    array (
      'id' => 126,
      'cardId' => '122_card',
      'cardLevel' => 1,
      'rate' => 25,
    ),
    1117 => 
    array (
      'id' => 127,
      'cardId' => '174_card',
      'cardLevel' => 1,
      'rate' => 3000,
    ),
    1118 => 
    array (
      'id' => 127,
      'cardId' => '84_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1119 => 
    array (
      'id' => 127,
      'cardId' => '208_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1120 => 
    array (
      'id' => 127,
      'cardId' => '122_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    1121 => 
    array (
      'id' => 127,
      'cardId' => '122_card',
      'cardLevel' => 3,
      'rate' => 25,
    ),
    1122 => 
    array (
      'id' => 128,
      'cardId' => '200_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1123 => 
    array (
      'id' => 128,
      'cardId' => '85_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1124 => 
    array (
      'id' => 128,
      'cardId' => '208_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1125 => 
    array (
      'id' => 128,
      'cardId' => '122_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    1126 => 
    array (
      'id' => 128,
      'cardId' => '122_card',
      'cardLevel' => 7,
      'rate' => 25,
    ),
    1127 => 
    array (
      'id' => 129,
      'cardId' => '168_card',
      'cardLevel' => 1,
      'rate' => 3000,
    ),
    1128 => 
    array (
      'id' => 129,
      'cardId' => '206_card',
      'cardLevel' => 3,
      'rate' => 2250,
    ),
    1129 => 
    array (
      'id' => 129,
      'cardId' => '82_card',
      'cardLevel' => 3,
      'rate' => 2250,
    ),
    1130 => 
    array (
      'id' => 129,
      'cardId' => '119_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    1131 => 
    array (
      'id' => 129,
      'cardId' => '119_card',
      'cardLevel' => 1,
      'rate' => 25,
    ),
    1132 => 
    array (
      'id' => 130,
      'cardId' => '173_card',
      'cardLevel' => 1,
      'rate' => 3000,
    ),
    1133 => 
    array (
      'id' => 130,
      'cardId' => '206_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1134 => 
    array (
      'id' => 130,
      'cardId' => '82_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1135 => 
    array (
      'id' => 130,
      'cardId' => '119_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    1136 => 
    array (
      'id' => 130,
      'cardId' => '119_card',
      'cardLevel' => 3,
      'rate' => 25,
    ),
    1137 => 
    array (
      'id' => 131,
      'cardId' => '199_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1138 => 
    array (
      'id' => 131,
      'cardId' => '206_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1139 => 
    array (
      'id' => 131,
      'cardId' => '83_card',
      'cardLevel' => 8,
      'rate' => 2500,
    ),
    1140 => 
    array (
      'id' => 131,
      'cardId' => '119_card',
      'cardLevel' => 1,
      'rate' => 50,
    ),
    1141 => 
    array (
      'id' => 131,
      'cardId' => '119_card',
      'cardLevel' => 7,
      'rate' => 25,
    ),
    1142 => 
    array (
      'id' => 132,
      'cardId' => '98_card',
      'cardLevel' => 1,
      'rate' => 200,
    ),
    1143 => 
    array (
      'id' => 132,
      'cardId' => '101_card',
      'cardLevel' => 1,
      'rate' => 200,
    ),
    1144 => 
    array (
      'id' => 132,
      'cardId' => '104_card',
      'cardLevel' => 1,
      'rate' => 200,
    ),
    1145 => 
    array (
      'id' => 132,
      'cardId' => '107_card',
      'cardLevel' => 1,
      'rate' => 200,
    ),
    1146 => 
    array (
      'id' => 132,
      'cardId' => '110_card',
      'cardLevel' => 1,
      'rate' => 200,
    ),
    1147 => 
    array (
      'id' => 133,
      'cardId' => '98_card',
      'cardLevel' => 3,
      'rate' => 400,
    ),
    1148 => 
    array (
      'id' => 133,
      'cardId' => '101_card',
      'cardLevel' => 3,
      'rate' => 400,
    ),
    1149 => 
    array (
      'id' => 133,
      'cardId' => '104_card',
      'cardLevel' => 3,
      'rate' => 400,
    ),
    1150 => 
    array (
      'id' => 133,
      'cardId' => '107_card',
      'cardLevel' => 3,
      'rate' => 400,
    ),
    1151 => 
    array (
      'id' => 133,
      'cardId' => '110_card',
      'cardLevel' => 3,
      'rate' => 400,
    ),
    1152 => 
    array (
      'id' => 134,
      'cardId' => '98_card',
      'cardLevel' => 7,
      'rate' => 500,
    ),
    1153 => 
    array (
      'id' => 134,
      'cardId' => '101_card',
      'cardLevel' => 7,
      'rate' => 500,
    ),
    1154 => 
    array (
      'id' => 134,
      'cardId' => '104_card',
      'cardLevel' => 7,
      'rate' => 500,
    ),
    1155 => 
    array (
      'id' => 134,
      'cardId' => '107_card',
      'cardLevel' => 7,
      'rate' => 500,
    ),
    1156 => 
    array (
      'id' => 134,
      'cardId' => '110_card',
      'cardLevel' => 7,
      'rate' => 500,
    ),
    1157 => 
    array (
      'id' => 134,
      'cardId' => '99_card',
      'cardLevel' => 3,
      'rate' => 50,
    ),
    1158 => 
    array (
      'id' => 134,
      'cardId' => '102_card',
      'cardLevel' => 3,
      'rate' => 50,
    ),
    1159 => 
    array (
      'id' => 134,
      'cardId' => '105_card',
      'cardLevel' => 3,
      'rate' => 50,
    ),
    1160 => 
    array (
      'id' => 134,
      'cardId' => '108_card',
      'cardLevel' => 3,
      'rate' => 50,
    ),
    1161 => 
    array (
      'id' => 134,
      'cardId' => '111_card',
      'cardLevel' => 3,
      'rate' => 50,
    ),
    1162 => 
    array (
      'id' => 134,
      'cardId' => '193_card',
      'cardLevel' => 3,
      'rate' => 10,
    ),
    1163 => 
    array (
      'id' => 135,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 2000,
    ),
    1164 => 
    array (
      'id' => 135,
      'cardId' => '184_card',
      'cardLevel' => 1,
      'rate' => 2000,
    ),
    1165 => 
    array (
      'id' => 136,
      'cardId' => '182_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1166 => 
    array (
      'id' => 136,
      'cardId' => '185_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1167 => 
    array (
      'id' => 137,
      'cardId' => '183_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1168 => 
    array (
      'id' => 137,
      'cardId' => '186_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1169 => 
    array (
      'id' => 138,
      'cardId' => '160_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1170 => 
    array (
      'id' => 138,
      'cardId' => '161_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1171 => 
    array (
      'id' => 138,
      'cardId' => '162_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1172 => 
    array (
      'id' => 138,
      'cardId' => '163_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1173 => 
    array (
      'id' => 138,
      'cardId' => '164_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1174 => 
    array (
      'id' => 138,
      'cardId' => '165_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1175 => 
    array (
      'id' => 139,
      'cardId' => '226_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1176 => 
    array (
      'id' => 139,
      'cardId' => '230_card',
      'cardLevel' => 1,
      'rate' => 1250,
    ),
    1177 => 
    array (
      'id' => 140,
      'cardId' => '226_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1178 => 
    array (
      'id' => 140,
      'cardId' => '230_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1179 => 
    array (
      'id' => 140,
      'cardId' => '234_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1180 => 
    array (
      'id' => 141,
      'cardId' => '230_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1181 => 
    array (
      'id' => 141,
      'cardId' => '234_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1182 => 
    array (
      'id' => 141,
      'cardId' => '238_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1183 => 
    array (
      'id' => 142,
      'cardId' => '227_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1184 => 
    array (
      'id' => 142,
      'cardId' => '231_card',
      'cardLevel' => 1,
      'rate' => 1250,
    ),
    1185 => 
    array (
      'id' => 143,
      'cardId' => '227_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1186 => 
    array (
      'id' => 143,
      'cardId' => '231_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1187 => 
    array (
      'id' => 143,
      'cardId' => '235_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1188 => 
    array (
      'id' => 144,
      'cardId' => '231_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1189 => 
    array (
      'id' => 144,
      'cardId' => '235_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1190 => 
    array (
      'id' => 144,
      'cardId' => '239_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1191 => 
    array (
      'id' => 145,
      'cardId' => '228_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1192 => 
    array (
      'id' => 145,
      'cardId' => '232_card',
      'cardLevel' => 1,
      'rate' => 1250,
    ),
    1193 => 
    array (
      'id' => 146,
      'cardId' => '228_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1194 => 
    array (
      'id' => 146,
      'cardId' => '232_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1195 => 
    array (
      'id' => 146,
      'cardId' => '236_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1196 => 
    array (
      'id' => 147,
      'cardId' => '232_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1197 => 
    array (
      'id' => 147,
      'cardId' => '236_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1198 => 
    array (
      'id' => 147,
      'cardId' => '240_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1199 => 
    array (
      'id' => 148,
      'cardId' => '229_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1200 => 
    array (
      'id' => 148,
      'cardId' => '233_card',
      'cardLevel' => 1,
      'rate' => 1250,
    ),
    1201 => 
    array (
      'id' => 149,
      'cardId' => '229_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1202 => 
    array (
      'id' => 149,
      'cardId' => '233_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1203 => 
    array (
      'id' => 149,
      'cardId' => '237_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1204 => 
    array (
      'id' => 150,
      'cardId' => '233_card',
      'cardLevel' => 1,
      'rate' => 4000,
    ),
    1205 => 
    array (
      'id' => 150,
      'cardId' => '237_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1206 => 
    array (
      'id' => 150,
      'cardId' => '241_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1207 => 
    array (
      'id' => 151,
      'cardId' => '226_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1208 => 
    array (
      'id' => 151,
      'cardId' => '230_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1209 => 
    array (
      'id' => 151,
      'cardId' => '227_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1210 => 
    array (
      'id' => 151,
      'cardId' => '231_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1211 => 
    array (
      'id' => 151,
      'cardId' => '228_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1212 => 
    array (
      'id' => 151,
      'cardId' => '232_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1213 => 
    array (
      'id' => 151,
      'cardId' => '229_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1214 => 
    array (
      'id' => 151,
      'cardId' => '233_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1215 => 
    array (
      'id' => 152,
      'cardId' => '226_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1216 => 
    array (
      'id' => 152,
      'cardId' => '230_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1217 => 
    array (
      'id' => 152,
      'cardId' => '234_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    1218 => 
    array (
      'id' => 152,
      'cardId' => '227_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1219 => 
    array (
      'id' => 152,
      'cardId' => '231_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1220 => 
    array (
      'id' => 152,
      'cardId' => '235_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    1221 => 
    array (
      'id' => 152,
      'cardId' => '228_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1222 => 
    array (
      'id' => 152,
      'cardId' => '232_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1223 => 
    array (
      'id' => 152,
      'cardId' => '236_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    1224 => 
    array (
      'id' => 152,
      'cardId' => '229_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1225 => 
    array (
      'id' => 152,
      'cardId' => '233_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1226 => 
    array (
      'id' => 152,
      'cardId' => '237_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    1227 => 
    array (
      'id' => 153,
      'cardId' => '230_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1228 => 
    array (
      'id' => 153,
      'cardId' => '234_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1229 => 
    array (
      'id' => 153,
      'cardId' => '238_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    1230 => 
    array (
      'id' => 153,
      'cardId' => '231_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1231 => 
    array (
      'id' => 153,
      'cardId' => '235_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1232 => 
    array (
      'id' => 153,
      'cardId' => '239_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    1233 => 
    array (
      'id' => 153,
      'cardId' => '232_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1234 => 
    array (
      'id' => 153,
      'cardId' => '236_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1235 => 
    array (
      'id' => 153,
      'cardId' => '240_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    1236 => 
    array (
      'id' => 153,
      'cardId' => '233_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1237 => 
    array (
      'id' => 153,
      'cardId' => '237_card',
      'cardLevel' => 1,
      'rate' => 300,
    ),
    1238 => 
    array (
      'id' => 153,
      'cardId' => '241_card',
      'cardLevel' => 1,
      'rate' => 150,
    ),
    1239 => 
    array (
      'id' => 154,
      'cardId' => '181_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1240 => 
    array (
      'id' => 154,
      'cardId' => '184_card',
      'cardLevel' => 1,
      'rate' => 1000,
    ),
    1241 => 
    array (
      'id' => 155,
      'cardId' => '182_card',
      'cardLevel' => 1,
      'rate' => 750,
    ),
    1242 => 
    array (
      'id' => 155,
      'cardId' => '185_card',
      'cardLevel' => 1,
      'rate' => 750,
    ),
    1243 => 
    array (
      'id' => 156,
      'cardId' => '183_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
    1244 => 
    array (
      'id' => 156,
      'cardId' => '186_card',
      'cardLevel' => 1,
      'rate' => 500,
    ),
  ),
);
?>