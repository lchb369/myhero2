<?php
return array (
  'columns' => 
  array (
    0 => 'goodsId',
    1 => 'goodsName',
    2 => 'goodsDesc',
    3 => 'price',
    4 => 'coin',
    5 => 'limit',
    6 => 'TPCard',
    7 => 'TPCardId',
    8 => 'TPSms',
    9 => 'TPSmsId',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'goodsId' => 1,
      'goodsName' => 'goods01',
      'goodsDesc' => '10 KNB',
      'price' => 10000,
      'coin' => 10,
      'limit' => 0,
      'TPCard' => 10000,
      'TPCardId' => 10000,
      'TPSms' => NULL,
      'TPSmsId' => NULL,
    ),
    1 => 
    array (
      'goodsId' => 2,
      'goodsName' => 'goods02',
      'goodsDesc' => '20 KNB',
      'price' => 20000,
      'coin' => 20,
      'limit' => 0,
      'TPCard' => 20000,
      'TPCardId' => 20000,
      'TPSms' => NULL,
      'TPSmsId' => NULL,
    ),
    2 => 
    array (
      'goodsId' => 3,
      'goodsName' => 'goods03',
      'goodsDesc' => '50 KNB',
      'price' => 50000,
      'coin' => 50,
      'limit' => 0,
      'TPCard' => 50000,
      'TPCardId' => 50000,
      'TPSms' => NULL,
      'TPSmsId' => NULL,
    ),
    3 => 
    array (
      'goodsId' => 4,
      'goodsName' => 'goods04',
      'goodsDesc' => '110 KNB',
      'price' => 100000,
      'coin' => 110,
      'limit' => 0,
      'TPCard' => 100000,
      'TPCardId' => 100000,
      'TPSms' => NULL,
      'TPSmsId' => NULL,
    ),
    4 => 
    array (
      'goodsId' => 5,
      'goodsName' => 'goods05',
      'goodsDesc' => '230 KNB',
      'price' => 200000,
      'coin' => 230,
      'limit' => 0,
      'TPCard' => 200000,
      'TPCardId' => 200000,
      'TPSms' => NULL,
      'TPSmsId' => NULL,
    ),
    5 => 
    array (
      'goodsId' => 6,
      'goodsName' => 'goods06',
      'goodsDesc' => '600 KNB',
      'price' => 500000,
      'coin' => 600,
      'limit' => 0,
      'TPCard' => 500000,
      'TPCardId' => 500000,
      'TPSms' => NULL,
      'TPSmsId' => NULL,
    ),
    6 => 
    array (
      'goodsId' => 7,
      'goodsName' => 'goods07',
      'goodsDesc' => '4KNB ',
      'price' => 10000,
      'coin' => 4,
      'limit' => 0,
      'TPCard' => NULL,
      'TPCardId' => NULL,
      'TPSms' => 10000,
      'TPSmsId' => 7626,
    ),
    7 => 
    array (
      'goodsId' => 8,
      'goodsName' => 'goods08',
      'goodsDesc' => '6KNB',
      'price' => 15000,
      'coin' => 6,
      'limit' => 0,
      'TPCard' => NULL,
      'TPCardId' => NULL,
      'TPSms' => 15000,
      'TPSmsId' => 7726,
    ),
  ),
);
?>