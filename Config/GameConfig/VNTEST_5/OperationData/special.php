<?php
return array (
  1 => 
  array (
    'name' => 'Bạch mã chiến',
    'start_time' => '2012-02-25 00:00:00',
    'end_time' => '2013-12-25 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Nhan Lương',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '620,621,622,623',
        'boss' => 
        array (
          0 => '624',
        ),
        'package' => 113,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Nhan Lương',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '625,626,627,628',
        'boss' => 
        array (
          0 => '629',
          1 => '629',
        ),
        'package' => 114,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Nhan Lương',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '630,631,632,633',
        'boss' => 
        array (
          0 => '634',
          1 => '635',
          2 => '636',
        ),
        'package' => 115,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  2 => 
  array (
    'name' => 'Thiên hạ kì sĩ',
    'start_time' => '2013-08-13 12:00:00',
    'end_time' => '2013-12-25 00:00:03',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Quách Gia',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '503,504,501,505',
        'boss' => 
        array (
          0 => '502',
        ),
        'package' => 116,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Quách Gia',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 507,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 508,
            1 => 510,
            2 => 509,
          ),
        ),
        'monsters' => '503,504,505,506',
        'boss' => 
        array (
          0 => '511',
        ),
        'package' => 117,
        'randMonster' => 1001,
        'randDrop' => 118,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Quách Gia',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 515,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 516,
            1 => 517,
          ),
        ),
        'monsters' => '512,513,514',
        'boss' => 
        array (
          0 => '518',
        ),
        'package' => 119,
        'randMonster' => 1002,
        'randDrop' => 118,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  3 => 
  array (
    'name' => 'Uy dũng tướng quân',
    'start_time' => '2013-05-09 00:00:00',
    'end_time' => '2013-05-20 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Chu Thái',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '638,639,640,641,642,643,644,645,646,647',
        'boss' => 
        array (
          0 => '648',
          1 => '649',
          2 => '650',
        ),
        'package' => 120,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Chu Thái',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '651,652,653,654,655,656,657,658,659,660',
        'boss' => 
        array (
          0 => '661',
          1 => '662',
          2 => '663',
        ),
        'package' => 121,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Chu Thái',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '664,665,666,667,668,669,670,671,672,673',
        'boss' => 
        array (
          0 => '674',
          1 => '675',
          2 => '676',
        ),
        'package' => 122,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  4 => 
  array (
    'name' => 'Hộ bộ quan hữu',
    'start_time' => '2013-05-27 00:00:00',
    'end_time' => '2013-06-03 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Hạ Hầu Uyên',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '677,678,679,680',
        'boss' => 
        array (
          0 => '681',
        ),
        'package' => 123,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Hạ Hầu Uyên',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '682,683,684,685',
        'boss' => 
        array (
          0 => '686',
          1 => '687',
        ),
        'package' => 124,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) hạ Hầu Uyên',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '688,689,690,691',
        'boss' => 
        array (
          0 => '692',
          1 => '693',
          2 => '694',
        ),
        'package' => 125,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  5 => 
  array (
    'name' => 'Độc nhãn long',
    'start_time' => '2013-06-03 00:00:00',
    'end_time' => '2013-06-10 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Hạ Hầu Đôn',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '695,696,697,698',
        'boss' => 
        array (
          0 => '699',
        ),
        'package' => 126,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Hạ Hầu Đôn',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '700,701,702,703',
        'boss' => 
        array (
          0 => '704',
          1 => '705',
        ),
        'package' => 127,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Hạ Hầu Đôn',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '706,707,708,709',
        'boss' => 
        array (
          0 => '710',
          1 => '711',
          2 => '712',
        ),
        'package' => 128,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  6 => 
  array (
    'name' => 'Cổ chi ác lai',
    'start_time' => '2013-06-10 00:00:00',
    'end_time' => '2013-06-17 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Điển Vi',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '713,714,715,716',
        'boss' => 
        array (
          0 => '717',
        ),
        'package' => 129,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Điển Vi',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '718,719,720,721',
        'boss' => 
        array (
          0 => '722',
          1 => '723',
        ),
        'package' => 130,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Điển Vi',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '724,725,726,727',
        'boss' => 
        array (
          0 => '728',
          1 => '729',
          2 => '730',
        ),
        'package' => 131,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  7 => 
  array (
    'name' => 'Nữ tướng anh hùng',
    'start_time' => '2013-04-24 19:15:00',
    'end_time' => '2013-04-24 20:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Nữ tướng',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '731,732,733,734,735',
        'boss' => 
        array (
          0 => '736',
          1 => '737',
          2 => '738',
        ),
        'package' => 132,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Nữ tướng',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '739,740,741,742,743',
        'boss' => 
        array (
          0 => '744',
          1 => '745',
          2 => '746',
        ),
        'package' => 133,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Nữ tướng',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '747,748,749,750,751,752',
        'boss' => 
        array (
          0 => '753',
        ),
        'package' => 134,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  29 => 
  array (
    'name' => 'Lao động vinh quang',
    'start_time' => '2013-04-28 00:00:00',
    'end_time' => '2013-05-14 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Gương LĐ',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '754,755,757,758',
        'boss' => 
        array (
          0 => '754',
          1 => '755',
          2 => '758',
          3 => '757',
        ),
        'package' => 135,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) GươngLĐ',
        'stamina' => 15,
        'steps' => 4,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
        ),
        'monsters' => '754,755,756,757,758,759',
        'boss' => 
        array (
          0 => '754',
          1 => '755',
          2 => '758',
          3 => '757',
        ),
        'package' => 136,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Gương LĐ',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '754,755,756,757,758,759',
        'boss' => 
        array (
          0 => '755',
          1 => '756',
          2 => '759',
          3 => '758',
        ),
        'package' => 137,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  30 => 
  array (
    'name' => 'Lao động tự hào',
    'start_time' => '2013-04-28 00:00:00',
    'end_time' => '2013-05-14 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Tinh thần LĐ',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '760,761,762,763,764',
        'boss' => 
        array (
          0 => '765',
        ),
        'package' => 138,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 10000,
      ),
      2 => 
      array (
        'name' => '(Trung) Tinh thần LĐ',
        'stamina' => 15,
        'steps' => 4,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
        ),
        'monsters' => '766,767,768,769,770',
        'boss' => 
        array (
          0 => '771',
          1 => '771',
        ),
        'package' => 138,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1500,
        'dropGoldRate' => 10000,
      ),
      3 => 
      array (
        'name' => '(Cao) Tinh thần LĐ',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '772,773,774,775,776',
        'boss' => 
        array (
          0 => '777',
          1 => '777',
          2 => '777',
        ),
        'package' => 138,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 2000,
        'dropGoldRate' => 10000,
      ),
    ),
  ),
  31 => 
  array (
    'name' => 'Thanh tống hương',
    'start_time' => '2013-06-13 00:00:00',
    'end_time' => '2013-06-14 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Thanh tống hương',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '908,909,910,911,912',
        'boss' => 
        array (
          0 => '913',
        ),
        'package' => 140,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Thanh tống hương',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 903,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 905,
            1 => 906,
            2 => 904,
          ),
        ),
        'monsters' => '898,899,900,901',
        'boss' => 
        array (
          0 => '907',
        ),
        'package' => 140,
        'randMonster' => 902,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Thanh tống hương',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 961,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 962,
            1 => 963,
          ),
        ),
        'monsters' => '957,958',
        'boss' => 
        array (
          0 => '964',
        ),
        'package' => 140,
        'randMonster' => 959,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  32 => 
  array (
    'name' => 'Tầm  tống hương',
    'start_time' => '2013-06-09 00:00:00',
    'end_time' => '2013-06-10 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Tầm  tống hương',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '924,925,926,927,928',
        'boss' => 
        array (
          0 => '929',
        ),
        'package' => 141,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Tầm  tống hương',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 919,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 921,
            1 => 922,
            2 => 920,
          ),
        ),
        'monsters' => '914,915,916,917',
        'boss' => 
        array (
          0 => '923',
        ),
        'package' => 141,
        'randMonster' => 918,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Tầm  tống hương',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 969,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 970,
            1 => 971,
          ),
        ),
        'monsters' => '965,966',
        'boss' => 
        array (
          0 => '972',
        ),
        'package' => 141,
        'randMonster' => 967,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  33 => 
  array (
    'name' => 'Mê tống diệp',
    'start_time' => '2013-06-10 00:00:00',
    'end_time' => '2013-06-11 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Mê tống diệp',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '952,953,954,955',
        'boss' => 
        array (
          0 => '956',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Mê tống diệp',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 947,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 949,
            1 => 948,
            2 => 950,
          ),
        ),
        'monsters' => '944,945,946',
        'boss' => 
        array (
          0 => '951',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Mê tống diệp',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 982,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 983,
            1 => 984,
          ),
        ),
        'monsters' => '980,981',
        'boss' => 
        array (
          0 => '985',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  34 => 
  array (
    'name' => 'Trầm tống diệp',
    'start_time' => '2013-06-11 00:00:00',
    'end_time' => '2013-06-12 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Trầm tống diệp',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '892,893,894,895,896',
        'boss' => 
        array (
          0 => '897',
        ),
        'package' => 139,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Trầm tống diệp',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 887,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 889,
            1 => 888,
            2 => 890,
          ),
        ),
        'monsters' => '882,883,884,885',
        'boss' => 
        array (
          0 => '891',
        ),
        'package' => 139,
        'randMonster' => 896,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Trầm tống diệp',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 878,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 879,
            1 => 880,
          ),
        ),
        'monsters' => '874,875',
        'boss' => 
        array (
          0 => '881',
        ),
        'package' => 139,
        'randMonster' => 876,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  35 => 
  array (
    'name' => 'Thiền tống diệp',
    'start_time' => '2013-06-12 00:00:00',
    'end_time' => '2013-06-13 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Thiền tống diệp',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '939,940,941,942',
        'boss' => 
        array (
          0 => '943',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Thiền tống diệp',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 934,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 935,
            1 => 936,
            2 => 937,
          ),
        ),
        'monsters' => '930,931,932,933',
        'boss' => 
        array (
          0 => '938',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Thiền tống diệp',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 976,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 977,
            1 => 978,
          ),
        ),
        'monsters' => '973,974,975',
        'boss' => 
        array (
          0 => '979',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  36 => 
  array (
    'name' => 'Nữ thần giáng thế',
    'start_time' => '2013-07-02 12:00:00',
    'end_time' => '2013-10-30 23:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Nữ thần giáng thế',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '731,732,733,734,735',
        'boss' => 
        array (
          0 => '736',
          1 => '737',
          2 => '738',
        ),
        'package' => 132,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Nữ thần giáng thế',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '739,740,741,742,743',
        'boss' => 
        array (
          0 => '744',
          1 => '745',
          2 => '746',
        ),
        'package' => 133,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Nữ thần giáng thế',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '747,748,749,750,751,752',
        'boss' => 
        array (
          0 => '753',
        ),
        'package' => 134,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  37 => 
  array (
    'name' => 'Hoa Dung đạo',
    'start_time' => '2013-07-05 00:00:00',
    'end_time' => '2013-07-05 23:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Hoa Dung đạo',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '908,909,910,911,912',
        'boss' => 
        array (
          0 => '913',
        ),
        'package' => 140,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Hoa Dung đạo',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 903,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 905,
            1 => 906,
            2 => 904,
          ),
        ),
        'monsters' => '898,899,900,901',
        'boss' => 
        array (
          0 => '907',
        ),
        'package' => 140,
        'randMonster' => 902,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Hoa Dung đạo',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 961,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 962,
            1 => 963,
          ),
        ),
        'monsters' => '957,958',
        'boss' => 
        array (
          0 => '964',
        ),
        'package' => 140,
        'randMonster' => 959,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  38 => 
  array (
    'name' => 'Ải Hồ Lô',
    'start_time' => '2013-07-06 00:00:00',
    'end_time' => '2013-07-06 23:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Ải Hồ Lô',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '924,925,926,927,928',
        'boss' => 
        array (
          0 => '929',
        ),
        'package' => 141,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Ải Hồ Lô',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 919,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 921,
            1 => 922,
            2 => 920,
          ),
        ),
        'monsters' => '914,915,916,917',
        'boss' => 
        array (
          0 => '923',
        ),
        'package' => 141,
        'randMonster' => 918,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) ải Hồ Lô',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 969,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 970,
            1 => 971,
          ),
        ),
        'monsters' => '965,966',
        'boss' => 
        array (
          0 => '972',
        ),
        'package' => 141,
        'randMonster' => 967,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  39 => 
  array (
    'name' => 'Cầu gió đông',
    'start_time' => '2013-07-07 00:00:00',
    'end_time' => '2013-07-07 23:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Cầu gió đông',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '952,953,954,955',
        'boss' => 
        array (
          0 => '956',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Cầu gió đông',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 947,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 949,
            1 => 948,
            2 => 950,
          ),
        ),
        'monsters' => '944,945,946',
        'boss' => 
        array (
          0 => '951',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Cầu gió đông',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 982,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 983,
            1 => 984,
          ),
        ),
        'monsters' => '980,981',
        'boss' => 
        array (
          0 => '985',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  40 => 
  array (
    'name' => 'Chướng chỉ huy',
    'start_time' => '2013-07-08 00:00:00',
    'end_time' => '2013-07-08 23:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Chướng chỉ huy',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '892,893,894,895,896',
        'boss' => 
        array (
          0 => '897',
        ),
        'package' => 139,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Chướng chỉ huy',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 887,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 889,
            1 => 888,
            2 => 890,
          ),
        ),
        'monsters' => '882,883,884,885',
        'boss' => 
        array (
          0 => '891',
        ),
        'package' => 139,
        'randMonster' => 896,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Chướng chỉ huy',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 878,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 879,
            1 => 880,
          ),
        ),
        'monsters' => '874,875',
        'boss' => 
        array (
          0 => '881',
        ),
        'package' => 139,
        'randMonster' => 876,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  41 => 
  array (
    'name' => 'Dốc Trường Bản',
    'start_time' => '2013-07-09 00:00:00',
    'end_time' => '2013-07-09 23:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Dốc Trường Bản',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '939,940,941,942',
        'boss' => 
        array (
          0 => '943',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Dốc Trường Bản',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 934,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 935,
            1 => 936,
            2 => 937,
          ),
        ),
        'monsters' => '930,931,932,933',
        'boss' => 
        array (
          0 => '938',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Dốc Trường Bản',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 976,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 977,
            1 => 978,
          ),
        ),
        'monsters' => '973,974,975',
        'boss' => 
        array (
          0 => '979',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  42 => 
  array (
    'name' => 'Bá chủ Giang Đông',
    'start_time' => '2013-07-12 00:00:00',
    'end_time' => '2013-07-13 11:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Bá chủ Giang Đông',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2062,2063,2064,2065',
        'boss' => 
        array (
          0 => '2066',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Bá chủ Giang Đông',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2067,2068,2069,2070',
        'boss' => 
        array (
          0 => '2071',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Bá chủ Giang Đông',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2072,2073,2074,2075',
        'boss' => 
        array (
          0 => '2076',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  43 => 
  array (
    'name' => 'Nhân Vương Thục',
    'start_time' => '2013-07-13 12:00:00',
    'end_time' => '2013-07-14 11:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Nhân Vương Thục',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2077,2078,2079,2080',
        'boss' => 
        array (
          0 => '2081',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Nhân Vương Thục',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2082,2083,2084,2085',
        'boss' => 
        array (
          0 => '2086',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Nhân Vương Thục',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2087,2088,2089,2090',
        'boss' => 
        array (
          0 => '2091',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  44 => 
  array (
    'name' => 'Loạn thế kiêu hùng',
    'start_time' => '2013-07-14 12:00:00',
    'end_time' => '2013-07-15 11:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Loạn thế kiêu hùng',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2092,2093,2094,2095',
        'boss' => 
        array (
          0 => '2096',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Loạn thế kiêu hùng',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2097,2098,2099,2100',
        'boss' => 
        array (
          0 => '2101',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Loạn thế kiêu hùng',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2102,2103,2104,2105',
        'boss' => 
        array (
          0 => '2106',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  45 => 
  array (
    'name' => 'Bá chủ Giang Đông',
    'start_time' => '2013-07-15 12:00:00',
    'end_time' => '2013-07-16 11:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Bá chủ Giang Đông',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2062,2063,2064,2065',
        'boss' => 
        array (
          0 => '2066',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Bá chủ Giang Đông',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2067,2068,2069,2070',
        'boss' => 
        array (
          0 => '2071',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Bá chủ Giang Đông',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2072,2073,2074,2075',
        'boss' => 
        array (
          0 => '2076',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  46 => 
  array (
    'name' => 'Nhân Vương Thục',
    'start_time' => '2013-07-16 12:00:00',
    'end_time' => '2013-07-17 11:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Nhân Vương Thục',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2077,2078,2079,2080',
        'boss' => 
        array (
          0 => '2081',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Nhân Vương Thục',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2082,2083,2084,2085',
        'boss' => 
        array (
          0 => '2086',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Nhân Vương Thục',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2087,2088,2089,2090',
        'boss' => 
        array (
          0 => '2091',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  47 => 
  array (
    'name' => 'Loạn thế kiêu hùng',
    'start_time' => '2013-07-17 12:00:00',
    'end_time' => '2013-07-18 11:59:59',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Loạn thế kiêu hùng',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2092,2093,2094,2095',
        'boss' => 
        array (
          0 => '2096',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Loạn thế kiêu hùng',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2097,2098,2099,2100',
        'boss' => 
        array (
          0 => '2101',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Loạn thế kiêu hùng',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2102,2103,2104,2105',
        'boss' => 
        array (
          0 => '2106',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  48 => 
  array (
    'name' => 'Thiên hạ ngọc ấn',
    'start_time' => '2013-08-02 12:00:00',
    'end_time' => '2013-08-09 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Thiên hạ ngọc ấn',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '754,755,757,758',
        'boss' => 
        array (
          0 => '754',
          1 => '755',
          2 => '758',
          3 => '757',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Thiên hạ ngọc ấn',
        'stamina' => 15,
        'steps' => 4,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
        ),
        'monsters' => '754,755,756,757,758,759',
        'boss' => 
        array (
          0 => '754',
          1 => '755',
          2 => '758',
          3 => '757',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Thiên hạ ngọc ấn',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '754,755,756,757,758,759',
        'boss' => 
        array (
          0 => '755',
          1 => '756',
          2 => '759',
          3 => '758',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  49 => 
  array (
    'name' => 'Trường Bản chiến',
    'start_time' => '2013-08-30 12:00:00',
    'end_time' => '2013-09-05 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Trường Bản chiến',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2116,2117,2118,2119',
        'boss' => 
        array (
          0 => '2120',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Trường Bản chiến',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2111,2112,2113,2114',
        'boss' => 
        array (
          0 => '2115',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Trường Bản chiến',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2107,2108,2109',
        'boss' => 
        array (
          0 => '2110',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  50 => 
  array (
    'name' => 'Kinh bang tế thế',
    'start_time' => '2013-08-02 12:00:00',
    'end_time' => '2013-08-09 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Kinh bang tế thế',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2130,2131,2132,2133',
        'boss' => 
        array (
          0 => '2134',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Kinh bang tế thế',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2125,2126,2127,2128',
        'boss' => 
        array (
          0 => '2129',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Kinh bang tế thế',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2121,2122,2123',
        'boss' => 
        array (
          0 => '2124',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  51 => 
  array (
    'name' => 'Chiến công hiển hách',
    'start_time' => '2013-08-23 12:00:00',
    'end_time' => '2013-08-29 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Chiến công hiển hách',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2144,2145,2146,2147',
        'boss' => 
        array (
          0 => '2148',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Chiến công hiển hách',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2139,2140,2141,2142',
        'boss' => 
        array (
          0 => '2143',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Chiến công hiển hách',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2135,2136,2137',
        'boss' => 
        array (
          0 => '2138',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  52 => 
  array (
    'name' => 'Thác hồ phụ chính',
    'start_time' => '2013-09-06 12:00:00',
    'end_time' => '2013-09-12 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Thác hồ phụ chính',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2156,2157,2158',
        'boss' => 
        array (
          0 => '2159',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Thác hồ phụ chính',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2152,2153,2154',
        'boss' => 
        array (
          0 => '2155',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Thác hồ phụ chính',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2149,2150',
        'boss' => 
        array (
          0 => '2151',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  53 => 
  array (
    'name' => 'Danh sĩ lều cỏ',
    'start_time' => '2011-07-19 12:00:12',
    'end_time' => '2011-07-25 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Trung) Danh sĩ lều cỏ',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2167,2168,2169',
        'boss' => 
        array (
          0 => '2170',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Cao) Danh sĩ lều cỏ',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2163,2164,2165',
        'boss' => 
        array (
          0 => '2166',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Siêu) Danh sĩ lều cỏ',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2160,2161',
        'boss' => 
        array (
          0 => '2162',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  54 => 
  array (
    'name' => 'Thiên hạ ấn',
    'start_time' => '2013-08-30 12:00:00',
    'end_time' => '2013-09-05 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Thiên hạ ấn',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '754,755,757,758',
        'boss' => 
        array (
          0 => '754',
          1 => '755',
          2 => '758',
          3 => '757',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Thiên hạ ấn',
        'stamina' => 15,
        'steps' => 4,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
        ),
        'monsters' => '754,755,756,757,758,759',
        'boss' => 
        array (
          0 => '754',
          1 => '755',
          2 => '758',
          3 => '757',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Thiên hạ ấn',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '754,755,756,757,758,759',
        'boss' => 
        array (
          0 => '755',
          1 => '756',
          2 => '759',
          3 => '758',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  55 => 
  array (
    'name' => 'Tết Trung thu',
    'start_time' => '2013-08-02 12:00:00',
    'end_time' => '2013-09-12 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Tết Trung thu',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '2171,2172,2173,2174,2175',
        'boss' => 
        array (
          0 => '2176',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Tết Trung thu',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2177,2178,2179,2180,2181',
        'boss' => 
        array (
          0 => '2182',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Tết Trung thu',
        'stamina' => 30,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2183,2184,2185,2186,2187',
        'boss' => 
        array (
          0 => '2188',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  56 => 
  array (
    'name' => '济世圣手',
    'start_time' => '2013-09-13 12:00:00',
    'end_time' => '2013-09-18 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中级 济世圣手',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2130,2131,2132,2133',
        'boss' => 
        array (
          0 => '2134',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上级 济世圣手',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2125,2126,2127,2128',
        'boss' => 
        array (
          0 => '2129',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超级 济世圣手',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2121,2122,2123',
        'boss' => 
        array (
          0 => '2124',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  57 => 
  array (
    'name' => '江东霸主',
    'start_time' => '2013-09-18 12:00:00',
    'end_time' => '2013-09-20 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中级 江东霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2062,2063,2064,2065',
        'boss' => 
        array (
          0 => '2066',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上级 江东霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2067,2068,2069,2070',
        'boss' => 
        array (
          0 => '2071',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超级 江东霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2072,2073,2074,2075',
        'boss' => 
        array (
          0 => '2076',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  58 => 
  array (
    'name' => '蜀中仁君',
    'start_time' => '2013-09-20 12:00:00',
    'end_time' => '2013-09-22 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中级 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2077,2078,2079,2080',
        'boss' => 
        array (
          0 => '2081',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上级 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2082,2083,2084,2085',
        'boss' => 
        array (
          0 => '2086',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超级 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2087,2088,2089,2090',
        'boss' => 
        array (
          0 => '2091',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  59 => 
  array (
    'name' => '乱世枭雄',
    'start_time' => '2013-09-22 12:00:00',
    'end_time' => '2013-09-24 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中级 乱世枭雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2092,2093,2094,2095',
        'boss' => 
        array (
          0 => '2096',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上级 乱世枭雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2097,2098,2099,2100',
        'boss' => 
        array (
          0 => '2101',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超级 乱世枭雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2102,2103,2104,2105',
        'boss' => 
        array (
          0 => '2106',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  60 => 
  array (
    'name' => '茅庐名仕',
    'start_time' => '2013-09-24 12:00:00',
    'end_time' => '2013-09-26 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中级 茅庐名仕',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2167,2168,2169',
        'boss' => 
        array (
          0 => '2170',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上级 茅庐名仕',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2163,2164,2165',
        'boss' => 
        array (
          0 => '2166',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超级 茅庐名仕',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2160,2161',
        'boss' => 
        array (
          0 => '2162',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  61 => 
  array (
    'name' => '欢度国庆5',
    'start_time' => '2013-10-08 12:00:00',
    'end_time' => '2013-10-11 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中级 华容道',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '908,909,910,911,912',
        'boss' => 
        array (
          0 => '913',
        ),
        'package' => 140,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上级 华容道',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 903,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 905,
            1 => 906,
            2 => 904,
          ),
        ),
        'monsters' => '898,899,900,901',
        'boss' => 
        array (
          0 => '907',
        ),
        'package' => 140,
        'randMonster' => 902,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超级 华容道',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 961,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 962,
            1 => 963,
          ),
        ),
        'monsters' => '957,958',
        'boss' => 
        array (
          0 => '964',
        ),
        'package' => 140,
        'randMonster' => 959,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  62 => 
  array (
    'name' => '欢度国庆3',
    'start_time' => '2013-10-03 12:00:00',
    'end_time' => '2013-10-05 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中级 葫芦口',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '924,925,926,927,928',
        'boss' => 
        array (
          0 => '929',
        ),
        'package' => 141,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上级 葫芦口',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 919,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 921,
            1 => 922,
            2 => 920,
          ),
        ),
        'monsters' => '914,915,916,917',
        'boss' => 
        array (
          0 => '923',
        ),
        'package' => 141,
        'randMonster' => 918,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超级 葫芦口',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 969,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 970,
            1 => 971,
          ),
        ),
        'monsters' => '965,966',
        'boss' => 
        array (
          0 => '972',
        ),
        'package' => 141,
        'randMonster' => 967,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  63 => 
  array (
    'name' => '欢度国庆2',
    'start_time' => '2013-09-30 12:00:00',
    'end_time' => '2013-10-03 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中级 东风祭坛',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '952,953,954,955',
        'boss' => 
        array (
          0 => '956',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上级 东风祭坛',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 947,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 949,
            1 => 948,
            2 => 950,
          ),
        ),
        'monsters' => '944,945,946',
        'boss' => 
        array (
          0 => '951',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超级 东风祭坛',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 982,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 983,
            1 => 984,
          ),
        ),
        'monsters' => '980,981',
        'boss' => 
        array (
          0 => '985',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  64 => 
  array (
    'name' => '欢度国庆1',
    'start_time' => '2013-09-27 12:00:00',
    'end_time' => '2013-09-30 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中级 中军帐',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '892,893,894,895,896',
        'boss' => 
        array (
          0 => '897',
        ),
        'package' => 139,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上级 中军帐',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 887,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 889,
            1 => 888,
            2 => 890,
          ),
        ),
        'monsters' => '882,883,884,885',
        'boss' => 
        array (
          0 => '891',
        ),
        'package' => 139,
        'randMonster' => 896,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超级 中军帐',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 878,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 879,
            1 => 880,
          ),
        ),
        'monsters' => '874,875',
        'boss' => 
        array (
          0 => '881',
        ),
        'package' => 139,
        'randMonster' => 876,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  65 => 
  array (
    'name' => '欢度国庆4',
    'start_time' => '2013-10-05 12:00:00',
    'end_time' => '2013-10-08 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中级 长坂坡',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '939,940,941,942',
        'boss' => 
        array (
          0 => '943',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上级 长坂坡',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 934,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 935,
            1 => 936,
            2 => 937,
          ),
        ),
        'monsters' => '930,931,932,933',
        'boss' => 
        array (
          0 => '938',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超级 长坂坡',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 976,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 977,
            1 => 978,
          ),
        ),
        'monsters' => '973,974,975',
        'boss' => 
        array (
          0 => '979',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  66 => 
  array (
    'name' => '白马之战',
    'start_time' => '2013-10-12 12:00:00',
    'end_time' => '2013-10-15 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 烈火 颜良',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '620,621,622,623',
        'boss' => 
        array (
          0 => '624',
        ),
        'package' => 113,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 烈火 颜良',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '625,626,627,628',
        'boss' => 
        array (
          0 => '629',
          1 => '629',
        ),
        'package' => 114,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上级 烈火 颜良',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '630,631,632,633',
        'boss' => 
        array (
          0 => '634',
          1 => '635',
          2 => '636',
        ),
        'package' => 115,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  67 => 
  array (
    'name' => '奋威将军',
    'start_time' => '2013-10-24 12:00:00',
    'end_time' => '2013-10-27 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 黑暗 周泰',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '638,639,640,641,642,643,644,645,646,647',
        'boss' => 
        array (
          0 => '648',
          1 => '649',
          2 => '650',
        ),
        'package' => 120,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 黑暗 周泰',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '651,652,653,654,655,656,657,658,659,660',
        'boss' => 
        array (
          0 => '661',
          1 => '662',
          2 => '663',
        ),
        'package' => 121,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上级 黑暗 周泰',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '664,665,666,667,668,669,670,671,672,673',
        'boss' => 
        array (
          0 => '674',
          1 => '675',
          2 => '676',
        ),
        'package' => 122,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  68 => 
  array (
    'name' => '虎步关右',
    'start_time' => '2013-10-21 12:00:00',
    'end_time' => '2013-10-24 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 夏侯渊',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '677,678,679,680',
        'boss' => 
        array (
          0 => '681',
        ),
        'package' => 123,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 夏侯渊',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '682,683,684,685',
        'boss' => 
        array (
          0 => '686',
          1 => '687',
        ),
        'package' => 124,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上级 夏侯渊',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '688,689,690,691',
        'boss' => 
        array (
          0 => '692',
          1 => '693',
          2 => '694',
        ),
        'package' => 125,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  69 => 
  array (
    'name' => '独眼悍将',
    'start_time' => '2013-10-18 12:00:00',
    'end_time' => '2013-10-21 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 夏侯惇',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '695,696,697,698',
        'boss' => 
        array (
          0 => '699',
        ),
        'package' => 126,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 夏侯惇',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '700,701,702,703',
        'boss' => 
        array (
          0 => '704',
          1 => '705',
        ),
        'package' => 127,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上级 夏侯惇',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '706,707,708,709',
        'boss' => 
        array (
          0 => '710',
          1 => '711',
          2 => '712',
        ),
        'package' => 128,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  70 => 
  array (
    'name' => '古之恶来',
    'start_time' => '2013-10-12 12:00:00',
    'end_time' => '2013-10-18 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 苍劲 典韦',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '713,714,715,716',
        'boss' => 
        array (
          0 => '717',
        ),
        'package' => 129,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 苍劲 典韦',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '718,719,720,721',
        'boss' => 
        array (
          0 => '722',
          1 => '723',
        ),
        'package' => 130,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上级 苍劲 典韦',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '724,725,726,727',
        'boss' => 
        array (
          0 => '728',
          1 => '729',
          2 => '730',
        ),
        'package' => 131,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  71 => 
  array (
    'name' => '万圣节',
    'start_time' => '2013-08-02 12:00:00',
    'end_time' => '2013-11-12 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 万圣节',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '2171,2172,2173,2174,2175',
        'boss' => 
        array (
          0 => '2176',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 万圣节',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '2177,2178,2179,2180,2181',
        'boss' => 
        array (
          0 => '2182',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '高级 万圣节',
        'stamina' => 20,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '2183,2184,2185,2186,2187',
        'boss' => 
        array (
          0 => '2188',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  72 => 
  array (
    'name' => 'Thiên hạ kì sĩ',
    'start_time' => '2013-01-27 10:00:00',
    'end_time' => '2014-02-10 14:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '(Sơ) Quách Gia',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '503,504,501,505',
        'boss' => 
        array (
          0 => '502',
        ),
        'package' => 116,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '(Trung) Quách Gia',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 507,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 508,
            1 => 510,
            2 => 509,
          ),
        ),
        'monsters' => '503,504,505,506',
        'boss' => 
        array (
          0 => '511',
        ),
        'package' => 117,
        'randMonster' => 1001,
        'randDrop' => 118,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '(Cao) Quách Gia',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 515,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 516,
            1 => 517,
          ),
        ),
        'monsters' => '512,513,514',
        'boss' => 
        array (
          0 => '518',
        ),
        'package' => 119,
        'randMonster' => 1002,
        'randDrop' => 118,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
);
?>