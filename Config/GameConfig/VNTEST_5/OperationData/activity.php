<?php
return array (
  100 => 
  array (
    1 => 
    array (
      'subType' => NULL,
      'desc' => 'Thần tướng 1+1',
      'start_time' => '2013-08-13 00:00:00',
      'end_time' => '2013-08-19 00:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    18 => 
    array (
      'subType' => NULL,
      'desc' => 'Thần tướng 1+1',
      'start_time' => '2013-01-27 10:00:00',
      'end_time' => '2014-02-10 14:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  101 => 
  array (
    2 => 
    array (
      'subType' => NULL,
      'desc' => 'Cường hóa siêu thành công x2',
      'start_time' => '2013-08-13 00:00:00',
      'end_time' => '2013-08-19 00:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    16 => 
    array (
      'subType' => NULL,
      'desc' => 'Đại Cường Hóa',
      'start_time' => '2013-01-31 00:00:00',
      'end_time' => '2014-02-02 23:59:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    17 => 
    array (
      'subType' => NULL,
      'desc' => 'Đại Cường Hóa',
      'start_time' => '2013-02-08 10:00:00',
      'end_time' => '2014-02-10 14:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  200 => 
  array (
    3 => 
    array (
      'subType' => NULL,
      'desc' => 'Đăng nhập tặng điểm chi viện, Xu',
      'start_time' => '2013-08-13 00:00:00',
      'end_time' => '2013-08-19 00:00:00',
      'coin' => 0,
      'gold' => 3000,
      'gachaPoint' => 100,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    4 => 
    array (
      'subType' => NULL,
      'desc' => 'Đăng nhập mỗi ngày tặng KNB',
      'start_time' => '2012-07-23 00:00:00',
      'end_time' => '2012-07-28 00:00:00',
      'coin' => 6,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    5 => 
    array (
      'subType' => 10,
      'desc' => 'Đăng nhập liên tiếp',
      'start_time' => '2012-07-23 00:00:00',
      'end_time' => '2012-08-02 00:00:00',
      'coin' => 100,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    13 => 
    array (
      'subType' => NULL,
      'desc' => 'Đăng nhập nhận Lì Xì',
      'start_time' => '2013-01-31 00:00:00',
      'end_time' => '2014-01-31 23:59:00',
      'coin' => 0,
      'gold' => 10000,
      'gachaPoint' => 1000,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    14 => 
    array (
      'subType' => NULL,
      'desc' => 'Đăng nhập nhận Lì Xì',
      'start_time' => '2013-02-01 00:00:00',
      'end_time' => '2014-02-01 23:59:00',
      'coin' => 0,
      'gold' => 30000,
      'gachaPoint' => 1500,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    15 => 
    array (
      'subType' => NULL,
      'desc' => 'Đăng nhập nhận Lì Xì',
      'start_time' => '2013-02-02 00:00:00',
      'end_time' => '2014-02-02 23:59:00',
      'coin' => 0,
      'gold' => 50000,
      'gachaPoint' => 2000,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  102 => 
  array (
    6 => 
    array (
      'subType' => NULL,
      'desc' => 'Nhân đôi lần nạp đầu',
      'start_time' => '2013-08-13 00:00:00',
      'end_time' => '2013-08-19 00:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    11 => 
    array (
      'subType' => NULL,
      'desc' => 'Nhân đôi lần nạp đầu',
      'start_time' => '2013-01-27 10:00:00',
      'end_time' => '2014-02-05 23:59:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    12 => 
    array (
      'subType' => NULL,
      'desc' => 'Nhân đôi lần nạp đầu',
      'start_time' => '2013-02-08 10:00:00',
      'end_time' => '2014-02-10 14:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  110 => 
  array (
    7 => 
    array (
      'subType' => NULL,
      'desc' => 'Thần khí 1+1',
      'start_time' => '2013-08-13 00:00:00',
      'end_time' => '2013-08-13 00:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  103 => 
  array (
    10 => 
    array (
      'subType' => NULL,
      'desc' => '限时多倍经验',
      'start_time' => '2013-10-01 00:00:00',
      'end_time' => '2013-10-15 00:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 1.5,
      'pattern' => 1,
      'custom' => '',
    ),
  ),
);
?>