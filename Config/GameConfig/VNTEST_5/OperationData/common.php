<?php
return array (
  'help' => 'http://kco.vn/Ingame/Manual.html',
  'dungeon_clear_coin' => 5,
  'revive_coin' => 9,
  'max_gacha_point' => 10000,
  'app_url' => NULL,
  'gacha_cost_coin' => 45,
  'gacha_cost_10_coin' => 340,
  'weapon_cost_coin' => 8,
  'weapon_cost_10_coin' => 60,
  'weapon_cost_pt' => 125,
  'gacha_cost_pt' => 200,
  'card_extend_coin' => 9,
  'recover_stamina_coin' => 9,
  'card_extend_num' => 5,
  'voice_icon' => NULL,
  'gacha_notice' => 'gacha_notice.html?ver=12092201',
  'aboutus' => 'aboutus.html?ver=12073101',
  'tapjoy_fg' => NULL,
  'praise' => NULL,
  'admob_fg' => NULL,
  'stamina_recover_time' => 10,
  'maxLevel' => 240,
  'hpCoefficient1' => 1,
  'hpCoefficient2' => 1,
  'hpCoefficient3' => 5,
  'atkCoefficient1' => 1,
  'atkCoefficient2' => 1,
  'atkCoefficient3' => 3,
  'defCoefficient1' => 2,
  'defCoefficient2' => 170,
  'defCoefficient3' => 1,
  'criCoefficient1' => 3,
  'criCoefficient2' => 250,
  'dodCoefficient1' => 3,
  'dodCoefficient2' => 300,
  'pointHelp' => 'http://kco.vn/Ingame/Manual.html',
  'arenaRefreshTimesCoin' => 9,
  'arenaRefreshEnemyCoin' => 2,
  'disablePayment' => 1,
);
?>