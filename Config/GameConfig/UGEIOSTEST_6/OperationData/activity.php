<?php
return array (
  100 => 
  array (
    1 => 
    array (
      'subType' => 1,
      'desc' => '神将1+1',
      'start_time' => '2013-12-05 10:00:00',
      'end_time' => '2013-12-08 23:59:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  101 => 
  array (
    2 => 
    array (
      'subType' => 2,
      'desc' => '强化2倍UP',
      'start_time' => '2013-12-05 10:00:00',
      'end_time' => '2013-12-11 23:59:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    8 => 
    array (
      'subType' => 2,
      'desc' => '强化2倍UP',
      'start_time' => '2013-01-31 12:00:00',
      'end_time' => '2014-02-06 12:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  102 => 
  array (
    3 => 
    array (
      'subType' => 3,
      'desc' => '限时首冲',
      'start_time' => '2014-02-01 12:00:00',
      'end_time' => '2014-02-03 12:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  103 => 
  array (
    4 => 
    array (
      'subType' => 8,
      'desc' => '庆元旦经验爽翻天',
      'start_time' => '2013-12-31 12:00:00',
      'end_time' => '2014-01-03 12:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 2,
      'pattern' => 0,
      'custom' => '',
    ),
    9 => 
    array (
      'subType' => 8,
      'desc' => '经验翻倍闹新春',
      'start_time' => '2013-01-31 12:00:00',
      'end_time' => '2014-02-06 12:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 2,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  200 => 
  array (
    5 => 
    array (
      'subType' => 5,
      'desc' => '天天登录送元宝',
      'start_time' => '2013-12-05 10:00:00',
      'end_time' => '2013-12-12 23:59:00',
      'coin' => 5,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    6 => 
    array (
      'subType' => 6,
      'desc' => '天天登录送金币',
      'start_time' => '2013-12-17 10:00:00',
      'end_time' => '2013-12-23 23:59:00',
      'coin' => 0,
      'gold' => 5000,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    7 => 
    array (
      'subType' => NULL,
      'desc' => '新春登陆送好礼',
      'start_time' => '2013-01-31 00:00:00',
      'end_time' => '2014-02-06 23:59:00',
      'coin' => 5,
      'gold' => 10000,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  '' => 
  array (
    '' => 
    array (
      'subType' => 7,
      'desc' => '圣诞节登陆就送元宝',
      'start_time' => '2013-12-24 00:00:00',
      'end_time' => '2013-12-30 00:00:00',
      'coin' => 5,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
);
?>