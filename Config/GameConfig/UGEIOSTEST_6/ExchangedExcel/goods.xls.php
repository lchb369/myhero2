<?php
return array (
  'columns' => 
  array (
    0 => 'goodsId',
    1 => 'goodsName',
    2 => 'goodsDesc',
    3 => 'price',
    4 => 'coin',
    5 => 'limit',
    6 => 'AppStore',
    7 => 'AppStoreId',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'goodsId' => 1,
      'goodsName' => 'goods01',
      'goodsDesc' => '6個元寶',
      'price' => '6',
      'coin' => 6,
      'limit' => 0,
      'AppStore' => '6',
      'AppStoreId' => '001',
    ),
    1 => 
    array (
      'goodsId' => 2,
      'goodsName' => 'goods02',
      'goodsDesc' => '30個元寶',
      'price' => '30',
      'coin' => 30,
      'limit' => 0,
      'AppStore' => '30',
      'AppStoreId' => '009',
    ),
    2 => 
    array (
      'goodsId' => 3,
      'goodsName' => 'goods03',
      'goodsDesc' => '71個元寶',
      'price' => '68',
      'coin' => 71,
      'limit' => 0,
      'AppStore' => '68',
      'AppStoreId' => '010',
    ),
    3 => 
    array (
      'goodsId' => 4,
      'goodsName' => 'goods04',
      'goodsDesc' => '141個元寶',
      'price' => '128',
      'coin' => 141,
      'limit' => 0,
      'AppStore' => '128',
      'AppStoreId' => '011',
    ),
    4 => 
    array (
      'goodsId' => 5,
      'goodsName' => 'goods05',
      'goodsDesc' => '331個元寶',
      'price' => '288',
      'coin' => 331,
      'limit' => 0,
      'AppStore' => '288',
      'AppStoreId' => '012',
    ),
    5 => 
    array (
      'goodsId' => 6,
      'goodsName' => 'goods06',
      'goodsDesc' => '706個元寶',
      'price' => '588',
      'coin' => 706,
      'limit' => 0,
      'AppStore' => '588',
      'AppStoreId' => '013',
    ),
    6 => 
    array (
      'goodsId' => 7,
      'goodsName' => 'goods07',
      'goodsDesc' => '1248個元寶',
      'price' => '998',
      'coin' => 1248,
      'limit' => 0,
      'AppStore' => '998',
      'AppStoreId' => '014',
    ),
    7 => 
    array (
      'goodsId' => 101,
      'goodsName' => 'goods101',
      'goodsDesc' => '100個元寶',
      'price' => '50',
      'coin' => 100,
      'limit' => 1,
      'AppStore' => '50',
      'AppStoreId' => '015',
    ),
  ),
);
?>