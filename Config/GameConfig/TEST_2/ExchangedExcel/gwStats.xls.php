<?php
return array (
  'columns' => 
  array (
    0 => 'UC平台',
    1 => '41389',
    2 => '41390',
    3 => '41391',
    4 => '41392',
    5 => '41393',
    6 => '41394',
    7 => '41395',
    8 => '41396',
    9 => '41397',
    10 => '41398',
    11 => '41399',
    12 => '41400',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'UC平台' => '新注册登录',
      41389 => 3325,
      41390 => 1562,
      41391 => 855,
      41392 => 890,
      41393 => 3960,
      41394 => 2149,
      41395 => 1518,
      41396 => 1217,
      41397 => 631,
      41398 => NULL,
      41399 => NULL,
      41400 => NULL,
    ),
    1 => 
    array (
      'UC平台' => '日活跃用户',
      41389 => 3325,
      41390 => '=B4*B7+C3',
      41391 => '=D3+C3*C7+B4*B8',
      41392 => '=E3+D3*D7+C3*C8+B3*B9',
      41393 => '=F3+E3*E7+D3*D8+C3*C9+B3*B10',
      41394 => '=G3+F3*F7+E3*E8+D3*D9+C3*C10+B3*B11',
      41395 => '=H3+G3*G7+F3*F8+E3*E9+D3*D10+C3*C11+B3*B12',
      41396 => '=I3+H3*H7+G3*G8+F3*F9+E3*E10+D3*D11+C3*C12',
      41397 => '=J3+I3*I7+H3*H8+G3*G9+F3*F10+E3+D3*D12',
      41398 => NULL,
      41399 => NULL,
      41400 => NULL,
    ),
    2 => 
    array (
      'UC平台' => '订单总量',
      41389 => '=SUM(B14:B21)',
      41390 => '=SUM(C14:C21)',
      41391 => '=SUM(D14:D21)',
      41392 => '=SUM(E14:E21)',
      41393 => '=SUM(F14:F21)',
      41394 => '=SUM(G14:G21)',
      41395 => '=SUM(H14:H21)',
      41396 => '=SUM(I14:I21)',
      41397 => '=SUM(J14:J21)',
      41398 => NULL,
      41399 => NULL,
      41400 => NULL,
    ),
    3 => 
    array (
      'UC平台' => '订单金额',
      41389 => '=B14*A14+B15*A15+B16*A16+B17*A17+B18*A18+B19*A19+B20*A20+B21*A21',
      41390 => '=A14*C14+A15*C15+A16*C16+A17*C17+A18*C18+A19*C19+A20*C20+A21*C21',
      41391 => '=A14*D14+D15*A15+D16*A16+D17*A17+D18*A18+D19*A19+D20*A20+D21*A21',
      41392 => '=E14*A14+E15*A15+E16*A16+E17*A17+E18*A18+E19*A19+E20*A20+E21*A21',
      41393 => '=F14*A14+F15*A15+F16*A16+F17*A17+F18*A18+F19*A19+F20*A20+F21*A21',
      41394 => '=G14*A14+G15*A15+G16*A16+G17*A17+G18*A18+G19*A19+G20*A20+G21*A21',
      41395 => '=H14*A14+H15*A15+H16*A16+H17*A17+H18*A18+H19*A19+H20*A20+H21*A21',
      41396 => NULL,
      41397 => NULL,
      41398 => NULL,
      41399 => NULL,
      41400 => NULL,
    ),
    4 => 
    array (
      'UC平台' => '次日留存',
      41389 => 0.2894,
      41390 => 0.345,
      41391 => 0.4322,
      41392 => 0.3512,
      41393 => 0.2541,
      41394 => 0.3233,
      41395 => 0.2683,
      41396 => 0.301,
      41397 => NULL,
      41398 => '·',
      41399 => NULL,
      41400 => NULL,
    ),
    5 => 
    array (
      'UC平台' => '2日留存',
      41389 => 0.2171,
      41390 => 0.2588,
      41391 => 0.3142,
      41392 => 0.2634,
      41393 => 0.1906,
      41394 => 0.2424,
      41395 => NULL,
      41396 => NULL,
      41397 => NULL,
      41398 => NULL,
      41399 => NULL,
      41400 => NULL,
    ),
    6 => 
    array (
      'UC平台' => '3日留存',
      41389 => 0.2084,
      41390 => 0.2484,
      41391 => 0.3112,
      41392 => 0.2529,
      41393 => 0.183,
      41394 => NULL,
      41395 => NULL,
      41396 => NULL,
      41397 => NULL,
      41398 => NULL,
      41399 => NULL,
      41400 => NULL,
    ),
    7 => 
    array (
      'UC平台' => '4日留存',
      41389 => 0.1881,
      41390 => 0.2243,
      41391 => 0.2809,
      41392 => 0.2293,
      41393 => NULL,
      41394 => NULL,
      41395 => NULL,
      41396 => NULL,
      41397 => NULL,
      41398 => NULL,
      41399 => NULL,
      41400 => NULL,
    ),
    8 => 
    array (
      'UC平台' => '5日留存',
      41389 => 0.1592,
      41390 => 0.1898,
      41391 => 0.2377,
      41392 => NULL,
      41393 => NULL,
      41394 => NULL,
      41395 => NULL,
      41396 => NULL,
      41397 => NULL,
      41398 => NULL,
      41399 => NULL,
      41400 => NULL,
    ),
    9 => 
    array (
      'UC平台' => '7日留存',
      41389 => 0.0796,
      41390 => 0.0949,
      41391 => NULL,
      41392 => NULL,
      41393 => NULL,
      41394 => NULL,
      41395 => NULL,
      41396 => NULL,
      41397 => NULL,
      41398 => NULL,
      41399 => NULL,
      41400 => NULL,
    ),
    10 => 
    array (
      'UC平台' => '14日留存',
      41389 => NULL,
      41390 => NULL,
      41391 => NULL,
      41392 => NULL,
      41393 => NULL,
      41394 => NULL,
      41395 => NULL,
      41396 => NULL,
      41397 => NULL,
      41398 => NULL,
      41399 => NULL,
      41400 => NULL,
    ),
  ),
);
?>