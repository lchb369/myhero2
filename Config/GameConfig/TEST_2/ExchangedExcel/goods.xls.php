<?php
return array (
  'columns' => 
  array (
    0 => 'goodsId',
    1 => 'goodsName',
    2 => 'goodsDesc',
    3 => 'price',
    4 => 'coin',
    5 => 'limit',
    6 => 'AppStore',
    7 => 'AppStoreId',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'goodsId' => 1,
      'goodsName' => 'goods01',
      'goodsDesc' => '원보6개',
      'price' => 2200,
      'coin' => 6,
      'limit' => 0,
      'AppStore' => 2200,
      'AppStoreId' => 'avo_pzsam_kakao_item001',
    ),
    1 => 
    array (
      'goodsId' => 2,
      'goodsName' => 'goods02',
      'goodsDesc' => '원보15개',
      'price' => 5500,
      'coin' => '15',
      'limit' => 0,
      'AppStore' => 5500,
      'AppStoreId' => 'avo_pzsam_kakao_item002',
    ),
    2 => 
    array (
      'goodsId' => 3,
      'goodsName' => 'goods03',
      'goodsDesc' => '원보30개',
      'price' => 11000,
      'coin' => '30',
      'limit' => 0,
      'AppStore' => 11000,
      'AppStoreId' => 'avo_pzsam_kakao_item003',
    ),
    3 => 
    array (
      'goodsId' => 4,
      'goodsName' => 'goods04',
      'goodsDesc' => '원보80개',
      'price' => 27500,
      'coin' => '80',
      'limit' => 0,
      'AppStore' => 27500,
      'AppStoreId' => 'avo_pzsam_kakao_item004',
    ),
    4 => 
    array (
      'goodsId' => 5,
      'goodsName' => 'goods05',
      'goodsDesc' => '원보170개',
      'price' => 55000,
      'coin' => '170',
      'limit' => 0,
      'AppStore' => 55000,
      'AppStoreId' => 'avo_pzsam_kakao_item005',
    ),
    5 => 
    array (
      'goodsId' => 6,
      'goodsName' => 'goods06',
      'goodsDesc' => '원보375개',
      'price' => 110000,
      'coin' => '375',
      'limit' => 0,
      'AppStore' => 110000,
      'AppStoreId' => 'avo_pzsam_kakao_item006',
    ),
  ),
);
?>