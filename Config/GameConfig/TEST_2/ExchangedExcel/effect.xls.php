<?php
return array (
  'columns' => 
  array (
    0 => 'effectId',
    1 => 'name',
    2 => 'typeName1',
    3 => 'typeValue1',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'effectId' => 1,
      'name' => '体力消耗减少1/2',
      'typeName1' => 'stamina',
      'typeValue1' => 0.5,
    ),
  		
    1 => 
    array (
      'effectId' => 2,
      'name' => '武将掉落2倍',
      'typeName1' => 'gold',
      'typeValue1' => 2,
    ),
    2 => 
    array (
      'effectId' => 3,
      'name' => '铜钱掉落率2倍',
      'typeName1' => 'card',
      'typeValue1' => 2,
    ),
    3 => 
    array (
      'effectId' => 4,
      'name' => '武将掉落率1.5倍',
      'typeName1' => 'card',
      'typeValue1' => 1.5,
    ),
  ),
);
?>