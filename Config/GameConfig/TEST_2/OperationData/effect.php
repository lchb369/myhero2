<?php
return array (
  1 => 
  array (
    'name' => '体力消耗减少1/2',
    'type' => 
    array (
      'stamina' => 0.5,
    ),
  ),
  2 => 
  array (
    'name' => '武将掉落2倍',
    'type' => 
    array (
      'gold' => 2,
    ),
  ),
  3 => 
  array (
    'name' => '铜钱掉落率2倍',
    'type' => 
    array (
      'card' => 2,
    ),
  ),
  4 => 
  array (
    'name' => '武将掉落率1.5倍',
    'type' => 
    array (
      'card' => 1.5,
    ),
  ),
);
?>