<?php
return array (
  'columns' => 
  array (
    0 => 'id',
    1 => 'prizeCoin',
    2 => 'prizeGold',
    3 => 'prizePoint',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'id' => 1,
      'prizeCoin' => 0,
      'prizeGold' => 500,
      'prizePoint' => 0,
    ),
    1 => 
    array (
      'id' => 2,
      'prizeCoin' => 0,
      'prizeGold' => 0,
      'prizePoint' => 200,
    ),
    2 => 
    array (
      'id' => 3,
      'prizeCoin' => 1,
      'prizeGold' => 1000,
      'prizePoint' => 0,
    ),
    3 => 
    array (
      'id' => 4,
      'prizeCoin' => 1,
      'prizeGold' => 0,
      'prizePoint' => 300,
    ),
    4 => 
    array (
      'id' => 5,
      'prizeCoin' => 1,
      'prizeGold' => 2000,
      'prizePoint' => 0,
    ),
    5 => 
    array (
      'id' => 6,
      'prizeCoin' => 1,
      'prizeGold' => 0,
      'prizePoint' => 400,
    ),
    6 => 
    array (
      'id' => 7,
      'prizeCoin' => 1,
      'prizeGold' => 3000,
      'prizePoint' => 0,
    ),
  ),
);
?>