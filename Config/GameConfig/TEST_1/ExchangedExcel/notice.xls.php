<?php
return array (
  'columns' => 
  array (
    0 => 'id',
    1 => 'enable',
    2 => 'start_time',
    3 => 'end_time',
    4 => 'interval',
    5 => 'msg',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'id' => 1,
      'enable' => 0,
      'start_time' => '2013-12-01 00:00:00',
      'end_time' => '2014-12-01 00:00:00',
      'interval' => 15,
      'msg' => 'The quick brown fox jumps over a lazy dog.',
    ),
    1 => 
    array (
      'id' => 2,
      'enable' => 1,
      'start_time' => '2013-12-01 00:00:00',
      'end_time' => '2014-12-01 00:00:00',
      'interval' => 5,
      'msg' => 'J.Q.Schwartz flung D.V.Pike my box.',
    ),
    2 => 
    array (
      'id' => 3,
      'enable' => 1,
      'start_time' => '2013-12-01 00:00:00',
      'end_time' => '2014-12-30 00:00:00',
      'interval' => 7,
      'msg' => 'Waltz nymph，for quickj igs vex Bud',
    ),
  ),
);
?>