<?php
return array (
  'columns' => 
  array (
    0 => 'goodsId',
    1 => 'goodsName',
    2 => 'goodsDesc',
    3 => 'price',
    4 => 'coin',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'goodsId' => 1,
      'goodsName' => 'goods01',
      'goodsDesc' => '1个元宝',
      'price' => 1,
      'coin' => 1,
    ),
    1 => 
    array (
      'goodsId' => 2,
      'goodsName' => 'goods02',
      'goodsDesc' => '6个元宝',
      'price' => '6',
      'coin' => 6,
    ),
    2 => 
    array (
      'goodsId' => 3,
      'goodsName' => 'goods03',
      'goodsDesc' => '10个元宝',
      'price' => '10',
      'coin' => 10,
    ),
    3 => 
    array (
      'goodsId' => 4,
      'goodsName' => 'goods04',
      'goodsDesc' => '30个元宝',
      'price' => '30',
      'coin' => 30,
    ),
    4 => 
    array (
      'goodsId' => 5,
      'goodsName' => 'goods05',
      'goodsDesc' => '110个元宝',
      'price' => '100',
      'coin' => 110,
    ),
    5 => 
    array (
      'goodsId' => 6,
      'goodsName' => 'goods06',
      'goodsDesc' => '345个元宝',
      'price' => '300',
      'coin' => 345,
    ),
    6 => 
    array (
      'goodsId' => 7,
      'goodsName' => 'goods07',
      'goodsDesc' => '720个元宝',
      'price' => '600',
      'coin' => 720,
    ),
    7 => 
    array (
      'goodsId' => 8,
      'goodsName' => 'goods08',
      'goodsDesc' => '1170个元宝',
      'price' => '900',
      'coin' => 1170,
    ),
  ),
);
?>