<?php
return array (
  'columns' => 
  array (
    0 => 'id',
    1 => 'type',
    2 => 'desc',
    3 => 'startTime',
    4 => 'endTime',
    5 => 'paidCoin',
    6 => 'prizeCard',
    7 => 'prizeCardNum',
    8 => 'prizeDesc',
  ),
  'datas' => 
  array (
    0 => 
    array (
      'id' => 1,
      'type' => 3,
      'desc' => '누적하여 2200원 이상 충전 시 보너스',
      'startTime' => '2013-12-05 10:00:00',
      'endTime' => '2014-12-12 23:59:00',
      'paidCoin' => 6,
      'prizeCard' => '336_card',
      'prizeCardNum' => 1,
      'prizeDesc' => '황충부인',
    ),
    1 => 
    array (
      'id' => 2,
      'type' => 3,
      'desc' => '누적하여 5500원 이상 충전 시 보너스',
      'startTime' => '2013-12-05 10:00:00',
      'endTime' => '2014-12-12 23:59:00',
      'paidCoin' => 15,
      'prizeCard' => '337_card',
      'prizeCardNum' => 1,
      'prizeDesc' => '황충부인',
    ),
    2 => 
    array (
      'id' => 3,
      'type' => 3,
      'desc' => '누적하여 11000원 이상 충전 시 보너스',
      'startTime' => '2013-12-05 10:00:00',
      'endTime' => '2014-12-12 23:59:00',
      'paidCoin' => 30,
      'prizeCard' => '338_card',
      'prizeCardNum' => 1,
      'prizeDesc' => '장비부인',
    ),
    3 => 
    array (
      'id' => 4,
      'type' => 3,
      'desc' => '누적하여 27500원 이상 충전 시 보너스',
      'startTime' => '2013-12-05 10:00:00',
      'endTime' => '2014-12-12 23:59:00',
      'paidCoin' => 80,
      'prizeCard' => '339_card',
      'prizeCardNum' => 1,
      'prizeDesc' => '장비부인',
    ),
    4 => 
    array (
      'id' => 5,
      'type' => 1,
      'desc' => '원보 누적 사용량 100 도달 시 보너스',
      'startTime' => '2013-12-05 10:00:00',
      'endTime' => '2014-12-12 23:59:00',
      'paidCoin' => 6,
      'prizeCard' => '340_card',
      'prizeCardNum' => 1,
      'prizeDesc' => '단팥월병',
    ),
    5 => 
    array (
      'id' => 6,
      'type' => 1,
      'desc' => '원보 누적 사용량 200 도달 시 보너스',
      'startTime' => '2013-12-05 10:00:00',
      'endTime' => '2014-12-12 23:59:00',
      'paidCoin' => 15,
      'prizeCard' => '341_card',
      'prizeCardNum' => 1,
      'prizeDesc' => '블루베리월병',
    ),
    6 => 
    array (
      'id' => 7,
      'type' => 1,
      'desc' => '원보 누적 사용량 400 도달 시 보너스',
      'startTime' => '2013-12-05 10:00:00',
      'endTime' => '2014-12-12 23:59:00',
      'paidCoin' => 30,
      'prizeCard' => '342_card',
      'prizeCardNum' => 1,
      'prizeDesc' => '녹차월병',
    ),
    7 => 
    array (
      'id' => 8,
      'type' => 1,
      'desc' => '원보 누적 사용량 800 도달 시 보너스',
      'startTime' => '2013-12-05 10:00:00',
      'endTime' => '2014-12-12 23:59:00',
      'paidCoin' => 80,
      'prizeCard' => '343_card',
      'prizeCardNum' => 1,
      'prizeDesc' => '크림월병',
    ),
    8 => 
    array (
      'id' => 9,
      'type' => 2,
      'desc' => '이벤트 기간 중 첫 충전 보너스',
      'startTime' => '2013-12-05 10:00:00',
      'endTime' => '2014-12-12 23:59:00',
      'paidCoin' => 0,
      'prizeCard' => '344_card',
      'prizeCardNum' => 1,
      'prizeDesc' => '땅콩월병',
    ),
    9 => 
    array (
      'id' => 13,
      'type' => 4,
      'desc' => '1회에 2200원 이상 충전 시 보너스',
      'startTime' => '2013-12-05 10:00:00',
      'endTime' => '2014-12-12 23:59:00',
      'paidCoin' => 6,
      'prizeCard' => '345_card',
      'prizeCardNum' => 1,
      'prizeDesc' => '옥토끼',
    ),
  ),
);
?>