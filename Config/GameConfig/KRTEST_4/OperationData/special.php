<?php
return array (
  1 => 
  array (
    'name' => '白馬之戰',
    'start_time' => '2013-08-12 12:00:00',
    'end_time' => '2013-08-15 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 烈火 顏良',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '620,621,622,623',
        'boss' => 
        array (
          0 => '624',
        ),
        'package' => 113,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 烈火 顏良',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '625,626,627,628',
        'boss' => 
        array (
          0 => '629',
          1 => '629',
        ),
        'package' => 114,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上級 烈火 顏良',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '630,631,632,633',
        'boss' => 
        array (
          0 => '634',
          1 => '635',
          2 => '636',
        ),
        'package' => 115,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  2 => 
  array (
    'name' => '世之奇士',
    'start_time' => '2013-09-11 12:00:00',
    'end_time' => '2013-09-14 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 烈焰 郭嘉',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '503,504,501,505',
        'boss' => 
        array (
          0 => '502',
        ),
        'package' => 116,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 烈焰 郭嘉',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 507,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 508,
            1 => 510,
            2 => 509,
          ),
        ),
        'monsters' => '503,504,505,506',
        'boss' => 
        array (
          0 => '511',
        ),
        'package' => 117,
        'randMonster' => 1001,
        'randDrop' => 118,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上級 烈焰 郭嘉',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 515,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 516,
            1 => 517,
          ),
        ),
        'monsters' => '512,513,514',
        'boss' => 
        array (
          0 => '518',
        ),
        'package' => 119,
        'randMonster' => 1002,
        'randDrop' => 118,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  3 => 
  array (
    'name' => '奮威將軍',
    'start_time' => '2013-09-09 12:00:00',
    'end_time' => '2013-10-24 23:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 黑暗 周泰',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '638,639,640,641,642,643,644,645,646,647',
        'boss' => 
        array (
          0 => '648',
          1 => '649',
          2 => '650',
        ),
        'package' => 120,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 黑暗 周泰',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '651,652,653,654,655,656,657,658,659,660',
        'boss' => 
        array (
          0 => '661',
          1 => '662',
          2 => '663',
        ),
        'package' => 121,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上級 黑暗 周泰',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '664,665,666,667,668,669,670,671,672,673',
        'boss' => 
        array (
          0 => '674',
          1 => '675',
          2 => '676',
        ),
        'package' => 122,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  4 => 
  array (
    'name' => '虎步關右',
    'start_time' => '2013-09-02 12:00:00',
    'end_time' => '2013-09-05 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 夏侯淵',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '677,678,679,680',
        'boss' => 
        array (
          0 => '681',
        ),
        'package' => 123,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 夏侯淵',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '682,683,684,685',
        'boss' => 
        array (
          0 => '686',
          1 => '687',
        ),
        'package' => 124,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上級 夏侯淵',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '688,689,690,691',
        'boss' => 
        array (
          0 => '692',
          1 => '693',
          2 => '694',
        ),
        'package' => 125,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  5 => 
  array (
    'name' => '獨眼悍將',
    'start_time' => '2013-08-26 12:00:00',
    'end_time' => '2013-08-29 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 夏侯惇',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '695,696,697,698',
        'boss' => 
        array (
          0 => '699',
        ),
        'package' => 126,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 夏侯惇',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '700,701,702,703',
        'boss' => 
        array (
          0 => '704',
          1 => '705',
        ),
        'package' => 127,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上級 夏侯惇',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '706,707,708,709',
        'boss' => 
        array (
          0 => '710',
          1 => '711',
          2 => '712',
        ),
        'package' => 128,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  6 => 
  array (
    'name' => '古之惡來',
    'start_time' => '2013-08-19 12:00:00',
    'end_time' => '2013-08-22 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 蒼勁 典韋',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '713,714,715,716',
        'boss' => 
        array (
          0 => '717',
        ),
        'package' => 129,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 蒼勁 典韋',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '718,719,720,721',
        'boss' => 
        array (
          0 => '722',
          1 => '723',
        ),
        'package' => 130,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上級 蒼勁 典韋',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '724,725,726,727',
        'boss' => 
        array (
          0 => '728',
          1 => '729',
          2 => '730',
        ),
        'package' => 131,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  7 => 
  array (
    'name' => '天下皇璽已經使用',
    'start_time' => '2013-06-10 00:00:00',
    'end_time' => '2013-06-17 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 天下皇璽',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '754,755,757,758',
        'boss' => 
        array (
          0 => '754',
          1 => '755',
          2 => '758',
          3 => '757',
        ),
        'package' => 135,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 天下皇璽',
        'stamina' => 15,
        'steps' => 4,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
        ),
        'monsters' => '754,755,756,757,758,759',
        'boss' => 
        array (
          0 => '754',
          1 => '755',
          2 => '758',
          3 => '757',
        ),
        'package' => 136,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上級 天下皇璽',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '754,755,756,757,758,759',
        'boss' => 
        array (
          0 => '755',
          1 => '756',
          2 => '759',
          3 => '758',
        ),
        'package' => 137,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  22 => 
  array (
    'name' => '天下皇璽',
    'start_time' => '2013-07-31 00:00:00',
    'end_time' => '2013-08-07 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 天下皇璽',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '754,755,757,758',
        'boss' => 
        array (
          0 => '754',
          1 => '755',
          2 => '758',
          3 => '757',
        ),
        'package' => 135,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 天下皇璽',
        'stamina' => 15,
        'steps' => 4,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
        ),
        'monsters' => '754,755,756,757,758,759',
        'boss' => 
        array (
          0 => '754',
          1 => '755',
          2 => '758',
          3 => '757',
        ),
        'package' => 136,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上級 天下皇璽',
        'stamina' => 25,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '754,755,756,757,758,759',
        'boss' => 
        array (
          0 => '755',
          1 => '756',
          2 => '759',
          3 => '758',
        ),
        'package' => 137,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  23 => 
  array (
    'name' => '女神降臨',
    'start_time' => '2013-08-13 12:00:00',
    'end_time' => '2013-08-16 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 女神降臨',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '731,732,733,734,735',
        'boss' => 
        array (
          0 => '736',
          1 => '737',
          2 => '738',
        ),
        'package' => 132,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 女神降臨',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '739,740,741,742,743',
        'boss' => 
        array (
          0 => '744',
          1 => '745',
          2 => '746',
        ),
        'package' => 133,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上級 女神降臨',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '747,748,749,750,751,752',
        'boss' => 
        array (
          0 => '753',
        ),
        'package' => 134,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  24 => 
  array (
    'name' => '華容道',
    'start_time' => '2013-08-23 00:00:00',
    'end_time' => '2013-08-24 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 華容道',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '908,909,910,911,912',
        'boss' => 
        array (
          0 => '913',
        ),
        'package' => 140,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 華容道',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 903,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 905,
            1 => 906,
            2 => 904,
          ),
        ),
        'monsters' => '898,899,900,901',
        'boss' => 
        array (
          0 => '907',
        ),
        'package' => 140,
        'randMonster' => 902,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 華容道',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 961,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 962,
            1 => 963,
          ),
        ),
        'monsters' => '957,958',
        'boss' => 
        array (
          0 => '964',
        ),
        'package' => 140,
        'randMonster' => 959,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  25 => 
  array (
    'name' => '葫蘆口',
    'start_time' => '2013-08-30 00:00:00',
    'end_time' => '2013-08-31 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 葫蘆口',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '924,925,926,927,928',
        'boss' => 
        array (
          0 => '929',
        ),
        'package' => 141,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 葫蘆口',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 919,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 921,
            1 => 922,
            2 => 920,
          ),
        ),
        'monsters' => '914,915,916,917',
        'boss' => 
        array (
          0 => '923',
        ),
        'package' => 141,
        'randMonster' => 918,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 葫蘆口',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 969,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 970,
            1 => 971,
          ),
        ),
        'monsters' => '965,966',
        'boss' => 
        array (
          0 => '972',
        ),
        'package' => 141,
        'randMonster' => 967,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  26 => 
  array (
    'name' => '東風祭壇',
    'start_time' => '2013-09-06 00:00:00',
    'end_time' => '2013-09-07 00:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 東風祭壇',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '952,953,954,955',
        'boss' => 
        array (
          0 => '956',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 東風祭壇',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 947,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 949,
            1 => 948,
            2 => 950,
          ),
        ),
        'monsters' => '944,945,946',
        'boss' => 
        array (
          0 => '951',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 東風祭壇',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 982,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 983,
            1 => 984,
          ),
        ),
        'monsters' => '980,981',
        'boss' => 
        array (
          0 => '985',
        ),
        'package' => 143,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  27 => 
  array (
    'name' => '中軍帳',
    'start_time' => '2013-09-13 12:00:00',
    'end_time' => '2013-09-15 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 中軍帳',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '892,893,894,895,896',
        'boss' => 
        array (
          0 => '897',
        ),
        'package' => 139,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 中軍帳',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 887,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 889,
            1 => 888,
            2 => 890,
          ),
        ),
        'monsters' => '882,883,884,885',
        'boss' => 
        array (
          0 => '891',
        ),
        'package' => 139,
        'randMonster' => 896,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 中軍帳',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 878,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 879,
            1 => 880,
          ),
        ),
        'monsters' => '874,875',
        'boss' => 
        array (
          0 => '881',
        ),
        'package' => 139,
        'randMonster' => 876,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  28 => 
  array (
    'name' => '長阪坡',
    'start_time' => '2011-08-13 12:00:00',
    'end_time' => '2012-08-16 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 長阪坡',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '939,940,941,942',
        'boss' => 
        array (
          0 => '943',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 長阪坡',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 934,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 935,
            1 => 936,
            2 => 937,
          ),
        ),
        'monsters' => '930,931,932,933',
        'boss' => 
        array (
          0 => '938',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 長阪坡',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 976,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 977,
            1 => 978,
          ),
        ),
        'monsters' => '973,974,975',
        'boss' => 
        array (
          0 => '979',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  29 => 
  array (
    'name' => '江東霸主',
    'start_time' => '2013-09-27 12:00:00',
    'end_time' => '2013-09-29 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 江東霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2062,2063,2064,2065',
        'boss' => 
        array (
          0 => '2066',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 江東霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2067,2068,2069,2070',
        'boss' => 
        array (
          0 => '2071',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 江東霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2072,2073,2074,2075',
        'boss' => 
        array (
          0 => '2076',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  30 => 
  array (
    'name' => '蜀中仁君',
    'start_time' => '2013-10-04 12:00:00',
    'end_time' => '2013-10-06 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2077,2078,2079,2080',
        'boss' => 
        array (
          0 => '2081',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2082,2083,2084,2085',
        'boss' => 
        array (
          0 => '2086',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2087,2088,2089,2090',
        'boss' => 
        array (
          0 => '2091',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  31 => 
  array (
    'name' => '亂世梟雄',
    'start_time' => '2013-10-11 12:00:00',
    'end_time' => '2013-10-13 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 亂世梟雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2092,2093,2094,2095',
        'boss' => 
        array (
          0 => '2096',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 亂世梟雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2097,2098,2099,2100',
        'boss' => 
        array (
          0 => '2101',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 亂世梟雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2102,2103,2104,2105',
        'boss' => 
        array (
          0 => '2106',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  32 => 
  array (
    'name' => '長阪之戰',
    'start_time' => '2013-09-20 12:00:00',
    'end_time' => '2013-09-22 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 長阪之戰',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2116,2117,2118,2119',
        'boss' => 
        array (
          0 => '2120',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 長阪之戰',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2111,2112,2113,2114',
        'boss' => 
        array (
          0 => '2115',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 長阪之戰',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2107,2108,2109',
        'boss' => 
        array (
          0 => '2110',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  33 => 
  array (
    'name' => '濟世聖手',
    'start_time' => '2013-09-23 12:00:00',
    'end_time' => '2013-09-26 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 濟世聖手',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2130,2131,2132,2133',
        'boss' => 
        array (
          0 => '2134',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 濟世聖手',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2125,2126,2127,2128',
        'boss' => 
        array (
          0 => '2129',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 濟世聖手',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2121,2122,2123',
        'boss' => 
        array (
          0 => '2124',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  34 => 
  array (
    'name' => '戰功赫赫',
    'start_time' => '2013-09-17 12:00:00',
    'end_time' => '2013-09-20 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 戰功赫赫',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2144,2145,2146,2147',
        'boss' => 
        array (
          0 => '2148',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 戰功赫赫',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2139,2140,2141,2142',
        'boss' => 
        array (
          0 => '2143',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 戰功赫赫',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2135,2136,2137',
        'boss' => 
        array (
          0 => '2138',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  35 => 
  array (
    'name' => '托孤輔政',
    'start_time' => '2013-10-07 12:00:00',
    'end_time' => '2013-10-10 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 托孤輔政',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2156,2157,2158',
        'boss' => 
        array (
          0 => '2159',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 托孤輔政',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2152,2153,2154',
        'boss' => 
        array (
          0 => '2155',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 托孤輔政',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2149,2150',
        'boss' => 
        array (
          0 => '2151',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  36 => 
  array (
    'name' => '茅廬名仕',
    'start_time' => '2013-09-27 12:00:00',
    'end_time' => '2013-09-29 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 茅廬名仕',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2167,2168,2169',
        'boss' => 
        array (
          0 => '2170',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 茅廬名仕',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2163,2164,2165',
        'boss' => 
        array (
          0 => '2166',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 茅廬名仕',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2160,2161',
        'boss' => 
        array (
          0 => '2162',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  37 => 
  array (
    'name' => '中秋佳節',
    'start_time' => '2012-09-18 12:00:00',
    'end_time' => '2012-09-22 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 中秋佳節',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '2171,2172,2173,2174,2175',
        'boss' => 
        array (
          0 => '2176',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 中秋佳節',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2177,2178,2179,2180,2181',
        'boss' => 
        array (
          0 => '2182',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '高級 中秋佳節',
        'stamina' => 30,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2183,2184,2185,2186,2187',
        'boss' => 
        array (
          0 => '2188',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  38 => 
  array (
    'name' => '長阪坡',
    'start_time' => '2013-10-12 12:00:00',
    'end_time' => '2013-10-15 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 單騎 趙雲',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '939,940,941,942',
        'boss' => 
        array (
          0 => '943',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 單騎 趙雲',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 934,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 935,
            1 => 936,
            2 => 937,
          ),
        ),
        'monsters' => '930,931,932,933',
        'boss' => 
        array (
          0 => '938',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 單騎 趙雲',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 976,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 977,
            1 => 978,
          ),
        ),
        'monsters' => '973,974,975',
        'boss' => 
        array (
          0 => '979',
        ),
        'package' => 142,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  39 => 
  array (
    'name' => '華容道',
    'start_time' => '2013-10-16 12:00:00',
    'end_time' => '2013-10-19 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 武聖 關羽',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '908,909,910,911,912',
        'boss' => 
        array (
          0 => '913',
        ),
        'package' => 140,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 武聖 關羽',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 903,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 905,
            1 => 906,
            2 => 904,
          ),
        ),
        'monsters' => '898,899,900,901',
        'boss' => 
        array (
          0 => '907',
        ),
        'package' => 140,
        'randMonster' => 902,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 武聖 關羽',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 961,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 962,
            1 => 963,
          ),
        ),
        'monsters' => '957,958',
        'boss' => 
        array (
          0 => '964',
        ),
        'package' => 140,
        'randMonster' => 959,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  40 => 
  array (
    'name' => '巾幗不讓鬚眉',
    'start_time' => '2013-10-20 12:00:00',
    'end_time' => '2013-10-23 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 巾幗英雄',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '731,732,733,734,735',
        'boss' => 
        array (
          0 => '736',
          1 => '737',
          2 => '738',
        ),
        'package' => 132,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 巾幗英雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '739,740,741,742,743',
        'boss' => 
        array (
          0 => '744',
          1 => '745',
          2 => '746',
        ),
        'package' => 133,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '高級 巾幗英雄',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '747,748,749,750,751,752',
        'boss' => 
        array (
          0 => '753',
        ),
        'package' => 134,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  41 => 
  array (
    'name' => '收復失土',
    'start_time' => '2013-10-25 12:00:00',
    'end_time' => '2013-10-28 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 猛將 張飛',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '924,925,926,927,928',
        'boss' => 
        array (
          0 => '929',
        ),
        'package' => 141,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 猛將 張飛',
        'stamina' => 25,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 919,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 921,
            1 => 922,
            2 => 920,
          ),
        ),
        'monsters' => '914,915,916,917',
        'boss' => 
        array (
          0 => '923',
        ),
        'package' => 141,
        'randMonster' => 918,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 猛將 張飛',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 969,
          ),
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 
          array (
            0 => 970,
            1 => 971,
          ),
        ),
        'monsters' => '965,966',
        'boss' => 
        array (
          0 => '972',
        ),
        'package' => 141,
        'randMonster' => 967,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  42 => 
  array (
    'name' => '萬聖節',
    'start_time' => '2013-11-01 12:00:00',
    'end_time' => '2013-11-06 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初级 萬聖節',
        'stamina' => 10,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '2189,2190,2191,2192,2193',
        'boss' => 
        array (
          0 => '2194',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中级 萬聖節',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '2195,2196,2197,2198,2199',
        'boss' => 
        array (
          0 => '2200',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '高级 萬聖節',
        'stamina' => 20,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '2201,2202,2203,2204,2205',
        'boss' => 
        array (
          0 => '2206',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  43 => 
  array (
    'name' => '絕世才女',
    'start_time' => '2013-11-08 12:00:00',
    'end_time' => '2013-11-11 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 蔡文姬',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 2329,
            1 => 2330,
          ),
          6 => 
          array (
            0 => 2331,
          ),
        ),
        'monsters' => '2326,2327,2328',
        'boss' => 
        array (
          0 => '2332',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 蔡文姬',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 2322,
            1 => 2323,
          ),
          6 => 
          array (
            0 => 2324,
          ),
        ),
        'monsters' => '2319,2320,2321',
        'boss' => 
        array (
          0 => '2325',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  44 => 
  array (
    'name' => '戰功赫赫',
    'start_time' => '2013-11-11 12:00:00',
    'end_time' => '2013-11-14 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 戰功赫赫',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2144,2145,2146,2147',
        'boss' => 
        array (
          0 => '2148',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 戰功赫赫',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2139,2140,2141,2142',
        'boss' => 
        array (
          0 => '2143',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 戰功赫赫',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2135,2136,2137',
        'boss' => 
        array (
          0 => '2138',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  45 => 
  array (
    'name' => '後主之母',
    'start_time' => '2013-11-28 12:00:00',
    'end_time' => '2013-12-02 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 甘夫人',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 2301,
            1 => 2302,
          ),
          6 => 
          array (
            0 => 2303,
          ),
        ),
        'monsters' => '2298,2299,2300',
        'boss' => 
        array (
          0 => '2304',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 甘夫人',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 2294,
            1 => 2295,
          ),
          6 => 
          array (
            0 => 2296,
          ),
        ),
        'monsters' => '2291,2292,2293',
        'boss' => 
        array (
          0 => '2297',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  46 => 
  array (
    'name' => '紅顏薄命',
    'start_time' => '2013-11-21 12:00:00',
    'end_time' => '2013-11-25 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 糜夫人',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 2315,
            1 => 2316,
          ),
          6 => 
          array (
            0 => 2317,
          ),
        ),
        'monsters' => '2312,2313,2314',
        'boss' => 
        array (
          0 => '2318',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 糜夫人',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 2308,
            1 => 2309,
          ),
          6 => 
          array (
            0 => 2310,
          ),
        ),
        'monsters' => '2305,2306,2307',
        'boss' => 
        array (
          0 => '2311',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  47 => 
  array (
    'name' => '東吳寵妃',
    'start_time' => '2013-11-14 12:00:00',
    'end_time' => '2013-11-18 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 步夫人',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 2343,
            1 => 2344,
          ),
          6 => 
          array (
            0 => 2345,
          ),
        ),
        'monsters' => '2340,2341,2342',
        'boss' => 
        array (
          0 => '2346',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 步夫人',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 2336,
            1 => 2337,
          ),
          6 => 
          array (
            0 => 2338,
          ),
        ),
        'monsters' => '2333,2334,2335',
        'boss' => 
        array (
          0 => '2339',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  48 => 
  array (
    'name' => '內助之賢',
    'start_time' => '2013-10-11 12:00:08',
    'end_time' => '2013-10-22 12:00:08',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 卞夫人',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 2357,
            1 => 2358,
          ),
          6 => 
          array (
            0 => 2359,
          ),
        ),
        'monsters' => '2354,2355,2356',
        'boss' => 
        array (
          0 => '2360',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 卞夫人',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 
          array (
            0 => 2350,
            1 => 2351,
          ),
          6 => 
          array (
            0 => 2352,
          ),
        ),
        'monsters' => '2347,2348,2349',
        'boss' => 
        array (
          0 => '2353',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  49 => 
  array (
    'name' => '茅廬名仕',
    'start_time' => '2013-10-11 12:00:10',
    'end_time' => '2013-10-22 12:00:10',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 茅廬名仕',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2167,2168,2169',
        'boss' => 
        array (
          0 => '2170',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 茅廬名仕',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2163,2164,2165',
        'boss' => 
        array (
          0 => '2166',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 茅廬名仕',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2160,2161',
        'boss' => 
        array (
          0 => '2162',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  50 => 
  array (
    'name' => '托孤輔政',
    'start_time' => '2013-10-11 12:00:13',
    'end_time' => '2013-10-22 12:00:13',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 托孤輔政',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2156,2157,2158',
        'boss' => 
        array (
          0 => '2159',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 托孤輔政',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2152,2153,2154',
        'boss' => 
        array (
          0 => '2155',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 托孤輔政',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2149,2150',
        'boss' => 
        array (
          0 => '2151',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  51 => 
  array (
    'name' => '感恩節',
    'start_time' => '2013-10-11 12:00:16',
    'end_time' => '2013-10-22 12:00:16',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 感恩節',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2116,2117,2118,2119',
        'boss' => 
        array (
          0 => '2120',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 感恩節',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2111,2112,2113,2114',
        'boss' => 
        array (
          0 => '2115',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 感恩節',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2107,2108,2109',
        'boss' => 
        array (
          0 => '2110',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  52 => 
  array (
    'name' => '江東霸主',
    'start_time' => '2013-10-11 12:00:19',
    'end_time' => '2013-10-22 12:00:19',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 江東霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2062,2063,2064,2065',
        'boss' => 
        array (
          0 => '2066',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 江東霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2067,2068,2069,2070',
        'boss' => 
        array (
          0 => '2071',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 江東霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2072,2073,2074,2075',
        'boss' => 
        array (
          0 => '2076',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  53 => 
  array (
    'name' => '江東霸主',
    'start_time' => '2013-12-16 12:00:00',
    'end_time' => '2013-12-18 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 江東霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2062,2063,2064,2065',
        'boss' => 
        array (
          0 => '2066',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 江東霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2067,2068,2069,2070',
        'boss' => 
        array (
          0 => '2071',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 江東霸主',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2072,2073,2074,2075',
        'boss' => 
        array (
          0 => '2076',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  54 => 
  array (
    'name' => '蜀中仁君',
    'start_time' => '2013-12-18 12:00:00',
    'end_time' => '2013-12-20 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2077,2078,2079,2080',
        'boss' => 
        array (
          0 => '2081',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2082,2083,2084,2085',
        'boss' => 
        array (
          0 => '2086',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2087,2088,2089,2090',
        'boss' => 
        array (
          0 => '2091',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  55 => 
  array (
    'name' => '亂世梟雄',
    'start_time' => '2013-12-20 12:00:00',
    'end_time' => '2013-12-22 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 亂世梟雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2092,2093,2094,2095',
        'boss' => 
        array (
          0 => '2096',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 亂世梟雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2097,2098,2099,2100',
        'boss' => 
        array (
          0 => '2101',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 亂世梟雄',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2102,2103,2104,2105',
        'boss' => 
        array (
          0 => '2106',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  56 => 
  array (
    'name' => '圣诞节',
    'start_time' => '2013-12-24 12:00:00',
    'end_time' => '2013-12-26 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 圣诞节',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2116,2117,2118,2119',
        'boss' => 
        array (
          0 => '2120',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 圣诞节',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2111,2112,2113,2114',
        'boss' => 
        array (
          0 => '2115',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 圣诞节',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2107,2108,2109',
        'boss' => 
        array (
          0 => '2110',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  57 => 
  array (
    'name' => '蜀中仁君',
    'start_time' => '2013-12-26 12:00:01',
    'end_time' => '2013-12-28 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2077,2078,2079,2080',
        'boss' => 
        array (
          0 => '2081',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2082,2083,2084,2085',
        'boss' => 
        array (
          0 => '2086',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 蜀中仁君',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
        ),
        'monsters' => '2087,2088,2089,2090',
        'boss' => 
        array (
          0 => '2091',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  58 => 
  array (
    'name' => '托孤輔政',
    'start_time' => '2013-12-28 12:00:01',
    'end_time' => '2013-12-31 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '中級 托孤輔政',
        'stamina' => 15,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2156,2157,2158',
        'boss' => 
        array (
          0 => '2159',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '上級 托孤輔政',
        'stamina' => 25,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '2152,2153,2154',
        'boss' => 
        array (
          0 => '2155',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '超級 托孤輔政',
        'stamina' => 50,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
          7 => 2,
          8 => 2,
          9 => 2,
        ),
        'monsters' => '2149,2150',
        'boss' => 
        array (
          0 => '2151',
        ),
        'package' => 0,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  59 => 
  array (
    'name' => '女神降臨',
    'start_time' => '2014-01-06 12:00:00',
    'end_time' => '2016-01-10 12:00:00',
    'effect' => 
    array (
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '初級 女神降臨',
        'stamina' => 10,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '731,732,733,734,735',
        'boss' => 
        array (
          0 => '736',
          1 => '737',
          2 => '738',
        ),
        'package' => 132,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '中級 女神降臨',
        'stamina' => 15,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '739,740,741,742,743',
        'boss' => 
        array (
          0 => '744',
          1 => '745',
          2 => '746',
        ),
        'package' => 133,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '上級 女神降臨',
        'stamina' => 20,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 2,
          4 => 3,
        ),
        'monsters' => '747,748,749,750,751,752',
        'boss' => 
        array (
          0 => '753',
        ),
        'package' => 134,
        'randMonster' => 0,
        'randDrop' => 0,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
);
?>