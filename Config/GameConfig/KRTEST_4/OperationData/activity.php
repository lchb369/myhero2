<?php
return array (
  100 => 
  array (
    1 => 
    array (
      'subType' => NULL,
      'desc' => '레전드 장수1+1',
      'start_time' => '2013-08-13 00:00:00',
      'end_time' => '2013-08-19 00:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  101 => 
  array (
    2 => 
    array (
      'subType' => NULL,
      'desc' => '강화 대박성공*2',
      'start_time' => '2013-08-13 00:00:00',
      'end_time' => '2013-08-19 00:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  200 => 
  array (
    3 => 
    array (
      'subType' => NULL,
      'desc' => '접속 시 원군포인트, 코인1 획득 ',
      'start_time' => '2013-08-13 00:00:00',
      'end_time' => '2013-08-19 00:00:00',
      'coin' => 0,
      'gold' => 3000,
      'gachaPoint' => 100,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    4 => 
    array (
      'subType' => NULL,
      'desc' => '매일 접속 시 원보1개 보너스',
      'start_time' => '2012-07-23 00:00:00',
      'end_time' => '2012-07-28 00:00:00',
      'coin' => 6,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
    5 => 
    array (
      'subType' => 10,
      'desc' => '한정 연속 접속시（10일）1',
      'start_time' => '2012-07-23 00:00:00',
      'end_time' => '2012-08-02 00:00:00',
      'coin' => 100,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  102 => 
  array (
    6 => 
    array (
      'subType' => NULL,
      'desc' => '한정 처음 충전시 2배',
      'start_time' => '2013-08-13 00:00:00',
      'end_time' => '2013-08-19 00:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
  110 => 
  array (
    7 => 
    array (
      'subType' => NULL,
      'desc' => '레전드 아이템1+1',
      'start_time' => '2013-08-13 00:00:00',
      'end_time' => '2014-08-19 00:00:00',
      'coin' => 0,
      'gold' => 0,
      'gachaPoint' => 0,
      'exp' => 0,
      'pattern' => 0,
      'custom' => '',
    ),
  ),
);
?>