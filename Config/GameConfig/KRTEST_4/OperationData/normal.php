<?php
return array (
  1 => 
  array (
    'name' => '난세의 시작',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '정벌의 서막',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '1,2,3,4,5,6',
        'boss' => 
        array (
          0 => '7',
          1 => '8',
          2 => '9',
        ),
        'package' => 1,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '첫번째 전투',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '1,2,3,4,5,6',
        'boss' => 
        array (
          0 => '8',
          1 => '9',
          2 => '10',
        ),
        'package' => 2,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '낯선 전장터',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '1,2,3,4,5,6',
        'boss' => 
        array (
          0 => '11',
        ),
        'package' => 3,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  2 => 
  array (
    'name' => '붉은빛의 황야',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '황야의 홍건적',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '12,13,14,15,16',
        'boss' => 
        array (
          0 => '17',
        ),
        'package' => 4,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '화웅의 죽음',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '12,13,14,15,16',
        'boss' => 
        array (
          0 => '18',
        ),
        'package' => 5,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '난세의 간웅',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '12,13,14,15,16',
        'boss' => 
        array (
          0 => '19',
        ),
        'package' => 6,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  3 => 
  array (
    'name' => '녹색영혼의 호수',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '1',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '호수의 녹건적',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '20,21,22,23,24',
        'boss' => 
        array (
          0 => '25',
        ),
        'package' => 7,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '간교한장수',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '20,21,22,23,24',
        'boss' => 
        array (
          0 => '26',
        ),
        'package' => 8,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '푸른눈의 중모',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 4,
        ),
        'monsters' => '20,21,22,23,24',
        'boss' => 
        array (
          0 => '27',
        ),
        'package' => 9,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  4 => 
  array (
    'name' => '짙은 안개의숲',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '3',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '석양의 황건적',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '28,29,30,31,32',
        'boss' => 
        array (
          0 => '33',
        ),
        'package' => 10,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '악래의 기개',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '28,29,30,31,32',
        'boss' => 
        array (
          0 => '34',
        ),
        'package' => 11,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '현덕의 인내',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
        ),
        'monsters' => '28,29,30,31,32',
        'boss' => 
        array (
          0 => '35',
        ),
        'package' => 12,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  5 => 
  array (
    'name' => '석양의 협곡',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '4',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '자건적의 숲',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '36,37,38,39,40,41',
        'boss' => 
        array (
          0 => '42',
        ),
        'package' => 13,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '장안의 난',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '36,37,38,39,40,41',
        'boss' => 
        array (
          0 => '43',
        ),
        'package' => 14,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '태사의 바람',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '36,37,38,39,40,41',
        'boss' => 
        array (
          0 => '44',
        ),
        'package' => 15,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  6 => 
  array (
    'name' => '서황의 유역',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '5',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '한중의 재앙',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
        ),
        'monsters' => '45,46,47,48,49,50',
        'boss' => 
        array (
          0 => '51',
        ),
        'package' => 16,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '천명의 정벌',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '45,46,47,48,49,50',
        'boss' => 
        array (
          0 => '52',
        ),
        'package' => 17,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '교위맹주',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 4,
        ),
        'monsters' => '45,46,47,48,49,50',
        'boss' => 
        array (
          0 => '53',
        ),
        'package' => 18,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
    ),
  ),
  7 => 
  array (
    'name' => '시련의 전장',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '한중 혈전',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
        ),
        'monsters' => '54,55,56,57,58,59,60,61,62,63,64,65,66,67,68',
        'boss' => 
        array (
          0 => '69',
          1 => '70',
          2 => '71',
        ),
        'package' => 19,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      2 => 
      array (
        'name' => '간교한 부대',
        'stamina' => 3,
        'steps' => 3,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
        ),
        'monsters' => '54,55,56,57,58,59,60,61,62,63,64,65,66,67,68',
        'boss' => 
        array (
          0 => '72',
          1 => '73',
        ),
        'package' => 20,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 300,
      ),
      3 => 
      array (
        'name' => '江表虎臣
동오의 맹장',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '54,55,56,57,58,59,60,61,62,63,64,65,66,67,68',
        'boss' => 
        array (
          0 => '74',
        ),
        'package' => 21,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  8 => 
  array (
    'name' => '불타는 언덕',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '절체절명의 위기',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '75,76,77,78,79,80,81,82',
        'boss' => 
        array (
          0 => '83',
          1 => '83',
          2 => '83',
        ),
        'package' => 22,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '귀계의 병법가',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '75,76,77,78,79,80,81,82',
        'boss' => 
        array (
          0 => '84',
          1 => '84',
          2 => '84',
        ),
        'package' => 23,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '36계 주위상',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '75,76,77,78,79,80,81,82',
        'boss' => 
        array (
          0 => '85',
        ),
        'package' => 24,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '자매의 용기',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '75,76,77,78,79,80,81,82',
        'boss' => 
        array (
          0 => '86',
          1 => '86',
          2 => '86',
        ),
        'package' => 25,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '거세진 파도',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '75,76,77,78,79,80,81,82',
        'boss' => 
        array (
          0 => '87',
        ),
        'package' => 26,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  9 => 
  array (
    'name' => '죽음의 연못',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '1',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '난세 쟁탈전',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '88,89,90,91,92,93,94,95',
        'boss' => 
        array (
          0 => '96',
          1 => '96',
          2 => '96',
        ),
        'package' => 27,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '북쪽으로 창을 겨누다',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '88,89,90,91,92,93,94,95',
        'boss' => 
        array (
          0 => '97',
          1 => '97',
          2 => '97',
        ),
        'package' => 28,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '천리무장',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '88,89,90,91,92,93,94,95',
        'boss' => 
        array (
          0 => '98',
        ),
        'package' => 29,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '당당한 영웅',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '88,89,90,91,92,93,94,95',
        'boss' => 
        array (
          0 => '99',
          1 => '99',
          2 => '99',
        ),
        'package' => 30,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '동상이몽',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '88,89,90,91,92,93,94,95',
        'boss' => 
        array (
          0 => '100',
        ),
        'package' => 31,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  10 => 
  array (
    'name' => '죽음의 숲',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '3',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '연목구어',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '101,102,103,104,105,106,107,108',
        'boss' => 
        array (
          0 => '109',
          1 => '109',
          2 => '109',
        ),
        'package' => 32,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '문무를 겸비한 자',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '101,102,103,104,105,106,107,108',
        'boss' => 
        array (
          0 => '110',
          1 => '110',
          2 => '110',
        ),
        'package' => 33,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '대군의 길을 열다',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '101,102,103,104,105,106,107,108',
        'boss' => 
        array (
          0 => '111',
        ),
        'package' => 34,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '연환지계',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '101,102,103,104,105,106,107,108',
        'boss' => 
        array (
          0 => '112',
          1 => '112',
          2 => '112',
        ),
        'package' => 35,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '집중된 일격',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '101,102,103,104,105,106,107,108',
        'boss' => 
        array (
          0 => '113',
        ),
        'package' => 36,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  11 => 
  array (
    'name' => '요새의 사막',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '4',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '업성을 취한 지략가',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 4,
        ),
        'monsters' => '114,115,116,117,118,119,120',
        'boss' => 
        array (
          0 => '121',
          1 => '121',
          2 => '121',
        ),
        'package' => 37,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '검은혼령의 탄식',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '114,115,116,117,118,119,120',
        'boss' => 
        array (
          0 => '122',
          1 => '122',
        ),
        'package' => 38,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '타오르는 능신',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '114,115,116,117,118,119,120',
        'boss' => 
        array (
          0 => '123',
        ),
        'package' => 39,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '도탄에 빠진 천하',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '114,115,116,117,118,119,120',
        'boss' => 
        array (
          0 => '124',
          1 => '124',
          2 => '124',
        ),
        'package' => 40,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '바닥난 계책',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '114,115,116,117,118,119,120',
        'boss' => 
        array (
          0 => '125',
        ),
        'package' => 41,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  12 => 
  array (
    'name' => '자색의 성벽',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '5',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '태사의 위험',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '126,127,128,129,130,131,132',
        'boss' => 
        array (
          0 => '133',
          1 => '133',
          2 => '133',
        ),
        'package' => 42,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '부대의 재습격',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '126,127,128,129,130,131,132',
        'boss' => 
        array (
          0 => '134',
          1 => '134',
        ),
        'package' => 43,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '어부지리',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '126,127,128,129,130,131,132',
        'boss' => 
        array (
          0 => '135',
        ),
        'package' => 44,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '지혜의 여신',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '126,127,128,129,130,131,132',
        'boss' => 
        array (
          0 => '136',
          1 => '136',
          2 => '136',
        ),
        'package' => 45,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '도성 밖 병사훈련',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
        ),
        'monsters' => '126,127,128,129,130,131,132',
        'boss' => 
        array (
          0 => '137',
        ),
        'package' => 46,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
    ),
  ),
  13 => 
  array (
    'name' => '귀장의 교장',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '양양도성 외곽',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
        ),
        'monsters' => '138,139,140,141,142,143,144,145,146,147,148,149,150,151,152',
        'boss' => 
        array (
          0 => '153',
          1 => '154',
          2 => '155',
        ),
        'package' => 47,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      2 => 
      array (
        'name' => '맹장의 풍모',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
        ),
        'monsters' => '138,139,140,141,142,143,144,145,146,147,148,149,150,151,152',
        'boss' => 
        array (
          0 => '156',
          1 => '157',
          2 => '158',
        ),
        'package' => 48,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      3 => 
      array (
        'name' => '삼공의 계략',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
        ),
        'monsters' => '138,139,140,141,142,143,144,145,146,147,148,149,150,151,152',
        'boss' => 
        array (
          0 => '159',
          1 => '160',
          2 => '161',
        ),
        'package' => 49,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      4 => 
      array (
        'name' => '편장군의 위엄',
        'stamina' => 5,
        'steps' => 5,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
        ),
        'monsters' => '138,139,140,141,142,143,144,145,146,147,148,149,150,151,152',
        'boss' => 
        array (
          0 => '162',
          1 => '162',
        ),
        'package' => 50,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 400,
      ),
      5 => 
      array (
        'name' => '만인지적',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '138,139,140,141,142,143,144,145,146,147,148,149,150,151,152',
        'boss' => 
        array (
          0 => '163',
        ),
        'package' => 51,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  14 => 
  array (
    'name' => '악래투혼',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '귀장의 통솔',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '164,165,166,167,168,169',
        'boss' => 
        array (
          0 => '170',
        ),
        'package' => 52,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '따뜻한 여걸',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '164,165,166,167,168,169',
        'boss' => 
        array (
          0 => '171',
        ),
        'package' => 53,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '타오르는 유성',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '164,165,166,167,168,169',
        'boss' => 
        array (
          0 => '172',
          1 => '172',
        ),
        'package' => 54,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '영혼의 애상',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '164,165,166,167,168,169',
        'boss' => 
        array (
          0 => '173',
          1 => '174',
        ),
        'package' => 55,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '천인의 너그러움',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '164,165,166,167,168,169',
        'boss' => 
        array (
          0 => '175',
        ),
        'package' => 56,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  15 => 
  array (
    'name' => '차가운 얼음성벽',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '1',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '설원의 배고픈늑대',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 4,
        ),
        'monsters' => '176,177,178,179,180,181',
        'boss' => 
        array (
          0 => '182',
        ),
        'package' => 57,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '명군을 선택하다',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '176,177,178,179,180,181',
        'boss' => 
        array (
          0 => '183',
        ),
        'package' => 58,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '창운촉용',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '176,177,178,179,180,181',
        'boss' => 
        array (
          0 => '184',
          1 => '184',
        ),
        'package' => 59,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '충혼의 마음',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '176,177,178,179,180,181',
        'boss' => 
        array (
          0 => '185',
          1 => '186',
        ),
        'package' => 60,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '영웅의 마지막',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '176,177,178,179,180,181',
        'boss' => 
        array (
          0 => '187',
        ),
        'package' => 61,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  16 => 
  array (
    'name' => '짙푸른색의 언덕',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '3',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '군웅의 이리',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 4,
        ),
        'monsters' => '188,189,190,191,192,193',
        'boss' => 
        array (
          0 => '194',
        ),
        'package' => 62,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '국색천향',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 4,
        ),
        'monsters' => '188,189,190,191,192,193',
        'boss' => 
        array (
          0 => '195',
        ),
        'package' => 63,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '간교한 독사',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 4,
        ),
        'monsters' => '188,189,190,191,192,193',
        'boss' => 
        array (
          0 => '196',
          1 => '196',
        ),
        'package' => 64,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '자욱한 영혼',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 4,
        ),
        'monsters' => '188,189,190,191,192,193',
        'boss' => 
        array (
          0 => '197',
          1 => '198',
        ),
        'package' => 65,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '무적천하',
        'stamina' => 7,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 4,
        ),
        'monsters' => '188,189,190,191,192,193',
        'boss' => 
        array (
          0 => '199',
        ),
        'package' => 66,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  17 => 
  array (
    'name' => '황색의 대보루',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '4',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '최강의 방어',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '200,201,202,203,204',
        'boss' => 
        array (
          0 => '205',
        ),
        'package' => 67,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '경운폐월',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '200,201,202,203,204',
        'boss' => 
        array (
          0 => '206',
        ),
        'package' => 68,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '불의를 진압하는 봉황',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '200,201,202,203,204',
        'boss' => 
        array (
          0 => '207',
          1 => '207',
        ),
        'package' => 69,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '영혼의 용기',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '200,201,202,203,204',
        'boss' => 
        array (
          0 => '208',
          1 => '209',
        ),
        'package' => 70,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '조정을 주무르는 권력',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 2,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '200,201,202,203,204',
        'boss' => 
        array (
          0 => '210',
        ),
        'package' => 71,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  18 => 
  array (
    'name' => '암흑의성',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '5',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '망령의 유랑',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '211,212,213,214,215',
        'boss' => 
        array (
          0 => '216',
        ),
        'package' => 72,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '천고의 여걸',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '211,212,213,214,215',
        'boss' => 
        array (
          0 => '217',
        ),
        'package' => 73,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '고대의기린',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '211,212,213,214,215',
        'boss' => 
        array (
          0 => '218',
          1 => '218',
        ),
        'package' => 74,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '불굴의혼',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 2,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '211,212,213,214,215',
        'boss' => 
        array (
          0 => '219',
          1 => '220',
        ),
        'package' => 75,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '무법의 마왕',
        'stamina' => 8,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '211,212,213,214,215',
        'boss' => 
        array (
          0 => '221',
        ),
        'package' => 76,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  19 => 
  array (
    'name' => '바람의성역',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '귀장의 전장',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237',
        'boss' => 
        array (
          0 => '238',
          1 => '239',
        ),
        'package' => 77,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '마왕여걸',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 2,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237',
        'boss' => 
        array (
          0 => '240',
          1 => '241',
        ),
        'package' => 78,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '신수곤붕',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237',
        'boss' => 
        array (
          0 => '242',
          1 => '243',
          2 => '244',
        ),
        'package' => 79,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '귀마의 출현',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237',
        'boss' => 
        array (
          0 => '245',
          1 => '246',
          2 => '247',
        ),
        'package' => 80,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '광마귀장',
        'stamina' => 11,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
          7 => 4,
          8 => 3,
          9 => 3,
        ),
        'monsters' => '222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237',
        'boss' => 
        array (
          0 => '248',
          1 => '249',
        ),
        'package' => 81,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  20 => 
  array (
    'name' => '불타는 왕궁',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '용음호소',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '424,425,426,427,428,429,430,431',
        'boss' => 
        array (
          0 => '432',
          1 => '433',
        ),
        'package' => 82,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '철벽당노',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '424,425,426,427,428,429,430,431',
        'boss' => 
        array (
          0 => '434',
        ),
        'package' => 83,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '맹장의진',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '424,425,426,427,428,429,430,431',
        'boss' => 
        array (
          0 => '435',
          1 => '436',
        ),
        'package' => 84,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '재모겸비',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '424,425,426,427,428,429,430,431',
        'boss' => 
        array (
          0 => '437',
          1 => '438',
        ),
        'package' => 85,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '불타는 신수',
        'stamina' => 11,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 3,
          4 => 3,
          5 => 4,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 3,
        ),
        'monsters' => '424,425,426,427,428,429,430,431',
        'boss' => 
        array (
          0 => '439',
          1 => '440',
          2 => '441',
        ),
        'package' => 86,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  21 => 
  array (
    'name' => '광해지랑',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '1',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '반간지계',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449',
        'boss' => 
        array (
          0 => '450',
          1 => '451',
        ),
        'package' => 87,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '충의도성',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449',
        'boss' => 
        array (
          0 => '452',
        ),
        'package' => 88,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '악랄한 명령',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 3,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449',
        'boss' => 
        array (
          0 => '453',
          1 => '454',
        ),
        'package' => 89,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '미녀무장',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449',
        'boss' => 
        array (
          0 => '455',
          1 => '456',
        ),
        'package' => 90,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '짙푸른 신수',
        'stamina' => 11,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449',
        'boss' => 
        array (
          0 => '457',
          1 => '458',
          2 => '459',
        ),
        'package' => 91,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  22 => 
  array (
    'name' => '거목의숲',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '3',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '사령의 신수',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449,460,461',
        'boss' => 
        array (
          0 => '462',
          1 => '463',
          2 => '464',
        ),
        'package' => 92,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '마지막 결전',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449,460,461',
        'boss' => 
        array (
          0 => '465',
        ),
        'package' => 93,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '기주결전',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449,460,461',
        'boss' => 
        array (
          0 => '466',
          1 => '467',
          2 => '468',
        ),
        'package' => 94,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '푸른깃털의 힘',
        'stamina' => 9,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 4,
          4 => 3,
          5 => 4,
          6 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449,460,461',
        'boss' => 
        array (
          0 => '469',
          1 => '470',
        ),
        'package' => 95,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '고대신수',
        'stamina' => 11,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 3,
        ),
        'monsters' => '442,443,444,445,446,447,448,449,460,461',
        'boss' => 
        array (
          0 => '471',
          1 => '472',
          2 => '473',
          3 => '474',
        ),
        'package' => 96,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  23 => 
  array (
    'name' => '진정한장수',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '화염의초원',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 2,
          3 => 2,
          4 => 2,
          5 => 2,
          6 => 2,
        ),
        'monsters' => '475,476,477,478,479,480,481,482,483',
        'boss' => 
        array (
          0 => '484',
          1 => '485',
          2 => '486',
        ),
        'package' => 97,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '광활한 얼음성',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 3,
          5 => 3,
          6 => 2,
        ),
        'monsters' => '475,476,477,478,479,480,481,482,483',
        'boss' => 
        array (
          0 => '487',
          1 => '492',
        ),
        'package' => 98,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '초목의 봄',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 2,
        ),
        'monsters' => '475,476,477,478,479,480,481,482,483',
        'boss' => 
        array (
          0 => '488',
          1 => '489',
        ),
        'package' => 99,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '가득한 빛',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 2,
        ),
        'monsters' => '475,476,477,478,479,480,481,482,483',
        'boss' => 
        array (
          0 => '490',
          1 => '491',
        ),
        'package' => 100,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '혼돈의 괴력',
        'stamina' => 12,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 2,
          7 => 2,
          8 => 3,
          9 => 3,
        ),
        'monsters' => '475,476,477,478,479,480,481,482,483',
        'boss' => 
        array (
          0 => '493',
        ),
        'package' => 101,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  24 => 
  array (
    'name' => '국경의 봉화',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '2',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '반짝거리는 별',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1100,1101,1102,1103,1104,1105,1106',
        'boss' => 
        array (
          0 => '1107',
          1 => '1108',
          2 => '1109',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '서촉의 인재',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1110,1111,1112,1113,111,1115,1116',
        'boss' => 
        array (
          0 => '1117',
          1 => '1118',
          2 => '1119',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '철벽도성',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1127,
            1 => 1128,
            2 => 1129,
          ),
        ),
        'monsters' => '1120,1121,1122,1123,1124,1125,1126',
        'boss' => 
        array (
          0 => '1130',
          1 => '1131',
          2 => '1132',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '장엄한 산천',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1140,
            1 => 1141,
            2 => 1142,
          ),
        ),
        'monsters' => '1133,1134,1135,1136,1137,1138,1139',
        'boss' => 
        array (
          0 => '1143',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '탐욕스러운 자',
        'stamina' => 12,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1151,
            1 => 1152,
            2 => 1153,
          ),
        ),
        'monsters' => '1144,1145,1146,1147,1148,1149,1150',
        'boss' => 
        array (
          0 => '1154',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  25 => 
  array (
    'name' => '이수의 경계',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '1',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '죽음의 지역',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1155,1156,1157,1158,1159,1160,1161',
        'boss' => 
        array (
          0 => '1162',
          1 => '1163',
          2 => '1164',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '이익을 쫓는자',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1155,1156,1157,1158,1159,1160,1161',
        'boss' => 
        array (
          0 => '1172',
          1 => '1173',
          2 => '1174',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '구사일생',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1182,
            1 => 1183,
            2 => 1184,
          ),
        ),
        'monsters' => '1175,1176,1177,1178,1179,1180,1181',
        'boss' => 
        array (
          0 => '1185',
          1 => '1186',
          2 => '1187',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '영민의 깨달음',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1195,
            1 => 1196,
            2 => 1197,
          ),
        ),
        'monsters' => '1188,1189,1190,1191,1192,1193,1194',
        'boss' => 
        array (
          0 => '1198',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '낙봉의 화살',
        'stamina' => 12,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1206,
            1 => 1207,
            2 => 1208,
          ),
        ),
        'monsters' => '1199,1200,1201,1202,1203,1204,1205',
        'boss' => 
        array (
          0 => '1209',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  26 => 
  array (
    'name' => '환술의 성',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '3',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '일기충천',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1210,1211,1212,1213,1214,1215,1216',
        'boss' => 
        array (
          0 => '1217',
          1 => '1218',
          2 => '1219',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '언변의 달인',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1220,1221,1222,1223,1224,1225,1226',
        'boss' => 
        array (
          0 => '1227',
          1 => '1228',
          2 => '1229',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '오위 혈전',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1237,
            1 => 1238,
            2 => 1239,
          ),
        ),
        'monsters' => '1230,1231,1232,1233,1234,1235,1236',
        'boss' => 
        array (
          0 => '1240',
          1 => '1241',
          2 => '1242',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '야망을 가진 여걸',
        'stamina' => 10,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1250,
            1 => 1251,
            2 => 1252,
          ),
        ),
        'monsters' => '1243,1244,1245,1246,1247,1248,1249',
        'boss' => 
        array (
          0 => '1253',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '용의 천하',
        'stamina' => 12,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1261,
            1 => 1262,
            2 => 1263,
          ),
        ),
        'monsters' => '1254,1255,1256,1257,1258,1259,1260',
        'boss' => 
        array (
          0 => '1264',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  27 => 
  array (
    'name' => '빛나는 도성',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '4',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '위풍당당한 기세',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1265,1266,1267,1268,1269,1270',
        'boss' => 
        array (
          0 => '1271',
          1 => '1272',
          2 => '1273',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '서촉의 모사',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1274,1275,1276,1277,1278,1279',
        'boss' => 
        array (
          0 => '1280',
          1 => '1281',
          2 => '1282',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '끝없는 복수',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1289,
            1 => 1290,
            2 => 1291,
          ),
        ),
        'monsters' => '1283,1284,1285,1286,1287,1288',
        'boss' => 
        array (
          0 => '1292',
          1 => '1293',
          2 => '1294',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '귀재무쌍',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1301,
            1 => 1302,
            2 => 1303,
          ),
        ),
        'monsters' => '1295,1296,1297,1298,1299,1300',
        'boss' => 
        array (
          0 => '1304',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '신선의 여행',
        'stamina' => 14,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1311,
            1 => 1312,
            2 => 1313,
          ),
        ),
        'monsters' => '1305,1306,1307,1308,1309,1310',
        'boss' => 
        array (
          0 => '1314',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  28 => 
  array (
    'name' => '암흑의 외성',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '5',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '절차탁마의 노력',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1315,1316,1317,1318,1319,1320',
        'boss' => 
        array (
          0 => '1321',
          1 => '1322',
          2 => '1323',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '설득의 귀재',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
        ),
        'monsters' => '1324,1325,1326,1327,1328,1329',
        'boss' => 
        array (
          0 => '1330',
          1 => '1331',
          2 => '1332',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '역발산의 기개',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1339,
            1 => 1340,
            2 => 1341,
          ),
        ),
        'monsters' => '1333,1334,1335,1336,1337,1338',
        'boss' => 
        array (
          0 => '1342',
          1 => '1343',
          2 => '1344',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '눈부신 방랑',
        'stamina' => 11,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1351,
            1 => 1352,
            2 => 1353,
          ),
        ),
        'monsters' => '1345,1346,1347,1348,1349,1350',
        'boss' => 
        array (
          0 => '1354',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '상서로운 기운',
        'stamina' => 14,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1361,
            1 => 1362,
            2 => 1363,
          ),
        ),
        'monsters' => '1355,1356,1357,1358,1359,1360',
        'boss' => 
        array (
          0 => '1364',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
  29 => 
  array (
    'name' => '끝없는 전쟁',
    'effect' => 
    array (
      'start_time' => '2012-09-29 11:00:00',
      'end_time' => '2013-09-29 23:00:00',
      'number' => '',
    ),
    'dungeon_type' => '6',
    'rooms' => 
    array (
      1 => 
      array (
        'name' => '용장과 모사',
        'stamina' => 12,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1373,
            1 => 1374,
          ),
        ),
        'monsters' => '1365,1366,1367,1368,1369,1370,1371,1372',
        'boss' => 
        array (
          0 => '1375',
          1 => '1376',
          2 => '1377',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      2 => 
      array (
        'name' => '음율의 천재',
        'stamina' => 12,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1386,
          ),
        ),
        'monsters' => '1378,1379,1380,1381,1382,1383,1384,1385',
        'boss' => 
        array (
          0 => '1387',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      3 => 
      array (
        'name' => '동오로 진격',
        'stamina' => 12,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1396,
            1 => 1397,
          ),
        ),
        'monsters' => '1388,1389,1390,1391,1392,1393,1394,1395',
        'boss' => 
        array (
          0 => '1398',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      4 => 
      array (
        'name' => '호접지몽',
        'stamina' => 12,
        'steps' => 7,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 
          array (
            0 => 1407,
            1 => 1408,
          ),
        ),
        'monsters' => '1399,1400,1401,1402,1403,1404,1405,1406',
        'boss' => 
        array (
          0 => '1409',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
      5 => 
      array (
        'name' => '귀모의 중달',
        'stamina' => 15,
        'steps' => 10,
        'stepsMonster' => 
        array (
          1 => 2,
          2 => 3,
          3 => 3,
          4 => 2,
          5 => 3,
          6 => 3,
          7 => 3,
          8 => 3,
          9 => 
          array (
            0 => 1418,
            1 => 1419,
          ),
        ),
        'monsters' => '1410,1411,1412,1413,1414,1415,1416,1417',
        'boss' => 
        array (
          0 => '1420',
        ),
        'package' => 0,
        'randMonster' => NULL,
        'randDrop' => NULL,
        'dropGold' => 1000,
        'dropGoldRate' => 600,
      ),
    ),
  ),
);
?>