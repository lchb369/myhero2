<?php
/**
 * 提供给后端服务器的接口
 */
$err = isset( $_GET['err'] )  ? ( E_ALL ^ E_NOTICE ) : 0;
error_reporting( $err );
$_GET = $_POST = array_merge( $_GET , $_POST );
define( 'IN_INU' , true );
define( 'API_GAME' , true );
define( "ROOT_DIR" , dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) );

define( "MOD_DIR" , ROOT_DIR ."/Model" );
define( "CON_DIR" , ROOT_DIR ."/Controller/Api" );
define( "CONFIG_DIR" , ROOT_DIR . "/Config" );

$appPlatform = 'wk';
$appSid = 19;
include MOD_DIR .'/Common.php';

$conObject = new PaymentController();
$act = "telepayCPCGI";

	try
	{
	    $info = $conObject->$act();
	    echo json_encode( $info );
	    ObjectStorage::save();
    }
    catch ( Exception $e )
    {
        $code = $e->getCode();
        echo json_encode( array( 'code' => $e->getCode() ) );
    }
	return ;

