<?php
/**
 * 越南天赋支付回调
 */
$err = isset( $_GET['err'] )  ? ( E_ALL ^ E_NOTICE ) : 0;
error_reporting( $err );
$_GET = $_POST = array_merge( $_GET , $_POST );
define( 'IN_INU' , true );
define( 'API_GAME' , true );
define( "ROOT_DIR" , dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) );

define( "MOD_DIR" , ROOT_DIR ."/Model" );
define( "CON_DIR" , ROOT_DIR ."/Controller/Api" );
define( "CONFIG_DIR" , ROOT_DIR . "/Config" );


include MOD_DIR .'/Common.php';

//获得分区号
$message = trim($_REQUEST['message']);
$msgArr = explode(" ", $message);
if(empty($msgArr[2]))
{
	$ret = array('status' => 1, 'message' => 'sid error');
	echo json_encode( $ret );
	exit;
}
$serverConf = Common::getServerConfBySid($msgArr[2]);
$appPlatform = $serverConf['pf'];
$appSid = $serverConf['sid'];

$conObject = new PaymentController();
$act = "ThienPhuNotify";

	try
	{
	    $info = $conObject->$act();
	    echo json_encode( $info );
	    ObjectStorage::save();
    }
    catch ( Exception $e )
    {
        $code = $e->getCode();
        echo json_encode( array( 'code' => $e->getCode() ) );
    }
	return ;

