<?php
/**
 * 提供给后端服务器的接口
 */
$err = isset( $_GET['err'] )  ? ( E_ALL ^ E_NOTICE ) : 0;
error_reporting( $err );
$_GET = $_POST = array_merge( $_GET , $_POST );
define( 'IN_INU' , true );
define( 'API_GAME' , true );
define( "ROOT_DIR" , dirname( dirname( dirname( __FILE__ ) ) ) );
define( "MOD_DIR" , ROOT_DIR ."/Model" );
define( "CON_DIR" , ROOT_DIR ."/Controller/Api" );
define( "CONFIG_DIR" , ROOT_DIR . "/Config" );

//初始化平台服务器ID
$appPlatform = $_REQUEST['pf'];
$appSid = $_REQUEST['sid'];

include MOD_DIR .'/Common.php';



//调试设置
if( isset( $_GET["debug"] )  )
{
	error_reporting( E_ALL ^ E_NOTICE );
	ini_set( 'display_errors' , 'On' );
}
else 
{
	error_reporting( 0 );
	ini_set( 'display_errors' , 'Off' );
}

$payLog = new ErrorLog( "TenpayApiGame" );
$message = $_SERVER['REQUEST_URI'];
$payLog->addLog( $message );

$method = explode( '.' , $_GET['method'] );
$con = ucfirst( strtolower( $method[0] ) ) . 'Controller';
$act = $method[1];
if( file_exists( CON_DIR . "/{$con}.php" ) )
{
	$conObject = new $con;
	if( method_exists( $conObject , $act ) )
	{
	    try
	    {
		    $info = $conObject->$act();
		    echo json_encode( $info );
		    ObjectStorage::save();
	    }
	    catch ( Exception $e )
	    {
	        $code = $e->getCode();
	        echo json_encode( array( 'code' => $e->getCode() ) );
	    }
		return ;
	}
}
echo json_encode( array( 'result' => 3 , 'msg' => 'method not exist' ) );
