<?php
/*
 * Copyright 2012, Changbing Liu.  All rights reserved.
 * https://free.svnspot.com/lchb.cppdev/trunk/phpframework
 *
 * Use of this source code is governed by a BSD-style
 * license that can be found in the License file.
 */

if(extension_loaded('zlib'))
{
	ini_set('zlib.output_compression', 'On');
	ini_set('zlib.output_compression_level', '-1');
}

$startTime = microtime( true );
define( 'DEBUG' , true );
define( 'IN_INU' , true );
define( "ROOT_DIR" , dirname( dirname( dirname( __FILE__ ) ) ) );
define( "MOD_DIR" , ROOT_DIR ."/Model" );
define( "CON_DIR" , ROOT_DIR ."/Controller/Api" );
define( "CONFIG_DIR" , ROOT_DIR . "/Config" );

$_POST = $_GET = $_REQUEST;

//初始化平台服务器ID
$appPlatform = $_REQUEST['pf'];
$appSid = $_REQUEST['sid'];


//调试设置
if( DEBUG && isset( $_GET["debug"] )  )
{
	error_reporting( E_ALL ^ E_NOTICE );
	ini_set( 'display_errors' , 'On' );
}
else 
{
	error_reporting( 0 );
	ini_set( 'display_errors' , 'Off' );
}

include MOD_DIR .'/Common.php';
Helper_RunLog::getInstance()->addLog( "Api" , "request start..." );

//EDAC数据中心统计,初始化
Stats_Edac::init();

//性能分析器
$config = Common::getConfig();
if( $config['xhprof']['isOpen'] && function_exists( 'xhprof_enable' ) )
{
	include_once MOD_DIR .'/XHProf/xhprof_lib.php';
	include_once MOD_DIR .'/XHProf/xhprof_runs.php';
	xhprof_enable();
}


//参数校验
if( checkArg() == false && DEBUG == false )
{
	$result = array
	(
			'rc' => 101,
			'method' => $_GET['method'] ,
			'msg' => "arg error" ,
	);
	echo json_encode( $result );
	return;
}


//接收参数
$_POST = json_decode( $_POST['data'] , true );
if( !is_array( $_POST ) )
{
	$_POST = array();
	
	if( DEBUG && $_GET['data'] )
	{
		$_POST = json_decode( $_GET['data'] , true );
	}
}


$uId = intval( trim( $_GET['uid'] ) );
$loginName = trim( $_GET['loginName'] );



if( !empty( $_GET['method'] )  )
{

	$method = explode( '.' , $_GET['method'] );
	$controller = ucfirst( strtolower( $method[0] ) ) . 'Controller';
	$action = $method[1];
	/**
	 * 将平台ID转换为游戏内部ID
	 */
	if( !$uId )
	{
		if( 
				( $controller == 'UserController' && $action == 'setNewbie' && $_GET['step'] == 0 )
				|| ( $controller == 'SceneController' && $action == 'init'  )
		)
		{
			Helper_RunLog::getInstance()->addLog( "Api" , "rand LoginName start..." );
			if(  !$loginName  || ( $uId = User_ConvertUserId::convertToGameUserId( $loginName ) ) == 0 )
			{
				return false;
			}
		}
	}

	//啥验证都不要
	$filterAlwaysMethod = array( 
		"Payment.ucLogin" , 
		"Config.get" ,
		"Config.server",
		"Config.getMonster" , 
		"Config.language" , 
		"Config.langVer" , 
		"User.getSession" , 
		"Friend.checkInviteCode"
    );
	//不需要session验证
	$filterNoSessionMethod = array_merge( array( "Payment.getOrderId" ) , $filterAlwaysMethod );
	if( !in_array( trim( $_GET['method'] ),   $filterNoSessionMethod  ) 
	 )
	{
		//校验session
	
	    $sess = $_POST['session'] ? $_POST['session'] : $_GET['session'];
		if(  !$sess && !User_Model::checkSession( $uId , $sess ) )
        {
           $result = array
           (
              'rc' => User_Exception::STATUS_SESSION_ERROR ,
              'method' => $_GET['method'] ,
              'msg' => "session error , errorCode:".User_Exception::STATUS_SESSION_ERROR ,
                                //'info' => $info ,
            );
            echo json_encode( $result );
            return;
        }
        
        //校验重复登录
        if( User_Model::checkSig( $uId , $_GET['sig'] ) == false && DEBUG == false )
        {
        	 
        	$result = array
        	(
        			'rc' => 102 ,
        			'method' => $_GET['method'] ,
        			'msg' => "sig error" ,
        	);
        	echo json_encode( $result );
        	return;
        }
        
	}

	//如果用户不存在
	$filterNoUserIDMethod = array_merge( array( "Scene.init" , "User.setNewbie" ) , $filterAlwaysMethod );
	if( User_Model::exist( $uId ) == false
		&& !in_array( $_GET['method'] , $filterNoUserIDMethod )
	 )
	{
		$result = array
		(
				'rc' => 201 ,
				'method' => $_GET['method'] ,
				'msg' => "not exist user" ,
				//'info' => $info ,
		);
		echo json_encode( $result );
		return;
	}

	if( file_exists( CON_DIR . "/{$controller}.php" ) )
	{
		$conObject = new $controller;
		if( method_exists( $conObject , $action ) )
		{
			
			$conObject->setUser( $uId );
			try
			{
				everyRequestDo( $uId );
				$info = $conObject->$action();
				$result = 0;
				$msg = '';
				
				if( isset( $info['errorCode'] ) && $info['errorCode'] > 0 )
				{
					$result = $info['errorCode'];
					$info = array();
					$msg = '';
				}
				else 
				{
					ObjectStorage::save();
				}
			}
			catch ( Exception $e )
			{
				$info = array();
				$result = $e->getCode();
				$msg = $e->getMessage();
				
				$errorLog  = new ErrorLog( "traceException" , $uId  );
				$errorContent = "\nParam:".json_encode( array_merge( $_GET , $_POST ) )."\nErrorLog:".$e;
				$errorLog->addLog(  $errorContent );
			}
			
			$result = array
			(
				'rc' => $result ,
				'method' => $_GET['method'] ,
				//'msg' => $msg ,
				'msg' => (string)$result ,
				//'info' => $info ,
			);
			
			
			$info['server_now'] = $_SERVER['REQUEST_TIME'];
			if( $controller == 'SceneController' && $action == 'init' )
			{
				$info['login_name'] = $loginName;
			}
			
			$result = array_merge( $result , $info );
			

			$endTime = microtime( true );
			if( DEBUG && isset( $_GET["debug"] ) )
			{
				echo "<pre>";print_r( $result );printf( "\n%dus.\n" , ( $endTime - $startTime ) * 1000000 );echo Helper_RunLog::getInstance()->getRunData();echo "</pre>";
			}
			echo json_encode( $result );
			$traceTime = ( int )( ( $endTime - $startTime ) * 1000 );
			
			if( $config['xhprof']['isOpen'] && function_exists( 'xhprof_disable' ) )
			{
				$xhprof_data = xhprof_disable();
				$xhprof_runs = new XHProfRuns_Default( $config['xhprof']['logDir'] );
				$xhprof_runs->save_run( $xhprof_data , $_GET['method'] , $uId );
			}
			return;
		}
	}
}
echo json_encode( array( 'rc' => 3 , 'msg' => 'method not exist' ) );

exit;

/**
 * 参数校验
 */

function checkArg()
{
	define( 'SECRET_CODE_1' , 'sdfe' );
	define( 'SECRET_CODE_2' , '$5ser' );
	define( 'SECRET_CODE_3' , 'f!@P' );
	define( 'SECRET_CODE_4' , 'sdfsd~2$#%@' );
	define( 'SECRET_CODE_5' , 'ssdfe' );
	
	$argValue = $_GET['arg'];
	$timestamp = $_GET['timestamp'];
	
	$makeArg = $timestamp.SECRET_CODE_1.SECRET_CODE_2.SECRET_CODE_3.SECRET_CODE_4.SECRET_CODE_5;
	$makeArg = md5( $makeArg );
	$makeArg = substr( $makeArg , 0 , 10 );
	
	if( $argValue == $makeArg )  
	{
		return true;
	}
	return false;
}


function everyRequestDo( $userId )
{
	if( $userId <=0 )
	{
		return;
	}

	Stats_User::checkBanList( $userId );
	
	User_Info::getInstance( $userId )->autoRefreshStamina();
	
	Stats_Model::taskWeekly( $userId );
	
	Stats_Model::taskDaily( $userId );
	
	Stats_Model::taskPer5Min( $userId );
}


