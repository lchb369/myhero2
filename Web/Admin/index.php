<?php
error_reporting( E_ALL ^ E_NOTICE );
ini_set( 'display_errors' , 'On' );


session_start();
$time = microtime( true );
define( "IN_INU" , true );
define( "ROOT_DIR" , dirname( dirname( dirname( __FILE__ ) ) ) );		#修改成游戏的根目录
define( "CONFIG_DIR" , ROOT_DIR . "/Config" );
define( "MOD_DIR" , ROOT_DIR ."/Model" );
define( "CON_DIR" , ROOT_DIR ."/Controller/Admin" );
define( "TPL_DIR" , ROOT_DIR ."/Tpl/Admin" );
define( "CACHE_DIR" , ROOT_DIR ."/Cache" );

include MOD_DIR .'/Common.php';

if(!!$config = Common::getConfig('ServerConfig'))
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>My Hero</title>
	</head>
	<body>
		<table border="1">
<?php
	$machineConf = Common::getConfig('MachineConfig');
	foreach($config as $conf)
	{
		if(!$conf['enable']) continue;
		if($machineConf['machinePfs'] && !Helper_Common::inPlatform($machineConf['machinePfs'], $conf['pf'])) continue;
		
		echo '<tr>';
		echo '<td>';
		echo preg_match( "/test/", $conf['pf'] ) ? '测试服' : '';
		echo '</td>';
		echo '<td>';
		echo $conf['statsName'].' ： '.$conf['serverName'];
		echo '</td>';
		echo '<td>';
		echo '<a href="http://'.$conf['serverIp'][0].'/myhero2/Web/Admin/admin.php'
				.'?pf='.$conf['pf'].'&sid='.$conf['sid'].'" target="_blank">'
				.'GM工具'.'</a>';
		echo '</td>';
		echo '<td>';
		echo '<a href="http://'.(!empty($conf['edacWeb']) ? $conf['edacWeb'].'/EDAC' : 'edac.ecngame.com').'/Web/Admin/admin.php'
				.'?f=content&appId=1003&sid='.$conf['sid'].'" target="_blank">'
				.'统计后台'.'</a>';
		echo '</td>';
		echo '<td>';
		echo $conf['pf'];
		echo '</td>';
		echo '</tr>';
	}
?>
		</table>
	</body>
</html>
<?php
}
 