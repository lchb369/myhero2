<?php
/**
 * 后台管理入口(只部署在管理机上)
 */
error_reporting( E_ALL ^ E_NOTICE );
$time = microtime( true );
define( "IN_INU" , true );
define( "ROOT_DIR" , dirname( dirname( dirname( __FILE__ ) ) ) );		#修改成游戏的根目录
define( "CONFIG_DIR" , ROOT_DIR . "/Config" );
define( "MOD_DIR" , ROOT_DIR ."/Model" );
define( "CON_DIR" , ROOT_DIR ."/Controller/Admin" );
define( "TPL_DIR" , ROOT_DIR ."/Tpl/Admin" );
define( "CACHE_DIR" , ROOT_DIR ."/Cache" );
include MOD_DIR .'/Common.php';
$con = 'UploadConfigController';
$conFile = CON_DIR . "/{$con}.php";
$act = empty( $_GET['act'] ) ? 'run' : $_GET['act'];
if( file_exists( $conFile ) )
{
	include $conFile;
	$object = new $con;
	if( method_exists( $object , $act ) )
	{
		echo json_encode( $object->$act() );
		return ;
	}
}
echo json_encode( array( 'status' => 2 , 'error' => array( array( 'message' => '没有这个方法' , 'level' => 2 ) ) ) );