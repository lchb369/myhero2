<?php
require_once 'PHPUnit\Framework\TestSuite.php';

$startTime = microtime( true );
error_reporting( E_ALL ^ E_NOTICE );
define( 'IN_INU' , true );
define( 'IN_UNIT_TEST' , true );
define( "ROOT_DIR" , dirname( dirname( __FILE__ ) ) );
define( "MOD_DIR" , ROOT_DIR ."/Model" );
define( "CON_DIR" , ROOT_DIR ."/Controller/Api" );
define( "CONFIG_DIR" , ROOT_DIR . "/Config" );
include MOD_DIR .'/Common.php';

require_once ROOT_DIR .'/Test/Shop_ModelTest.php';
require_once ROOT_DIR .'/Test/Map_ModelTest.php';
require_once ROOT_DIR .'/Test/Buff_ModelTest.php';
require_once ROOT_DIR .'/Test/Task_UserTest.php';
require_once ROOT_DIR .'/Test/Bag_ModelTest.php';
//require_once ROOT_DIR .'/Test/Skill_ModelTest.php';
//require_once ROOT_DIR .'/Test/Ship_ModelTest.php';
//require_once ROOT_DIR .'/Test/Ship_DuelAttacklTest.php';
//require_once ROOT_DIR .'/Test/Building_GiftTest.php';

/**
 * Static test suite.
 */
class TestSuite extends PHPUnit_Framework_TestSuite
{
	/**
	 * Constructs the test suite handler.
	 */
	public function __construct()
	{
		date_default_timezone_set( 'Asia/ShangHai' );
		$this->setName( 'TestSuite' );
		
		$this->addTestSuite( 'Shop_ModelTest' );
		$this->addTestSuite( 'Map_ModelTest' );
		$this->addTestSuite( 'Buff_ModelTest' );
		
		$this->addTestSuite( 'Bag_ModelTest' );
		$this->addTestSuite( 'Task_UserTest' );
		//$this->addTestSuite( 'Skill_ModelTest' );
		//$this->addTestSuite( 'Ship_ModelTest' );
		//$this->addTestSuite( 'Ship_DuelAttacklTest' );
		//$this->addTestSuite( 'Building_GiftTest' );
	
	}

	/**
	 * Creates the suite.
	 */
	public static function suite()
	{
		return new self();
	}
}

