<?php
require_once 'PHPUnit\Framework\TestCase.php';
/**
 * Bag_Model test case.
 */
class Bag_ModelTest extends PHPUnit_Framework_TestCase
{
	const FAKE_USER_ID = 4932074;

	/**
	 * @var Bag_Model
	 */
	private $Bag_Model;

	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp()
	{
		parent::setUp();
		Logical_Abstract::setConfig( 'TaskItemConfig' , array( 
			'1001' => array( 
				'weight' => 1 , 
				'sellPrice' => 0 
			) ,
			'1002' => array( 
				'weight' => 1 , 
				'sellPrice' => 0 
			) ,
		) );
		Logical_Abstract::setConfig( 'EquipmentFrontCannonForMountConfig' , array( 
			'20001' => array( 
				'weight' => 1 , 
				'unlockLevel' => 1 , 
				'sellPrice' => 40 , 
				'equipmentPoint' => 1 
			) ,
			'20002' => array( 
				'weight' => 1 , 
				'unlockLevel' => 1 , 
				'sellPrice' => 40 , 
				'equipmentPoint' => 1 
			) ,
			'20003' => array( 
				'weight' => 1 , 
				'unlockLevel' => 1 , 
				'sellPrice' => 40 , 
				'equipmentPoint' => 1 
			) 
		) );
		$dataBagMock = Data_Bag_Model::getInstance( self::FAKE_USER_ID  , true , true ,true  );
		$dataBagMock->set( 'weight' , 10 );
		$dataBagMock->set( 'weightLimit' , 100 );
		$dataBagMock->set( 'itemList' , array( 
			1001 => array( 'number' => 1 ) , 
			1002 => array( 'number' => 2 ), 
		) );
		$dataBagMock->set( 'equipmentList' , array( 
			1 => array( 
				'itemId' => 20002 , 
				'enchantLevel' => 0 , 
				'isUsing' => 0 , 
				'color' => 0 
			) , 
			2=> array( 
				'itemId' => 20001 , 
				'enchantLevel' => 0 , 
				'isUsing' => 0 , 
				'color' => 0 
			) , 
		) );
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'bagModel' , $dataBagMock);
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'bagModelWithWriteLock' , $dataBagMock);
		
		$userAbilityMock = Data_User_Ability::getInstance(   self::FAKE_USER_ID  , true , true , true  );
		$userAbilityMock->set(  'level' , 4 );
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'userAbility' , $userAbilityMock );
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'userAbilityWithWriteLock' , $userAbilityMock );
		$userTaskMock = Data_Task_Model::getInstance(   self::FAKE_USER_ID  , true , true   );

		$userTaskMock->setTaskNeedItemIds(  array() );
		$userTaskMock->set( 'rebirthTime' , array( ) );
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'taskModel' , $userTaskMock );
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'taskModelWithWriteLock' , $userTaskMock );
		$this->assertFalse( $dataBagMock->isFixedBagWeight() );
		$this->Bag_Model = Bag_Model::getInstance( self::FAKE_USER_ID );
	}

	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown()
	{
		// TODO Auto-generated Bag_ModelTest::tearDown()
		$this->Bag_Model = null;
		parent::tearDown();
	}

	/**
	 * Constructs the test case.
	 */
	public function __construct()
	{
		// TODO Auto-generated constructor
		//$this->markTestIncomplete( "construct test not implemented" );
	}

	/**
	 * Tests Bag_Model::getInstance()
	 */
	public function testGetInstance()
	{
		$this->markTestIncomplete( "GetInstance test not implemented" );
	}

	/**
	 * Tests Bag_Model->changeItem()
	 */
	public function testChangeItem()
	{
		try {
			$this->Bag_Model->changeItem(  1002 , -12 );
		}
		catch ( Bag_Exception $be )
		{
			$this->assertEquals(  $be->getCode() , 205  );	
		}
		$this->Bag_Model->changeItem(  1002 , 3 );		
		$this->assertEquals( $this->Bag_Model->getHowMany( 1002 ) , 5 );	
	}

	/**
	 * Tests Bag_Model->addEquipment()
	 */
	public function testAddEquipment()
	{
		$this->Bag_Model->addEquipment(  20003 , 0 , 1 );		
		$bagData = $this->Bag_Model->getData();
		$this->assertEquals( $bagData['equipmentList']['3']['itemId'] , 20003  );
		$this->assertEquals( $bagData['equipmentList']['3']['isUsing'] , 1  );
	}

	/**
	 * Tests Bag_Model->removeEquipment()
	 */
	public function testRemoveEquipment()
	{
		$this->Bag_Model->removeEquipment( 2 );
		$bagData = $this->Bag_Model->getData();
		$this->assertArrayNotHasKey(  2 , $bagData['equipmentList'] );
	}

	/**
	 * Tests Bag_Model->setUsing()
	 */
	public function testSetUsing()
	{
		$this->Bag_Model->setUsing( 1 , 1);
		$bagData = $this->Bag_Model->getData();
		$this->assertEquals( $bagData['equipmentList']['1']['isUsing'] , 1  );
	}

	/**
	 * Tests Bag_Model->isUsing()
	 */
	public function testIsUsing()
	{
		$this->Bag_Model->setUsing( 1 , 1);
		$bagData = $this->Bag_Model->getData();
		$isUsing = $this->Bag_Model->isUsing( 1 );
		$this->assertEquals(  $isUsing , true );
	}

	/**
	 * Tests Bag_Model::getItemType()
	 */
	public function testGetItemType()
	{
		// TODO Auto-generated Bag_ModelTest::testGetItemType()
		$this->markTestIncomplete( "getItemType test not implemented" );
		Bag_Model::getItemType(/* parameters */);
	}

	/**
	 * Tests Bag_Model::getItemCategory()
	 */
	public function testGetItemCategory()
	{
		// TODO Auto-generated Bag_ModelTest::testGetItemCategory()
		$this->markTestIncomplete( "getItemCategory test not implemented" );
		Bag_Model::getItemCategory(/* parameters */);
	}

	/**
	 * Tests Bag_Model->getItemByItemId()
	 */
	public function testGetItemByItemId()
	{
		// TODO Auto-generated Bag_ModelTest->testGetItemByItemId()
		$this->markTestIncomplete( "getItemByItemId test not implemented" );
		$this->Bag_Model->getItemByItemId(/* parameters */);
	}

	/**
	 * Tests Bag_Model->haveItem()
	 */
	public function testHaveItem()
	{
		$have = $this->Bag_Model->haveItem( 1003 );
		$this->assertEquals( $have , false );
	}

	/**
	 * Tests Bag_Model->getHowMany()
	 */
	public function testGetHowMany()
	{
		$number = $this->Bag_Model->getHowMany( 1001 );
		$this->assertEquals( $number  , 1 );
	}

	/**
	 * Tests Bag_Model->haveEquipment()
	 */
	public function testHaveEquipment()
	{
		$have = $this->Bag_Model->haveEquipment( 1 );
		$this->assertEquals( $have  , 1 );
		$have = $this->Bag_Model->haveEquipment( 3 );
		$this->assertEquals( $have  , 0 );
		
	}

	/**
	 * Tests Bag_Model->getEquipmentInfo()
	 */
	public function testGetEquipmentInfo()
	{
		$info = $this->Bag_Model->getEquipmentInfo( 1 );
		$expected = array(
			'itemId' => 20002,
			'enchantLevel' => 0 , 
			'isUsing' => 0,
			 'color' => 0 ,
		);
		$this->assertEquals( $expected , $info );
	}

	/**
	 * Tests Bag_Model->getEquipmentItemId()
	 */
	public function testGetEquipmentItemId()
	{
		$itemId = $this->Bag_Model->getEquipmentItemId( 1 );
		$this->assertEquals(  $itemId ,  20002  );
	}

	/**
	 * Tests Bag_Model->setEquipmentLevel()
	 */
	public function testSetEquipmentLevel()
	{
		// TODO Auto-generated Bag_ModelTest->testSetEquipmentLevel()
		$this->markTestIncomplete( "setEquipmentLevel test not implemented" );
		$this->Bag_Model->setEquipmentLevel(/* parameters */);
	}

	/**
	 * Tests Bag_Model::getItemDetailInfo()
	 */
	public function testGetItemDetailInfo()
	{
		// TODO Auto-generated Bag_ModelTest::testGetItemDetailInfo()
		$this->markTestIncomplete( "getItemDetailInfo test not implemented" );
		Bag_Model::getItemDetailInfo(/* parameters */);
	}

	/**
	 * Tests Bag_Model->getItemWeight()
	 */
	public function testGetItemWeight()
	{
		// TODO Auto-generated Bag_ModelTest->testGetItemWeight()
		$this->markTestIncomplete( "getItemWeight test not implemented" );
		$this->Bag_Model->getItemWeight(/* parameters */);
	}

	/**
	 * Tests Bag_Model->changeWeight()
	 */
	public function testChangeWeight()
	{
		// TODO Auto-generated Bag_ModelTest->testChangeWeight()
		$this->markTestIncomplete( "changeWeight test not implemented" );
		$this->Bag_Model->changeWeight(/* parameters */);
	}

	/**
	 * Tests Bag_Model->getWeightLimit()
	 */
	public function testGetWeightLimit()
	{
		// TODO Auto-generated Bag_ModelTest->testGetWeightLimit()
		$this->markTestIncomplete( "getWeightLimit test not implemented" );
		$this->Bag_Model->getWeightLimit(/* parameters */);
	}

	/**
	 * Tests Bag_Model->setWeightLimit()
	 */
	public function testSetWeightLimit()
	{
		// TODO Auto-generated Bag_ModelTest->testSetWeightLimit()
		$this->markTestIncomplete( "setWeightLimit test not implemented" );
		$this->Bag_Model->setWeightLimit(/* parameters */);
	}

	/**
	 * Tests Bag_Model->setColor()
	 */
	public function testSetColor()
	{
		$this->Bag_Model->setColor( 1 , 2 );
		$color = $this->Bag_Model->getColor( 1 );
		$this->assertEquals(  $color ,  2 );
	}

	/**
	 * Tests Bag_Model->getColor()
	 */
	public function testGetColor()
	{
		// TODO Auto-generated Bag_ModelTest->testGetColor()
		$this->markTestIncomplete( "getColor test not implemented" );
		$this->Bag_Model->getColor(/* parameters */);
	}

	/**
	 * Tests Bag_Model->getData()
	 */
	public function testGetData()
	{
		// TODO Auto-generated Bag_ModelTest->testGetData()
		$this->markTestIncomplete( "getData test not implemented" );
		$this->Bag_Model->getData(/* parameters */);
	}
}

