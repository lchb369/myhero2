<?php

require_once 'PHPUnit\Framework\TestCase.php';

/**
 * Ship_Model test case.
 */
class Ship_ModelTest extends PHPUnit_Framework_TestCase
{

	/**
	 * @var Ship_Model
	 */
	private $Ship_Model;
	
	const FAKE_USER_ID = 8148975;

	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp()
	{
		parent::setUp();
		// TODO Auto-generated Ship_ModelTest::setUp()
		$this->Ship_Model = Ship_Model::getInstance( self::FAKE_USER_ID );
	}

	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown()
	{
		// TODO Auto-generated Ship_ModelTest::tearDown()
		$this->Ship_Model = null;
		parent::tearDown();
	}

	/**
	 * Constructs the test case.
	 */
	public function __construct()
	{
		// TODO Auto-generated constructor
	}

	/**
	 * Tests Ship_Model::getInstance()
	 */
	public function testGetInstance()
	{
		// TODO Auto-generated Ship_ModelTest::testGetInstance()
		$this->markTestIncomplete( "getInstance test not implemented" );
		Ship_Model::getInstance(/* parameters */);
	}

	/**
	 * Tests Ship_Model->setShipInfo()
	 */
	public function testSetShipInfo()
	{
		// TODO Auto-generated Ship_ModelTest->testSetShipInfo()
		$this->markTestIncomplete( "setShipInfo test not implemented" );
		$this->Ship_Model->setShipInfo(/* parameters */);
	}

	/**
	 * Tests Ship_Model->getShipInfo()
	 */
	public function testGetShipInfo()
	{
		// TODO Auto-generated Ship_ModelTest->testGetShipInfo()
		$this->markTestIncomplete( "getShipInfo test not implemented" );
		$this->Ship_Model->getShipInfo(/* parameters */);
	}

	/**
	 * Tests Ship_Model->compute3Size()
	 */
	public function testCompute3Size()
	{
		// TODO Auto-generated Ship_ModelTest->testCompute3Size()
		$this->markTestIncomplete( "compute3Size test not implemented" );
		$this->Ship_Model->compute3Size(/* parameters */);
	}

	/**
	 * Tests Ship_Model->unlockSlot()
	 */
	public function testUnlockSlot()
	{
		// TODO Auto-generated Ship_ModelTest->testUnlockSlot()
		$this->markTestIncomplete( "unlockSlot test not implemented" );
		$this->Ship_Model->unlockSlot(/* parameters */);
	}

	/**
	 * Tests Ship_Model->isUnlockedSlot()
	 */
	public function testIsUnlockedSlot()
	{
		// TODO Auto-generated Ship_ModelTest->testIsUnlockedSlot()
		$this->markTestIncomplete( "isUnlockedSlot test not implemented" );
		$this->Ship_Model->isUnlockedSlot(/* parameters */);
	}

	/**
	 * Tests Ship_Model->getUnlockSlot()
	 */
	public function testGetUnlockSlot()
	{
		// TODO Auto-generated Ship_ModelTest->testGetUnlockSlot()
		$this->markTestIncomplete( "getUnlockSlot test not implemented" );
		$this->Ship_Model->getUnlockSlot(/* parameters */);
	}

	/**
	 * Tests Ship_Model->changeBaseMP()
	 */
	public function testChangeBaseMP()
	{
		// TODO Auto-generated Ship_ModelTest->testChangeBaseMP()
		$this->markTestIncomplete( "changeBaseMP test not implemented" );
		$this->Ship_Model->changeBaseMP(/* parameters */);
	}

	/**
	 * Tests Ship_Model->changeBaseHP()
	 */
	public function testChangeBaseHP()
	{
		// TODO Auto-generated Ship_ModelTest->testChangeBaseHP()
		$this->markTestIncomplete( "changeBaseHP test not implemented" );
		$this->Ship_Model->changeBaseHP(/* parameters */);
	}

	/**
	 * Tests Ship_Model->changeMovement()
	 */
	public function testChangeMovement()
	{
		// TODO Auto-generated Ship_ModelTest->testChangeMovement()
		$this->markTestIncomplete( "changeMovement test not implemented" );
		$this->Ship_Model->changeMovement(/* parameters */);
	}

	/**
	 * Tests Ship_Model->changePlaySpeed()
	 */
	public function testChangePlaySpeed()
	{
		// TODO Auto-generated Ship_ModelTest->testChangePlaySpeed()
		$this->markTestIncomplete( "changePlaySpeed test not implemented" );
		$this->Ship_Model->changePlaySpeed(/* parameters */);
	}

	/**
	 * Tests Ship_Model->changeBasePoint()
	 */
	public function testChangeBasePoint()
	{
		// TODO Auto-generated Ship_ModelTest->testChangeBasePoint()
		$this->markTestIncomplete( "changeBasePoint test not implemented" );
		$this->Ship_Model->changeBasePoint(/* parameters */);
	}

	/**
	 * Tests Ship_Model->getMPLimit()
	 */
	public function testGetMPLimit()
	{
		// TODO Auto-generated Ship_ModelTest->testGetMPLimit()
		$this->markTestIncomplete( "getMPLimit test not implemented" );
		$this->Ship_Model->getMPLimit(/* parameters */);
	}

	/**
	 * Tests Ship_Model->changeMpLimit()
	 */
	public function testChangeMpLimit()
	{
		// TODO Auto-generated Ship_ModelTest->testChangeMpLimit()
		$this->markTestIncomplete( "changeMpLimit test not implemented" );
		$this->Ship_Model->changeMpLimit(/* parameters */);
	}

	/**
	 * Tests Ship_Model->getHPLimit()
	 */
	public function testGetHPLimit()
	{
		// TODO Auto-generated Ship_ModelTest->testGetHPLimit()
		$this->markTestIncomplete( "getHPLimit test not implemented" );
		$this->Ship_Model->getHPLimit(/* parameters */);
	}

	/**
	 * Tests Ship_Model->getMP()
	 */
	public function testGetMP()
	{
		// TODO Auto-generated Ship_ModelTest->testGetMP()
		$this->markTestIncomplete( "getMP test not implemented" );
		$this->Ship_Model->getMP(/* parameters */);
	}

	/**
	 * Tests Ship_Model->getDamage()
	 */
	public function testGetDamage()
	{
		// TODO Auto-generated Ship_ModelTest->testGetDamage()
		$this->markTestIncomplete( "getDamage test not implemented" );
		$this->Ship_Model->getDamage(/* parameters */);
	}

	/**
	 * Tests Ship_Model->getAgility()
	 */
	public function testGetAgility()
	{
		// TODO Auto-generated Ship_ModelTest->testGetAgility()
		$this->markTestIncomplete( "getAgility test not implemented" );
		$this->Ship_Model->getAgility(/* parameters */);
	}

	/**
	 * Tests Ship_Model->getSailor()
	 */
	public function testGetSailor()
	{
		// TODO Auto-generated Ship_ModelTest->testGetSailor()
		$this->markTestIncomplete( "getSailor test not implemented" );
		$this->Ship_Model->getSailor(/* parameters */);
	}

	/**
	 * Tests Ship_Model->getBasePoint()
	 */
	public function testGetBasePoint()
	{
		// TODO Auto-generated Ship_ModelTest->testGetBasePoint()
		$this->markTestIncomplete( "getBasePoint test not implemented" );
		$this->Ship_Model->getBasePoint(/* parameters */);
	}

	/**
	 * Tests Ship_Model->changeMP()
	 */
	public function testChangeMP()
	{
		// TODO Auto-generated Ship_ModelTest->testChangeMP()
		$this->markTestIncomplete( "changeMP test not implemented" );
		$this->Ship_Model->changeMP(/* parameters */);
	}

	/**
	 * Tests Ship_Model->actionReduceMp()
	 */
	public function testActionReduceMp()
	{
		// TODO Auto-generated Ship_ModelTest->testActionReduceMp()
		$this->markTestIncomplete( "actionReduceMp test not implemented" );
		$this->Ship_Model->actionReduceMp(/* parameters */);
	}

	/**
	 * Tests Ship_Model->actionRollbackMp()
	 */
	public function testActionRollbackMp()
	{
		// TODO Auto-generated Ship_ModelTest->testActionRollbackMp()
		$this->markTestIncomplete( "actionRollbackMp test not implemented" );
		$this->Ship_Model->actionRollbackMp(/* parameters */);
	}

	/**
	 * Tests Ship_Model->getTodayMpOfMonster()
	 */
	public function testGetTodayMpOfMonster()
	{
		// TODO Auto-generated Ship_ModelTest->testGetTodayMpOfMonster()
		$this->markTestIncomplete( "getTodayMpOfMonster test not implemented" );
		$this->Ship_Model->getTodayMpOfMonster(/* parameters */);
	}

	/**
	 * Tests Ship_Model->activeToCompute3Size()
	 */
	public function testActiveToCompute3Size()
	{
		// TODO Auto-generated Ship_ModelTest->testActiveToCompute3Size()
		$this->markTestIncomplete( "activeToCompute3Size test not implemented" );
		$this->Ship_Model->activeToCompute3Size(/* parameters */);
	}

	/**
	 * Tests Ship_Model->attackReduceSailor()
	 */
	public function testAttackReduceSailor()
	{
		// TODO Auto-generated Ship_ModelTest->testAttackReduceSailor()
		$this->markTestIncomplete( "attackReduceSailor test not implemented" );
		$this->Ship_Model->attackReduceSailor(/* parameters */);
	}

	/**
	 * Tests Ship_Model->changeSailor()
	 */
	public function testChangeSailor()
	{
		// TODO Auto-generated Ship_ModelTest->testChangeSailor()
		$this->markTestIncomplete( "changeSailor test not implemented" );
		$this->Ship_Model->changeSailor(/* parameters */);
	}

	/**
	 * Tests Ship_Model->checkSailor()
	 */
	public function testCheckSailor()
	{
		// TODO Auto-generated Ship_ModelTest->testCheckSailor()
		$this->markTestIncomplete( "checkSailor test not implemented" );
		$this->Ship_Model->checkSailor(/* parameters */);
	}

	/**
	 * Tests Ship_Model->sailorIsEnough()
	 */
	public function testSailorIsEnough()
	{
		// TODO Auto-generated Ship_ModelTest->testSailorIsEnough()
		$this->markTestIncomplete( "sailorIsEnough test not implemented" );
		$this->Ship_Model->sailorIsEnough(/* parameters */);
	}

	/**
	 * Tests Ship_Model->addNpc()
	 */
	public function testAddNpc()
	{
		// TODO Auto-generated Ship_ModelTest->testAddNpc()
		$this->markTestIncomplete( "addNpc test not implemented" );
		$this->Ship_Model->addNpc(/* parameters */);
	}

	/**
	 * Tests Ship_Model->removeNpc()
	 */
	public function testRemoveNpc()
	{
		// TODO Auto-generated Ship_ModelTest->testRemoveNpc()
		$this->markTestIncomplete( "removeNpc test not implemented" );
		$this->Ship_Model->removeNpc(/* parameters */);
	}

	/**
	 * Tests Ship_Model->getData()
	 */
	public function testGetData()
	{
		// TODO Auto-generated Ship_ModelTest->testGetData()
		$this->markTestIncomplete( "getData test not implemented" );
		$this->Ship_Model->getData(/* parameters */);
	}
}

