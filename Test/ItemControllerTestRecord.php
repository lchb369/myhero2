<?php
/*! @file   ItemController.php
*   @author Nebula yudong.jiang@fminutes.com
*   @date   2010.11.02
*   @brief  装备的强化
*   


测试controller同时以下都测试通过。

装备强化
熔合装备


注册用户
添加装备
添加物品
使用装备
取下装备
改变装备等级
计算三围
改变基本能力值
改变MP上限
根据装备位置ID获取对应在背包中的位置ID
和controller中调用过的方法都经过测试。

*/
class ItemController extends BaseController
{
	
	/**
	 * 强化装备
	 *
	 */
	public function reinforce()
	{
		$this->userId = 101;
		
		
		
		$bagModel = Bag_Model::getInstance( $this->userId , true );
		$userModel = User_Model::getInstance( $this->userId , true );
	    $shipModel = Ship_Model::getInstance( $this->userId , true  );
	    
	   // $userModel->register( $this->userId );
	   //  $userModel->save();
	   // exit;
	    
	    
	    /**
	     * 查看该用户数据
	     */
	    echo "<br>****************用户数据**************<br />";
	    $userData = $userModel->getUser();
		print_r( $userData );
		echo "<br>****************背包数据**************<br />";
		$bagData = $bagModel->getData();
		print_r( $bagData );
		echo "<br>****************船只数据**************<br />";
		$shipData = $shipModel->getData();
		print_r( $shipData );
		echo "<br>****************船只三围**************<br />";
		$compute3Size = $shipModel->compute3Size( $userModel , $bagModel );
		print_r( $compute3Size );
		/**
		 *  建立测试条件
		 */

		$_POST['bagId'] = 1;//装备在背包的位置为1,由船信息得知此装备为：中帆23001
		
		//往背包中添加幸运符 itemId => 11001 
		//$bagModel->changeItem( 11001 , 10 );

		$_POST['luckyRuneItemId'] = 11001; //幸运符
		
		$_POST['GraceRuneItemId'] = 12001; //神恩符
		//往背包中添加强化石 itemId => 11001 
		//$bagModel->changeItem( 10001 , 10 );
		//$bagModel->changeItem( 10002 , 10 );
		//$bagModel->changeItem( 12001 , 100 );
		//$bagModel->save();

		
	    $_POST['glyphItem'] = array();
		$arr = array(
             'itemId' => 10001, //雕文的道具ID
             'number' => 2, //雕文数量
              ) ;
		
		$_POST['glyphItem'][] = $arr;
		//要强化的装备不能在使用中
		
		//$shipModel->mountEquipment( $bagModel , 1 ); //把中帆装上

		//$shipModel->unmountEquipment( $bagModel , 5 ); //把中帆取下
		//$shipModel->changeMPLimit( 500 );
		//$bagModel->save();
		//$shipModel->save();
	

		
		/**
		 * 测试正文
		 */
		
	   echo "<hr>";
		
		/**
		 * 以上是测试
		 */

		//装备在背包的位置
		if( ( $bagId = (integer)$_POST['bagId'] ) <= 0 )
		{
			return array(
					'errorCode' => 100,		//参数错误
			     );
		}
		//幸运符的道具ID
		if( ( $luckyRuneItemId = (integer)$_POST['luckyRuneItemId'] ) <= 0 )
		{
			return array(
					'errorCode' => 100,		//参数错误
			     );
		}
		//神恩符道具id
		if( ( $graceRuneItemId = ( integer )$_POST['GraceRuneItemId'] ) <= 0 )
		{
			return array(
					'errorCode' => 100,		//参数错误
			     );
		}
		
		//雕文（强化石）的道具ID和数量
		if( !$_POST['glyphItem']  || !is_array( $_POST['glyphItem'] ) || empty( $_POST['glyphItem'] ) )
		{
			return array(
					'errorCode' => 100,		//参数错误
			     );
		}
		$glyphItem = $_POST['glyphItem'];
		//$bagModel = Bag_Model::getInstance( $this->userId , true );
		//判断背包中是否存在装备
		if( !$bagModel->haveEquipment( $bagId ) )
		{
			return array(
					'errorCode' => 201,		//装备不存在
			     );
		}
		
		//判断背包中是否存在幸运符
		if( !$bagModel->haveItem( $luckyRuneItemId ) )
		{
			return array(
					'errorCode' => 202,		//幸运符不存在
			     );
		}
		//判断背包中是否存在神恩符
		if( !$bagModel->haveItem( $graceRuneItemId ) )
		{
			return array(
					'errorCode' => 203,		//神恩符不存在
			     );
		}
			
		//判断强化石数量
		foreach ( $glyphItem as $item )
		{
			if( $bagModel->getHowMany( $item['itemId'] ) < $item['number'] )
			{
				return array(
					'errorCode' => 204,		//强化石不存在或数量不够
			     );
			}
		}

		
		//$userModel = User_Model::getInstance( $this->userId , true );
		//强化
		try {
			$res = Item_Equipment::reinforce( $bagModel , $userModel , $this->userId , $bagId , $glyphItem , $luckyRuneItemId , $graceRuneItemId );
		}
		catch ( Exception $e )
		{
			return array(
					'errorCode' => 210,		//强化错误
			     );
		}
		
		$bagModel->save();
		$userModel->save();
		$returnData = $bagModel->getBagInfo();
		return $returnData;
	}
	/**
	 * 熔合装备
	 */
	public function fusion()
	{
		
		$this->userId = 101;
		$bagModel = Bag_Model::getInstance( $this->userId , true );
		$userModel = User_Model::getInstance( $this->userId , true );
	    $shipModel = Ship_Model::getInstance( $this->userId , true  );
	    /**
	     * 查看该用户数据
	     */
	    echo "<br>****************用户数据**************<br />";
	    $userData = $userModel->getUser();
		print_r( $userData );
		echo "<br>****************背包数据**************<br />";
		$bagData = $bagModel->getData();
		print_r( $bagData );
		echo "<br>****************船只数据**************<br />";
		$shipData = $shipModel->getData();
		print_r( $shipData );
		echo "<br>****************船只三围**************<br />";
		$compute3Size = $shipModel->compute3Size( $userModel , $bagModel );
		print_r( $compute3Size );
		
		/**
		 *  建立测试条件
		 */
		//老装备
		$oldItemId = 24001;//$oldBagId = 6,
		//添加一件新装备
		$newItemId = 24010;//$newBagId = 7,
		
		//$bagModel->addEquipment( $itemId  );
		//$bagModel->save();
		$_POST['fromEquipmentBagId'] = 6;
		$_POST['toEquipmentBagId']  = 7;
		$_POST['fusionLevel'] = 2;
		
		/*$bagModel->changeItem( 13001 , 100 );
		$bagModel->changeItem( 13002 , 100 );
		$bagModel->save();*/
		
		echo "<hr>";
	
		//老装备
		if( ( $fromEquipmentBagId = ( integer)$_POST['fromEquipmentBagId'] ) <= 0 )
		{
			return array(
					'errorCode' => 100,		//参数错误
			     );
		}
		//新装备，要熔合的装备
		if( ( $toEquipmentBagId = ( integer )$_POST['toEquipmentBagId'] ) <= 0 )
		{
			return array(
					'errorCode' => 100,		//参数错误
			     );
		}
		//熔合石
		if( ( $fusionLevel = ( integer)$_POST['fusionLevel'] ) <= 0 )
		{
			return array(
					'errorCode' => 100,		//参数错误
			     );
		}
	
		//$bagModel = Bag_Model::getInstance( $this->userId , true );
		//判断背包中是否存在装备
		if( !$bagModel->haveEquipment( $fromEquipmentBagId ) )
		{
			return array(
					'errorCode' => 201,		//装备不存在
			     );
		}
		
		//判断背包中是否存在装备
		if( !$bagModel->haveEquipment( $toEquipmentBagId ) )
		{
			return array(
					'errorCode' => 201,		//装备不存在
			     );
		}
					
		
		try {
			
			Item_Equipment::fusion( $bagModel , $fromEquipmentBagId , $toEquipmentBagId , $fusionLevel );
				
		}
		catch ( Exception $e )
		{
			return array(
					'errorCode' => 211,		//熔合错误
			     );
		}
		
		$bagModel->save();
		$returnData = $bagModel->getBagInfo();
		return $returnData;
		
	}
	/**
	 * 合成雕文（强化石）
	 */
	public function composite()
	{
		$this->userId = 101;
		$bagModel = Bag_Model::getInstance( $this->userId , true );
		$userModel = User_Model::getInstance( $this->userId , true );
	    $shipModel = Ship_Model::getInstance( $this->userId , true  );
	    /**
	     * 查看该用户数据
	     */
	    echo "<br>****************用户数据**************<br />";
	    $userData = $userModel->getUser();
		print_r( $userData );
		echo "<br>****************背包数据**************<br />";
		$bagData = $bagModel->getData();
		print_r( $bagData );
		echo "<br>****************船只数据**************<br />";
		$shipData = $shipModel->getData();
		print_r( $shipData );
		echo "<br>****************船只三围**************<br />";
		$compute3Size = $shipModel->compute3Size( $userModel , $bagModel );
		print_r( $compute3Size );
		//强化石id
		if( ( $glyphItemId = ( integer)$_POST['glyphItemId'] ) <= 0 )
		{
			return array(
					'errorCode' => 100,		//参数错误
			     );
		}
		
		if( ( $number = ( integer)$_POST['number'] ) <= 0 )
		{
			return array(
					'errorCode' => 100,		//参数错误
			     );
		}
		
		$bagModel = Bag_Model::getInstance( $this->userId , true );
		if( $bagModel->getHowMany( $glyphItemId ) < $number )
		{
			return array(
					'errorCode' => 204,		//强化石不存在或数量不够
			     );
		}
		
		if( ( $glyphCompositeRuneItemId = ( integer )$_POST['glyphCompositeRuneItemId'] ) <= 0 )
		{
			$glyphCompositeRuneItemId = 0;
		}

		try {
			Item_Material::composite( $bagModel , $glyphItemId , $number , $glyphCompositeRuneItemId );
		}
		catch ( Exception $e )
		{
			return array(
					'errorCode' => 212,		//合成错误
			     );
		}
		
		$bagModel->save();
		$returnData = $bagModel->getBagInfo();
		return $returnData;
	}
}



?>