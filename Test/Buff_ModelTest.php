<?php

require_once 'Model\Buff\Model.php';

require_once 'PHPUnit\Framework\TestCase.php';

/**
 * Buff_Model test case.
 */
class Buff_ModelTest extends PHPUnit_Framework_TestCase {
	
	/**
	 * @var Buff_Model
	 */
	private $Buff_Model;
	
	const FAKE_USER_ID = 89237456;
	
	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp() {
		parent::setUp ();
		// TODO Auto-generated Buff_ModelTest::setUp()
		$buffConfig = array(
			 130001 => 
			  array (
			    'duration' => 0,
			    'isAttacked' => 1,
			    'times' => 30,
			    'type' => 2,
			    'power' => 1.1,
			  ),
			  //BUFF类型打好友时加攻击力百分比持续100秒
			  130002 => 
			  array (
			    'duration' => 100,
			    'isAttacked' => 1,
			    'times' => 0,
			    'type' => 11,
			    'power' => 1.1,
			  ),
			  130003 => 
			  array (
			    'duration' => 0,
			    'isAttacked' => 1,
			    'times' => 30,
			    'type' => 3,
			    'power' => 1.1,
			  ),
			  130004 => 
			  array (
			    'duration' => 0,
			    'isAttacked' => 1,
			    'times' => 30,
			    'type' => 4,
			    'power' => 1.15,
			  ),
		);
		Logical_Abstract::setConfig( 'BuffConfig', $buffConfig );
		
		
		
		
		//初始化map
		$buffMock = Data_Buff_Model::getInstance( self::FAKE_USER_ID , true , true ,true );
		$buffMock->set( 'buffList' , array());
				
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'buffModel' , $buffMock );
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'buffModelWithWriteLock' , $buffMock );
		$this->Buff_Model = Buff_Model::getInstance( self::FAKE_USER_ID );
	}
	
	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown() {
		// TODO Auto-generated Buff_ModelTest::tearDown()
		

		$this->Buff_Model = null;
		
		parent::tearDown ();
	}
	
	/**
	 * Constructs the test case.
	 */
	public function __construct() {
		// TODO Auto-generated constructor
	}
	
	/**
	 * Tests Buff_Model::getInstance()
	 */
	public function testGetInstance() {
		// TODO Auto-generated Buff_ModelTest::testGetInstance()
		$this->markTestIncomplete ( "getInstance test not implemented" );
		
		Buff_Model::getInstance(/* parameters */);
	
	}
	
	/**
	 * Tests Buff_Model->__construct()
	 */
	public function test__construct() {
		// TODO Auto-generated Buff_ModelTest->test__construct()
		$this->markTestIncomplete ( "__construct test not implemented" );
		
		$this->Buff_Model->__construct(/* parameters */);
	
	}
	
	/**
	 * Tests Buff_Model->isUsedBuff()
	 */
	public function testIsUsedBuff() {
		// TODO Auto-generated Buff_ModelTest->testIsUsedBuff()
		$this->markTestIncomplete ( "isUsedBuff test not implemented" );
		
		$this->Buff_Model->isUsedBuff(/* parameters */);
	
	}
	
	/**
	 * Tests Buff_Model->setBuff()
	 */
	public function testSetBuff() {
		// TODO Auto-generated Buff_ModelTest->testSetBuff()
		//$this->markTestIncomplete ( "setBuff test not implemented" );
		$this->assertTrue( $this->Buff_Model->setBuff( 130001 ));
		//print_r( $this->Buff_Model->getBuffList());
	}
	
	/**
	 * Tests Buff_Model->useBuff()
	 */
	public function testUseBuff() {
		// TODO Auto-generated Buff_ModelTest->testUseBuff()
		$this->markTestIncomplete ( "useBuff test not implemented" );
		$this->Buff_Model->useBuff(/* parameters */);
	
	}
	
	/**
	 * Tests Buff_Model->getBuffList()
	 */
	public function testGetBuffList() {
		// TODO Auto-generated Buff_ModelTest->testGetBuffList()
		$this->markTestIncomplete ( "getBuffList test not implemented" );
		
		$this->Buff_Model->getBuffList(/* parameters */);
	
	}
	
	/**
	 * Tests Buff_Model->getBuffByBuffId()
	 */
	public function testGetBuffByBuffId() {
		// TODO Auto-generated Buff_ModelTest->testGetBuffByBuffId()
		$this->markTestIncomplete ( "getBuffByBuffId test not implemented" );
		
		$this->Buff_Model->getBuffByBuffId(/* parameters */);
	
	}
	
	/**
	 * Tests Buff_Model->setOffBuff()
	 */
	public function testSetOffBuff() {
		// TODO Auto-generated Buff_ModelTest->testSetOffBuff()
		$this->markTestIncomplete ( "setOffBuff test not implemented" );
		
		$this->Buff_Model->setOffBuff(/* parameters */);
	
	}
	
	/**
	 * Tests Buff_Model->getBuffInfo()
	 */
	public function testGetBuffInfo() {
		// TODO Auto-generated Buff_ModelTest->testGetBuffInfo()
		$this->markTestIncomplete ( "getBuffInfo test not implemented" );
		
		$this->Buff_Model->getBuffInfo(/* parameters */);
	
	}
	
	/**
	 * Tests Buff_Model->getBuffedAbility()
	 */
	public function testGetBuffedAbility() {
		// TODO Auto-generated Buff_ModelTest->testGetBuffedAbility()
		//$this->markTestIncomplete ( "getBuffedAbility test not implemented" );
		$attr = array(
			'hpLimit'=> 1000,
		  	'damage' => 100,
		  	'agility' => 100,
		  	'mpLimit' => 100,
		  	'robAll' => true,
		  	'notLostGold' => true,
		  	'clearFog' => true,
		  	'moveSpeed' => 100,
		);
		//决斗时攻击提高百分之十，可以使用30次
		$this->assertTrue( $this->Buff_Model->setBuff( 130001 ));
		$buffedAttr = $this->Buff_Model->getBuffedAbility( $attr , Buff_Model::USE_BUFF_FOR_DUEL , 1 );
		//攻击力是否提到了110
		$this->assertEquals( 110 , $buffedAttr['damage'] );
		$buffInfo = $this->Buff_Model->getBuffInfo( 130001 );
		$buffList = $this->Buff_Model->getBuffList();
		//还可以使用29次
		$this->assertEquals( 29 , $buffList[$buffInfo['type']]['times'] );
		//打好友时攻击提高百分之十
		$this->assertTrue( $this->Buff_Model->setBuff( 130002 ));
		$buffedAttr = $this->Buff_Model->getBuffedAbility( $attr , Buff_Model::USE_BUFF_FOR_ATTACK_FRIEND , 1 );
		//两种BUFF同时生效，攻击为20%
		$this->assertEquals( 120 , $buffedAttr['damage']);

	}
	
	/**
	 * Tests Buff_Model->getRangeDamage()
	 */
	public function testGetRangeDamage() {
		// TODO Auto-generated Buff_ModelTest->testGetRangeDamage()
		$this->markTestIncomplete ( "getRangeDamage test not implemented" );
		
		$this->Buff_Model->getRangeDamage(/* parameters */);
	
	}
	
	/**
	 * Tests Buff_Model->changeEndTime()
	 */
	public function testChangeEndTime() {
		// TODO Auto-generated Buff_ModelTest->testChangeEndTime()
		$this->markTestIncomplete ( "changeEndTime test not implemented" );
		
		$this->Buff_Model->changeEndTime(/* parameters */);
	
	}
	
	/**
	 * Tests Buff_Model->getData()
	 */
	public function testGetData() {
		// TODO Auto-generated Buff_ModelTest->testGetData()
		$this->markTestIncomplete ( "getData test not implemented" );
		
		$this->Buff_Model->getData(/* parameters */);
	
	}

}

