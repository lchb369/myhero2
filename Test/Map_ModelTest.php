<?php

require_once 'PHPUnit\Framework\TestCase.php';
/**
 * Map_Model test case.
 */
class Map_ModelTest extends PHPUnit_Framework_TestCase
{

	/**
	 * @var Map_Model
	 */
	private $Map_Model;
	const FAKE_USER_ID = 89237456;

	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp()
	{
		parent::setUp();
		// TODO Auto-generated Map_ModelTest::setUp()
		$config = array( 
			100020 => array( 
				'birthPlace' => array( 
					'passCoordinate' => array( 
						'x' => 140 , 
						'y' => 0 , 
						'z' => 109 
					) , 
					'notPassCoordinate' => array( 
						'x' => 140 , 
						'y' => 0 , 
						'z' => 109 
					) 
				) , 
				'condition' => array( 
					'popularity' => 100 
				) , 
				'units' => array() 
			) , 
			100030 => array( 
				'birthPlace' => array( 
					'passCoordinate' => array( 
						'x' => 140 , 
						'y' => 0 , 
						'z' => 109 
					) , 
					'notPassCoordinate' => array( 
						'x' => 140 , 
						'y' => 0 , 
						'z' => 109 
					) 
				) , 
				'condition' => array( 
					'popularity' => 100 
				) , 
				'units' => array() 
			) , 
			100050 => array( 
				'birthPlace' => array( 
					'passCoordinate' => array( 
						'x' => 140 , 
						'y' => 0 , 
						'z' => 109 
					) , 
					'notPassCoordinate' => array( 
						'x' => 140 , 
						'y' => 0 , 
						'z' => 109 
					) 
				) , 
				'condition' => array( 
					'popularity' => 100000 
				) , 
				'units' => array() 
			) 
		);
		Logical_Abstract::setConfig( 'MapConfig' , $config );
		//初始化map
		$mapMock = Data_Map_Model::getInstance( self::FAKE_USER_ID , true, true ,true );
		$mapMock->set( 'deadUnitList' , array() );
		$mapMock->set( 'mapId' , 100010 );
		$mapMock->set( 'unlockedMap' , array( 100010 ) );
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'mapModel' , $mapMock );
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'mapModelWithWriteLock' , $mapMock );
		//初始化userProfile
		$userProfileMock = Data_User_Profile::getInstance(  self::FAKE_USER_ID , true , true , true );
		$userProfileMock->set( 'clearStage' , 100010 );
		$userProfileMock->set( 'popularity' , 1000 );
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'userProfile' , $userProfileMock );
		Logical_Abstract::setObject( self::FAKE_USER_ID , 'userProfileWithWriteLock' , $userProfileMock );
		$this->Map_Model = Map_Model::getInstance( self::FAKE_USER_ID );
	}

	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown()
	{
		// TODO Auto-generated Map_ModelTest::tearDown()
		$this->Map_Model = null;
		parent::tearDown();
	}

	/**
	 * Constructs the test case.
	 */
	public function __construct()
	{
		// TODO Auto-generated constructor
	}

	/**
	 * Tests Map_Model::getInstance()
	 */
	public function testGetInstance()
	{
		// TODO Auto-generated Map_ModelTest::testGetInstance()
		$this->markTestIncomplete( "getInstance test not implemented" );
		Map_Model::getInstance(/* parameters */);
	}

	/**
	 * Tests Map_Model->unlockMap()
	 */
	public function testUnlockMap()
	{
		try
		{
			$this->Map_Model->unlockMap( 100030 );
		}
		catch( Map_Exception $me )
		{
			$this->assertEquals( Map_Exception::STATUS_ERROR_NOT_EXIST_MAP , $me->getCode() );
		}
		try
		{
			$this->Map_Model->unlockMap( 100050 );
		}
		catch( Map_Exception $me )
		{
			$this->assertEquals( Map_Exception::STATUS_ERROR_NOT_ENOUGH_POPULARITY , $me->getCode() );
		}
		$this->Map_Model->unlockMap( 100020 );
	}

	/**
	 * Tests Map_Model->isUnLockMap()
	 */
	public function testIsUnlockMap()
	{
		$this->Map_Model->unlockMap( 100020 );
		$this->assertEquals( true , $this->Map_Model->isUnlockMap( 100020 ) );
		$this->assertEquals( false , $this->Map_Model->isUnlockMap( 100070 ) );
	}

	/**
	 * Tests Map_Model->lockMap()
	 */
	public function testLockMap()
	{
		$this->Map_Model->unlockMap( 100020 );
		$this->Map_Model->lockMap( 100020 );
		$this->assertEquals( false , $this->Map_Model->isUnlockMap( 100020 ) );
	}

	/**
	 * Tests Map_Model->changeMap()
	 */
	public function testChangeMap()
	{
		//如果没有解锁，则抛错
		try
		{
			$this->Map_Model->changeMap( 100020 );
		}
		catch( Map_Exception $me )
		{
			$this->assertEquals( Map_Exception::STATUS_ERROR_NOT_CLEARSTAGE , $me->getCode() );
		}
		//将地图切换到100020后，获取当前地图ID是否等于100020
		$this->Map_Model->unlockMap( 100020 );
		$this->Map_Model->changeMap( 100020 );
		$data = $this->Map_Model->getData();
		$this->assertEquals( 100020 , $data['mapId'] );
		$coordinate = array( 
			'x' => 140 , 
			'y' => 0 , 
			'z' => 109 
		);
		$this->assertEquals( $coordinate , $data['coordinate'] );
	}

	/**
	 * Tests Map_Model->move()
	 */
	public function testMove()
	{
		// TODO Auto-generated Map_ModelTest->testMove()
		$this->markTestIncomplete( "move test not implemented" );
		$this->Map_Model->move(/* parameters */);
	}

	/**
	 * Tests Map_Model->clearDeadUnit()
	 */
	public function testClearDeadUnit()
	{
		// TODO Auto-generated Map_ModelTest->testClearDeadUnit()
		$this->markTestIncomplete( "clearDeadUnit test not implemented" );
		$this->Map_Model->clearDeadUnit(/* parameters */);
	}

	/**
	 * Tests Map_Model->refreshUnit()
	 */
	public function testRefreshUnit()
	{
		// TODO Auto-generated Map_ModelTest->testRefreshUnit()
		$this->markTestIncomplete( "refreshUnit test not implemented" );
		$this->Map_Model->refreshUnit(/* parameters */);
	}

	/**
	 * Tests Map_Model->getMonsterAttribute()
	 */
	public function testGetMonsterAttribute()
	{
		// TODO Auto-generated Map_ModelTest->testGetMonsterAttribute()
		$this->markTestIncomplete( "getMonsterAttribute test not implemented" );
		$this->Map_Model->getMonsterAttribute(/* parameters */);
	}

	/**
	 * Tests Map_Model->setMonsterHP()
	 */
	public function testSetMonsterHP()
	{
		// TODO Auto-generated Map_ModelTest->testSetMonsterHP()
		$this->markTestIncomplete( "setMonsterHP test not implemented" );
		$this->Map_Model->setMonsterHP(/* parameters */);
	}

	/**
	 * Tests Map_Model->getPointInfo()
	 */
	public function testGetPointInfo()
	{
		// TODO Auto-generated Map_ModelTest->testGetPointInfo()
		$this->markTestIncomplete( "getPointInfo test not implemented" );
		$this->Map_Model->getPointInfo(/* parameters */);
	}

	/**
	 * Tests Map_Model->isBossMonster()
	 */
	public function testIsBossMonster()
	{
		// TODO Auto-generated Map_ModelTest->testIsBossMonster()
		$this->markTestIncomplete( "isBossMonster test not implemented" );
		$this->Map_Model->isBossMonster(/* parameters */);
	}

	/**
	 * Tests Map_Model->getMapId()
	 */
	public function testGetMapId()
	{
		// TODO Auto-generated Map_ModelTest->testGetMapId()
		$this->markTestIncomplete( "getMapId test not implemented" );
		$this->Map_Model->getMapId(/* parameters */);
	}

	/**
	 * Tests Map_Model->getTaskMonsters()
	 */
	public function testGetTaskMonsters()
	{
		// TODO Auto-generated Map_ModelTest->testGetTaskMonsters()
		$this->markTestIncomplete( "getTaskMonsters test not implemented" );
		$this->Map_Model->getTaskMonsters(/* parameters */);
	}

	/**
	 * Tests Map_Model->setTaskMonsters()
	 */
	public function testSetTaskMonsters()
	{
		// TODO Auto-generated Map_ModelTest->testSetTaskMonsters()
		$this->markTestIncomplete( "setTaskMonsters test not implemented" );
		$this->Map_Model->setTaskMonsters(/* parameters */);
	}

	/**
	 * Tests Map_Model->getMapConfig()
	 */
	public function testGetMapConfig()
	{
		// TODO Auto-generated Map_ModelTest->testGetMapConfig()
		$this->markTestIncomplete( "getMapConfig test not implemented" );
		$this->Map_Model->getMapConfig(/* parameters */);
	}

	/**
	 * Tests Map_Model->canPlunder()
	 */
	public function testCanPlunder()
	{
		// TODO Auto-generated Map_ModelTest->testCanPlunder()
		$this->markTestIncomplete( "canPlunder test not implemented" );
		$this->Map_Model->canPlunder(/* parameters */);
	}

	/**
	 * Tests Map_Model->getMonsterInfo()
	 */
	public function testGetMonsterInfo()
	{
		// TODO Auto-generated Map_ModelTest->testGetMonsterInfo()
		$this->markTestIncomplete( "getMonsterInfo test not implemented" );
		$this->Map_Model->getMonsterInfo(/* parameters */);
	}

	/**
	 * Tests Map_Model->getMonsterDropPackages()
	 */
	public function testGetMonsterDropPackages()
	{
		// TODO Auto-generated Map_ModelTest->testGetMonsterDropPackages()
		$this->markTestIncomplete( "getMonsterDropPackages test not implemented" );
		$this->Map_Model->getMonsterDropPackages(/* parameters */);
	}

	/**
	 * Tests Map_Model->getDeadMonsterList()
	 */
	public function testGetDeadMonsterList()
	{
		// TODO Auto-generated Map_ModelTest->testGetDeadMonsterList()
		$this->markTestIncomplete( "getDeadMonsterList test not implemented" );
		$this->Map_Model->getDeadMonsterList(/* parameters */);
	}

	/**
	 * Tests Map_Model->getMapConfigIndexId()
	 */
	public function testGetMapConfigIndexId()
	{
		// TODO Auto-generated Map_ModelTest->testGetMapConfigIndexId()
		$this->markTestIncomplete( "getMapConfigIndexId test not implemented" );
		$this->Map_Model->getMapConfigIndexId(/* parameters */);
	}

	/**
	 * Tests Map_Model->getData()
	 */
	public function testGetData()
	{
		// TODO Auto-generated Map_ModelTest->testGetData()
		$this->markTestIncomplete( "getData test not implemented" );
		$this->Map_Model->getData(/* parameters */);
	}
}

