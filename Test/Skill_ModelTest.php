<?php
require_once 'Model\Skill\Model.php';
require_once 'PHPUnit\Framework\TestCase.php';
/**
 * Skill_Model test case.
 */
class Skill_ModelTest extends PHPUnit_Framework_TestCase
{

	/**
	 * @var Skill_Model
	 */
	private $Skill_Model;
	
	const FAKE_USER_ID = 38945843;

	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp()
	{
		parent::setUp();
		// TODO Auto-generated Skill_ModelTest::setUp()
		$this->Skill_Model = Skill_Model::getInstance( self::FAKE_USER_ID );
	}

	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown()
	{
		// TODO Auto-generated Skill_ModelTest::tearDown()
		$this->Skill_Model = null;
		parent::tearDown();
	}

	/**
	 * Constructs the test case.
	 */
	public function __construct()
	{
		// TODO Auto-generated constructor
	}

	/**
	 * Tests Skill_Model::getInstance()
	 */
	public function testGetInstance()
	{
		// TODO Auto-generated Skill_ModelTest::testGetInstance()
		$this->markTestIncomplete( "getInstance test not implemented" );
		Skill_Model::getInstance(/* parameters */);
	}

	/**
	 * Tests Skill_Model->__construct()
	 */
	public function test__construct()
	{
		// TODO Auto-generated Skill_ModelTest->test__construct()
		$this->markTestIncomplete( "__construct test not implemented" );
		$this->Skill_Model->__construct(/* parameters */);
	}

	/**
	 * Tests Skill_Model->studySkill()
	 */
	public function testStudySkill()
	{
		// TODO Auto-generated Skill_ModelTest->testStudySkill()
		$this->markTestIncomplete( "studySkill test not implemented" );
		$this->Skill_Model->studySkill(/* parameters */);
	}

	/**
	 * Tests Skill_Model->resetSkill()
	 */
	public function testResetSkill()
	{
		// TODO Auto-generated Skill_ModelTest->testResetSkill()
		$this->markTestIncomplete( "resetSkill test not implemented" );
		$this->Skill_Model->resetSkill(/* parameters */);
	}

	/**
	 * Tests Skill_Model->upgradeSkill()
	 */
	public function testUpgradeSkill()
	{
		// TODO Auto-generated Skill_ModelTest->testUpgradeSkill()
		$this->markTestIncomplete( "upgradeSkill test not implemented" );
		$this->Skill_Model->upgradeSkill(/* parameters */);
	}

	/**
	 * Tests Skill_Model->setSkillOrder()
	 */
	public function testSetSkillOrder()
	{
		// TODO Auto-generated Skill_ModelTest->testSetSkillOrder()
		$this->markTestIncomplete( "setSkillOrder test not implemented" );
		$this->Skill_Model->setSkillOrder(/* parameters */);
	}

	/**
	 * Tests Skill_Model->isMasteredSkill()
	 */
	public function testIsMasteredSkill()
	{
		// TODO Auto-generated Skill_ModelTest->testIsMasteredSkill()
		$this->markTestIncomplete( "isMasteredSkill test not implemented" );
		$this->Skill_Model->isMasteredSkill(/* parameters */);
	}

	/**
	 * Tests Skill_Model->changeSkillOrderSlotCount()
	 */
	public function testChangeSkillOrderSlotCount()
	{
		// TODO Auto-generated Skill_ModelTest->testChangeSkillOrderSlotCount()
		$this->markTestIncomplete( "changeSkillOrderSlotCount test not implemented" );
		$this->Skill_Model->changeSkillOrderSlotCount(/* parameters */);
	}

	/**
	 * Tests Skill_Model->addNewOrder()
	 */
	public function testAddNewOrder()
	{
		// TODO Auto-generated Skill_ModelTest->testAddNewOrder()
		$this->markTestIncomplete( "addNewOrder test not implemented" );
		$this->Skill_Model->addNewOrder(/* parameters */);
	}

	/**
	 * Tests Skill_Model->removeOrder()
	 */
	public function testRemoveOrder()
	{
		// TODO Auto-generated Skill_ModelTest->testRemoveOrder()
		$this->markTestIncomplete( "removeOrder test not implemented" );
		$this->Skill_Model->removeOrder(/* parameters */);
	}

	/**
	 * Tests Skill_Model->setDefaultOrder()
	 */
	public function testSetDefaultOrder()
	{
		// TODO Auto-generated Skill_ModelTest->testSetDefaultOrder()
		$this->markTestIncomplete( "setDefaultOrder test not implemented" );
		$this->Skill_Model->setDefaultOrder(/* parameters */);
	}

	/**
	 * Tests Skill_Model->getDefaultOrderId()
	 */
	public function testGetDefaultOrderId()
	{
		// TODO Auto-generated Skill_ModelTest->testGetDefaultOrderId()
		$this->markTestIncomplete( "getDefaultOrderId test not implemented" );
		$this->Skill_Model->getDefaultOrderId(/* parameters */);
	}

	/**
	 * Tests Skill_Model->getDefaultOrder()
	 */
	public function testGetDefaultOrder()
	{
		// TODO Auto-generated Skill_ModelTest->testGetDefaultOrder()
		$this->markTestIncomplete( "getDefaultOrder test not implemented" );
		$this->Skill_Model->getDefaultOrder(/* parameters */);
	}

	/**
	 * Tests Skill_Model->getSkillLevel()
	 */
	public function testGetSkillLevel()
	{
		// TODO Auto-generated Skill_ModelTest->testGetSkillLevel()
		$this->markTestIncomplete( "getSkillLevel test not implemented" );
		$this->Skill_Model->getSkillLevel(/* parameters */);
	}

	/**
	 * Tests Skill_Model->getOrderList()
	 */
	public function testGetOrderList()
	{
		// TODO Auto-generated Skill_ModelTest->testGetOrderList()
		$this->markTestIncomplete( "getOrderList test not implemented" );
		$this->Skill_Model->getOrderList(/* parameters */);
	}

	/**
	 * Tests Skill_Model->deleteSkill()
	 */
	public function testDeleteSkill()
	{
		// TODO Auto-generated Skill_ModelTest->testDeleteSkill()
		$this->markTestIncomplete( "deleteSkill test not implemented" );
		$this->Skill_Model->deleteSkill(/* parameters */);
	}

	/**
	 * Tests Skill_Model->getSkillOrderSlotCount()
	 */
	public function testGetSkillOrderSlotCount()
	{
		// TODO Auto-generated Skill_ModelTest->testGetSkillOrderSlotCount()
		$this->markTestIncomplete( "getSkillOrderSlotCount test not implemented" );
		$this->Skill_Model->getSkillOrderSlotCount(/* parameters */);
	}

	/**
	 * Tests Skill_Model->getData()
	 */
	public function testGetData()
	{
		// TODO Auto-generated Skill_ModelTest->testGetData()
		$this->markTestIncomplete( "getData test not implemented" );
		$this->Skill_Model->getData(/* parameters */);
	}
}

