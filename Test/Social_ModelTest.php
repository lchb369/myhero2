<?php
/**
 * Social_Model test case.
 */
class Social_ModelTest extends PHPUnit_Framework_TestCase
{

	/**
	 * @var Social_Model
	 */
	private $Social_Model;
	
	const FAKE_USER_ID = 89237456;

	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp()
	{
		parent::setUp();
		// TODO Auto-generated Social_ModelTest::setUp()
		$this->Social_Model = Social_Model::getInstance( self::FAKE_USER_ID );
	}

	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown()
	{
		// TODO Auto-generated Social_ModelTest::tearDown()
		$this->Social_Model = null;
		parent::tearDown();
	}

	/**
	 * Constructs the test case.
	 */
	public function __construct()
	{
		// TODO Auto-generated constructor
	}

	/**
	 * Tests Social_Model::getInstance()
	 */
	public function testGetInstance()
	{
		// TODO Auto-generated Social_ModelTest::testGetInstance()
		$this->markTestIncomplete( "getInstance test not implemented" );
		Social_Model::getInstance(/* parameters */);
	}

	/**
	 * Tests Social_Model->isVisited()
	 */
	public function testIsVisited()
	{
		// TODO Auto-generated Social_ModelTest->testIsVisited()
		$this->markTestIncomplete( "isVisited test not implemented" );
		$this->Social_Model->isVisited(/* parameters */);
	}

	/**
	 * Tests Social_Model->setVisited()
	 */
	public function testSetVisited()
	{
		// TODO Auto-generated Social_ModelTest->testIsVisited()
		$this->markTestIncomplete( "isVisited test not implemented" );
		$this->Social_Model->setVisited(/* parameters */);
	}

	/**
	 * Tests Social_Model->getVisitedList()
	 */
	public function testGetVisitedList()
	{
		// TODO Auto-generated Social_ModelTest->testGetVisitedList()
		$this->markTestIncomplete( "getVisitedList test not implemented" );
		$this->Social_Model->getVisitedList(/* parameters */);
	}

	/**
	 * Tests Social_Model->isTodayCommunicated()
	 */
	public function testIsTodayCommunicated()
	{
		// TODO Auto-generated Social_ModelTest->testIsTodayCommunicated()
		$this->markTestIncomplete( "isTodayCommunicated test not implemented" );
		$this->Social_Model->isTodayCommunicated(/* parameters */);
	}

	/**
	 * Tests Social_Model->setTodayCommunicated()
	 */
	public function testSetTodayCommunicated()
	{
		// TODO Auto-generated Social_ModelTest->testSetTodayCommunicated()
		$this->markTestIncomplete( "setTodayCommunicated test not implemented" );
		$this->Social_Model->setTodayCommunicated(/* parameters */);
	}

	/**
	 * Tests Social_Model->getTodayCommunicatedFriendIds()
	 */
	public function testGetTodayCommunicatedFriendIds()
	{
		// TODO Auto-generated Social_ModelTest->testGetTodayCommunicatedFriendIds()
		$this->markTestIncomplete( "getTodayCommunicatedFriendIds test not implemented" );
		$this->Social_Model->getTodayCommunicatedFriendIds(/* parameters */);
	}

	/**
	 * Tests Social_Model->setCombatPerformance()
	 */
	public function testSetCombatPerformance()
	{
		// TODO Auto-generated Social_ModelTest->testSetCombatPerformance()
		$this->markTestIncomplete( "setCombatPerformance test not implemented" );
		$this->Social_Model->setCombatPerformance(/* parameters */);
	}

	/**
	 * Tests Social_Model->getCombatPerformance()
	 */
	public function testGetCombatPerformance()
	{
		// TODO Auto-generated Social_ModelTest->testGetCombatPerformance()
		$this->markTestIncomplete( "getCombatPerformance test not implemented" );
		$this->Social_Model->getCombatPerformance(/* parameters */);
	}

	/**
	 * Tests Social_Model->getLastCombatTime()
	 */
	public function testGetLastCombatTime()
	{
		// TODO Auto-generated Social_ModelTest->testGetLastCombatTime()
		$this->markTestIncomplete( "getLastCombatTime test not implemented" );
		$this->Social_Model->getLastCombatTime(/* parameters */);
	}

	/**
	 * Tests Social_Model->isProtected()
	 */
	public function testIsProtected()
	{
		// TODO Auto-generated Social_ModelTest->testIsProtected()
		$this->markTestIncomplete( "isProtected test not implemented" );
		$this->Social_Model->isProtected(/* parameters */);
	}

	/**
	 * Tests Social_Model->getData()
	 */
	public function testGetData()
	{
		// TODO Auto-generated Social_ModelTest->testGetData()
		$this->markTestIncomplete( "getData test not implemented" );
		$this->Social_Model->getData(/* parameters */);
	}
}

