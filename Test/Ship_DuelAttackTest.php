<?php

require_once 'PHPUnit\Framework\TestCase.php';
/**
 * Ship_DuelAttack test case.
 */
class Ship_DuelAttackTest extends PHPUnit_Framework_TestCase
{

	/**
	 * @var Ship_DuelAttack
	 */
	private $Ship_DuelAttack;
	
	const FAKE_USER_ID = 82392381;

	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp()
	{
		parent::setUp();
		// TODO Auto-generated Ship_DuelAttackTest::setUp()
		$this->Ship_DuelAttack = new Ship_DuelAttack( self::FAKE_USER_ID );
	}

	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown()
	{
		// TODO Auto-generated Ship_DuelAttackTest::tearDown()
		$this->Ship_DuelAttack = null;
		parent::tearDown();
	}

	/**
	 * Constructs the test case.
	 */
	public function __construct()
	{
		// TODO Auto-generated constructor
	}

	/**
	 * Tests Ship_DuelAttack->__construct()
	 */
	public function test__construct()
	{
		// TODO Auto-generated Ship_DuelAttackTest->test__construct()
		$this->markTestIncomplete( "__construct test not implemented" );
		$this->Ship_DuelAttack->__construct(/* parameters */);
	}

	/**
	 * Tests Ship_DuelAttack::getBattleGroup()
	 */
	public function testGetBattleGroup()
	{
		// TODO Auto-generated Ship_DuelAttackTest::testGetBattleGroup()
		$this->markTestIncomplete( "getBattleGroup test not implemented" );
		Ship_DuelAttack::getBattleGroup(/* parameters */);
	}

	/**
	 * Tests Ship_DuelAttack->fightBattleMonster()
	 */
	public function testFightBattleMonster()
	{
		// TODO Auto-generated Ship_DuelAttackTest->testFightBattleMonster()
		$this->markTestIncomplete( "fightBattleMonster test not implemented" );
		$this->Ship_DuelAttack->fightBattleMonster(/* parameters */);
	}

	/**
	 * Tests Ship_DuelAttack->plunder()
	 */
	public function testPlunder()
	{
		// TODO Auto-generated Ship_DuelAttackTest->testPlunder()
		$this->markTestIncomplete( "plunder test not implemented" );
		$this->Ship_DuelAttack->plunder(/* parameters */);
	}

	/**
	 * Tests Ship_DuelAttack->attackFriend()
	 */
	public function testAttackFriend()
	{
		// TODO Auto-generated Ship_DuelAttackTest->testAttackFriend()
		$this->markTestIncomplete( "attackFriend test not implemented" );
		$this->Ship_DuelAttack->attackFriend(/* parameters */);
	}

	/**
	 * Tests Ship_DuelAttack->arenaFight()
	 */
	public function testArenaFight()
	{
		// TODO Auto-generated Ship_DuelAttackTest->testArenaFight()
		$this->markTestIncomplete( "arenaFight test not implemented" );
		$this->Ship_DuelAttack->arenaFight(/* parameters */);
	}

	/**
	 * Tests Ship_DuelAttack::simulateFight()
	 */
	public function testSimulateFight()
	{
		// TODO Auto-generated Ship_DuelAttackTest::testSimulateFight()
		$this->markTestIncomplete( "simulateFight test not implemented" );
		Ship_DuelAttack::simulateFight(/* parameters */);
	}

	/**
	 * Tests Ship_DuelAttack::caculateDropGoldWhenWinArena()
	 */
	public function testCaculateDropGoldWhenWinArena()
	{
		// TODO Auto-generated Ship_DuelAttackTest::testCaculateDropGoldWhenWinArena()
		$this->markTestIncomplete( "caculateDropGoldWhenWinArena test not implemented" );
		Ship_DuelAttack::caculateDropGoldWhenWinArena(/* parameters */);
	}
}

