<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 场景初始化控制器
 * @author	Liuchangbing
 */
class SceneController extends BaseController
{
	/**
	 * 场景初始化
	 * @author	liuchangbing
	 * @return	array(
	 * 				userInfo
	 * 			)
	 */
	public function init()
	{
		Helper_RunLog::getInstance()->addLog( "Scene.init" , "start....." );
		$isNewUser = false;
		try
		{
			/**
			 * 用户注册信息
			 * @var unknown_type
			 */
			$userProfile = User_Profile::getInstance( $this->userId );
			
			$isNewUser = $userProfile->isNewUser();
			 
			 //用户游戏信息
			$userInfo = User_Info::getInstance( $this->userId );	
			
			//武将/将领模块
			$cardModel = Card_Model::getInstance( $this->userId );

			//每次登录初始化
			$perLoginInfo = $userInfo->initPerLogin();
			
			//每天首次登录初始化
			$loginRecord = $userInfo->initFirstLogin();
			
		}
		catch( User_Exception $ue )
		{
			if( $ue->getCode() != User_Exception::STATUS_USER_NOT_EXIST )
			{
				throw $ue;
			}
		}
		catch ( Exception $e )
		{
			throw $e;
		}
		
		$loginLog = new ErrorLog( "login" );
		$loginLog->addLog( json_encode( $_REQUEST )."reward:"  );
		
		Helper_RunLog::getInstance()->addLog( "Scene.init" , "addHelperUser...." );
		
		//将自己加入到义军名单，供别人搜
		Battle_Model::addHelperUser( $this->userId );
		
		//$result['inviteGold'] = 10000;
		//记录登录时间
		$userInfo = array( 'loginTime' => $_SERVER['REQUEST_TIME'] );
		User_Profile::getInstance( $this->userId )->setUserInfo( $userInfo );
		
		$returnData = array(
			'user_info' => MakeCommand_User::userInfo( $this->userId ),
			'user_cards' => MakeCommand_Card::cards( $this->userId ),
			'user_deck' => MakeCommand_Card::cardsTeam( $this->userId ),
			'user_deck_multi' => MakeCommand_Card::cardsTeamMulti( $this->userId ),
			'user_dungeon_info' => array(
			     'normal_current' => Battle_Model::getInstance( $this->userId )->getNormalCurrent(),
			     'special' => Battle_Model::getInstance( $this->userId )->getSpecial(), 
			     'weekly' => Battle_Model::getInstance( $this->userId )->getWeekly(),
			),
			'activity_consume' => MakeCommand_Activity::consume( $this->userId ),
			'invite/d_award' => "0"  ,
			'invite_code_reward' => array(
				'times' => $perLoginInfo['rewardTimes'] ?  (int)$perLoginInfo['rewardTimes'] : 0 , 
				'info' => $perLoginInfo['inviteReward'] ? $perLoginInfo['inviteReward'] : array() , 
			),
			'help_reward' => $loginRecord['helpeReward'] ?  $loginRecord['helpeReward']  : array(),
			'notice' => "http://sg.ecngame.com/notice.html",
		);
		
		/**
		 * EDAC统计：登录统计
		 */
		Stats_Edac::userLogin( $this->userId );
		//Stats_Model::addUserActionLog( $this->userId , "Scene.init" );
		
		return $returnData;
	}
}

?>
