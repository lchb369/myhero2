<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 获取配置信息
 * @since	2013.1.14
 * @author	liuchangbing
 */
class ConfigController extends BaseController
{
	/**
	 * 获取服务器列表
	 */
	public function server()
	{
		$full = trim($_GET['full']);
		$pf = trim( $_GET['pf'] );
		$serverList = array();
		$sid = 0;
		if( $pf )
		{
			$config = Common::getConfig( 'ServerConfig' );
			foreach ( $config as $conf )
			{
				if(empty($conf['enable'])) continue;
				
				if( $conf['pf'] == $pf )
				{
					$serverList[$conf['sid']] = $conf;
				}
			}
		}
		if(empty($full))
		{
			echo json_encode( $serverList );
			exit;
		}
		else
		{
			return array('list' => $serverList);
		}
	}	
	/**
	 * 根据配置名获取配置信息
	 */
	public function get()
	{
		$weeklyConfig = Common::getConfig( "weekly" );
		foreach ( $weeklyConfig as $key => $weekly )
		{
			$todayWeekDay = date( "N" , $_SERVER['REQUEST_TIME'] );	
			$todayStart = date( "Y-m-d ", $_SERVER['REQUEST_TIME']).( intval( $weekly['start_time'] ) == 0 ? " 00:00:00" : $weekly['start_time'] );
			$todayEnd = date( "Y-m-d ", $_SERVER['REQUEST_TIME']).( intval( $weekly['end_time'] ) == 0 ? " 24:00:00" : $weekly['end_time'] );

			if( $todayWeekDay ==  $weekly['week_day'] && $_SERVER['REQUEST_TIME'] >= strtotime( $todayStart ) && $_SERVER['REQUEST_TIME'] < strtotime( $todayEnd ) )
			{
				$weeklyConfig[$key]['start_time'] =  $todayStart ;
				$weeklyConfig[$key]['end_time'] = $todayEnd;
			}
			else
			{
				unset( $weeklyConfig[$key] );
			}
		}
		
		$specialConfig = Common::getConfig( "special" );
		$specialConfig = $this->filterBattleInTime( $specialConfig );

		$commonConfig = $this->getConfig( "common" , $_REQUEST['common'] );
		
		$effectConfig = $this->getConfig( "effect" , $_REQUEST['effect'] );
		$cardLevelConfig = $this->getConfig( "cardLevel" , $_REQUEST['card_level'] );
		$skillLevelConfig = $this->getConfig( "skillLevel" , $_REQUEST['skill_level'] );
		$upgradeConfig = $this->getConfig( "upgradeSkill" , $_REQUEST['upgrade_skill'] );
		
		$normalConfig = $this->getConfig( "normal" , $_REQUEST['normal'] );
		$cardConfig = $this->getConfig( "card" , $_REQUEST['card'] );
		$monsterConfig = $this->getConfig( "monster" , $_REQUEST['monster'] );
		
		$returnData = array(
			'dungeon' => array(
				'push_list' => array(),
				'weekly' => $weeklyConfig,
				'effect' => $effectConfig['conf'],
				'special' => $specialConfig,
				'normal' => $normalConfig['conf'],
			),
			'skill_level' => $skillLevelConfig['conf'],
			'upgrade_skill' => $upgradeConfig['conf'],
			'monster' => $monsterConfig['conf'],
			'server_now' => $_SERVER['REQUEST_TIME'],
			'card_level' => $cardLevelConfig['conf'],
			'rc' => 0,
			'card' => $cardConfig['conf'],
			'common' => $commonConfig['conf'],
			'ver' => array(
				"card" => $cardConfig['ver'] ,
				"monster" => $monsterConfig['ver'] ,
				"normal" => $normalConfig['ver'] ,
				"common" => $commonConfig['ver'],
// 				"effect" => $effectConfig['ver'],
				"card_level" => $cardLevelConfig['ver'],
				"skill_level" => $skillLevelConfig['ver'],
				"upgrade_skill" => $upgradeConfig['ver'],
			),
		);
		
		//Stats_Model::addUserActionLog( $this->userId , "Config.get" );
		
		return $returnData;
	}

	
	public function getMonster()
	{
		$monsterConfig = $this->getConfig( "monster" , $_REQUEST['monster'] );
		
		$returnData = array(
			'monster' => $monsterConfig['conf'],
			'ver' => array(
				"monster" => $monsterConfig['ver'] ,
			),
		);
		
		return $returnData;
	}
	
	public function language()
	{
		error_reporting( E_ALL ^ E_NOTICE );
		ini_set( 'display_errors' , 'On' );
	
		header('Cache-Control: no-cache, must-revalidate');
		header("Content-Type: text/plain; charset=utf-8");
		$fileName = $this->getLangFile( $this->get['lang'] );
		$lan = file_get_contents( $fileName );
		echo $lan;
	}
	
	/**
	 * 所有相关配置文件的版本号
	 * Enter description here ...
	 */
	public function langVer()
	{
		$lang = "cn";
		
		if( isset( $this->get['lang'] ) )
		{
			$lang = $this->get['lang'];
		}
		
		$ver = $this->getModifyTime( $this->getLangFile( $lang ) );
		//为了补小五埋的坑 1,2位后面加个点
		if( !empty( $ver ) )
		{
			$len = 3;
			$pre = substr( strval( $ver ) , 0 , $len );
			$temp = "";
			for( $i = 0 ; $i < strlen( $pre ) ; $i++ ) $temp .= ( !empty( $temp ) ? "." : "" ).$pre[$i];
			$pre = $temp;
			$ver = $pre.substr( strval( $ver ) , $len );
		}
		else
		{
			$ver = "1.0.0";
		}
		
		$returnData = array(
			"lang" => $lang,
			"ver" => $ver,
		);
		
		return $returnData;
		
	}
	
	private function filterBattleInTime( $battles )
	{
		$ret = array();
		if( !empty( $battles ) )
		{
			foreach( $battles as $id => $battle )
			{
				if(
					strtotime( date( 'Y-m-d' , $_SERVER['REQUEST_TIME'] ) ) >= strtotime( date( 'Y-m-d' , strtotime( $battle['start_time'] ) ) ) 
					&& strtotime( date( 'Y-m-d' , $_SERVER['REQUEST_TIME'] ) ) <= strtotime( date( 'Y-m-d' , strtotime( $battle['end_time'] ) ) )
				)
				{
					$ret[$id] = $battle;
				}
			}
		}
		//special不能为空
		if( empty( $ret ) )
		{
			foreach( $battles as $id => $battle )
			{
				$ret[$id] = $battle;
				break;
			}
		}
		return $ret;			
	}
	
	private function getLangFile( $lang )
	{
		$pathPre = CONFIG_DIR.'/GameConfig/';
		$fileName = "enLanguageBag1_tw.txt";
		if( !empty( $lang ) )
		{
			$_fileName = "enLanguageBag1_".$lang.".txt"; 
			if( file_exists($pathPre.$_fileName) ) $fileName = $_fileName;
		}
		return $pathPre.$fileName;
	}
	
	private function getModifyTime( $filename )
	{
		$ret = 0;
		if( file_exists( $filename ) )
		{
			$ret = filemtime( $filename );
		}
		return $ret;
	}
	
	private function getConfig( $confName , $clientVer )
	{
		$ret = array( "conf" => array() , "ver" => "0" );
		
		$clientVer = !empty( $clientVer ) ? intval( $clientVer ) : 0;
		$serVer = $this->getModifyTime( CONFIG_DIR.Common::getGameConfPath()."/OperationData/".$confName.".php" );
		if( $clientVer == 0 || $clientVer < $serVer )
		{
			$ret = array(
				"conf" => Common::getConfig( $confName ),
				"ver" => strval( $serVer ),
			);
		}
		
		return $ret;
	}
	
}