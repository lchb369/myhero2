<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * @since	2013.1.14
 */
class FriendController extends BaseController
{
	/**
	 * 获取好友请求列表
	 */
	public function getRequest()
	{
		$returnData = array(
			'friend_requests' => MakeCommand_Friend::friendReqList( $this->userId ),
			'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}
	
	/**
	 * 接受邀请
	 */
	public function acceptReq()
	{
		$fId = (int)$this->get["fId"];
		if( $fId <= 0 )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		//是否存在这个用户数据
		if( User_Model::exist( $fId ) == false )
		{
			return array(  'errorCode' => User_Exception::STATUS_USER_NOT_EXIST );
		}
		
		$friendModel = Friend_Model::getInstance( $this->userId );
		//是否已经是我好友
		if( $friendModel->areFriend( $fId ) == true )
		{
				$friendModel->reject( $fId );
				ObjectStorage::save();
				return array(  'errorCode' => Friend_Exception::STATUS_ALREADY_FRIEND  );
		}
		
		
		//接受好友的请求
		$friendModel->accept( $fId );
		//$friendReqs = Friend_Model::getInstance( $this->userId )->getFriendReqs();
		$returnData = array(
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
		
	}
	
	public function acceptAllReq()
	{
		if(empty($this->userId)) return array(  'errorCode' => GameException::PARAM_ERROR );
		
		$retAcceptReq = Friend_Model::getInstance($this->userId)->acceptAll();
		
		$returnData = array(
			'success' => $retAcceptReq['success'], 
			'fail' => $retAcceptReq['fail'],
		);
		return $returnData;
	}
	
	/**
	 * 拒绝请求
	 */
	public function rejectReq()
	{
		$fId = (int)$this->get["fId"];
		if( $fId <= 0 )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		//是否存在这个用户数据
		if( User_Model::exist( $fId ) == false )
		{
			return array(  'errorCode' => User_Exception::STATUS_USER_NOT_EXIST );
		}
		
		$friendModel = Friend_Model::getInstance( $this->userId );
		//是否已经是我好友
		if( $friendModel->areFriend( $fId ) == true )
		{
			//return array(  'errorCode' => Friend_Exception::STATUS_ALREADY_FRIEND );
		}
		
		
		//接受好友的请求
		$friendModel->reject( $fId );
		
		
		//$friendReqs = Friend_Model::getInstance( $this->userId )->getFriendReqs();
		
		$returnData = array(
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
		
	}
	
	/**
	 * 删除好友
	 */
	public function remove()
	{
		$fId = (int)$this->get["fId"];
		if( $fId <= 0 )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		//是否存在这个用户数据
		if( User_Model::exist( $fId ) == false )
		{
			return array(  'errorCode' => User_Exception::STATUS_USER_NOT_EXIST );
		}
		
		$friendModel = Friend_Model::getInstance( $this->userId );
		//是否已经是我好友
		if( $friendModel->areFriend( $fId ) == false )
		{
			return array(  'errorCode' => Friend_Exception::STATUS_NOT_FRIEND );
		}
		
		//删除好友
		$friendModel->removeFriend( $fId );
		//从别人的好友里把我删掉
		Friend_Model::getInstance($fId)->removeFriend($this->userId);
		
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		
		$friendReqs = Friend_Model::getInstance( $this->userId )->getFriendReqs();
		$friendList = Friend_Model::getInstance( $this->userId )->getFriends();
		$returnData = array(
			'user_info' => MakeCommand_User::userInfo( $this->userId ),
			'friends' => MakeCommand_Friend::friendList( $this->userId ),
			'max_num' => $userInfo['maxFriendNum'],
			'now_num' => count( $friendList ),
		);
		return $returnData;
	}

	/**
	 * 验证邀请码是否合法
	 * @return multitype:number
	 */
	public function checkInviteCode()
	{
		$code = strval( $this->get['code'] );
		if( trim( $code ) )
		{
			$status = User_Info::checkInviteCode( $code );
			$status = $status ? 1 : 0;
		}
		else
		{
			$status = 0;
		}
		
		$returnData = array(
		     'status' => (int)$status,
		);
	
		return  $returnData ;
	}

	/**
	 * 邀请好友
	 * 如果成功，把我加入到好友的请求列表
	 */
	public function invite()
	{
		$tId = trim(addslashes($this->get['tId']));
		$fId = (int)$this->get["fId"];
		if($fId <= 0 && !$tId) return array(  'errorCode' => GameException::PARAM_ERROR );
		
		//是否存在这个用户数据
		if($tId)
		{
			$fId = User_Model::getIdByThird($tId);
			if(!$fId) return array(  'errorCode' => User_Exception::STATUS_USER_NOT_EXIST );
		}
		else
		{
			if(!User_Model::exist($fId)) return array(  'errorCode' => User_Exception::STATUS_USER_NOT_EXIST );
		}
		
		if( $fId == $this->userId )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		//是否已经是我好友
		if( Friend_Model::getInstance( $this->userId )->areFriend( $fId ) == true )
		{
			//TODO::如果已经是好友，客户端没判断，会出错
			//return array(  'errorCode' => Friend_Exception::STATUS_ALREADY_FRIEND );
			$returnData = array(
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
			);
			return $returnData;
		}
		
		Friend_Model::getInstance( $this->userId )->invite( $fId );
		//$friendReqs = Friend_Model::getInstance( $fId )->getFriendReqs();
		
		$returnData = array(
			'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	
	}
	
	/**
	 * 搜索好友
	 */
	public function search()
	{
		$fId = (int)$this->get["fId"];
		
		if( empty( $fId ) )
		{
			return array(  'errorCode' => User_Exception::STATUS_FRIEND_SEARCH_EMPTY );
		}
		
		if( $fId <= 0 )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		if( $fId == $this->userId )
		{
			return array(  'errorCode' => User_Exception::STATUS_FRIEND_SEARCH_MYSELF );
		}
		
		//是否存在这个用户数据
		if( User_Model::exist( $fId ) == false )
		{
			return array(  'errorCode' => User_Exception::STATUS_USER_NOT_EXIST );
		}
		
		//是否已经是我好友
		$friendModel = Friend_Model::getInstance( $this->userId );
		if( $friendModel->areFriend( $fId ) == true )
		{
			//return array(  'errorCode' => Friend_Exception::STATUS_ALREADY_FRIEND );
		}
		
		
		$returnData = array(
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
				'friend_info' => MakeCommand_Friend::userInfo( $this->userId , $fId ),
		);
		return $returnData;
	}
	
	
	/**
	 * 获取好友列表
	 */
	public function getList()
	{
		$friendList = MakeCommand_Friend::friendList( $this->userId );
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		
		//好友赠送信息
		$friendSendGiftData = Friend_Model::sendGiftHistory($this->userId);
		
		$returnData = array(
			'friends' => $friendList,
			'send_gift' => $friendSendGiftData,
			'max_num' => (int)$userInfo['maxFriendNum'],
			'now_num' => (int)count( $friendList ),
		);
		return $returnData;
	}
	
	/**
	 * 获得第三方用户好友邀请信息
	 * @return multitype:NULL multitype:multitype:number   mixed
	 */
	public function inviteThirdHistory()
	{
		$returnData = Friend_Model::inviteThirdHistory($this->userId);
		return $returnData;
	}
	
	/**
	 * 邀请第三方好友
	 * @return Ambigous <multitype:unknown , multitype:number >
	 */
	public function inviteThird()
	{
		$returnData = array(  'errorCode' => GameException::PARAM_ERROR );
		do{
			$thirdId = trim($_REQUEST['tId']);
			if(empty($thirdId)) break;
			//不能邀请自己
			$userProfile = User_Profile::getInstance($this->userId)->getData();
			if($thirdId == $userProfile['thirdId']) break;
			
			$returnData = Friend_Model::inviteThird($this->userId, $thirdId);
		}while(0);
		
		return $returnData;
	}
	
	/**
	 * 获得第三方好友邀请奖励
	 * @return Ambigous <multitype:unknown , multitype:number >
	 */
	public function getInviteThirdBonus()
	{
		$returnData = array(  'errorCode' => GameException::PARAM_ERROR );
		do{
			$inviteNum = intval($_REQUEST['inviteNum']);
			if(empty($inviteNum)) break;
	
			$returnData = Friend_Model::getInviteThirdBonus($this->userId, $inviteNum);
		}while(0);
	
		return $returnData;
	}
	
	public function sendGift()
	{
		$returnData = array(  'errorCode' => GameException::PARAM_ERROR );
		do{
			$toId = intval($_REQUEST['toId']);
			if(empty($toId) 
			|| $toId == $this->userId 
			|| !User_Model::exist($toId) 
			|| !Friend_Model::getInstance($this->userId)->areFriend($toId)) break;
				
			$returnData = Friend_Model::sendGift($this->userId, $toId, Friend_Model::$giftBonus);
		}while(0);
		
		return $returnData;
	}
	
	public function getGift()
	{
		$returnData = array(  'errorCode' => GameException::PARAM_ERROR );
		do{
			$fromId = intval($_REQUEST['fromId']);
			if(empty($fromId) 
			|| $fromId == $this->userId
			|| !User_Model::exist($fromId) 
			|| !Friend_Model::getInstance($this->userId)->areFriend($fromId)) break;
	
			$returnData = Friend_Model::getGift($this->userId, $fromId);
		}while(0);
	
		return $returnData;
	}
	
	public function getGiftHistory()
	{
		$giftHis = Friend_Model::getGiftHistory($this->userId);
		$canGet = false;
		foreach($giftHis as &$his)
		{
			if($his['time'] == 0)
			{
				$canGet = true;
				break;
			}
		}
		$returnData = array(
			"list" => $giftHis,
			"canGet" => $canGet, 
		);
		return $returnData;
	}
	
	public function getAppFriend()
	{
		$ret = array('errorCode' => GameException::PARAM_ERROR);
		do{
			$fIds = trim($_REQUEST['fids']);
			if(empty($fIds)) break;
			$fIds = explode(",", $fIds);
			$ret = array(
				"list" => Friend_Model::getAppFriend($this->userId, $fIds),
			);
			
		}while(0);
		
		return $ret;
	}
	
}
