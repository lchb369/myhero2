<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * @since	2013.1.14
 * PVP战斗模块
 */
class ArenaController extends BaseController
{

// 	public function __construct()
// 	{
// 		parent::__construct();
// 		$_SERVER['REQUEST_TIME'] = Helper_Date::getWeekEndTime() + 3600 * 10 + 1;
// 	} 
	
	private function init()
	{
		//判断时间是否在维护时间中
		if( !!Arena_Normal::getInstance( $this->userId )->isInUpdating() )
		{
			throw new Battle_Exception( Arena_Exception::STATUS_IN_UPDATING );
		}
		Arena_Normal::resetInfo( $this->userId );
	}
	
	/**
	 * 竞技场挑战
	 * @return multitype:number |multitype:unknown
	 */
	public function attack()
	{
		$this->init();
		$type = $this->get['type'];
		$enemyId = $this->get['enemyId'];
		if( $type != "ai" && $type != "user" )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		//判断挑战次数
		$arenaData = Data_Arena_Normal::getNormalInfo( $this->userId );
		if(  $arenaData['normalTimes'] < 1 )
		{
			return array(  'errorCode' => Arena_Exception::STATUS_NOT_ENOUGH_CHALLENGE_TIMES );
		}
		
		$result = Arena_Normal::getInstance( $this->userId )->attack( $type , $enemyId );
		$reward['noramlScore'] = 0;
		$reward['benefit'] = 0;
		//如果打胜发奖励
		$statExt = '';
		if( $result['challengerWin'] == 1 )
		{
			$setData['normalScore'] = $arenaData['normalScore'] + 10;
			$setData['benefit'] = $arenaData['benefit'] + 10;
			
			$reward['noramlScore'] = 10;
			$reward['benefit'] = 10;
			
			$statExt = '积分+10，福利+10';
			//打赢刷新对手
			Arena_Normal::getInstance( $this->userId )->delEnemyHistory();
		}
		
		/**
		 * 添加竞技场流水
		 */
// 		$cScore = $arenaData['normalScore'];
// 		$enemyData = Data_Arena_Normal::getNormalInfo( $enemyId );
// 		$bScore = $enemyData['normalScore'];
// 		Stats_Model::addUserArenaLog( $this->userId , 'normal' , $this->userId , $enemyId , $cScore, $bScore, $reward );
		
		/**
		 * EDAC统计
		 */
		Stats_Edac::playMethod( $this->userId , "竞技场挑战" );
		Stats_Edac::customAction( $this->userId , "竞技场挑战" , $enemyId , 1 , $statExt );
		
		
		//减少挑战次数，奖励入库
		$setData['normalTimes'] = $arenaData['normalTimes'] - 1;
		$setData['normalDate'] = $_SERVER['REQUEST_TIME'];
		//等级也一起修改,加入排行条件
		$setData['level'] = User_Info::getInstance( $this->userId )->getLevel();

		Data_Arena_Normal::setNormalInfo( $this->userId , $setData );
		
		$result['reward'] = $reward;
		$result['arenaInfo'] = Data_Arena_Normal::getNormalInfo( $this->userId );
		
		$returnData = array(
				'result' => $result,
		);
		return $returnData;
		
	}
	
	/**
	 * 获取普通JJC对手及我的竞技场信息
	 */
	public function getNormalEnemy()
	{
		$this->init();
		if( !Arena_Normal::getInstance( $this->userId )->isInLevel() )
		{
			return array(  'errorCode' => Arena_Exception::STATUS_NOT_ENOUGH_CHALLENGE_LEVEL );
		}
		
		$enemyList = Arena_Normal::getInstance( $this->userId )->getEnemy();
		$myInfo = Arena_Normal::getInstance( $this->userId )->getNormalRank(false);
		
		$endTime = Helper_Date::getWeekEndTime() + 10 * 3600 - $_SERVER['REQUEST_TIME'];
		$curMon = Helper_Date::getWeekStartTime();
		if( $_SERVER['REQUEST_TIME'] >= $curMon && $_SERVER['REQUEST_TIME'] < $curMon + 10 * 3600)
		{
			$endTime = $curMon + 10 * 3600 - $_SERVER['REQUEST_TIME'];
		}
		
		$returnData = array(
				'enemy' => $enemyList,
				'myInfo' => $myInfo,
				'endTime' => $endTime,
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}

	/**
	 * 获取普通JJC排行信息及我的竞技场信息
	 * @return array
	 */
	public function getNormalRank()
	{
		$returnData = Arena_Normal::getInstance( $this->userId )->getNormalRank();
		return $returnData;
	}
	
	/**
	 * 获取福利兑换信息
	 * @return array
	 * */
	public function getBenefitExchg()
	{
		$returnData = Arena_Normal::getInstance( $this->userId )->getBenefitExchg();
		return $returnData;
	}
	
	/**
	 * 福利兑换
	 * @return array
	 * */
	public function benefitExchg()
	{
		$this->init();
		$type = 1;
		$id = intval($this->get['exchgId']);
		
		if( empty($id) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		$returnData = Arena_Normal::getInstance( $this->userId )->benefitExchg($type,$id);
		return $returnData;
	}
	
	/**
	 * 获取排行奖励信息
	 * @return array
	 * */
	public function rankBonusInfo()
	{
		$this->init();
		//1普通, 2精英
		$type = intval($this->get['rankType']);
		if( empty( $type ) ) $type = 1;
		
		if( $type != 1 && $type != 2)
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		$returnData = Arena_Normal::getInstance( $this->userId )->rankBonusInfo( $type );
		return $returnData;
	}
	
	/**
	 * 领取排行奖励
	 * @return array
	 * */
	public function getRankBonus()
	{
		$this->init();
		
		$type = 2;
		$id = intval($this->get['bonusId']);
		
		if( empty($id) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		$returnData = Arena_Normal::getInstance( $this->userId )->getRankBonus( $type , $id );
		return $returnData;	
	}
	
	/**
	 * 刷新挑战次数
	 * @return array
	 * */
	public function refreshNormalTimes()
	{
		$this->init();
		$commonConfig = Common::getConfig('common');

		$refreshCoin = !empty($commonConfig['arenaRefreshTimesCoin']) ? $commonConfig['arenaRefreshTimesCoin'] : 6;
		$fullNormalTimes = 10;
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		if( $userInfo['coin'] < $refreshCoin )
		{
			return array( 'errorCode' => User_Exception::STATUS_NOT_ENOUGH_COIN );
		}
		else
		{
			$status = User_Info::getInstance( $this->userId )->changeCoin( -1 * $refreshCoin , 'arenaRefreshTimes' );
			//重置次数
			Data_Arena_Normal::setNormalInfo( $this->userId , array(
				"normalTimes" => $fullNormalTimes
			) );
		}
		return array(
			'normalTimes' => $fullNormalTimes ,
			'user_info' => MakeCommand_User::userInfo( $this->userId ) ,
		);
	} 
	
	/**
	 * 刷新挑战对手
	 * @return array
	 * */
	public function refreshNormalEnemy(  )
	{
		$this->init();
		$commonConfig = Common::getConfig('common');
		
		$refreshCoin = !empty($commonConfig['arenaRefreshEnemyCoin']) ? $commonConfig['arenaRefreshEnemyCoin'] : 1;
		
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		if( $userInfo['coin'] < $refreshCoin )
		{
			return array( 'errorCode' => User_Exception::STATUS_NOT_ENOUGH_COIN );
		}
		else
		{
			$status = User_Info::getInstance( $this->userId )->changeCoin( -1 * $refreshCoin , 'arenaRefreshEnemy' );
			//刷新挑战对手
			Arena_Normal::getInstance( $this->userId )->delEnemyHistory();
			$ret = $this->getNormalEnemy();
		}
		return $ret;
	}
	
}
