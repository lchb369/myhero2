<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * @since	2013.1.14
 * 战场模块
 */
class ActivityController extends BaseController
{
	
	public function consumeReward()
	{
		$actId = (int)$this->get['actId'];
		if( $actId <= 0 ) return array(  'errorCode' => GameException::PARAM_ERROR );
	
		Activity_Model::getInstance( $this->userId )->consumeReward( $actId );
		
		return array();
	}
	
	/**
	 * 领取首冲奖励
	 * @return multitype:unknown multitype:number string unknown Ambigous <> multitype:number Ambigous <>
	 */
	public function getFirstChargeReward()
	{
// 		$actId = (int)$this->get['actId'];
// 		if( $actId <= 0 ) return array(  'errorCode' => GameException::PARAM_ERROR );
		
		$cardInfo = Activity_Model::getInstance( $this->userId )->getFirstChargeReward();
		
		return array();
	}
	
	public function getActivity()
	{
		$type = intval( $this->get['type'] );
		if( empty( $type )) $type = 1;
		$returnData = array(
			'activity_consume' => MakeCommand_Activity::consume( $this->userId , $type ),
			//'activitys' =>  Activity_Model::getInstance( $this->userId )->getData(),
		);
		return $returnData;
	}
	
	public function useActiveCode()
	{
		$returnData = array(  'errorCode' => GameException::PARAM_ERROR );
		
		do{
			$code = trim( $this->get['code'] );
			if(!$code) break;
			if(empty($this->userId)) break;
			
			$reward = Activity_Model::getInstance( $this->userId )->useActiveCode( $code );
			if($reward == false) throw new User_Exception( User_Exception::STATUS_ACTIVE_CODE_INVALID );
			
			$returnData = array(
				'reward' => $reward,
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
			);
		}while(0);
		
		return $returnData;
	}
	
	
	/**
	 * 登录奖励
	 */
	public function getLoginReward()
	{
		$type = (int)$this->get['type'];
	
		if( $type != Activity_Model::ACTIVE_TYPE_LOGIN_DAYS && $type != Activity_Model::ACTIVE_TYPE_TOTAL_LOGINS )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
	
		$reward = array(
				'prizeCoin' => 0,
				'prizeGold' => 0,
				'prizePoint' => 0,
		);
	
		//是否已经领过
		$status = Activity_Model::getInstance( $this->userId )->rewardLoginActivity( $type );
		if( $status == false )
		{
			$returnData = array(
					'user_info' => MakeCommand_User::userInfo( $this->userId ),
					'activity' => Activity_Model::getInstance( $this->userId )->getData(),
					'reward' => $reward,
			);
			return $returnData;
		}
	
	
	
		//连续登录奖励
		$userProfile = User_Profile::getInstance( $this->userId )->getData();
		if( $type == 1 )
		{
			//连续7天登录
			$days = $userProfile['loginDays'];
			$config = Common::getConfig( "loginDays" );
				
			$days = $days == 0 ? 1 : $days;
			$days = ( $days > 7 ) ? 7 : $days;
			$reward =  $config[$days];
		}
		else
		{
			//累计登录
			$days = $userProfile['totalLogins'];
			$config = Common::getConfig( "consecutiveLogin" );
	
			foreach ( $config as $eachDays => $eachConf )
			{
				//累计到那一天才给奖励
				if( $days == $eachDays )
				{
					$reward = $eachConf;
					break;
				}
			}
		}
	
		if( $reward['prizeCoin'] > 0 )
		{
			User_Info::getInstance( $this->userId )->changeCoin( $reward['prizeCoin'] , '登录奖励');
		}
			
			
		if( $reward['prizeGold'] > 0 )
		{
			User_Info::getInstance( $this->userId )->changeGold( $reward['prizeGold'] , '登录奖励'  );
		}
	
		if( $reward['prizePoint'] > 0 )
		{
			User_Info::getInstance( $this->userId )->changeGachaPoint( $reward['prizePoint']  );
		}
	
		$returnData = array(
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
				'activity' => Activity_Model::getInstance( $this->userId )->getData(),
				'reward' => $reward,
		);
		return $returnData;
	}
	
	public function getLoginNotice()
	{
		$showType = trim($this->get['showType']);
		
		$notice = Activity_LoginNotice::getInstance( $this->userId )->getLoginNotice($showType);
		
		return array( "web" => $notice ); 
	}
	
	/**
	 * 手动恢复体力
	 * 活动名称：沙场点兵（在规定时间段内可以回复十点体力值）
		活动时间段为12点-13点，18点-19点，每天零点活动重置
	 */
	public function recoveryStamina()
	{
		$ret = Activity_Model::getInstance($this->userId)->manualRecoveryStamina();
		if($ret)
		{
			return array('user_info' => MakeCommand_User::userInfo( $this->userId ));	
		}
		else
		{
			return array('errorCode' => User_Exception::STATUS_RECOVERY_STAMINA_FAIL);	
		}
	}
	
	public function getPointCardData()
	{
		$pointCardConfig = Common::getConfig("pointCard");
		$retConfig = array();
		foreach($pointCardConfig as $conf)
		{
			if(Helper_Date::isInTime($conf['start_time'], $conf['end_time']))
			{
				$conf['start_time'] = strtotime($conf['start_time']);
				$conf['end_time'] = strtotime($conf['end_time']);
				$retConfig[] = $conf;
			}
		}
		$pointCardData = Activity_PointCard::getInstance( $this->userId )->getPointCardData();
		$returnData = array(
			'activitys' =>  $pointCardData,
			'config' => $retConfig,
		);
		return $returnData;
	}
	
	/**
	 * 点将台
	 */
	public function pointCard()
	{
		$actId = intval($this->get['actId']);
		$actType = intval($this->get['actType']);
		if( $actId <= 0 || $actType <= 0) return array(  'errorCode' => GameException::PARAM_ERROR );
		
		$ret = Activity_PointCard::getInstance( $this->userId )->pointCard($actId, $actType);
		return $ret;
	}
	
}
