<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * @since	2013.1.14
 * 战场模块
 */
class BattleController extends BaseController
{
	
	/**
	 * 获取援军
	 */
	public function getHelper()
	{
		//获取义军
		$otherHelpers = Battle_Model::getInstance( $this->userId )->getOtherHelpers();
		
		//获取友军
		$friendHelpers = Battle_Model::getInstance( $this->userId )->getFriendHelpers();
		
		Battle_Model::getInstance( $this->userId )->setHelperHistory( $otherHelpers , $friendHelpers );
		
		$returnData = array(
			'friend' => MakeCommand_Friend::HelpersInfo( $friendHelpers , true ),
			'other' => MakeCommand_Friend::HelpersInfo( $otherHelpers , false ),
			'gtMaxFriend' => Friend_Model::checkFriendlimit( $this->userId , true ),
		);
		return $returnData;
	}
	
	
	/**
	 * 获取房间里的怪物列表
	 */
	public function getMonster()
	{
		$floorId = (int)$this->get["floor"];
		$roomId = (int)$this->get["room"];
		
		$helperId = (int)$this->get["helper"];
		$type = trim( $this->get["type"] );
		if( $floorId <= 0 || $roomId <= 0 || $helperId <= 0 || !$type  )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		$monsters = Battle_Model::getInstance( $this->userId )->getMonster( $floorId , $roomId , $helperId , $type  );
		
		$returnData = array(
			'steps_info' => $monsters,
			'user_info' => MakeCommand_User::userInfo( $this->userId ),
			'auto_skill' => Battle_Model::getInstance($this->userId)->getAutoSkill(),
		);
		return $returnData;
	}
	
	
	/**
	 * 奖励
	 */
	public function reward()
	{
		$damages = trim( $this->get["damages"] );
		$damageArr = explode( "," , $damages );

		$result = Battle_Model::getInstance( $this->userId )->reward( $damageArr );
	
		$returnData = array(
			'user_info' => MakeCommand_User::userInfo( $this->userId ),
			'get_cards' => $result['getCards'],
			'dungeon_info' => $result['battleInfo'],
			'add_gold' => (int)$result['addGold'],
			'bonus_gold' => (int)$result['bonusGold'],
			'add_exp' => (int)$result['addExp'],
			'bonus_exp' => (int)$result['bonusExp'],
			'levelUpBonus' => $result['levelUpBonus'],
			'level' => intval($result['level']),
			//'user_cards' => MakeCommand_Card::cards( $this->userId ),
		);
		return $returnData;
	
	}
	
	/**
	 * 复活
	 */
	public function revive()
	{
		User_Info::getInstance( $this->userId )->revive();
		$returnData = array(
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
		
	}

	/**
	 * 获得用户战场进度信息
	 * */
	public function getDungeonInfo()
	{
		return array(
			'user_dungeon_info' => array(
				'normal_current' => Battle_Model::getInstance( $this->userId )->getNormalCurrent(),
				'special' => Battle_Model::getInstance( $this->userId )->getSpecial(),
				'weekly' => Battle_Model::getInstance( $this->userId )->getWeekly(),
		) );
	}
	
	public function test()
	{
		/*
		echo "http://211.144.68.31:8080/liuchangbing/xproject/Web/Api/api.php?method=Battle.getHelper&uid=931915856&debug";
		echo "\n";
		echo "http://211.144.68.31:8080/liuchangbing/xproject/Web/Api/api.php?method=Battle.getMonster&uid=931915856&floor=1&room=1&helper=463044892&type=normal&debug";		
		*/
	
		Battle_Model::getInstance( $this->userId )->setInfo();
		ObjectStorage::save();
		
	
	}	
}
