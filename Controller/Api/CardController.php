<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 武将卡模块
 * @since	2013.1.14
 */
class CardController extends BaseController
{
	/**
	 * 武将编队
	 */
	public function formatTeam()
	{
		$cardMulti = Data_Card_Team::getInstance( $this->userId )->getAll();
		$cardMultiArr = &$cardMulti['teams'];
		
		if(empty($cardMultiArr[0][0])) return array(  'errorCode' => GameException::PARAM_ERROR );
		
		$needFmt = false;
		//参数验证
		for($i = 0; $i < Card_Model::TEAM_NUM; $i++)
		{
			$cardStr = trim( $this->get['cards'.($i == 0 ? "" : $i)] );
			if( !empty( $cardStr ) )
			{
				$needFmt = true;
				
				$cards = explode( ",", $cardStr );
				//if($i != 0) array_unshift($cards, $cardMultiArr[0][0]);
				$valid = $this->isCardsValid($cards);
				if($valid['errorCode']) return $valid;
				
				$cardMultiArr[$i] = $cards;
			}
			
			//少武将补0
			//for($j = count($cardMultiArr[$i]); $j < Card_Model::TEAM_CARD_NUM; $j++) array_push($cardMultiArr[$i], "0");
		}
		
		//当前编队
		if(isset($_REQUEST['cur']))
		{
			$curTeamStr = intval($_REQUEST['cur']);
			if($curTeamStr >= 0 && $curTeamStr < Card_Model::TEAM_NUM)
			{
				$needFmt = true;
				$cardMulti['cur'] = $curTeamStr;
			}
		}
		
		if(!$needFmt) return array( 'errorCode' => GameException::PARAM_ERROR );
		
		//编队
		Data_Card_Team::getInstance($this->userId, true)->setCardsMulti($cardMulti);
		$returnData = array(
			'user_info' => MakeCommand_User::userInfo( $this->userId ),
			'user_deck' => MakeCommand_Card::cardsTeam( $this->userId ),
			'user_deck_multi' => MakeCommand_Card::cardsTeamMulti( $this->userId ),
		);
		return $returnData;
		
	}
	
	/**
	 * 武将强化
	 */
	public function reinforce()
	{
		$cardStr = trim( $this->get['cards'] );
		$tCardId = intval( $this->get['cardId'] );
		if( empty( $cardStr ) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		$cards = explode( ",", $cardStr );
		
		//判断武将数量
		if( count( $cards ) > 5 )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		//判断序列ID是否有重复
		$countValues = array_count_values( $cards );
		foreach ( $countValues as $value => $times )
		{
			if(  $value && $times != 1 )
			{
				return array(  'errorCode' => GameException::PARAM_ERROR );
			}
		}
		
		//判断是否存在
		$cardModel = Card_Model::getInstance( $this->userId );
		$reCards = array();
		foreach ( $cards as $cardId )
		{
			if( $cardId > 0 )
			{
				$reCards[] = $cardId;
			}
			
			if( $cardId > 0 && !$cardModel->getCardInfo( $cardId ) )
			{
				return array(  'errorCode' => GameException::PARAM_ERROR );
			}
		}

		if( !$cardModel->getCardInfo( $tCardId ) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
	
		/**
		 * 强化
		 * @var unknown_type
		 */
		$status = $cardModel->reinforce(  $tCardId , $reCards );
		$returnData = array(
				'success_type' => $status,
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}
	
	
	/**
	 * 武将转生
	 */
	public function rebirth()
	{
		$cardStr = trim( $this->get['cards'] );
		$tCardId = intval( $this->get['cardId'] );
		if( empty( $cardStr ) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		$cards = explode( ",", $cardStr );
	
		
		//判断序列ID是否有重复
		$countValues = array_count_values( $cards );
		foreach ( $countValues as $value => $times )
		{
			if(  $value && $times != 1 )
			{
				return array(  'errorCode' => GameException::PARAM_ERROR );
			}
		}
		
		//判断是否存在
		$cardModel = Card_Model::getInstance( $this->userId );
		$reCards = array();
		foreach ( $cards as $cardId )
		{
			if( $cardId > 0 )
			{
				$reCards[] = $cardId;
			}
				
			if( $cardId > 0 && !$cardModel->getCardInfo( $cardId ) )
			{
				return array(  'errorCode' => GameException::PARAM_ERROR );
			}
		}
		
		if( !$cardModel->getCardInfo( $tCardId ) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		/**
		 * 转生
		 * @var unknown_type
		 */
		$status = $cardModel->rebirth(  $tCardId , $reCards );
		$returnData = array(
				'user_cards' => MakeCommand_Card::cards( $this->userId ),
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}
	
	/**
	 * 武将出售
	 */
	public function sell()
	{
		$cardStr = trim( $this->get['cards'] );
		if( empty( $cardStr ) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		$cards = explode( ",", $cardStr );
	
		//判断序列ID是否有重复
		$countValues = array_count_values( $cards );
		foreach ( $countValues as $value => $times )
		{
			if(  $value  && $times != 1 )
			{
				return array(  'errorCode' => GameException::PARAM_ERROR );
			}
		}
	
		//判断是否存在
		$cardModel = Card_Model::getInstance( $this->userId );
		$reCards = array();
		foreach ( $cards as $cardId )
		{
			if( $cardId > 0 )
			{
				$reCards[] = $cardId;
			}
	
			if( $cardId > 0 && !$cardModel->getCardInfo( $cardId ) )
			{
				return array(  'errorCode' => GameException::PARAM_ERROR );
			}
		}
		/**
		 * 出售
		 * @var unknown_type
		 */
		$status = $cardModel->sell( $reCards );
		$returnData = array(
				'user_cards' => MakeCommand_Card::cards( $this->userId ),
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	
	
	}
	
	/**
	 * 获取所有武将,也是所谓的军营
	 */
	public function getAll()
	{
		$cardModel = Card_Model::getInstance( $this->userId );
		$returnData = array(
				'user_cards' => MakeCommand_Card::cards( $this->userId ),
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}
	
	/**
	 * 武将加锁
	 */
	public function lock()
	{
		$cardId = intval( $this->get['cardId'] );
		//判断是否存在
		$cardModel = Card_Model::getInstance( $this->userId );
	
		if( !$cardModel->getCardInfo( $cardId ) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		/**
		 * 强化
		 * @var unknown_type
		 */
		$status = $cardModel->lock( $cardId );
		$returnData = array(
				'user_cards' => MakeCommand_Card::cards( $this->userId ),
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}
	
	/**
	 * 武将解锁
	 */
	public function unlock()
	{
		$cardId = intval( $this->get['cardId'] );
		//判断是否存在
		$cardModel = Card_Model::getInstance( $this->userId );
		
		if( !$cardModel->getCardInfo( $cardId ) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		/**
		 * 强化
		 * @var unknown_type
		 */
		$status = $cardModel->unlock( $cardId );
		$returnData = array(
				'user_cards' => MakeCommand_Card::cards( $this->userId ),
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
		
	}
	
	/**
	 * 求良将，求神将/抽将
	 */
	public function drawCard()
	{
		//是否免费抽
		$times =  intval( $this->get['times'] );
		if($times < 1) $times = 1;
		
		//类型，1为求良将，2,求神将， 3,为求良器，4,求神器,  以前的免费抽，为抽良将，付费抽为抽神将
		$type = intval( $this->get['type'] ) ?  intval( $this->get['type'] )  : 1;
		
		//兼容老接口
		if( isset( $_REQUEST['isFree'] ) ) $type = intval( $this->get['isFree'] ) ? 1 : 2;
		
		//首次免费抽，判断一下
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		$isFirst = 0;
		if( $type == 1  )
		{
			//没有免费抽良将
//			if( $userInfo['firstFreeDraw'] == 0 )
//			{
//				$isFirst = 1;
//			}
		}
		elseif( $type == 2 )
		{
//			if( $userInfo['firstChargeDraw'] == 0 )
//			{
//				$isFirst = 1;
//			}
			//免费抽神将走另外的接口
//			if( $_SERVER['REQUEST_TIME'] - $userInfo['freeDrawTime'] >=  86400 * 3 )
//			{
//				$isFirst = 1;
//			}
		}
		
		$cardModel = Card_Model::getInstance( $this->userId );
		$cardIds = $cardModel->drawCard( $type , $times , $isFirst );
	
		$returnData = array(
				'new_card' => MakeCommand_Card::newCard( $this->userId , $cardIds , $isFirst ),
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		if(  empty( $cardIds )  )
		{
			$returnData['new_card'] = array();
		}
	
		return $returnData;
	}
	
	/**
	 * 每三天免费抽神将
	 */
	public function freeDrawSuperCard()
	{
		//类型，1为求良将，2,求神将， 3,为求良器，4,求神器,  以前的免费抽，为抽良将，付费抽为抽神将
		$type = intval( $this->get['type'] ) ?  intval( $this->get['type'] )  : 2;

		$cardModel = Card_Model::getInstance( $this->userId );
		$cardIds = $cardModel->drawCard(  $type  , 1 , 1  );
		$returnData = array(
				'new_card' => MakeCommand_Card::newCard( $this->userId , $cardIds , 0  ),
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		if(  empty( $cardIds )  )
		{
			$returnData['new_card'] = array();
		}
		return $returnData;
	}
	
	/**
	 * 技能升级
	 */
	public function upgradeSkill()
	{
		$cardStr = trim( $this->get['cards'] );
		$tCardId = intval( $this->get['cardId'] );
		if( empty( $cardStr ) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		$cards = explode( ",", $cardStr );
		
		//判断武将数量
		if( count( $cards ) > 5 )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		//判断序列ID是否有重复
		$countValues = array_count_values( $cards );
		foreach ( $countValues as $value => $times )
		{
			if(  $value && $times != 1 )
			{
				return array(  'errorCode' => GameException::PARAM_ERROR );
			}
		}
		
		//判断是否存在
		$cardModel = Card_Model::getInstance( $this->userId );
		$reCards = array();
		foreach ( $cards as $cardId )
		{
			if( $cardId > 0 )
			{
				$reCards[] = $cardId;
			}
				
			if( $cardId > 0 && !$cardModel->getCardInfo( $cardId ) )
			{
				return array(  'errorCode' => GameException::PARAM_ERROR );
			}
		}
		
		if( !$cardModel->getCardInfo( $tCardId ) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		/**
		 * 升级技能
		 * @var unknown_type
		 */
		$status = $cardModel->upgradeSkill(  $tCardId , $reCards );
		$returnData = array(
				'success_type' => (int)$status,
		);
		return $returnData;
	}
	
	/**
	 * 获取解锁的武将卡信息
	 */
	public function unlocked()
	{
		$unlockedCard = Card_Model::getInstance( $this->userId )->getUnlockCard();
		$returnData = array(
				'collection' => $unlockedCard,
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}
	
	/**
	 * 消息通知
	 */
	public function noticeMsg()
	{
		User_Model::heartbeat( $this->userId );
		
		$msg = Card_Model::getInstance( $this->userId )->getMsgQueue();
		$returnData = array(
			'msgList' => $msg
		);
		return $returnData;
	}
	
	/**
	 * 武将信息是否有效
	 * Enter description here ...
	 * @param unknown_type $cards
	 */
	private function isCardsValid(&$cards, $cntLimit = 5)
	{
		//判断武将数量
		if( !is_array($cards) || count( $cards ) > $cntLimit ) return array(  'errorCode' => GameException::PARAM_ERROR );
	
		if( !$cards[0] ) return array(  'errorCode' => GameException::PARAM_ERROR );
	
		//判断序列ID是否有重复
		$countValues = array_count_values( $cards );
		foreach ( $countValues as $value => $times )
		{
			if( $value && $times != 1 ) return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		
		//判断是否存在
		foreach ( $cards as $cardId )
		{
			if( $cardId > 0 && !Card_Model::getInstance( $this->userId )->getCardInfo( $cardId ) ) return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		return array('errorCode' => 0);
	}
	
}
