<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * 邮件
 * @since	2013.08.23
 */
class MailController extends BaseController
{
	
	public function getList()
	{
		$maxSize = 20;
		$defaultSize = 5;
		$page = abs(intval($_REQUEST['page']));
		$size = abs(empty($_REQUEST['size']) ? $defaultSize : intval($_REQUEST['size']));
		$size = min($maxSize, $size);
		
		//0为所有, 1为未读
		$type = intval($_REQUEST['type']);
		
		if($type != 0 && $type != 1) return array(  'errorCode' => GameException::PARAM_ERROR );
		
		$returnData = Mail_Model::getInstance($this->userId)->getList($page, $size, $type);
		return $returnData;
	}

	public function unread()
	{
		$unreadNum = Mail_Model::getInstance($this->userId)->getUnreadNum();
		return array('num' => $unreadNum);
	}
	
	/**
	 * 目前只有后台发送，用户不能发送
	 */
	/*
	public function sendMail()
	{
		$ret = Mail_Model::getInstance($this->userId)->sendMail($fromId, $title, $content, $bonus, $deadTime);
		return $ret;
	}
	*/
	
	public function dealMail()
	{
		$id = intval($_REQUEST['id']);
		$type = strval(trim($_REQUEST['type']));
		
		if($id < 1 && $type != "delAll") return array(  'errorCode' => GameException::PARAM_ERROR );
		
		if($type == "read")
		{
			$result = Mail_Model::getInstance($this->userId)->readMail($id);
		}
		elseif($type == "bonus")
		{
			$result = Mail_Model::getInstance($this->userId)->getBonus($id);
		}
		elseif($type == "lock")
		{
			$result = Mail_Model::getInstance($this->userId)->lockMail($id);
		}
		elseif($type == "unlock")
		{
			$result = Mail_Model::getInstance($this->userId)->unLockMail($id);
		}
		elseif($type == "del")
		{
			$result = Mail_Model::getInstance($this->userId)->delMail($id);
		}
		elseif($type == "delAll")
		{
			$result = Mail_Model::getInstance($this->userId)->delAll();
		}
		else
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		return $result ? $result : array(  'errorCode' => Mail_Exception::STATUS_OPERATION_FAIL );
	}
	
}
