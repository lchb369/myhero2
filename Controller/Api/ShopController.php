<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * @since	2013.1.14
 * 商店模块
 */
class ShopController extends BaseController
{
	
	/**
	 * 购买物品
	 */
	public function buy()
	{
		$itemId = (int)$this->post['itemId'];
		$itemConfig = User_Info::getInstance( $this->userId )->buy( $itemId );
		$returnData = array(
			'user_info' => MakeCommand_User::userInfo( $this->userId ),
			'itemType' => $itemConfig['type'],
		);
		return $returnData;
	}
	
	/**
	 * 物品列表
	 */
	public function itemList()
	{	
		$returnData = array(
				'list' => User_Info::getInstance( $this->userId )->itemList(),
		);
		return $returnData;
	}
	
	/**
	 * 体力回复
	 */
	public function recoverStamina()
	{
		User_Info::getInstance( $this->userId )->recoverStamina();
		$returnData = array(
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}
	
	
	/**
	 * 军营扩充
	 * @deprecated
	 */
	public function expandCamp()
	{
		User_Info::getInstance( $this->userId )->expandCamp();
		$returnData = array(
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}
	
	
	/**
	 * 好友扩充
	 * @deprecated
	 */
	public function expandFriend()
	{
		$maxNum = User_Info::getInstance( $this->userId )->expandFriend();
		$returnData = array(
				'max_num' => $maxNum,
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}
	
}