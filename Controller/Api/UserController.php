<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * @since	2013.1.14
 */
class UserController extends BaseController
{
	
	/**
	 * 兼容老版本
	 */
	public function getOldSession()
	{
		$loginName = $this->get['loginName'];
		
		//如果微博绑定默认，那么loginName为微博帐号，bindName为自动生成的
		$bindName = trim( $this->get['bindName'] );
		
		if(  !$bindName  )
		{
			$existFlag =  $loginName;
			while( $existFlag == false )
			{
				$randNum = mt_rand( 1 , 100000000 );
				$loginName = "loginName_$randNum".$_SERVER['REQUEST_TIME'];
				$loginName =  md5( $loginName );
				$loginName =  substr(  $loginName , 1 , 27 )."xiuce";
		
				$uId = User_ConvertUserId::convertToGameUserId( $loginName );
				if( $uId > 0 )
				{
					if( User_Model::exist( $uId ) == true )
					{
						$existFlag = false;
					}
					else
					{
						break;
					}
				}
			}
		
			if( !$uId )
			{
				$uId = User_ConvertUserId::convertToGameUserId( $loginName );
			}
		}
		//绑定帐号功能，两个登录帐号，绑定同一个游戏ID
		else
		{
			$bindUid = User_ConvertUserId::convertToGameUserId( $bindName );
			if( $bindUid <=0 )
			{
				return array(  'errorCode' => GameException::PARAM_ERROR );
			}
		
			$uId = User_ConvertUserId::bindGameUserId( $loginName  , $bindUid  ) ;
			if(  $uId == 0 || $uId != $bindUid  )
			{
				//return array(  'errorCode' => User_Exception::STATUS_LOGIN_NAME_HAS_BIND  );
			}
		}
		
		User_Model::setSig( $uId , $_POST['sig'] );
		$session = User_Model::getSession(   $uId  );
		if( User_Model::exist( $uId ) == false )
		{
			$userInfo['newbieStep'] = 0;
		}
		else
		{
			$userInfo = User_Info::getInstance( $uId )->getData();
		}
		
		//Stats_Model::addUserActionLog( $uId , "User.getSession" );
		
		$returnData = array(
				'loginName' =>  $loginName,
				'session' => $session,
				'newbie' => $userInfo['newbieStep'] < User_Model::$maxNewbieStep ? 1 : 0,
				'newbie_step' => $userInfo['newbieStep'] ? (int)$userInfo['newbieStep'] : 0 ,
		);
		return $returnData;
	}
	
	/**
	 * 用户注册
	 */
	public function getSession()
	{
		$loginName = addslashes(trim($this->get['loginName']));
		
		$loginType = intval($this->get['loginType']);
		if( !$loginType )
		{
			return $this->getOldSession();
		}
		
		//官网登陆校验
// 		$loginSig = trim($this->get['loginSig']);
// 		$retCheckLogin = Helper_WWW::get(User_Model::$ECN_LOGIN_SERVER.'?',array(
// 			'method' => 'User.checkLogin',
// 			'uid' => $this->userId,
// 			'appId' => User_Model::$ECN_LOGIN_APPID,
// 			'loginSig' => $loginSig,
// 		));
		$retCheckLogin['data']['status'] = 1;
		$retCheckLogin = json_encode($retCheckLogin);
		do{
			$verify = false;
			
			if(empty($retCheckLogin)) break;
			$retCheckLogin = json_decode($retCheckLogin, true);
			if(!isset($retCheckLogin['data']['status']) || $retCheckLogin['data']['status'] != 1) break;
			
			$verify = true;
		}while(0);
		if(!$verify) return array(  'errorCode' => GameException::PARAM_ERROR );
		
		
		User_Model::setSig( $this->userId , $_POST['sig'] );
		$session = User_Model::getSession(   $this->userId  );
		if( User_Model::exist( $this->userId ) == false )
		{
			$userInfo['newbieStep'] = 0;
		}
		else
		{
			$userInfo = User_Info::getInstance( $this->userId )->getData();
		}
		
		//Stats_Model::addUserActionLog( $this->userId , "User.getSession" );
		
		$returnData = array(
				'loginName' =>  $loginName,
				'session' => $session,
				'newbie' => $userInfo['newbieStep'] < User_Model::$maxNewbieStep ? 1 : 0,
				'newbie_step' => $userInfo['newbieStep'] ? (int)$userInfo['newbieStep'] : 0 ,
		);
		return $returnData;	
	}

	
	/**
	 * 新手引导
	 * @return multitype:Ambigous <multitype:, string> multitype:multitype:number unknown   multitype:number string unknown multitype:number unknown  Ambigous <>  multitype:number unknown string
	 */
	public function setNewbie()
	{
		$returnData = array();
		//初始化每日登录
		$userModel = User_Info::getInstance( $this->userId );
		$userinfo = $userModel->getData();
		//新手步骤，从0开始
		$step = intval( $this->get['step'] );
		//第一步，选一个国家,如果步骤为0，国家也为0，什么也不做
        $country = intval( $this->get['country'] );
		        
        if( (  $step > 0 ||  ( $step == 0 && $country > 0 ) ) && $step < $userinfo['newbieStep'] )
        {
        	$step = 0; 
        	$country =0;
       	}

        //中断恢复
		if( $step == 0 && $country == 0 )
		{
		}
		else
		{
			//记录的是，下一次步骤
			$result = User_Info::getInstance( $this->userId )->setNewbie(
				$step < User_Model::$maxNewbieStep - 1
					? $step + 1 
					: 99);
		}
		
		$_newbieStep = "_newbieStep".$step;
		//不同的步骤分支
		switch ($step) {
			case 0:
				if($country > 0)
				{
					$nickName = strval( $this->get['nickName'] );	
					//$nickName=  iconv( 'gb2312','utf-8',$nickName );
					$thirdId = trim($this->get['thirdId']);
					
					//第三方平台信息
					$thirdParams = array('tel', 'mail', 'thirdId', 'partner');
					foreach($thirdParams as $param)
					{
						$info = trim($this->get[$param]);
						if(!empty($info)) $thirdInfo[$param] = $info;
					}
					
					$this->$_newbieStep( $country , $nickName, $thirdId, $thirdInfo);
					
					/**
					 *EDAC统计
					 */
					Stats_Edac::newUser( $this->userId );
					
					//将自己加入到义军名单，供别人搜
					Battle_Model::addHelperUser( $this->userId );
					
					//使用邀请码登录游戏
					$useInviteCode = $this->get["inviteCode"];
					//如果是新用户
					if( $useInviteCode  )
					{
						$InviteResult = $userModel->initNewUser( $useInviteCode );
					}
				}
				break;
			default:
				if(method_exists(__CLASS__, $_newbieStep))
				{
					$this->$_newbieStep($step, $returnData);
					//新手跳过
					if($step == 1 && $_REQUEST['skipNewbie'] == 1)
					{
						for($i = $step + 1; $i < User_Model::$maxNewbieStep; $i++)
						{
							$__newbieStep = "_newbieStep".$i;
							if(method_exists(__CLASS__, $__newbieStep))
							{
								$this->$__newbieStep($i, $returnData);
								ObjectStorage::save();
							}
						}
						User_Info::getInstance( $this->userId )->setNewbie( 99 );
					}
				}
				break;
		}
		
		$returnData['user_info'] = MakeCommand_User::userInfo( $this->userId );
		$returnData['user_cards'] = MakeCommand_Card::cards( $this->userId );
        $returnData['user_deck'] = MakeCommand_Card::cardsTeam( $this->userId );
		$returnData['invite/d_award'] = strval( $InviteResult['inviteGold'] ) 
										? strval( $InviteResult['inviteGold'] )  : "0";
		
		$userModel->initFirstLogin();
		
		/**
		 * 数据中心统计edac:登录统计，新手引导统计
		 */
		Stats_Edac::userLogin( $this->userId );
		//新手引导
		if( $step > 0 )
		{
			Stats_Edac::upNewbie( $this->userId , $step );
		}
		
		//Stats_Model::addUserActionLog( $this->userId , "User.setNewbie" , strval( $step ) );
		
		return $returnData;
	}

	/**
	 * 0 创建角色 
	 * @param unknown $country
	 * @param unknown $nickName
	 * @param unknown $thirdId
	 * @param unknown $thirdInfo
	 */
	private function _newbieStep0($country, $nickName, $thirdId, $thirdInfo)
	{
		$userProfile = User_Profile::getInstance( $this->userId );
		//设置玩家名字
		if(  $userProfile->setNickName( $nickName ) == false )
		{
			throw new User_Exception(   User_Exception::STATUS_NICKNAME_USED  );
		}
	
		$isNewUser = $userProfile->isNewUser();
		
		$userProfile->setThirdId($thirdId);
		//设置第三方用户到游戏用户转换的缓存
		if(!empty($thirdId))
		{
			$cache = Common::getCache();
			$key = User_Model::$CACHE_KEY_TID2UID.$thirdId;
			$cache->set($key, $this->userId);
		}
		//初始化第三方信息
		if(!empty($thirdInfo))
		{
			Data_Activity_Model::getInstance( $this->userId, true )->updateRecord(Activity_Model::ACTIVE_THIRD_INFO, $thirdInfo);
		}
		
		//用户游戏信息
		$userInfo = User_Info::getInstance( $this->userId );
	
		//武将/将领模块
		$cardModel = Card_Model::getInstance( $this->userId );
	
		//如果是新用户
		if( $isNewUser == true )
		{
		}
	
		if(  $country > 3 ||  $country <= 0 )
		{
			throw new GameException(   GameException::PARAM_ERROR  );
		}
	
		User_Info::getInstance( $this->userId )->setCountry( $country );
		
		//增加一个武将,并将其设置为主将
		if( $country == 1 )	//操操
		{
			$cardModel->addCard( "1_card" );
			$cardModel->addCard( "43_card" );
			$cardModel->addCard( "45_card" );
		}
		elseif( $country == 2 ) //备备
		{
			$cardModel->addCard( "9_card" );
			$cardModel->addCard( "41_card" );
			$cardModel->addCard( "43_card" );
		}
		elseif( $country == 3 ) //权权
		{
			$cardModel->addCard( "5_card" );
			$cardModel->addCard( "41_card" );
			$cardModel->addCard( "45_card" );
		}
		//设置武将队伍，首个是主将
		$cardModel->formatTeam( "1,2,3" );
	}
	
	/**
	 * 1 通过关卡战场1 消除玩法介绍
	 */
	private function _newbieStep1($step, &$returnData)
	{
		$result = User_Info::getInstance( $this->userId )->setNewbie( $step + 2 );
		User_Info::getInstance( $this->userId )->changeCoin(5, '新手引导', 1, $step);
		User_Info::getInstance( $this->userId )->addExp(12);
		User_Info::getInstance( $this->userId )->changeGold(12, '新手引导');
	}
	
	/**
	 * 2 关卡战场2“战场宝藏”
	 */
	private function _newbieStep3($step, &$returnData)
	{
		User_Info::getInstance( $this->userId )->changeCoin(5, '新手引导', 1, $step);
		User_Info::getInstance( $this->userId )->addExp(40);
		User_Info::getInstance( $this->userId )->changeGold(16, '新手引导');
		Card_Model::getInstance( $this->userId )->addCard( "61_card" );
	}
	
	/**
	 * 3 编队
	 */
	private function _newbieStep4($step, &$returnData)
	{
		$cardModel = Card_Model::getInstance( $this->userId )->formatTeam( "1,2,3,4" );
	}
	
	/**
	 * 4 关卡战场3“一骑当千”
	 */
	private function _newbieStep5($step, &$returnData)
	{
		User_Info::getInstance( $this->userId )->changeCoin(5, '新手引导', 1, $step);
		User_Info::getInstance( $this->userId )->addExp(80);
		User_Info::getInstance( $this->userId )->changeGold(32, '新手引导');
		Card_Model::getInstance( $this->userId )->addCard( "47_card" );
	}
	
	/**
	 * 5 强化
	 */
	private function _newbieStep6($step, &$returnData)
	{
		//主将
		$baseCardId = 1;
		//最后一个武将
		$costCardIds = Card_Model::getInstance( $this->userId )->getLastId();
		$costCardIdArr = explode( ",", $costCardIds );
		
		$status = Card_Model::getInstance( $this->userId )->reinforce( $baseCardId ,  $costCardIdArr );
		$returnData['success_type'] = $status;
	}
	
	/**
	 * 6 关卡战场4“武将之技”
	 */
	private function _newbieStep7($step, &$returnData)
	{
		User_Info::getInstance( $this->userId )->changeCoin(5, '新手引导', 1, $step);
		User_Info::getInstance( $this->userId )->addExp(70);
		User_Info::getInstance( $this->userId )->changeGold(28, '新手引导');
		Card_Model::getInstance( $this->userId )->addCard( "84_card" );
		Card_Model::getInstance( $this->userId )->addCard( "226_card" );
	}
	
	/**
	 * 7 技能强化
	 */
	private function _newbieStep8($step, &$returnData)
	{
		//主将
		$baseCardId = 1;
		//最后一个武将
		$costCardIds = Card_Model::getInstance( $this->userId )->getLastId();
		$costCardIdArr = explode( ",", $costCardIds );
		
		$status = Card_Model::getInstance( $this->userId )->upgradeSkill( $baseCardId , $costCardIdArr , true );
		$returnData['success_type'] = $status;
	}
	
	/**
	 * 8 关卡战场5“属性相克”
	 */
	private function _newbieStep9($step, &$returnData)
	{
		User_Info::getInstance( $this->userId )->changeCoin(5, '新手引导', 1, $step);
		User_Info::getInstance( $this->userId )->addExp(70);
		User_Info::getInstance( $this->userId )->changeGold(1028, '新手引导');
	}
	
	/**
	 * 9 抽神将
	 */
	private function _newbieStep10($step, &$returnData)
	{
		$cardIds = Card_Model::getInstance( $this->userId )->drawCard(2, 1, 0, true);
		$returnData['new_card'] = MakeCommand_Card::newCard( $this->userId , $cardIds , 1 );
		//$this->inviteThirdBonus();
	}
	
	/**
	 * @deprecated
	 * 第三方平台好友邀请奖励
	 */
	private function inviteThirdBonus()
	{
		$userProfile = User_Profile::getInstance( $this->userId )->getData();
		Friend_Model::inviteThirdBonus($userProfile['thirdId']);
	}
	
	/**
	 * 删除用户
	 */
	public function delete()
	{
		$payLog = new ErrorLog( "User.delete" );
		$payLog->addLog( "uid:".$this->userId.">>".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
		
		$ret = User_Model::deleteUser( $this->userId );
		$returnData = $ret 
			? array('errorCode' => GameException::PARAM_ERROR) 
			: array();
		return $returnData;
	}
	
	/**
	 * 排行榜(财富, 等级)
	 * @return array
	 */
	public function getRank()
	{
		$type = $this->get['type'];
		if( empty($type) || ( $type != "gold" && $type != "level" ) )
		{
			return array(  'errorCode' => GameException::PARAM_ERROR );
		}
		
		$rankList = Rank_Model::getInstance( $this->userId )->getRank( $type );
		
		return array( "rankList" => $rankList );
	}
	
	public function info()
	{
		return array( "user_info" => MakeCommand_User::userInfo( $this->userId ) );
	}
	
	/**
	 * 用户活动信息
	 * Enter description here ...
	 */
	public function activity()
	{
		$returnData = array();
		
		//是否有累计充值活动
		$paidConfigs = Common::getConfig('paid');
		
		foreach( $paidConfigs as $type => &$paidConfig )
		{
			foreach( $paidConfig as &$conf )
			{
				if(Helper_Date::isInTime($conf['startTime'], $conf['endTime']))
				{
					$returnData["paid".$conf['type']] = 1;
				}
			}
		}
		
		$returnData['coin_sale'] = intval( Data_Order_Model::getInstance( $this->userId )->itemHasExceedLimit( Order_Model::PROMOTION_PACK_1 ) );
		
		$recoveryStaminaInfo = Data_Activity_Model::getInstance($this->userId)->getActivity(Activity_Model::ACTIVE_MANUAL_RECOVERY_STAMINA);
		//恢复体力
		$returnData['recoveryStamina'] = array(
			'conf' => Activity_Model::getConfRecoveryStamina(),
			'rewardTime' => intval($recoveryStaminaInfo['rewardTime']),
		);
		//点将台
		$returnData['pointCard'] = 0;
		$pointConfigs = Common::getConfig('pointCard');
		foreach($pointConfigs as &$conf)
		{
			if(Helper_Date::isInTime($conf['start_time'], $conf['end_time']))
			{
				$returnData['pointCard'] = 1;
				break;
			}
		}
		
		$returnData['disablePayment'] = 1;
		
		return $returnData;
	}

	public function festival()
	{
		$returnData = array();
		//首页活动
		$activityConfs = Common::getConfig('activity');
		$_activityConfs = array();
		foreach($activityConfs as &$actTypeConfs)
		{
			foreach($actTypeConfs as $id => &$conf)
			{
				$conf['id'] = $id;
				$_activityConfs[$id] = $conf;
			}
		}
		$activityConfs = $_activityConfs;
		$festivalConfs = Common::getConfig('festival');
		$festival = array();
		foreach($festivalConfs as &$conf)
		{
			$conf['start_time'] = strtotime( $conf['start_time'] );
			$conf['end_time'] = strtotime( $conf['end_time'] );
			if( !($_SERVER['REQUEST_TIME'] >= $conf['start_time'] && $_SERVER['REQUEST_TIME'] < $conf['end_time'] ) )
				continue;
				
			//有关联数据
			if($conf['rewardType'] == 1)
			{
				
				if(!empty($activityConfs[$conf['rewardValue']]))
					$conf['linkedData'] = $activityConfs[$conf['rewardValue']];
			}
			$festival[] = $conf;
		}
		$returnData['festival'] = $festival;
		return $returnData;
	}
	
}
