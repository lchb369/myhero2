<?php
if( !defined( 'IN_INU' ) )
{
	return;
}

/**
 * @since	2013.1.14
 * 支付模块
 */
class PaymentController extends BaseController
{
	
	/**
	 * 支付宝购买物品
	 */
	public function aliTradeCallBack()
	{
		//error_reporting( E_ALL ^ E_NOTICE );
		//ini_set( 'display_errors' , 'On' );
		$payLog = new ErrorLog( "aliPaymentLog" );
		$payLog->addLog( "response:".$_SERVER['QUERY_STRING'] );
		$payLog->addLog( "response:".json_encode( $_REQUEST ) );
		
		$result = Payment_AliPay_NotifyUrl::processCallBack();
		$payLog->addLog( "result:".json_encode( $result ) );
		//$result['uid'] = 838860;
		if( $result['status'] == "success" && $result['uid'] > 0 )
		{
			$status = Order_Model::getInstance( $result['uid'] )->addOrder( $result['order'] );
			if( $status == false )
			{
				echo "success";
				exit;
			}
			//根据goodsName和数量发货
			$goodsConfig = Common::getConfig( "goods" );
			foreach ( $goodsConfig as $goodCnf )
			{
				if( $result['order']['goodsName'] == $goodCnf['goodsDesc'] )
				{
					$number = $result['order']['goodsNum'] ? $result['order']['goodsNum'] : 1;
					//User_Info::getInstance( $result['uid']  )->changeCoin( $addCoin );
					Order_Model::getInstance(   $result['uid']  )->SendGoods( $goodCnf['goodsId'] , $number , "ali" , $result['order']['totalPrice'] , $result['order']['id'] );
				}	
			}
		}
		echo $result['status'];
	}
	
	
	public function get91OrderId()
	{
		return $this->getOrderId();
	}
	/**
	 * 获取订单ID
	 */
	public function getOrderId()
	{
		$goodsId = $this->post['gid'];
		
		$orderId = Payment_Model::getInstance($this->userId)->getOrderId($goodsId);
		
		return array( 'order' => $orderId );
	}

	/**
	 * 获取订单ID
	 */
	public function earnPoint()
	{
		//元宝数
		$goodsId = $this->post['gid'];
		$orderId = $this->post['order'];
		$sign = $this->post['sign'];
		$key = "myhero_tapjoy12#";
		
		$payLog = new ErrorLog( "tapjoyEarnPoint" );
        $payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
		
		if( $sign != md5( $key.$goodsId.$orderId ) )
		{
			throw new GameException(   GameException::PARAM_ERROR  );
		}
		
		$cache = Common::getCache();
		if( !$order = $cache->get( $orderId ) )
		{
			throw new GameException(   GameException::PARAM_ERROR  );
		}
		
		if( !!$status = User_Info::getInstance( $this->userId )->changeCoin( $goodsId , "tapjoy" ) )
		{
			$cache->delete( $orderId );
		}
		
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		$returnData = array(
				'coin' => intval( $userInfo['coin'] ),
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}
	
	/**
	 * 91支付回调
	 */
	public function callBack91Pay()
	{
		//TODO:: 分区回调
		$payLog = new ErrorLog( "91PaymentLog" );
		$payLog->addLog(  "response:".$_SERVER['QUERY_STRING']."\n" );

		$result = Payment_DianJing91_Model::payResultNotifyProcess();
		
		ObjectStorage::save();
		echo $result;exit;
	}
	
	/**
	 * 91平台:客户端验证支付状态,供客户端调用
	 */
	public function get91PayStatus()
	{
		$orderId = $this->post['orderId'];
		$status = Order_Model::getInstance( $this->userId )->checkOrderStatus( $orderId );
		
		$returnData = array(
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
				'status' => $status,
		);
		return $returnData;
	}
	

	/**
	 * 财富通支付接口
	 * 参数:uid , gid , num , price
	 */
	public function tenPay()
	{
		$gid = $this->post['gid'];
		$num = $this->post['num'];
		$token = Payment_TenPay_PayRequest::getToken( $this->userId , $gid , $num );
		
		$returnData = array(
				'token' =>trim( $token ),
		);
		
		$payLog = new ErrorLog( "tenPayGetToken" );
		$payLog->addLog( "token:".$token );
		return $returnData;
	}
	
	/**
	 * 财富通支付回调
	 */
	public function tenPayReturn()
	{
		Payment_TenPay_PayReturnUrl::proccess();
		ObjectStorage::save();
		exit;
	}
	
	/**
	 * 财富通支付回调消息
	 */
	public function tenPayNotify()
	{
		//error_reporting( E_ALL ^ E_NOTICE );
		//ini_set( 'display_errors' , 'On' );
		$payLog = new ErrorLog( "tenPayNotify" );
		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
		
		Payment_TenPay_PayNotifyUrl::proccess();
		ObjectStorage::save();
		exit;
	}
	
	
	/**
	 * 当乐支付回调
	 */
	public function dangleNotify()
	{
		$payLog = new ErrorLog( "danglePaymentLog" );
		$payLog->addLog( "callbackParam:".$_SERVER['QUERY_STRING'] );
		$status = Payment_DangLe_PayNotifyUrl::proccess();
		ObjectStorage::save();
		
		if( $status == true )
		{
			echo "success";exit;
		}
		else
		{
			echo "fail";exit;
		}
			
	}
	
	
	/**
	 * mo9支付回调
	 */
	public function mo9Notify()
	{

		$payLog = new ErrorLog( "mo9PaymentLog" );
		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_POST ) );
		Payment_Mo9_PayNotifyUrl::proccess();
		ObjectStorage::save();
		exit;
	
	}
	
	/**
	 * 微派
	 */
	public function weipaiNotify()
	{
		$payLog = new ErrorLog( "weipaiPaymentLog" );
		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
	    echo Payment_WeiPai_PayNotifyUrl::proccess();
		
		ObjectStorage::save();
		exit;
	}
	
	/**
	 * weipai
	 * 参数:uid , gid , num , price
	 */
	public function createWeiPaiOrder()
	{
		
		$payLog = new ErrorLog( "createWeiPaiOrder" );
		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
		
		/*
		$str = '{"os":"android","refer":"1","method":"Payment.createWeiPaiOrder","uid":"1","session":"0fcb18a85281841f4bd2af5b2726204c","orderId":"003778167","gid":"8"}';
		$this->post = json_decode( $str , true );
		print_r( $this->post );exit;
		*/
		
		$gid = $this->post['gid'];
		$orderId =  $this->post['orderId'];
		
		if(  !$gid  || !$orderId )
		{
			throw new GameException(   GameException::PARAM_ERROR  );
		}
		
		$status = Payment_WeiPai_PayNotifyUrl::createOrder( $this->userId , $gid , $orderId );
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		
		$returnData = array(
				'status' => $status ? 1 : 0,
				'coin' => intval( $userInfo['coin'] ),
		);
		return $returnData;
	}

	/**
	 * 新浪登录
	 */
	public function sinaLogin()
	{
		$payLog = new ErrorLog( "sinaLogin" );
		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
	}
	
	/**
	 * 获取元宝数量
	 */
	public function getCoin()
	{
		$userInfo = User_Info::getInstance( $this->userId )->getData();
		$returnData = array(
				'coin' => intval( $userInfo['coin'] ),
				'user_info' => MakeCommand_User::userInfo( $this->userId ),
		);
		return $returnData;
	}
	
	/**
	 * uc登录
	 */
	public function ucLogin()
	{
		  error_reporting( E_ALL ^ E_NOTICE );
		  ini_set( 'display_errors' , 'On' );
		  $sid = $this->post['sid'];
          $rs =  Payment_UC_Api::sidInfo( $sid );
          echo $rs;exit;
	}
	
	
	public function UCPayNotify()
	{
		$payLog = new ErrorLog( "ucPayNotify" );
		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
		//echo 234;exit;
		echo Payment_UC_Api::payCallBack();
		ObjectStorage::save();
		exit;
	}
	
	/**
	 * 小米
	 */
	public function XMPayNotify()
	{
		$payLog = new ErrorLog( "XMPayNotify" );
		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
		
		echo Payment_XM_Api::payCallBack();
		ObjectStorage::save();
		exit;
	}
	
	/**
	 * 机锋支付回调
	 */
	public function JFPayNotify()
	{
		$payLog = new ErrorLog( "JFPayNotify" );
		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
		
		echo Payment_JF_Api::payCallBack();
		ObjectStorage::save();
		exit;
	}
	

	/**
	 * 支付回调
	 */
	public function WLPayNotify()
	{
		$payLog = new ErrorLog( "WLPayNotify" );
		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
	
		echo Payment_WL_Api::payCallBack();
		ObjectStorage::save();
		exit;
	
	}
	
	

	/**
	 * 点金是点金，91是91注意区分，上面的91命名不确切
	 * 点金支付回调
	 */
   	public function DJPayNotify()
    {
         $payLog = new ErrorLog( "DJPaymentLog" );
         $payLog->addLog(  "response:".$_SERVER['QUERY_STRING']."\n" );
         $payLog->addLog(  "response:".json_encode( $_POST )."\n" );
         $result = Payment_DJ_Model::process();

          ObjectStorage::save();
          echo $result;exit;
     }
	
        
        /**
         * 宝软购买
         */
        public function BRBuy()
        {
        	//CMJFK00010001  CMJFK	移动
        	//CMJFK00010014  CMJFK  福建移动
        	//DXJFK00010001   DXJFK   中国电信
        	//LTJFK00020000     LTJFK	联通
        	$cardNum = trim(  $this->post['cardNum'] );
        	$cardPwd =  trim(  $this->post['cardPwd'] );
        	
        	$pmId = trim(  $this->post['pmId'] );
        	$pcId = trim(  $this->post['pcId'] );
        	
        	$price = trim(  $this->post['price'] );
        	//Payment_BR_Api::buy( $this->userId , '07922014061823564' , '013714485034432100' ,  'CMJFK00010001' , 'CMJFK' );
        	
        	$payLog = new ErrorLog( "BRPayNotify" );
        	$payLog->addLog(  "response:".json_encode( $_REQUEST )."\n" );
        	
        	$result = Payment_BR_Api::buy( $this->userId , $cardNum  , $cardPwd  , $pmId  , $pcId , $price  );
        	
        	$payLog->addLog(  "result:".$result."\n" );
        	$returnData = array(
        			'result' => $result,
        	);
        	return $returnData;
        }
	
       
        /**
         *  宝软回调
         * 点金支付回调
         */
        public function BRPayNotify()
        {
        	error_reporting( E_ALL ^ E_NOTICE );
        	ini_set( 'display_errors' , 'On' );
        	
        	$payLog = new ErrorLog( "BRPayNotify" );
        	$payLog->addLog(  "response:".$_SERVER['QUERY_STRING']."\n" );
        	$payLog->addLog(  "response:".json_encode( $_POST )."\n" );
        	$result = Payment_BR_Api::payNotify();
        	ObjectStorage::save();
        	
        	if( $result == false )
        	{
        		echo "error";
        	}
        	else 
        	{
        		echo "ok";
        	}
        	exit;
        }
        
        
        /**
         *  宝软回调
         * 点金支付回调
         */
        public function PayNotify51()
        {
        	error_reporting( E_ALL ^ E_NOTICE );
        	ini_set( 'display_errors' , 'On' );
        
        	$payLog = new ErrorLog( "PayNotify51" );
        	$payLog->addLog(  "response:".$_SERVER['QUERY_STRING']."\n" );
        	$payLog->addLog(  "response:".json_encode( $_REQUEST )."\n" );
        	$result = Payment_51_Api::payNotify();
        	ObjectStorage::save();
        
        	if( $result == false )
        	{
        		echo "failure";
        	}
        	else
        	{
        		echo "success";
        	}
        	exit;
        }
        
        /**
         * 91_ios 支付回调
         */
        public function PayNotify91IOS()
        {
        	$payLog = new ErrorLog( "91IOSPaymentLog" );
        	$payLog->addLog(  "response:".$_SERVER['QUERY_STRING']."\n" );
        
        	$result = Payment_91IOS_Model::payResultNotifyProcess();
        
        	ObjectStorage::save();
        	echo $result;exit;
        }
	
        public function get360Token()
        {
        	$code = $this->get['code'];
        	$result = Payment_360_Model::getAccessToken( $code );
        	$returnData = array(
        			'result' => $result,
        	);
        	return $returnData;
        	 
        }
        
        public function get360User()
        {
        	 
        	$token = $this->get['token'];
        	$result = Payment_360_Model::getUserInfo( $token );
        	$returnData = array(
        			'result' => $result,
        	);
        	return $returnData;
        	 
        }
        
        /**
         * appStore(掌趣)
         */
        public function appStoreBuy()
        {
        
        	$payLog = new ErrorLog( "appStoreBuy" );
        	$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        
        	$gid = $this->post['gid'];
        	$orderId =  $this->post['orderId'];
        
        	if(  !$gid  || !$orderId )
        	{
        		throw new GameException(   GameException::PARAM_ERROR  );
        	}
        
        	$status = Payment_AppStore_Model::createOrder( $this->userId , $gid , $orderId );
        	$userInfo = User_Info::getInstance( $this->userId )->getData();
        
        	$returnData = array(
        			'status' => $status ? 1 : 0,
        			'coin' => intval( $userInfo['coin'] ),
        			'msg' => $status 
        					? MakeCommand_LangPack::get("paySuccess", "購買成功") 
        					: MakeCommand_LangPack::get("payFail", "購買失敗"),
        	);
        	return $returnData;
        }
        
        /**
         * googleplay
         * 参数:uid , gid , num , price
         */
        public function googlePlayOrder()
        {
        	$payLog = new ErrorLog( "GooglePlayOrder" );
        	$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );

        	$orderId =  $this->post['orderId'];
	        $signdata = $this->post['signdata'];
	        $signature =  $this->post['signature'];
	        
	        if(  !$orderId || !$signdata  || !$signature )
	        {
	        	throw new GameException(   GameException::PARAM_ERROR  );
	        }

	        $status = Payment_GooglePlay_PayNotifyUrl::verifyAndCreateOrder( $this->userId , $orderId , $signdata , $signature );
        	
        	$userInfo = User_Info::getInstance( $this->userId )->getData();
        
        	$returnData = array(
        			'status' => $status ? 1 : 0,
        			'coin' => intval( $userInfo['coin'] ),
        	);
        	return $returnData;
        }
        
        
        /**
         * 统振 IMoney
         */
        public function IMoneyReturnUrl()
        {
        	$userId = $this->get['userId'];
        	$orderId =  $this->get['orderId'];
        	
        	Payment_IMoney_PayNotifyUrl::returnUrl( $userId , $orderId );
        }
        
        /**
         * 统振 IMoney 验证
         * C->S
         */
        public function IMoneyVerify()
        {
        	if( empty( $this->userId ) )
        	{
        		throw new GameException(   GameException::PARAM_ERROR  );
        	}
        	$payLog = new ErrorLog( "IMoneyOrder" );
        	$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );

        	$returnData = Payment_IMoney_PayNotifyUrl::verify( $this->userId );
        	exit($returnData);
        }
        
        /**
         * 统振 IMoney 回调通知
         * Third->S
         */
        public function IMoneyNotify()
        {
        	$payLog = new ErrorLog( "IMoneyOrder" );
        	$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	
        	$returnData = Payment_IMoney_PayNotifyUrl::notify();
        	echo $returnData;
        	ObjectStorage::save();
        	exit;
        }
        
        public function MyCard_IsEligible()
        {
        	$payLog = new ErrorLog( "MyCardOrder" );
        	$payLog->addLog( "isEligible:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	 
        	$returnData = Payment_MyCard_Model::isEligible();
        	echo json_encode( array( "ResultCode" => ( $returnData ? 1 : 0 ) ) );
        	exit;
        }
        
        public function MyCard_Bridge()
        {
        	$payLog = new ErrorLog( "MyCardOrder" );
        	$payLog->addLog( "bridge:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        
        	$returnData = Payment_MyCard_Model::bridge();
        	
        	$payLog->addLog( "bridge-return:".json_encode( $returnData ) );
        	
        	echo json_encode( $returnData );
        	ObjectStorage::save();
        	exit;
        }
        
        public function MyCardCheckOrder()
        {
        	$payLog = new ErrorLog( "MyCardOrder" );
        	$payLog->addLog( "checkOrder:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	
        	$orderId =  $this->post['orderId'];
        	if( empty( $this->userId ) || empty( $orderId ) )
        	{
        		throw new GameException(   GameException::PARAM_ERROR  );
        	}
        	$returnData = Payment_MyCard_Model::checkOrder( $orderId );
        	return array( "check" => $returnData );
        }
        
        /**
         * pp助手
         * Enter description here ...
         */
        public function ppNotify()
        {
        	$payLog = new ErrorLog( "ppPaymentLog" );
      		$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
      		
      		//判断分区
      		if( intval( $_POST['zone'] ) > 0 )
      		{
				//$returnData = Payment_PP_Model::Appreturn();
	        	//echo $returnData;
	        	echo "fail";
	        	exit;      			
      		}
      		else
      		{
      			$this->ppNotifyPartition();	
      		}
        }
        
        public function ppNotifyPartition()
        {
        	$payLog = new ErrorLog( "ppPaymentLog" );
      		$payLog->addLog( "callbackParamPartition:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
      		
        	$returnData = Payment_PP_Model::Appreturn();
        	echo $returnData;
        	ObjectStorage::save();
        	exit;
        }
        
        public function telepayReady()
        {
        	$payLog = new ErrorLog( "telepayLog" );
        	$payLog->addLog( "Ready:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	$orderId = $this->getOrderId();

        	Payment_Telepay_Model::ready( $this->userId , $this->post['gid'] , $orderId['order'] );
        	exit;
        }
        
        public function telepayCPCGI()
        {
        	$payLog = new ErrorLog( "telepayLog" );
        	$payLog->addLog( "CPCGI:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	
        	Payment_Telepay_Model::cpcgi();
        	ObjectStorage::save();
        	exit;
        }
        
        public function telepaySuccess()
        {
        	$payLog = new ErrorLog( "telepayLog" );
        	$payLog->addLog( "Success:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	 
        	Payment_Telepay_Model::success();
        	exit;
        }



	  /**
         * 悠悠村支付回调
         */
        public function uucunNotify()
        {
        	error_reporting( E_ALL ^ E_NOTICE );
        	ini_set( 'display_errors' , 'On' );
        	$payLog = new ErrorLog( "UUCPaymentLog" );
        	$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	
        	//echo 324234;
        	Payment_UUCun_Model::process();
        	ObjectStorage::save();
        	exit;
        }
        
        
        /**
         * naver
         */
        public function NaverIAPOrder()
        {
        	$payLog = new ErrorLog( "NaverOrder" );
        	$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        
        	$orderId =  $this->post['orderId'];
        	$signdata = $this->post['signdata'];
        	$signature =  $this->post['signature'];
        	 
        	if(  !$orderId || !$signdata  || !$signature )
        	{
        		throw new GameException(   GameException::PARAM_ERROR  );
        	}
        	
        	$status = Payment_NaverIAP_PayNotifyUrl::verifyAndCreateOrder( $this->userId , $orderId , $signdata , $signature );
        	 
        	$userInfo = User_Info::getInstance( $this->userId )->getData();
        
        	$returnData = array(
        		'status' => $status ? 1 : 0,
        		'coin' => intval( $userInfo['coin'] ),
        	);
        	return $returnData;
        }
        
        /**
         * T-Store
         */
        public function TStoreIAPOrder()
        {
        	$goodsId = intval($this->post['gid']);
        	$orderId = strval($this->post['order']);
        	$sign = strval($this->post['sign']);
        	
        	$status = Payment_TStore_PayNotifyUrl::verifyAndCreateOrder($this->userId, $goodsId, $orderId, $sign); 
        
        	$userInfo = User_Info::getInstance( $this->userId )->getData();
        	$returnData = array(
        		'status' => $status ? 1 : 0,
        		'coin' => intval( $userInfo['coin'] ),
        	);
        	return $returnData;
        }
        
        /**
         * 老版本的移动基地
         */
        public function CMGNotify()
        {
        	$payLog = new ErrorLog( "CMGOrder" );
        	$xmlstring = file_get_contents("php://input");
        	$payLog->addLog( "callbackParam:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST )."[xmlstring]:".$xmlstring );
        	
        	$retXML = '<?xml version="1.0" encoding="UTF-8"?><response><transIDO></transIDO><hRet>1</hRet><message>Fail</message></response>';
        	$retXML = new SimpleXMLElement($retXML);
        	
        	$status = 0;
        	do
        	{
        		$xml = simplexml_load_string($xmlstring);
        		if(empty($xml) || $xml->hRet != 0)
        		{
        			$payLog->addLog( "verify fail" );
        			break;
        		}
        		$thirdUid = strval($xml->userId);
        		$consumeCode = strval($xml->consumeCode);
        		$userId = intval($xml->cpParam);
        		$transIDO = strval($xml->transIDO);
        		$retXML->transIDO = $transIDO;
        		if(!!$status = Payment_CMG_PayNotifyUrl::verifyAndCreateOrder($userId, $consumeCode, $transIDO))
        		{
        			ObjectStorage::save();
        			$retXML->hRet = 0;
        			$retXML->message = 'Successful';
        		}
        		
        	}while(0);
        	
        	header("Content-type: text/xml; charset=utf-8");
        	echo $retXML->asXML();
        	exit;
        }
        
        public function CMGUserInfo()
        {
        	header("Content-type: text/plain; charset=utf-8");
        	echo Payment_CMG_PayNotifyUrl::userInfo();
        	exit;
        }
        
        public function H3GConfirm()
        {
        	$payLog = new ErrorLog( "H3GOrder" );
        	$payLog->addLog( "callbackParam[H3GConfirm]:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	
        	header("Content-type: text/html; charset=utf-8");
        	Payment_H3G_Model::getInstance($this->userId)->confirm();
        	exit;
        }
        
        public function H3GMobileNumber()
        {
        	$payLog = new ErrorLog( "H3GOrder" );
        	$payLog->addLog( "callbackParam[H3GMobileNumber]:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	
        	header("Content-type: text/html; charset=utf-8");
        	Payment_H3G_Model::getInstance($this->userId)->mobileNumber();
        	exit;
        }
        
        public function H3GAuth()
        {
        	$payLog = new ErrorLog( "H3GOrder" );
        	$payLog->addLog( "callbackParam[H3GAuth]:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	
        	header("Content-type: text/html; charset=utf-8");
        	$mobileNumber = intval($this->get['mobileNumber']);
        	Payment_H3G_Model::getInstance($this->userId)->auth($mobileNumber);
        	exit;
        }
        
        public function H3GComplete()
        {
        	$payLog = new ErrorLog( "H3GOrder" );
        	$payLog->addLog( "callbackParam[H3GComplete]:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	
        	header("Content-type: text/html; charset=utf-8");
        	$mobileNumber = intval($this->get['mobileNumber']);
        	$authCode = trim($this->get['authCode']);
        	Payment_H3G_Model::getInstance($this->userId)->complete($mobileNumber, $authCode);
        	ObjectStorage::save();
        	exit;
        }

        public function ThienPhuCard()
        {
        	$payLog = new ErrorLog( "ThienPhuPayLog" );
        	$payLog->addLog( "callbackParam[ThienPhuCard]:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        
        	header("Content-type: text/html; charset=utf-8");
        	
        	Payment_ThienPhu_Model::getInstance($this->userId)->card();
        	exit;
        }
        
        public function ThienPhuCardSubmit()
        {
        	$payLog = new ErrorLog( "ThienPhuPayLog" );
        	$payLog->addLog( "callbackParam[ThienPhuCardSubmit]:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	 
        	header("Content-type: text/html; charset=utf-8");
        	
        	//serial: 	String	充值卡序列号
        	$serial = trim($this->get['cardNo']);
        	//code: 	String	充值卡密码
        	$code = trim($this->get['cardPwd']);
        	//telco	String	电讯公司 VMS or VNP or VTT
        	$telco = trim($this->get['cardType']);
        	
        	Payment_ThienPhu_Model::getInstance($this->userId)->cardSubmit($serial, $code, $telco);
        	ObjectStorage::save();
        	exit;
        }
        
        public function ThienPhuSmsInit()
        {
        	$payLog = new ErrorLog( "ThienPhuPayLog" );
        	$payLog->addLog( "ThienPhuSmsInit:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	
        	$gid = intval($this->get['gid']);
        	if( empty( $this->userId ) || empty( $gid ) )
        	{
        		throw new GameException(   GameException::PARAM_ERROR  );
        	}
        	
        	$returnData = Payment_ThienPhu_Model::getInstance($this->userId)->ThienPhuSmsInit($gid);
        	
        	return $returnData;
        }
        
        public function ThienPhuNotify()
        {
        	$payLog = new ErrorLog( "ThienPhuPayLog" );
        	$payLog->addLog( "notify:".$_SERVER['REQUEST_URI'].json_encode( $_REQUEST ) );
        	 
        	$security = trim($this->get['security']);
        	$smsid = trim($this->get['smsid']);
        	$keyword = trim($this->get['keyword']);
        	$shortcode = trim($this->get['shortcode']);
        	$customer = trim($this->get['customer']);
        	$message = trim($this->get['message']);
        	 
        	$ret = Payment_ThienPhu_Model::notify($security, $smsid, $keyword, $shortcode, $customer, $message);
        	ObjectStorage::save();
        	echo json_encode($ret);
        	exit;
        }
        
        public function test()
        {
        	exit;
        }
        
}
