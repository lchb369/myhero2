<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class UploadConfigController extends AdminBaseController
{
	const UPDATE_TABLE_STATUS_NORMAL = 0;
	const UPDATE_TABLE_STATUS_FATAL_ERROR = 1;
	const UPDATE_TABLE_STATUS_CONFIG_ERROR = 2;
	const UPDATE_TABLE_STATUS_NOT_LOGIN = 3;
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function login()
	{
		$sessionId = $_POST['sessionId'];
		if( !empty( $sessionId ) )
		{
			session_write_close();
			session_id( $sessionId );
			session_start();
		}
		if( $_POST['data'] )
		{
			$_POST = array_merge( $_POST , json_decode( $_POST['data'] , true ) );
		}
		if( empty( $_SESSION[ 'admin' ] ))
		{
			$admin = empty( $_POST[ 'admin' ] ) ? null : $_POST[ 'admin' ];
			$password = empty( $_POST[ 'password' ] ) ? null: $_POST[ 'password' ];
			if( is_null( $admin ) || is_null( $password ) )
			{
				echo json_encode(
					array(
						'error' => array(
							array(
								'message'=> 'Please Login ^_^!' ,
								'level' => ExchangeConfig_ValidateError::ERR_ERROR ,
							) ,
						) ,
						'status' => self::UPDATE_TABLE_STATUS_NOT_LOGIN ,
					)
				);
				exit;
			}
			else 
			{
				$adminConfig = Common::getConfig( 'AdminConfig' );
				if( isset( $adminConfig[ 'adminList' ][ $admin ] ) )
				{
					$psw = $adminConfig[ 'adminList' ][ $admin ][ 'password' ];
					if( !empty( $psw ) && $psw ==  $password )
					{
						session_write_close();
						$sessionId = sha1( mt_rand() );
						session_id( $sessionId );
						session_start();
						$_SESSION[ 'admin' ] = $admin;
						echo json_encode(
							array(
								'status' => self::UPDATE_TABLE_STATUS_NORMAL ,
								'sessionId' => $sessionId ,
							)
						);
						exit;
					}
				}
				echo json_encode(
					array(
						'error' => array(
							array(
								'message'=> 'Password Error ^_^!' ,
								'level' => ExchangeConfig_ValidateError::ERR_ERROR ,
							) ,
						) ,
						'status' => self::UPDATE_TABLE_STATUS_NOT_LOGIN ,
					)
				);
			}
			exit;
		}
		else if( !empty( $_SESSION['admin'] ) && $_GET['act'] == 'login' )
		{
			return array(
				'status' => self::UPDATE_TABLE_STATUS_NORMAL ,
			);
		}
	}
	
	public function logout()
	{
		parent::logout();
		return array(
			'status' => self::UPDATE_TABLE_STATUS_NORMAL ,
		);
	}
	
	public function update()
	{
		return $this->_uploadFile();
	}
	
	public function validateUploadConfig()
	{
		return $this->_uploadFile( false );
	}
	
	private function _uploadFile( $isWriteFile = true )
	{
		$configs = array();
		if( isset( $_POST['config'] ) )
		{
			$configs = json_decode( $_POST['config'] , true );
		}
		
		if( !$configs )
		{var_dump( $_POST );
			return array(
				'error' => array(
					array(
						'message'=> '数据错误' ,
						'level' => ExchangeConfig_ValidateError::ERR_ERROR ,
					) ,
				) ,
				'status' => self::UPDATE_TABLE_STATUS_FATAL_ERROR ,
			);
		}
		
		$errorMessages = array();
		$excelList = new ExchangeConfig_Exchanger_List_Excel();
		foreach( $configs as $tableName => $data )
		{
			if( !isset( $excelList[$tableName] ) )
			{
				$errorMessages[] = array(
					'message' => "{$tableName}的转换器还没有提供\n" ,
					'level' => ExchangeConfig_ValidateError::ERR_NOTICE ,
				);
				continue;
			}
			
			$this->_getExcelExchanger( $excelList , $tableName )->setDatas( $data , $isWriteFile );
			$this->_getExcelExchanger( $excelList , $tableName )->validateExcelFile();
			foreach( $this->_getExcelExchanger( $excelList , $tableName )->getValidateError() as $error )
			{
				$errorMessages[] = array(
					'message' => $error->getErrorMessage() ,
					'level' => $error->getErrorLevel()
				);
			}
		}
		
		if( !empty( $errorMessages ) )
		{
			return array(
				'error' => $errorMessages ,
				'status' => self::UPDATE_TABLE_STATUS_CONFIG_ERROR ,
			);
		}
		return array(
			'status' => self::UPDATE_TABLE_STATUS_NORMAL ,
		);
	}
	
	/**
	 * 获取Excel转换器
	 * @param ExchangeConfig_Exchanger_List_Excel $excelList
	 * @param string $tableName
	 * @return	ExchangeConfig_Exchanger_Excel_Abstract
	 */
	private function _getExcelExchanger( $excelList , $tableName )
	{
		return $excelList[$tableName];
	}
	
	/* (non-PHPdoc)
	 * @see AdminBaseController::main()
	 */
	protected function main()
	{
		
	}

}
?>