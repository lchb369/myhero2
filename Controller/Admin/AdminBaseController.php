<?php 

if( !defined( 'IN_INU' ) )
{
	return;
}
/**
 * 所有controller的基类
 * @author	Luckyboys
 * @since	2010.11.02
 */
abstract class AdminBaseController
{
	protected $post = array();
	protected $get = array();
	protected $userId = 0;
	protected $config = array();
	private $tplData = array();
	protected $adminObj = array();
	protected $adminType = 0;
	
	public function __construct()
	{
		if( get_magic_quotes_gpc() )
		{
			Common::prepareGPCData( $_GET );
			Common::prepareGPCData( $_POST );
		}

		$_POST = $_GET = $this->get = $this->post = array_merge( $_POST , $_GET );
		$this->config = Common::getConfig();
		$this->login();
	}
	
	public function login()
	{
		if( preg_match( "/Admin\/third/", $_SERVER[REQUEST_URI] ))
		{
			//表示是提供给第三方查询入口
			$this->adminType = 1;
		}

		if( empty( $_SESSION[ 'admin' ] )
			//不同平台登录 
			|| (!empty($_GET['pf']) && !empty($_GET['sid']) 
				&& ($_SESSION['appPlatform'] != $_GET['pf'] || $_SESSION['appSid'] != $_GET['sid'])
		))
		{
			$loginErr = "请输入管理员帐号和密码";
			$admin = empty( $_POST[ 'admin' ] ) ? null : $_POST[ 'admin' ];
			$password = empty( $_POST[ 'password' ] ) ? null: $_POST[ 'password' ];
			if( is_null( $admin ) || is_null( $password )  )
			{
				$this->assign( 'loginErr', $loginErr);
				$this->display( 'login.php' );
			}
			else 
			{
				$this->adminObj = Data_Admin_Model::getInstance( 1 , true );
				$adminData = $this->adminObj->getData();
				if( $adminData[$admin] &&  $this->adminObj->checkPwd( $admin , $password ))
				{ 
					
					session_start();
					$_SESSION[ 'admin' ] = $adminData[$admin];
					$_SESSION['appPlatform'] = $_GET['pf'];
					$_SESSION['appSid'] = $_GET['sid'];
					if(!empty($_REQUEST['lang'])) $_SESSION['lang'] = $_REQUEST['lang'];
					
					return $this->main();
				}
				$loginErr = '用户名/密码错误';
				$this->assign( 'loginErr', $loginErr);
				$this->display( 'login.php' );
			}
			exit;
		}
		else
		{
			$this->main();
		}
	}
	
	public function logout()
	{
		session_unset();
		session_destroy();
		header("location:index.php");
		/*
		$loginErr = "请输入管理员帐号和密码";
		$this->assign( 'loginErr', $loginErr);
		$this->display( 'login.php' );
		header( 'Local');
		*/
	}
	
	/**
	 * 设置模板变量
	 */
	protected function assign( $key , $value )
	{
		$this->tplData[$key] = $value;
	}
	
	/**
	 * 展示模板
	 */
	protected function display( $tpl )
	{
		extract( $this->tplData );
		include TPL_DIR.'/'.$tpl;
	}
	

	protected function getChecked( $x )
	{
		if( $x == "*" )
		{
			$x = 63;
		}
		
		$step = array();
		for( $i=1;$i<10;$i++)
		{
		$y = decbin( $x );
		$z = $y % 2 ;
		if( $z == 1 )
		{
			$step[] = pow( 2 , $i-1);
		}
		$x = $x>>1;
	}
	return $step;
	}
	
	/**
	 * 主程序
	 */
	abstract protected function main();
}
?>
