<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class IndexController extends AdminBaseController
{
	private $db;
	
	public function __construct()
	{
		$_GET['refer'] = $_GET['input_uid'] = trim(  $_GET['input_uid'] );
		
		$userSearchType = trim($_GET['userSearchType']);
		//$notUid = !preg_match("/^[0-9]{1,11}$/", $_GET['input_uid']);
		
		do{
			if(empty($_GET['input_uid'])) break;
			
			//判断昵称
			if($userSearchType == "nickName")
			{
				$uId = $this->getUid( $_GET['input_uid'] );
				if(!empty($uId))
				{
					$_GET['input_uid'] = intval( $uId );
					break;
				}
			}
			//寻找第三方id
			elseif($userSearchType == "thirdId")
			{
				$uId = User_Model::getIdByThird($_GET['input_uid']);
				if(!empty($uId))
				{
					$_GET['input_uid'] = intval($uId);
					break;
				}
			}
			//游戏uid
			else
			{
				$_GET['input_uid'] = intval($_GET['input_uid']);
				break;
			}
			
			$_GET['input_uid'] = 0;
			
		}while(0);
		
		parent::__construct();
	}
	
	
	public function main()
	{
		//print_r( $conf );exit;	
		$f = empty( $_GET[ 'f' ] ) ? 'user' : $_GET[ 'f' ];
		
		if(  $this->adminType == 1  )
		{
			$f = empty( $_GET[ 'f' ] ) ? 'thirdQuery' : $_GET[ 'f' ];
		}
		
		$this->_checkPermiss( $f );

		$this->assign( 'f' , $f );
		if( method_exists( $this , $f ) )
		{
			$this->$f();
		}
		exit();
	}
	
	public function getUid( $nickName )
	{
		$sql = "select uid from user_profile where nickname='{$nickName}' ";
		$dbEngine = Common::_getDbEngine( 1 );
		$result = $dbEngine->fetchArray($sql);
		return $result[0]['uid'];
		
	}
	
	/**
	 * 用户模块
	 *
	 */
	public function user()
	{
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
		//$userId = 524667377;
			
		if( $do == "updateCard" )
		{
			/*
			[f] => user
			[do] => updateCard
			[uid] => 151535950
			[cid] => 1
			[cexp] => 44
			[input_uid] => 0
			*/
			$cardInfo = Card_Model::getInstance( $_GET['uid'] )->getCardInfo( $_GET['cid'] );
			$addExp = $_GET['cexp'] - $cardInfo['exp'];
			$cardModel = Card_Model::getInstance( $_GET['uid'] )->addExp( $_GET['cid'] , $addExp );
			ObjectStorage::save();
			exit;
		}
		elseif( $do == "bind" )
		{
			$loginName = trim( $_GET['lname']);
			$bindUid = intval( $_GET['oldUid']);
			$uId = User_ConvertUserId::forceBindGameUserId( $loginName  , $bindUid  ) ;
			if( $uId == $bindUid )
			{
				$bindMsg = "绑定成功";
			}
		}
		elseif( $do == "delUser" )
		{
			$result = User_Model::deleteUser( $_GET['uid'] );
			echo $result ? "删除成功" : "删除失败";
			exit;
		}
		elseif( $do == "testOrder" )
		{
			$gid = intval( $_GET['gid'] );
			$goodsConfig = Common::getConfig( "goods" );
			$goodsConfig = $goodsConfig[$gid];
			if( !empty( $goodsConfig ) )
			{
				$price = $goodsConfig[$gid]['price'];
					
				$order = array(
						'id' => "test",
						'goodsName' => $gid,
						'goodsNum' => 1,
						'price' => $price,
						'discount' => 0,
						'totalPrice' => $price,
						'buyerId' => $userId,
						'tradeTime' => $_SERVER['REQUEST_TIME'],
						'addTime' => $_SERVER['REQUEST_TIME'],
						'email' => '',
						'platform' => "MyCard",
						'status' => 4,
						'thirdId' => "test",
				);
				$rs = Order_Model::getInstance( $userId )->addOrder( $order );
				
				Order_Model::getInstance( $userId )->SendGoods( $gid , 1 , "test" , $order['price'] , $order['id'] );
				ObjectStorage::save();
			}
		}

		if( $userId > 0 )
		{
			$userData = Data_User_Info::getInstance( $userId , true )->getData();		
			
			//用户信息
			if( isset( $_POST['level'] ) )
			{
				$addExp = 0;
				foreach ( $_POST as $item => $value )
				{
					if( !isset( $userData[$item] ) )
					{
						continue;
					}
					
					if( $item == "exp" )
					{
						$addExp = $value - $userData['exp'];
					}
					
					if( $item == "coin" )
					{
						Stats_Model::addGMActionLog( $userId , "user" , "coin" , $value - $userData['coin'] , $userData['coin'] );
					}
					
					//如果修改了，加上
					$userData[$item] = $value;
				}
				
				Data_User_Info::getInstance( $userId , true )->setUserInfo( $userData );
				if( $addExp > 0 )
				{
					User_Info::getInstance( $userId )->addExp( $addExp , false );
				}
				ObjectStorage::save();
			}
			//PVP信息
			if( isset( $_POST['benefit'] ) )
			{
				Data_Arena_Normal::getInstance( $userId , true )->setData( array(
					"benefit" => intval( $_POST['benefit'] )
				) );
				ObjectStorage::save();
			}
			
			//普通战场信息
			if( isset( $_POST['normal_floor'] ) )
			{
				Data_Battle_Normal::getInstance( $userId , true )->setData( array(
					'floor' => intval( $_POST['normal_floor'] ) ,
					'room' => intval( $_POST['normal_room'] ),
					'pass' => intval( $_POST['normal_pass'] ), 
				) );
				ObjectStorage::save();
			}
			
			/*
			$cardFormat = trim(  $_POST['cardTeam'] );
			if( $cardFormat && $cardFormat !=  $cardTeam['cards'])
			{
				Card_Model::getInstance( $userId )->formatTeam( $cardFormat );
				ObjectStorage::save();
			}
			*/

			if( $_POST['card']  || $_POST['inputCard'])
			{
				if( $do == "add" )
				{
				
					if( $_POST['inputCard']  )
					{
						$inputNum = $_POST['inputNum'] > 50 ? 50 : $_POST['inputNum'];
						for ( $i = 0; $i < $inputNum ; $i++ )
						{
							Card_Model::getInstance( $userId )->addCard( $_POST['inputCard']  , 1 , true , "GM" );
						}
					}
					
					if(  $_POST['card']  )
					{
						foreach ( $_POST['card'] as $cardCode )
						{
							Card_Model::getInstance( $userId )->addCard( $cardCode , 1 , true , "GM" );
						}
					}
					ObjectStorage::save();
				}
				elseif( $do == "delete" )
				{
					Card_Model::getInstance( $userId )->delCard( $_POST['card'] );
					ObjectStorage::save();
				}
			}
			
			$userData = Data_User_Info::getInstance( $userId , true )->getData();
			$userProfile = Data_User_Profile::getInstance( $userId , true )->getData();
			$cardList = Data_Card_Model::getInstance( $userId , true )->getData();
			$arenaData = Data_Arena_Normal::getInstance( $userId , true )->getData();
			$battleNormalData = Data_Battle_Normal::getInstance( $userId , true )->getData();
			
			//第三方信息
			$thirdInfo = Data_Activity_Model::getInstance($userId)->getActivity(Activity_Model::ACTIVE_THIRD_INFO);
			$userData['thirdId'] = $thirdInfo['data'];
			
			//充值记录
			$totalRecharge = Data_Order_Model::getInstance( $userId )->getTotalRecharge();
			if( Helper_Common::inPlatform( array( "wk" ) ) ) $totalRecharge *= Helper_Common::CURRENCY_CN2TW;
			$userData['totalRecharge'] = $totalRecharge; 
			
			$userData['totalRechargeCoin'] = Data_Order_Model::getInstance( $userId )->getTotalRechargeCoin();
		}
		else
		{
			$cardList = array();
		}
		
		$permissArr = $this->getChecked( $_SESSION['admin']['permission'] );
		$canWrite = 1;
		if( !in_array( 2, $permissArr ))
		{
			$canWrite = 0;
		}
		
//$cardTeam

		$this->assign( "canwrite", $canWrite );
		$this->assign( 'userId' , $userId );
		$this->assign( 'userData' , $userData );
		$this->assign( 'cardTeam' , $cardTeam );
		$this->assign( 'userProfile' , $userProfile );
		$this->assign( 'cardList' , $cardList );
		$this->assign( 'arenaData' , $arenaData);
		$this->assign( 'battleNormalData' , $battleNormalData );
		$this->display( 'user.php' );
	}
	
	
	/**
	 * 好友管理
	 */
	public function friend()
	{
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
		//$userId = 524667377;
		
		try {
			$helpers = Battle_Model::getInstance( $userId )->getOtherHelpers();
			if( $do == 'add' )
			{
				$randFriend = $_GET['randFriendId'];
				$friendId = $_GET['friendId'];
				
				if( $friendId > 0 && User_Model::exist( $friendId ) )
				{
					//好友邀请我
					Friend_Model::getInstance( $friendId )->invite( $userId );
					//我再接受
					Friend_Model::getInstance( $userId )->accept( $friendId );
					ObjectStorage::save();
				}
				
				if( $randFriend > 0 && User_Model::exist( $randFriend ) )
				{
					//好友邀请我
					Friend_Model::getInstance( $randFriend )->invite( $userId );
					//我再接受
					Friend_Model::getInstance( $userId )->accept( $randFriend );
					ObjectStorage::save();
				}
				
				
			}
			elseif($do == 'req')
			{
				$randFriend = $_GET['randFriendId'];
				$friendId = $_GET['friendId'];
				
				if( $friendId > 0 && User_Model::exist( $friendId ) )
				{
					//好友邀请我
					Friend_Model::getInstance( $friendId )->invite( $userId );
					ObjectStorage::save();
				}
				
				if( $randFriend > 0 && User_Model::exist( $randFriend ) )
				{
					//好友邀请我
					Friend_Model::getInstance( $randFriend )->invite( $userId );
					ObjectStorage::save();
				}
			}
			
			$friendList = Data_Friend_Model::getInstance( $userId , true )->getData();
		}
		catch ( Exception $e )
		{}
		
		$friendList = $friendList ? $friendList :array();
		
		
		$permissArr = $this->getChecked( $_SESSION['admin']['permission'] );
		$canWrite = 1;
		if( !in_array( 2, $permissArr ))
		{
			$canWrite = 0;
		}
		$this->assign( "canwrite", $canWrite );
		
		
		$this->assign( 'userId' , $userId );
		$this->assign( 'helpers' , $helpers );
		$this->assign( 'friendList' , $friendList );
		$this->display( 'friend.php' );
	
	}

	/**
	 * 订单模块
	 */
	public function order()
	{
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
		
	
		$dbUid = 428373593;
		$dbEngine = Common::_getDbEngine( $dbUid );
		
		//最近10条订单记录
		$currPage = $_GET['currPage'] ? intval(  $_GET['currPage'] ) : 1 ;
		$start = ($currPage-1)*20;
		
		$getStartTime =  $_GET['startTime'] ;
		$getEndTime = $_GET['endTime'];
		
		if( !empty( $_GET['date'] ) )
		{
			$getStartTime = $_GET['date'];
			$getEndTime = $getStartTime." 23:59:59";
		}
		
		if( !empty( $getStartTime ))
		{
			$startTime = strtotime( $getStartTime );
		}
		
		
		if( !empty($getEndTime ))
		{
			$endTime = strtotime($getEndTime );
		}
		$endTime = $endTime ? $endTime : $_SERVER['REQUEST_TIME'];
		$endTime =   $endTime > $_SERVER['REQUEST_TIME'] ? $_SERVER['REQUEST_TIME'] :  $endTime;

		$startTime = $startTime ? $startTime : 0;
		$and = " and status = 1 and addTime > {$startTime} and addTime < {$endTime} ";
		
	
		
		if( !empty( $_GET['byUid'] ) )
		{
			$and .= "  and uid = ".intval( $_GET['byUid'] );
		}
		
		
		if( !empty( $_GET['payPf'] ) )
		{
			$and .= "  and platform = '".$_GET['payPf']."' ";
		}
		
		$goodsConf = Common::getConfig( "goods" );
		
		if( isset( $_GET['type'] ) )
		{
			if( $_GET['type'] == 'export' )
			{
				$dbUid = 428373593;
				$dbEngine = Common::_getDbEngine( $dbUid );
				$sql = "select * from `order` where 1 = 1 $and order by addTime desc ";
				$orders = $dbEngine->fetchArray( $sql );
				$pf = Common::getConfig( "platform" );
				
				$file_name = $pf."_ORDER_".date('YmdHis').".csv";//文件名
				$encoded_filename  = urlencode($file_name);
				$encoded_filename  = str_replace("+","%20",$encoded_filename );
				//判断浏览器，输出双字节文件名不乱码
				$ua = $_SERVER["HTTP_USER_AGENT"];
				if (preg_match("/MSIE/", $ua)) {
					header('Content-Disposition: attachment; filename="' . $encoded_filename . '"');
				}
				else if (preg_match("/Firefox/", $ua)) {
					header('Content-Disposition: attachment; filename*="utf8\'\'' . $file_name . '"');
				}
				else {
					header('Content-Disposition: attachment; filename="' . $file_name . '"');
				}
				header("Content-Disposition:attachment;filename=".$file_name);
				
				//header("Content-type:text/csv;charset=UTF-8");
				
				header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
				header('Expires:0');
				header('Pragma:public');
				
				$str =  "订单号\t商品名称\t商品数量\t金额\t用户ID\t支付平台\t第三方流水\t交易时间\n";
				
				foreach( $orders as $order )
				{
					if( Helper_Common::inPlatform( array( "wk" ) ) ) $order['price'] *= Order_Model::CURRENCY_CNY_2_TWD;
					$desc = $goodsConf[$order['goodsName']]['goodsDesc'];
					$str .=  $order['id']."\t$desc\t1\t{$order['price']}\t{$order['uid']}\t{$order['platform']}\t=\"{$order['thirdId']}\"\t".date( "Y-m-d H:i:s" , $order['addTime'])."\n";
				}
				
				//$str =  iconv('UTF-8','GBK',$str );
				if(function_exists('mb_convert_encoding')){
					header('Content-type: text/csv; charset=UTF-16LE');
					//输出BOM
					echo(chr(255).chr(254));
					echo(mb_convert_encoding($str,"UTF-16LE","UTF-8"));
					exit;
				}
			}
		}
		else
		{
			$sql = "select * from `order` where 1 = 1 $and order by addTime desc limit $start , 20  ";
			
			$orders = $dbEngine->fetchArray( $sql );
			
			foreach( $orders as &$order )
			{
				if( Helper_Common::inPlatform( array( "wk" ) ) )
				{
					$order['price'] *= Order_Model::CURRENCY_CNY_2_TWD;
					$order['coin'] = $goodsConf[$order['goodsName']]['coin'];
				}
			}
			
			//总订单数量
			$sql = "select count(*) as totalNum from `order` where 1= 1 $and ";
			$result = $dbEngine->fetchArray( $sql );
			$totalNum = $result[0]['totalNum'];
			
			//成功支付定单数量
			$sql = "select count(*) as succNum from `order` where status = 1 $and ";
			$result = $dbEngine->fetchArray( $sql );
			$succNum = $result[0]['succNum'];
			
			//成功总金额
			$sql = "select sum(totalPrice) as total from `order` where status = 1 $and";
			$result = $dbEngine->fetchArray( $sql );
			$totalPrice = $result[0]['total'];
			
			//实际支付总金额
			$sql = "select sum(price) as price from `order` where status = 1 $and";
			$result = $dbEngine->fetchArray( $sql );
			$price = $result[0]['price'];
			
			if( Helper_Common::inPlatform( array( "wk" ) ) )
			{
				$totalPrice *= Order_Model::CURRENCY_CNY_2_TWD;
				$price *= Order_Model::CURRENCY_CNY_2_TWD;
			}
			
			//付费用户数量
			/*
			$sql = "select count(*)  as count from `order` where status = 1 $and  group by uid";
			$result = $dbEngine->fetchArray( $sql );
			$count = $result[0]['count'];
			*/
			
			//支付平台类型
			$cache = Common::getCache();
			$cacheKey = 'admin_payTypes';
			if(!$payTypes = $cache->get($cacheKey))
			{
				$sql = "select distinct platform from `order` ";
				$payTypes = $dbEngine->fetchArray( $sql );
				$cache->set($cacheKey, $payTypes, 5 * 60);
			}
			
			$this->assign( 'payTypes' , $payTypes);
			
			$this->assign( 'userId' , $userId );
			$this->assign( 'orders' , $orders );
			$this->assign( 'totalNum' , $totalNum );
			$this->assign( 'currPage' , $currPage );
			$this->assign( 'succNum' , $succNum );
			$this->assign( 'totalPrice' , $totalPrice );
			$this->assign( 'price' , $price );
			$this->assign('startTime', $startTime);
			$this->assign('endTime', $endTime);
			
			$this->display( 'order.php' );
		}
	}
	
	/**
	 * 第三方用户查询
	 */
	//public function crossmo()
	public function thirdQuery()
	{
				$refers = array( 
					 "crossmo" => 4 , 
					 "dianjing-1" => 121,
					 "ud" => 25,
					 "qc" => 26, //千尺
					 "51" => 131,
				 );
				$refer =$_REQUEST['refer']  ?  trim( $_REQUEST['refer'] ) : trim(  $_REQUEST['input_uid'] );
		
				$referCode =  $refers[$refer];
				
				if( $referCode <= 0 )
				{
					$this->display( "thirdQuery.php" );
					return;
				}

				
                $dbUid = 428373593;
                $dbEngine = Common::_getDbEngine( $dbUid );

                //查询一个最小时间
                
               // $referCode = 1;
                
                $sql = "select min( registerTime  ) as minTime , max( loginTime ) as maxTime from user_profile where refer = $referCode ";
                $data =  $dbEngine->fetchArray( $sql );
                $minTime = $data[0]['minTime'];
                $maxTime = $data[0]['maxTime'];
                $MinMonth =  date( "Y-m" , $minTime );
                $MaxMonth =  date( "Y-m" , $maxTime );
                
                $minTime = strtotime(  $MinMonth."-01 00:00:00" );
                 
                $and = " and status = 1 ";
                
                $recordData = array();
                //按月查询
                if( strlen( $_GET['date'] ) != 7 )
                {
	                for(  $i = 0 ; ;$i++  )
	                {	
	                	$startTime = strtotime( "+$i month" , $minTime );
	                	$nextMonth = $i+1;
	                	$nextTime = strtotime( "+$nextMonth month" , $minTime );
	                	
	                	$MinMonth = date( "Y-m" , $startTime );
	                	
	                	
						$sql = "select count(uid )  as count from user_profile where refer = $referCode  and registerTime >= $startTime and registerTime < $nextTime  ";
						$data =  $dbEngine->fetchArray( $sql );
						$recordData[$MinMonth]['register'] = $data[0]['count'];
						
						
						$sql = "select count(o.uid )  as count from `order`  o inner join user_profile up  on up.uid = o.uid where refer = $referCode $and  and registerTime >= $startTime and registerTime < $nextTime  ";
						$data =  $dbEngine->fetchArray( $sql );
						$recordData[$MinMonth]['orderCount'] = $data[0]['count'];
			
						
						$sql = "select sum( price )  as count from `order`  o inner join user_profile up on up.uid = o.uid where refer = $referCode  $and and registerTime >= $startTime and registerTime < $nextTime  ";
						$data =  $dbEngine->fetchArray( $sql );
						$recordData[$MinMonth]['orderSum'] = $data[0]['count'];
						
					
	                	if( $MinMonth == $MaxMonth )
	                	{
	                		break;
	                	}
	                }
                }
                //按天查
                else 
                {
              		$month = $_GET['date'];
              		$startTime = strtotime( $month."-01 00:00:00" );
              		$endTime = strtotime( $month."-01 23:59:59" );
              		for(  $i = 0 ; ;$i++  )
              		{
     
              			$day = date( "Y-m-d" , $startTime );
              			$sql = "select count(uid )  as count from user_profile where refer = $referCode  and registerTime >= $startTime and registerTime <= $endTime  ";
              			$data =  $dbEngine->fetchArray( $sql );
              			$recordData[$day]['register'] = $data[0]['count'] ;

              			
              			
              			$sql = "select count(o.uid )  as count from `order`   o inner join user_profile up on up.uid = o.uid  where refer = $referCode  $and and registerTime >= $startTime and registerTime <= $nextTime  ";
              			$data =  $dbEngine->fetchArray( $sql );
              			$recordData[$day]['orderCount'] = $data[0]['count'];
              				
              			
              			$sql = "select sum( price )  as count from `order`  o inner join user_profile up on up.uid = o.uid  where refer = $referCode $and  and registerTime >= $startTime and registerTime <= $nextTime  ";
              			$data =  $dbEngine->fetchArray( $sql );
              			$recordData[$day]['orderSum'] = $data[0]['count'];
              			
   
              			$startTime = $startTime + 86400;
              			$endTime = $endTime + 86400;
              			if( date( "Y-m" , $startTime  ) !=  $month  )
              			{
              				break;
              			}
              		}
                }
              
            
                $sql = "select count(*) as num from user_profile where  refer = $referCode";
                $data = $dbEngine->fetchArray( $sql );
            	$total = $data[0]['num'];    
            	
            	
            	$sql = "select count(*) as num from `order`  o inner join user_profile up on up.uid = o.uid  where  refer = $referCode $and";
            	$data = $dbEngine->fetchArray( $sql );
            	$orderCount = $data[0]['num'];
            	
            	
            	$sql = "select sum(price) as num from `order`  o inner join user_profile up on up.uid = o.uid  where  refer = $referCode $and";
            	$data = $dbEngine->fetchArray( $sql );
            	$sumTotal = $data[0]['num'];
            	
            	$this->assign( 'recordData' , $recordData );
            	$this->assign( 'total' , $total );
            	$this->assign( 'orderCount' , $orderCount );
            	$this->assign( 'sumTotal' , $sumTotal );
                $this->display( "thirdQuery.php" );
	}
	

	/**
	 * 运维工具
	 */
	public function tools()
	{
		$this->display( 'tools.php' );
	}
	
	/**
	 * 测试工具
	 */
	public function unitTest()
	{
		$this->display( 'unitTest.php' );
	}
	public function newstatsPay()
	{
		$this->display('newstatsPay.php');
	}
	public function activestatsLogin()
	{
		//$cache = Common::getCache();
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
	
		$monthStats = array();
		$useTime = 0;
		Stats_Active::getActiveData(  $monthStats , $useTime  );
	
		$this->assign( "monthStats", $monthStats );
		$this->assign( "useTime", strval( $useTime) );
		$this->display('activestatsLogin.php');
	}
	//渠道付费统计
	public function channelstatsPay()
	{
		$statsName = "channelstatsPay";
		$useStartTime = microtime( true );
		
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
		
		$serverTime = $_GET['time'] ?   $_GET['time']  :  $_SERVER['REQUEST_TIME'];
		
		$statsData = Stats_Pay::getChannelStatsPay( $statsName , $serverTime );
		
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "serverTime", $serverTime );
		$this->assign( "statsName", $statsName );
		
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 新注册来源统计 
	 * Enter description here ...
	 */
	public function newstatsLogin()
	{
		$statsName = "newstatsLogin";
		$useStartTime = microtime( true );
		
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
		
		$serverTime = $_GET['time'] ?   $_GET['time']  :  $_SERVER['REQUEST_TIME'];
		
		$statsData = Stats_Active::getNewStatsLogin(  $statsName , $serverTime  );
		
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "serverTime", $serverTime );
		$this->assign( "statsName", $statsName );
		
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 新注册留存
	 * Enter description here ...
	 */
	public function newloginKeep()
	{
		$serverTime = $_GET['time'] ?   $_GET['time']  :  $_SERVER['REQUEST_TIME'];
		$cache = Common::getCache();
		
		$key = "monthStats".date("Ym" , $serverTime );
		$monthStats = $cache->get( $key );
		
		$this->assign( "monthStats", $monthStats );
		$this->display('newloginKeep.php');
	}
	
	public function newloginPay()
	{
		$this->display('newloginPay.php');
	}
	public function loginSource()
	{
		$this->display('loginSource.php');
	}
	public function continuestatsActive()
	{
		$this->display('continuestatsActive.php');
	}
	public function dataStatistics()
	{
		//	$this->display('dataShow.php');
		$this->display('dataStatistics.php');
	}
	/*
	 * 统计菜单
	 */
	public function statsMenu()
	{
		$this->display('statsMenu.php');
	}
	
	/**
	 * 统计数据显示
	 */
	public function statDisplay()
	{
		$act = trim( $_REQUEST['act'] );
		$startDate = trim( $_GET['startDate'] );
		$endDate = trim( $_GET['endDate'] ); 		 


		$data = Stats_Display::doDisplay( $act , $startDate , $endDate );
		$this->assign( "data", $data );
		
		$tpl = Stats_Display::$menu[$act]['tpl'];
		$this->display( "stats/{$tpl}.php" );
	}
	
	
	
	/**
	 * 竞技场测试
	 */
	public function arenaTest()
	{
		$do = empty( $_GET[ 'do' ] ) ? '' : $_GET[ 'do' ];
		if($do == 'pk'){
			$cCard = json_decode($_GET['cCard'],true);
			$bCard = json_decode($_GET['bCard'],true);
			$times = $_GET['times'];
			$isFirst = intval($_GET['isFirst']);
			$testStr = Stats_Test::attackTest( $cCard , $bCard , $times , $isFirst );
			echo $testStr;
		}
		else
		{
			$this->display('arenaTest.php');
		}
	}
	
	public function robots()
	{
		$this->display('robots.php');
	}
	
	public function robotClient()
	{
		$this->display('robotClient.php');
	}
	
	/**
	 * 邮件
	 */
	public function mail()
	{
		$do = empty( $_GET[ 'do' ] ) ? '' : $_GET[ 'do' ];
		if($do == 'send')
		{
			$ret = array("rc" => 1);
			do{
				$fromId = 0;
				$toId = intval($_REQUEST['toId']);
				if(User_Model::exist( $toId ) == false) break;
				$title = strval($_REQUEST['title']);
				$content = strval($_REQUEST['content']);
				//$bonus = json_decode($_REQUEST['bonus'], true);
				$bonus = null;
				$deadTime = 0;
				if(!empty($_REQUEST['sendAll']))
				{
					
				}
				else
				{
					$id = Mail_Model::getInstance($toId)->sendMail($fromId, $title, $content, $bonus, $deadTime);
					ObjectStorage::save();
					$ret['rc'] = 0;
					$ret['id'] = $id;
				}
			}while(0);
			
			echo json_encode($ret);
		}
		else
		{
			$this->display('mail.php');
		}
	}
	
	/**
	 * 数据统计:登录统计 
	 */
	public function statsLogin()
	{
		$statsName = "statsLogin";
		//$cache = Common::getCache();
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
		$serverTime = $_GET['time'] ?   $_GET['time']  :  $_SERVER['REQUEST_TIME'];
		                
		$loginData = Stats_Active::getStatsLogin( $statsName , $serverTime , $do , $userId );
		                
		$this->assign( "monthStats", $loginData );
		$this->display( 'stats.php' );
	}
	
	/**
	 * 数据统计：活跃统计
	 */
	public function statsActive()
	{
		//$cache = Common::getCache();
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];

		$monthStats = array();
		$useTime = 0;
		Stats_Active::getActiveData(  $monthStats , $useTime  );
		$this->assign( "monthStats", $monthStats );
		$this->assign( "useTime", strval( $useTime) );
		$this->display( 'statsActive.php' );
		
	}
	
	/**
	 * 数据统计：付费统计
	 */
	public function statsPay()
	{
		//$cache = Common::getCache();
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
	
		$startTime = microtime( true );
		
		$monthStats = Stats_Pay::getStatsPay();
		
		$endTime = microtime( true );
		$useTime = sprintf( "%6.fS" , ( $endTime - $startTime )  );
		$this->assign( "monthStats", $monthStats );
		$this->assign( "useTime", strval( $useTime) );
	
		$this->display( 'statsPay.php' );
	
	}
	
	/**
	 * 元宝消耗统计
	 */
	public function coinConsume()
	{
		$statsName = "coinConsume";
		$useStartTime = microtime( true );
		
		$serverTime = $_GET['time'] ?   $_GET['time']  :  $_SERVER['REQUEST_TIME'];
		//按月查询

		$result = Stats_Consume::getCoinConsume( $statsName , $serverTime );
		
		$this->assign( "consumeData" , $result );
		
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "serverTime", $serverTime );
		$this->assign( "statsName", $statsName );
		
		$this->display( $statsName.".php" );
	}
	
	/**
	 * 元宝留存统计
	 */
	public function coinKeep()
	{
		$statsName = "coinKeep";
		$useStartTime = microtime( true );
		
		$serverTime = $_GET['time'] ?   $_GET['time']  :  $_SERVER['REQUEST_TIME'];
		//按月查询

		$result = Stats_Consume::getCoinKeep( $statsName , $serverTime );
		
		$this->assign( "statsData" , $result );
		
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "serverTime", $serverTime );
		$this->assign( "statsName", $statsName );
		
		$this->display( $statsName.".php" );
	}
	
	
	/**
	 * 禁用用户列表
	 */
	public function banList()
	{
		$statsName = "banList";
		$useStartTime = microtime( true );
		
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET['input_uid'] ) ? null : intval( $_GET[ 'input_uid' ] );
		
		$pageSize = 10;
		$currPage = $_GET['currPage'] ? intval(  $_GET['currPage'] ) : 1 ;
		$totalNum = 0;
		
		$byUid = strval ( trim( $_GET['byUid'] ) );
		if( $do != "query" && User_Model::exist( $byUid ) )
		{
			if( $do == "add" )
			{
				Stats_User::banUser( $byUid );
			}
			elseif( $do == "del" )
			{
				Stats_User::releaseUser( $byUid );
			}
		}
		
		$statsData = Stats_User::getBanList( $currPage , $pageSize , $totalNum );
		
		$this->assign( 'pageSize' , $pageSize );
		$this->assign( 'totalNum' , $totalNum );
		$this->assign( 'currPage' , $currPage );
		
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "statsName", $statsName );
		
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 激活码使用记录
	 */
	public function activeCode()
	{
		$statsName = "activeCode";
		$useStartTime = microtime( true );
	
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET['input_uid'] ) ? null : intval( $_GET[ 'input_uid' ] );
	
		$pageSize = 10;
		$currPage = $_GET['currPage'] ? intval(  $_GET['currPage'] ) : 1 ;
		$totalNum = 0;
	
		$byUid = strval ( trim( $_GET['byUid'] ) );
		if( $do != "query" && User_Model::exist( $byUid ) )
		{
			/**
			if( $do == "add" )
			{
				Stats_User::banUser( $byUid );
			}
			elseif( $do == "del" )
			{
				Stats_User::releaseUser( $byUid );
			}
			*/
		}
	
		$statsData = Stats_User::getActiveCodeHistory( $currPage , $pageSize , $totalNum );
	
		$this->assign( 'pageSize' , $pageSize );
		$this->assign( 'totalNum' , $totalNum );
		$this->assign( 'currPage' , $currPage );
	
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "statsName", $statsName );
	
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 第三方平台邀请记录
	 */
	public function inviteThirdHistory()
	{
		$statsName = "inviteThirdHistory";
		$useStartTime = microtime( true );
	
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET['input_uid'] ) ? null : intval( $_GET[ 'input_uid' ] );
	
		$statsData = $userId ? Friend_Model::inviteThirdHistory($userId) : array();
	
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "statsName", $statsName );
	
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 用户列表
	 */
	public function userList()
	{
		$statsName = "userList";
		$useStartTime = microtime( true );
	
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET['input_uid'] ) ? null : intval( $_GET[ 'input_uid' ] );
	
		$pageSize = 20;
		$currPage = $_GET['currPage'] ? intval(  $_GET['currPage'] ) : 1 ;
		$totalNum = 0;
	
		$statsData = Stats_User::getUserList( $currPage , $pageSize , $totalNum );
	
		$this->assign( 'pageSize' , $pageSize );
		$this->assign( 'totalNum' , $totalNum );
		$this->assign( 'currPage' , $currPage );
	
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "statsName", $statsName );
	
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 用户登录流水
	 */
	public function userLoginLog()
	{
		$statsName = "userLoginLog";
		$useStartTime = microtime( true );
		
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET['input_uid'] ) ? null : intval( $_GET[ 'input_uid' ] );
		
		$pageSize = 10;
		$currPage = $_GET['currPage'] ? intval(  $_GET['currPage'] ) : 1 ;
		$totalNum = 0;
		
		$statsData = Stats_Active::getUserLoginLog( $currPage , $pageSize , $totalNum );
		
		$this->assign( 'pageSize' , $pageSize );
		$this->assign( 'totalNum' , $totalNum );
		$this->assign( 'currPage' , $currPage );
		
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "statsName", $statsName );
		
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 用户元宝流水
	 */
	public function userCoinLog()
	{
		$statsName = "userCoinLog";
		$useStartTime = microtime( true );
		
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET['input_uid'] ) ? null : intval( $_GET[ 'input_uid' ] );
		
		$pageSize = 20;
		$currPage = $_GET['currPage'] ? intval(  $_GET['currPage'] ) : 1 ;
		$totalNum = 0;
		
		$_GET['coinType'] = isset( $_GET['coinType'] ) ? $_GET['coinType'] : "consume";
		
		$statsData = Stats_User::getCoinLog( $currPage , $pageSize , $totalNum );
		
		$this->assign( 'pageSize' , $pageSize );
		$this->assign( 'totalNum' , $totalNum );
		$this->assign( 'currPage' , $currPage );
		
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "statsName", $statsName );
		
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 用户道具流水
	 */
	public function userItemLog()
	{
		$statsName = "userItemLog";
		$useStartTime = microtime( true );
		
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET['input_uid'] ) ? null : intval( $_GET[ 'input_uid' ] );
		
		$pageSize = 20;
		$currPage = $_GET['currPage'] ? intval(  $_GET['currPage'] ) : 1 ;
		$totalNum = 0;
		
		$statsData = Stats_User::getItemLog( $currPage , $pageSize , $totalNum );
		
		$this->assign( 'pageSize' , $pageSize );
		$this->assign( 'totalNum' , $totalNum );
		$this->assign( 'currPage' , $currPage );
		
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "statsName", $statsName );
		
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 用户竞技场流水
	 */
	public function userArenaLog()
	{
		$statsName = "userArenaLog";
		$useStartTime = microtime( true );
		
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET['input_uid'] ) ? null : intval( $_GET[ 'input_uid' ] );
		
		$pageSize = 20;
		$currPage = $_GET['currPage'] ? intval(  $_GET['currPage'] ) : 1 ;
		$totalNum = 0;
		
		$statsData = Stats_User::getArenaLog( $currPage , $pageSize , $totalNum );
		
		$this->assign( 'pageSize' , $pageSize );
		$this->assign( 'totalNum' , $totalNum );
		$this->assign( 'currPage' , $currPage );
		
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "statsName", $statsName );
		
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 玩家在线统计 
	 * Enter description here ...
	 */
	public function statsUserOnline()
	{
		$statsName = "statsUserOnline";
		$useStartTime = microtime( true );
		
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
		
		
		$serverTime = $_SERVER['REQUEST_TIME'];
		$min = date("i" , $serverTime )- date("i" , $serverTime )%5;
		//查询2小时内数据
		$endTime = strtotime( date( "Ymd H:{$min}:00" , $serverTime  ) );
		$startTime = $_GET['startTime'] ?   $_GET['startTime']  :  ($endTime - 1*60*60 );
		
		//前24个五分钟，2小时的在线人数
		$statsData = Stats_Active::getStatsUserOnline( $statsName , $startTime  );
		//print_r( $statsData );exit;
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "serverTime", $serverTime );
		$this->assign( "startTime", $startTime );
		$this->assign( "endTime", $endTime );
		$this->assign( "statsName", $statsName );
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 玩家在线统计 
	 * Enter description here ...
	 */
	public function statsUserOnlineByMonth()
	{
		$statsName = "statsUserOnlineByMonth";
		$useStartTime = microtime( true );
		
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
		
		$serverTime = $_GET['time'] ?   $_GET['time']  :  $_SERVER['REQUEST_TIME'];
		
		$statsData = Stats_Active::getStatsUserOnlineByMonth( $statsName , $serverTime );
		
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "serverTime", $serverTime );
		$this->assign( "statsName", $statsName );
		
		$this->display( $statsName.'.php' );
	}
	
	/**
	 * 新手引导统计
	 * Enter description here ...
	 */
	public function statsUserGuide()
	{
		$statsName = "statsUserGuide";
		$useStartTime = microtime( true );
		
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
		
		$serverTime = $_GET['time'] ?   $_GET['time']  :  $_SERVER['REQUEST_TIME'];
		
		$statsData = Stats_Active::getStatsUserGuide( $statsName , $serverTime );
		
		$this->assign( "statsData", $statsData );
		$useEndTime = microtime( true );
		$this->assign( "useTime", strval( $useEndTime - $useStartTime ) );
		$this->assign( "serverTime", $serverTime );
		$this->assign( "statsName", $statsName );
		
		$this->display( $statsName.'.php' );
	}
	
	public function fileLog()
	{
		$this->display( 'fileLog.php' );
	}
	
	/**
	 * 管理员管理,权限分配
	 */
	public function admin()
	{
		//$cache = Common::getCache();
		$do = empty( $_GET[ 'do' ] ) ? 'get' : $_GET[ 'do' ];
		$userId = empty( $_GET[ 'input_uid' ] ) ? null: (int) $_GET[ 'input_uid' ];
		$adminObj = Data_Admin_Model::getInstance( 1 , true );
		if( $do == 'add' )
		{
			$loginName = $_POST['loginName'];
			$password = $_POST['password'];
			$permissions = $_POST['permissions'];
			$permission = array_sum( $permissions );
			
			$adminObj->addAdmin( $loginName , $password , $permission );
			ObjectStorage::save();
		}
		elseif( $do == 'delete' ) 
		{
			$loginName = $_POST['loginName'];
			$adminObj->deleteAdmin( $loginName );
			ObjectStorage::save();
		}
		
		$permName = array(
				1 => '游戏数据只读',
				2 => '游戏数据读写',
				4 => '配置管理',
				8 => '订单管理',
				16 => '数据分析',
				32 => '管理员管理',
		);
		
		$adminList = $adminObj->getData();
		foreach ( $adminList as $name => $admin )
		{
			$adminList[$name]['checked'] = $this->getChecked( $admin['permission'] );
			$perString = "";
			foreach ( $adminList[$name]['checked'] as $checked )
			{
				$perString .= $permName[$checked]."--";
			}
			
			if($admin['permission'] == "*")
			{
				$perString = "所有权限";
			}
			$adminList[$name]['checkedName'] = $perString;
		}
	
		$this->assign( 'adminList', $adminList );
		$this->display( 'admin.php' );
	}
	
	public function serverAdmin()
	{
		$this->display( 'serverAdmin.php' );
	}
	
	/**
	 * 
	 * 删除用户信息
	 * @param 用户ID
	 * @return array
	 */
	protected function deleteUser( $userId )
	{
		if( is_null( $userId ) )return FALSE;
		try 
		{
			User_Model::deleteUser( $userId );
		}
		catch ( Exception $e )
		{
			echo '出错啦！错误号:'.$e->getCode().' 错误信息:'.$e->getMessage();
		}
		return TRUE;
	}
	
	private function _checkPermiss( $f )
	{
		//1,游戏数据只读
		//2,游戏数据读写
		//4,配置管理
		//8,订单管理
		//16,数据分析
		//32,管理员管理
		$permissArr = $this->getChecked( $_SESSION['admin']['permission'] );	
		if( $f == 'order' )
		{
			if(  !in_array( 8 , $permissArr ) )
			{
				$this->display( 'noPermiss.php' );exit;
			}	
		}
		elseif(  preg_match( "/stats/",  $f )  )
		{
			if(  !in_array( 16 , $permissArr ) )
			{
				$this->display( 'noPermiss.php' );exit;
			}
		}
		elseif( $f == 'admin'  )
		{
			if(  !in_array( 32 , $permissArr ) )
			{
				$this->display( 'noPermiss.php' );exit;
			}
		}
		
	}
	
	
	public function exportCsv()
	{
		$act = $_GET['act'] ? $_GET['act'] : 'stat';
		header("Content-Disposition:attachment;filename={$act}.csv" );
		header('Content-type: text/csv; charset=UTF-16LE');
		echo mb_convert_encoding($_GET['data'],"GB2312","UTF-8");
		
		
	}
	
}

?>
