<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

class ConfigController extends AdminBaseController
{
	const UPDATE_TABLE_STATUS_NORMAL = 0;
	const UPDATE_TABLE_STATUS_FATAL_ERROR = 1;
	const UPDATE_TABLE_STATUS_CONFIG_ERROR = 2;
	
	public function __construct()
	{
		parent::__construct();
		
		$permissArr = $this->getChecked( $_SESSION['admin']['permission'] );
		if(  !in_array( 4 , $permissArr ) )
		{
			$this->display( 'noPermiss.php' );exit;
		}
	}
	
	public function showExcelList()
	{
		$excelList = new ExchangeConfig_Exchanger_List_Excel();
		$phpConfigList = new ExchangeConfig_Exchanger_List_PHPConfig();
		include( TPL_DIR .'/Config/showExcelList.php' );
	}
	
	public function showExcelFile()
	{
		if( strlen( $excelFileName = trim( $_POST['fileName'] ) ) <= 0 )
		{
			echo '请选择文件名称';
			return;
		}
		
		$excelList = new ExchangeConfig_Exchanger_List_Excel();
		if( !isset( $excelList[$excelFileName] ) )
		{
			echo '没有对应的转换器';
			return;
		}
		
		$exchanger = $excelList[$excelFileName];
		$table = $exchanger->getTable();
		include( TPL_DIR .'/Config/showExcelFile.php' );
	}
	
	public function showPHPList()
	{
		$phpConfigList = new ExchangeConfig_Exchanger_List_PHPConfig();
		include( TPL_DIR .'/Config/showPHPList.php' );
	}
	
	public function showPHPFile()
	{
		if( strlen( $phpFileName = trim( $_POST['fileName'] ) ) <= 0 )
		{
			echo '请选择文件名称';
			return;
		}
		
		$phpConfigList = new ExchangeConfig_Exchanger_List_PHPConfig();
		if( !isset( $phpConfigList[$phpFileName] ) )
		{
			echo '没有对应的转换器';
			return;
		}
		
		$exchanger = $phpConfigList[$phpFileName];
		$phpConfig = $exchanger->getPHPConfig();
		include( TPL_DIR .'/Config/showPHPFile.php' );
	}
	
	public function uploadFile()
	{
		$excelList = new ExchangeConfig_Exchanger_List_Excel();
		$messages = array();

		foreach( $_FILES['uploadFiles']['type'] as $fileIndex => $fileType )
		{
		
			if( filesize( $_FILES['uploadFiles']['tmp_name'][$fileIndex] ) <= 0 )
			{
				continue;
			}
			
			$excelFileName = $_FILES['uploadFiles']['name'][$fileIndex];

			
			if( $fileType != 'application/vnd.ms-excel'
				&& $fileType != 'application/octet-stream'
				&& $fileType != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
				&& $fileType != 'application/json'
				&& $fileType != 'application/kset' )
			{
				$messages[] = "{$excelFileName} 文件格式错误:".$fileType;
				continue;
			}
			
	
			if( !isset( $excelList[$excelFileName] ) )
			{
				$messages[] = "还没有建立 {$excelFileName} 文件的转换器";
				continue;
			}

			
			$messages[] = "{$excelFileName} 上传成功。";
			
			$excelList[$excelFileName]->setTable( $_FILES['uploadFiles']['tmp_name'][$fileIndex] );
			$excelList[$excelFileName]->validateExcelFile();
			foreach( $excelList[$excelFileName]->getValidateError() as $error )
			{
				$messages[] = $error;
			}
		}
		
		include( TPL_DIR .'/Config/showExcelList.php' );
	}
	
	
	public function buildConfig()
	{
		$phpConfigList = new ExchangeConfig_Exchanger_List_PHPConfig();
		$phpConfigList->buildConfigurations();
			
		header("location:admin.php?mod=Config&act=showExcelList");
	}
	
	public function synchronizeConfig()
	{
		$phpConfigList = new ExchangeConfig_Exchanger_List_PHPConfig();
		$phpConfigList->synchronizeConfig();
		
		include( TPL_DIR .'/Config/synchronizeConfig.php' );
	}
	
	public function uploadPage()
	{
		include( TPL_DIR .'/Config/uploadPage.php' );
	}
	
	/* (non-PHPdoc)
	 * @see AdminBaseController::main()
	 */
	protected function main()
	{
		;
	}

	
}
?>