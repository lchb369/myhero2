<?php
/*
argv0  server host
argv1  server port
argv2  server mode SWOOLE_BASE or SWOOLE_THREAD or SWOOLE_PROCESS
argv3  sock_type  SWOOLE_SOCK_TCP or SWOOLE_SOCK_TCP6 or SWOOLE_SOCK_UDP or SWOOLE_SOCK_UDP6
*/
$serv = swoole_server_create("127.0.0.1", 4477, SWOOLE_PROCESS, SWOOLE_SOCK_TCP);

swoole_server_set($serv, array(
    'timeout' => 2.5,  //select and epoll_wait timeout. 
    'poll_thread_num' => 2, //reactor thread num
    'writer_num' => 2,     //writer thread num
    'worker_num' => 4,    //worker process num
    'backlog' => 128,   //listen backlog
    'max_request' => 1000,
));

$cache = new Memcache();
$cache->addserver( '127.0.0.1' , 11211 );

//一个消息包
$proto = array(
	1000,
	1,
	"s2",
	array(
	  '看到请回复我！', 
	),
);

$protojson = json_encode( $proto );
$protojson = strlen( $protojson ).$protojson;

/*
argv0  server resource
argv1  listen host
argv2  listen port
argv3  sock_type  SWOOLE_SOCK_TCP or SWOOLE_SOCK_TCP6 or SWOOLE_SOCK_UDP or SWOOLE_SOCK_UDP6
*/
//swoole_server_addlisten($serv, "127.0.0.1", 9500, SWOOLE_SOCK_UDP);

function onStartEvent($serv)
{
    echo "Server：start\n";
}

function onShutdownEvent($serv)
{
    echo "Server：onShutdown\n";
}

function onTimerEvent($serv, $interval)
{
    echo "Server：Timer Call.Interval=$interval \n";
}

function onCloseEvent($serv,$fd,$from_id)
{
	echo "Client：Close.\n";
}

function onConnectEvent($serv,$fd,$from_id)
{
	$connList = $cache->get( 'swconn_list_v1' );
	$connList[$fd] = array();
	$cache->set( 'swconn_list_v1' , $connList );
	
	echo "Client：Connect.\n";
}

function onWorkerStartEvent($serv, $worker_id)
{
	echo "WorkerStart[$worker_id]|pid=".posix_getpid().".\n";
}

function onWorkerStopEvent($serv, $worker_id)
{
	echo "WorkerStop[$worker_id]|pid=".posix_getpid().".\n";
}

function onReceiveEvent($serv, $fd, $from_id, $data)
{
    //echo "Client：Data. fd=$fd|from_id=$from_id|data=$data\n";
    swoole_server_send($serv, $fd, $data);
	//swoole_server_send($serv, $fd, 'Server: '.$data);
	//swoole_server_send($serv, $other_fd, "Server: $data", $other_from_id);
	//swoole_server_close($serv, $fd, $from_id);
	//swoole_server_close($serv, $ohter_fd, $other_from_id);
}
function onMasterCloseEvent($serv,$fd,$from_id)
{
    echo "Client：Close.PID=".posix_getpid().PHP_EOL;
}

function onMasterConnectEvent($serv,$fd,$from_id)
{
    echo "Client：Connect.PID=".posix_getpid().PHP_EOL;
}

swoole_server_handler($serv, 'onStart', 'onStartEvent');
swoole_server_handler($serv, 'onConnect', 'onConnectEvent');
swoole_server_handler($serv, 'onReceive', 'onReceiveEvent');
swoole_server_handler($serv, 'onClose', 'onCloseEvent');
swoole_server_handler($serv, 'onShutdown', 'onShutdownEvent');
swoole_server_handler($serv, 'onTimer', 'onTimerEvent');
swoole_server_handler($serv, 'onWorkerStart', 'onWorkerStartEvent');
swoole_server_handler($serv, 'onWorkerStop', 'onWorkerStopEvent');
swoole_server_handler($serv, 'onMasterConnect', 'onMasterConnectEvent');
swoole_server_handler($serv, 'onMasterClose', 'onMasterCloseEvent');

#swoole_server_addtimer($serv, 2);
#swoole_server_addtimer($serv, 10);
swoole_server_start($serv);

