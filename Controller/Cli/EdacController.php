<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

ini_set( 'display_errors' , 'on' );
error_reporting( E_ALL ^ E_NOTICE );

class EdacController extends CliBaseController
{
	
	/**
		启动数据中心消息队列服务端
		$data = array(
				'method' => 'Stat.init',
				'xxx' => 111,
				'yyy' => 2222,
				'data' => $i,
			);
			$cacheQueue->put(  $data  );
		
		shell脚 本:
		res=`ps -ef|grep Edac.runQueueServer|awk '{print int($2)}'`
	    if [ ! "$res" ]; then
	             /opt/php/bin/php /home/www/wwwroot/liuchangbing/myhero_dev/Web/Cli/cli.php Edac.runQueueServer
	    else
	             kill $res
	             /opt/php/bin/php /home/www/wwwroot/liuchangbing/myhero_dev/Web/Cli/cli.php Edac.runQueueServer
	    fi
	    echo "start ok"
	*/
	public function runQueueServer()
	{
		$machineConf = Common::getConfig('MachineConfig');
		if($machineConf['edacUrl'])
		{
			$edacUrl = $machineConf['edacUrl']; 
		}
		else 
		{
			$edacUrl = "http://edac.ecngame.com/Web/Api/api.php";
		}
		
		$cacheQueue = CacheQueue::getInstance();
		$restClient = new RestClient( $edacUrl);
		$errorLog = new ErrorLog( "StatsQueueServer" );

		echo date( "Y-m-d H:i:s")."--server start";
		while(1)
		{
			$params = $cacheQueue->get();
			if( $params )
			{
		    	$rs = $restClient->callMethod( '' ,  $params );
		    	if( $rs['status'] > 0  )
		    	{
		    		$errorLog->addLog( json_encode( $params ) );
		    		$errorLog->addLog( json_encode( $rs ) );
		    	}
			}
			usleep( 1000 );
		}
	}


	public function test()
	{
		$edacUrl = "http://211.144.68.31/EDAC/Web/Api/api.php";
		$queueConfig = array(
			'prefix' => 'cache_queue',
			'name' =>   'edac_stats'
		);
		$cacheQueue = new CacheQueue( $queueConfig );
		
		for( $i = 1 ; $i <= 100 ; $i++ )
		{
			$data = array(
				'method' => 'Stat.init',
				'xxx' => 111,
				'yyy' => 2222,
				'data' => $i,
			);
			$cacheQueue->put(  $data  );
		}
	}

	
}
