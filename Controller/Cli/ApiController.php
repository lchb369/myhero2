<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

ini_set( 'display_errors' , 'on' );
error_reporting( E_ALL ^ E_NOTICE );
ini_set( 'memory_limit' , '512M' );

class ApiController extends CliBaseController
{
	private $_logs = array();
	
	/**
	 * GM工具后台生成统计数据
	 * /opt/php/bin/php myhero_lchb/Web/Cli/cli.php Api.doStatData 
	 */
	public function doStatData()
	{
		$date = $_SERVER['argv'][2];
		$time = $_SERVER['REQUEST_TIME'];
		
		if( trim( $date ) )
		{
			$time = strtotime( $date ) + 2*86400;
		}
		
		/**
		 * 批量执行
		*/
		if( !!$days = $_SERVER['argv'][3] )
		{
			$start = $time;
			$maxSec = $start + 86400*$days;
			for ( $i = $start; $i <  $maxSec ; $i += 86400 )
			{
				Stats_Query::run( $i );
			}
			echo "run over!!";
		}
		else 
		{
			Stats_Query::run( $time );
			echo "run over!!";
		}
		
	}


	public function runToday()
	{
		$time = $_SERVER['REQUEST_TIME'] + 86400;
		Stats_Query::run( $time );
	}
		
	public function test()
	{
		$serverConfig = Common::getConfigFile('ServerConfig');
		foreach($serverConfig as $conf)
		{
			if($conf['sid'] == 1)
			{
				unset($res, $rc);
				exec("php"
					." ".ROOT_DIR."/Web/Cli/cli_sid.php"
					." Api.testSid"
					." ".$conf['pf']
					." ".$conf['sid'], $res, $rc);
				echo "\n".json_encode($res)."==".$rc; 
			}
		}
		
	}
	
	public function testSid()
	{
		//Stats_Test::sendCoinBonus(1);
	}
	
}
