<?php

if( !defined( 'IN_INU' ) )
{
	return;
}

ini_set( 'display_errors' , 'on' );
error_reporting( E_ALL ^ E_NOTICE );

class RobotController extends CliBaseController
{
	
	private $webUrl = '';
	
	public function __construct()
	{
		parent::__construct();
		
		$this->webUrl = Common::getServerAddr().'Web/Api/api.php';
	}
	
	/**
		机器人客户端
        /opt/php/bin/php /home/www/wwwroot/liuchangbing/myhero_dev/Web/Cli/cli.php Robot.start kr 25 1 10
	*/
	public function start()
	{
		//method, pf, sid, startId, endId
		if(count($argv) < 6) return false;
		
		$pf = intval($argv[2]);
		$sid = intval($argv[3]);
		$startId = intval($argv[4]);
		$endId = intval($argv[5]);
		
		$this->_foreach('getSession', $startId, $endId);
		$this->_foreach('setNewbie', $startId, $endId);
		$this->_foreach('randomAction', $startId, $endId);
		
		while(1)
		{
			$this->_foreach('randomAction', $startId, $endId);
			usleep( 10000 );
		}
		
		return true;
	}

	private function getSession($id)
	{
		$url = $this->webUrl;
		$result = Helper_WWW::get($url, array(
			
		));
	}
	
	private function setNewbie($id)
	{
		
	}
	
	private function randomAction($id)
	{
		
	}
	
	private function sceneInit($id)
	{
		
	}
	
	private function _foreach($method, $startId, $endId)
	{
		for($i = $startId; $i < $endId; $i++)
		{
			$this->$method($i);
		}
	}
	
}
